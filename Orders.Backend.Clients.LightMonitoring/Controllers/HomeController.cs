﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Orders.Backend.Clients.LightMonitoring.Controllers
{
    public class HomeController : Controller
    {
        // GET: Search
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
    }
}