﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Http;
using AutoMapper;
using OnlineOrders.Common.Helpers;
using Order.Backend.Dal.Common;
using Orders.Backend.Clients.LightMonitoring.Models;
using Orders.Backend.Clients.LightMonitoring.P.Commands;
using Orders.Backend.Clients.LightMonitoring.Proxy.Auth;
using Orders.Backend.Clients.LightMonitoring.Proxy.InstallationServices;
using Orders.Backend.Clients.LightMonitoring.Proxy.Online;
using Orders.Backend.Clients.LightMonitoring.Proxy.StockLocationSelector;
using Orders.Backend.Clients.LightMonitoring.Proxy.TicketTool;
using Orders.Backend.Clients.LightMonitoring.Utils;
using Orders.Backend.Dal;
using Orders.Backend.Router.Dal.Model;
using Installation = Orders.Backend.Clients.LightMonitoring.Proxy.InstallationServices.Installation;
using OperationResult = Orders.Backend.Clients.LightMonitoring.Models.OperationResult;
using OrderLine = Orders.Backend.Dal.OrderLine;
using OrderServiceClient = Orders.Backend.Clients.LightMonitoring.Proxy.Router.OrderServiceClient;
using ReserveLine = Orders.Backend.Dal.ReserveLine;
using ReturnCode = Orders.Backend.Clients.LightMonitoring.Proxy.Router.ReturnCode;

namespace Orders.Backend.Clients.LightMonitoring.Controllers
{
    [Authorize]
    public class OrdersController : ApiController
    {
        private const int MaxResults = 10;

        private static readonly ConcurrentBag<string> ApprovedTemporary = new ConcurrentBag<string>();

        [HttpGet]
        [ActionName("FindByParameters")]
        public async Task<FindBeParametersResult> FindByParameters(string orderId = "", string wwsOrderId = "", string name = "", string phone = "", string email = "")
        {
            UserInfo currentUser;
            using(var client = new AuthenticationServiceClient())
            {
                currentUser = await client.GetCurrentUserAsync();
                if(currentUser == null)
                    return null;
            }

            using(var transactionScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                using(var ctx = new RoutableOrderDbEntities())
                {
                    var orderMonitorings = ctx.OrderMonitorings.AsQueryable();

                    if(currentUser.Roles.Contains(ConfigurationManager.AppSettings["RoleToRestrict"]))
                    {
                        var targetParameters =
                            currentUser.Parameters.Union(new[] { ConfigurationManager.AppSettings["LipsStore"] })
                                .ToArray();
                        orderMonitorings =
                            orderMonitorings.Where(
                                el =>
                                    targetParameters
                                        .Contains(el.SapCode));
                    }

                    if(!string.IsNullOrEmpty(orderId))
                    {
                        orderMonitorings = orderMonitorings.Where(m => m.OrderId == orderId);
                    }
                    if(!string.IsNullOrEmpty(wwsOrderId))
                    {
                        orderMonitorings = orderMonitorings.Where(m => m.WWSOrderId == wwsOrderId);
                    }
                    if(!string.IsNullOrEmpty(name))
                    {
                        orderMonitorings = orderMonitorings.Where(m => m.CustomerName.Contains(name));
                    }
                    if(!string.IsNullOrEmpty(phone))
                    {
                        orderMonitorings = orderMonitorings.Where(m => m.CustomerPhone.Contains(phone));
                    }
                    if(!string.IsNullOrEmpty(email))
                    {
                        orderMonitorings = orderMonitorings.Where(m => m.CustomerEmail == email);
                    }

                    var minDate = DateTime.Now.AddDays(-30);
                    var orderMonitoringResult =
                        orderMonitorings.Where(m => m.CreateDate > minDate).Take(MaxResults).ToArray();

                    transactionScope.Complete();

                    var message = string.Empty;
                    if(orderMonitoringResult.Length > MaxResults)
                    {
                        message = string.Format("Найдено слишком много заказов. Показано первые {0}", MaxResults);
                    }

                    var monitorings =
                        Mapper.Map<IEnumerable<OrderMonitoringResult>>(orderMonitoringResult.Take(MaxResults));

                    return new FindBeParametersResult(monitorings, message);
                }
            }
        }

        [HttpGet]
        [ActionName("GetTicketTasks")]
        public async Task<TicketTaskViewModel[]> GetTicketTasks(string orderId)
        {
            var tasks = new List<TicketTaskViewModel>();
            using (var client = new TicketToolServiceClient())
            {
                var result = await client.GetTasksByWorkitemIdAsync(orderId);
                var tasksViewModel = Mapper.Map<IEnumerable<TicketTaskViewModel>>(result).ToList();
                tasksViewModel.ForEach(m =>
                {
                    m.Created = m.Created.AsMoscowDateTime();
                    m.Completed = m.Completed.AsMoscowDateTime();
                    m.ReCallInfo = null;
                    var response = m.Response;
                    if (response != null && response.FilledForm != null && response.FilledForm.Any())
                    {
                        var isContain = response.FilledForm.ContainsKey("Перезвоните мне через");
                        if (isContain)
                        {
                            var isTs = response.FilledForm["Перезвоните мне через"] is TimeSpan;
                            if (isTs)
                            {
                                var ts = (TimeSpan) response.FilledForm["Перезвоните мне через"];
                                var tsSections = new List<string>();
                                if (ts.Days > 0)
                                {
                                    tsSections.Add(ts.ToString(@"d\д"));
                                }
                                if (ts.Hours > 0)
                                {
                                    tsSections.Add(ts.ToString(@"h\ч"));
                                }
                                if (ts.Minutes > 0)
                                {
                                    tsSections.Add(ts.ToString(@"m\м"));
                                }
                                m.ReCallInfo = String.Format("Перезвоните мне через {0}", String.Join(" ", tsSections));
                            }
                        }}
                 
                  
                });
                tasks.AddRange(tasksViewModel);
            }

            bool showDescription;
            UserInfo currentUser;
            using(var client = new AuthenticationServiceClient())
            {
                currentUser = await client.GetCurrentUserAsync();
                showDescription = currentUser != null && currentUser.Roles.Contains(ConfigurationManager.AppSettings["AdminRole"]);
            }

            using (var client = new CloudWfProxyServiceClient())
            {
                var commands = await client.GetCommandsTrackByRelatedEntityIdAsync(orderId);
                var commandsTasks = commands.SenderCommand
                    .Select(c => c.MapToTaskViewModel(
                        commands.ReceiverCommandResponses.FirstOrDefault(r => r.RefCommandKey != null && r.RefCommandKey.CorrelationId == c.CommandKey.CorrelationId),
                        commands.ReceiverCommands,
                        commands.SenderResponses,
                        showDescription
                    ));
                tasks.AddRange(commandsTasks);

                var receiveTasks = commands.ReceiverCommands.Where(c => c.ParentCommand == null).Select(c => c.MapToTaskViewModel(
                        commands.SenderResponses.FirstOrDefault(sr => sr.RefCommandKey != null && sr.RefCommandKey.CorrelationId == c.CommandKey.CorrelationId),
                        showDescription
                    ));
                tasks.AddRange(receiveTasks);
            }
            return tasks.OrderBy(m => m.Created).ToArray();
        }

        [HttpGet]
        [ActionName("GetStockLocationOutcomes")]
        public async Task<GetSaleLocationOutcomesResult> GetStockLocationOutcomes(string orderId)
        {
            using(var client = new SaleLocationServiceClient())
            {
                return await client.GetSaleLocationOutcomesAsync(orderId);
            }
        }

        [HttpGet]
        [ActionName("GetReserveHistory")]
        public ReserveHeader[] GetReserveHistory(string orderId)
        {
            using(var ctx = new OnlineOrdersContainer())
            {
                ctx.ContextOptions.LazyLoadingEnabled = false;
                var lines = ctx.ReserveHeader.Where(tt => tt.OrderHeader.OrderId == orderId)
                            .OrderByDescending(tt => tt.WWSOrderId)
                            .ToArray();
                return lines;
            }
        }

        [HttpGet]
        [ActionName("GetOrderLines")]
        public OrderLine[] GetOrderLines(string orderId)
        {
            using(var ctx = new OnlineOrdersContainer())
            {
                ctx.ContextOptions.LazyLoadingEnabled = false;
                return ctx.OrderLines.Where(h => h.OrderId == orderId).ToArray();
            }
        }

        [HttpGet]
        [ActionName("GetReserveLines")]
        public IEnumerable<ReserveLine> GetReserveLines(string orderId)
        {
            using(var ctx = new OnlineOrdersContainer())
            {
                ctx.ContextOptions.LazyLoadingEnabled = false;
                var lines = ctx.ReserveLine.Where(tt => tt.ReserveHeader.OrderHeader.OrderId == orderId)
                    .GroupBy(tt => tt.ReserveHeader.ReserveId)
                    .OrderByDescending(tt => tt.Key)
                    .FirstOrDefault();
                return lines == null ? Enumerable.Empty<ReserveLine>() : lines.ToArray();
            }
        }

        [HttpGet]
        [ActionName("GetOrderStatuses")]
        public OrderStatusHistory[] GetOrderStatuses(string orderId)
        {
            using(var ctx = new OnlineOrdersContainer())
            {
                ctx.ContextOptions.LazyLoadingEnabled = false;
                var result = ctx.OrderStatusHistories
                    .Where(tt => tt.OrderId == orderId)
                    .OrderByDescending(tt => tt.UpdateDate).ToList();
                result.ForEach(m => m.UpdateDateTime = m.UpdateDateTime.AsMoscowSmallDateTime()); //TODO Fix it
                return result.ToArray();
            }
        }

        [HttpGet]
        [ActionName("GetAdditionalInfo")]
        public GetAdditionalInfoResult GetAdditionalInfo(string orderId)
        {
            try
            {
                using(var ctx = new OnlineOrdersContainer())
                {
                    var order = ctx.OrderHeader.First(t => t.OrderId == orderId);
                    return new GetAdditionalInfoResult
                    {
                        BasketId = order.BasketId,
                        OrderSource = order.OrderSource,
                        Comment = order.Comment,
                        IsSuccesfull = true,
                        ExpirationDatePostponed = order.ExpirationDatePostponed,
                        ExpirationDate = order.ExpirationDate.HasValue
                            ? TimeZoneInfo.ConvertTime(order.ExpirationDate.Value.DateTime,
                                TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"))
                            : (DateTime?)null
                    };
                }
            }
            catch(Exception e)
            {
                return new GetAdditionalInfoResult { IsSuccesfull = false, Message = e.Message };
            }
        }

        [HttpGet]
        [ActionName("GetTask")]
        public async Task<TaskResponse> GetTask(string orderId)
        {
            using(var client = new TicketToolServiceClient())
            {
                if(!OrderUtils.CanApprove(orderId))
                {
                    return new TaskResponse { AvailableToComplete = false, TaskUrl = string.Empty };
                }
                if(ApprovedTemporary.Any(el => el == orderId))
                    return new TaskResponse { AvailableToComplete = false, TaskUrl = string.Empty };

                var task = OrderUtils.GetTasksFromRequest(orderId).OrderByDescending(el => el.Created)
                    .First(el => el.Name == OrderUtils.ApproveOrderTaskName);
                var taskInfo = await client.GetTaskAsync(task.TaskId);

                return new TaskResponse { AvailableToComplete = true, TaskUrl = taskInfo.WorkitemPageUrl };
            }
        }

        [HttpGet]
        [ActionName("GetTaskForFix")]
        public async Task<TaskResponse> GetTaskForFix(string orderId)
        {
            using(var client = new TicketToolServiceClient())
            {
                if(!OrderUtils.CanFix(orderId))
                {
                    return new TaskResponse { AvailableToComplete = false, TaskUrl = string.Empty };
                }
                if(ApprovedTemporary.Any(el => el == orderId))
                    return new TaskResponse { AvailableToComplete = false, TaskUrl = string.Empty };

                var task = OrderUtils.GetTasksFromRequest(orderId).OrderByDescending(el => el.Created)
                    .First(el => el.Name == OrderUtils.FixOrderTaskName);
                var taskInfo = await client.GetTaskAsync(task.TaskId);

                return new TaskResponse { AvailableToComplete = true, TaskUrl = taskInfo.WorkitemPageUrl };
            }
        }

        [HttpPost]
        [ActionName("ApproveOrder")]
        public async Task<OperationResult> ApproveOrder([FromBody] ApproveOrderRequest request)
        {
            using(var client = new OrderServiceClient())
            using(var ticketToolClient = new TicketToolServiceClient())
            using(var auth = new AuthenticationServiceClient())
            {
                try
                {
                    var tasks = await ticketToolClient.GetTasksByWorkitemIdAsync(request.OrderId);

                    var task = tasks.FirstOrDefault(el => el.Name == OrderUtils.ApproveOrderTaskName);
                    if(task == null)
                        return new OperationResult { IsSuccesfull = false, Message = "Заказ пока нельзя подтвердить" };

                    if(tasks.Any(el => el.Name != OrderUtils.ApproveOrderTaskName))
                        return new OperationResult
                        {
                            IsSuccesfull = false,
                            Message = "Заказ уже подтвержден"
                        };
                    if(tasks.Any(el => el.Outcome == "Подтверждено"))
                        return new OperationResult
                        {
                            IsSuccesfull = false,
                            Message = "Заказ уже подтвержден"
                        };
                    if(tasks.Any(el => el.Outcome == "Отклонено покупателем"))
                        return new OperationResult
                        {
                            IsSuccesfull = false,
                            Message = "Заказ отменен"
                        };
                    if(ApprovedTemporary.Contains(request.OrderId))
                        return new OperationResult
                        {
                            IsSuccesfull = true,
                            Message = "Заказ уже подтвержден"
                        };

                    var user = await auth.GetCurrentUserAsync();
                    var result =
                        await
                            client.ApproveOrderAsync(request.OrderId, user.LoginName);

                    ApprovedTemporary.Add(request.OrderId);
                    return new OperationResult
                    {
                        IsSuccesfull = result.ReturnCode == ReturnCode.Ok,
                        Message = result.ErrorMessage
                    };
                }
                catch(Exception e)
                {
                    return new OperationResult
                    {
                        IsSuccesfull = false,
                        Message = e.Message
                    };
                }
            }
        }

        [HttpPost]
        [ActionName("FixOrder")]
        public async Task<OperationResult> FixOrder([FromBody] FixOrderRequest request)
        {
            using(var client = new OrderServiceClient())
            using(var ticketToolClient = new TicketToolServiceClient())
            using(var auth = new AuthenticationServiceClient())
            {
                try
                {
                    var tasks = await ticketToolClient.GetTasksByWorkitemIdAsync(request.OrderId);

                    var task =
                        tasks.OrderByDescending(el => el.Created)
                            .FirstOrDefault(el => el.Name == OrderUtils.FixOrderTaskName);
                    if(task == null)
                        return new OperationResult { IsSuccesfull = false, Message = "Заказ пока нельзя подтвердить" };

                    if(tasks.OrderByDescending(el => el.Created).First().TaskId != task.TaskId)
                        return new OperationResult
                        {
                            IsSuccesfull = false,
                            Message = "Заказ уже исправлен"
                        };
                    if(tasks.Any(el => el.Outcome == "Отклонено покупателем" || el.Outcome == "Выполнение невозможно"))
                        return new OperationResult
                        {
                            IsSuccesfull = false,
                            Message = "Заказ отменен"
                        };
                    if(ApprovedTemporary.Contains(request.OrderId))
                        return new OperationResult
                        {
                            IsSuccesfull = true,
                            Message = "Заказ уже исправлен"
                        };

                    var user = await auth.GetCurrentUserAsync();
                    var result =
                        await
                            client.FixOrderAsync(request.OrderId, user.LoginName);

                    ApprovedTemporary.Add(request.OrderId);
                    return new OperationResult
                    {
                        IsSuccesfull = result.ReturnCode == ReturnCode.Ok,
                        Message = result.ErrorMessage
                    };
                }
                catch(Exception e)
                {
                    return new OperationResult
                    {
                        IsSuccesfull = false,
                        Message = e.Message
                    };
                }
            }
        }

        [HttpPost]
        [ActionName("AddComment")]
        public async Task<OperationResult> AddComment([FromBody]ChangeCommentRequest changeCommentRequest)
        {
            try
            {
                using (var client = new Proxy.Online.OrderServiceClient())
                {
                    var order = await client.GerOrderDataByOrderIdAsync(changeCommentRequest.OrderId);
                        //TODO Move comment logic inside orders.backend
                    var oldComment = order.OrderInfo.Comment;


                    var login = HttpContext.Current.User.Identity != null
                        ? HttpContext.Current.User.Identity.Name
                        : null;
                    var result = await client.UpdateOrderAsync(new UpdateOrderData
                    {
                        OrderId = changeCommentRequest.OrderId,
                        Comment = CommentHelper.FormatComment(changeCommentRequest.Comment, oldComment, login)
                    }, null);

                    if (result.ReturnCode == Proxy.Online.ReturnCode.Ok)
                        return new OperationResult {IsSuccesfull = true};
                    return ErrorResult(result.ErrorMessage);
                }
            }
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [ActionName("AssignForMe")]
        public async Task<OperationResult> AssignForMe([FromBody]AssignForMeRequest request)
        {
            using(var client = new TicketToolServiceClient())
            {
                try
                {
                    var currentTask = client.GetTasksByWorkitemId(request.OrderId)
                    .OrderByDescending(t => t.Created)
                    .FirstOrDefault();

                    if(currentTask == null /*&& currentTask.State == TaskState.Created*/)
                    {
                        return ErrorResult("Заказ не может быть взят в работу.");
                    }

                    var loggedOnSkillSets = client.Ping().LoggedOnSkillSets;
                    if(loggedOnSkillSets == null ||
                        !loggedOnSkillSets.Contains(currentTask.SkillSet.NameWithoutCluster))
                    {
                        if(loggedOnSkillSets == null || !loggedOnSkillSets.Any())
                        {
                            await client.LogOutFromSkillSetsAsync();
                        }

                        await
                            client.LogInToSkillSetsAsync(new[] { new SkillSet { Name = currentTask.SkillSet.NameWithoutCluster } });
                    }

                    var result = await client.AssignTaskForMeAsync(currentTask.TaskId);

                    return new AssignForMeResult
                    {
                        IsSuccesfull = !result.HasError,
                        Message = result.MessageText,
                        TaskDetailsUrl = string.Format(currentTask.TaskPageUrl + "?taskId={0}", currentTask.TaskId)
                    };
                }
                catch(Exception e)
                {
                    return ErrorResult(e);
                }
            }
        }

        [HttpPost]
        [ActionName("CancelOrder")]
        public async Task<OperationResult> CancelOrder([FromBody]CancelOrderRequest request)
        {
            using(var client = new OrderServiceClient())
            using(var auth = new AuthenticationServiceClient())
            {
                try
                {
                    var user = await auth.GetCurrentUserAsync();
                    var result = await client.CancelOrderAsync(request.OrderId, request.Message, user != null ? user.LoginName : string.Empty);
                    return ResultFromResult(result);
                }
                catch(Exception e)
                {
                    return ErrorResult(e);
                }
            }
        }

        [HttpPost]
        [ActionName("ChangeReserve")]
        public async Task<OperationResult> ChangeReserve([FromBody]ChangeReservRequest changeReservRequest)
        {
            if(!ModelState.IsValid)
            {
                var errorString = ModelState.Aggregate(String.Empty, (current1, state) => state.Value.Errors.Aggregate(current1, (current, error) => current + (" " + error.ErrorMessage)));
                return new OperationResult
                {
                    IsSuccesfull = false,
                    Message = string.Format("Для заказа №{0} не изменен номер резерва {1}", changeReservRequest.OrderId, errorString)
                };
            }
            try
            {
                using(var client = new OrderServiceClient())
                {
                    var result = await client.UpdateWWSOrderNumberAsync(changeReservRequest.OrderId, changeReservRequest.WwsOrderId);

                    if(result.ReturnCode == ReturnCode.Ok)
                    {
                        return new OperationResult { IsSuccesfull = true, Message = string.Format("Для заказа №{0} изменен номер резерва на {1}", changeReservRequest.OrderId, changeReservRequest.WwsOrderId) };
                    }
                    return ErrorResult(result.ErrorMessage);
                }
            }
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [ActionName("UpdateExpirationDate")]
        public async Task<OperationResult> UpdateExpirationDate([FromBody] UpdateExpirationDateRequest request)
        {
            if(!ModelState.IsValid)
            {
                var errorString = ModelState.Aggregate(String.Empty, (current1, state) => state.Value.Errors.Aggregate(current1, (current, error) => current + (" " + error.ErrorMessage)));
                return ErrorResult(string.Format("Для заказа №{0} резерв не продлен: {1}", request.OrderId, errorString));
            }
            try
            {
                using(var ordersClient = new Proxy.Online.OrderServiceClient())
                using(var client = new OrderServiceClient())
                using(var auth = new AuthenticationServiceClient())
                {
                    var currentUser = (await auth.GetCurrentUserAsync()).LoginName;

                    var orderInfo =
                        await ordersClient.GerOrderDataByOrderIdAsync(request.OrderId);
                    if(orderInfo.OrderInfo.ExpirationDate == null ||
                        orderInfo.OrderInfo.StatusInfo.Status != InternalOrderStatus.Confirmed)
                        return
                            ErrorResult("Резерв не может быть продлен только для заказов в статусе Confirmed");

                    if(orderInfo.OrderInfo.ExpirationDatePostponed)
                    {
                        return ErrorResult("Резерв уже был продлен");
                    }

                    var useMockExpiration = ConfigurationManager.AppSettings["MockReserveExpiration"] == "true"; //Убрать в отдельный класс

                    DateTime expirationDate;
                    Proxy.Router.OperationResult result;
                    if(useMockExpiration)
                    {
                        expirationDate = orderInfo.OrderInfo.ExpirationDate.Value.AddSeconds(30);
                        result =
                            await client.UpdateExpirationDateAsync(request.OrderId, currentUser, expirationDate);
                    }
                    else
                    {
                        expirationDate = orderInfo.OrderInfo.ExpirationDate.Value.AddDays(request.DaysToExpire);
                        result =
                            await client.UpdateExpirationDateAsync(request.OrderId, currentUser, expirationDate);
                    }

                    if(result.ReturnCode == ReturnCode.Ok)
                    {
                        return new OperationResult
                        {
                            IsSuccesfull = true,
                            Message =
                                string.Format("Резерв продлен на {0} дней", request.DaysToExpire)
                        };
                    }
                    return ErrorResult(result.ErrorMessage);
                }
            }
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        public async Task<Installation[]> GetInstallationServices(string orderId)
        {
            using(var client = new InstallationServiceClient())
            {
                return await client.GetInstallationAsync(orderId);
            }
        }

        /// <summary>
        /// Results from result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        private OperationResult ResultFromResult(Proxy.Router.OperationResult result)
        {
            return new OperationResult
            {
                IsSuccesfull = result.ReturnCode == ReturnCode.Ok,
                Message = result.ErrorMessage
            };
        }

        /// <summary>
        /// Errors the result.
        /// </summary>
        /// <param name="exc">The exc.</param>
        /// <returns></returns>
        private OperationResult ErrorResult(Exception exc)
        {
            return ErrorResult(exc.Message);
        }

        /// <summary>
        /// Errors the result.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private OperationResult ErrorResult(string message)
        {
            return new OperationResult
            {
                IsSuccesfull = false,
                Message = message
            };
        }
    }
}
