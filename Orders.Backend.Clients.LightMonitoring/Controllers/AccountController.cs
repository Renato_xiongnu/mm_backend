﻿using System.Web.Mvc;

namespace Orders.Backend.Clients.LightMonitoring.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string login, string password)
        {
            var authService = new Proxy.Auth.AuthenticationServiceClient();
            var authResult = authService.LogIn(login, password, true);

            switch (authResult)
            {
                case true:
                    return RedirectToAction("Index", "Home");
                case false:
                    return View("Index");
            }

            return View("Index");
        }

        public ActionResult Logout()
        {
            var authService = new Proxy.Auth.AuthenticationServiceClient();
            authService.LogOut();
            return this.RedirectToAction("Index");
        }
    }
}