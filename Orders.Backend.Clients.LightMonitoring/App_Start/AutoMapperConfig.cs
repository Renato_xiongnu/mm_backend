﻿using System;
using System.Configuration;
using AutoMapper;
using Orders.Backend.Clients.LightMonitoring.Models;
using Orders.Backend.Clients.LightMonitoring.Proxy.TicketTool;
using Orders.Backend.Clients.LightMonitoring.Utils;
using Orders.Backend.Router.Dal.Model;

namespace Orders.Backend.Clients.LightMonitoring
{
    public static class AutoMapperConfig
    {
        public static void Config()
        {
            Mapper.CreateMap<OrderMonitoring, OrderMonitoringResult>()
                .ForMember(result => result.CreateDate,
                    expression => expression.MapFrom(monitoring => monitoring.CreateDate.AsMoscowDateTime()))
                .ForMember(result => result.DeliveryDate,
                    expression => expression.MapFrom(monitoring => monitoring.DeliveryDate.AsMoscowDateTime()))
                .ForMember(result => result.CanCancel,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanCancel(monitoring)))
                .ForMember(result => result.CanAssignForMe,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanReassign(monitoring)))
                .ForMember(result => result.CanChangeComment,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanChangeComment(monitoring)))
                .ForMember(result => result.CanChangeReserve,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanChangeReserve(monitoring)))
                .ForMember(result => result.CanUpdateExpirationDate,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanUpdateExpirationDate(monitoring)))
                .ForMember(result => result.ExpirationDate,
                    expression =>
                        expression.MapFrom(
                            monitoring =>
                                monitoring.ExpirationDate.HasValue
                                    ? monitoring.ExpirationDate.Value.Date
                                    : (DateTime?) null))
                .ForMember(result => result.DateAndRequestedTimeslot, expression =>
                    expression.MapFrom(monitoring => monitoring.DeliveryDate.HasValue
                        ? string.Format("{0} {1}", monitoring.DeliveryDate.Value.ToString("dd.MM.yyyy"),
                            (string.IsNullOrEmpty(monitoring.RequestedDeliveryTimeslot)
                                ? ConfigurationManager.AppSettings["DefaultTimeslot"]
                                : monitoring.RequestedDeliveryTimeslot))
                        : monitoring.RequestedDeliveryTimeslot))
                .ForMember(result => result.CanApprove,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanApprove(monitoring.OrderId,monitoring)))
                .ForMember(result => result.CanFix,
                    expression => expression.MapFrom(monitoring => OrderUtils.CanFix(monitoring.OrderId)));

            Mapper.CreateMap<TicketTask, TicketTaskViewModel>()
                .ForMember(el => el.ReCallInfo, opt => opt.Ignore())
                .ForMember(el => el.SkillSetName, opt => opt.MapFrom(t => t.SkillSet.Name))
                .ForMember(el => el.ShowDescription, opt => opt.UseValue(true));

            Mapper.AssertConfigurationIsValid();
        }
    }
}