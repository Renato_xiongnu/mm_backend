﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Clients.LightMonitoring.Models;
using Orders.Backend.Clients.LightMonitoring.P.Commands;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Orders.Backend.Clients.LightMonitoring.Utils
{
    public static class MappingExtensions
    {
        public static string MapCommandName(this string commandName)
        {
            return ConfigurationManager.AppSettings[commandName] ?? commandName;
        }

        public static TicketTaskViewModel MapToTaskViewModel(this SenderCommand command, ReceiverCommandResponse response, IEnumerable<ReceiverCommand> receiverCommands, IEnumerable<SenderCommandResponse> senderResponses, bool showDescription)
        {
            var outcome = "Исполняется";

            JObject descriptionJson = null;
            if(showDescription)
            {
                descriptionJson = new JObject(new JProperty(command.TypeName ?? command.CommandName, JObject.Parse(command.SerializedData)));

                var externalCommandsJson = new JArray();
                receiverCommands.Where(rc => rc.ParentCommand != null && rc.ParentCommand.CorrelationId == command.CommandKey.CorrelationId).OrderBy(rc => rc.CreatedDate).Select(rc => MapToReceiverCommand(rc, senderResponses)).ToList().ForEach(externalCommandsJson.Add);                

                descriptionJson.Add("ExternalCommands", externalCommandsJson);

                if(response != null)
                {
                    var responseData = JObject.Parse(response.SerializedData);
                    var result = responseData["Result"];
                    outcome = (result == null || result.ToObject<int>() == 0) ? "Выполнена" : "Ошибка";
                    descriptionJson.Add(response.CommandName + "Response", responseData);
                }
            }

            return new TicketTaskViewModel
            {
                Name = MapCommandName(command.CommandName),
                Description = descriptionJson != null ? descriptionJson.ToString() : "",
                ShowDescription = showDescription,
                Outcome = outcome,
                Created = command.ClientCreatedDate,
                Completed = null,
                Performer = command.CreatorSystemId
            };
        }

        public static TicketTaskViewModel MapToTaskViewModel(this ReceiverCommand command, SenderCommandResponse response, bool showDescription)
        {
            var outcome = "Исполняется";

            JObject descriptionJson = null;
            if (showDescription)
            {
                descriptionJson = new JObject(new JProperty(command.TypeName ?? command.CommandName, JObject.Parse(command.SerializedData)));
                if (response != null)
                {
                    var responseData = JObject.Parse(response.SerializedData);
                    var result = responseData["Result"];
                    outcome = result != null && result.ToObject<int>() == 0 ? "Выполнена" : "Ошибка";
                    descriptionJson.Add(response.CommandName + "Response", responseData);
                }
            }

            return new TicketTaskViewModel
            {
                Name = MapCommandName(command.CommandName),
                Description = descriptionJson != null ? descriptionJson.ToString() : "",
                ShowDescription = showDescription,
                Outcome = outcome,
                Created = command.ClientCreatedDate,
                Completed = null,
                Performer = command.CreatorSystemId
            };
        }

        public static IEnumerable<JObject> MapToReceiverCommand(ReceiverCommand rc, IEnumerable<SenderCommandResponse> senderResponses)
        {
            yield return JObject.Parse(rc.SerializedData);
            var senderResponse = senderResponses.Where(sr => sr.RefCommandKey != null && sr.RefCommandKey.CorrelationId == rc.CommandKey.CorrelationId).FirstOrDefault();
            if (senderResponse != null)
            {
                yield return JObject.Parse(senderResponse.SerializedData);
            }
        }

    }
}