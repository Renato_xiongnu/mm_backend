﻿using System;

namespace Orders.Backend.Clients.LightMonitoring.Utils
{
    /// <summary>
    /// The date time extention
    /// </summary>
    public static class DateTimeExtention
    {
        private static readonly TimeZoneInfo RussionTimeZone;

        /// <summary>
        /// Initializes the <see cref="DateTimeExtention"/> class.
        /// </summary>
        static DateTimeExtention()
        {
            RussionTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
        }

        /// <summary>
        /// Ases the moscow date time.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime AsMoscowDateTime(this DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTime(dateTime, RussionTimeZone).AddHours(-2); //TODO
        }

        /// <summary>
        /// Ases the moscow small date time.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime AsMoscowSmallDateTime(this DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTime(dateTime, RussionTimeZone).AddHours(-3); //TODO
        }


        /// <summary>
        /// Ases the moscow date time.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime? AsMoscowDateTime(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                return TimeZoneInfo.ConvertTime(dateTime.Value, RussionTimeZone).AddHours(-2);
            }
            return null;
        }
    }
}