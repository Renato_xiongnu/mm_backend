﻿using System;
using System.Linq;
using System.Web;
using Order.Backend.Dal.Common;
using Orders.Backend.Clients.LightMonitoring.Properties;
using Orders.Backend.Clients.LightMonitoring.Proxy.TicketTool;
using Orders.Backend.Dal.Properties;
using Orders.Backend.Router.Dal.Model;

namespace Orders.Backend.Clients.LightMonitoring.Utils
{
    /// <summary>
    /// The order utils
    /// </summary>
    public static class OrderUtils
    {
        private static readonly TicketToolServiceClient TicketToolClient;

        public static readonly string ApproveOrderTaskName="Подтвердите заказ";

        public static readonly string FixOrderTaskName = "Внеси изменения в заказ";

        /// <summary>
        /// Initializes the <see cref="OrderUtils"/> class.
        /// </summary>
        static OrderUtils()
        {
            TicketToolClient = new TicketToolServiceClient();
        }

        /// <summary>
        /// Determines whether this instance can reassign the specified order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public static bool CanReassign(OrderMonitoring order)
        {
            var isAllowChangeComment = EnumUtils.AllowAssingTask(order.OrderStatusEnum);
            var isProcessed = order.IsProcessOrder;

            if (!isProcessed || !isAllowChangeComment) return false;
            var ticketTasks = GetTasksFromRequest(order.OrderId);
            var lastTask = ticketTasks.OrderByDescending(task => task.Created).FirstOrDefault();
            return lastTask != null && lastTask.Name != ApproveOrderTaskName && lastTask.Name != FixOrderTaskName &&
                   lastTask.SkillSet.ParamType == SkillSetParamType.None
                   &&
                   string.IsNullOrEmpty(lastTask.AssignedTo) && lastTask.State == TaskState.InProgress;
        }

        /// <summary>
        /// Determines whether this instance [can change comment] the specified order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public static bool CanChangeComment(OrderMonitoring order)
        {
            return true;
        }

        public static bool CanChangeReserve(OrderMonitoring order)
        {
            return EnumUtils.AllowUpdateWWSOrderNumber(order.OrderStatusEnum) &&
                   order.PaymentTypeEnum != OrderPaymentType.Online &&
                   !Configuration.Default.StoreFulfillmentSupported.Contains(order.SapCode);
        }

        public static bool CanUpdateExpirationDate(OrderMonitoring order)
        {
            return order.OrderStatus == (short) InternalOrderStatus.Confirmed &&
                   order.Delivery == DeliveryTypeEnum.SelfDelivery &&
                   order.SapCode != Configuration.Default.LipsSapCode &&
                   order.SapCode != Configuration.Default.VeteransSaleLocation &&
                   (!order.ExpirationDatePostponed.HasValue || !order.ExpirationDatePostponed.Value); //TODO
        }

        public static bool CanApprove(string orderId, OrderMonitoring order=null)
        {
            if (order != null)
            {
                if (order.SapCode == Configuration.Default.VeteransSaleLocation) return false;
                if (order.OrderStatusEnum == InternalOrderStatus.Rejected ||
                    order.OrderStatusEnum == InternalOrderStatus.RejectedByCustomer ||
                    order.PaymentTypeEnum == OrderPaymentType.SocialCard)
                {
                    return false;
                }

                if (!string.IsNullOrEmpty(order.WWSOrderId)) //Резерв создан - подтвержать заказ нет смысла
                {
                    return false;
                }
            }

            var tasks = GetTasksFromRequest(orderId);

            var task = tasks.FirstOrDefault(el => el.Name == ApproveOrderTaskName);
            if (task == null) return false;
            if (tasks.Any(el => el.Name != ApproveOrderTaskName))
                return false;
            if (
                tasks.Any(el =>
                        el.Outcome == "Подтверждено" || el.State == TaskState.Cancelled ||
                        el.Outcome == "Отклонено покупателем"))
                //Хак для проверки, что задачу мы же и отменили
                return false;

            return true;
        }

        public static bool CanFix(string orderId)
        {
            var tasks = GetTasksFromRequest(orderId);

            var task =
                tasks.OrderByDescending(el => el.Created)
                    .FirstOrDefault(el => el.Name == FixOrderTaskName);
            if (task == null) return false;

            if (tasks.OrderByDescending(el => el.Created).First().TaskId != task.TaskId)
                return false;
            if (
                tasks.Where(el => el.Name == FixOrderTaskName).Any(el =>
                        el.Outcome == "Выполнение невозможно"
                        || el.Outcome == "Отклонено покупателем"))
                //Хак для проверки, что задачу мы же и отменили
                return false;

            return true;
        }

        public static Proxy.TicketTool.TicketTask[] GetTasksFromRequest(string orderId)
        {
            var tasksForOrderObj = HttpContext.Current.Items[orderId];
            if (tasksForOrderObj != null) return (Proxy.TicketTool.TicketTask[])tasksForOrderObj;

            var tasksForOrder = TicketToolClient.GetTasksByWorkitemId(orderId);
            HttpContext.Current.Items[orderId] = tasksForOrder;

            return tasksForOrder;
        }

        public static bool CanCancel(OrderMonitoring order)
        {
            return order.IsProcessOrder &&
                   (order.OrderStatusEnum != InternalOrderStatus.Shipped &&
                   order.OrderStatusEnum != InternalOrderStatus.Rejected &&
                   order.OrderStatusEnum != InternalOrderStatus.RejectedByCustomer &&
                   order.OrderStatusEnum != InternalOrderStatus.Closed);
        }
    }
}