﻿try {
    document.domain = 'mediasaturnrussia.ru';
}
catch (e) {
}

kendo.culture("ru-RU");

kendo.data.binders.date = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        kendo.data.Binder.fn.init.call(this, element, bindings, options);
        this.dateformat = "dd.MM.yyyy HH:mm";
    },
    refresh: function () {
        var data = this.bindings["date"].get();
        if (data) {
            var dateObj = new Date(data);
            $(this.element).text(kendo.toString(dateObj, this.dateformat));
        }
    }
});
kendo.data.binders.dateOnly = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        kendo.data.Binder.fn.init.call(this, element, bindings, options);
        this.dateformat = "dd.MM.yyyy";
    },
    refresh: function () {
        var data = this.bindings["dateOnly"].get();
        if (data) {
            var dateObj = new Date(data);
            $(this.element).text(kendo.toString(dateObj, this.dateformat));
        }
    }
});
var prevSelectedRow = null;
function selectRowOnButtonClick(order) {
    try {
        if (prevSelectedRow != null) {
            prevSelectedRow.css("background", "");
        }

        var $row = $(order.target).closest('tr');
        prevSelectedRow = $row;
        $row.css("background", "skyblue");
    } catch (e) {
    }
};

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


$(document).ready(function () {
    var viewModel = kendo.observable({
        orderId: "",
        wwsOrderId: "",
        name: "",
        phone: "",
        email: "",
        search: function (e) {
            if (e) {
                e.preventDefault();
            }
            //костыль, но влом заморачиваться
            var phone = this.get('phone');
            if (phone === '+7 (___) ___ __ __')
                phone = '';
            $.getJSON(FastMonitoring.FindByParameters, {
                orderId: this.get("orderId"),
                wwsOrderId: this.get("wwsOrderId"),
                name: this.get("name"),
                phone: phone,//this.get("phone"),
                email: this.get("email")
            })
                .done(function (data) {
                    $('#details').hide();
                    if (data !== null) {
                        var resultViewModel = kendo.observable({
                            showDetails: function (ord) {
                                selectRowOnButtonClick(ord);
                                showTicktTaskDetailsDetails(ord.data.OrderId);
                            },
                            cancelOrder: function (ord) {
                                selectRowOnButtonClick(ord);
                                showCancelationForm(ord.data.OrderId);
                            },
                            approveOrder: function (ord) {
                                selectRowOnButtonClick(ord);
                                showApproveForm(ord.data.OrderId);
                            },
                            fixOrder: function (ord) {
                                selectRowOnButtonClick(ord);
                                showFixForm(ord.data.OrderId);
                            },
                            changeComment: function (orderItem) {
                                selectRowOnButtonClick(orderItem);
                                showChangeCommentForm(orderItem.data);
                            },
                            assignForMe: function (orderItem) {
                                selectRowOnButtonClick(orderItem);
                                showAssignForMeForm(orderItem.data);
                            },
                            changeReserve: function (orderItem) {
                                selectRowOnButtonClick(orderItem);
                                changeWwsReserve(orderItem.data);
                            },
                            updateExpirationDate: function (orderItem) {
                                selectRowOnButtonClick(orderItem);
                                updateOrderExpirationDate(orderItem.data);
                            },
                            orders: data.Result,
                            message: data.Message
                        });

                        kendo.bind($("#result"), resultViewModel);
                    } else {
                        appendResult('Заказов по таким параметрам не найдено');
                    }

                })
                .fail(function () {
                    appendError('Что-то пошло не так. Попробуйте еще раз или обратитесь к разработчикам');
                })
                .always(function () {
                    $('#submit-search').removeAttr('disabled');
                });

            $('#submit-search').attr('disabled', 'disabled');
        },
        clear: function () {
            var resultViewModel = kendo.observable({
                showDetails: function (ord) { },
                cancelOrder: function (ord) { },
                changeComment: function (ord) { },
                assignForMe: function (ord) { },
                orders: [],
                message: ""
            });

            kendo.bind($("#result"), resultViewModel);
        }
    });

    kendo.bind($("#search"), viewModel);

    function appendResult(text) {
        $('#result').text(text);
    }

    function appendError(text) {
        $('#errors').text(text);
    }

    function showAssignForMeForm(orderViewModel) {
        var windowContent = $('#assignForMeWindow');
        windowContent.show();

        var confirmWindow = windowContent.kendoWindow({
            actions: ["Close"],
            draggable: true,
            height: "100px",
            modal: true,
            resizable: false,
            title: "Взять в работу",
            width: "500px",
            visible: false
        }).data("kendoWindow");

        var assignForMeViewModel = {
            orderId: orderViewModel.OrderId,
            save: function () {
                $.post(FastMonitoring.AssignForMe, { OrderId: this.orderId }
                ).done(function (res) {
                    if (res.IsSuccesfull) {
                        var windowHandle = window.open(res.TaskDetailsUrl, '_blank', 'resizable,scrollbars');
                        $(windowHandle).on("beforeunload", function () {
                            viewModel.search();
                        });
                    } else {
                        alert(res.Message ? res.Message : 'Не удалось взять в работу');
                    }
                    viewModel.search();
                }).fail(function (req, status, message) {
                    alert('Не удалось взять в работу.\n' + message);
                }).always(function () {
                    confirmWindow.close();
                });
            }
        }

        kendo.bind(windowContent, assignForMeViewModel);
        confirmWindow.center().open();
        //$('#cancelWindow').data("kendoWindow").center().open();
    };

    function showChangeCommentForm(orderViewModel) {
        var windowContent = $('#changeCommentWindow');
        windowContent.show();

        var window = windowContent.kendoWindow({
            actions: ["Close"],
            draggable: true,
            height: "100px",
            modal: true,
            resizable: false,
            title: "Добавить комментарий",
            width: "500px",
            visible: false
        }).data("kendoWindow");

        var changneCommentViewModel = {
            orderId: orderViewModel.OrderId,
            comment: orderViewModel.Comment,
            save: function () {
                $.post(FastMonitoring.AddComment, { OrderId: this.orderId, Comment: this.comment }
                ).done(function (res) {
                    if (res.IsSuccesfull) {
                        viewModel.search();
                        alert('Комментарий добавлен');

                    } else {
                        alert(res.Message);
                    }
                }).fail(function () {
                    alert('Не удалось добавить коментарий');
                }).always(function () {
                    window.close();
                });
            }
        }

        kendo.bind(windowContent, changneCommentViewModel);
        window.center().open();
        //$('#cancelWindow').data("kendoWindow").center().open();
    };

    function changeWwsReserve(orderViewModel) {
        var windowContent = $('#changeReserveWindow');
        windowContent.show();

        var window = windowContent.kendoWindow({
            actions: ["Close"],
            draggable: true,
            height: "100px",
            modal: true,
            resizable: false,
            title: "Заменить резерв",
            width: "200px",
            visible: false
        }).data("kendoWindow");


        var changeReserveViewModel = {
            orderId: orderViewModel.OrderId,
            wwsOrderId: orderViewModel.WwsOrderId,
            save: function () {
                $.post(FastMonitoring.ChangeReserve, { OrderId: this.orderId, WwsOrderId: this.wwsOrderId }
                ).done(function (res) {
                    if (res.IsSuccesfull) {
                        viewModel.search();
                        alert(res.Message);

                    } else {
                        alert(res.Message);
                    }
                }).fail(function () {
                    alert('Не удалось заменить резерв');
                }).always(function () {
                    window.close();
                });
            }
        }

        kendo.bind(windowContent, changeReserveViewModel);
        window.center().open();
    };

    function updateOrderExpirationDate(orderViewModel) {
        var windowContent = $('#updateOrderExpirationWindow');
        windowContent.show();

        var window = windowContent.kendoWindow({
            actions: ["Close"],
            draggable: true,
            height: "100px",
            modal: true,
            resizable: false,
            title: "Продлить резерв",
            width: "200px",
            visible: false
        }).data("kendoWindow");

        var changeReserveViewModel = kendo.observable({
            orderId: orderViewModel.OrderId,
            daysToExpire: 2,
            updating: false,
            save: function () {
                this.set("updating", true);
                $.post(FastMonitoring.UpdateExpirationDate, { OrderId: this.orderId, DaysToExpire: this.daysToExpire }
                ).done(function (res) {
                    if (res.IsSuccesfull) {
                        viewModel.search();
                        alert(res.Message);
                    } else {
                        alert(res.Message);
                    }
                }).fail(function () {
                    alert('Не удалось продлить резерв');
                }).always(function () {
                    window.close();
                });
            }
        });

        kendo.bind(windowContent, changeReserveViewModel);
        window.center().open();
    }

    function showTicktTaskDetailsDetails(orderId) {
        $('#details').hide({ duration: 0 });
        $("#details").kendoTabStrip().data("kendoTabStrip").destroy();
        $("#details").kendoTabStrip({
            select: onDetailsSelect,
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });

        var detailsTab = $("#details").kendoTabStrip().data("kendoTabStrip");
        detailsTab.select(0);
        showStatuses(orderId);
        $('#details').show({ duration: 0 });

        function onDetailsSelect(e) {
            var selectedItem = $(e.item).text().trim();
            switch (selectedItem) {
                case "Статусы":
                    {
                        showStatuses(orderId);
                        break;
                    }
                case "Задачи":
                    {
                        $.getJSON(FastMonitoring.GetTicketTasks, {
                            orderId: orderId,
                        }).done(function (data) {
                            data.forEach(function (element) {
                                if (element.ShowDescription) {
                                    //"<pre>" + syntaxHighlight(JSON.stringify(JSON.parse(element.Description), null, 4)) + "</pre>"
                                    var description = element.Escalated === null ? JsonHuman.format(JSON.parse(element.Description)) : element.Description;
                                    element.showTask = function () {
                                        $('#description').html(description);
                                        var window = $('#description').kendoWindow({
                                            width: "600px",
                                            height: "200px",
                                            title: "Описание задачи",
                                            actions: [
                                                "Pin",
                                                "Minimize",
                                                "Maximize",
                                                "Close"
                                            ],
                                        }).data("kendoWindow");
                                        window.center().open();
                                    };
                                }
                            });
                            var detailsiewModel = kendo.observable({
                                tickettasks: data
                            });
                            kendo.bind($('#tickettasks'), detailsiewModel);
                        })
                            .error(function (err) {
                                $('#errors').text(JSON.stringify(err));
                            });
                        break;
                    }
                case "История резервов":
                    {
                        $.getJSON(FastMonitoring.GetReserveHistory, {
                            orderId: orderId,
                        })
                            .done(function (data) {
                                var detailsiewModel = kendo.observable({
                                    reservehistory: data
                                });
                                kendo.bind($('#reservehistory'), detailsiewModel);
                            })
                            .error(function (err) {
                                $('#errors').text(JSON.stringify(err));

                            });
                        break;
                    }
                case "Строки резерва":
                    {
                        $.getJSON(FastMonitoring.GetReserveLines, {
                            orderId: orderId,
                        })
                            .done(function (data) {
                                var total = 0;
                                for (var i = data.length; i--;) {
                                    total += data[i].Price * data[i].Qty;
                                };
                                var detailsiewModel = kendo.observable({
                                    reservelines: data,
                                    totalPrice: total
                                });
                                kendo.bind($('#reservelines'), detailsiewModel);
                            })
                            .error(function (err) {
                                $('#errors').text(JSON.stringify(err));

                            });
                        break;
                    }
                case "Строки заказа":
                    {
                        $.getJSON(FastMonitoring.GetOrderLines, {
                            orderId: orderId,
                        })
                            .done(function (data) {
                                var detailsiewModel = kendo.observable({
                                    orderlines: data
                                });
                                kendo.bind($('#orderlines'), detailsiewModel);
                            })
                            .error(function (err) {
                                $('#errors').text(JSON.stringify(err));

                            });
                        break;
                    }
                case "Исходы по магазинам":
                    {
                        $.getJSON(FastMonitoring.GetStockLocationOutcomes, {
                            orderId: orderId,
                        })
                            .done(function (data) {
                                var detailsiewModel = kendo.observable({
                                    stockresult: data.Result
                                });
                                kendo.bind($('#stockresult'), detailsiewModel);
                            })
                            .error(function (err) {
                                $('#errors').text(JSON.stringify(err));

                            });
                        break;
                    }
                case "Дополнительная информация":
                    {
                        $.getJSON(FastMonitoring.GetAdditionalInfo, { orderId: orderId })
                            .done(function (result) {
                                if (result.IsSuccesfull) {
                                    var detailsiewModel = kendo.observable({});
                                    $.extend(detailsiewModel, result);
                                    kendo.bind($('#additional-info'), detailsiewModel);
                                }
                            })
                            .error(function (err) {
                                $('#errors').text(JSON.stringify(err));
                            });
                        break;
                    }
                case "Статусы установок":
                    {
                        $.getJSON(FastMonitoring.GetInstallationServices, {
                            orderId: orderId,
                        })
                                .done(function (data) {
                                    var detailsiewModel = kendo.observable({
                                        installationsServices: data
                                    });
                                    kendo.bind($('#installation-services'), detailsiewModel);
                                })
                                .error(function (err) {
                                    $('#errors').text(JSON.stringify(err));

                                });
                        break;
                    }
            }
        }
    }

    function showStatuses(orderId) {
        $.getJSON(FastMonitoring.GetOrderStatuses, {
            orderId: orderId,
        })
            .done(function (data) {
                var detailsiewModel = kendo.observable({
                    orderstatuses: data
                });
                kendo.bind($('#orderstatuses'), detailsiewModel);
            })
            .error(function (err) {
                $('#errors').text(JSON.stringify(err));
            });
    }

    function showCancelationForm(orderId) {
        $('#cancelWindow').show();
        $('#cancelWindow').kendoWindow({
            actions: ["Close"],
            draggable: true,
            height: "100px",
            modal: true,
            resizable: false,
            title: "Отмена заказа",
            width: "500px",
            visible: false,
            deactivate: function () {
                $('#cancelConfirm').unbind('click');
            }
        });

        var cancelReasons = [
            { text: "Витрина", value: "Витрина" },
            { text: "Другое", value: "Другое" },
            { text: "Купил в другом магазине (не ММ)", value: "Купил в другом магазине (не ММ)" },
            { text: "Неверная цена на сайте", value: "Неверная цена на сайте" },
            { text: "Невозможно связаться с покупателеме", value: "Невозможно связаться с покупателем" },
            { text: "Недействительный/ тестовый заказ/ заказ переоформлен", value: "Недействительный/ тестовый заказ/ заказ переоформлен" },
            { text: "Нет в наличии", value: "Нет в наличии" },
            { text: "Ошибка ВВС/ТТ", value: "Ошибка ВВС/ТТ" },
            { text: "Передумал делать покупку", value: "Передумал делать покупку" },
            { text: "Перекуп/ОПТ/Безнал", value: "Перекуп/ОПТ/Безнал" },
            { text: "Товар после ремонта/ сломан/ ожидает возврата поставщику", value: "Товар после ремонта/ сломан/ ожидает возврата поставщику" },
            { text: "Уже купил в ММ", value: "Уже купил в ММ" },
            { text: "Хотел первоначальные условия доставки/самовывоза", value: "Хотел первоначальные условия доставки/самовывоза" },
            { text: "ЯМ: Изменился состав заказа", value: "ЯМ: Изменился состав заказа" }
        ];

        $('#cancelReason').kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: cancelReasons,
            index: 0
        });
        $('#cancelWindow').data("kendoWindow").center().open();

        $('#cancelConfirm').click(function () {
            $('#cancelConfirm').attr('disabled', 'diasbled');
            var cancelReason = $('#cancelReason').val();

            $.post(FastMonitoring.CancelOrder, { OrderId: orderId, Message: cancelReason }
                ).done(function (res) {
                    if (res.IsSuccesfull) {
                        alert('Заказ отменен');
                        viewModel.search();
                    } else {
                        alert(res.Message);
                    }
                }
                ).fail(function () {
                    alert('Не удалось отменить заказ');
                })
                .always(function () {
                    $('#cancelConfirm').removeAttr('disabled', 'diasbled');
                    $('#cancelWindow').hide();
                    var cancelClosedWindow = $("#cancelWindow").data("kendoWindow");
                    cancelClosedWindow.close();
                    $('#cancelConfirm').unbind('click');
                });
        });
    }

    function showApproveForm(orderId) {
        var taskPageUrl = '';
        jQuery.ajaxSetup({ async: false });
        $.getJSON(FastMonitoring.GetTask, {
                orderId: orderId,
            })
            .done(function (data) {
                taskPageUrl = data.TaskUrl;
            })
            .error(function (err) {
                $('#errors').text(JSON.stringify(err));
            });
        jQuery.ajaxSetup({ async: true });

        var approveOrderTemplate = $("#approve-order-template").html();
        var template = kendo.template(approveOrderTemplate);
        var approveOrder = template({ taskUrl: taskPageUrl });
        $('#approveWindow').show();
        $('#approveWindow').kendoWindow({
            actions: ["Close"],
            content: {
                template: approveOrder
            },
            draggable: true,
            modal: true,
            resizable: false,
            title: "Подтверждение заказа",
            width: "50%",
            visible: false,
            deactivate: function () {
                $('#approveOrderConfirm').unbind('click');
            }
        });

        $('#approveWindow').data("kendoWindow").center().open();

        $('#approveOrderConfirm').click(function () {
            $('#approveOrderConfirm').attr('disabled', 'diasbled');
            var itemsToDisable = 'input,textarea,select';
            $('iframe#order-content').contents().find(itemsToDisable).attr('disabled', 'disabled');
            $('iframe#order-content').contents().find('a').click(function (e) {
                e.preventDefault();
            });
            $.post(FastMonitoring.ApproveOrder, {
                orderId: orderId
            }).done(function (res) {
                if (res.IsSuccesfull) {
                    alert('Заказ подтвержен');
                } else {
                    alert(res.Message);
                }
            }).fail(function () {
                alert('Не удалось подтвердить заказ');
            })
                .always(function () {
                    $('#approveWindow').removeAttr('disabled', 'diasbled');
                    $('#approveWindow').hide();
                    var approvedClosedWindow = $("#approveWindow").data("kendoWindow");
                    approvedClosedWindow.close();
                    $('#approveOrderConfirm').unbind('click');
                    $('iframe#order-content').contents().find(itemsToDisable).removeAttr('attr');
                    $('iframe#order-content').contents().find('a').unbind('click');
                });
        });
    }

    function showFixForm(orderId) {
        var taskPageUrl = '';
        jQuery.ajaxSetup({ async: false });
        $.getJSON(FastMonitoring.GetTaskForFix, {
            orderId: orderId,
        })
            .done(function (data) {
                taskPageUrl = data.TaskUrl;
            })
            .error(function (err) {
                $('#errors').text(JSON.stringify(err));
            });
        jQuery.ajaxSetup({ async: true });

        var approveOrderTemplate = $("#approve-order-template").html();
        var template = kendo.template(approveOrderTemplate);
        var approveOrder = template({ taskUrl: taskPageUrl });
        $('#approveWindow').show();
        $('#approveWindow').kendoWindow({
            actions: ["Close"],
            content: {
                template: approveOrder
            },
            draggable: true,
            modal: true,
            resizable: false,
            title: "Подтверждение заказа",
            width: "50%",
            visible: false
        });

        $('#approveWindow').data("kendoWindow").center().open();

        $('#approveOrderConfirm').click(function () {
            $('#approveOrderConfirm').attr('disabled', 'diasbled');
            var itemsToDisable = 'input,textarea,select';
            $('iframe#order-content').contents().find(itemsToDisable).attr('disabled', 'disabled');
            $('iframe#order-content').contents().find('a').click(function (e) {
                e.preventDefault();
            });
            $.post(FastMonitoring.FixOrder, {
                orderId: orderId
            }).done(function (res) {
                if (res.IsSuccesfull) {
                    alert('Заказ исправлен');
                } else {
                    alert(res.Message);
                }
            }).fail(function () {
                alert('Не удалось подтвердить заказ');
            })
                .always(function () {
                    $('#approveWindow').removeAttr('disabled', 'diasbled');
                    $('#approveWindow').hide();
                    var approvedClosedWindow = $("#approveWindow").data("kendoWindow");
                    approvedClosedWindow.close();
                    $('#approveOrderConfirm').unbind('click');
                    $('iframe#order-content').contents().find(itemsToDisable).removeAttr('attr');
                    $('iframe#order-content').contents().find('a').unbind('click');
                });
        });
    }

});