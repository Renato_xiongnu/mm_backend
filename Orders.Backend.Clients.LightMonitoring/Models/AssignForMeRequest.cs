﻿namespace Orders.Backend.Clients.LightMonitoring.Models
{
    /// <summary>
    /// The assign for me request
    /// </summary>
    public class AssignForMeRequest
    {
        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public string OrderId { get; set; }
    }
}