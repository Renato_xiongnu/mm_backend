﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class OperationResult
    {
        public string Message { get; set; }

        public bool IsSuccesfull { get; set; }
    }
}