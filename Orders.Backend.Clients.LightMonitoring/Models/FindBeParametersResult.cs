﻿using System.Collections.Generic;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class FindBeParametersResult
    {
        public IEnumerable<OrderMonitoringResult> Result { get; private set; }

        public string Message { get; private set; }

        public FindBeParametersResult(IEnumerable<OrderMonitoringResult> orderMonitorings, string message)
        {
            Result = orderMonitorings;
            Message = message;
        }
    }
}