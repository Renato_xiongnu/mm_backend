﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class CancelOrderRequest
    {
        public string OrderId { get; set; }

        public string Message { get; set; }
    }
}