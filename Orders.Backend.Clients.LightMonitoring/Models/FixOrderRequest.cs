﻿namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class FixOrderRequest
    {
        public string OrderId { get; set; }
    }
}