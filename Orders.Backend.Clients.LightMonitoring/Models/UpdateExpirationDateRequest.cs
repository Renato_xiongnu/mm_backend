﻿using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class UpdateExpirationDateRequest
    {
        [Required]
        public string OrderId { get; set; }

        [Range(1,7,ErrorMessage = "Резерв можно продлиль от одного до семи дней")]
        public int DaysToExpire { get; set; }
    }
}