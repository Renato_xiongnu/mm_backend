﻿using System;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class GetAdditionalInfoResult:OperationResult
    {
        public string BasketId { get; set; }

        public string OrderSource { get; set; }

        public string Comment { get; set; }

        public bool ExpirationDatePostponed { get; set; }

        public DateTime? ExpirationDate { get; set; }
    }
}