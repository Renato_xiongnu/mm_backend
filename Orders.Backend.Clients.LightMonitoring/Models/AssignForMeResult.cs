﻿namespace Orders.Backend.Clients.LightMonitoring.Models
{
    /// <summary>
    /// The assign for me result
    /// </summary>
    public class AssignForMeResult : OperationResult
    {
        /// <summary>
        /// Gets or sets the popup URL.
        /// </summary>
        /// <value>
        /// The popup URL.
        /// </value>
        public string TaskDetailsUrl { get; set; }
    }
}