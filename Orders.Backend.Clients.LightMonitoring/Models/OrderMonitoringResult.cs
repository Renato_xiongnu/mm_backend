﻿using System;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class OrderMonitoringResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance can change comment.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can change comment; otherwise, <c>false</c>.
        /// </value>
        public bool CanChangeComment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance can assign to me.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can assign to me; otherwise, <c>false</c>.
        /// </value>
        public bool CanAssignForMe { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        /// <value>
        /// The create date.
        /// </value>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the order source.
        /// </summary>
        /// <value>
        /// The order source.
        /// </value>
        public string OrderSource { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public string OrderId { get; set; }

        /// <summary>
        /// Gets or sets the WWS order identifier.
        /// </summary>
        /// <value>
        /// The WWS order identifier.
        /// </value>
        public string WwsOrderId { get; set; }

        /// <summary>
        /// Gets or sets the sap code.
        /// </summary>
        /// <value>
        /// The sap code.
        /// </value>
        public string SapCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the customer phone.
        /// </summary>
        /// <value>
        /// The customer phone.
        /// </value>
        public string CustomerPhone { get; set; }

        /// <summary>
        /// Gets or sets the customer email.
        /// </summary>
        /// <value>
        /// The customer email.
        /// </value>
        public string CustomerEmail { get; set; }

        /// <summary>
        /// Gets or sets the delivery description.
        /// </summary>
        /// <value>
        /// The delivery description.
        /// </value>
        public string DeliveryDescription { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the payment type description.
        /// </summary>
        /// <value>
        /// The payment type description.
        /// </value>
        public string PaymentTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the order status description.
        /// </summary>
        /// <value>
        /// The order status description.
        /// </value>
        public string OrderStatusDescription { get; set; }

        /// <summary>
        /// Gets or sets the name of the task.
        /// </summary>
        /// <value>
        /// The name of the task.
        /// </value>
        public string TaskName { get; set; }

        public string PickupAddress { get; set; }

        public bool CanChangeReserve { get; set; }

        public bool CanUpdateExpirationDate { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string DateAndRequestedTimeslot { get; set; }

        public bool CanCancel { get; set; }

        public bool CanApprove { get; set; }

        public bool CanFix { get; set; }
    }
}