﻿using System;
using System.Runtime.Serialization;
using Orders.Backend.Clients.LightMonitoring.Proxy.TicketTool;

namespace Orders.Backend.Clients.LightMonitoring.Models
{

    [DataContract]
    public class TicketTaskViewModel 
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool ShowDescription { get; set; }

        [DataMember]
        public string Outcome { get; set; }
        
        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime? Completed { get; set; }

        [DataMember]
        public bool? Escalated { get; set; }
        
        [DataMember]
        public string AssignedTo { get; set; }

        [DataMember]
        public string Performer { get; set; }
        
        public PerformerResponse Response { get; set; }
        
        [DataMember]
        public string SkillSetName { get; set; }

        [DataMember]
        public string ReCallInfo { get; set; }

    }
}