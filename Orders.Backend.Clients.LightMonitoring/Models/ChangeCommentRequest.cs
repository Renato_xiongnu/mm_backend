﻿using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class ChangeCommentRequest
    {
        [Required]
        public string OrderId { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}