﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class ApproveOrderRequest
    {
        public string OrderId { get; set; }
    }
}