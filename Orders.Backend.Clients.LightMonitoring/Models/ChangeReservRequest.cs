﻿using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class ChangeReservRequest
    {
        [Required]
        public string OrderId { get; set; }

        [Required]
        public string WwsOrderId { get; set; }
      
    }
}