﻿namespace Orders.Backend.Clients.LightMonitoring.Models
{
    public class TaskResponse
    {
        public string TaskUrl { get; set; }

        public bool AvailableToComplete { get; set; }
    }
}