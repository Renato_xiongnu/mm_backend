﻿using System.Diagnostics.Contracts;
using Orders.Backend.OrdersToCloud.Entities;

namespace Orders.Backend.OrdersToCloud
{
    [ContractClass(typeof(CodeContracts.CustomerOrderTransformer))]
    public interface ICustomerOrderTransformer
    {
        CustomerOrder Transform(TicketToolOrder ticketToolOrder);
    }
}