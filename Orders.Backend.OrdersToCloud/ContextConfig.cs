﻿using System;
using System.Xml.Serialization;

namespace Orders.Backend.OrdersToCloud
{

    [Serializable]
    public class ContextConfig
    {
        [XmlElement]
        public string ConnectionStringName { get; set; }

        [XmlAttribute("type")]
        public string ContextType { get; set; }
    }
}
