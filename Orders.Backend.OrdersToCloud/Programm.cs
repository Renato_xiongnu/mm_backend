﻿using System;
using System.ServiceProcess;
using MMS.Shared;

namespace Orders.Backend.OrdersToCloud
{
    class Program
    {
        static void Main()
        {
            var pushService = new PollingService();

            if (Environment.UserInteractive)
            {
                DebugHelper.RunServiceAsWinForm(pushService);
            }
            else
            {
                var servicesToRun = new ServiceBase[] {pushService};
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
