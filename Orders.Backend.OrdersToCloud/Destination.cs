using System;
using System.Linq;
using MMS.Cloud.Services.Clients;
using MMS.Cloud.Shared.Exceptions;
using MMS.Integration.Polling.Core.Configuration;
using MMS.Integration.Polling.Core.Destination;
using Orders.Backend.OrdersToCloud.Entities;

namespace Orders.Backend.OrdersToCloud
{
    public class Destination : Destination<CustomerOrder, string, CloudDestinationConfiguration>
    {
        public Destination(CloudDestinationConfiguration destinationConfiguration) : base(destinationConfiguration)
        {
        }

        public override string GetLastTimeStamp(string sourceName)
        {
            var client = new DataSyncServiceClient(new Uri(Configuration.DataSyncServiceUri))
            {
                DataSyncCustomServiceName = Configuration.DataSyncCustomServiceName,
                DataSyncServiceUserName = Configuration.DataSyncServiceUserName,
                DataSyncServiceUserPassword = Configuration.DataSyncServiceUserPassword,
            };
            string timeStamp;
            DataSyncException exception;
            client.TryGetTimestamp<MMS.Storage.DataModel.Orders.CustomerOrder>(out timeStamp, out exception);
            if (exception != null)
                throw exception;

            return timeStamp;
        }

        public override PutChangesResponse<CustomerOrder, string> PutChanges(PutChangesRequest<CustomerOrder, string> putChangesRequest)
        {
            var client = new DataSyncServiceClient(new Uri(Configuration.DataSyncServiceUri))
            {
                DataSyncCustomServiceName = Configuration.DataSyncCustomServiceName,
                DataSyncServiceUserName = Configuration.DataSyncServiceUserName,
                DataSyncServiceUserPassword = Configuration.DataSyncServiceUserPassword,
            };
            var array = putChangesRequest.Data.Entities.Select(e => e.WrappedModel).ToArray();
            DataSyncException exp;
            if (array.Any())
            {
                client.TryPutChanges(putChangesRequest.NewTimeStamp, array, out exp);
                if (exp != null)
                {
                    throw exp;
                }
            }
            return new PutChangesResponse<CustomerOrder, string>();
        }
    }
}