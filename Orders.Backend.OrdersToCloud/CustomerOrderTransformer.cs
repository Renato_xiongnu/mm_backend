﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Common.Helpers;
using MMS.Storage.DataModel;
using MMS.Storage.DataModel.Orders;
using Order.Backend.Dal.Common;
using Orders.Backend.OrdersToCloud.Entities;
using Customer = MMS.Storage.DataModel.Orders.Customer;
using OrderLine = MMS.Storage.DataModel.Orders.OrderLine;


namespace Orders.Backend.OrdersToCloud
{
    public class CustomerOrderTransformer : ICustomerOrderTransformer
    {
        public Entities.CustomerOrder Transform(TicketToolOrder ticketToolOrder)
        {
            return new Entities.CustomerOrder(new MMS.Storage.DataModel.Orders.CustomerOrder
            {
                Customer = new Customer
                {
                    Email = ticketToolOrder.Customer.Email,
                    FirstName = ticketToolOrder.Customer.Name,
                    Surname = ticketToolOrder.Customer.Surname,
                    Phone = ticketToolOrder.Customer.Phone,
                },
                Delivery = ticketToolOrder.Delivery != null
                    ? new DeliveryInfo
                    {
                        City = ticketToolOrder.Delivery.City,
                        Address = ticketToolOrder.Delivery.Address,
                        AddressComment = ticketToolOrder.Delivery.DeliveryComment,
                        ActualDeliveryFrom = ticketToolOrder.Delivery.DeliveryDate,
                        ActualDeliveryTo = ticketToolOrder.Delivery.DeliveryDate,
                        DeliveryType = ticketToolOrder.Delivery.HasDelivery ? DeliveryType.Delivery : DeliveryType.Pickup,
                        Kladr = ticketToolOrder.Delivery.Kladr,
                        Floor = ticketToolOrder.Delivery.Floor.HasValue? ticketToolOrder.Delivery.Floor.ToString():null,
                        RequestedDeliveryServiceOption = ticketToolOrder.Delivery.RequestedDeliveryServiceOption,
                        PickupLocationId = ticketToolOrder.Delivery.PickupLocationId,
                    }
                    : null,
                OrderId = ticketToolOrder.OrderId,
                OrderInfo = new OrderInfo
                {
                    CreatedBy = ticketToolOrder.CreatedBy,
                    CreateDate = ticketToolOrder.CreateDate,
                    Comment = ticketToolOrder.Comment,
                    WWSOrderId = ticketToolOrder.WWSOrderId,
                    LastUpdateDate = ticketToolOrder.UpdateDate,
                    OrderState = ticketToolOrder.OrderStatus.GetDescription(),
                    OrderSourceSystem = ticketToolOrder.OrderSource,
                    OrderSource = ticketToolOrder.OrderSource,
                    FinalPaymentType = ticketToolOrder.FinalPaymentType.GetDescription(),
                    PaymentType = ticketToolOrder.PaymentType.GetDescription(),
                    DiscountCardNo = ticketToolOrder.GiftCertificateId,
                    IsPreorder = ticketToolOrder.IsPreorder,
                    LastChangingSystem = "Media Markt",
                    UtmSource =  ticketToolOrder.UtmSource,
                    OrderStatusHistories = ticketToolOrder.OrderStatusHistory.Select(osh => new OrderStatusHistory
                    {
                        Comment = osh.ReasonText,
                        FromStatus = osh.FromStatus.GetDescription(),
                        ToStatus = osh.ToStatus.GetDescription(),
                        UpdateBy = osh.WhoChanged,
                        UpdateDate = osh.UpdateDate,
                    }).ToList(),
                    CouponCampaignId = ticketToolOrder.CouponCampaignId,
                    CouponBenefit = ticketToolOrder.CouponBenefit,
                    Downpayment = ticketToolOrder.Downpayment,
                    CODAmount= ticketToolOrder.OrderLines.Sum(el => el.Qty * el.Price) - ticketToolOrder.Downpayment,
                    TotalAmount = ticketToolOrder.OrderLines.Sum(el => el.Qty * el.Price)
                },

                OrderLines = MapOrderLines(ticketToolOrder.OrderLines),
                SaleLocation = ticketToolOrder.Store != null
                    ? new SaleLocationInfo
                    {
                        Address = ticketToolOrder.Store.Address,
                        SaleLocationId = "shop_" + ticketToolOrder.Store.SapCode,
                        ChannelId = "MM",
                        SapCode = ticketToolOrder.SapCode
                    }
                    : null,
            
            }, ticketToolOrder.TimeStamp);
        }

        private List<OrderLine> MapOrderLines(IEnumerable<Entities.OrderLine> orderLines)
        {
            if (orderLines == null)
                return null;

            var result = new List<OrderLine>();
            var regex = new Regex(@"\d{7,}");

            foreach (var ol in orderLines)
            {
                int artNo;
                if (int.TryParse(ol.ArticleNum, out artNo))
                    result.Add(new OrderLine
                    {
                        ArticleNo = artNo,
                        SubOrderLines = MapOrderLines(ol.SubOrderLines),
                        Quantity = ol.Qty,
                        Price = ol.Price,
                        ArticleTitle = ol.Title,
                        ArticleProductType = ol.ArticleProductType
                    });
                else
                    if(regex.IsMatch(ol.ArticleNum))
                    {
                        var orderLine = new OrderLine
                        {
                            ArticleNo = 0,
                            Quantity = ol.Qty,
                            Price = ol.Price,
                            ArticleTitle = ol.ArticleNum,
                            SubOrderLines = new List<OrderLine>(),
                            ArticleProductType = ol.ArticleProductType
                        };
                        foreach (var match in regex.Matches(ol.ArticleNum))
                        {
                            int value;
                            if (Int32.TryParse(match.ToString(), out value))
                                orderLine.SubOrderLines.Add(new OrderLine
                                {
                                    ArticleNo = value,
                                });
                        }
                        result.Add(orderLine);
                    }
            }

            return result;
        }

      
    }
}
