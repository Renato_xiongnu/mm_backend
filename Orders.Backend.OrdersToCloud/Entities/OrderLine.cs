using System;
using System.Collections.Generic;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class OrderLine
    {
        public String OrderId { get; set; }
        public Int64 LineId { get; set; }
        public String ArticleNum { get; set; }
        public Int32 Qty { get; set; }
        public Decimal Price { get; set; }
        public Int16 LineType { get; set; }
        public String Promotions { get; set; }
        public String Title { get; set; }
        public String ArticleProductType { get; set; }

        public List<OrderLine> SubOrderLines { get; set; }
    }
}