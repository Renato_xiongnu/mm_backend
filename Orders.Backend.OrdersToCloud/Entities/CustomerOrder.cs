using MMS.Integration.Polling.Core;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class CustomerOrder : EntityWrapper<MMS.Storage.DataModel.Orders.CustomerOrder, string>
    {
        public CustomerOrder(MMS.Storage.DataModel.Orders.CustomerOrder wrapped, string ts = ""): base(wrapped, ts)
        {
        }
    }
}