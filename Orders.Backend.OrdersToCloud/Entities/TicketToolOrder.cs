using System;
using System.Collections.Generic;
using MMS.Integration.Polling.Core;
using Order.Backend.Dal.Common;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class TicketToolOrder : EntityBase<string>
    {
        public DateTime CreateDate { get; set; }
        public String OrderId { get; set; }
        public String WWSOrderId { get; set; }
        public InternalOrderStatus OrderStatus { get; set; }
        public Int64 CustomerId { get; set; }
        public DateTime UpdateDate { get; set; }
        public String GiftCertificateId { get; set; }
        public String SapCode { get; set; }
        public OrderPaymentType PaymentType { get; set; }
        public String OrderSource { get; set; }
        public Boolean IsPreorder { get; set; }
        public OrderPaymentType FinalPaymentType { get; set; }
        public String Comment { get; set; }
        public String BasketComment { get; set; }
        public String ExternalNumber { get; set; }
        public Int16 ExternalSystem { get; set; }
        public DateTimeOffset? ExpirationDate { get; set; }
        public string CreatedBy { get; set; }

        public Store Store { get; set; }

        public Customer Customer { get; set; }

        public Delivery Delivery { get; set; }

        public List<TicketToolOrderStatusHistory> OrderStatusHistory { get; set; }

        public List<OrderLine> OrderLines { get; set; }

        public string UtmSource { get; set; }

        /// <summary>
        /// id �����
        /// </summary>
        public string CouponCampaignId { get; set; }

        /// <summary>
        /// ����� ������ (� ������) �� ������
        /// </summary>
        public decimal? CouponBenefit { get; set; }

        public decimal Downpayment { get; set; }
    }
}