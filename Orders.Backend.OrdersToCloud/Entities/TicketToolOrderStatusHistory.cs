using System;
using Order.Backend.Dal.Common;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class TicketToolOrderStatusHistory
    {
        public String OrderId { get; set; }
        public InternalOrderStatus FromStatus { get; set; }
        public InternalOrderStatus ToStatus { get; set; }
        public DateTime UpdateDate { get; set; }
        public String ReasonText { get; set; }
        public Int16 ChangedType { get; set; }
        public String WhoChanged { get; set; }
    }
}