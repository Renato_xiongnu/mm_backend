using System;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class Delivery
    {
        public Int64 CustomerId { get; set; }
        public String OrderId { get; set; }
        public String City { get; set; }
        public String Address { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public String DeliveryPeriod { get; set; }
        public String DeliveryComment { get; set; }
        public String ExactAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool HasDelivery { get; set; }
        public string Kladr { get; set; }

        public Int32? Floor { get; set; } 
        public string RequestedDeliveryServiceOption { get; set; }
        public string PickupLocationId { get; set; }

    }
}