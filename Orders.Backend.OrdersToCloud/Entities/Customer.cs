using System;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class Customer
    {
        public Int64 CustomerId { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public String Phone2 { get; set; }
        public Boolean IsDefault { get; set; }
    }
}