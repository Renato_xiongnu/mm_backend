using System;

namespace Orders.Backend.OrdersToCloud.Entities
{
    public class Store
    {
        public String SapCode { get; set; }
        public String City { get; set; }
        public String Phone { get; set; }
        public String Address { get; set; }
        public String ChainType { get; set; }
        public Decimal CertificateAmount { get; set; }
        public DateTime? ReserveLifeTime { get; set; }
        public TimeSpan? StartWorkingTime { get; set; }
        public TimeSpan? EndWorkingTime { get; set; }
        public String Name { get; set; }
        public int? PaidDeliveryArticleId { get; set; }
        public int? FreeDeliveryArticleId { get; set; }
        public String OrderMail { get; set; }
        public String OutOfCatalogEmail { get; set; }
        public String SpecialOfferEmail { get; set; }
    }
}