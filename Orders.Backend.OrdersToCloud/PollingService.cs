﻿using System;
using Microsoft.Practices.Unity;
using MMS.Integration.Polling.Core;
using MMS.Integration.Polling.Core.Configuration;
using MMS.Shared;
using Orders.Backend.OrdersToCloud.Entities;

namespace Orders.Backend.OrdersToCloud
{
    public class PollingService : PushService
    {
        protected override void Initialize()
        {
            ServiceName = "Push Service - CustomerOrders from TT MM To Cloud";

            var configs =
                Config<CustomerOrderSourceConfiguration, CloudDestinationConfiguration, TimerPollingConfiguration>
                    .Pollings;

            foreach (var config in configs)
            {
                var name = config.Name;
                if(!PollingList.TryAdd(name, Container.Resolve<TimerPolling<string, Entities.TicketToolOrder, CustomerOrder, CustomerOrderSourceConfiguration, CloudDestinationConfiguration>>(name, new ParameterOverride("name", name))))
                    throw new Exception("Can't add polling service");
            }
        }

        public void Test()
        {
            PollingList.Values.ForEach(p => p.StartPolling());
        }
    }
}
