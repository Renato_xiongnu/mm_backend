﻿namespace Orders.Backend.OrdersToCloud
{
    public static class DeliveryType
    {
        /// <summary>
        /// Курьерская доставка
        /// </summary>
        public const string Delivery = "DELIVERY";
        /// <summary>
        /// Самовывоз
        /// </summary>
        public const string Pickup = "PICKUP";
        /// <summary>
        /// Почта
        /// </summary>
        public const string Post = "POST";
    }
}
