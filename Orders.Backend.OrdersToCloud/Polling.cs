﻿using System.Linq;
using MMS.Integration.Polling.Core;
using MMS.Integration.Polling.Core.Configuration;
using MMS.Integration.Polling.Core.Destination;
using Orders.Backend.OrdersToCloud.Entities;

namespace Orders.Backend.OrdersToCloud
{
    public class Polling : TimerPolling<string, TicketToolOrder, CustomerOrder, CustomerOrderSourceConfiguration, CloudDestinationConfiguration>
    {
        public Polling(string name) : base(name)
        {
        }

        protected override Data<CustomerOrder, string> MapData(string sourceName, MMS.Integration.Polling.Core.Source.Data<TicketToolOrder, string> sourceData)
        {
            var transformer = new CustomerOrderTransformer();
            var customerOrders = sourceData.Entities.Select(transformer.Transform).ToList();

            return new Data<CustomerOrder, string>
            {
                Entities = customerOrders,
                SourceName = sourceName,
            };
        }
    }
}
