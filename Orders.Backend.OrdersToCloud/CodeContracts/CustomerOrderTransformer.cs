﻿using System;
using System.Diagnostics.Contracts;
using Orders.Backend.OrdersToCloud.Entities;

namespace Orders.Backend.OrdersToCloud.CodeContracts
{

    [ContractClassFor(typeof(ICustomerOrderTransformer))]
    public abstract class CustomerOrderTransformer : ICustomerOrderTransformer
    {
        public CustomerOrder Transform(TicketToolOrder ticketToolOrder)
        {
            Contract.Requires<ArgumentNullException>(ticketToolOrder != null);

            return null;
        }
    }
}
