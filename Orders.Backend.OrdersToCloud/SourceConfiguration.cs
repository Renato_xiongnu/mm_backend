using System;
using System.Xml.Serialization;

namespace Orders.Backend.OrdersToCloud
{
    [Serializable]
    [XmlRoot("SourceConfiguration")]
    public class CustomerOrderSourceConfiguration : MMS.Integration.Polling.Core.Configuration.SourceConfiguration
    {
        [XmlElement]
        public ContextConfig CustomerOrdersContextConfig { get; set; }
        [XmlElement]
        public ContextConfig RoutableOrderContextConfig { get; set; }
    }
}