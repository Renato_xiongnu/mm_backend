﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using MMS.Integration.Polling.Core.Source;
using Order.Backend.Dal.Common;
using Orders.Backend.Dal;
using Orders.Backend.OrdersToCloud.Entities;
using Customer = Orders.Backend.OrdersToCloud.Entities.Customer;
using Delivery = Orders.Backend.OrdersToCloud.Entities.Delivery;
using OrderLine = Orders.Backend.OrdersToCloud.Entities.OrderLine;
using Store = Orders.Backend.OrdersToCloud.Entities.Store;

namespace Orders.Backend.OrdersToCloud
{
    public class Source : Source<TicketToolOrder, string, CustomerOrderSourceConfiguration>
    {
        public Source(CustomerOrderSourceConfiguration sourceConfiguration) : base(sourceConfiguration)
        {
        }

        static string SerializeSequence(long? sequence)
        {
            return (sequence.HasValue ? sequence.Value : 0).ToString(CultureInfo.InvariantCulture);
        }

        static long DeserializeSequence(string sequence)
        {
            long result;
            return long.TryParse(sequence, out result) ? result : 0;
        }

       

        public override GetChangesResponse<TicketToolOrder, string> GetChanges(string lastTimeStamp)
        {
            var sequence = DeserializeSequence(lastTimeStamp); 
          
            using (var ordersContainer = new OnlineOrdersContainer())
            {

                var headers = ordersContainer.OrderHeader
                    .Include(x => x.Deliveries)
                    .Include(x => x.Customer)
                    .Include(x => x.OrderLines)
                    .Include(x => x.ReserveHeaders)
                    .Where(oh => oh.Sequence > sequence)
                    .OrderBy(oh => oh.Sequence)
                    .Take(Configuration.PageSize);
                
                var headersWhithStore = headers.Select(x => new
                {
                     header =x,
                     store = ordersContainer.Stores.FirstOrDefault(s => s.SapCode == x.SapCode)
                });

                var items = headersWhithStore.Select(oh => new
                {
                    oh.header, 
                    oh.store, 
                    date = ordersContainer.OrderHeader.Where(ohwd => ohwd.OrderId == oh.header.OrderId)
                                                      .Select(ohwd => ohwd.Created).Cast<DateTime?>().Min(),
                    reserve=oh.header.ReserveHeaders.OrderByDescending(x=>x.ReserveId).FirstOrDefault()
                }).ToArray();


                
                return new GetChangesResponse<TicketToolOrder, string>
                {
                    Data = new Data<TicketToolOrder, string>
                    {
                        Entities = items.Select(e =>
                        {
                            var delivery = e.header.Deliveries.SingleOrDefault(d => d.CustomerId == e.header.CustomerId);

                            var order = new TicketToolOrder
                            {
                                BasketComment = e.header.BasketComment,
                                Comment = e.header.Comment,
                                Customer = new Customer
                                {
                                    CustomerId = e.header.Customer.CustomerId,
                                    Email = e.header.Customer.Email,
                                    IsDefault = e.header.Customer.IsDefault,
                                    Name = e.header.Customer.Name,
                                    Phone = e.header.Customer.Phone,
                                    Phone2 = e.header.Customer.Phone2,
                                    Surname = e.header.Customer.Surname,
                                },
                                CreateDate = e.date ?? new DateTime(),
                                CustomerId = e.header.CustomerId,
                                Delivery = delivery != null
                                    ? new Delivery
                                    {
                                        Address = delivery.Address,
                                        City = delivery.City,
                                        CustomerId = delivery.CustomerId,
                                        DeliveryComment = delivery.DeliveryComment,
                                        OrderId = delivery.OrderId,
                                        Latitude = delivery.Latitude,
                                        DeliveryDate = delivery.DeliveryDate,
                                        DeliveryPeriod = delivery.DeliveryPeriod,
                                        ExactAddress = delivery.ExactAddress,
                                        Longitude = delivery.Longitude,
                                        HasDelivery = delivery.HasDelivery,
                                        Kladr = delivery.Kladr,
                                        Floor = delivery.Floor,
                                        RequestedDeliveryServiceOption = delivery.RequestedDeliveryServiceOption,
                                        PickupLocationId = delivery.PickupLocationId
                                    }
                                    : null,
                                TimeStamp = SerializeSequence(e.header.Sequence),
                                PaymentType = (OrderPaymentType) e.header.PaymentType,
                                OrderId = e.header.OrderId,
                                OrderLines = e.reserve == null
                                    ? e.header.OrderLines.Where(ol => ol.ParentOrderLine == null).Select(ol => new OrderLine
                                    {
                                        OrderId = ol.OrderId,
                                        Price = ol.Price,
                                        SubOrderLines = ol.NesterOrderLines.Select(sol => new OrderLine
                                        {
                                            OrderId = sol.OrderId,
                                            Price = sol.Price,
                                            ArticleNum = sol.ArticleNum,
                                            LineId = sol.LineId,
                                            LineType = sol.LineType,
                                            Promotions = sol.Promotions,
                                            Qty = sol.Qty,
                                            Title = sol.Title,
                                            ArticleProductType = sol.ArticleProductType==null ? ArticleProductType.Product.ToString() : ((ArticleProductType)sol.ArticleProductType).ToString()
                                        }).ToList(),
                                        ArticleNum = ol.ArticleNum,
                                        LineId = ol.LineId,
                                        LineType = ol.LineType,
                                        Promotions = ol.Promotions,
                                        Qty = ol.Qty,
                                        Title = ol.Title,
                                        ArticleProductType = ol.ArticleProductType == null ? ArticleProductType.Product.ToString() : ((ArticleProductType)ol.ArticleProductType).ToString()                                 
                                    }).ToList()
                                    : e.reserve.ReserveLine.Where(ol=>ol.ParentReserveLine==null).Select(ol => new OrderLine
                                    {
                                        OrderId = e.header.OrderId,
                                        Price = ol.Price,
                                        SubOrderLines = ol.NestedReserveLine.Select(sol => new OrderLine
                                        {
                                            OrderId = e.header.OrderId,
                                            Price = sol.Price,
                                            ArticleNum = sol.ArticleNum,
                                            LineId = sol.LineId,
                                            LineType = sol.LineType,
                                            Promotions = sol.Promotions,
                                            Qty = sol.Qty,
                                            Title = sol.Title,
                                            ArticleProductType = ((ArticleProductType)sol.ArticleProductType).ToString()
                                        }).ToList(),
                                        ArticleNum = ol.ArticleNum,
                                        LineId = ol.LineId,
                                        LineType = ol.LineType,
                                        Promotions = ol.Promotions,
                                        Qty = ol.Qty,
                                        Title = ol.Title,
                                        ArticleProductType = ((ArticleProductType)ol.ArticleProductType).ToString() 
                                    }).ToList(),
                                IsPreorder = e.header.IsPreorder,
                                WWSOrderId = e.header.WWSOrderId,
                                ExpirationDate = e.header.ExpirationDate,
                                FinalPaymentType = (OrderPaymentType) e.header.FinalPaymentType,
                                OrderSource = e.header.OrderSource,
                                UpdateDate = e.header.UpdateDate,
                                Downpayment = (OrderPaymentType)e.header.PaymentType==OrderPaymentType.SocialCard //Проблема с ветеранами - в поле Prepay хранится сколько клиенту осталось заплатить, а не наоборот. В остальных случаях должно быть ок
                                    ? e.reserve.ReserveLine.Sum(el=>el.Price*el.Qty) - e.header.Prepay
                                    : e.header.Prepay,
                                CreatedBy =
                                    e.header.OrderStatusHistories.OrderBy(sh => sh.UpdateDate)
                                        .Select(sh => sh.WhoChanged)
                                        .FirstOrDefault(),
                                OrderStatusHistory =
                                    e.header.OrderStatusHistories.Select(sh => new TicketToolOrderStatusHistory
                                    {
                                        OrderId = sh.OrderId,
                                        UpdateDate = sh.UpdateDate,
                                        ToStatus = (InternalOrderStatus) sh.ToStatus,
                                        FromStatus = (InternalOrderStatus) sh.FromStatus,
                                        ChangedType = sh.ChangedType,
                                        ReasonText = sh.ReasonText,
                                        WhoChanged = sh.WhoChanged,
                                    }).ToList(),
                                SapCode = e.header.SapCode,
                                Store = e.store != null
                                    ? new Store
                                    {
                                        Name = e.store.Name,
                                        Phone = e.store.Phone,
                                        Address = e.store.Address,
                                        City = e.store.City,
                                        SapCode = e.store.SapCode,
                                        CertificateAmount = e.store.CertificateAmount,
                                        ChainType = e.store.ChainType,
                                        EndWorkingTime = e.store.EndWorkingTime,
                                        FreeDeliveryArticleId = e.store.FreeDeliveryArticleId,
                                        OrderMail = e.store.OrderMail,
                                        OutOfCatalogEmail = e.store.OutOfCatalogEmail,
                                        PaidDeliveryArticleId = e.store.PaidDeliveryArticleId,
                                        ReserveLifeTime = e.store.ReserveLifeTime,
                                        SpecialOfferEmail = e.store.SpecialOfferEmail,
                                        StartWorkingTime = e.store.StartWorkingTime,
                                    }
                                    : null,
                                ExternalNumber = e.header.ExternalNumber,
                                ExternalSystem = e.header.ExternalSystem,
                                GiftCertificateId = e.header.GiftCertificateId,
                                OrderStatus = (InternalOrderStatus) e.header.OrderStatus,
                                UtmSource = e.header.UtmSource,
                            };
                            if (e.reserve == null)
                            {
                                var couponInfo = e.header.OrderLines.FirstOrDefault(x => x.Benefit>0);
                                if (couponInfo != null)
                                {
                                    order.CouponBenefit = couponInfo.Benefit;
                                    order.CouponCampaignId = couponInfo.CouponCompainId;
                                }
                                else
                                {
                                    order.CouponBenefit = 0;
                                    order.CouponCampaignId = "";
                                }
                            }
                            else
                            {
                                var couponInfo = e.reserve.ReserveLine.FirstOrDefault(x => x.Benefit > 0);
                                if (couponInfo != null)
                                {
                                    order.CouponBenefit = couponInfo.Benefit;
                                    order.CouponCampaignId = couponInfo.CouponCompainId;
                                }
                                else
                                {
                                    order.CouponBenefit = 0;
                                    order.CouponCampaignId = "";
                                }
                            }
                            return order;

                        }).ToList(),
                    },
                    NewTimeStamp = SerializeSequence(headersWhithStore.Any() ? items.Max(e => e.header.Sequence) : sequence),
                };
            }
        }
    }
}
