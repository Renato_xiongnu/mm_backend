﻿using System;
using System.Data.Objects;
using MMS.Integration.Polling.Core;
using MMS.Integration.Polling.Core.Configuration;
using MMS.Integration.Polling.Core.Destination;
using Orders.Backend.Dal;
using Orders.Backend.Services.PushService.Infastructure;

namespace Orders.Backend.Services.PushService.Destinations
{
    public class PickupLocationDestination :
        Destination<EntityWrapper<Dal.PickupLocation, string>, string, DestinationConfiguration>
    {
        private IStringBasedLastSequanceProvider _lastSequanceProvider;

        public PickupLocationDestination(DestinationConfiguration destinationConfiguration)
            : this(destinationConfiguration,new FileLastSequenceStringProvider("PickupLocation.txt"))
        {
        }

        protected PickupLocationDestination(DestinationConfiguration destinationConfiguration,
            IStringBasedLastSequanceProvider lastSequanceProvider)
            : base(destinationConfiguration)
        {
            _lastSequanceProvider = lastSequanceProvider;
        }

        public override string GetLastTimeStamp(string sourceName)
        {
            return _lastSequanceProvider.Get();
        }

        public override PutChangesResponse<EntityWrapper<PickupLocation, string>, string> PutChanges(
            PutChangesRequest<EntityWrapper<PickupLocation, string>, string> putChangesRequest)
        {
            using (var provider= new LightProvider())
            {
                foreach (var pickupLoctionWrapper in putChangesRequest.Data.Entities)
                {
                    var pickupLocation =
                        provider.GetPickupLocation(pickupLoctionWrapper.WrappedModel.PickupLocationId);
                    try
                    {
                        if (pickupLocation == null)
                        {
                            pickupLocation = new PickupLocation
                            {
                                PickupLocationId = pickupLoctionWrapper.WrappedModel.PickupLocationId,
                                Address = pickupLoctionWrapper.WrappedModel.Address,
                                IsActive = pickupLoctionWrapper.WrappedModel.IsActive,
                                IsDeleted = pickupLoctionWrapper.WrappedModel.IsDeleted,
                                PickupPointType = pickupLoctionWrapper.WrappedModel.PickupPointType,
                                OperatorName = pickupLoctionWrapper.WrappedModel.OperatorName,
                                OperatorPuPId = pickupLoctionWrapper.WrappedModel.OperatorPuPId,
                                Title = pickupLoctionWrapper.WrappedModel.Title,
                                City = pickupLoctionWrapper.WrappedModel.City,
                                ZipCode = pickupLoctionWrapper.WrappedModel.ZipCode

                            };
                            provider.PickupLocation.AddObject(pickupLocation);
                            provider.Save();
                        }
                        else
                        {
                            if (ShoudBeIgnored(pickupLoctionWrapper)) continue;

                            pickupLocation.Address = pickupLoctionWrapper.WrappedModel.Address;
                            pickupLocation.IsActive = pickupLoctionWrapper.WrappedModel.IsActive;
                            pickupLocation.IsDeleted = pickupLoctionWrapper.WrappedModel.IsDeleted;
                            pickupLocation.PickupPointType = pickupLoctionWrapper.WrappedModel.PickupPointType;
                            pickupLocation.OperatorName = pickupLoctionWrapper.WrappedModel.OperatorName;
                            pickupLocation.OperatorPuPId = pickupLoctionWrapper.WrappedModel.OperatorPuPId;
                            pickupLocation.Title = pickupLoctionWrapper.WrappedModel.Title;
                            pickupLocation.City = pickupLoctionWrapper.WrappedModel.City;
                            pickupLocation.ZipCode = pickupLoctionWrapper.WrappedModel.ZipCode;

                            provider.Save();
                        }

                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
            }

            _lastSequanceProvider.Set(putChangesRequest.NewTimeStamp);

            return new PutChangesResponse<EntityWrapper<PickupLocation, string>, string>();
        }

        private static bool ShoudBeIgnored(EntityWrapper<PickupLocation, string> pickupLoctionWrapper)
        {
            if (pickupLoctionWrapper.WrappedModel.PickupLocationId.ToLower().StartsWith("pickup_r") ||
                pickupLoctionWrapper.WrappedModel.PickupPointType == "shop") return true;
            return false;
        }
    }
}
