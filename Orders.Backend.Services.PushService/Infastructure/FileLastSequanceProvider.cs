using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using NLog;

namespace Orders.Backend.Services.PushService.Infastructure
{
    public class FileLastSequanceProvider : ILastSequanceProvider
    {
        private long _lastSequance = 0;

        private readonly FileInfo _fileInfo;

        private readonly ReaderWriterLockSlim _lockObject = new ReaderWriterLockSlim();

        private readonly Logger _logger = LogManager.GetLogger("FileLastSequanceProvider");

        public FileLastSequanceProvider(string filePath, bool isAbsolutePath = false)
        {            
            filePath = isAbsolutePath ? filePath : GetAbsolutePath(filePath);
            _fileInfo = new FileInfo(filePath);
            if (_fileInfo.Directory != null && !_fileInfo.Directory.Exists)
            {
                _fileInfo.Directory.Create();
            }
            if (!_fileInfo.Exists)
            {
                _fileInfo.Create();
            }
            else
            {
                _lastSequance = ReadFromFile();
            }
        }

        private string GetAbsolutePath(string filePath)
        {
            var assemblyDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            var path = string.IsNullOrEmpty(assemblyDirectory)
                ? filePath
                : Path.Combine(assemblyDirectory, filePath);
            return path;
        }

        public long Get()
        {
            _lockObject.EnterReadLock();
            try
            {
                _logger.Info("Return lastSeq {0}", _lastSequance);
                return _lastSequance;
            }
            finally
            {
                _lockObject.ExitReadLock();
            }
        }

        public void Set(long value)
        {
            _lockObject.EnterWriteLock();
            try
            {
                _logger.Info("Save lastSeq {0}", _lastSequance);
                using (var sw = _fileInfo.CreateText())
                {
                    sw.Write(value);
                }
                _lastSequance = value;
            }
            finally
            {
                _lockObject.ExitWriteLock();
            }
        }

        private long ReadFromFile()
        {
            _lockObject.EnterWriteLock();
            try
            {
                using (var sr = _fileInfo.OpenText())
                {
                    var str = sr.ReadToEnd();
                    var match = Regex.Match(str, @"(?<number>[1-9]\d*)");
                    if (match.Success)
                    {
                        return long.Parse(match.Groups["number"].Value);
                    }
                }
                return 0;
            }
            finally
            {
                _lockObject.ExitWriteLock();
            }
        }
    }
}