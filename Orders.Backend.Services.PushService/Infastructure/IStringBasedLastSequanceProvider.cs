﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.Services.PushService.Infastructure
{
    public interface IStringBasedLastSequanceProvider
    {
        string Get();
        void Set(string value);
    }
}
