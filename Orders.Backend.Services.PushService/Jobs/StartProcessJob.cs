﻿using Orders.Backend.Services.PushService.Services;
using Quartz;

namespace Orders.Backend.Services.PushService.Jobs
{    
    [DisallowConcurrentExecution]
    public class StartProcessJob : ServiceExecutionJobBase<StartProcessService>
    {        
    }
}
