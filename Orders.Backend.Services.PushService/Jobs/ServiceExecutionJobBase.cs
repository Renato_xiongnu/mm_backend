﻿using Orders.Backend.Services.PushService.Services;

namespace Orders.Backend.Services.PushService.Jobs
{
    public class ServiceExecutionJobBase<TService> : JobBase
        where TService : ServiceBase,new()
    {
        private readonly TService _service = new TService();
        
        protected sealed override void Execute()
        {
            _service.Execute();
        }
    }
}