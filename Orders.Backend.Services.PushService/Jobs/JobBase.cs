﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using NLog;
using Quartz;

namespace Orders.Backend.Services.PushService.Jobs
{
    public abstract class JobBase : IJob
    {
        protected Logger Logger;

        //protected MonitoringServiceClient MonitoringClient;

        protected NameValueCollection AppSettings;

        protected JobBase()
        {
            Logger = LogManager.GetLogger(GetType().Name);
            Logger.Trace("******  {0} instantiated ******", GetType().Name);
            try
            {
                AppSettings = ConfigurationManager.AppSettings;

                if(Configuration.NagiosEnabled)
                {
                    //MonitoringClient = new MonitoringServiceClient(Configuration.NagiosHost,
                    //    Configuration.NagiosServiceAddress);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "Startup service error");
            }
        }

        protected abstract void Execute();

        protected virtual string MonitoringServiceName()
        {
            return GetType().Name;
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Execute();
                //if(MonitoringClient != null)
                //{
                   // MonitoringClient.InfoAsync(MonitoringServiceName(), "Execute ended successfuly");
                //}
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "------  {0} failed. ------", GetType().Name);

                //if(MonitoringClient != null)
                //{
                    //MonitoringClient.ErrorAsync(MonitoringServiceName(), "Executing error: " + err.Message);
                //}

                throw new JobExecutionException(ex, false);
            }
            finally
            {
                Logger.Trace("******  {0} finished ******", GetType().Name);
            }
        }
    }
}