using System;
using NLog;

namespace Orders.Backend.Services.PushService.Services
{
    public abstract class ServiceBase
    {
        protected Logger Logger;

        protected ServiceBase()
        {
            Logger = LogManager.GetLogger(GetType().Name);
            Logger.Trace("******  {0} instantiated ******", GetType().Name);
        }

        public void Execute()
        {
            try
            {
                while (ExecuteInternal())
                {
                }
            }
            catch (Exception exception)
            {
                Logger.Error(string.Format("Execute exception: {0}", exception));
                throw;
            }
        }

        protected abstract bool ExecuteInternal();
    }
}