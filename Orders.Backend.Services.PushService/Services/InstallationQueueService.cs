﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Proxies;
using System.Text;
using System.Threading.Tasks;
using Order.Backend.Dal.Common;
using Orders.Backend.Dal;
using Orders.Backend.Dal.Entities;

namespace Orders.Backend.Services.PushService.Services
{
    public class InstallationQueueService:ServiceBase
    {
        protected override bool ExecuteInternal()
        {
            using (var context = new LightProvider())
            {
                var items = context.InstallationCreationQueue.Where(x => x.Processed == false).OrderBy(x => x.TryCount).Take(Properties.Settings.Default.MaxQueueCount).ToList();
                if (items.Any())
                {
                    foreach (var item in items)
                    {
                        var operationType = (InstallationCreationOperationType)item.OperationType;

                        item.UpdateDate = DateTime.UtcNow;
                        item.TryCount++;
                        Logger.Info(String.Format("Start process installation for order {0}, operation {1}",
                            item.OrderId, operationType));
                        var errorMessage = String.Empty;
                        switch (operationType)
                        {
                            case InstallationCreationOperationType.Create:
                            {
                                var order = context.GetOrderDataByOrderId(item.OrderId, RelatedEntity.OrderLines);
                                if (
                                    order.ReserveHeaders.First()
                                        .ReserveLine.All(
                                            el =>
                                                el.ArticleProductType !=
                                                (short) (ArticleProductType.InstallationService)))
                                {
                                    item.Processed = true;
                                    errorMessage = "Order has no installations";
                                    break;
                                }
                                using (var client = new Proxy.InstallationService.InstallationServiceClient())
                                {
                                    try
                                    {
                                        client.CreateInstallation(item.OrderId);
                                        item.Processed = true;
                                    }
                                    catch (Exception e)
                                    {
                                        errorMessage = e.ToString();
                                    }
                                }
                                break;
                            }
                            case  InstallationCreationOperationType.Cancel:
                                using (var client = new Proxy.InstallationService.InstallationServiceClient())
                                {
                                    try
                                    {
                                        client.CancelInstallation(item.OrderId);
                                        item.Processed = true;
                                    }
                                    catch (Exception e)
                                    {
                                        errorMessage = e.ToString();
                                    }
                                }
                                break;
                            default:
                                throw new NotSupportedException(operationType.ToString());
                        }
                        item.ErrorMessage = errorMessage;
                        Logger.Info(String.Format("Installation for order {0} operation {1} processed {2}",
                            item.OrderId, operationType, item.Processed));

                    }
                    context.Save();
                }
            }

            return false;
        }
    }
}
