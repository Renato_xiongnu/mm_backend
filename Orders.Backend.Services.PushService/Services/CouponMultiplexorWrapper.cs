﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Multiplexor.Client;
using MMS.Multiplexor.Client.Api;

namespace Orders.Backend.Services.PushService.Services
{
    internal class CouponMultiplexorWrapper
    {
        internal static Boolean UseCoupon(String couponCode, ref String errorMessage)
        {
            try
            {
                var multiplexorClient = new MultiplexorClient(Properties.Settings.Default.MultiplexorUrl, Properties.Settings.Default.MultiplexorApiKey, Properties.Settings.Default.MultiplexorTimeout);

                multiplexorClient.Coupon.Use(couponCode);
                multiplexorClient.Execute();
                var executionCommandResults = multiplexorClient.ExecutionCommandResults;
                var requestResult = executionCommandResults[0].Result;

                var errors = new Dictionary<string, string>();
                if (requestResult.Errors != null)
                {
                    foreach (var error in requestResult.Errors)
                    {
                        errors[error.Key] = error.Value;
                    }
                }

                if (errors.Count > 0)
                {
                    errorMessage = string.Join("\r\n", errors.Select(e => e.Value));
                }
                var result = requestResult.Result as CouponApi.CouponUseResponsTO;

                return result != null && result.Status;
            }
            catch (Exception ex)
            {
                errorMessage += ex.Message;
                return false;
            }  
        }

        internal static Boolean UnfreezCoupon(String couponCode, ref String errorMessage)
        {
            try
            {
                var multiplexorClient = new MultiplexorClient(Properties.Settings.Default.MultiplexorUrl, Properties.Settings.Default.MultiplexorApiKey, Properties.Settings.Default.MultiplexorTimeout);

                multiplexorClient.Coupon.Unfreeze(couponCode);
                multiplexorClient.Execute();
                var executionCommandResults = multiplexorClient.ExecutionCommandResults;
                var requestResult = executionCommandResults[0].Result;

                var errors = new Dictionary<string, string>();
                if (requestResult.Errors != null)
                {
                    foreach (var error in requestResult.Errors)
                    {
                        errors[error.Key] = error.Value;
                    }
                }

                if (errors.Count > 0)
                {
                    errorMessage = string.Join("\r\n", errors.Select(e => e.Value));
                }
                var result = requestResult.Result as CouponApi.CouponUnfreezeResponsTO;

                return result != null && result.Status;
            }
            catch (Exception ex)
            {
                errorMessage += ex.Message;
                return false;
            }  

           
        }
    }
}
