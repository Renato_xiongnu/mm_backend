using System;
using System.Net.Http;
using MMS.Cloud.Services.Clients;

namespace Orders.Backend.Services.PushService.Services
{
    public abstract class CouchServiceBase : ServiceBase
    {
        protected readonly DataSyncServiceClient DataSyncServiceClient;

        protected CouchServiceBase()
        {
            var serviceUri = new Uri(Configuration.DataSyncServiceUri);
            DataSyncServiceClient = new DataSyncServiceClient(serviceUri)
            {
                DataSyncCustomServiceName = Configuration.DataSyncCustomServiceName,
                DataSyncServiceUserName = Configuration.DataSyncServiceUserName,
                DataSyncServiceUserPassword = Configuration.DataSyncServiceUserPassword
            };
        }
    }
}