﻿using System;
using System.Linq;
using MMS.Integration.Polling.Core;
using MMS.Integration.Polling.Core.Configuration;
using MMS.Integration.Polling.Core.Destination;
using MMS.Proxy.Monitoring;
using MMS.Storage.DataModel.Version3.PickupLocation;
using Orders.Backend.Dal;

namespace Orders.Backend.Services.PushService.Services
{
    public class PickupLocationPollingService :
        TimerPolling<string, EntityWrapper<PickupLocationV3, string>, EntityWrapper<Dal.PickupLocation, string>,
            CloudSourceConfiguration, DestinationConfiguration>
    {
        public PickupLocationPollingService(string name) : base(name)
        {
        }

        public PickupLocationPollingService(string name, IMonitoringServiceClient monitor) : base(name, monitor)
        {
        }

        protected override Data<EntityWrapper<PickupLocation, string>, string> MapData(string sourceName,
            MMS.Integration.Polling.Core.Source.Data<EntityWrapper<PickupLocationV3, string>, string> sourceData)
        {
            var filteredEntities = sourceData.Entities.Select(el => el.WrappedModel);
            return new Data<EntityWrapper<PickupLocation, string>, string>
            {
                SourceName = sourceName,
                Entities =
                    filteredEntities.Select(
                        el => new EntityWrapper<PickupLocation, string>(MapInternal(el)))
                        .ToList()
            };
        }

        private PickupLocation MapInternal(PickupLocationV3 pickupLocation)
        {
            var isActiveInMM = pickupLocation.AvailableInChannels != null
                               &&
                               (pickupLocation.AvailableInChannels.Any(
                                   el => el.ChannelId ==
                                         "MM") &&
                                pickupLocation.AvailableInChannels.First(
                                    el => el.ChannelId ==
                                          "MM").IsActive);

            ExternalSystem operatorName = null;
            ExternalSystem operatorPuPId = null;

            if (pickupLocation.ExternalSystems != null)
            {
                operatorName = pickupLocation.ExternalSystems.FirstOrDefault(x => x.Type.Equals("OperatorName", StringComparison.InvariantCultureIgnoreCase));
                operatorPuPId = pickupLocation.ExternalSystems.FirstOrDefault(x => x.Type.Equals("OperatorPuPId", StringComparison.InvariantCultureIgnoreCase));  
            }

            return new PickupLocation
            {
                PickupLocationId = pickupLocation.PickupLocationId,
                Address = GetAddress(pickupLocation),
                IsActive = pickupLocation.IsActive.HasValue &&
                           pickupLocation.IsActive.Value && isActiveInMM,
                IsDeleted = pickupLocation.IsDeleted,
                PickupPointType = pickupLocation.Type ?? string.Empty,
                OperatorName = operatorName != null ? operatorName.Id : null,
                OperatorPuPId = operatorPuPId != null ? operatorPuPId.Id : null,
                Title = pickupLocation.Title,
                City = pickupLocation.Location != null ? pickupLocation.Location.CityName : null,
                ZipCode = pickupLocation.Location != null ? pickupLocation.Location.ZipCode : null
            };
        }

        private string GetAddress(PickupLocationV3 pickupLocation)
        {
            if (pickupLocation.Location == null) return string.Empty;
            if (!string.IsNullOrEmpty(pickupLocation.Location.Address)) return pickupLocation.Location.Address;
            if (pickupLocation.Location.AddressProperties == null) return string.Empty;
            var address = pickupLocation.Location.AddressProperties;
            return string.Format("{0}, {1} {2}, {3}{4} {5}, {6}, {7}", address.City, address.NameOfStreet, address.StreetAbbreviation, address.Property,
                address.PropertyExt, address.Building,address.Office,address.CommentOfAddress);
        }
    }
}
   
