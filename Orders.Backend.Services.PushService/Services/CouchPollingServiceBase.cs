﻿using System.Globalization;
using MMS.Cloud.Shared.Exceptions;

namespace Orders.Backend.Services.PushService.Services
{
    public abstract class CouchPollingServiceBase<TEntity> : CouchServiceBase
    {        

        private readonly int _couchListenerBatchSize;

        protected CouchPollingServiceBase()
        {
            _couchListenerBatchSize = Configuration.CouchListenerBatchSize;            
        }

        protected override bool ExecuteInternal()
        {
            var lastSequence = GetLastSequence();
            var lastSequenceStr = lastSequence.ToString(CultureInfo.InvariantCulture);
            TEntity[] entities;
            DataSyncException exception;
            DataSyncServiceClient.TryGetChanges(_couchListenerBatchSize, ref lastSequenceStr, out entities, out exception);
            if (exception != null)
            {
                Logger.Error(string.Format("GetChanges exception: {0}", exception));
                return 0 == _couchListenerBatchSize;
            }
            long newSequence;
            if (!long.TryParse(lastSequenceStr, out newSequence))
            {
                Logger.Error(string.Format("GetChanges return bad sequence: {0}", lastSequenceStr));
                return 0 == _couchListenerBatchSize;
            }
            if (entities == null)
            {
                return 0 == _couchListenerBatchSize;
            }
            foreach (var entity in entities)
            {
                var operationResult = SaveEntity(entity);
                if (!operationResult)
                {
                    Logger.Error(string.Format("SaveEntity error: {0}", entity));
                }
            }
            SaveLastSequence(newSequence);
            return entities.Length == _couchListenerBatchSize;
        }

        protected abstract bool SaveEntity(TEntity entity);

        protected abstract long SaveLastSequence(long newSequence);

        protected abstract long GetLastSequence();
    }
}