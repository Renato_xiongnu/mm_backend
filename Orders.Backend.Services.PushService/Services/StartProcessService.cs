using System;
using System.Linq;
using Common.Helpers;
using Orders.Backend.Dal;
using Orders.Backend.Router.Dal.Model;
using Orders.Backend.Router.Infastructure;
using Orders.Backend.Services.Contracts.OrderService;
using Orders.Backend.Services.Order.Common;

namespace Orders.Backend.Services.PushService.Services
{
    public class StartProcessService : ServiceBase
    {
        private readonly LogDataProvider _dataProvider = new LogDataProvider();

        protected override bool ExecuteInternal()
        {
            Logger.Trace("Start get should be processed orders");
            var shouldBeProcessedOrders = _dataProvider.GetShouldBeProcessedOrders();
            Logger.Trace("Must be processed {0} orders", shouldBeProcessedOrders.Length);
            foreach(var shouldBeProcessedOrder in shouldBeProcessedOrders)
            {
                StartProcess(shouldBeProcessedOrder);
            }
            //Parallel.ForEach(shouldBeProcessedOrders, StartProcess);
            return false;
        }

        private void StartProcess(RoutableOrder notProcessedOrder)
        {
            var orderId = notProcessedOrder.OrderId;
            var sapCode = notProcessedOrder.SapCode;
            try
            {
                using (var provider = new LightProvider())
                {
                    var order = provider.GetOrderDataByOrderId(orderId, RelatedEntity.OrderLines);
                    sapCode = order.SaleLocationSapCode.Trim();
                    var source = order.OrderSource.ToEnum<OrderSource>();
                    var paymentType = order.PaymentType.ToEnum<PaymentType>();
                    var articleDatas = order.OrderLines.Select(el => el.ToArticleData()).ToList();
                    var hasDelivery = order.ToDeliveryInfo().HasDelivery;
                    var processSelectRequest = new ProcessSelectRequest(source, paymentType, sapCode,
                        articleDatas, hasDelivery);
                    var processSelector =
                        ProcessSelectorBase.Create(SoapServiceHelper.GetSmartStartSapCodecollection().ToList(),
                            SoapServiceHelper.GetSmartStart2SapCodecollection().ToList(),
                            SoapServiceHelper.GetDiscontSapCodeCollection().ToList());
                    var processName = processSelector.GetProcessName(processSelectRequest);
                    
                    var version = SoapServiceHelper.GetProcessCurrentVersion(processName);
                    var currentProcessVersion = processName + version;

                    Logger.Info("Process starting from order {0}", orderId);
                    var address = SoapServiceHelper.GetServiceAddress(currentProcessVersion);
                    Logger.Debug("{0} process name {1} version {2} service address {3}", orderId, processName,version, address);
                    var startProcess = SoapServiceHelper.StartProcess(new SoapServiceHelper.StartRequest
                    {
                        ParameterName = order.SapCode,
                        WorkItemId = order.OrderId
                    }, address);
                    Logger.Info("Process started from order {0} TicketId = {1}", orderId, startProcess.TicketId);
                    _dataProvider.SetProcessVersion(notProcessedOrder.OrderId, currentProcessVersion);
                    _dataProvider.MarkIsProcessed(notProcessedOrder.OrderId);
                    _dataProvider.LogInfo(notProcessedOrder.OrderId, "Order put to process");
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "An error occurred while starting the process for order {0}", orderId);
                var fullExceptionText = ex.GetExceptionFullInfoText();
                _dataProvider.LogError(notProcessedOrder.OrderId, sapCode, fullExceptionText);
            }
        }
    }
}