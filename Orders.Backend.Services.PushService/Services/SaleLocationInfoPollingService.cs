﻿using System;
using System.Text.RegularExpressions;
using MMS.Storage.DataModel.Version3.SaleLocationInfo;
using Orders.Backend.Dal;
using Orders.Backend.Services.PushService.Infastructure;

namespace Orders.Backend.Services.PushService.Services
{
    public class SaleLocationInfoPollingService : CouchPollingServiceBase<SaleLocationV3>
    {

        private const string KeyParsTemplate = @"^shop_+(?<sapcode>[^_\s-].+)\s*$";

        private readonly ILastSequanceProvider _lastSequanceProvider;

        public SaleLocationInfoPollingService(ILastSequanceProvider lastSequanceProvider)
        {
            _lastSequanceProvider = lastSequanceProvider;
        }

        public SaleLocationInfoPollingService()
            : this(new FileLastSequanceProvider("SaleLocationInfoPollingServiceLastSequance.txt"))
        {
        }

        private DateTime GetMinDateTime2()
        {
            return new DateTime(1900,1,1);
        }

        protected override bool SaveEntity(SaleLocationV3 entity)
        {            
            if (!string.Equals(entity.ChannelId,"MM",StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            using(var provider = new LightProvider())
            {
                var entitySapCode = Regex.Replace(entity.SaleLocationId, KeyParsTemplate, "$1");
                var store = provider.GetStore(entitySapCode);
                if(store == null)
                {
                    Logger.Info("SaleLocation with SapCode={0} not founded", entitySapCode);
                    return true;
                }
                store.Name = string.IsNullOrEmpty(entity.Title) ? store.Name : entity.Title;
                if(entity.OrderProperties != null)
                {
                    Logger.Info("LifetimeDaysOrderReserve = {0}", entity.OrderProperties.LifetimeDaysOrderReserve);
                    store.ReserveLifeTime = entity.OrderProperties.LifetimeDaysOrderReserve.HasValue &&
                                            entity.OrderProperties.LifetimeDaysOrderReserve.Value > 0
                        ? GetMinDateTime2().AddDays(entity.OrderProperties.LifetimeDaysOrderReserve.Value)
                        : store.ReserveLifeTime;
                }
                Logger.Info(
                    "Store info changed ({0}): FreeDeliveryArticleId={1} PaidDeliveryArticleId={2} Name={3} ReserveLifeTime={4}",
                    store.SapCode, store.FreeDeliveryArticleId, store.PaidDeliveryArticleId, store.Name,
                    store.ReserveLifeTime);                               
                provider.Save();
                return true;
            }
        }

        protected override long SaveLastSequence(long newSequence)
        {
            _lastSequanceProvider.Set(newSequence);
            return GetLastSequence();
        }

        protected override long GetLastSequence()
        {
            return _lastSequanceProvider.Get();
        }
    }
}