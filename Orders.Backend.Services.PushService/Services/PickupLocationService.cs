﻿namespace Orders.Backend.Services.PushService.Services
{
    public class PickupLocationService:MMS.Integration.Polling.Core.PushService
    {
        protected override void Initialize()
        {
            ServiceName = "Cloud to Orders.Backend PickupPoint push service";

            base.Initialize();
        }
    }
}
