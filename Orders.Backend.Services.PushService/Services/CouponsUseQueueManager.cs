﻿using System;
using System.Linq;
using Orders.Backend.Dal;
using Orders.Backend.Dal.Entities;

namespace Orders.Backend.Services.PushService.Services
{
    public class CouponsUseQueueManager : ServiceBase
    {
        protected override bool ExecuteInternal()
        {
            using (var context = new LightProvider())
            {
                var items = context.CouponsUseQueue.Where(x => x.Processed == false).OrderBy(x=> x.TryCount).Take(Properties.Settings.Default.MaxQueueCount).ToList();
                if (items.Any())
                {
                    foreach (var item in items)
                    {
                        if (item.UpdateDate - item.CreateDate > Properties.Settings.Default.CouponsProcessignTime)
                        {
                            Logger.Warn("Coupon processing time more than expected {0}. CoupounCode: {1}, OrderId: {2}, TryCount: {3}. Coupon marked as processed.",
                                Properties.Settings.Default.CouponsProcessignTime,
                                item.CouponCode,
                                item.OrderId,
                                item.TryCount);
                            item.Processed = true;
                            continue;
                        }
                        var operationType = (CouponsUseQueueOperationType) item.OperationType;

                        item.UpdateDate = DateTime.UtcNow;
                        item.TryCount++;
                        Logger.Info("Start process coupon {0} for order {1}, operation {2}", item.CouponCode, item.OrderId, operationType);
                        var errorMessage = String.Empty;
                        switch (operationType)
                        {
                            case CouponsUseQueueOperationType.UseCoupon:
                                item.Processed = CouponMultiplexorWrapper.UseCoupon(item.CouponCode, ref errorMessage);
                                break;
                            case CouponsUseQueueOperationType.UnfreezCoupon:
                                item.Processed = CouponMultiplexorWrapper.UnfreezCoupon(item.CouponCode,
                                    ref errorMessage);
                                break;
                        }
                        item.ErrorMessage = errorMessage;
                        Logger.Info("Coupon {0} for order {1} operation {2} is processed {3}", item.CouponCode, item.OrderId, operationType, item.Processed);

                    }
                    context.Save();
                }
            }

            return false;
        }
    }
}
