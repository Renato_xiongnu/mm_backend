﻿using System.Configuration;

namespace Orders.Backend.Services.PushService
{
    public static class Configuration
    {
        static Configuration()
        {
            DataSyncCustomServiceName = ConfigurationManager.AppSettings["Orders.Backend.DataSyncCustomServiceName"];
            DataSyncServiceUserName = ConfigurationManager.AppSettings["Orders.Backend.DataSyncServiceUserName"];
            DataSyncServiceUserPassword = ConfigurationManager.AppSettings["Orders.Backend.DataSyncServiceUserPassword"];
            DataSyncServiceUri = ConfigurationManager.AppSettings["Orders.Backend.DataSyncServiceUri"];
            CouchPublisherBathSize = int.Parse(ConfigurationManager.AppSettings["Orders.Backend.CouchPublisher.BathSize"]);
            CouchListenerBatchSize = int.Parse(ConfigurationManager.AppSettings["Orders.Backend.CouchListener.BatchSize"]);
            NagiosHost = ConfigurationManager.AppSettings["Orders.Backend.Nagios.Host"];
            NagiosServiceAddress = ConfigurationManager.AppSettings["Orders.Backend.Nagios.ServiceAddress"];
            NagiosEnabled = bool.Parse(ConfigurationManager.AppSettings["Orders.Backend.Nagios.Enabled"]);
        }

        public static readonly string DataSyncCustomServiceName;

        public static readonly string DataSyncServiceUserName;

        public static readonly string DataSyncServiceUserPassword;

        public static readonly string DataSyncServiceUri;

        public static readonly int CouchPublisherBathSize;

        public static readonly int CouchListenerBatchSize;

        public static readonly string NagiosHost;

        public static readonly string NagiosServiceAddress;

        public static readonly bool NagiosEnabled;
    }
}
