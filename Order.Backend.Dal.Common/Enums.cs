﻿using Common.Helpers;
using System.Linq;

namespace Order.Backend.Dal.Common
{
    public enum InternalOrderStatus
    {
        [EnumFieldDescription(Name = "Blank")]
        Blank = 0, //пустой

        [EnumFieldDescription(Name = "Created")]
        Created = 1, // Создан

        [EnumFieldDescription(Name = "Confirmed")]
        Confirmed = 2, // Подтвержден менеджером        

        [EnumFieldDescription(Name = "GC number is send to customer")]
        GiftCertificateNumberReservedNotifyCustomer = 3, // Пользователь оповещен о номере подарочной карты

        [EnumFieldDescription(Name = "Order is paid")]
        Paid = 4, //Заказ оплачен ( на карту начислена 1 копейка)

        [EnumFieldDescription(Name = "GS is credited")]
        GiftCertificateCredited = 5,

        [EnumFieldDescription(Name = "GS is activated")]
        GiftCertificateCreditedNotifyCustomer = 6,

        [EnumFieldDescription(Name = "Order is shipped")]
        Shipped = 10,

        [EnumFieldDescription(Name = "Error in GS activation")]
        GiftCertificateCreditedError = 50,        

        [EnumFieldDescription(Name = "Life cycle is ignored")]
        LifeCycleIgnored = 75,

        [EnumFieldDescription(Name = "Order is closed")]
        Closed = 100, // Заказ завершен

        /// <summary>
        /// Отменен менеджером
        /// </summary>
        [EnumFieldDescription(Name = "Order is rejected")]
        Rejected = 101,

        /// <summary>
        /// Отменен клиентом
        /// </summary>
        [EnumFieldDescription(Name = "Order is rejected by customer")]
        RejectedByCustomer = 102,
    }


    public enum ChangedType
    {
        [EnumFieldDescription(Name = "Система")]
        System = 0,
        [EnumFieldDescription(Name = "Клиент")]
        Customer = 1,
        [EnumFieldDescription(Name = "Менеджер")]
        Manager = 2
    }


    public enum LineType
    {
        [EnumFieldDescription("Товар")]
        Item = 0,
        [EnumFieldDescription("Набор")]
        Set = 1,
        [EnumFieldDescription("Подарочная карта")]
        GiftCard = 2
    }

    public enum ExternalSystem
    {
        [EnumFieldDescription("Отсутствует")]
        None = 0,
        [EnumFieldDescription("Авито")]
        Avito = 1,
        [EnumFieldDescription("ZZT")]
        ZZT = 2
    }
    public enum OrderPaymentType
    {
        [EnumFieldDescription("Наличные")]
        Cash = 0,
        [EnumFieldDescription("Карта")]
        Online = 1,
        [EnumFieldDescription("Онлайн кредит")]
        OnlineCredit = 2,
        [EnumFieldDescription("Социальная карта")]
        SocialCard = 3,
        [EnumFieldDescription("-")]
        Unknown
    }

    public enum OrderSourceType
    {
        CallCenter,
        IPhone,
        MobileWebSite,
        WebSite,
        Windows8,
        Store,
        Avito,
        Metro,
        MetroCallCenter,
        WebSiteQuickOrder,
        MobileWebSiteQuickOrder,
        YandexMarket
    }


    public enum ReserveStatus
    {
        [EnumFieldDescription("Подготовлен")]
        Prepared,
        [EnumFieldDescription("В редактировании")]
        IsEditing,
        [EnumFieldDescription("Создан")]
        Created,
        [EnumFieldDescription("Отменен")]
        Canceled,
    }

    public enum StockItemState
    {
        [EnumFieldDescription("Новый")]
        New,
        [EnumFieldDescription("Витринный образец")]
        DisplayItem,
        [EnumFieldDescription("Из ремонта")]
        Repaired
    }

    /* Not use for this version
    public enum BackendItemState
    {
        [EnumFieldDescription("Есть много")]
        HeightStockInShop,
        [EnumFieldDescription("В наличии")]
        StockInShop,
        [EnumFieldDescription("Новый")]
        New,
        [EnumFieldDescription("Витринный образец")]
        DisplayItem,
        [EnumFieldDescription("Есть в городе")]
        StockInCity,
        [EnumFieldDescription("Нет в городе")]
        NotInCity,
        [EnumFieldDescription("Нет в наличии")]
        Nowhere,
        [EnumFieldDescription("Заблокировано")]
        IsBlocked
    }
    */

    public enum ReviewItemState
    {
        [EnumFieldDescription("Не указано")]
        Empty,

        [EnumFieldDescription("Отложен")]
        Reserved,

        [EnumFieldDescription("Отложен частично")]
        ReservedPart,

        [EnumFieldDescription("Отложен витринный экземпляр")]
        ReservedDisplayItem,

        [EnumFieldDescription("Не найден")]
        NotFound,

        [EnumFieldDescription("Неправильная цена")]
        IncorrectPrice,

        [EnumFieldDescription("Трансфер")]
        Transfer,

        [EnumFieldDescription("Витрина")]
        DisplayItem,

        [EnumFieldDescription("Слишком низкая цена")]
        TooLowPrice ,
    }

    public enum ReserveContentStatus
    {
        New = 1,
        Updated = 10,
        Printed = 20,
        Error = 30
    }

    public enum  PrintingStatus
    {
        None = 0,
        //Skiped,
        Printed = 10,
        Error = 20
    }

    public enum ArticleProductType
    {
        Product = 0,
        Set = 1,
        WarrantyPlus = 2,
        InstallationService = 3,
        DeliveryService = 4,
        Statistical = 5,
        InstallServiceMarging = 6,
    }
    
    public static class EnumUtils
    {
        public static bool AllowAssingTask(InternalOrderStatus status)
        {
            return (new[]
                        {
                            InternalOrderStatus.Created,
                            InternalOrderStatus.Shipped,
                            InternalOrderStatus.Paid,
                            InternalOrderStatus.Closed,                            
                            InternalOrderStatus.Rejected, 
                            InternalOrderStatus.RejectedByCustomer, 
                        }).Contains(status);
        }

        public static bool AllowUpdateWWSOrderNumber(InternalOrderStatus status)
        {
            return (new[]
                        {
                            InternalOrderStatus.Confirmed,
                            InternalOrderStatus.GiftCertificateNumberReservedNotifyCustomer,
                            InternalOrderStatus.Paid,
                            InternalOrderStatus.GiftCertificateCredited,
                            InternalOrderStatus.GiftCertificateCreditedNotifyCustomer,
                            InternalOrderStatus.GiftCertificateCreditedError,
                            InternalOrderStatus.LifeCycleIgnored
                        }).Contains(status);
        }

        public static bool AllowCancelOrder(InternalOrderStatus status)
        {
            return !(new[]
                        {
                            InternalOrderStatus.Shipped,
                            InternalOrderStatus.Paid,
                            InternalOrderStatus.Closed,                            
                            InternalOrderStatus.Rejected, 
                            InternalOrderStatus.RejectedByCustomer, 
                        }).Contains(status);
        }

        public static bool AllowConfirmOrder(InternalOrderStatus status)
        {
            return true;
        }
    }
}
