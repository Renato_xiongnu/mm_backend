﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.BasketService
{
    [DataContract]
    public class OperationResult
    {
        public OperationResult()
        {
            OperationCompleted = true;
        }

        [DataMember]
        public bool OperationCompleted { get; set; }
        
        [DataMember]
        public string ErrorMessage { get; set; }
    }

    [DataContract]
    public class CreateOrderResult : OperationResult
    {
        [DataMember]
        public string OrderId { get; set; }
    }

    [DataContract]
    public class ValidateOrderResult : OperationResult
    {
        [DataMember]
        public decimal TotalPrice { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public IDictionary<string, string> BasketMessages { get; set; }

        [DataMember]
        public ICollection<ArticleInfo> Articles { get; set; }

        [DataMember]
        public ICollection<ArticleInfo> ExternalArticles { get; set; }

        [DataMember]
        public bool HasDelivery { get; set; }
    }

    [DataContract]
    public class ArticleInfo : ArticleQty
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string BrandName { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public ArticleType Type { get; set; }
    }

    public enum ArticleType
    {
        Item,
        Delivery
    }

    [DataContract]
    public class CancelOrderResult : OperationResult
    {
    }

    [DataContract]
    public class GetOrderInfoResult : OperationResult
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public ArticleType Type { get; set; }

        [DataMember]
        public DateTime DateTime { get; set; }

        [DataMember]
        public CustomerInfo CustomerInfo { get; set; }

        [DataMember]
        public decimal TotalPrice { get; set; }

        [DataMember]
        public OrderStatus OrderStatus { get; set; }

        [DataMember]
        public PaymentType PaymentType { get; set; }

        [DataMember]
        public ICollection<ArticleInfo> Articles { get; set; }

        [DataMember]
        public ICollection<ArticleInfo> ExternalArticles { get; set; }

        [DataMember]
        public bool WithDelivery { get; set; }
    }

 



}
