﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.BasketService
{
    [DataContract]
    public class ValidateOrderRequest
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public ICollection<ArticleQty> Articles { get; set; }

        [DataMember]
        public CustomerInfo CustomerInfo { get; set; }

        [DataMember]
        public bool WithDelivery { get; set; }
    }

    [DataContract]
    public class CustomerInfo
    {
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Address { get; set; }
    }

    [DataContract]
    public class ArticleQty
    {
        [DataMember]
        public string ArticleId { get; set; }

        [DataMember]
        public int  Qty {get; set;}
    }

    [DataContract]
    public class CreateOrderRequest
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public ICollection<ArticleQty> Articles { get; set; }

        [DataMember]
        public CustomerInfo CustomerInfo { get; set; }

        [DataMember]
        public bool WithDelivery { get; set; }
    }

    [DataContract]
    public class CancelOrderRequest
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string ReasonText { get; set; }
    }

    [DataContract]
    public class GetOrderInfoRequest
    {
        [DataMember]
        public string OrderId { get; set; }
    }
}
