﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orders.Backend.Services.Contracts.BasketService
{
    public enum OrderStatus
    {
        Undefined,
        NotConfirmed,
        Confirmed,
        Cancelled,
        Paid
    }


    public enum PaymentType
    { 
        Cash,
        Online
    }
}
