﻿using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.RequestFormService
{
    [DataContract]
    public class ArticleInfo
    {
        [DataMember]
        public long ArticleId { get; set; }

        [DataMember]
        public string SerialNumber { get; set; } // Серийный номер  ремонт

        [DataMember]
        public string ReasonForClaim { get; set; } // Описание неисправности или Причина обмена

        [DataMember]
        public bool WasInUse { get; set; } //Был товар в употреблении ремонт        

        [DataMember]
        public CompositionType CompositionType { get; set; } //Комплектация товара

    }
}
