﻿using System;
using System.Runtime.Serialization;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Services.Contracts.RequestFormService
{
    [DataContract]
    public class CreateRequestFormData
    {
        [DataMember]
        public string RequestId { get; set; }

        [DataMember]
        public StoreInfo StoreInfo { get; set; }

        [DataMember]
        public DateTime PurchaseDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public RequestFormType FormType { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public bool NeedPickup { get; set; }
    }
}
