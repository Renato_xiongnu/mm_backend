﻿namespace Orders.Backend.Services.Contracts.RequestFormService
{
    public enum RequestFormType
    {
        Repair,
        Exchange
    }

    public enum CompositionType
    {
        None,
        Full,
        Partial
    }
}
