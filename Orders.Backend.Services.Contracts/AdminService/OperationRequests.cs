﻿using Orders.Backend.Services.Contracts.GiftCertificate;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.AdminService
{
    [DataContract]
    public class Store
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public ChainType ChainType { get; set; }

        [DataMember]
        public City City { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public decimal CertificateAmount { get; set; }
    }

    [DataContract]
    public class City
    {
        [DataMember]
        public string CityId { get; set; }

        [DataMember]
        public string CityName { get; set; }
    }
}
