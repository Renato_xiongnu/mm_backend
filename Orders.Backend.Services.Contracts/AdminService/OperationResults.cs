﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Services.Contracts.AdminService
{       

    [DataContract]
    public class GetStoresResult
    {
        [DataMember]
        public ICollection<Store> Stores { get; set; }
    }

    
    [DataContract]
    public class EntityStatistics
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Object Value { get; set; }

        [DataMember]
        public string PrimitiveType { get; set; }
    }
}
 
