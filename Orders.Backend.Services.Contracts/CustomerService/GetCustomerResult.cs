﻿using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.CustomerService
{
    [DataContract]
    public class GetCustomerResult
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "surname")]
        public string Surname { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "delivery_address")]
        public string DeliveryAddress { get; set; }

        [DataMember(Name = "pickup_location_id")]
        public string PickupLocationId { get; set; }
    }
}
