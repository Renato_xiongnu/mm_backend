﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class CertificateInfo
    {
        [DataMember]
        public string CertificateId { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public GiftCertStatus Status { get; set; }

        [DataMember]
        public string Content { get; set; }
    }
}
