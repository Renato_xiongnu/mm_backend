﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class InitArticleData : ArticleData
    {
        [DataMember]
        public string WarrantyInsuranceArticle { get; set; }

        [DataMember]
        public bool HasShippedFromStock { get; set; }

        [DataMember]
        public ICollection<InitArticleData> SubArticleData { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public int? StockNumber { get; set; }
    }
}
