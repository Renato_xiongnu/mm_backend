﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ReserveInfo
    {
        [DataMember]
        public PrintingStatus PrintingStatus { get; set; }

        [DataMember]
        public ReserveStatus Status { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }

        [DataMember]
        public string StoreManagerName { get; set; }

        [DataMember]
        public DateTime? CreationDate { get; set; }

        [DataMember]
        public ReservePrice TotalPrice { get; set; }

        [DataMember]
        public ReservePrice PrepaymentPrice { get; set; }

        [DataMember]
        public ReservePrice LeftoverPrice { get; set; }

        [DataMember]
        public DeliveryPeriod DeliveryPeriod { get; set; }

        [DataMember]
        public ReserveCustomerInfo CustomerInfo { get; set; }

        [DataMember]
        public SalesDocumentData SalesDocumentData { get; set; }

        [DataMember]
        public ICollection<ReserveLine> ReserveLines { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string WWSOrderId { get; set; }   
      
    }
}
