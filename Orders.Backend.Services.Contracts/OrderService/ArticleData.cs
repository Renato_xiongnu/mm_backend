﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    [KnownType(typeof(InitArticleData))]
    public class ArticleData
    {
        [DataMember]
        public string ArticleNum { get; set; }

        [DataMember]
        public Decimal Price { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public ItemState ItemState { get; set; }

        [DataMember]
        public ICollection<string> Promotions { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string BrandTitle { get; set; }

        [DataMember]
        public bool? IsVirtual { get; set; }

        [DataMember]
        public Decimal? OldPrice { get; set; }

        [DataMember]
        public Decimal? Benefit { get; set; }

        [DataMember]
        public String CouponCompainId { get; set; }

        [DataMember]
        public String CouponCode { get; set; }

        [DataMember]
        public ArticleProductType ArticleProductType { get; set; }

        [DataMember]
        public Int64? RefersToItem { get; set; }

    }
}
