﻿using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class OrderInfo
    {
        [DataMember]
        public CertificateInfo certificateInfo { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string BasketId { get; set; }

        [DataMember]
        public bool IsPreorder { get; set; }

        [DataMember]
        public string WWSOrderId { get; set; }

        [DataMember]
        public DateTime UpdateDate { get; set; }

        [DataMember]
        public OrderSource OrderSource { get; set; }

        [DataMember]
        public PaymentType PaymentType { get; set; }

        [DataMember]
        public decimal Prepay { get; set; }

        [DataMember]
        public PaymentType FinalPaymentType { get; set; }

        [DataMember]
        public InternalStatusInfo StatusInfo { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string BasketComment { get; set; }

        [DataMember]
        public string ExternalNumber { get; set; }

        [DataMember]
        public ExternalSystem ExternalSystem { get; set; }

        [DataMember]
        public DateTime? ExpirationDate { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public string SaleLocationSapCode { get; set; }

        [DataMember]
        public string CouponCode { get; set; }

        [DataMember]
        public bool ExpirationDatePostponed { get; set; }

        [DataMember]
        public string InstallServiceAddress { get; set; }
    }
}
