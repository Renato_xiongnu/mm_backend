﻿using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class OrderStatusHistoryItem
    {
        [DataMember]
        public InternalStatusInfo FromStatus { get; set; }

        [DataMember]
        public InternalStatusInfo ToStatus { get; set; }

        [DataMember]
        public DateTime UpdateDate { get; set; }

        [DataMember]
        public string WhoChanged { get; set; }

        [DataMember]
        public string ReasonText { get; set; }

        [DataMember]
        public ChangedTypeInfo ChangedType { get; set; }
    }
}