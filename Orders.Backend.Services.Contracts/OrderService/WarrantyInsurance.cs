﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class WarrantyInsurance
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public int DocumentPositionNumber { get; set; }

        [DataMember]
        public int RelatedArticleNo { get; set; }

        [DataMember]
        public decimal WarrantySum { get; set; }

        [DataMember]
        public string WwsCertificateState { get; set; }

        [DataMember]
        public string WwsExtensionPrint { get; set; }
    }
}
