﻿using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    public enum ReturnCode
    {
        //NotImplemented,
        Ok,

        Error
    }

    public enum GeneralOrderStatus
    {
        Created,

        Confirmed,

        Rejected,

        RejectedByCustomer,

        ConfirmedWithGiftCertificate,

        GiftCertificateNumberReservedNotifyCustomer,

        OnlinePaymentReceived,

        OnlinePaymentDeclined,

        Paid,

        Closed,

        Shipped
    }

    public enum ContractStatus
    {
        NotImplemented,

        OK
    }

    public enum OrderSource
    {
        CallCenter,

        IPhone,

        MobileWebSite,

        WebSite,

        Windows8,

        Store,

        Avito,

        Metro,

        MetroCallCenter,

        WebSiteQuickOrder,

        MobileWebSiteQuickOrder,

        YandexMarket,

        IPhoneQuickOrder
    }

    public enum ExternalSystem
    {
        None, // default

        Avito,

        ZZT
    }

    public enum PaymentType
    {
        Cash, // Pay at the store

        Online, // Pay online

        OnlineCredit,

        SocialCard,
    }

    public enum GiftCertStatus
    {
        Reserved,

        Credited,

        IsUsed // for future use if we will start seek usage of certificate
    }

    public enum DeliveryType
    {
        Standard,

        Accurate
    }

    public enum ItemState
    {
        New,

        DisplayItem,

        Repaired
    }

    public enum ReviewItemState
    {
        Empty,

        Reserved,

        ReservedPart,

        ReservedDisplayItem,

        NotFound,

        IncorrectPrice,

        Transfer,

        DisplayItem,
        
        TooLowPrice,
    }

    public enum ProductType
    {
        [EnumMember]
        Article = 0,

        [EnumMember]
        Set = 1,

        [EnumMember]
        WarrantyPlus = 2,
    }

    public enum ArticleProductType
    {
        [EnumMember]
        Product = 0,

        [EnumMember]
        Set = 1,

        [EnumMember]
        WarrantyPlus = 2,

        [EnumMember]
        InstallationService = 3,

        [EnumMember]
        DeliveryService = 4,

        [EnumMember]
        Statistical = 5,

        /// <summary>
        /// доплата для сервиса установки
        /// </summary>
        [EnumMember]
        InstallServiceMarging = 6,


    }
}