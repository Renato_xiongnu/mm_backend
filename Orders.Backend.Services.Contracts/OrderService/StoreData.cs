﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class StoreData
    {                                                    
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string TimeZome { get; set; }

        [DataMember]
        public TimeSpan ReserveLifeTime { get; set; }

        [DataMember]
        public TimeSpan StartWorkingTime { get; set; }

        [DataMember]
        public TimeSpan EndWorkingTime { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? PaidDeliveryArticleId { get; set; }

        [DataMember]
        public int? FreeDeliveryArticleId { get; set; }

        [DataMember]
        public bool IsVirtual { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsHub { get; set; }
    }
}
