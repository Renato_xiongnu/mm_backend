﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ReserveCustomerInfo
    {
        [DataMember]
        public string ClassificationNumber { get; set; }

        [DataMember]
        public string Salutation { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Phone2 { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string CountryAbbreviation { get; set; }

        [DataMember]
        public string AddressIndex { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string INN { get; set; }

        [DataMember]
        public string KPP { get; set; }

        [DataMember]
        public string Email { get; set; }
    }
}
