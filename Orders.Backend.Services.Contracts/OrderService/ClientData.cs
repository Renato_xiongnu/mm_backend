﻿using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ClientData
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Phone2 { get; set; }

        [DataMember]
        public string Fax { get; set; }
		
        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime? Birthday { get; set; }

        [DataMember]
        public string Gender { get; set; }        

        [DataMember]
        public CardInfo SocialCard { get; set; }
        
        [DataMember]
        [Obsolete]
        public long? ZZTCustomerId { get; set; }
    }

    [DataContract]
    public class CardInfo
    {
        [DataMember]
        public string Number { get; set; }

        //[DataMember]
        //public DateTime ValidThru { get; set; }
    }
    
}
