﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class SalesDocumentData
    {
        [DataMember]
        public string OutletInfo { get; set; }
        
        [DataMember]
        public string ProductPickupInfo { get; set; }
        
        [DataMember]
        public string PrintableInfo { get; set; }

        [DataMember]
        public int? SalesPersonId { get; set; }
    }
}
