﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{   

    [DataContract]
    public class CreateOrderData
    {
        [DataMember]
        public OrderSource OrderSource { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public PaymentType PaymentType { get; set; } // New

        [DataMember]
        public bool IsTest { get; set; }

        [DataMember]
        public bool IsPreorder { get; set; }

        [DataMember]
        public StoreInfo StoreInfo { get; set; }
        
        [DataMember]
        public string Comment { get; set; } // New

        [DataMember]
        public string BasketComment { get; set; } // New

        [DataMember]
        public ExternalSystem ExternalSystem { get; set; } // New

        [DataMember]
        public string ExternalNumber { get; set; } // New   

        [DataMember]
        public DateTime? ExpirationDate { get; set; } // New   

        [DataMember]
        public bool IsOnlineOrder { get; set; }

        [DataMember]
        public SalesDocumentData SalesDocumentData { get; set; }

        [DataMember]
        public string UtmSource { get; set; }

        [DataMember]
        public Decimal? Prepay { get; set; }

        [DataMember]
        public DateTime? Created { get; set; }

        [DataMember]
        public string BasketId { get; set; }

        [DataMember]
        public string InitialSapCode { get; set; }

        [DataMember]
        public string CouponCode { get; set; }

        [DataMember]
        public string InstallServiceAddress { get; set; }

    }

    [DataContract]
    public class UpdateOrderData
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string WWSOrderId { get; set; }

        [DataMember]
        public string UpdatedBy { get; set; }

        [DataMember]
        public string ReasonText { get; set; }

        [DataMember]
        public GeneralOrderStatus? ChangeStatusTo { get; set; }

        [DataMember]
        public bool? IsApproved { get; set; }

        [DataMember]
        public DeliveryInfo DeliveryInfo { get; set; }

        [DataMember]
        public ClientData ClientData { get; set; }

        [DataMember]
        public PaymentType? FinalPaymentType { get; set; } // for onlineCredit

        [DataMember]
        public string Comment { get; set; } // New

        [DataMember]
        public string ReserveContent { get; set; }

        [DataMember]
        public DateTime? ExpirationDate { get; set; } // New   

        [DataMember]
        public bool? IsPreorder { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public Decimal? Prepay { get; set; }

        [DataMember]
        public string BasketId { get; set; }

        [DataMember]
        public string CouponCode { get; set; }

        [DataMember]
        public string InstallServiceAddress { get; set; }

    }

    [DataContract]
    public class UpdateReservesContentData
    {
        [DataMember]
        public ICollection<ReserveContentInfo> Contents { get; set; }        
    }
    
}
