﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ReserveContentInfo
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string WWSOrderId { get; set; }

        [DataMember]
        public PrintingStatus PrintingStatus { get; set; }

        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class ReserveContent
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string WWSOrderId { get; set; }

        [DataMember]
        public string Content { get; set; }
    }
}
