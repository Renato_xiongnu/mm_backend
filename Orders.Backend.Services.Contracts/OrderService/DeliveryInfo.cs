using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class DeliveryPeriod
    {
        [DataMember]
        public DateTime? DeliveryDate { get; set; }

        [DataMember]
        public string Period { get; set; }

        [DataMember]
        public string Comment { get; set; }
    }

    [DataContract]
    public class DeliveryInfo : DeliveryPeriod
    {
        [DataMember]
        public bool HasDelivery { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string ExactAddress { get; set; } //TODO: add to version 1.0 

        [DataMember]
        public double? Latitude { get; set; } //Delivery

        [DataMember]
        public double? Longitude { get; set; } //Delivery

        [DataMember]
        public string PickupLocationId { get; set; }

        [DataMember]
        public bool? TransferAgreementWithCustomer { get; set; }

        [DataMember]
        public string ZIPCode { get; set; }

        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public string House { get; set; }

        [DataMember]
        public string Housing { get; set; }

        [DataMember]
        public string Building { get; set; }

        [DataMember]
        public string Apartment { get; set; }

        [DataMember]
        public string Entrance { get; set; }

        [DataMember]
        public string EntranceCode { get; set; }

        [DataMember]
        public int? Floor { get; set; }

        [DataMember]
        public string SubwayStationName { get; set; }

        [DataMember]
        public string District { get; set; }

        [DataMember]
        public bool? HasServiceLift { get; set; }

        [DataMember]
        public DateTime? RequestedDeliveryDate { get; set; }

        [DataMember]
        public string RequestedDeliveryTimeslot { get; set; }

        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public string Kladr { get; set; }

        [DataMember]
        public string StreetAbbr { get; set; }

        [DataMember]
        public string RequestedDeliveryServiceOption { get; set; }

        //���� �� ������������, �������� ��� ��������� ���������� ���� ��� � �������� ������� (����� � ��� ��� ���� ���� Period)
        //[DataMember]
        //public DeliveryTypeInfo DeliveryTypeInfo { get; set; }
    }

    [DataContract]
    public class PickupLocation
    {
        [DataMember]
        public string PickupLocationId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public PickupPointType PickupPointType { get; set; }

        [DataMember]
        public string OperatorPuPId { get; set; }

        [DataMember]
        public string OperatorName { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string ZipCode { get; set; }
    }

    [DataContract]
    public enum PickupPointType
    {
        [EnumMember]
        Shop,
        [EnumMember]
        Postomant
    }

    [DataContract]
    public class DeliveryTypeInfo
    {
        [DataMember]
        public DeliveryType DeliveryType { get; set; }

        [DataMember]
        public DateTime? DeliveryDate { get; set; }

        [DataMember]
        public TimeSpan? FromTime { get; set; }

        [DataMember]
        public TimeSpan? ToTime { get; set; }
    }
}