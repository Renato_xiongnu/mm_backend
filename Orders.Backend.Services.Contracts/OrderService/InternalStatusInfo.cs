﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class InternalStatusInfo
    {
        [DataMember]
        public InternalOrderStatus Status { get; set; }

        [DataMember]
        public string Text { get; set; }
    }
}
