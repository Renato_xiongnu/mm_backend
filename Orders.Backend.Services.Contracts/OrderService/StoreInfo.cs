﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class StoreInfo
    {
        [DataMember]
        public string McsId { get; set; }

        [DataMember]
        public string SapCode { get; set; }
    }
}
