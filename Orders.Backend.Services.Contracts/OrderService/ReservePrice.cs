﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ReservePrice
    {
        [DataMember]
        public int Vat { get; set; }

        [DataMember]
        public decimal NetPrice { get; set; }

        [DataMember]
        public decimal VatPrice { get; set; }

        [DataMember]
        public decimal GrossPrice { get; set; }
    }
}
