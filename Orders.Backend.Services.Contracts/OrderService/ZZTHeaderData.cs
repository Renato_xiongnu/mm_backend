﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ZZTHeaderData
    {
        [DataMember]
        public string RecipientName { get; set; }

        [DataMember]
        public ZZTDeliveryAddress DeliveryAddress { get; set; }

        [DataMember]
        public ZZTDeliveryInfo DeliveryInfo { get; set; }

        [DataMember]
        public string CampaignId { get; set; }

        [DataMember]
        public string ConsumerId { get; set; }

        [DataMember]
        public string ZztOrderId { get; set; }
    }

    [DataContract] 
    public class ZZTDeliveryAddress
    {
        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public string District { get; set; }

        [DataMember]
        public string MetroStation { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string CityId { get; set; }

        [DataMember]
        public string RegionId { get; set; }

        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public string House { get; set; }

        [DataMember]
        public string Housing { get; set; }

        [DataMember]
        public string Building { get; set; }

        [DataMember]
        public string Flat { get; set; }

        [DataMember]
        public string Entrance { get; set; }

        [DataMember]
        public string EntranceCode { get; set; }

        [DataMember]
        public string Floor { get; set; }

        [DataMember]
        public bool? HasElevator { get; set; }

        [DataMember]
        public string Additional { get; set; }
    }

    [DataContract] 
    public class ZZTDeliveryInfo
    {
        [DataMember]
        public string DeliveryTime { get; set; }

        [DataMember]
        public decimal? ClimbPrice { get; set; }

        [DataMember]
        public decimal? DeliveryPrice { get; set; }

        [DataMember]
        public DateTime? DeliveryDate { get; set; }

        [DataMember]
        public string DeliveryType { get; set; }

        [DataMember]
        public string DeliveryPlaceSapCode { get; set; }

        [DataMember]
        public string DeliveryPlaceId { get; set; }

    }
}
