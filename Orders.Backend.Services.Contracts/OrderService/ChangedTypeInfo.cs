﻿using System.Runtime.Serialization;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ChangedTypeInfo
    {
        [DataMember]
        public ChangedType ChangedType { get; set; }

        [DataMember]
        public string Text { get; set; }
    }
}