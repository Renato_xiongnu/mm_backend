﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class OperationResult
    {
        private IList<string> _errorMessage;

        [DataMember]
        public ReturnCode ReturnCode { get; set; }

        [DataMember]
        public IList<string> ErrorMessages
        {
            get
            {
                return _errorMessage ?? (_errorMessage = new List<string>());
            }
            set
            {
                _errorMessage = value;
            }
        }

        [DataMember]
        public string ErrorMessage
        {
            get
            {
                return string.Join(Environment.NewLine, ErrorMessages);
            }
            set
            {
            }
        }

        public void AddError(string message, params object[] args)
        {
            if(message == null)
            {
                throw new ArgumentNullException("message");
            }
            ReturnCode = ReturnCode.Error;
            ErrorMessages.Add(string.Format(message, args));
        }
    }

    [DataContract]
    public class ValidateOrderOperationResult : OperationResult
    {
    }

    [DataContract]
    public class CreateOrderOperationResult : OperationResult
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public GeneralOrderStatus GeneralOrderStatus { get; set; }
    }

    [DataContract]
    public class UpdateOrderOperationResult : OperationResult
    {
        [DataMember]
        public GeneralOrderStatus FromStatus { get; set; }

        [DataMember]
        public GeneralOrderStatus ToStatus { get; set; }
    }

    [DataContract]
    public class ImplContractsStatusOperationResult
    {
        [DataMember]
        public ContractStatus ValidateOrder { get; set; }

        [DataMember]
        public ContractStatus CreateOrder { get; set; }

        [DataMember]
        public ContractStatus UpdateOrderStatus { get; set; }
    }

    [DataContract]
    public class GenerateProofOfSaleResult : OperationResult
    {
        [DataMember]
        public long NotificationId { get; set; }
    }

    [DataContract]
    public class GerOrderDataResult : OperationResult
    {
        [DataMember]
        public OrderInfo OrderInfo { get; set; }

        [DataMember]
        public DeliveryInfo DeliveryInfo { get; set; }

        [DataMember]
        public ClientData ClientData { get; set; }

        [DataMember]
        public ICollection<ArticleData> ArticleLines { get; set; }

        [DataMember]
        public ReserveInfo ReserveInfo { get; set; }

        [DataMember]
        public ZZTHeaderData ZZTData { get; set; }

        [DataMember]
        public PickupLocation PickupLocation { get; set; }
    }

    [DataContract]
    public class GetOrderDataResult : OperationResult
    {
        [DataMember]
        public OrderInfo OrderInfo { get; set; }

        [DataMember]
        public DeliveryInfo DeliveryInfo { get; set; }

        [DataMember]
        public ClientData ClientData { get; set; }

        [DataMember]
        public ICollection<ArticleData> ArticleLines { get; set; }

        [DataMember]
        public ReserveInfo ReserveInfo { get; set; }        
    }


    [DataContract]
    public class GetReservesContentResult : OperationResult
    {
        [DataMember]
        public ICollection<ReserveContent> Contents { get; set; }
    }

    [DataContract]
    public class GetStoreDataResult : OperationResult
    {
        [DataMember]
        public StoreData StoreData { get; set; }
    }

    [DataContract]
    public class GetAllStoresDataResult : OperationResult
    {
        [DataMember]
        public ICollection<StoreData> Stores { get; set; }
    }

    [DataContract]
    public class GerOrderStatusHistoryResult : OperationResult
    {
        [DataMember]
        public OrderStatusHistoryItem[] OrderStatusHistory { get; set; }
    }
    
    
    [DataContract]
    public class GerReservesResult : OperationResult
    {
        [DataMember]
        public ReserveInfo[] Reserves { get; set; }
    }
}