﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.OrderService
{
    [DataContract]
    public class ReserveLine
    {
        [DataMember]
        public long LineId { get; set; }

        [DataMember]
        public ArticleData ArticleData { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int StockQty { get; set; }

        [DataMember]
        public Decimal StockPrice { get; set; }

        [DataMember]
        public ReviewItemState ReviewItemState { get; set; }

        [DataMember]
        public ItemState StockItemState { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public bool IsOffered { get; set; }

        [DataMember]
        public bool HasShippedFromStock { get; set; }
        
        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public string WWSProductGroupName { get; set; }

        [DataMember]
        public int WWSProductGroupNumber { get; set; }

        [DataMember]
        public int WWSFreeQty { get; set; }

        [DataMember]
        public int WWSReservedQty { get; set; }

        [DataMember]
        public decimal WWSPriceOrig { get; set; }

        [DataMember]
        public int WWSStockNumber { get; set; }

        [DataMember]
        public int WWSDepartmentNumber { get; set; }

        [DataMember]
        public string ArticleCondition { get; set; }        

        [DataMember]
        public int WWSPositionNumber { get; set; }

        [DataMember]
        public int WWSStoreNumber { get; set; }

        [DataMember]
        public int WWSVAT { get; set; }                                

        [DataMember]
        public ProductType ProductType { get; set; }

        [DataMember]
        public int LineNumber { get; set; }

        [DataMember]
        public int? RefLineNumber { get; set; }

        [DataMember]
        public WarrantyInsurance WarrantyInsurance { get; set; }
        
        [DataMember]
        public ICollection<ReserveLine> SubLines { get; set; }

        [DataMember]
        public string TransferSapCode { get; set; }

        [DataMember]
        public int TransferNumber { get; set; }

        [DataMember]
        public bool? IsVirtual { get; set; }

        [DataMember]
        public Decimal? OldPrice { get; set; }

        [DataMember]
        public Decimal? Benefit { get; set; }

        [DataMember]
        public String CouponCompainId { get; set; }

        [DataMember]
        public String CouponCode { get; set; }
                                                       
        [DataMember]
        public ArticleProductType ArticleProductType { get; set; }
    }
}
