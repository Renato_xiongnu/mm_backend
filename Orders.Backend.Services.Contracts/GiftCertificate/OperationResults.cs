﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.GiftCertificate
{
    [DataContract]
    public class StoreCertificates
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public ICollection<string>  CertificateIds { get; set; }
    }

    [DataContract]
    public class GetCertificatesForSlipsResult
    {
        [DataMember]
        public ICollection<StoreCertificates> ReserverCertificates { get; set; }
    }

    [DataContract]
    public class GetCertificatesForCreditingResult
    {
        [DataMember]
        public ICollection<CertificateForCrediting> Certificates { get; set; }
    }
   
}
