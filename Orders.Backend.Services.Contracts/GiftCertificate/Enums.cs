﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orders.Backend.Services.Contracts.GiftCertificate
{
    public enum ItemType
    {
        Item,
        Set,
        GiftCard
    }


    public enum ChainType
    {
        MediaMarkt,
        Saturn
    }
}
