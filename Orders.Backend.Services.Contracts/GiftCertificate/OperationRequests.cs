﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.Contracts.GiftCertificate
{

    [DataContract]
    public class ItemData
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Decimal Price { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public ItemType ItemType { get; set; }
    }

    [DataContract]
    public class CreditedCertificate
    {
        [DataMember]
        public string CertificateId { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

    }

    [DataContract]
    public class CertificateForCrediting
    {
        [DataMember]
        public ChainType Chain { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string CertificateId { get; set; }

        [DataMember]
        public decimal Amount { get; set; }
    }
}
