iisreset \stop

net stop MM.Orders.Backend.Scheduler
net stop MM.TicketTool.Scheduler
net stop MMS.Cloud.Wf.Connector.TTMMFORD
net stop MMS.Orders.Backend.Push.FromCloud
net stop MMS.Orders.Backend.Push.PickupLocation
net stop TTMoitoringService
net stop AppFabricEventCollectionService
net stop AppFabricWorkflowManagementService

