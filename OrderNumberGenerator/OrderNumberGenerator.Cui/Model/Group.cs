﻿using System;

namespace OrderNumberGenerator.Cui.Model
{
    public class Group
    {
        public String Number { get; set; }
        public Boolean IsUsed { get; set; }

        public Boolean IsActive { get; set; }

        public DateTime? LastUpdate { get; set; }
    }
}
