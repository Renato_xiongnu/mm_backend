﻿using System;

namespace OrderNumberGenerator.Cui.Model
{
    public class Numbers
    {
        public String Number { get; set; }

        public Boolean IsUsed { get; set; } 
    }
}
