﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NumbersGenerator;
using OrderNumberGenerator.Cui.Model;

namespace OrderNumberGenerator.Cui
{
    class Program
    {
    
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["OrderNumberGen"].ConnectionString;

        #region file magic

        private static void ReadFromFiles(HashSet<String> numbers, Int32 skipCount)
        {
            const string fName = @"C:\4\lines";
            const int takeCount = 40000;
            skipCount *= takeCount;

            const string fileName0 = fName + "0.txt";
            const string fileName1 = fName + "1.txt";
            const string fileName2 = fName + "2.txt";
            const string fileName3 = fName + "3.txt";
            const string fileName4 = fName + "4.txt";

            var line0 = new List<String>(takeCount);
            var line1 = new List<String>(takeCount);
            var line2 = new List<String>(takeCount);
            var line3 = new List<String>(takeCount);
            var line4 = new List<String>(takeCount);

            GetNumbersFromFile(fileName0, skipCount, takeCount, line0);
            GetNumbersFromFile(fileName1, skipCount, takeCount, line1);
            GetNumbersFromFile(fileName2, skipCount, takeCount, line2);
            GetNumbersFromFile(fileName3, skipCount, takeCount, line3);
            GetNumbersFromFile(fileName4, skipCount, takeCount, line4);
            for (var i = 0; i < takeCount; i++)
            {
                numbers.Add(line0[i]);
                numbers.Add(line1[i]);
                numbers.Add(line2[i]);
                numbers.Add(line3[i]);
                numbers.Add(line4[i]);
            }

        }


        private static void GetNumbersFromFile(String fileName, Int32 skipCount, Int32 takeCount, List<String> numbers)
        {
            using (var sr = new SkippableStreamReader(fileName))
            {
                sr.SkipLines(skipCount);
                string line;
                var counter = 0;
                while ((line = sr.ReadLine()) != null && counter <= takeCount)
                {
                    numbers.Add(line);
                    counter++;
                }
            }
        }

        //private static void ReadFromFilesRandomNumbers()
        //{ 

        //    var numbers = new HashSet<String>();
        //    var connectionString = ConfigurationManager.ConnectionStrings["OrderNumberGen"].ConnectionString;
        //    var globalSw = new Stopwatch();
        //    globalSw.Start(); 
        //    var stw = new Stopwatch();
        //    stw.Start();

        //    var insCounter = 1;
        //    for (var i =0; i < 500;i++)
        //    {
        //        ReadFromFiles(numbers, i);
        //        //if(numbers.Count < 200000)
        //        //    continue;
        //        var list = numbers.Select(line => new OrderNumber { OrderId = line }).ToList();
        //        BulkInsert(connectionString, "OrderNumber", list);
        //        stw.Stop();
        //        Console.WriteLine(stw.ElapsedMilliseconds);
        //        numbers.Clear();
        //        list.Clear();
        //        Console.WriteLine("Insert complite -"+ insCounter);
        //        insCounter++;
        //        stw.Reset();
        //        stw.Start();


        //    }
        //    globalSw.Stop();
        //    Console.WriteLine(globalSw.Elapsed.Minutes);
        //    Console.WriteLine(globalSw.Elapsed.Seconds);
        //    Console.WriteLine(globalSw.ElapsedMilliseconds);

        //}

        #endregion file magic


        #region numbergen

        private static void GenGroupsNumbers()
        {
            var groups = GenerateNumbers(5);
            var list = groups.Select(line => new Group { Number = line, IsActive = false, IsUsed = false }).ToList();
            BulkInsert(ConnectionString, "[Groups]", list);
            Console.WriteLine("ok");
        }

        private static void GenNumbers()
        {
            var numbers = GenerateNumbers(3);
            var list = numbers.Select(line => new Numbers { Number = line, IsUsed = false }).ToList();
            BulkInsert(ConnectionString, "[Numbers]", list);
            Console.WriteLine("ok");
        }
        #endregion numbergen


        static void Main(string[] args)
        {
            var groups = GenerateNumbers(5);
            var list = groups.Select(line => new { Number = line, IsUsed = false }).ToList();
            BulkInsert(ConnectionString, "[NumberBase]", list);
            Console.WriteLine("ok");
            Console.ReadLine();
        }

       


       

      

        private static IEnumerable<String> GenerateNumbers(Int32 itemLenght)
        {
            var values = new List<Int32> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var allVariations = new Variations<Int32>(values, itemLenght, GenerateOption.WithRepetition);
            var list = allVariations.Select(x => String.Join("", x));
            return list.Shuffle();
        }

        private static void GenerateAndWriteToFileNumbers()
        {
            const UInt16 itemLenght = 8;
            var values = new List<Int32> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var i = 0;
            const string fName = @"C:\4\lines";
            var counter = 0;
            const Int32 pageSize = 2000000;
            var allVariations = new Variations<Int32>(values, itemLenght, GenerateOption.WithRepetition);
            while (i <= 50)
            {
                
                if (i == 0 )
                 counter =0;
                else if (i%10 == 0)
                    counter = i/10;
                var path = fName + counter + ".txt";
                var allStr = allVariations.Select(x => String.Join("", x)).Skip(i * pageSize).Take(pageSize);
                var stw = new Stopwatch();
                stw.Start();
                var foo = allStr.Shuffle();
                using (var fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                {
                    using (var sw = new StreamWriter(fs))
                    {
                        foreach (var item in foo)
                        {
                            sw.WriteLine(item);
                        }
                    }

                }
                stw.Stop();
                Console.WriteLine(stw.ElapsedMilliseconds);
                i++;
            }
           
            
        }


        public static void BulkInsert<T>(string connection, string tableName, IList<T> list)
        {
            using (var bulkCopy = new SqlBulkCopy(connection))
            {
                bulkCopy.BatchSize = list.Count;
                bulkCopy.DestinationTableName = tableName;

                var table = new DataTable();
                var props = TypeDescriptor.GetProperties(typeof(T))
                    //Dirty hack to make sure we only have system data types
                    //i.e. filter out the relationships/collections
                .Cast<PropertyDescriptor>()
                .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                .ToArray();
                foreach (var propertyInfo in props)
                {
                    bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);
                    table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                }

                var values = new object[props.Length];
                foreach (var item in list)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }

                    table.Rows.Add(values);
                }

                bulkCopy.WriteToServer(table);
            }
        } 


    }
}
