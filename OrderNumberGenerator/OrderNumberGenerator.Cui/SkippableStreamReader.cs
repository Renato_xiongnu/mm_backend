﻿using System;
using System.IO;

namespace OrderNumberGenerator.Cui
{
    class SkippableStreamReader : StreamReader
    {
        public SkippableStreamReader(String path) : base(path) { }

        public void SkipLines(Int32 linecount)
        {
            for (var i = 0; i < linecount; i++)
            {
                ReadLine();
            }
        }
    }
}
