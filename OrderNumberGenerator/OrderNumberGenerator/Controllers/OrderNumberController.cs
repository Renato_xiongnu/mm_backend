﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web.Http;
using Dapper;
using NLog;
using OrderNumberGenerator.Common;
using OrderNumberGenerator.Models;


namespace OrderNumberGenerator.Controllers
{ 
    [OrderNumberGeneratorAuthenticationFilter]
    public class OrderNumberController : ApiController
    {
        protected readonly Logger Logger;

        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["OrderNumberGenerator"].ConnectionString;

        public OrderNumberController()
        {
            Logger = LogManager.GetCurrentClassLogger(); 
        }

        [HttpGet] 
        public async Task<OrderNumber> generate_order_num(String sap_code = null)
        {
            var orderNumber = new OrderNumber();
            Logger.Info("New request with sapCode " + sap_code);
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(OrderNumberController.ConnectionString))
                {
                    var result = (string)await SqlMapper.ExecuteScalarAsync((IDbConnection)sqlConnection, "GetNumber", (object)null, (IDbTransaction)null, new int?(), new CommandType?(CommandType.StoredProcedure));
                    Logger.Info("Take from db new ID " + result);
                    orderNumber.orderNum = sap_code + "-" + result;
                }
                return orderNumber;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return orderNumber;

        }
      
    }
}
