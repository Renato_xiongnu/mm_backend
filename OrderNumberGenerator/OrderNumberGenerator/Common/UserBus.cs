﻿using System;
using System.Configuration;
using System.Linq;
using NLog;

namespace OrderNumberGenerator.Common
{
    public class UserBus
    {
        protected readonly Logger Logger;

        public UserBus()
        {
            Logger = LogManager.GetCurrentClassLogger();
        }
        public Boolean AuthenticateAndLoad(String username, String password)
        {
            var result = false;
            try
            {
                var usersSection = ConfigurationManager.GetSection("basicUsers") as BasicUsersSection;
                if (usersSection != null)
                {
                    var items = usersSection.UsersItems.OfType<UserElement>().ToList();
                    var isExist = items.FirstOrDefault(x => x.UserName == username && x.Password == password);
                    if (isExist != null)
                        result = true; 
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return result;
        }
    }
}