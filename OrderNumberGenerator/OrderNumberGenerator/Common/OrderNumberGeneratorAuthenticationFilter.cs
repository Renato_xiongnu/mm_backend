﻿using System.Web.Http.Controllers;

namespace OrderNumberGenerator.Common
{
    public class OrderNumberGeneratorAuthenticationFilter: BasicAuthenticationFilter
    {

        public OrderNumberGeneratorAuthenticationFilter()
        { }

        public OrderNumberGeneratorAuthenticationFilter(bool active)
            : base(active)            
        { }


        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            var userBus = new UserBus();
            var isUser = userBus.AuthenticateAndLoad(username, password);
            return isUser;
        }
    }
}