﻿using System;
using System.Configuration;

namespace OrderNumberGenerator.Common
{
    
    public class BasicUsersSection: ConfigurationSection
    {
        [ConfigurationProperty("Users", IsRequired = true, IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(UsersCollection), AddItemName = "User")]
        public UsersCollection UsersItems
        {
            get { return ((UsersCollection)(base["Users"])); }
        }
    }

    [ConfigurationCollection(typeof(UserElement),AddItemName = "User")]
    public class UsersCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var userElement =((UserElement)( element ));
            return userElement;
        }

        public UserElement this[int idx]
        {
            get { return (UserElement)BaseGet(idx); }
        }
    }

    public class UserElement : ConfigurationElement
    {

        [ConfigurationProperty("userName", DefaultValue="", IsKey=true, IsRequired=true)]
        public String UserName
        {
            get {return ((string) (base["userName"]));}
            set{base["userName"] = value; }
        }

        [ConfigurationProperty( "password", DefaultValue = "", IsKey = false, IsRequired = false )]
        public string Password
        {
            get{return ( (string)( base[ "password" ] ) ); }
            set{base[ "password" ] = value; }
        }
    }
}