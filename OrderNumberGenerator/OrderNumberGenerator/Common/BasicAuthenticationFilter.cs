﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace OrderNumberGenerator.Common
{
  
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class BasicAuthenticationFilter : AuthorizationFilterAttribute
    {
        readonly bool _active = true;
        public BasicAuthenticationFilter()
        { }
      
        public BasicAuthenticationFilter(bool active)
        {
            _active = active;
        }
      
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (_active)
            {
                var identity = ParseAuthorizationHeader(actionContext);
                if (identity == null)
                {
                    Challenge(actionContext);
                    return;
                }
                if (!OnAuthorizeUser(identity.Name, identity.Password, actionContext))
                {
                    Challenge(actionContext);
                    return;
                }
                var principal = new GenericPrincipal(identity, null);
                Thread.CurrentPrincipal = principal;
                base.OnAuthorization(actionContext);
            }
        }
      
        protected virtual bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;
            return true;
        }
        
        protected virtual BasicAuthenticationIdentity ParseAuthorizationHeader(HttpActionContext actionContext)
        {
            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;
            if (auth != null && auth.Scheme == "Basic")
                authHeader = auth.Parameter;
            if (string.IsNullOrEmpty(authHeader))
                return null;
            authHeader = Encoding.Default.GetString(Convert.FromBase64String(authHeader));
          
            var idx = authHeader.IndexOf(':');
            if (idx < 0)
                return null;
            var username = authHeader.Substring(0, idx);
            var password = authHeader.Substring(idx + 1);
            return new BasicAuthenticationIdentity(username, password);
        }

      
        static void Challenge(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }
    }
}