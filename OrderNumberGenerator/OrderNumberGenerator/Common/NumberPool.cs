﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NumbersGenerator;

namespace OrderNumberGenerator.Common
{
    public sealed class NumberPoolSingleton
    {
        private ConcurrentBag<String> _numbers;
        private String _numberBase = "00000";
        private String path = @"c:\MyTestIds.txt";

        private static readonly Lazy<NumberPoolSingleton> Lazy = new Lazy<NumberPoolSingleton>(() => new NumberPoolSingleton());

        public static NumberPoolSingleton Instance { get { return Lazy.Value; } }

        private NumberPoolSingleton()
        {
            InitPool();
        }

        public String GetNumber()
        {
            if (_numbers.IsEmpty)
                InitPool();
            var item = String.Empty;
            var isPeek = false;
            while (!isPeek)
            {
                isPeek = _numbers.TryTake(out item);
            }
            return item;

        }

        private void InitPool()
        {
            //using (var db = new NumberBaseDbContext())
            //{
            //    var numBase = db.NumberBases.FirstOrDefault(x => !x.IsUsed);
            //    if (numBase == null)
            //        throw new Exception("Number is end");
            //    numBase.IsUsed = true;
            //    db.SaveChanges();
            //    _numberBase = numBase.Number;
            //}
            var items = File.ReadAllLines(path).Where(arg => !String.IsNullOrWhiteSpace(arg)).ToList();
            _numberBase = items[0];
            items.RemoveAt(0);
            File.WriteAllLines(path,items);


            _numbers = new ConcurrentBag<String>(GenerateNumbers(3).Select(x => String.Format("{0}{1}", _numberBase, x)));
        }

        private static IEnumerable<String> GenerateNumbers(Int32 itemLenght)
        {
            var values = new List<Int32> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var allVariations = new Variations<Int32>(values, itemLenght, GenerateOption.WithRepetition);
            var list = allVariations.Select(x => String.Join("", x));
            return list.Shuffle();
        }
    } 
}