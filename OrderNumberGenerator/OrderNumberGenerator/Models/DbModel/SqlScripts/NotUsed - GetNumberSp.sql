﻿USE [OrdersNumbers]
GO
/****** Object:  StoredProcedure [dbo].[GetNumber]    Script Date: 12/03/2014 10:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetNumber]
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRAN
DECLARE @Group NVARCHAR(8);
--DECLARE @Number NVARCHAR(8);
DECLARE @MyTableVar TABLE( Number NVARCHAR(50));

DECLARE @CurrentDate DATETIME = GETUTCDATE()

UPDATE TOP (1) Numbers
SET IsUsed = 1
OUTPUT deleted.Number
INTO @MyTableVar
WHERE IsUsed = 0

IF (SELECT Number FROM @MyTableVar) IS NULL
BEGIN
    UPDATE Groups SET
        IsActive = 0,
        IsUsed = 1,
        LastUpdate = @CurrentDate
    WHERE IsActive = 1
    
    UPDATE Groups SET
        IsActive = 1,
        LastUpdate = @CurrentDate
    WHERE Id = (SELECT TOP 1 Id
                    FROM Groups
                    WHERE IsUsed = 0)
    
    UPDATE Numbers SET
        IsUsed = 0    

    UPDATE TOP (1) Numbers
    SET IsUsed = 1
    OUTPUT deleted.Number
    INTO @MyTableVar
    WHERE IsUsed = 0
    --SELECT TOP (1) @Number = Number
    --FROM Numbers
    --WHERE IsUsed = 0
END

SELECT @Group = Number FROM Groups WHERE IsActive = 1

SELECT @Group + Number FROM @MyTableVar
COMMIT
END