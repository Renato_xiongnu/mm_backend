﻿USE [master]
GO

/****** Object:  Database [OrdersNumbers]    Script Date: 12/04/2014 10:20:59 ******/
CREATE DATABASE [OrdersNumbers] 
GO

SET QUOTED_IDENTIFIER OFF;
GO
USE [OrdersNumbers];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO


/****** Object:  Login [OrderNumberLogin]    Script Date: 12/16/2014 16:14:29 ******/
CREATE LOGIN [OrderNumberLogin] WITH PASSWORD=N'Rep@rting1', DEFAULT_DATABASE=[OrdersNumbers], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

CREATE USER [OrderNumberLogin] FOR LOGIN [OrderNumberLogin] WITH DEFAULT_SCHEMA=[dbo]
GO

EXEC sp_addrolemember N'db_datareader', N'OrderNumberLogin'
GO
EXEC sp_addrolemember N'db_datawriter', N'OrderNumberLogin'
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Groups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Groups];
GO
IF OBJECT_ID(N'[dbo].[Numbers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Numbers];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Groups'
CREATE TABLE [dbo].[Groups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] nvarchar(8)  NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsUsed] bit  NOT NULL,
    [LastUpdate] datetime  NULL
);
GO

-- Creating table 'Numbers'
CREATE TABLE [dbo].[Numbers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] nvarchar(8)  NOT NULL,
    [IsUsed] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

---- Creating primary key on [Id] in table 'Groups'
--ALTER TABLE [dbo].[Groups]
--ADD CONSTRAINT [PK_Groups]
--    PRIMARY KEY CLUSTERED ([Id] ASC);
--GO

---- Creating primary key on [Id] in table 'Numbers'
--ALTER TABLE [dbo].[Numbers]
--ADD CONSTRAINT [PK_Numbers]
--    PRIMARY KEY CLUSTERED ([Id] ASC);
--GO

SET QUOTED_IDENTIFIER ON;
GO

/****** Object:  Index [FI_Groups_IsActive]    Script Date: 12/04/2014 16:10:38 ******/
CREATE UNIQUE NONCLUSTERED INDEX [FI_Groups_IsActive] ON [dbo].[Groups] 
(
	[Number] ASC
)
WHERE ([IsActive]=(1))
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Index [CI_Groups_IsUsedId]    Script Date: 12/04/2014 16:18:29 ******/
CREATE UNIQUE CLUSTERED INDEX [CI_Groups_IsUsedId] ON [dbo].[Groups] 
(
	[IsUsed] ASC,
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


CREATE UNIQUE CLUSTERED INDEX [CI_Numbers_IsUsedNumber] ON [dbo].[Numbers] 
(
	[IsUsed] ASC,
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------