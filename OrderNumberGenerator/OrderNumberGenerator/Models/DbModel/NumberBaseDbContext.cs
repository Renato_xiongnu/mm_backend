﻿namespace OrderNumberGenerator.Models.DbModel
{
    using System.Data.Entity;

    public partial class NumberBaseDbContext : DbContext
    {
        public NumberBaseDbContext()
            : base("name=NumberGeneratorConnection")
        {
        }

        public virtual DbSet<NumberBase> NumberBases { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
   
}