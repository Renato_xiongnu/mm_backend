﻿namespace OrderNumberGenerator.Models.DbModel
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NumberBase")]
    public partial class NumberBase
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }


        [Column(Order = 1)]
        [StringLength(10)]
        public string Number { get; set; }

        [Column(Order = 2)]
        public bool IsUsed { get; set; }
    }
}