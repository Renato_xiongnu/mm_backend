﻿using System;
using System.Collections.Generic;

namespace NumbersGenerator
{ 
    public class Variations<T> : IMetaCollection<T>
    {
        #region Constructors

      
        protected Variations()
        { }

      
        public Variations(IEnumerable<T> values, int lowerIndex)
        {
            Initialize(values, lowerIndex, GenerateOption.WithoutRepetition);
        }

      
        public Variations(IEnumerable<T> values, int lowerIndex, GenerateOption type)
        {
            Initialize(values, lowerIndex, type);
        }

        #endregion

        #region IEnumerable Interface

  
        public IEnumerator<IList<T>> GetEnumerator()
        {
            if (Type == GenerateOption.WithRepetition)
            {
                return new EnumeratorWithRepetition(this);
            }
            return new EnumeratorWithoutRepetition(this);
        }

      
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (Type == GenerateOption.WithRepetition)
            {
                return new EnumeratorWithRepetition(this);
            }
            return new EnumeratorWithoutRepetition(this);
        }

        #endregion

        #region Enumerator Inner Class

     
        public class EnumeratorWithRepetition : IEnumerator<IList<T>>
        {

            #region Constructors

          
            public EnumeratorWithRepetition(Variations<T> source)
            {
                _myParent = source;
                Reset();
            }

            #endregion

            #region IEnumerator interface

          
            public void Reset()
            {
                _myCurrentList = null;
                _myListIndexes = null;
            } 
           
            public bool MoveNext()
            {
                int carry = 1;
                if (_myListIndexes == null)
                {
                    _myListIndexes = new List<int>();
                    for (var i = 0; i < _myParent.LowerIndex; ++i)
                    {
                        _myListIndexes.Add(0);
                    }
                    carry = 0;
                }
                else
                {
                    for (var i = _myListIndexes.Count - 1; i >= 0 && carry > 0; --i)
                    {
                        _myListIndexes[i] += carry;
                        carry = 0;
                        if (_myListIndexes[i] >= _myParent.UpperIndex)
                        {
                            _myListIndexes[i] = 0;
                            carry = 1;
                        }
                    }
                }
                _myCurrentList = null;
                return carry != 1;
            }

            public IList<T> Current
            {
                get
                {
                    ComputeCurrent();
                    return _myCurrentList;
                }
            }

        
            object System.Collections.IEnumerator.Current
            {
                get
                {
                    ComputeCurrent();
                    return _myCurrentList;
                }
            }

          
            public void Dispose()
            {
            }

            #endregion

            #region Heavy Lifting Members

           
            private void ComputeCurrent()
            {
                if (_myCurrentList == null)
                {
                    _myCurrentList = new List<T>();
                    foreach (var index in _myListIndexes)
                    {
                        _myCurrentList.Add(_myParent._myValues[index]);
                    }
                }
            }

            #endregion

            #region Data

          
            private readonly Variations<T> _myParent;

            private List<T> _myCurrentList;

         
            private List<int> _myListIndexes;

            #endregion
        }

    
        public class EnumeratorWithoutRepetition : IEnumerator<IList<T>>
        {

            #region Constructors

         
            public EnumeratorWithoutRepetition(Variations<T> source)
            {
                _myParent = source;
                _myPermutationsEnumerator = (Permutations<int>.Enumerator)_myParent._myPermutations.GetEnumerator();
            }

            #endregion

            #region IEnumerator interface

         
            public void Reset()
            {
                _myPermutationsEnumerator.Reset();
            }

          
            public bool MoveNext()
            {
                var ret = _myPermutationsEnumerator.MoveNext();
                _myCurrentList = null;
                return ret;
            }

       
            public IList<T> Current
            {
                get
                {
                    ComputeCurrent();
                    return _myCurrentList;
                }
            }

        
            object System.Collections.IEnumerator.Current
            {
                get
                {
                    ComputeCurrent();
                    return _myCurrentList;
                }
            }

          
            public void Dispose()
            {  }

            #endregion

            #region Heavy Lifting Members

         
            private void ComputeCurrent()
            {
                if (_myCurrentList == null)
                {
                    _myCurrentList = new List<T>();
                    int index = 0;
                    var currentPermutation = (IList<int>)_myPermutationsEnumerator.Current;
                    for (int i = 0; i < _myParent.LowerIndex; ++i)
                    {
                        _myCurrentList.Add(_myParent._myValues[0]);
                    }
                    for (var i = 0; i < currentPermutation.Count; ++i)
                    {
                        int position = currentPermutation[i];
                        if (position != Int32.MaxValue)
                        {
                            _myCurrentList[position] = _myParent._myValues[index];
                            if (_myParent.Type == GenerateOption.WithoutRepetition)
                            {
                                ++index;
                            }
                        }
                        else
                        {
                            ++index;
                        }
                    }
                }
            }

            #endregion

            #region Data

      
            private readonly Variations<T> _myParent;

            private List<T> _myCurrentList;

         
            private readonly Permutations<int>.Enumerator _myPermutationsEnumerator;

            #endregion
        }
        #endregion

        #region IMetaList Interface

        public long Count
        {
            get
            {
                if (Type == GenerateOption.WithoutRepetition)
                {
                    return _myPermutations.Count;
                }
                return (long)Math.Pow(UpperIndex, LowerIndex);
            }
        }

    
        public GenerateOption Type
        {
            get
            {
                return _myMetaCollectionType;
            }
        }

     
        public int UpperIndex
        {
            get
            {
                return _myValues.Count;
            }
        }


        public int LowerIndex { get; private set; }

        #endregion

        #region Heavy Lifting Members

  
        private void Initialize(IEnumerable<T> values, int lowerIndex, GenerateOption type)
        {
            _myMetaCollectionType = type;
            LowerIndex = lowerIndex;
            _myValues = new List<T>();
            _myValues.AddRange(values);
            if (type == GenerateOption.WithoutRepetition)
            {
                var myMap = new List<int>();
                var index = 0;
                for (var i = 0; i < _myValues.Count; ++i)
                {
                    myMap.Add(i >= _myValues.Count - LowerIndex ? index++ : Int32.MaxValue);
                }
                _myPermutations = new Permutations<int>(myMap);
            }
            else
            { }
        }

        #endregion

        #region Data 
     
        private List<T> _myValues;

        private Permutations<int> _myPermutations;

        private GenerateOption _myMetaCollectionType;

        #endregion
    }
}
