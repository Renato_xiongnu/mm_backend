﻿using System.Collections.Generic;

namespace NumbersGenerator
{
    interface IMetaCollection<T> : IEnumerable<IList<T>>
    {
        
        long Count { get; } 
    
        GenerateOption Type { get; } 
      
        int UpperIndex { get; }

        int LowerIndex { get; }
    }
}
