﻿using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;
using Orders.Backend.StockLocationSelector.Dal.Entities;


namespace Orders.Backend.StockLocationSelector.Dal
{
    public class SeleSelectorDbContext : DbContext
    {
        public SeleSelectorDbContext() : base("SelectorDbContext") { }

        public virtual IDbSet<StockLocation> StockLocations { get; set; }

        public virtual IDbSet<SaleLocation> SaleLocations { get; set; }

        public virtual IDbSet<AvailableStock> AvailableStocks { get; set; }

        public virtual IDbSet<StockLocationOutcome> StockLocationOutcomes { get; set; }

        public virtual IDbSet<UsedDeliveryResource> UsedDeliveryResources { get; set; } 



        public new void SaveChanges()
        {
            try
            { 
                base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder("Entity Validation Failed - errors follow:\n");

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(sb.ToString(), ex.EntityValidationErrors);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SaleLocation>().ToTable("SaleLocations");
            modelBuilder.Entity<StockLocation>().ToTable("StockLocations");
            base.OnModelCreating(modelBuilder);
        }
    }
}
