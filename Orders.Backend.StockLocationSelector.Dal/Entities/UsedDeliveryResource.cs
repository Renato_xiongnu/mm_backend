﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.StockLocationSelector.Dal.Entities
{
    public class UsedDeliveryResource
    {
        [Key]
        public Int32 Id { get; set; }

        [Required]
        public virtual SaleLocation SaleLocation { get; set; }

        [Required]
        public virtual StockLocation StockLocation { get; set; }

        public Double? UsedResource { get; set; }

        public DateTime DeliveryDate { get; set; }
    }
}
