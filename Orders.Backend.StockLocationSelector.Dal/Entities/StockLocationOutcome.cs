﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orders.Backend.StockLocationSelector.Dal.Entities
{
    [Table("StockLocationOutcome")]
    public class StockLocationOutcome
    {
        [Key]
        public Int32 Id { get; set; }

        [Required]
        public String OrderId { get; set; }

        [Required]
        public virtual StockLocation StockLocation { get; set; }

        [Required]
        public virtual SaleLocation SaleLocation { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public String Outcome { get; set; }

        public String Performer { get; set; }
    }
}
