﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.StockLocationSelector.Dal.Entities
{
    public abstract class LocationBase
    {
        [Key]
        [MaxLength(100)]
        [Display(Name = "Код магазина")]
        public String SapCode { get; set; }

        [Display(Name = "Название магазина")]
        public String Name { get; set; }

        [Display(Name = "Активный")]
        public Boolean IsActive { get; set; }

        [Display(Name = "Склад")]
        public Boolean IsHub { get; set; }
    }
}
