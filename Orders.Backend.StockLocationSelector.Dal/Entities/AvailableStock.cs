﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Orders.Backend.StockLocationSelector.Dal.Entities
{
    [Table("AvailableStocks")] 
    public class AvailableStock
    {
        [Key]
        public Int32 Id { get; set; }

        [Required]
        public virtual SaleLocation SaleLocation { get; set; }

        [Required]
        public virtual StockLocation StockLocation { get; set; } 

        [Required]
        [Display(Name = "Рейтинг")]
        public Int32 Rate { get; set; }

        public Int64? AvailableShippingMethods { get; set; }

        [Display(Name = "Пропускная способность магазина")]
        public Double? Capacity { get; set; }
    }
}
