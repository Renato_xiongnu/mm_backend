﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orders.Backend.StockLocationSelector.Dal.Entities
{
    [Table("SaleLocations")]
    public class SaleLocation: LocationBase
    {
        public Int16? StockLocationSelectorStrategy { get; set; }
    }
}
