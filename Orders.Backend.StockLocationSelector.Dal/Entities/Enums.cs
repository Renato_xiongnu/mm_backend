﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Orders.Backend.StockLocationSelector.Dal.Entities
{
    [DataContract]
    public enum StockLocationSelectorStrategyType: short
    { 
        [EnumMember]
        [Description("Выбор по рейтингу")]
        [Display(Name = "Выбор по рейтингу")]
        Rating,

        [EnumMember]
        [Description("Выбор по пропускной способности")]
        [Display(Name="Выбор по пропускной способности")]
        Сapacity,
    }

    [Flags]
    [DataContract]
    public enum StockShippingMethod: long
    {
        /// <summary>
        /// Самовывоз из магазина
        /// </summary>
        [EnumMember]
        [Description("Самовывоз из магазина")]
        PickupShop = 2,

        /// <summary>
        /// Самовывоз из постаматов
        /// </summary>
        [EnumMember]
        [Description("Самовывоз из постаматов")]
        PickupExternal = 4,

        /// <summary>
        /// Доставка
        /// </summary>
        [EnumMember]
        [Description("Доставка")]
        Delivery = 8
    }
}
