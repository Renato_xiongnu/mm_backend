namespace Orders.Backend.StockLocationSelector.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCapacityStockSelectorStrategyUsedDeliveryResource : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsedDeliveryResources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UsedResource = c.Double(),
                        DeliveryDate = c.DateTime(nullable: false),
                        SaleLocation_SapCode = c.String(nullable: false, maxLength: 100),
                        StockLocation_SapCode = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SaleLocations", t => t.SaleLocation_SapCode, cascadeDelete: true)
                .ForeignKey("dbo.StockLocations", t => t.StockLocation_SapCode, cascadeDelete: true)
                .Index(t => t.SaleLocation_SapCode)
                .Index(t => t.StockLocation_SapCode);
            
            AddColumn("dbo.AvailableStocks", "Capacity", c => c.Double());
            AddColumn("dbo.SaleLocations", "StockLocationSelectorStrategy", c => c.Short());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsedDeliveryResources", "StockLocation_SapCode", "dbo.StockLocations");
            DropForeignKey("dbo.UsedDeliveryResources", "SaleLocation_SapCode", "dbo.SaleLocations");
            DropIndex("dbo.UsedDeliveryResources", new[] { "StockLocation_SapCode" });
            DropIndex("dbo.UsedDeliveryResources", new[] { "SaleLocation_SapCode" });
            DropColumn("dbo.SaleLocations", "StockLocationSelectorStrategy");
            DropColumn("dbo.AvailableStocks", "Capacity");
            DropTable("dbo.UsedDeliveryResources");
        }
    }
}
