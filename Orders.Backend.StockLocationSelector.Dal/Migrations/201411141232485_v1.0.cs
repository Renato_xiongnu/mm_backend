namespace Orders.Backend.StockLocationSelector.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AvailableStocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rate = c.Int(nullable: false),
                        AvailableShippingMethods = c.Long(),
                        SaleLocation_SapCode = c.String(nullable: false, maxLength: 100),
                        StockLocation_SapCode = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SaleLocations", t => t.SaleLocation_SapCode, cascadeDelete: true)
                .ForeignKey("dbo.StockLocations", t => t.StockLocation_SapCode, cascadeDelete: true)
                .Index(t => t.SaleLocation_SapCode)
                .Index(t => t.StockLocation_SapCode);
            
            CreateTable(
                "dbo.SaleLocations",
                c => new
                    {
                        SapCode = c.String(nullable: false, maxLength: 100),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsHub = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SapCode);
            
            CreateTable(
                "dbo.StockLocations",
                c => new
                    {
                        SapCode = c.String(nullable: false, maxLength: 100),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsHub = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SapCode);
            
            CreateTable(
                "dbo.StockLocationOutcome",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.String(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Outcome = c.String(nullable: false),
                        Performer = c.String(),
                        SaleLocation_SapCode = c.String(nullable: false, maxLength: 100),
                        StockLocation_SapCode = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SaleLocations", t => t.SaleLocation_SapCode, cascadeDelete: true)
                .ForeignKey("dbo.StockLocations", t => t.StockLocation_SapCode, cascadeDelete: true)
                .Index(t => t.SaleLocation_SapCode)
                .Index(t => t.StockLocation_SapCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StockLocationOutcome", "StockLocation_SapCode", "dbo.StockLocations");
            DropForeignKey("dbo.StockLocationOutcome", "SaleLocation_SapCode", "dbo.SaleLocations");
            DropForeignKey("dbo.AvailableStocks", "StockLocation_SapCode", "dbo.StockLocations");
            DropForeignKey("dbo.AvailableStocks", "SaleLocation_SapCode", "dbo.SaleLocations");
            DropIndex("dbo.StockLocationOutcome", new[] { "StockLocation_SapCode" });
            DropIndex("dbo.StockLocationOutcome", new[] { "SaleLocation_SapCode" });
            DropIndex("dbo.AvailableStocks", new[] { "StockLocation_SapCode" });
            DropIndex("dbo.AvailableStocks", new[] { "SaleLocation_SapCode" });
            DropTable("dbo.StockLocationOutcome");
            DropTable("dbo.StockLocations");
            DropTable("dbo.SaleLocations");
            DropTable("dbo.AvailableStocks");
        }
    }
}
