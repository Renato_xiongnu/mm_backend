﻿using System;

namespace Orders.Backend.StockLocationSelector.Dal
{
    public abstract class DbServiceBase
    {
        private static ContextFactory _contexеFactory;
      

        public static void SetContextFactory(ContextFactory contextFactory)
        {
            if (contextFactory == null)
                contextFactory = new ContextFactory();
            _contexеFactory = contextFactory;

        }
                                                 
        protected T WithDb<T>(Func<SeleSelectorDbContext, T> workingWithDb)
        {
            if(_contexеFactory == null)
                _contexеFactory = new ContextFactory();
            T ret;
            using (var dbContext = _contexеFactory.Create())
            {
                ret = workingWithDb(dbContext);
            }
            return ret;
        }

        protected void WithDb(Action<SeleSelectorDbContext> workingWithDb)
        {
            if (_contexеFactory == null)
                _contexеFactory = new ContextFactory();
            using (var dbContext = _contexеFactory.Create())
            {
                workingWithDb(dbContext);
            }
        }
    }


    public class ContextFactory
    {
        public virtual SeleSelectorDbContext Create()
        {
            return new SeleSelectorDbContext();
        }
    }
}
