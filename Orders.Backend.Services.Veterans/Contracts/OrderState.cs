﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Orders.Backend.Services.Veterans.Contracts
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OrderState
    {
        [JsonProperty(PropertyName = "unknown")]
        [EnumMember(Value = "unknown")]
        Unknown,

	    [JsonProperty(PropertyName = "created")]
        [EnumMember(Value = "created")]
        Created,
        [JsonProperty(PropertyName = "shipped")]
        [EnumMember(Value = "shipped")]
        Shipped,
        
        [JsonProperty(PropertyName = "paid")]
        [EnumMember(Value = "paid")]
        Paid,

        [JsonProperty(PropertyName = "canceled")]
        [EnumMember(Value = "canceled")]
        Canceled,        
    }
}
