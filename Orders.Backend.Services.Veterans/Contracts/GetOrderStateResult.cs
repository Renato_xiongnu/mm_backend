﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Orders.Backend.Services.Veterans.Contracts
{
    [DataContract]
    public class GetStateResult : OperationResult
    {
        [DataMember]
        [JsonProperty(PropertyName = "state")]
        public OrderState OrderState { get; set; }
    }
}