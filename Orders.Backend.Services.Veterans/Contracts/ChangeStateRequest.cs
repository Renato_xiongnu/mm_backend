﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Orders.Backend.Services.Veterans.Contracts
{
    [DataContract]
    public class ChangeStateRequest
    {
        [DataMember]
        [JsonProperty(PropertyName = "order_id")]
        public string OrderId { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "state")]
        public OrderState OrderState { get; set; }
    }
}