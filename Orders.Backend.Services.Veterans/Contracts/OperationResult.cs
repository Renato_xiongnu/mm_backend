﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Orders.Backend.Services.Veterans.Contracts
{
    [DataContract]
    public class OperationResult
    {
        [DataMember]
        [JsonProperty(PropertyName = "status")]
        public Status Status { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "error")]
        public string ErrorCode { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "error_message")]
        public string ErrorMessage { get; set; }
    }
}