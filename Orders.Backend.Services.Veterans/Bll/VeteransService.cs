﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Common.Logging;
using Newtonsoft.Json;
using Orders.Backend.Services.Veterans.Contracts;
using Orders.Backend.Services.Veterans.Properties;

namespace Orders.Backend.Services.Veterans.Bll
{
    public class VeteransService : IVeteransService
    {
        private readonly ErrorsProvider _errorsProvider;

        private readonly ILog _logger;

        public VeteransService(ErrorsProvider errorsProvider, ILog logger)
        {
            _errorsProvider = errorsProvider;
            _logger = logger;
        }

        public ChangeStateResult ChangeState(ChangeStateRequest request)
        {
            _logger.InfoFormat("{0} : Begin change status to {1}", request.OrderId, request.OrderState);
            var result = new ChangeStateResult();
            try
            {
                var getStateResult = GetState(request.OrderId);
                if(getStateResult.Status != Status.Ok)
                {
                    result.Status = getStateResult.Status;
                    result.ErrorCode = getStateResult.ErrorCode;
                    result.ErrorMessage = getStateResult.ErrorMessage;
                    return result;
                }
                if(getStateResult.OrderState == request.OrderState)
                {
                    result.Status = Status.Ok;
                    return result;
                }
                using(var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Settings.Default.VeteransWebApiBaseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var serializeObject = JsonConvert.SerializeObject(request);
                    var stringContent = new StringContent(serializeObject);
                    stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var response =
                        client.PostAsync(Settings.Default.VeteransWebApiChangeStateMethod, stringContent).Result;
                    if(!response.IsSuccessStatusCode)
                    {
                        result.Status = Status.Exception;
                        result.ErrorMessage = response.ReasonPhrase;
                    }
                    else
                    {
                        result = response.Content.ReadAsAsync<ChangeStateResult>().Result;
                        if(result.Status != Status.Ok)
                        {
                            var errorInfo = _errorsProvider.GetErrorInfoByCode(result.ErrorCode);
                            if(errorInfo != null)
                            {
                                result.ErrorMessage = string.Format("{0} ({1})", errorInfo.ShortDescription,
                                    errorInfo.Code);
                            }
                            else
                            {
                                result.Status = Status.Exception;
                                result.ErrorMessage = string.Format("{0} ({1})", "Неизвестная ошибка",
                                    result.ErrorCode);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                result.Status = Status.Exception;
                result.ErrorMessage = ex.Message;
                _logger.ErrorFormat(
                    "{0} : Complete change status to {1}. Operation result Status = {2} ; ErrorCode = {3} ; ErrorMessage = {4}",
                    ex,
                    request.OrderId,
                    request.OrderState,
                    result.Status,
                    result.ErrorCode,
                    result.ErrorMessage);
            }
            _logger.InfoFormat(
                "{0} : Complete change status to {1}. Operation result Status = {2} ; ErrorCode = {3} ; ErrorMessage = {4}",
                request.OrderId,
                request.OrderState,
                result.Status,
                result.ErrorCode,
                result.ErrorMessage);
            return result;
        }

        public GetStateResult GetState(string orderId)
        {
            _logger.InfoFormat("{0} : Begin get status", orderId);
            var result = new GetStateResult();
            try
            {
                using(var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Settings.Default.VeteransWebApiBaseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var serializeObject = JsonConvert.SerializeObject(new
                    {
                        order_id = orderId
                    });
                    var stringContent = new StringContent(serializeObject);
                    stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var response =
                        client.PostAsync(Settings.Default.VeteransWebApiGetStateMethod, stringContent).Result;
                    if(!response.IsSuccessStatusCode)
                    {
                        result.Status = Status.Exception;
                        result.ErrorMessage = response.ReasonPhrase;
                    }
                    else
                    {
                        result = response.Content.ReadAsAsync<GetStateResult>().Result;
                        if(result.Status != Status.Ok)
                        {
                            var errorInfo = _errorsProvider.GetErrorInfoByCode(result.ErrorCode);
                            if(errorInfo != null)
                            {
                                result.ErrorMessage = errorInfo.ShortDescription;
                            }
                            else
                            {
                                result.Status = Status.Exception;
                                result.ErrorMessage = "Неизвестная ошибка";
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                result.Status = Status.Exception;
                result.ErrorMessage = ex.Message;
                _logger.ErrorFormat(
                    "{0} : Complete get status.New status = {1} Operation result Status = {2} ; ErrorCode = {3} ; ErrorMessage = {4}",
                    ex,
                    orderId,
                    result.OrderState,
                    result.Status,
                    result.ErrorCode,
                    result.ErrorMessage);
            }
            _logger.InfoFormat(
                "{0} : Complete get status.Current status = {1} Operation result Status = {2} ; ErrorCode = {3} ; ErrorMessage = {4}",
                orderId,
                result.OrderState,
                result.Status,
                result.ErrorCode,
                result.ErrorMessage);
            return result;
        }
    }
}