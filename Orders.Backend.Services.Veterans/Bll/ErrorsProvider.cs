﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace Orders.Backend.Services.Veterans.Bll
{
    public class ErrorsProvider
    {
        private readonly Dictionary<string,ErrorInfo> _errors = new Dictionary<string, ErrorInfo>();

        public ErrorsProvider(string errorsPath = @"Data\Errors.txt")
        {
            var errorsAbsolutePath = HttpContext.Current.Server.MapPath(errorsPath);                        
            if (!File.Exists(errorsAbsolutePath))
            {
                throw new ArgumentException("errorsPath");
            }
            var lines = File.ReadAllLines(errorsAbsolutePath);
            for(int i = 0; i+2 < lines.Length; i+=3)
            {
                var errorInfo = new ErrorInfo
                {
                    Code = lines[i],
                    ShortDescription = lines[i + 1],
                    FullDescription = lines[i + 2],
                };
                _errors[errorInfo.Code] = errorInfo;
            }
        }

        public ErrorInfo GetErrorInfoByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }
            if (_errors.ContainsKey(code))
            {
                return _errors[code];
            }
            return null;
        }
    }

    public class ErrorInfo
    {
        public string Code { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }
    }
}