﻿using System.ServiceModel;
using Orders.Backend.Services.Veterans.Contracts;

namespace Orders.Backend.Services.Veterans
{
    [ServiceContract]
    public interface IVeteransService
    {
        [OperationContract]
        ChangeStateResult ChangeState(ChangeStateRequest request);

        [OperationContract]
        GetStateResult GetState(string orderId);
    }    
}
