﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Common.Logging;
using Orders.Backend.Services.Veterans.Bll;
using Orders.Backend.Services.Veterans.Contracts;

namespace Orders.Backend.Services.Veterans.Mocks
{
    public class VeteransServiceMock : IVeteransService
    {
        private readonly ErrorsProvider _errorsProvider;

        private readonly ILog _logger;

        private Dictionary<string, Tuple<OrderState, string>> _dictionary =
            new Dictionary<string, Tuple<OrderState, string>>();

        public VeteransServiceMock(ErrorsProvider errorsProvider, ILog logger)
        {
            _errorsProvider = errorsProvider;
            _logger = logger;
        }

        public ChangeStateResult ChangeState(ChangeStateRequest request)
        {
            _logger.InfoFormat("ChangeState {0} to {1}", request.OrderId, request.OrderState);
            _dictionary[request.OrderId] = new Tuple<OrderState, string>(request.OrderState,"");
            Status status;
            var statusConfig = ConfigurationManager.AppSettings["ProcessingResult"];
            var parse = Enum.TryParse(statusConfig, out status);
            if (!parse)
            {
                _logger.InfoFormat("Warning {0} can't be parsed. Returned Status.Ok", statusConfig);
            }
            return new ChangeStateResult
            {
                Status = parse ? status : Status.Ok,
                ErrorCode = ""
            };
        }

        public GetStateResult GetState(string orderId)
        {
            _logger.InfoFormat("GetState {0} to {1}", orderId);
            return new GetStateResult
            {
                Status = Status.Ok,
                OrderState = _dictionary.ContainsKey(orderId) 
                ? _dictionary[orderId].Item1 
                : OrderState.Unknown
            };
        }
    }
}