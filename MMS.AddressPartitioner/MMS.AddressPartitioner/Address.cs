﻿using System.Runtime.Serialization;

namespace MMS.AddressPartitioner
{
    [DataContract]
    public class Address
    {
        [DataMember]
        public string KLADR { get; set; }
        [DataMember]
        public string LocationId { get; set; }
        [DataMember]
        public string AddressPlain { get; set; }
        [DataMember]
        public string ZIPCode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Region { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string StreetAbbr { get; set; }
        [DataMember]
        public string Building1 { get; set; }
        [DataMember]
        public string BuildingExt { get; set; }
        [DataMember]
        public string Building2 { get; set; }
        [DataMember]
        public string Building3 { get; set; }
        [DataMember]
        public string Apartment { get; set; }
        [DataMember]
        public string AdditionalInstructions { get; set; }
        [DataMember]
        public string Entrance { get; set; }
        [DataMember]
        public string EntranceCode { get; set; }
        [DataMember]
        public int Floor { get; set; }
        [DataMember]
        public bool? HasServiceLift { get; set; }
        [DataMember]
        public string SubwayStationName { get; set; }
        [DataMember]
        public string District { get; set; }
    }
}