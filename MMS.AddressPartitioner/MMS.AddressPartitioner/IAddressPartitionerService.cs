﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace MMS.AddressPartitioner
{
    [ServiceContract]
    public interface IAddressPartitionerService
    {
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Address ParseAddress(string addressPlain);
    }
}
