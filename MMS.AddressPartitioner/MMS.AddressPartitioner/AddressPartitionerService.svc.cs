﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MMS.Dadata;
using MMS.Dadata.Contracts;

namespace MMS.AddressPartitioner
{
    public class AddressPartitionerService : IAddressPartitionerService
    {
        public Address ParseAddress(string addressPlain)
        {
            var streetAbbrCode = new Dictionary<string, string>
            {
                {"аллея", "ал"},
                {"бульвар", "б-р"},
                {"дорога", "дор"},
                {"квартал","кв-л"},
                {"линия","линия"},
                {"микрорайон", "мкр"},
                {"набережная", "наб"},
                {"переулок", "пер"},
                {"площадь", "пл"},
                {"проспект", "пр-кт"},
                {"проезд", "проезд"},
                {"станция", "станция"},
                {"территория","территория"},
                {"тракт", "тракт"},
                {"тупик","тупик"},
                {"улица", "ул"},
                {"шоссе", "ш"}
            };

            var client = new FactorClient();
            var customerInfo = client.VerifyCustomer(new CustomerRequest
            {
                Address = addressPlain
            });
            if (customerInfo.Succesfull)
            {
                var addressData = customerInfo.AddressData;

                var originalHouse = addressData.House ?? string.Empty;
                originalHouse = Regex.Replace(originalHouse, @"\\/", "/");
                var house = originalHouse;
                string houseExt = null;
                string building = null;
                string housing = null;
                var houseMath = Regex.Match(originalHouse, @"(ДОМ\s(?<House>[/\d\\-]+)(?<HouseExt>\S+)?)", RegexOptions.IgnoreCase);
                if (houseMath.Success)
                {
                    house = houseMath.Groups["House"].Value;
                    houseExt = houseMath.Groups["HouseExt"].Value;
                    originalHouse = originalHouse.Replace(houseMath.Value, string.Empty);
                }
                var buildingMath = Regex.Match(originalHouse, @"((СТРОЕНИЕ|СООРУЖЕНИЕ|ЛИТЕРА)\s(?<Building>\S+))", RegexOptions.IgnoreCase);
                if (buildingMath.Success)
                {
                    building = buildingMath.Groups["Building"].Value;
                    originalHouse = originalHouse.Replace(buildingMath.Value, string.Empty);
                }
                var housingMath = Regex.Match(originalHouse, @"(КОРПУС\s(?<Housing>\S+))", RegexOptions.IgnoreCase);
                if (housingMath.Success)
                {
                    housing = housingMath.Groups["Housing"].Value;
                    originalHouse = originalHouse.Replace(housingMath.Value, string.Empty);
                }
                var apartment = string.IsNullOrEmpty(addressData.Flat)
                    ? string.Empty
                    : Regex.Replace(addressData.Flat, "(ОФИС|КВАРТИРА)", "", RegexOptions.IgnoreCase).Trim();


                var address = new Address();
                if (addressData.RegionType.Equals("ГОРОД", StringComparison.InvariantCultureIgnoreCase))
                {
                    address.Region = string.Format("г {0}", addressData.Region);
                    address.City = addressData.Region;
                }
                else
                {
                    address.Region = addressData.Region;
                    address.City = addressData.City;
                }
                address.KLADR = addressData.Kladr;
                address.ZIPCode = addressData.PostalCode;
                address.StreetAbbr = streetAbbrCode.ContainsKey(addressData.StreetType.ToLower())
                    ? streetAbbrCode[addressData.StreetType.ToLower()]
                    : addressData.StreetType;
                address.Street = addressData.Street;
                address.AdditionalInstructions = originalHouse.Trim();
                address.Building1 = house;
                address.BuildingExt = houseExt;
                address.Building2 = housing;
                address.Building3 = building;
                address.AddressPlain = addressPlain;
                address.Apartment = apartment;

                return address;
            }
            else
            {
                throw new Exception("Dadata call failed");
            }
        }
    }
}