﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Common.Helpers;

using OrderContract = Orders.Backend.Services.Contracts.OrderService;
using GiftCertContract = Orders.Backend.Services.Contracts.GiftCertificate;
using BllOrder = Orders.Backend.Bll.Order;

namespace Orders.Backend.Services
{
    public static class ContractExtensions
    {
        public static BllOrder.DeliveryInfo ToBllObject(this OrderContract.DeliveryInfo contract)
        {
            return new BllOrder.DeliveryInfo()
            {
                City = contract.City,
                Address = contract.Address,
                HasDelivery = contract.HasDelivery
            };
        }

        public static BllOrder.CustomerInfo ToBllObject(this OrderContract.ClientData contract)
        {
            return new BllOrder.CustomerInfo()
            {
                Name = contract.Name,
                Surname = contract.Surname,
                Phone = contract.Phone,
                Email = contract.Email
            };
        }


        public static BllOrder.OrderLine ToBllObject(this OrderContract.ArticleData contract)
        {
            return new BllOrder.OrderLine()
            {
                ArticleNum = contract.ArticleNum,
                Price = contract.Price,
                Qty = contract.Qty
            };
        }


        public static BllOrder.OrderLine ToBllObject(this GiftCertContract.ItemData contract)
        {
            return new BllOrder.OrderLine()
            {
                ArticleNum = contract.Number,
                Price = contract.Price,
                Qty = contract.Qty,
                //LineType = contract.ItemType.ToBllObject()
                LineType = contract.ItemType.ConvertTo<BllOrder.LineType>()
            };
        }


        //public static BllOrder.LineType ToBllObject(this GiftCertContract.ItemType contract)
        //{
        //    BllOrder.LineType lineType = BllOrder.LineType.Item;

        //    switch (contract)
        //    {
        //        case GiftCertContract.ItemType.GiftCard:
        //            lineType = BllOrder.LineType.GiftCard;
        //            break;

        //        case GiftCertContract.ItemType.Item:
        //            lineType = BllOrder.LineType.Item;
        //            break;

        //        case GiftCertContract.ItemType.Set:
        //            lineType = BllOrder.LineType.Set;
        //            break;
        //    }

        //    return lineType;
        //}

    }
}