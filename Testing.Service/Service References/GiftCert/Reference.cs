﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Testing.Service.GiftCert {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetCertificatesForSlipsResult", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class GetCertificatesForSlipsResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Testing.Service.GiftCert.StoreCertificates[] ReserverCertificatesField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Testing.Service.GiftCert.StoreCertificates[] ReserverCertificates {
            get {
                return this.ReserverCertificatesField;
            }
            set {
                if ((object.ReferenceEquals(this.ReserverCertificatesField, value) != true)) {
                    this.ReserverCertificatesField = value;
                    this.RaisePropertyChanged("ReserverCertificates");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="StoreCertificates", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class StoreCertificates : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string[] CertificateIdsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SapCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string[] CertificateIds {
            get {
                return this.CertificateIdsField;
            }
            set {
                if ((object.ReferenceEquals(this.CertificateIdsField, value) != true)) {
                    this.CertificateIdsField = value;
                    this.RaisePropertyChanged("CertificateIds");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SapCode {
            get {
                return this.SapCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.SapCodeField, value) != true)) {
                    this.SapCodeField = value;
                    this.RaisePropertyChanged("SapCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetCertificatesForCreditingResult", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class GetCertificatesForCreditingResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Testing.Service.GiftCert.CertificateForCrediting[] CertificatesField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Testing.Service.GiftCert.CertificateForCrediting[] Certificates {
            get {
                return this.CertificatesField;
            }
            set {
                if ((object.ReferenceEquals(this.CertificatesField, value) != true)) {
                    this.CertificatesField = value;
                    this.RaisePropertyChanged("Certificates");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CertificateForCrediting", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class CertificateForCrediting : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal AmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CertificateIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Testing.Service.GiftCert.ChainType ChainField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SapCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Amount {
            get {
                return this.AmountField;
            }
            set {
                if ((this.AmountField.Equals(value) != true)) {
                    this.AmountField = value;
                    this.RaisePropertyChanged("Amount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CertificateId {
            get {
                return this.CertificateIdField;
            }
            set {
                if ((object.ReferenceEquals(this.CertificateIdField, value) != true)) {
                    this.CertificateIdField = value;
                    this.RaisePropertyChanged("CertificateId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Testing.Service.GiftCert.ChainType Chain {
            get {
                return this.ChainField;
            }
            set {
                if ((this.ChainField.Equals(value) != true)) {
                    this.ChainField = value;
                    this.RaisePropertyChanged("Chain");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SapCode {
            get {
                return this.SapCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.SapCodeField, value) != true)) {
                    this.SapCodeField = value;
                    this.RaisePropertyChanged("SapCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ChainType", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    public enum ChainType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        MediaMarkt = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Saturn = 1,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CertificateWithItemData", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class CertificateWithItemData : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CertificateIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Testing.Service.GiftCert.ItemData[] ItemDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CertificateId {
            get {
                return this.CertificateIdField;
            }
            set {
                if ((object.ReferenceEquals(this.CertificateIdField, value) != true)) {
                    this.CertificateIdField = value;
                    this.RaisePropertyChanged("CertificateId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Testing.Service.GiftCert.ItemData[] ItemData {
            get {
                return this.ItemDataField;
            }
            set {
                if ((object.ReferenceEquals(this.ItemDataField, value) != true)) {
                    this.ItemDataField = value;
                    this.RaisePropertyChanged("ItemData");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ItemData", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class ItemData : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Testing.Service.GiftCert.ItemType ItemTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal PriceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int QtyField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Testing.Service.GiftCert.ItemType ItemType {
            get {
                return this.ItemTypeField;
            }
            set {
                if ((this.ItemTypeField.Equals(value) != true)) {
                    this.ItemTypeField = value;
                    this.RaisePropertyChanged("ItemType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Number {
            get {
                return this.NumberField;
            }
            set {
                if ((object.ReferenceEquals(this.NumberField, value) != true)) {
                    this.NumberField = value;
                    this.RaisePropertyChanged("Number");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Price {
            get {
                return this.PriceField;
            }
            set {
                if ((this.PriceField.Equals(value) != true)) {
                    this.PriceField = value;
                    this.RaisePropertyChanged("Price");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Qty {
            get {
                return this.QtyField;
            }
            set {
                if ((this.QtyField.Equals(value) != true)) {
                    this.QtyField = value;
                    this.RaisePropertyChanged("Qty");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ItemType", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    public enum ItemType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Item = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Set = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        GiftCard = 2,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CreditedCertificate", Namespace="http://schemas.datacontract.org/2004/07/OnlineOrders.Services.Contracts.GiftCerti" +
        "ficate")]
    [System.SerializableAttribute()]
    public partial class CreditedCertificate : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal AmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CertificateIdField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Amount {
            get {
                return this.AmountField;
            }
            set {
                if ((this.AmountField.Equals(value) != true)) {
                    this.AmountField = value;
                    this.RaisePropertyChanged("Amount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CertificateId {
            get {
                return this.CertificateIdField;
            }
            set {
                if ((object.ReferenceEquals(this.CertificateIdField, value) != true)) {
                    this.CertificateIdField = value;
                    this.RaisePropertyChanged("CertificateId");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="GiftCert.IGiftCertService")]
    public interface IGiftCertService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGiftCertService/GetCertificatesForSlips", ReplyAction="http://tempuri.org/IGiftCertService/GetCertificatesForSlipsResponse")]
        Testing.Service.GiftCert.GetCertificatesForSlipsResult GetCertificatesForSlips();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGiftCertService/GetCertificatesForCrediting", ReplyAction="http://tempuri.org/IGiftCertService/GetCertificatesForCreditingResponse")]
        Testing.Service.GiftCert.GetCertificatesForCreditingResult GetCertificatesForCrediting();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGiftCertService/UpdateCertificatesWithItemData", ReplyAction="http://tempuri.org/IGiftCertService/UpdateCertificatesWithItemDataResponse")]
        void UpdateCertificatesWithItemData(string SapCode, Testing.Service.GiftCert.CertificateWithItemData[] articleData);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGiftCertService/CreditGiftCertificates", ReplyAction="http://tempuri.org/IGiftCertService/CreditGiftCertificatesResponse")]
        void CreditGiftCertificates(Testing.Service.GiftCert.CreditedCertificate[] succeededCreditedGiftCards, Testing.Service.GiftCert.CreditedCertificate[] failedCreditedGiftCards);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IGiftCertServiceChannel : Testing.Service.GiftCert.IGiftCertService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GiftCertServiceClient : System.ServiceModel.ClientBase<Testing.Service.GiftCert.IGiftCertService>, Testing.Service.GiftCert.IGiftCertService {
        
        public GiftCertServiceClient() {
        }
        
        public GiftCertServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public GiftCertServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GiftCertServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GiftCertServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Testing.Service.GiftCert.GetCertificatesForSlipsResult GetCertificatesForSlips() {
            return base.Channel.GetCertificatesForSlips();
        }
        
        public Testing.Service.GiftCert.GetCertificatesForCreditingResult GetCertificatesForCrediting() {
            return base.Channel.GetCertificatesForCrediting();
        }
        
        public void UpdateCertificatesWithItemData(string SapCode, Testing.Service.GiftCert.CertificateWithItemData[] articleData) {
            base.Channel.UpdateCertificatesWithItemData(SapCode, articleData);
        }
        
        public void CreditGiftCertificates(Testing.Service.GiftCert.CreditedCertificate[] succeededCreditedGiftCards, Testing.Service.GiftCert.CreditedCertificate[] failedCreditedGiftCards) {
            base.Channel.CreditGiftCertificates(succeededCreditedGiftCards, failedCreditedGiftCards);
        }
    }
}
