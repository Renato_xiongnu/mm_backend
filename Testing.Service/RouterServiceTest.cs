﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Service.Router;

namespace Testing.Service
{
    [TestClass]
    public class RouterServiceTest
    {
        public static readonly string OrderId = "006";

        [TestMethod]
        public void GenerateTestReserveContent()
        {
            var bytes1 = File.ReadAllBytes("C:\\Users\\sergey.prokofyev\\Documents\\" + "act1.emf");
            var bytes2 = File.ReadAllBytes("C:\\Users\\sergey.prokofyev\\Documents\\" + "act2.emf");
            var s1 = Convert.ToBase64String(bytes1);
            var s2 = Convert.ToBase64String(bytes2);
            var result = s1 + "|" + s2;
            Console.WriteLine("aaa");
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCashPreorderDelivery_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                    {
                        Payment = PaymentType.Cash,
                        IsPreorder = true,
                        HasDelivery = true
                    });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCashDeleiveryNoPreorder_ReturnsOK()
        {           
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    ClientName = "NameDDD",
                    Payment = PaymentType.Cash,
                    ClientEmail = "obradovich@media-saturn.com",
                    Phone = "+79031329846",
                    HasDelivery = true,
                    SapCode = "R002",
                   //PickupLocationId = "PuP-001011",
                    Articles = new List<ArticleData>
                    {
                        new ArticleData
                        {
                            ArticleNum = "1159035",
                            Price = 20000,
                            Qty = 10,
                        },
                    },
                });
            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok, createResult.ErrorMessage);
            Console.WriteLine(createResult.OrderId);
        }

        [TestMethod]
        public void RouterService_CreateYandexMarketOrderWithCashDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Source = OrderSource.WebSite,
                    Payment = PaymentType.Cash,
                    ClientEmail = "korolyov@media-saturn.com",
                    Phone = "+79031329846",
                    HasDelivery = false,
                    SapCode = "R002",
                    PickupLocationId = "PuP-001883",
                    Articles = new List<ArticleData>
                    {
                        new ArticleData
                        {
                            ArticleNum = "280000004",
                            Price = 15000,
                            Qty = 3,
                        },
                    },
                    Comment = "Comment",
                    IsTest = false,
                    CreatedBy = "Alexander"
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCashlessDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Payment = PaymentType.Online,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79031329846",
                    HasDelivery = true,
                    Articles = new List<ArticleData>
                    {
                        new ArticleData
                        {
                            ArticleNum = "1142669",
                            Price = 10000,
                            Qty = 1,
                        },
                    }, 
                    Comment="Comment",
                    IsTest = false,
                    CreatedBy="Alexander"
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCashlessWithoutDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Payment = PaymentType.Online,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79152654597",
                    HasDelivery = false,
                    PickupLocationId="pickup_R006"
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateMobileWebSiteQuickOrderWithCashlessWithoutDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Source = OrderSource.MobileWebSiteQuickOrder,
                    Payment = PaymentType.Online,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79152654597",
                    HasDelivery = false
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }


        [TestMethod]
        public void RouterService_CreateWebSiteQuickOrderWithCashlessDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Source = OrderSource.WebSiteQuickOrder,
                    Payment = PaymentType.Online,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79031329846",
                    HasDelivery = true
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }               

        [TestMethod]
        public void RouterService_CreateOrderWithCashNoPreorderDeleivery_ReturnsOK()
        {
            //var requests = Enumerable.Repeat(new ExecuteOrderRequest()
            //    {
            //        Payment = PaymentType.Cash,
            //        ClientEmail = "savinovnickita@gmail.com",
            //        Phone = "+79152654597",
            //        HasDelivery = false,
            //        ClientName = "ТЕСТ НЕ СОЗДАВАТЬ",
            //        Source = OrderSource.MobileWebSiteQuickOrder,
            //        PickupLocationId="pickup_R007"
            //    }, 1);

            //Parallel.ForEach(requests, request => ExecuteRequest(request));

            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                    {
                        Payment = PaymentType.Cash,
                        ClientEmail = "savinovnickita@gmail.com",
                        Phone = "+79152654597",
                        HasDelivery = false,
                        ClientName = "",
                        Source = OrderSource.CallCenter,
                        SapCode = "R002",
                        PickupLocationId = "pickup_R007"
                    });

            Console.WriteLine(createResult.ErrorMessage);
            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
            Console.WriteLine(createResult.OrderId);
        }

        [TestMethod]
        public void RouterService_CreateMobileIPhoneQuickOrderWithCashWithoutDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Source = OrderSource.IPhoneQuickOrder,
                    Payment = PaymentType.Cash,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79031329846",
                    SapCode = "R002",
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }


        [TestMethod]
        public void RouterService_CreateIPhoneQuickOrderWithCashDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Source = OrderSource.IPhoneQuickOrder,
                    Payment = PaymentType.Cash,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79031329846",
                    SapCode = "R002",
                    HasDelivery = true
                });
            System.Diagnostics.Debug.WriteLine("{0}", createResult.OrderId);
            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateMetroOrderWithCashNoPreorderDeleivery_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                    {
                        Payment = PaymentType.Cash,
                        ClientEmail = "savinovn@media-saturn.com",
                        Phone = "+79152654597",
                        HasDelivery = false,
                        ClientName = "MetroIvan",
                        Source = OrderSource.Metro,
                        ExternalSystem = ExternalSystem.ZZT,
                        PickupLocationId = "pickup_R006"
                    });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateMetroOrderWithCashDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                    {
                        Payment = PaymentType.Cash,
                        ClientEmail = "savinovn@media-saturn.com",
                        Phone = "+79152654597",
                        HasDelivery = true,
                        ClientName = "MetroIvan",
                        Source = OrderSource.Metro,
                        ExternalSystem = ExternalSystem.ZZT
                    });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCashNoPreorderDeleiveryOneClick_ReturnsOK()
        {
            var createResult =
                ExecuteRequestFromOneClick(new ExecuteMetroOrderRequest
                    {
                        Payment = PaymentType.Cash,
                        ClientEmail = "savinovn@media-saturn.com",
                        Phone = "+79152654597",
                        HasDelivery = false,
                        Source = OrderSource.Metro
                    });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCreditDeleiveryNoPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest {Payment = PaymentType.OnlineCredit, HasDelivery = true});

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithCreditNoDeleiveryPreorder_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                    {
                        Payment = PaymentType.OnlineCredit,
                        Source = OrderSource.CallCenter,
                        HasDelivery = false,
                        PickupLocationId = "pickup_R006"
                    });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_RejectOrder()
        {
            var client = new OrderServiceClient();
            var result = client.CancelOrder("007-528-535", "завершаем", "sergey");
        }

        [TestMethod]
        public void RouterService_CreateOrderWithSalesDocument_CreatesCorrectHeader()
        {
            var request = new ExecuteOrderRequest {NeedSalesDocument = true};

            var result = ExecuteRequest2(request);

            Assert.AreEqual(ReturnCode.Ok, result.ReturnCode);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithSalesDocument_CreatesCorrectInitData()
        {
            var request = new ExecuteOrderRequest
                {
                    NeedSalesDocument = true,
                    Source = OrderSource.Store,
                    Phone = "+79152654597",
                    HasDelivery = false
                };

            var result = ExecuteRequest2(request);

            Assert.AreEqual(ReturnCode.Ok, result.ReturnCode);
        }

        [TestMethod]
        public void RouterService_CreateAvitoOrder_CreatesCorrectInitData()
        {
            var request = new ExecuteOrderRequest
                {
                    Phone = "+79152654597",
                    HasDelivery = false
                };

            var result = ExecuteRequest3(request);

            Assert.AreEqual(ReturnCode.Ok, result.ReturnCode);
        }

        #region SocialCard
        
        [TestMethod]
        public void RouterService_CreateOrderWithSocialCard_Pickup_NoPrepay_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Payment = PaymentType.SocialCard,
                    Prepay = 0.0M,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79152654597",
                    HasDelivery = false,
                    PickupLocationId = "pickup_R006",
                    SocialCard = new CardInfo()
                    {
                        Number = "111111111"
                    }
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithSocialCard_Pickup_Prepay_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Payment = PaymentType.SocialCard,
                    Prepay = 100.0M,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79152654597",
                    HasDelivery = false,
                    PickupLocationId = "pickup_R006",
                    SocialCard = new CardInfo()
                    {
                        Number = "111111111"
                    }
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithSocialCard_Delivery_NoPrepay_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Payment = PaymentType.SocialCard,
                    Prepay = 0.0M,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79152654597",
                    SapCode = "R003",
                    HasDelivery = true,                    
                    SocialCard = new CardInfo
                    {
                        Number = "111111111"
                    }
                });

            Console.WriteLine(createResult.OrderId);

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
        }

        [TestMethod]
        public void RouterService_CreateOrderWithSocialCard_Delivery_Prepay_ReturnsOK()
        {
            var createResult =
                ExecuteRequest(new ExecuteOrderRequest
                {
                    Payment = PaymentType.SocialCard,
                    Prepay = 100.0M,
                    ClientEmail = "mms.dev.test@gmail.com",
                    Phone = "+79152654597",
                    HasDelivery = true,
                    SocialCard = new CardInfo()
                    {
                        Number = "111111111"
                    }
                });

            Assert.IsTrue(createResult.ReturnCode == ReturnCode.Ok);
            Console.WriteLine(createResult.OrderId);
        }

        #endregion


        private CreateOrderOperationResult ExecuteRequest(ExecuteOrderRequest request)
        {
            var client = new OrderServiceClient();
            var rand = new Random((int) DateTime.Now.Ticks);
            var middle = rand.Next().ToString().Sum(t => Convert.ToInt16(t));
            var last = rand.Next(10000).ToString().Sum(t => Convert.ToInt16(t));

            var articlesData = new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 10000,
                    Qty = 1,
                    Benefit = -700,
                    CouponCode = "code",
                    CouponCompainId = "id",
                    OldPrice = 8000
                },
            };
            if (request.Articles != null && request.Articles.Any())
            {
                articlesData.AddRange(request.Articles);
            }
            var createResult =
                client.CreateOrder(
                    new ClientData
                        {
                            Name = request.ClientName,
                            Email = request.ClientEmail,
                            Phone = request.Phone,
                            Surname = "ОПЛАЧЕННЫЙ РЕЗЕРВ",
                            SocialCard = request.SocialCard
                        },
                    new CreateOrderData
                        {
                            OrderId = OrderId + "-" + middle.ToString() + "-" + last.ToString(),
                            StoreInfo = new StoreInfo { SapCode = request.SapCode },
                            OrderSource = request.Source,
                            IsPreorder = request.IsPreorder,
                            ExternalSystem = request.ExternalSystem,
                            PaymentType = request.Payment,
                            SalesDocumentData = request.NeedSalesDocument
                                                    ? new SalesDocumentData
                                                        {
                                                            OutletInfo = "OutletInfo",
                                                            ProductPickupInfo = "ProductPickupInfo",
                                                            PrintableInfo = "PrintableInfo"
                                                        }
                                                    : null,
                            Comment = request.Comment,
                            IsTest = request.IsTest,
                            CreatedBy = request.CreatedBy,
                            CouponCode = "test_1234567",
                            //Coup = "test_1234567"
                        },
                    new DeliveryInfo
                        {
                            City = "Москва",
                            Address = "Проспект Мира дом 5 кв. 47 Онлайн кредит",
                            HasDelivery = request.HasDelivery,
                            PickupLocationId = request.PickupLocationId,
                            //DeliveryDate = DateTime.Now.AddDays(2),
                            //RequestedDeliveryTimeslot = "с 3 до 7",
                        },
                    articlesData.ToArray());

            return createResult;
        }

        private CreateOrderOperationResult ExecuteRequest2(ExecuteOrderRequest request)
        {
            var client = new OrderServiceClient();
            var rand = new Random((int) DateTime.Now.Ticks);
            var middle = rand.Next().ToString().Sum(t => Convert.ToInt16(t));
            var last = rand.Next().ToString().Sum(t => Convert.ToInt16(t));

            var createResult =
                client.CreateOrder2(
                    new ClientData
                        {
                            Name = request.ClientName,
                            Email = request.ClientEmail,
                            Phone = request.Phone,
                            Surname = "Иванвв"
                        },
                    new CreateOrderData
                        {
                            OrderId = OrderId + "-" + middle.ToString() + "-" + last.ToString(),
                            StoreInfo = new StoreInfo {SapCode = "R004"},
                            OrderSource = request.Source,
                            IsPreorder = request.IsPreorder,
                            PaymentType = request.Payment,
                            SalesDocumentData = request.NeedSalesDocument
                                                    ? new SalesDocumentData
                                                        {
                                                            OutletInfo = "OutletInfo",
                                                            ProductPickupInfo = "ProductPickupInfo",
                                                            PrintableInfo = "PrintableInfo"
                                                        }
                                                    : null
                        },
                    new DeliveryInfo
                        {
                            City = "Москва",
                            Address = "Проспект Мира дом 5 кв. 47 Онлайн кредит",
                            HasDelivery = request.HasDelivery,
                            PickupLocationId = request.PickupLocationId
                        },
                    new List<InitArticleData>
                        {
                            new InitArticleData
                                {
                                    ArticleNum = "201000001",
                                    Price = 3000,
                                    Qty = 3,
                                    SerialNumber = "995989",
                                    SubArticleData = new List<InitArticleData>
                                        {
                                            new InitArticleData
                                                {
                                                    ArticleNum = "1149334",
                                                    ItemState = ItemState.New,
                                                    WarrantyInsuranceArticle = "1150091",
                                                    StockNumber = 12
                                                },
                                        }.ToArray()
                                },
                        }.ToArray());

            return createResult;
        }

        private CreateOrderOperationResult ExecuteRequest3(ExecuteOrderRequest request)
        {
            var client = new OrderServiceClient();
            var rand = new Random((int) DateTime.Now.Ticks);
            var middle = rand.Next().ToString().Sum(t => Convert.ToInt16(t));
            var last = rand.Next().ToString().Sum(t => Convert.ToInt16(t));

            var createResult =
                client.CreateOrder2(
                    new ClientData
                        {
                            Name = request.ClientName,
                            Email = request.ClientEmail,
                            Phone = request.Phone,
                            Surname = "Иванвв"
                        },
                    new CreateOrderData
                        {
                            OrderId = OrderId + "-" + middle.ToString() + "-" + last.ToString(),
                            StoreInfo = new StoreInfo {SapCode = "R004"},
                            PaymentType = request.Payment,
                            SalesDocumentData = request.NeedSalesDocument
                                                    ? new SalesDocumentData
                                                        {
                                                            OutletInfo = "OutletInfo",
                                                            ProductPickupInfo = "ProductPickupInfo",
                                                            PrintableInfo = "PrintableInfo"
                                                        }
                                                    : null
                        },
                    new DeliveryInfo
                        {
                            City = "Москва",
                            Address = "Проспект Мира дом 5 кв. 47 Онлайн кредит",
                            HasDelivery = request.HasDelivery,
                            PickupLocationId = request.PickupLocationId,
                            DeliveryDate = null,
                        },
                    new List<InitArticleData>
                        {
                            new InitArticleData
                                {
                                    ArticleNum = "201000001",
                                    Price = 3000,
                                    Qty = 3,
                                    SerialNumber = "995989",
                                    SubArticleData = new List<InitArticleData>
                                        {
                                            new InitArticleData
                                                {
                                                    ArticleNum = "1149334",
                                                    ItemState = ItemState.New,
                                                    WarrantyInsuranceArticle = "1150091",
                                                    StockNumber = 12
                                                },
                                        }.ToArray()
                                },
                        }.ToArray());

            return createResult;
        }

        private CreateOrderOperationResult ExecuteRequestFromOneClick(ExecuteMetroOrderRequest request)
        {
            var client = new OrderServiceClient();
            var rand = new Random((int) DateTime.Now.Ticks);
            var middle = rand.Next().ToString().Sum(t => Convert.ToInt16(t));
            var last = rand.Next().ToString().Sum(t => Convert.ToInt16(t));

            var createResult =
                client.CreateOrder(
                    new ClientData
                        {
                            Name = request.ClientName,
                            Email = request.ClientEmail,
                        },
                    new CreateOrderData
                        {
                            OrderId = OrderId + "-" + middle.ToString() + "-" + last.ToString(),
                            StoreInfo = new StoreInfo {SapCode = "R004"},
                            OrderSource = request.Source,
                            ExternalSystem = ExternalSystem.ZZT,
                        },
                    new DeliveryInfo
                        {
                        },
                    new List<ArticleData>
                        {
                            new ArticleData
                                {
                                    ArticleNum = "1111111",
                                    Price = 10000,
                                    Qty = 1,
                                },
                        }.ToArray()
                    );

            return createResult;
        }
    }

    public class ExecuteOrderRequest
    {
        public string ClientName { get; set; }

        public string ClientEmail { get; set; }

        public bool IsPreorder { get; set; }

        public PaymentType Payment { get; set; }

        public bool HasDelivery { get; set; }

        public string Phone { get; set; }

        public bool NeedSalesDocument { get; set; }

        public ExternalSystem ExternalSystem { get; set; }

        public OrderSource Source { get; set; }

        public List<ArticleData> Articles { get; set; }

        public ExecuteOrderRequest()
        {
            ClientName = "Иванов";
            ClientEmail = string.Format("vadik.on@gmail.com");
            Phone = "+79031329846";
            IsPreorder = false;
            Payment = PaymentType.Cash;
            HasDelivery = false;
            Source = OrderSource.WebSite;
            NeedSalesDocument = false;
            ExternalSystem = ExternalSystem.None;
            SapCode = "R007";
            Prepay = 0.0M;
        }

        public CardInfo SocialCard { get; set; }

        public string SapCode { get; set; }

        public string PickupLocationId { get; set; }

        public string Comment { get; set; }

        public bool IsTest { get; set; }

        public string CreatedBy { get; set; }

        public decimal Prepay { get; set; }
    }

    public class ExecuteMetroOrderRequest : ExecuteOrderRequest
    {
        public string ConsumerId { get; set; }
        public string CampaignId { get; set; }

        public ExecuteMetroOrderRequest()
        {
            ConsumerId = "market_mm";
            CampaignId = "4468_Flyer";
        }
    }
}