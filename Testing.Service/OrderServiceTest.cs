﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Service.Orders;
using System.Diagnostics;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Core;
using log4net.Config;

namespace Testing.Service
{
    [TestClass]
    public class OrderServiceTest
    {
        public ILog Log;
        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            Log = LogManager.GetLogger("OrderServiceTest");
        }

        private ReserveInfo newReserveInfo;
        private UpdateOrderData orderData;
        [TestMethod]
        public void OrderService_UpdateOrder_Test()
        {
            var order = new OrderInfo
            {
                OrderId = "006-469-220",
            };
            ReserveInfo reserveInfo;
            using (var client = new OrderServiceClient())
            {
                var gerOrderDataResult = client.GerOrderDataByOrderId(order.OrderId);
                order = gerOrderDataResult.OrderInfo;
                reserveInfo = gerOrderDataResult.ReserveInfo;
            }            
            
            orderData = new UpdateOrderData
            {
                ChangeStatusTo = GeneralOrderStatus.Confirmed,
                OrderId = order.OrderId,
                Comment = "Update Order 2",
            };
            newReserveInfo = new ReserveInfo
            {
                Status = reserveInfo.Status,
                SapCode = reserveInfo.SapCode,
                ReserveLines = reserveInfo.ReserveLines.ToArray()
            };

            foreach(var reserveLine in newReserveInfo.ReserveLines)
            {
                reserveLine.Comment += "<br/>Это очень длинный текст: Now to explain the code we have to take a slight diversion and talk about the relationship between Activity, ActivityInstance and ExecutionContext. I talked about is a while back here when the PDC CTP first came out (that’s what the reference to some base class called WorkflowElement is about) but to expand a little: The Activity is really just a template containing the code to execute for the activity. The ActivityInstance is the actual thing that is executing. It holds the state for this instance of the activity template. Now we need a way to bind the template code to the currently running instance of the activity and this is the role of the ExecutionContext. If you are using a CodeActivity base class then most of this is hidden from you except that you have to access arguments by passing in the ExecutionContext. However, with NativeActivity you have to get more directly involved with this model. Now how does the workflow engine know what data you need to store in the ActivityInstance? Well it turns out you need to tell it. NativeActivity has a virtual method called CacheMetadata (this post talks about it to some degree). The point being that the activity has to register all of the “stuff” that it wants to use during its execution. Now the base class implementation will do some fairly reasonable default actions but it cannot know, for example, that part of your functionality is there purely for implementation details and should not be public. Therefore, you will often override this when you create a NativeActivity. n CacheMetadata the first thing we do is specify that the Body is our child activity. Next we tell the engine that we want to be able to get hold of the CurrentRetry but that its only there for our implementation – we’re not expecting the child activity to try to make use of it. The next 3 lines (17-19) seem a little strange but essentially we’re saying that the MaxRetries argument needs to be accessed over the whole lifetime of the ActivityInstance so we need a slot for that. We next configure out Delay activity passing the RetryDelay as its duration (this is how long we want to wait between retries). Finally we add the Delay, not as a normal child, but as an implementation detail.";
            }

            var sw = new Stopwatch();
            sw.Start();
            using (var client = new OrderServiceClient())
            {
                client.UpdateOrder(orderData, newReserveInfo);
            }
            sw.Stop();
            System.Diagnostics.Debug.WriteLine("Elapsed: {0} ms", sw.ElapsedMilliseconds);

        }
    }
}