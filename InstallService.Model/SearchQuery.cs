﻿using System;

namespace InstallService.Model
{
    public class SearchQuery
    {
        public string OrderId { get; set; }

        public string WwsOrderId { get; set; }

        public string[] Parameters { get; set; }

        public string Phone { get; set; }

        public DateTime? InstalationDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public InstallationStatus? Status { get; set; }
    }
}
