﻿using System;

namespace InstallService.Model
{
    public class LogItem
    {
        public int Id { get; set; }

        public int InstallationId { get; set; }

        public DateTime? NewApprovedDate { get; set; }

        public DateTime? NewFactDate { get; set; }

        public InstallationStatus? NewStatus { get; set; }

        public string NewInstallationAddress { get; set; }

        public string NewStatusReason { get; set; }

        public DateTime LogCreatedDate { get; set; }

        public string UserName { get; set; }
    }
}
