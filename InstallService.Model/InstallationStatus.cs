﻿namespace InstallService.Model
{
    public enum InstallationStatus
    {
        None = 0,
        New = 1,
        ProcessingWithOpenDate = 2,
        Processing = 3,
        Closed = 4,
        Canceled = 5
    }
}
