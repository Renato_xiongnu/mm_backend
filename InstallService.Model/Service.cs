﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NLog;

namespace InstallService.Model
{
    public class Service
    {
        private const String InvalidArgumentException = "{0} - обязательное поле(я)";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly IInstallRepository _installRepository;

        public Service(IInstallRepository installRepository)
        {
            try
            {
                if (installRepository == null)
                    throw new ArgumentNullException(InvalidArgumentException, "orderRepository");
                _installRepository = installRepository;
            }
            catch (Exception ex)
            { 
                Log.Error(ex);
                throw;
            }
        }

        public Task CancelInstallationAsync(string orderId, string reason = "отмена заказа")
        {
            try
            {
                if (String.IsNullOrEmpty(orderId))
                    throw new ArgumentNullException(String.Format(InvalidArgumentException, "orderId"));
                return _installRepository.CancelOrderAsync(orderId,reason);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public async Task CreateInstallationAsync(String orderId)
        {
            try
            {
                if (String.IsNullOrEmpty(orderId))
                    throw new ArgumentNullException(String.Format(InvalidArgumentException, "orderId"));
                await _installRepository.CreatInstallationsForOrderAsync(orderId);
            }
            catch (AggregateException ex)
            {
                Log.Error(ex.Flatten());
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
        
        public async Task<Order> SearchInstallationAsync(SearchQuery query)
        {
            try
            {
                return await _installRepository.SearchInstallationsAsync(query);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public async Task<List<Order>> All(SearchQuery searchQuery)
        {
            try
            {
                return await _installRepository.All(searchQuery);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public async Task UpdateInstallationAsync(int id, DateTime? newApprovedlDate, DateTime? newFactDate,
            string newInstallationAddress, InstallationStatus? newInstallationStatus, string newStatusReason,
            string userName)
        {
            try
            {
                await
                    _installRepository.UpdateInstallationAsync(id, newApprovedlDate, newFactDate, newInstallationAddress,
                        newInstallationStatus, newStatusReason, userName);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public static string GetStatusLocalizableName(InstallationStatus installationStatus)
        {
            switch (installationStatus)
            {
                case InstallationStatus.New:
                    return "Новая заявка";
                case InstallationStatus.Processing:
                    return "В работе";
                case InstallationStatus.ProcessingWithOpenDate:
                    return "Открытая дата";
                case InstallationStatus.Closed:
                    return "Выполнено";
                case InstallationStatus.Canceled:
                    return "Не выполнено";
                case InstallationStatus.None:
                    return "-";
                default:
                    return installationStatus.ToString();
            }
        }
    }
}
