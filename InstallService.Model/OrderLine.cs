﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace InstallService.Model
{
    public class OrderLine
    {
        [Key]
        public Int32 Id { get; set; }

        public Order Order { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ApprovedDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? FactDate { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Select a correct status")]
        public InstallationStatus Status { get; set; }

        public String ArticleId { get; set; }

        public String Name { get; set; }

        public Decimal? Price { get; set; }

        public String ReferToArticle { get; set; }

        public string StatusReason { get; set; }

        public string ArticleType { get; set; }

        [DataType(DataType.Date)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? CompleteDate { get; set; }

        public OrderLine()
        {
            Status = InstallationStatus.New;
        }
    }
}
