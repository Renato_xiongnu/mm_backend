﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InstallService.Model
{
    public class Order
    {
        [Key] 
        public String OrderId { get; set; }

        public String SapCode { get; set; }

        public String WwsOrderId { get; set; }

        public String Status { get; set; }

        public String PaymentType { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public String DeliveryType { get; set; }

        public String DeliveryAddress { get; set; }

        public String RequestedDeliveruPeriod { get; set; }

        public String Name { get; set; }
     
        public String Phone { get; set; } 

        public String InstallationAddress { get; set; }

        public virtual IList<OrderLine> Items { get; set; }

        [NotMapped]
        public bool ShouldBeIgnored { get; set; }
    }
}
