﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InstallService.Model
{
    public interface IInstallRepository
    {
        Task CreatInstallationsForOrderAsync(String orderId);
        Task<Order> SearchInstallationsAsync(SearchQuery query);
        Task CancelOrderAsync(string orderId, string reason);
        Task UpdateInstallationAsync(int id, DateTime? newApprovedDate, DateTime? newFactDate,
            string newInstallationAddress, InstallationStatus? newStatus, string newStatusReason, string updatedBy);
        Task<List<Order>> All(SearchQuery searchQuery);
    }
}
