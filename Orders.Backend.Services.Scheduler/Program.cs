﻿using System;
using NLog;
using ServiceBase = System.ServiceProcess.ServiceBase;

namespace Orders.Backend.Services.Scheduler
{
    internal static class Program
    {
        private static readonly Logger Logger = LogManager.GetLogger("QuartzScheduler");

        private static JobSchedulerStartup _scheduler;

        private static void Main()
        {
            //XmlConfigurator.Configure();
            _scheduler = new JobSchedulerStartup();
            Logger.Trace("QuartzScheduler has been started");

            try
            {
                if (!Environment.UserInteractive)
                {                    
                    using (ServiceBase service = new SchedulerService(_scheduler))
                    {
                        ServiceBase.Run(service);
                    }
                }
                else
                {

                    Start();
                    //DebugHelper.RunServiceAsWinForm<PickupLocationService>();
                    Console.WriteLine("Press any key to stop...");
                    Console.ReadKey();
                    Stop();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        private static void Start()
        {
            _scheduler.StartQuartz();
        }

        private static void Stop()
        {
            _scheduler.StopQuartz();
        }
    }
}