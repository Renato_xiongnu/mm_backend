using System.ServiceProcess;

namespace Orders.Backend.Services.Scheduler
{
    public class SchedulerService : ServiceBase
    {
        private readonly JobSchedulerStartup _scheduler;

        private const string Name = "OrderBackend Polling Scheduler";

        public SchedulerService(JobSchedulerStartup scheduler)
        {
            _scheduler = scheduler;
            ServiceName = Name;
        }

        protected override void OnStart(string[] args)
        {
            _scheduler.StartQuartz();
        }

        protected override void OnStop()
        {
            _scheduler.StopQuartz();
        }
    }
}