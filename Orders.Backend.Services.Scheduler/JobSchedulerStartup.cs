﻿using System;
using NLog;
using Quartz;
using Quartz.Impl;

namespace Orders.Backend.Services.Scheduler
{
    public class JobSchedulerStartup
    {
        static readonly Logger Logger = LogManager.GetLogger("QuartzScheduler");
        IScheduler _scheduler;

        public void StartQuartz()
        {
            try
            {
                ISchedulerFactory factory = new StdSchedulerFactory();

                _scheduler = factory.GetScheduler();
                _scheduler.Start();

                Logger.Trace("QuartzScheduler has been started");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void StopQuartz()
        {
            try
            {
                if (_scheduler != null)
                {
                    _scheduler.Shutdown(false);
                    _scheduler = null;
                }
                Logger.Trace("QuartzScheduler has been stopped");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}