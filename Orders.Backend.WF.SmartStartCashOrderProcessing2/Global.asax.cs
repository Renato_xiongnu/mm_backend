﻿using System;
using System.Net;
using System.Web;
using Common.Logging;
using MMS.Activities.Logging;

namespace Orders.Backend.WF.SmartStartCashOrderProcessing2
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var logger = LogManager.GetLogger("SmartStartCashOrderProcessing2");
            logger.Info("Application started");
            Logger.Init(logger);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}