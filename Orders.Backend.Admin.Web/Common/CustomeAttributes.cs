﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Globalization;
using System.Reflection;

namespace Orders.Backend.Admin.Web.Common
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequiredIfAttribute : RequiredAttribute //, IClientValidatable
    {
        //private const string _defaultErrorMessage = "'{0}' is required when {1} equals {2}.";
        private const string _defaultErrorMessage = "'{0}' is required.";

        public string DependentProperty { get; set; }
        public object TargetValue { get; set; }
        public bool IsEqual { get; set; }
        public override object TypeId
        {
            get
            {
                return base.TypeId;
            }
        }


        public RequiredIfAttribute(string dependentProperty, object targetValue, bool isEqual = true)
        //: base(_defaultErrorMessage)
        {
            this.DependentProperty = dependentProperty;
            this.TargetValue = targetValue;
            this.IsEqual = isEqual;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString, name, DependentProperty, TargetValue);
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (context.ObjectInstance != null)
            {
                Type type = context.ObjectInstance.GetType();
                PropertyInfo info = type.GetProperty(DependentProperty);
                object dependentValue;
                if (info != null)
                {
                    dependentValue = info.GetValue(context.ObjectInstance, null);
                    var isNeedToCheck = IsEqual ? object.Equals(dependentValue, TargetValue) : !object.Equals(dependentValue, TargetValue);
                    if (isNeedToCheck)
                    {
                        if (string.IsNullOrWhiteSpace(Convert.ToString(value)))
                        {
                            return new ValidationResult(ErrorMessage);
                        }
                    }
                }
            }
            return ValidationResult.Success;
        }

        //public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        //{
        //    ModelClientValidationRule rule = new ModelClientValidationRule();
        //    rule.ErrorMessage = this.FormatErrorMessage(metadata.PropertyName);
        //    rule.ValidationType = "requiredif";
        //    rule.ValidationParameters.Add("depedentproperty", DependentProperty);
        //    rule.ValidationParameters.Add("targetvalue", TargetValue);
        //    yield return rule;
        //}
    }

    public class RequiredIfValidator : DataAnnotationsModelValidator<RequiredIfAttribute>
    {
        public RequiredIfValidator(ModelMetadata metadata, ControllerContext context, RequiredIfAttribute attribute)
            : base(metadata, context, attribute)
        {
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            return base.GetClientValidationRules();
        }

        public override IEnumerable<ModelValidationResult> Validate(object container)
        {
            var field = Metadata.ContainerType.GetProperty(Attribute.DependentProperty);
            if (field != null)
            {
                var value = field.GetValue(container, null);
                if ((value == null && Attribute.TargetValue == null) ||
                    (value.Equals(Attribute.TargetValue)))
                {
                    if (!Attribute.IsValid(Metadata.Model))
                        yield return new ModelValidationResult { Message = ErrorMessage };
                }
            }
        }
    }
}