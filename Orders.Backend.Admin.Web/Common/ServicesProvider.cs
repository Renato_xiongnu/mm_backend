﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Admin.Web.Common
{
    public class ServicesProvider
    {
        private ServicesProvider()
        {

        }

        public OrdersAdmin.OrdersAdminClient GetOrderAdminClient { get { return new OrdersAdmin.OrdersAdminClient(); } }

        public OrdersSlipService.OnlineOrderServiceClient GetOrderSlipClient { get { return new OrdersSlipService.OnlineOrderServiceClient(); } }

        public GmsSlipService.SlipServiceClient GetGmsSlipClient { get { return new GmsSlipService.SlipServiceClient(); } }

        public OsiProxyService.ProxyServiceClient GetOsiClient { get { return new OsiProxyService.ProxyServiceClient(); } }

        private ICollection<string> _stores;

        public ICollection<string> Stores { get { return _stores ?? (_stores = GetOrderAdminClient.GetStores().Stores.Select(t => t.SapCode).ToList()); } }

        private static ServicesProvider _instance;
        public static ServicesProvider GetInstance()
        {
            return _instance ?? (_instance = new ServicesProvider());
        }
    }
}