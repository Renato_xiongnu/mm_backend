﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Orders.Backend.Admin.Web.Models;
using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Admin.Web.Controllers
{
    public class CertificateController : Controller
    {
        //
        // GET: /Certificate/

        private CertificateModelProvider _certificateProvider = new CertificateModelProvider();

        public ActionResult Index()
        {
            return Redirect("Certificate/Details");
        }


        public PartialViewResult ChangeCertBalance(string certificateId, string sapCode, decimal amount)
        {
            OsiProxyService.TransactionType transType;
            var sign = Math.Sign(amount);
            if (sign == -1)
            {
                amount = Math.Abs(amount);
                transType = OsiProxyService.TransactionType.Debit;
            }
            else if (sign  == 1)
            {
                transType = OsiProxyService.TransactionType.Credit;
            }
            else
            {
                return PartialView("_OperationResult", new OperationResultModel()
                {
                    IsComplete = false,
                    ErrorText = "Сумма должна быть больше нуля"
                });
            }

            var response = _certificateProvider.UpdateOsiCardAmount(certificateId, sapCode, amount, transType);            

            var result = new OperationResultModel()
            {
                IsComplete = response != null,
            };

            if (result.IsComplete)
                result.CompleteText = string.Format("Сумма {0} {1}", amount, transType == OsiProxyService.TransactionType.Credit ? "начислена" : "списана" );
            else
                result.ErrorText = string.Format("произошла ошибка вовремя изменения баланса сертификата");

            return PartialView("_OperationResult", result);
        }
        
        public PartialViewResult TransferToPay(string certificateId)
        {
            var isOk = _certificateProvider.TransferToPay(certificateId);

            var result = new OperationResultModel()
            {
                IsComplete = isOk,
            };

            if (isOk)
                result.CompleteText = string.Format("заказа переведен в оплаченные");
            else
                result.ErrorText = string.Format("произошла ошибка вовремя перевода заказа в оплаченные");

            return PartialView("_OperationResult", result); 
        }


        //
        // GET: /Certificate/Details/5

        public ActionResult Details(string certificateId = "", string orderId ="", string wwsOrderId ="", string sapCode ="")
        {
            string resultSapCode = null;
            OrdersBackendInfo ordersBackendInfo = null;
            var certificateModel = new CertificateInfoModel();

            if (!ModelState.IsValid)
                return View(certificateModel);

            if (string.IsNullOrEmpty(certificateId))
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    if (string.IsNullOrEmpty(wwsOrderId) || string.IsNullOrEmpty(sapCode))
                    {
                        return View(certificateModel);
                    }
                    else
                    {
                        ordersBackendInfo = _certificateProvider.GetOrderByWWSOrderId(wwsOrderId, sapCode);
                        resultSapCode = sapCode;
                    }
                }
                else
                {
                    ordersBackendInfo = _certificateProvider.GetOrderByOrderId(orderId);
                }
            }
            else
            {
                ordersBackendInfo = _certificateProvider.GetOrderByCertificate(certificateId);
            }

            if (ordersBackendInfo.OrderInfo.certificateInfo != null)
            {
                certificateModel.CertificateId = ordersBackendInfo.OrderInfo.certificateInfo.CertificateId;


                certificateModel.OrdersBackendInfo = ordersBackendInfo;
                resultSapCode = resultSapCode ?? (ordersBackendInfo != null ? ordersBackendInfo.OrderInfo.SapCode : string.Empty);


                certificateModel.Slips = _certificateProvider.GetOrdersSlipByCertifate(certificateModel.CertificateId, resultSapCode);

                certificateModel.GmsSlips = _certificateProvider.GetGmsSlipByCertificate(certificateModel.CertificateId, resultSapCode);

                certificateModel.SapCode = resultSapCode;
                certificateModel.OrderId = ordersBackendInfo.OrderInfo.OrderId;
                certificateModel.WWSOrderId = ordersBackendInfo.OrderInfo.WWSOrderId;

                certificateModel.OsiInfo = new OsiInfo()
                {
                    cardInfo = _certificateProvider.GetOsiGiftCardInfo(certificateModel.CertificateId)
                };
            }
            return View(certificateModel);
        }

        //
        // GET: /Certificate/Create

        //public ActionResult Create()
        //{
        //    return View();
        //}

        //
        // POST: /Certificate/Create

        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /Certificate/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //
        // POST: /Certificate/Edit/5

        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /Certificate/Delete/5

        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //
        // POST: /Certificate/Delete/5

        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }


 
}
