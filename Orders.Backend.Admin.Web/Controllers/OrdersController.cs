﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Orders.Backend.Admin.Web.Models;

namespace Orders.Backend.Admin.Web.Controllers
{
    
    
    public class OrdersController : Controller
    {
        private OrdersProvider _provider = new OrdersProvider();
        
        public ActionResult Index()
        {
            var statusInfo = _provider.GetOrdersStatInfo();
            return View(statusInfo);
        }

    }
}
