﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Orders.Backend.Admin.Web.OrdersAdmin;
using Orders.Backend.Admin.Web.OrdersSlipService;
using Orders.Backend.Admin.Web.Common;
using Orders.Backend.Admin.Web.OsiProxyService;

namespace Orders.Backend.Admin.Web.Models
{

    public class CertificateModelProvider
    {
        private ServicesProvider _services = ServicesProvider.GetInstance();

        private ICollection<string> GetValideStores(string sapCode)
        {
            if (string.IsNullOrEmpty(sapCode))
                return _services.Stores;
           
            return new[] { sapCode };
        }

        public OrdersBackendInfo GetOrderByCertificate(string certificateId)
        {
            try
            {
                var client = _services.GetOrderAdminClient;

                var order = client.GetOrderDataByCertificate(certificateId);

                return new OrdersBackendInfo()
                {
                    OrderInfo = order.OrderInfo,
                    ClientData = order.ClientData,
                    ArticleLines = order.ArticleLines
                };
            }
            catch
            {
                return new OrdersBackendInfo()
                {
                    OrderInfo = new OrderInfo(),
                    ClientData = new ClientData(),
                    ArticleLines = new List<ArticleData>()
                };
            }
        }

        public OrdersBackendInfo GetOrderByOrderId(string orderId)
        {
            try
            {
                var client = _services.GetOrderAdminClient;

                var order = client.GetOrderDataByOrderId(orderId);

                return new OrdersBackendInfo()
                {
                    OrderInfo = order.OrderInfo,
                    ClientData = order.ClientData,
                    ArticleLines = order.ArticleLines
                };
            }
            catch
            {
                return new OrdersBackendInfo()
                {
                    OrderInfo = new OrderInfo(),
                    ClientData = new ClientData(),
                    ArticleLines = new List<ArticleData>()
                };
            }
        }


        public OrdersBackendInfo GetOrderByWWSOrderId(string wwsOrderId, string sapCode)
        {
            try
            {
                var client = _services.GetOrderAdminClient;
                var order = client.GetOrderDataByWWSOrderId(sapCode, wwsOrderId);
                return new OrdersBackendInfo()
                {
                    OrderInfo = order.OrderInfo,
                    ClientData = order.ClientData,
                    ArticleLines = order.ArticleLines
                };
            }
            catch
            {
                return new OrdersBackendInfo()
                {
                    OrderInfo = new OrderInfo(),
                    ClientData = new ClientData(),
                    ArticleLines = new List<ArticleData>()
                };
            }
            
        }

        public ICollection<Slip> GetGmsSlipByCertificate(string certificateId, string helpSapCode = "")
        {
            var client = _services.GetGmsSlipClient;

            var resultSlips = new List<Slip>();

            var sapCodes = GetValideStores(helpSapCode);
            foreach (var sapCode in sapCodes)
            {
                try
                {
                    var slips = client.GetSlipsByCertificate(sapCode, certificateId);

                    resultSlips.AddRange(InitSlipsFromItems(slips));
                }
                catch (Exception ex)
                {
 
                }
            }

            return resultSlips;
        }

        public ICollection<Slip> GetOrdersSlipByCertifate(string certificateId, string helpSapCode = "")
        {
            var client = _services.GetOrderSlipClient;

            var resultSlips = new List<Slip>();

            try
            {
                var sapCodes = GetValideStores(helpSapCode);
                foreach (var sapCode in sapCodes)
                {
                    var slips = client.FindSlipItems(sapCode, new[] { certificateId });

                    resultSlips.AddRange(InitSlipsFromItems(slips));
                }
            }
            catch
            {

            }

            return resultSlips;
        }

        public bool TransferToPay(string certificateId)
        {
            var ok = true;
            try
            {
                _services.GetOrderAdminClient.ChangeOrderToPaidStatus(certificateId);
            }
            catch
            {
                ok = false;
            }

            return ok;
        }

        public CardInfo GetOsiGiftCardInfo(string certificateId)
        {
            var response = _services.GetOsiClient.GetCardBalance(new GetCardBalanceRequest()
            {
                CardNumbers = new[] { certificateId },
                IncludeTransaction = true
            });

            if (response.SucceededCards.Length == 0)
                return new CardInfo();

            return response.SucceededCards.First();
        }

        public CardTransaction UpdateOsiCardAmount(string certificateId, string sapCode, decimal amount, TransactionType transType)
        {
            var response = _services.GetOsiClient.UpdateCardAmount(new UpdateCardAmountRequest()
            {
                GiftCards = new[]
                {
                    new CardTransaction()
                    {
                        Number = certificateId,
                        Chain = OsiProxyService.ChainType.MediaMarkt,
                        Comment = "Manual changes amount from ordersAdmin",
                        Reason = "Admin correction",
                        SapCode =  sapCode,
                        TransactionType = transType
                    }
                }
            });

            if (response.SucceededCards.Length == 0)
                return new CardTransaction();

            return response.SucceededCards.First();
        }

        private static ICollection<Slip> InitSlipsFromItems(GmsSlipService.Slip[] slips)
        {
            var ordersSlips = new List<Slip>();

            if (slips != null)
            {
                foreach (var slip in slips)
                {
                    var orderSlip = new Slip()
                    {
                        SapCode = slip.SapCode,
                        SlipDate = slip.SlipDate,
                        SlipNumber = slip.SlipNumber,
                        Status = slip.Status,
                        CashRegisterNumber = slip.CashRegisterNumber,
                        Amount = slip.SlipLines.Sum(t => t.Price),
                        TotalQty = slip.SlipLines.Sum(t => t.Quantity),
                        SlipLines = new List<SlipLine>()
                    };

                    foreach (var item in slip.SlipLines)
                    {
                        int docNumber;
                        int.TryParse(item.DocumentNumber, out docNumber);
                        var slipLine = new SlipLine()
                        {
                            DocumentNumber = docNumber,
                            Price = item.Price,
                            Qty = item.Quantity,
                        };

                        switch (item.LineType)
                        {
                            case GmsSlipService.SlipLineType.Article:
                                slipLine.ArticleNumber = Convert.ToInt32(item.ArticleNumber);
                                slipLine.ArticleName = item.ArticleName;
                                break;
                            case GmsSlipService.SlipLineType.GiftCard:
                                slipLine.GiftCardNumber = item.ArticleNumber;
                                break;
                            case GmsSlipService.SlipLineType.Set:
                                slipLine.SetNumber = Convert.ToInt32(item.ArticleNumber);
                                slipLine.SetName = item.ArticleName;
                                break;
                        }
                        orderSlip.SlipLines.Add(slipLine);
                    }

                    ordersSlips.Add(orderSlip);
                }
            }

            return ordersSlips;
        }

        private static ICollection<Slip> InitSlipsFromItems(SlipItem[] slips)
        {
            var ordersSlips = new List<Slip>();
            if (slips != null)
            {
                foreach (var slip in slips.GroupBy(t => new { t.SapCode, t.SlipNumber, t.SlipDate, }))
                {                    
                    var orderSlip = new Slip()
                    {
                        SapCode = slip.Key.SapCode,
                        SlipNumber = slip.Key.SlipNumber,
                        SlipDate = slip.Key.SlipDate,
                        Amount = slip.Sum(t => t.Price),
                        TotalQty = slip.Sum(t => t.Count),
                        SlipLines = new List<SlipLine>()
                    };

                    foreach (var item in slips)
                    {
                        orderSlip.SlipLines.Add(new SlipLine()
                        {
                            ArticleName = item.ArticleName,
                            ArticleNumber = item.ArticleNumber,
                            GiftCardNumber = item.GiftCardNumber,
                            SetName = item.SetName,
                            SetNumber = item.SetNumber,
                            DocumentNumber = item.DocumentNumber,
                            Price = item.Price,
                            Qty = item.Count,
                        });
                    }

                    ordersSlips.Add(orderSlip);
                }
            }
            return ordersSlips;
        }
    }

    public class CertificateInfoModel
    {
        [Display(Name = "Подарочная карта")]
        [Range(999999999999,99999999999999999, ErrorMessage = "Сертификат должен быть числом 13 знаков")]
        public string CertificateId { get; set; }

        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }

        [Display(Name = "Номер резерва", GroupName="Резерв")]
        [RequiredIf("SapCode", "", false)]
        public string WWSOrderId { get; set; }

        [Display(Name = "Магазин", GroupName = "Резерв")]
        [RequiredIf("WWSOrderId", "", false)]
        public string SapCode { get; set; }

        public OrdersBackendInfo OrdersBackendInfo { get; set; }

        public ICollection<Slip> Slips { get; set; }

        public ICollection<Slip> GmsSlips { get; set; }

        public OsiInfo OsiInfo { get; set; }
    }

    public class OsiInfo
    {
        public CardInfo cardInfo { get; set; }
    }

    public class OrdersBackendInfo
    {
        public OrderInfo OrderInfo { get; set; }

        public ClientData ClientData { get; set; }

        public ICollection<ArticleData> ArticleLines { get; set; }
    }

    public class Slip
    {
        public string SapCode { get; set; }

        public int SlipNumber { get; set; }

        public DateTime SlipDate { get; set; }

        public decimal Amount { get; set; }

        public int TotalQty { get; set; }

        public string Status { get; set; }

        public int CashRegisterNumber { get; set; }

        public ICollection<SlipLine> SlipLines { get; set; }
    }

    public class SlipLine
    {
        public string ArticleName { get; set; }

        public int ArticleNumber { get; set; }

        public string SetName { get; set; }

        public int SetNumber { get; set; }

        public int DocumentNumber { get; set; }

        public string GiftCardNumber { get; set; }

        public decimal Price { get; set; }

        public int Qty { get; set; }

    }

}