﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Admin.Web.OrdersAdmin;
using Orders.Backend.Admin.Web.Common;

namespace Orders.Backend.Admin.Web.Models
{
    public class OrdersProvider
    {
        private ServicesProvider services = ServicesProvider.GetInstance();

        public OrdersStatModel GetOrdersStatInfo()
        {
            try
            {
                var client = services.GetOrderAdminClient;
                return new OrdersStatModel()
                {
                    VersionText = client.GetSystemVersion(),
                    Statistics = new Statistics()
                    {
                        OrdersStatistics = client.GetOrderStatistics(),
                        CertificateStatistics = client.GetCertificateStatistics()
                    }
                };
            }
            catch
            {
                return new OrdersStatModel();
            }
        }

    }

    public class OrdersStatModel
    {
        public string VersionText { get; set; }

        public Statistics Statistics { get; set; }
    }

    public class Statistics
    {
        public ICollection<EntityStatistics> OrdersStatistics { get; set; }

        public ICollection<EntityStatistics> CertificateStatistics {get; set;}
    }
}