﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Admin.Web.Models
{
    public class OperationResultModel
    {
        public bool IsComplete { get; set; }
        public string CompleteText { get; set; }
        public string ErrorText { get; set; }
    }
}