﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using System.Diagnostics;

namespace Orders.Backend.Log.Tests
{
    [TestClass]
    public class LogDatabase_Test
    {
        Logger logger;

        [TestInitialize]
        public void Init()
        {
            NLog.MappedDiagnosticsContext.Set("CorrelationId", "123-45678901");
            logger = LogManager.GetCurrentClassLogger();
        }

        [TestMethod]
        public void LogDatabse_Test1()
        {
            logger.Fatal(": StartProcess receive log : WorkItemId = 006-531-505, Parameter=R007");

            logger.Trace("123-45678901 : Sample trace message");
            logger.Debug("123-45678901 : Sample debug message");
            logger.Info("123-45678901 : Sample informational message");
            logger.Warn("123-45678901 : Sample warning message");
            logger.Error("123-45678901 : Sample error message");
            logger.Fatal("123-45678901 : Sample fatal error message");

            try
            {
                throw new Exception("123-45678901 : throw new Exception");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "123-45678901 : throw new Exception");
            }
        }

        [TestMethod]
        public void LogDatabse_PerformaceTest()
        {
            int count = 1000;
            var random = new Random();

            var sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < count; i++ )
            {
                var a = random.Next(999);
                var b = random.Next(99999999);
                var number = string.Format("{0:000}-{1:00000000}", a, b);

                logger.Trace("{0} : Sample trace message", number);
                logger.Debug("{0} : Sample debug message", number);
                logger.Info("{0} : Sample informational message", number);
                logger.Warn("{0} : Sample warning message", number);
                logger.Error("{0} : Sample error message", number);
                logger.Fatal("{0} : Sample fatal error message", number);
            }
            sw.Stop();
            Console.WriteLine("ElapsedMilliseconds: " + sw.ElapsedMilliseconds);
        }

    }
}
