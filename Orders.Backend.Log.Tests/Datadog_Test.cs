﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMS.Monitoring.Statistics;
using System.Threading;

namespace Orders.Backend.Log.Tests
{
    [TestClass]
    public class Datadog_Test
    {
        [TestMethod]
        public void Datadog_Histograms_Test()
        {
            for (int i = 0; i < 10; i++)
            {
                var random = new Random();
                var metrics = new StatsMetric[]
                {
                    StatsMetric.Gauge("order_processing_time",
                        TimeSpan.FromMinutes(random.Next(1000)).TotalMinutes,
                        null,
                        "source:website,type:quick,delivery_type:pickup,payment_type:cash,result:success"),

                    StatsMetric.Gauge("order_processing_time",
                        TimeSpan.FromMinutes(random.Next(100)).TotalMinutes,
                        null,
                        "source:website,type:quick,delivery_type:pickup,payment_type:online,result:success"),

                    StatsMetric.Timer("order_processing_time2",
                        TimeSpan.FromMinutes(random.Next(100)),
                        null,
                        "source:website,type:quick,delivery_type:pickup,payment_type:cash,result:success"),
                    StatsMetric.Timer("order_processing_time2",
                        TimeSpan.FromMinutes(100),
                        null,
                        "source:website,type:quick,delivery_type:pickup,payment_type:online,result:success")
                };
                StatsEngine.Send(metrics);
                Thread.Sleep(2000);
            }
        }
    }
}
