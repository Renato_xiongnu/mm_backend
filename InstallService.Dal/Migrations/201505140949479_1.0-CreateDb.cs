namespace InstallService.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10CreateDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderLines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApprovedDate = c.DateTime(),
                        FactDate = c.DateTime(),
                        Status = c.Int(nullable: false),
                        ArticleId = c.String(maxLength: 200),
                        Name = c.String(maxLength: 200),
                        Price = c.Decimal(precision: 18, scale: 2),
                        ReferToArticle = c.String(maxLength: 200),
                        StatusReason = c.String(maxLength: 200),
                        ArticleType = c.String(maxLength: 200),
                        Order_OrderId = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Order_OrderId)
                .Index(t => t.Order_OrderId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.String(nullable: false, maxLength: 200),
                        SapCode = c.String(maxLength: 200),
                        WwsOrderId = c.String(maxLength: 200),
                        Status = c.String(maxLength: 200),
                        PaymentType = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(nullable: false),
                        DeliveryDate = c.DateTime(),
                        DeliveryType = c.String(maxLength: 200),
                        DeliveryAddress = c.String(maxLength: 200),
                        RequestedDeliveruPeriod = c.String(maxLength: 200),
                        Name = c.String(maxLength: 200),
                        Phone = c.String(maxLength: 200),
                        InstallationAddress = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.OrderId);
            
            CreateTable(
                "dbo.LogItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InstallationId = c.Int(nullable: false),
                        NewApprovedDate = c.DateTime(),
                        NewFactDate = c.DateTime(),
                        NewStatus = c.Int(),
                        NewInstallationAddress = c.String(maxLength: 200),
                        NewStatusReason = c.String(maxLength: 200),
                        LogCreatedDate = c.DateTime(nullable: false),
                        UserName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLines", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.OrderLines", new[] { "Order_OrderId" });
            DropTable("dbo.LogItems");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderLines");
        }
    }
}
