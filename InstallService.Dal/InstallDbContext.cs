﻿using System.Data.Entity;
using InstallService.Model;

namespace InstallService.Dal
{
    public class InstallDbContext: DbContext
    {
        public InstallDbContext()
        {
            Database.SetInitializer<InstallDbContext>(null);
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> Installations { get; set; }
        public DbSet<LogItem> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties()
                .Where(p => p.Name != "Id" && p.PropertyType==typeof(string))
                .Configure(p => p.HasMaxLength(200));
        } 
    }
}
