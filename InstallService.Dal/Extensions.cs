﻿using System;
using System.Collections.Generic;
using System.Linq;
using InstallService.Dal.OrderService;
using InstallService.Model;


namespace InstallService.Dal
{
    public static class Extensions
    {
        public static Order GetOrderDataResultToOrder(this GerOrderDataResult orderDataResult)
        {
            return new Order
            {
                OrderId = orderDataResult.OrderInfo.OrderId,
                SapCode = orderDataResult.OrderInfo.SapCode,
                WwsOrderId = orderDataResult.OrderInfo.WWSOrderId,
                Status = orderDataResult.OrderInfo.StatusInfo.Status.ToString(),
                PaymentType = orderDataResult.OrderInfo.PaymentType.ToString(),
                CreatedDate = orderDataResult.OrderInfo.Created,
                DeliveryDate = orderDataResult.DeliveryInfo.DeliveryDate,
                DeliveryType = orderDataResult.DeliveryInfo.HasDelivery ? "Доставка" : "Самовывоз",
                DeliveryAddress = orderDataResult.DeliveryInfo.Address,
                Name =
                    String.Format("{0} {1} {2}", orderDataResult.ClientData.LastName, orderDataResult.ClientData.Name,
                        orderDataResult.ClientData.Surname),
                Phone = String.IsNullOrEmpty(orderDataResult.ClientData.Phone)
                    ? orderDataResult.ClientData.Phone2
                    : orderDataResult.ClientData.Phone,
                InstallationAddress = orderDataResult.OrderInfo.InstallServiceAddress,
                Items = CreateOrderLines(orderDataResult.ReserveInfo.ReserveLines.ToList()),
                //CreateOrderLinesFake(orderDataResult.ArticleLines.ToList())//
                RequestedDeliveruPeriod = orderDataResult.DeliveryInfo.RequestedDeliveryTimeslot,
                ShouldBeIgnored =
                    orderDataResult.OrderInfo.PaymentType == PaymentType.SocialCard ||
                    orderDataResult.OrderInfo.Created < new DateTime(2015, 06, 15) //Костыль - игнорируем соц карты и старые заказы
            };
        }
        
        public static List<OrderLine> CreateOrderLines(List<ReserveLine> articles)
        {
            var result = new List<OrderLine>();
            var systemArticles = articles;//.Where(x =>x.ArticleProductType == ArticleProductType.InstallServiceMarging || x.ArticleProductType == ArticleProductType.InstallationService);
            foreach (var article in systemArticles)
            {
                for (var i = 0; i < article.ArticleData.Qty; i++)
                {
                    var item = new OrderLine
                    {
                        ArticleId = article.ArticleData.ArticleNum,
                        Name = article.Title,
                        Price = article.ArticleData.Price,
                        ReferToArticle = article.ArticleData.RefersToItem.ToString(),
                        ArticleType = article.ArticleProductType.ToString(),
                    };
                    result.Add(item);
                }
            }
            return result;
        }

    
    }
}
