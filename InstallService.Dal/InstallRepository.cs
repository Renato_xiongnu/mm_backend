﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using InstallService.Dal.OrderService;
using InstallService.Dal.ProductBackendService;
using InstallService.Model;
using NLog;

namespace InstallService.Dal
{
    public class InstallRepository: IInstallRepository
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public async Task CreatInstallationsForOrderAsync(String orderId)
        {
            using (
                var tr = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = CreateContext())
                {
                    var exist =
                        await
                            ctx.Orders.FirstOrDefaultAsync(
                                x => x.OrderId.Equals(orderId, StringComparison.InvariantCultureIgnoreCase));
                    if (exist != null)
                        return;
                    var order = await GetOrderInfoFromServiceAsync(orderId);
                    if (order.ShouldBeIgnored)
                    {
                        Log.Info("Ignoring order {0}, because it SocialCard", order.OrderId);
                        return;
                    }
                    if (order.Items.All(el => el.ArticleType != ArticleProductType.InstallationService.ToString()))
                    {
                        Log.Info("Ignoring order {0}, because it have no installation service articles", order.OrderId);
                        return;
                    }
                    var itemsWithoutTitle = order.Items.Where(el => string.IsNullOrEmpty(el.Name)).ToList();
                    if (itemsWithoutTitle.Any())
                    {
                        using (var client = new OrdersBackendServiceClient())
                        {
                            var articleStock = client.GetArticleStockStatus(new GetItemsRequest()
                            {
                                Articles = itemsWithoutTitle.Select(el => el.ArticleId).Distinct().ToArray(),
                                Channel = "MM",
                                SaleLocation = "shop_R002"
                            });
                            foreach (var ordersBackendItem in articleStock.Items)
                            {
                                var items =
                                    itemsWithoutTitle.Where(
                                        t => t.ArticleId == ordersBackendItem.Article.ToString());
                                foreach (var item in items)
                                {
                                    if (item != null)
                                    {
                                        item.Name = ordersBackendItem.Title;
                                    }
                                }
                            }
                        }
                    }

                    ctx.Orders.Add(order);
                    await ctx.SaveChangesAsync();
                }
                tr.Complete(); //Optimistic, if needed
            }
        }

        private IQueryable<Order> CreateSearchQuery(SearchQuery query, InstallDbContext ctx)
        {
            var orders = ctx.Orders.Include(x => x.Items).AsQueryable();
            if (query == null) return orders;
            if (!string.IsNullOrEmpty(query.OrderId))
            {
                orders = orders.Where(x => x.OrderId == query.OrderId);
            }
            if (!string.IsNullOrEmpty(query.WwsOrderId))
            {
                orders = orders.Where(x => x.WwsOrderId == query.WwsOrderId);
            }
            if (!string.IsNullOrEmpty(query.Phone))
            {
                orders = orders.Where(x => x.Phone == query.Phone);
            }
            if (query.DeliveryDate.HasValue)
            {
                orders = orders.Where(x => x.DeliveryDate == query.DeliveryDate);
            }
            if (query.Parameters != null)
            {
                orders = orders.Where(x => query.Parameters.Contains(x.SapCode));
            }
            return orders.OrderByDescending(el => el.CreatedDate);
        }

        public async Task<Order> SearchInstallationsAsync(SearchQuery query)
        {
            using (var ctx = CreateContext())
            {
                var orders = CreateSearchQuery(query, ctx);
                return await orders.FirstOrDefaultAsync();
            }
        }

        public async Task CancelOrderAsync(string orderId, string reason)
        {
            using (
                var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = CreateContext())
                {
                    var exist =
                        await
                            ctx.Orders.Include(x => x.Items)
                                .FirstOrDefaultAsync(
                                    x => x.OrderId.Equals(orderId, StringComparison.InvariantCultureIgnoreCase));
                    if (exist != null)
                        foreach (var item in exist.Items)
                        {
                            item.Status = InstallationStatus.Canceled;
                            item.StatusReason = reason;

                            ctx.Logs.Add(new LogItem
                            {
                                InstallationId = item.Id,
                                LogCreatedDate = DateTime.UtcNow,
                                NewStatus = InstallationStatus.Canceled,
                                NewStatusReason = item.StatusReason,
                                UserName = "external"
                            });
                        }


                    await ctx.SaveChangesAsync();
                    ts.Complete();
                }
            }
        }

        public async Task UpdateInstallationAsync(int id, DateTime? newApprovedDate, DateTime? newFactDate,
            string newInstallationAddress, InstallationStatus? newStatus, string newStatusReason, string updatedBy)
        {
            using (
                var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            using (var ctx = CreateContext())
            {
                var exist =
                    await
                        ctx.Installations.Include(t => t.Order)
                            .FirstOrDefaultAsync(
                                x => x.Id == id);
                if (exist == null) throw new ArgumentException("id");
                exist.ApprovedDate = newApprovedDate;
                exist.FactDate = newFactDate;
                exist.Order.InstallationAddress = newInstallationAddress;
                if (newStatus.HasValue)
                {
                    exist.Status = newStatus.Value;
                }
                exist.StatusReason = newStatusReason;

                ctx.Logs.Add(new LogItem
                {
                    LogCreatedDate = DateTime.UtcNow,
                    InstallationId = exist.Id,
                    NewApprovedDate = newApprovedDate,
                    NewFactDate = newFactDate,
                    NewInstallationAddress = newInstallationAddress,
                    NewStatus = newStatus,
                    NewStatusReason = newStatusReason,
                    UserName = updatedBy
                });

                await ctx.SaveChangesAsync();

                ts.Complete();
            }
        }

        public async Task<List<Order>> All(SearchQuery searchQuery)
        {
            using (var ctx = CreateContext())
            {
                return await ctx.Orders.Include(t=>t.Items).Where(el=>searchQuery.Parameters.Contains(el.SapCode)).ToListAsync();
            }
        }

        private async Task<Order> GetOrderInfoFromServiceAsync(String orderId)
        {
            GerOrderDataResult orderFromService;
            using (var client = new OrderServiceClient())
            {
                orderFromService = await client.GerOrderDataByOrderIdAsync(orderId);
                if (orderFromService.ReturnCode == ReturnCode.Error)
                    throw new Exception(String.Format("Order service returns error: {0}",orderFromService.ErrorMessage));
            }
            var order = orderFromService.GetOrderDataResultToOrder();
            return order;
        }

        private InstallDbContext CreateContext()
        {
            return new InstallDbContext();
        }
    }
}
