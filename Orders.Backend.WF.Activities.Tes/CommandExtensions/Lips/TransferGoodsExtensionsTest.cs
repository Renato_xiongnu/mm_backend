﻿using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.WF.Activities.CommandExtensions.Lips;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.Test.CommandExtensions.Lips
{
    [TestClass]
    public class TransferGoodsExtensionsTest
    {
        [TestMethod]
        public void TestToMultipleTransferOrderLine()
        {
            var orderItem = new OrderItem();
            orderItem.Number = "1234567";
            orderItem.Title = "Iphone";
            orderItem.Price = 20000;
            orderItem.Quantity = 1;

            var transferLine = TransferGoodsExtensions.ToMultipleTransferOrderLine(orderItem, new Order(), "1234567",
                "R007").ToList();

            Assert.AreEqual(1, transferLine.Count());
            Assert.AreEqual("R007", transferLine.First().SapCode);
            Assert.AreEqual("1234567", transferLine.First().WWSOrderId);
            Assert.AreEqual("MM_stock_R007", transferLine.First().StockLocation);
        }

        [TestMethod]
        public void TestToMultipleTransferOrderLineSets()
        {
            var orderItem = new OrderItem();
            orderItem.Number = "1234567";
            orderItem.Title = "Iphone";
            orderItem.Price = 20000;
            orderItem.Quantity = 1;
            orderItem.SubOrderItems = new Collection<OrderItem>
            {
                new OrderItem() {Number="1234555", Quantity = 1, TransferSapCode = "R006"},
                new OrderItem() {Number="12345557",Quantity = 1, TransferSapCode = "R006"}
            };

            var transferLine = TransferGoodsExtensions.ToMultipleTransferOrderLine(orderItem, new Order(), "1234567",
                "R007").ToList();

            Assert.AreEqual(2, transferLine.Count());
            Assert.AreEqual("R007", transferLine.First().SapCode);
            Assert.AreEqual("1234567", transferLine.First().WWSOrderId);
            Assert.AreEqual("MM_stock_R007", transferLine.First().StockLocation);
        }

        [TestMethod]
        public void TestToMultipleTransferOrderLineRemoveDuplicates()
        {
            var orderItem = new OrderItem();
            orderItem.Number = "1234567";
            orderItem.Title = "Iphone";
            orderItem.Price = 20000;
            orderItem.Quantity = 1;
            orderItem.SubOrderItems = new Collection<OrderItem>
            {
                new OrderItem() {Number = "1234555", Quantity = 1, TransferSapCode = "R006"},
                new OrderItem() {Number = "1234555", Quantity = 1, TransferSapCode = "R006"}
            };

            var transferLine = TransferGoodsExtensions.ToMultipleTransferOrderLine(orderItem, new Order(), "1234567",
                "R007").ToList();

            Assert.AreEqual(1, transferLine.Count());
            Assert.AreEqual("R007", transferLine.First().SapCode);
            Assert.AreEqual("1234567", transferLine.First().WWSOrderId);
            Assert.AreEqual("MM_stock_R007", transferLine.First().StockLocation);
        }

        [TestMethod]
        public void TestNoDeliverItems()
        {
            var orderItem = new OrderItem { Number = "1234567", Title = "Iphone", Price = 20000, Quantity = 1 };
            var deliveryItem = new OrderItem
            {
                Number = "12345555",
                Title = "Iphone",
                Price = 20000,
                Quantity = 1,
                ArticleProductType = ArticleProductType.DeliveryService
            };
            var order = new Order
            {
                Items = new Collection<OrderItem>() { orderItem, deliveryItem },
                Customer = new Customer(),
                Store = new Store(),
                Delivery = new Delivery(),
                WwsOrders = new Collection<WwsOrder> { new WwsOrder() }
            };

            var deliverOrder = TransferGoodsExtensions.ToTransferOrder(order, "1231234", "12312312", "R003");

            Assert.AreEqual(1, deliverOrder.Lines.Count);
            Assert.AreEqual(1234567, deliverOrder.Lines.First().ProductNumber);
        }
    }
}
