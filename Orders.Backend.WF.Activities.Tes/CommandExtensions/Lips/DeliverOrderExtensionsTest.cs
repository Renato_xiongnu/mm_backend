﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMS.Cloud.Commands.LIPS;
using Orders.Backend.WF.Activities.CommandExtensions.Lips;
using Orders.Backend.WF.Activities.Model;
using PaymentType = MMS.Cloud.Commands.LIPS.PaymentType;

namespace Orders.Backend.WF.Activities.Test.CommandExtensions.Lips
{
    [TestClass]
    public class DeliverOrderExtensionsTest
    {
        [TestMethod]
        public void ToMultipleOrderLine_Test()
        {
            var orderItem = new OrderItem();
            orderItem.Number = "1234567";
            orderItem.Title = "Iphone";
            orderItem.Price = 20000;
            orderItem.Quantity = 1;
            var order = new Order
            {
                WwsOrders = new Collection<WwsOrder>
                {
                    new WwsOrder
                    {
                        Id = "1234567222",
                        SapCode = "R007"
                    }
                }
            };

            var transferLine = DeliverOrderExtensions.ToMultipleOrderLine(orderItem, order).ToList();

            Assert.AreEqual(1, transferLine.Count());
            Assert.AreEqual("1234567222", transferLine.First().WWSOrderId);
            Assert.AreEqual("R007", transferLine.First().SapCode);
            Assert.AreEqual("MM_stock_R007", transferLine.First().StockLocation);
        }

        [TestMethod]
        public void TestCashMapping()
        {
            var order = new Order
            {
                Items = new Collection<OrderItem>() { new OrderItem { Number = "1111111", Price = 10000m, Quantity = 1 } },
                Customer = new Customer(),
                Store = new Store(),
                Delivery = new Delivery(),
                WwsOrders = new Collection<WwsOrder> { new WwsOrder() }
            };
            order.Payment = Model.PaymentType.Cash;
            var deliverOrder = DeliverOrderExtensions.ToDeliverOrder(order);

            Assert.AreEqual(10000m, deliverOrder.CODAmount);
            Assert.AreEqual(PaymentType.Cod, deliverOrder.PaymentType);
        }

        [TestMethod]
        public void TestOnlinePaymentMapping()
        {
            var order = new Order
            {
                Items = new Collection<OrderItem>(){new OrderItem{Number = "1111111",Price = 10000m,Quantity = 1}},
                Customer = new Customer(),
                Store = new Store(),
                Delivery = new Delivery(),
                WwsOrders = new Collection<WwsOrder> {new WwsOrder()}
            };
            order.Payment=Model.PaymentType.Online;
            var deliverOrder = DeliverOrderExtensions.ToDeliverOrder(order);

            Assert.AreEqual(0m,deliverOrder.CODAmount);
            Assert.AreEqual(PaymentType.Pre,deliverOrder.PaymentType);
        }

        [TestMethod]
        public void TestErrorDescriptionFromConfig()
        {
            var responce = new DeliverOrderResponse {Incidents = new List<Incident>()};
            var incident = new Incident {Code = 10006, Description = "Test"};
            responce.Incidents.Add(incident);

            var msg = responce.PrintIncidientsAsHtml();

            var result = !String.IsNullOrEmpty(msg);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestNoDeliverItems()
        {
            var orderItem = new OrderItem { Number = "1234567", Title = "Iphone", Price = 20000, Quantity = 1 };
            var deliveryItem = new OrderItem
            {
                Number = "12345555",
                Title = "Iphone",
                Price = 20000,
                Quantity = 1,
                ArticleProductType = ArticleProductType.DeliveryService
            };
            var order = new Order
            {
                Items = new Collection<OrderItem>() { orderItem, deliveryItem },
                Customer = new Customer(),
                Store = new Store(),
                Delivery = new Delivery(),
                WwsOrders = new Collection<WwsOrder> { new WwsOrder() }
            };
            
            var deliverOrder = DeliverOrderExtensions.ToDeliverOrder(order);

            Assert.AreEqual(1, deliverOrder.Lines.Count);
            Assert.AreEqual(1234567, deliverOrder.Lines.First().ProductNumber);
        }
    }
}
