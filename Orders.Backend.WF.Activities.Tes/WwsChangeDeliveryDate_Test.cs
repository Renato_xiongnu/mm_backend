﻿using System;
using System.Activities;
using Microsoft.VisualBasic.Activities;
using Microsoft.Activities.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.WF.Activities.Online;
using System.Activities.Expressions;
using MMS.Activities;
using System.Activities.Statements;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.WWS;
using System.Collections.ObjectModel;
using Orders.Backend.WF.Activities.Proxy.WWS;

namespace Orders.Backend.WF.Activities.Test
{
    [TestClass]
    public class WwsChangeDeliveryDate_Test
    {
        [TestMethod]
        public void ChangeDeliveryDateActivityTest()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (Model.PrintingStatus)int.MaxValue,
                Delivery = new Model.Delivery
                {
                    DeliveryDate = DateTime.Today
                },
                WwsOrders = new Collection<Model.WwsOrder>
                {
                    new Model.WwsOrder
                    {
                        Id = "1234567222",
                        SapCode = "R002"
                    }
                }
            };

            var activity = new WwsChangeDeliveryDate
            {
                DisplayName = "ChangeDeliveryDate",
                Order = new InArgument<Model.Order>((ctx) => order),
            };

            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(600));
                //AssertHelper.Throws<TimeoutException>(() => host.TestActivity(TimeSpan.FromSeconds(65)));
                //host.AssertOutArgument.AreNotEqual("Result", typeof(ChangeDeliveryDateResult), null);
            }
            finally
            {
                host.Tracking.Trace();
            }
        }
    }
}
