﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Activities.UnitTesting;
using System.Net.Http;
using System.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Integration.RulesEngine;

namespace Orders.Backend.WF.Activities.Test
{
    [TestClass]
    public class RulesEngineCallActivity_Test
    {
        //[TestMethod]
        //public void RestCallActivity_Test1()
        //{
        //    var url = "http://localhost/Orders.Backend.Rules.Web/api/run/order/validate";
        //    var contentType = "application/json";
        //    var result = RestCallActivity.CallService<string>(url, HttpMethod.Get, contentType).Result;
        //    Assert.AreEqual("Value 2", result);
        //}

        [TestMethod]
        public void RulesEngineCallActivity_Test2()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (Model.PrintingStatus)int.MaxValue,
                Comment = "old comment",
                Prepay = 12.0M,
                CreatedSuccessfully = true,
            };

            var orderContext = new Model.OrderContext
            {
                Order = order,
                ProductAvaliableInStore = false
            };

            var activity = new RulesEngineCallActivity
            {
                Workitem1 = new InArgument<string>(c => "run/OrderContext/OrderCreatedRoute"),
                Workitem2 = new InArgument<Model.OrderContext>(c => orderContext)
            };

            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(60));
                //Assert.AreEqual("Value 2", host.);
                //AssertHelper.Throws<TimeoutException>(() => host.TestActivity(TimeSpan.FromSeconds(65)));
                //host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
                var result = host.Output["Result"] as ExecutingResult<string>;
                Assert.IsNotNull(result);
                Assert.AreEqual(true, result.Success);
                Assert.AreEqual("CreatedByCallCenter", result.Result);
            }
            finally
            {
                host.Tracking.Trace();
            }
        }
    }
}
