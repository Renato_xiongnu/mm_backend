﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orders.Backend.WF.Activities.Test
{
    public class DecisionRule
    {
        public int Order { get; set; }

        public string Condition { get; set; }

        public string TargetParameter { get; set; }

        public string Value { get; set; }
    }
}
