﻿using Microsoft.VisualBasic.Activities;
using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Test
{
    public static class RulesExtensions
    {
        public static If ToActivity(If activity, DecisionRule rule)
        {
            var condition = new VisualBasicValue<bool>(rule.Condition);
            var ifActivity = new If(new InArgument<bool>(condition));
            ifActivity.Then = new Assign<string>
            {
                To = new OutArgument<string>(new VisualBasicReference<string>(rule.TargetParameter)),
                Value = new InArgument<string>(rule.Value)
            };
            if (activity != null)
            {
                ifActivity.Else = activity;
            }
            return ifActivity;
        }
    }
}
