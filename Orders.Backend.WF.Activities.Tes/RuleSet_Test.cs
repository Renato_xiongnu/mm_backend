﻿using System;
using System.Activities;
using Microsoft.VisualBasic.Activities;
using Microsoft.Activities.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.WF.Activities.Online;
using System.Activities.Expressions;
using MMS.Activities;
using System.Activities.Statements;
using System.IO;
using System.Linq;
using System.ServiceModel.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.WWS;
using System.ServiceModel.Activities.Description;
using Microsoft.Activities.UnitTesting.Persistence;
using System.Threading;
using Microsoft.Activities.Extensions;
using System.Collections.Generic;

namespace Orders.Backend.WF.Activities.Test
{
    [TestClass]
    public class RuleSet_Test
    {
        [TestMethod]
        public void RuleSet_IfElse()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (Model.PrintingStatus)int.MaxValue,
                Comment = "old comment",
                Prepay = 12.0M
            };

            var rules = new DecisionRule[]
            {
                new DecisionRule { Order = 0, Condition = "Order.Prepay <= 0", TargetParameter = "Outcome", Value = "Value1" },
                new DecisionRule { Order = 1, Condition = "Order.Prepay > 0 And Order.Prepay < 20", TargetParameter = "Outcome", Value = "Value2" },
                new DecisionRule { Order = 2, Condition = "true", TargetParameter = "Outcome", Value = "Value3" },
            };

            var activity = new DynamicRulesActivity<Model.Order>(rules);
            var arguments = new Dictionary<string, object>();
            arguments.Add("Order", order);
            var host = WorkflowInvokerTest.Create(activity, arguments);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(5));
                //AssertHelper.Throws<TimeoutException>(() => host.TestActivity(TimeSpan.FromSeconds(65)));
                //host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            }
        }
    }

    public class DynamicRulesActivity<T> : Activity
    {
        public InArgument<T> Order { get; set; }

        public OutArgument<string> Outcome { get; set; }

        public DynamicRulesActivity(IEnumerable<DecisionRule> rules)
        {
            var settings = new VisualBasicSettings
            {
                ImportReferences = 
                {
                    new VisualBasicImportReference { Assembly = typeof(string).Assembly.GetName().Name, Import = typeof(string).Namespace },
                    new VisualBasicImportReference { Assembly = typeof(T).Assembly.GetName().Name, Import = typeof(T).Namespace }
                }
            };
            VisualBasic.SetSettings(this, settings);

            // Order from bootom to top
            var ifActivity = rules.OrderByDescending(r => r.Order).Aggregate<DecisionRule, If>(null, RulesExtensions.ToActivity);
            this.Implementation = () => ifActivity;            
        }
    }
}
