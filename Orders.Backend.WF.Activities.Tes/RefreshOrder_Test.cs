﻿using System;
using System.Activities;
using Microsoft.VisualBasic.Activities;
using Microsoft.Activities.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.WF.Activities.Online;
using System.Activities.Expressions;
using MMS.Activities;
using System.Activities.Statements;
using Orders.Backend.WF.Activities.Common;

namespace Orders.Backend.WF.Activities.Test
{
    [TestClass]
    public class RefreshOrder_Test
    {
        [TestMethod]
        public void Failover_Fail_Test()
        {
            var activity = new FailoverFix
            {
                DisplayName = "Failover",
                Delegate = new InvokeDelegate
                {
                    Delegate = new ActivityAction()
                    {
                        Handler = new Sequence
                        {
                            Activities =
                            {
                                new TestCodeActivity { DisplayName = "TestCodeActivity 1"},
                                new TestCodeActivity { DisplayName = "TestCodeActivity 2"},
                                new TestCodeActivity { DisplayName = "TestCodeActivity 3"},
                            }
                        },
                    }
                }
            };

            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(65));
                host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            }
        }

        [TestMethod]
        public void FailoverWithCancel_Fail_Test()
        {
            var activity = new FailoverWithCancel
            {
                DisplayName = "Failover",
                Delegate = new InvokeDelegate
                {
                    Delegate = new ActivityAction()
                    {
                        Handler = new Sequence
                        {
                            Activities =
                            {
                                new TestCodeActivity { DisplayName = "TestCodeActivity 1"},
                                new TestCodeActivity { DisplayName = "TestCodeActivity 2"},
                                new TestCodeActivity { DisplayName = "TestCodeActivity 3"},
                            }
                        },
                    }
                }
            };

            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(65));
                host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            }
        }

        [TestMethod]
        public void Retry_Fail_Test()
        {
            var activity = new Retry
            {
                MaxRetries = 100,
                RetryDelay = TimeSpan.FromSeconds(10),
                Body = new Sequence
                {
                    Activities =
                    {
                        new TestCodeActivity { DisplayName = "TestCodeActivity 1"},
                        new TestCodeActivity { DisplayName = "TestCodeActivity 2"},
                        new TestCodeActivity { DisplayName = "TestCodeActivity 3"},
                    }
                },
            };
            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(22));
                host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            }
        }


        [TestMethod]
        public void RefreshOrder_Fail_Test()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (Model.PrintingStatus)int.MaxValue,
            };
            var activity = new RefreshOrder
            {
                DisplayName = "RefreshOrder",
                Order = new InArgument<Model.Order>((ctx) => order),
            };
            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("Record [{0}] = {1}", tr.RecordNumber, tr.EventTime);
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(65));
                //host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            } 
        }
    }
}
