﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities.Extensions;
using MMS.Activities.Properties;

namespace Orders.Backend.WF.Activities.Test
{
    [ToolboxItem(false)]
    public class FailoverFix : NativeActivity<bool>
    {        
        private ActivityAction _delay;

        [Browsable(false)]
        public InvokeDelegate Delegate { get; set; }

        private ActivityAction Delay
        {
            get
            {
                return _delay ?? (_delay = new ActivityAction
                {
                    Handler = new Delay
                    {
                        Duration = new InArgument<TimeSpan>(TimeSpan.FromSeconds(60))
                    }
                });
            }
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            metadata.AddChild(Delegate);
            metadata.AddImplementationDelegate(Delay);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleActivity(Delegate, OnCompleted, OnFaulted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            Result.Set(context, true);
        }

        private void OnFaulted(NativeActivityFaultContext faultContext, Exception propagatedException,
            ActivityInstance propagatedFrom)
        {
            Result.Set(faultContext, false);
            faultContext.LogErrorFormat("{0} faulted", propagatedException,
                string.IsNullOrEmpty(DisplayName) ? "Failover" : DisplayName);
            HandleFault(faultContext, propagatedException, propagatedFrom);            
        }

        protected virtual void HandleFault(NativeActivityFaultContext faultContext, Exception propagatedException, ActivityInstance propagatedFrom)
        {
            faultContext.CancelChild(propagatedFrom);
            faultContext.HandleFault();
            faultContext.ScheduleAction(Delay, OnDelayCompleted);
        }

        private void OnDelayCompleted(NativeActivityContext context, ActivityInstance completedInstance)
        {
            context.ScheduleActivity(Delegate, OnCompleted, OnFaulted);
        }
    }
}