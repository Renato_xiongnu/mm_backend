﻿using System;
using System.Activities;
using Microsoft.VisualBasic.Activities;
using Microsoft.Activities.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.WF.Activities.Online;
using System.Activities.Expressions;
using MMS.Activities;
using System.Activities.Statements;
using System.IO;
using System.Linq;
using System.ServiceModel.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.WWS;
using System.ServiceModel.Activities.Description;
using Microsoft.Activities.UnitTesting.Persistence;
using System.Threading;
using Microsoft.Activities.Extensions;
using OnlineOrders.Common.Helpers;

namespace Orders.Backend.WF.Activities.Test
{
    [TestClass]
    public class AssignComment_Test
    {
        [TestMethod]
        public void AssignComment_1()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (Model.PrintingStatus)int.MaxValue,
                Comment = "old comment"
            };
            var comment = CommentHelper.FormatComment("new comment", order.Comment, "manager_R202");
            var activity = new Assign
            {
                Value = new InArgument<string>(ctx => comment),
                To = new OutArgument<string>((ctx) => order.Comment)
            };

            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(5));
                Assert.AreEqual(comment, order.Comment);
                //AssertHelper.Throws<TimeoutException>(() => host.TestActivity(TimeSpan.FromSeconds(65)));
                //host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            }

        }
    }
}
