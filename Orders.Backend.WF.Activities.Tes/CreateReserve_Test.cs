﻿using System;
using System.Activities;
using Microsoft.VisualBasic.Activities;
using Microsoft.Activities.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.WF.Activities.Online;
using System.Activities.Expressions;
using MMS.Activities;
using System.Activities.Statements;
using System.IO;
using System.Linq;
using System.ServiceModel.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.WWS;
using System.ServiceModel.Activities.Description;
using Microsoft.Activities.UnitTesting.Persistence;
using System.Threading;
using Microsoft.Activities.Extensions;

namespace Orders.Backend.WF.Activities.Test
{
    [TestClass]
    public class CreateReserve_Test
    {
        [TestMethod]
        public void CreateReserve_Fail_Test()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (Model.PrintingStatus)int.MaxValue,
            };

            var activity = new CreateReserve
            {
                DisplayName = "Failover",
                Order = new InArgument<Model.Order>((ctx) => order),
                TicketId = new InArgument<string>(ctx => "11876"),
                CorrelationHandle = new InArgument<CorrelationHandle>(ctx => new CorrelationHandle())
            };

            var host = WorkflowInvokerTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[{0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            try
            {
                host.TestActivity(TimeSpan.FromSeconds(65));
                //AssertHelper.Throws<TimeoutException>(() => host.TestActivity(TimeSpan.FromSeconds(65)));
                //host.AssertOutArgument.AreEqual("Result", "FakeMyCustomActivity: Test");
            }
            finally
            {
                host.Tracking.Trace();
            }
        }

        [TestMethod]
        public void CreateReserve_Persistence_Test()
        {
            var order = new Model.Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = Model.PrintingStatus.None,
            };

            var activity = new CreateReserve
            {
                DisplayName = "Failover",
                Order = new InArgument<Model.Order>((ctx) => order),
                TicketId = new InArgument<string>(ctx => "11876"),
                CorrelationHandle = new InArgument<CorrelationHandle>(ctx => new CorrelationHandle())
            };
            var store = new MemoryStore();
            Guid hostId = Guid.Empty;
            var host = WorkflowApplicationTest.Create(activity);
            host.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[HOST1 {0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            host.InstanceStore = store;

            var host2 = WorkflowApplicationTest.Create(activity);
            host2.Tracking.OnTrack = (tr, ts) => System.Diagnostics.Debug.WriteLine("[HOST2 {0}] [{1}] {2}", tr.RecordNumber, tr.EventTime, tr.ToString());
            host2.InstanceStore = store;

            try
            {                
                host.TestActivity();
                hostId = host.Id;
                Thread.Sleep(200);
                //host.WaitForIdleEvent();
                //host.WaitForAbortedEvent();
                //Thread.Sleep(TimeSpan.FromSeconds(5));
                
                //host.Persist();
                host.Unload();

                host2.Load(hostId, TimeSpan.FromSeconds(5));
                host2.TestActivity();
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
            finally
            {
                
                MemoryStore.DisplayCommandCounts();
                host.Tracking.Trace();
                host2.Tracking.Trace();
            }
        }
    }
}