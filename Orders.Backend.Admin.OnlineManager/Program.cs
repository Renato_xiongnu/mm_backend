﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Helpers;

using Orders.Backend.Services;
using Orders.Backend.Services.Contracts.OrderService;


namespace Orders.Backend.Admin.OnlineManager
{    
    
    class Program
    {
        private const int actionTypeIndex = 0;
        private const int sapIndex = 1;
        private const int certErrorOpIndex = 2;
        private const int certDelimeterIndex = 3;

        private const int ConfirmOrderIdIndex = 1;
        private const int ConfirmWWSOrderIndex = 2;

#if DEBUG
        private const string CECTestInputstr = "CEC R206 recredit c 5312013313514";//5312013312888";//5312013313464"; //5312013313142

        private const string SCCInputstr = "SCC 1245535";

        private const string SCERTInputstr = "SCERT 10207385111";
#endif        

        private static void TestWork()
        {
            //OrderManager manager = new OrderManager();
            /*manager.UpdateOrder(GeneralOrderStatus.Confirmed, new UpdateOrderData()
                                                                 {
                                                                     OrderId = "202-176-132",
                                                                     ReasonText = "",
                                                                     UpdatedBy = "Козлова Марина",
                                                                     WWSOrderId = "40755263"

                                                                 });*/
            //manager.NotifyCustomer("202-176-132");
          //  manager.CreateOrder(new ClientData(), )
        }

        
        static void Main(string[] args)
        {
          //  TestWork();
          //  return;
#if DEBUG
            //args = CECTestInputstr.Split(' ');
            //args = SCCInputstr.Split(' ');
            args = SCERTInputstr.Split(' ');
#endif

            //TODO:  Add help output when not arguments found
            if (args.Length == 0)
            {
                Console.WriteLine("You must specify the certificates ids");
                return;
            }

            ActionType action = default(ActionType);
            try
            {
                action = args[actionTypeIndex].ToEnum<ActionType>();
            }
            catch
            {
                Console.WriteLine("The Command {0} does not specify", args[actionTypeIndex]);
            }

            try
            {
                switch (action)
                {
                    case ActionType.CEC:
                        CertificateErrorCorrection(args);
                        break;
                    case ActionType.SCC:
                        ReconfirmeOrder(args);
                        break;
                    case ActionType.SCERT:
                        ResentCert(args);
                        break;
                    default:
                        Console.WriteLine("The Command {0} does not specify", action.ToString());
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An exception has occurred {0}", ex);
            }

            Console.ReadKey();
        }

        private static void ResentCert(string[] args)
        {
            var orderId = args[ConfirmOrderIdIndex];

            string message;
            var result = new OrderAdmin().ResentCert(orderId, out message);

            if (result)
                Console.WriteLine(string.Format("ResendCert Command completed successfully {0}", message));
            else
                Console.WriteLine(string.Format("ResendCert Command not completed {0}", message));

        }

        private static void ReconfirmeOrder(string[] args)
        {
            if (args.Length < ConfirmOrderIdIndex)
            {
                Console.WriteLine("You need specify {0} parameters", ConfirmWWSOrderIndex);
                return;
            }

            var orderId = args[ConfirmOrderIdIndex];

            var wwsOrderId = args.Length > ConfirmWWSOrderIndex ? args[ConfirmWWSOrderIndex] : string.Empty;

            string message;
            var result = new OrderAdmin().ReconfirmeOrder(orderId, wwsOrderId, out message);
            if (result)
                Console.WriteLine(string.Format("Reconfirme Command completed successfully {0}", message));
            else
                Console.WriteLine(string.Format("Reconfirme Command not completed {0}", message));
        }

        private static void CertificateErrorCorrection(string[] args)
        {
            if (args.Length < certDelimeterIndex + 1)
            {
                Console.WriteLine("You need specify more than {0} parameters", certDelimeterIndex + 1);
                return;
            }
            var sapCode = args[sapIndex];

            if (string.IsNullOrEmpty(sapCode))
            {
                Console.WriteLine("You must specify sapCode number");
                return;
            }

            var certOp = args[certErrorOpIndex];
            bool fix;
            if (certOp == "recredit")
            {
                fix = true;
            }
            else if (certOp == "cancel")
            {
                fix = false;
            }
            else
            {
                Console.WriteLine("The must specify action with certificates ids");
                return;
            }

            var certDelimeter = args[certDelimeterIndex];
            if (certDelimeter != "c")
            {
                Console.WriteLine("You must specify the certificates ids after keychar \"c\"");
                return;
            }

            var certs = new List<string>();
            for (int i = certDelimeterIndex + 1; i < args.Length; i++)
            {
                certs.Add(args[i]);
            }

            if (certs.Count == 0)
            {
                Console.WriteLine("You must specify the certificates ids");
                return;
            }

            var manager = new OrderAdmin();

            if (fix)
                manager.RecreditFailedCert(sapCode, certs);
            else
                manager.RejectFailedCert(sapCode, certs);


            Console.WriteLine("Command completed successfully");
        }

    }
}
