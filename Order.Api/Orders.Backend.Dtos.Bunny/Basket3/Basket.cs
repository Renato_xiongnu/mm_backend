﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Orders.Backend.Contracts.Bunny.Basket3
{
    public class Basket : IValidatableObject
    {
        [Required]
        public string BasketId { get; set; }

        public string CallCenterAgentId { get; set; }

        [Required]
        public string Channel { get; set; }

        public string CouponCode { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhone { get; set; }

        [Required]
        public string CustomerSurname { get; set; }

        public string CustomerUserId { get; set; }

        public string DeliveryAddress { get; set; }

        public int? DeliveryFloor { get; set; }

        public string DeliveryInterval { get; set; }

        public string DeliveryOfferId { get; set; }

        public string DeliveryType { get; set; }

        public bool IsTestOrder { get; set; }

        public string OrderId { get; set; }

        [Required]
        public string OrderSource { get; set; }

        [Required]
        public string PaymentTypeId { get; set; }

        [Required]
        public string SaleLocationId { get; set; }

        public string SessionId { get; set; }        
        
        public string PickupLocationId { get; set; }

        public List<BasketLine> Lines { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string RequestedDeliveryServiceOption { get; set; }

        public string UtmSouce { get; set; }

        public string InstallServiceAddress { get; set; }

  
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Enumerable.Empty<ValidationResult>();
        }
    }
}