﻿using System;

namespace Orders.Backend.Contracts.Bunny.Basket3
{
    public class BasketLine
    {
        public long Article { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public string Title { get; set; }

        public string BrandTitle { get; set; }

        public bool IsVirtual { get; set; }

        public decimal? OldPrice { get; set; } 

        public decimal? Benefit { get; set; } 

        public string CouponCompainId { get; set; } 

        public string CouponCode { get; set; }

        public string VirtualType { get; set; }

        public string LineType { get; set; }

        public Int64? RefersToItem { get; set; }   

    }
}