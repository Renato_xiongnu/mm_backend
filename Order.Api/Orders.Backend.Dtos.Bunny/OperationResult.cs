﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

namespace Orders.Backend.Contracts.Bunny
{
    [DataContract]
    public class OperationResult
    {
        public OperationResult()
        {            
            ErrorMessages = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>());
        }

        [DataMember(Name = "status_enum")]
        public OperationStatus Status { get; set; }

        [DataMember(Name = "status")]
        public string StatusName
        {
            get { return Status.ToString(); }
            set { Status = (OperationStatus)Enum.Parse(typeof(OperationStatus), value); }
        }

        private ReadOnlyDictionary<string, string> _readOnlyErrorMessages;
        private Dictionary<string, string> _errorMessages;
        

        public ReadOnlyDictionary<string,string> ErrorMessages
        {
            get { return _readOnlyErrorMessages; }
            set
            {                
                _errorMessages = new Dictionary<string, string>(value);
                _readOnlyErrorMessages = new ReadOnlyDictionary<string, string>(_errorMessages);
            }
        }

        [DataMember(Name = "error_message")]
        public string ErrorMessage
        {
            get
            {
                return string.Join(Environment.NewLine,
                    ErrorMessages.Select(
                        kv => string.IsNullOrEmpty(kv.Value) ? kv.Key : string.Format("{0} : {1}", kv.Key, kv.Value)));
            }
// ReSharper disable once ValueParameterNotUsed
            set { }
        }

        public void AddError(string code, string error = "")
        {
            _errorMessages[code] = error;
        }
    }
}