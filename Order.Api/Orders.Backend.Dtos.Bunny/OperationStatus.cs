﻿using System.Runtime.Serialization;

namespace Orders.Backend.Contracts.Bunny
{
    [DataContract(Name = "Status")]
    public enum OperationStatus
    {
        [EnumMember(Value = "Ok")]
        Ok,

        [EnumMember(Value = "Error")]
        Error
    }
}