﻿using System.Runtime.Serialization;

namespace Orders.Backend.Contracts.Bunny
{
    [DataContract]
    public class UpdateOrderRequest
    {
        [DataMember]
        public string BasketId { get; set; }

        [DataMember]
        public string OrderId { get; set; }
    }
}