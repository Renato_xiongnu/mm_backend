﻿using System.Runtime.Serialization;

namespace Orders.Backend.Contracts.Bunny
{
    [DataContract]
    public class CreateOrderRequest
    {
        [DataMember]
        public string BasketId { get; set; }        
    }
}