﻿using System.Runtime.Serialization;

namespace Orders.Backend.Contracts.Bunny
{
    [DataContract]
    public class CreateOrderResult : OperationResult
    {
        [DataMember(Name = "order_id")]
        public string OrderId { get; set; }
    }
}