﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ServiceModel.Web;
using Common.Logging;
using Orders.Backend.Bunny.Dal;
using Orders.Backend.BunnyServices.IdGenerator;
using Orders.Backend.BunnyServices.Queue;
using Orders.Backend.BunnyServices.Strategy;
using Orders.Backend.BunnyServices.Validators;
using Orders.Backend.Contracts.Bunny;
using OperationStatus = Orders.Backend.Contracts.Bunny.OperationStatus;

namespace Orders.Backend.Basket.Services
{
    public class OrderService : IOrderService
    {
        private readonly ILog _logger = LogManager.GetLogger("Basket.OrderService");

        private readonly IOrdersStrategyFactory _ordersStrategyFactory;

        private readonly BasketValidator _basketValidator;

        private readonly BasketToOrderQueueProvider _basketQueueProvider;

        private readonly IIdGenerator _generator;

        private IOrderStrategy GetOrderStrategy(string channelId)
        {
            return _ordersStrategyFactory.CreateStrategy(channelId);
        }

        public OrderService(IOrdersStrategyFactory ordersStrategyFactory, BasketValidator basketValidator, BasketToOrderQueueProvider
            basketQueueProvider, IIdGenerator generator)
        {
            _ordersStrategyFactory = ordersStrategyFactory;
            _basketValidator = basketValidator;
            _basketQueueProvider = basketQueueProvider;
            _generator = generator;
        }

        // ReSharper disable once InconsistentNaming
        public CreateOrderResult CreateOrder(string basket3_basket_id, string channel, string sale_location_id, string order_id)
        {
            try
            {
                var queueElement = _basketQueueProvider.GetByBasketId(basket3_basket_id);
                if (queueElement != null)
                {
                    return new CreateOrderResult
                    {
                        Status = OperationStatus.Ok,
                        OrderId = queueElement.OrderId
                    };
                }

                _logger.TraceFormat("Start create order : BasketId = {0}", basket3_basket_id);
                if (string.IsNullOrEmpty(channel) || string.IsNullOrEmpty(sale_location_id))
                {
                    var basket = new BasketMultiplexorWrapper().GetChannelAndLocation(basket3_basket_id);
                    channel = basket.Channel;
                    sale_location_id = basket.SaleLocationId;
                }

                var orderId = string.IsNullOrEmpty(order_id)
                    ? _generator.GenerateId(channel, sale_location_id)
                    : order_id;
                queueElement = BasketToOrderQueueElement.CreateElement(basket3_basket_id, orderId);
                _basketQueueProvider.Save(queueElement);

                return new CreateOrderResult
                {
                    Status = OperationStatus.Ok,
                    OrderId = orderId
                };
            }
            catch(Exception ex)
            {
                var result = new CreateOrderResult
                {
                    Status = OperationStatus.Error,                    
                };
                SetInternalError();
                _logger.ErrorFormat("Error during creating order: BasketId = {0} , Message = {1}", ex,
                    basket3_basket_id, ex.Message);                
                result.AddError(ex.Message);
                return result;
            }
        }

        // ReSharper disable InconsistentNaming
        public UpdateOrderResult UpdateOrder(string basket3_basket_id, string order_id)
        {
            try
            {
                _logger.TraceFormat("Start update order : BasketId = {0}, OrderId = {1}", basket3_basket_id,
                    order_id);
                var basket = new BasketMultiplexorWrapper().GetBasket(basket3_basket_id);
                ICollection<ValidationResult> validationResults = new Collection<ValidationResult>();                
                if (!_basketValidator.Validate(basket, ref validationResults))
                {
                    var createOrderResult = new UpdateOrderResult
                    {
                        Status = OperationStatus.Error,
                    };
                    foreach (var validationResult in validationResults)
                    {
                        createOrderResult.AddError(validationResult.ErrorMessage);
                    }
                    SetBadRequest();
                    return createOrderResult;
                }
                var orderStrategy = GetOrderStrategy(basket.Channel);
                var result = orderStrategy.UpdateOrder(basket);
                if(result.Status == OperationStatus.Ok)
                {
                    _logger.TraceFormat("Order updated : BasketId = {0}, OrderId = {1}", basket3_basket_id,
                        order_id);
                }
                else
                {
                    SetInternalError();
                    _logger.TraceFormat("Order not updated : BasketId = ьу{0}, OrderId = {1}, Message = {2} ",
                        basket3_basket_id, order_id, "");
                }
                return result;
            }
            catch(Exception ex)
            {
                var result = new UpdateOrderResult
                {
                    Status = OperationStatus.Error
                };
                SetInternalError();

                _logger.ErrorFormat("Error during updating order: BasketId = {0}, OrderId = {1}, Message = {2}", ex,
                        basket3_basket_id, order_id, ex.Message);
                result.AddError(ex.Message);
                return result;
            }
        }

        private void SetInternalError()
        {
            var ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
        }

        private void SetBadRequest()
        {
            var ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.BadRequest;
        }
    }
}