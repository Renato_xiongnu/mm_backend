﻿using System;
using System.CodeDom;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Orders.Backend.Contracts.Bunny;

namespace Orders.Backend.Basket.Services
{
    [ServiceContract]    
    public interface IOrderService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/order.create",
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
// ReSharper disable once InconsistentNaming
        CreateOrderResult CreateOrder(string basket3_basket_id, string channel, string sale_location_id, string order_id);

        [OperationContract]
        [WebInvoke(UriTemplate = "/order.update",
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        // ReSharper disable once InconsistentNaming
        UpdateOrderResult UpdateOrder(string basket3_basket_id, string order_id);
    }   
}