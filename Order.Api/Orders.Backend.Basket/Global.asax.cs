﻿using System;
using System.Configuration;
using System.Data.Metadata.Edm;
using System.Timers;
using System.Web;
using Autofac;
using Autofac.Configuration;
using Autofac.Integration.Wcf;
using Common.Logging;
using Orders.Backend.BunnyServices.Properties;
using Orders.Backend.BunnyServices.Queue;

namespace Orders.Backend.Basket
{
    public class Global : HttpApplication
    {
        private ILog _logger;

        protected void Application_Start(object sender, EventArgs e)
        {
            _logger = LogManager.GetLogger("logger");
            var builder = new ContainerBuilder();
            var module = new ConfigurationSettingsReader("autofac");
            builder.RegisterModule(module);
            AutofacHostFactory.Container = builder.Build();

            var createOrders = ConfigurationManager.AppSettings["CreateOrders"];
            if (!string.IsNullOrEmpty(createOrders) && createOrders == "true")
            {
                var timer = new Timer {AutoReset = true, Interval = Settings.Default.DelayBeforeCall.TotalMilliseconds};
                timer.Start();
                timer.Elapsed += timer_Elapsed;
                _logger.Info("Start creating orders in backend");
            }
            else
            {
                _logger.Info("Not start creating orders in backend");
            }

            _logger.Info("Appliction started");
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                new BasketToOrderQueueProcessor(new BasketToOrderQueueProvider()).ProcessBasketToOrderQueue();
            }
            catch (Exception ex)
            {
                _logger.Fatal(ex);
                throw;
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            _logger.Info("Application stopped");
        }
    }
}