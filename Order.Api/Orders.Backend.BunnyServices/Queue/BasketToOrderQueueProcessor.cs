﻿using System;
using Common.Logging;
using Orders.Backend.Bunny.Dal;
using Orders.Backend.BunnyServices.Properties;
using Orders.Backend.BunnyServices.Strategy;
using Orders.Backend.Contracts.Bunny;

namespace Orders.Backend.BunnyServices.Queue
{
    public class BasketToOrderQueueProcessor
    {
        private static object _lockObject = new object();
        private readonly BasketToOrderQueueProvider _basketToOrderQueueProvider;
        private static ILog _logger = LogManager.GetLogger("BasketToOrderQueue");

        public BasketToOrderQueueProcessor(BasketToOrderQueueProvider queueProvider)
        {
            _basketToOrderQueueProvider = queueProvider;
        }

        public void ProcessBasketToOrderQueue()
        {
            lock (_lockObject)
            {
                _logger.TraceFormat("Start processing");
                var unprocessedOrders = _basketToOrderQueueProvider.GetUnprocessedBaskets();
                foreach (var basketToOrderQueueElement in unprocessedOrders)
                {
                    _logger.TraceFormat("Start creating order {0} for basker {1}", basketToOrderQueueElement.OrderId,
                        basketToOrderQueueElement.BasketId);
                    var strategy = new MMOrderStrategy();

                    try
                    {
                        if (RejectCreationOnMaxTryCount(basketToOrderQueueElement)) continue;

                        var createOrderResult = strategy.CreateOrder(basketToOrderQueueElement.BasketId,
                            basketToOrderQueueElement.OrderId);
                        if (createOrderResult.Status == OperationStatus.Ok)
                        {
                            basketToOrderQueueElement.Complete();
                            _logger.InfoFormat("Order {0} for basket {1} created succesfully",
                                basketToOrderQueueElement.OrderId, basketToOrderQueueElement.BasketId);
                        }
                        else
                        {
                            basketToOrderQueueElement.AddError(createOrderResult.ErrorMessage);
                            _logger.ErrorFormat("Order {0} for basket {1} was not created",
                                basketToOrderQueueElement.OrderId, basketToOrderQueueElement.BasketId);
                        }

                        _basketToOrderQueueProvider.Save(basketToOrderQueueElement);

                    }
                    catch (Exception e)
                    {
                        _logger.ErrorFormat("Fail to create order {0} for basket {1}: {2}", basketToOrderQueueElement.OrderId,
                            basketToOrderQueueElement.BasketId, e);
                        basketToOrderQueueElement.AddError(e.Message);
                        _basketToOrderQueueProvider.Save(basketToOrderQueueElement);
                    }
                }

                _logger.TraceFormat("Finish processing");
            }
        }

        private bool RejectCreationOnMaxTryCount(BasketToOrderQueueElement basketToOrderQueueElement)
        {
            if (basketToOrderQueueElement.TryCount > Settings.Default.MaxTryCount)
            {
                basketToOrderQueueElement.CompleteWithMaxTryCount();
                _basketToOrderQueueProvider.Save(basketToOrderQueueElement);
                return true;
            }
            return false;
        }
    }
}
