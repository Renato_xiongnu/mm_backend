﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Multiplexor.Client;
using MMS.Multiplexor.Client.Dtos.Basket3;
using Newtonsoft.Json;
using Orders.Backend.BunnyServices.Properties;
using Orders.Backend.Contracts.Bunny.Basket3;

namespace Orders.Backend.BunnyServices.Queue
{
    public class BasketMultiplexorWrapper
    {
        private const string DeliveryOptionsError = "basket3.get_delivery_options.precondition_failed";

        public Basket GetBasket(string basketId)
        {
            var multiplexorClient = new MultiplexorClient(Settings.Default.MultiplexorUrl,
                Settings.Default.MultiplexorApiKey, Settings.Default.MultiplexorTimeout);

            multiplexorClient
                .Basket3.GetFields(new GetFieldsRequest { BasketId = basketId })
                .Basket3.GetLines(new GetLinesRequest { BasketId = basketId })
                .Basket3.GetTrackingMarks(new GetTrackingMarksRequest { BasketId = basketId })
                .Basket3.GetDeliveryOptions(new GetDeliveryOptionsRequest { BasketId = basketId });
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults().ToArray();

            var requestGetFieldsResult = executionCommandResults[0].Result;
            var requestGetLinesResult = executionCommandResults[1].Result;
            var requestGetTrackingMarksResult = executionCommandResults[2].Result;
            var requestResultDeliveryOptionsResult = executionCommandResults[3].Result;
            var errors = MuxErrorsProcessing(executionCommandResults);
            if (errors.Count > 0)
            {
                throw new ApplicationException(string.Join("\r\n", errors.Select(e => e.Value)));
            }
            var fields = requestGetFieldsResult.Result as FieldsTo;
            var lines = requestGetLinesResult.Result as ItemTo[];
            var trackingMarks = requestGetTrackingMarksResult.Result as TrackingMarkTo[];
            var deliveryOptions = requestResultDeliveryOptionsResult.Result as DeliveryOptionTo;

            if (fields == null)
            {
                throw new ApplicationException();
            }
            var deliveryInterval = String.Empty;
            if (deliveryOptions != null && deliveryOptions.Options != null && deliveryOptions.Options.InretIntervals != null && deliveryOptions.Options.InretIntervals.Any())
            {
                var option = deliveryOptions.Options.InretIntervals.FirstOrDefault(x => x.DeliveryIntervalId == fields.DeliveryIntervalId);
                if (option != null)
                    deliveryInterval = option.Time;
            }
            var basket = MapToBasket(basketId, fields, lines, trackingMarks, deliveryInterval);
            return basket;
        }

        private Dictionary<string, string> MuxErrorsProcessing(IEnumerable<CommandResult> results)
        {
            var errors = new Dictionary<string, string>();
            foreach (var commandResult in results)
            {
                var cmdResult = commandResult.Result;
                if (cmdResult.Errors == null) 
                    continue;
                foreach (var error in cmdResult.Errors)
                {
                    if (error.Key == DeliveryOptionsError)
                    {
                        continue;
                    }
                    errors[error.Key] = error.Value;
                }
            } 
            return errors;
        }

        public Basket GetChannelAndLocation(string basketId)
        {
            var multiplexorClient = new MultiplexorClient(Settings.Default.MultiplexorUrl,
               Settings.Default.MultiplexorApiKey, Settings.Default.MultiplexorTimeout);

            multiplexorClient
                .Basket3.GetFields(new GetFieldsRequest{BasketId = basketId});
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults().ToArray();
            var requestResult0 = executionCommandResults[0].Result;
            var errors = new Dictionary<string, string>();
            if (requestResult0.Errors != null)
            {
                foreach (var error in requestResult0.Errors)
                {
                    errors[error.Key] = error.Value;
                }
            }
            if (errors.Count > 0)
            {
                throw new ApplicationException(string.Join("\r\n", errors.Select(e => e.Value)));
            }
            var fields = requestResult0.Result as FieldsTo;

            if (fields == null)
            {
                throw new ApplicationException();
            }
            return new Basket
            {
                Channel = fields.Channel,
                SaleLocationId = fields.SaleLocationId
            };
        }

        public Basket MapToBasket(string basketId, FieldsTo fields, ItemTo[] lines, TrackingMarkTo[] trackingMarks, string deliveryInterval)
        {
            var basket = new Basket
            {
                BasketId = basketId,
                CallCenterAgentId = fields.CallCenterAgentId,
                Channel = fields.Channel,
                CouponCode = fields.CouponCode,
                CustomerEmail = fields.CustomerEmail,
                CustomerName = fields.CustomerName,
                CustomerPhone = fields.CustomerPhone,
                CustomerSurname = fields.CustomerSurname,
                CustomerUserId = fields.CustomerUserId,
                DeliveryAddress = fields.DeliveryAddress,
                DeliveryFloor = fields.DeliveryFloor,
                DeliveryInterval = String.IsNullOrEmpty(deliveryInterval)? null : deliveryInterval,
                DeliveryOfferId = fields.DeliveryOfferId,
                DeliveryType = fields.DeliveryType,
                IsTestOrder = fields.IsTestOrder,
                OrderId = fields.OrderId,
                OrderSource = fields.OrderSource,
                PaymentTypeId = fields.PaymentTypeId,
                SaleLocationId = fields.SaleLocationId,
                SessionId = fields.SessionId,
                PickupLocationId = fields.PickupLocationId,
                DeliveryDate = fields.DeliveryDate,
                InstallServiceAddress = fields.ServiceAddress,
                Lines = lines == null
                    ? new List<BasketLine>()
                    : lines.Select(l => new BasketLine
                    {
                        Article = l.Article,
                        Price = l.Price,
                        Quantity = l.Count,
                        Title = l.Item == null ? string.Empty : l.Item.title,
                        BrandTitle = l.Item == null ? string.Empty : l.Item.brand_title,
                        IsVirtual = l.IsVirtual,
                        OldPrice = l.OldPrice,
                        Benefit = l.Benefit,
                        CouponCode = l.CouponCode,
                        CouponCompainId = l.CouponCompainId,
                        VirtualType = l.VirtualType,
                        RefersToItem = l.RefersToItem,
                        LineType = l.LineType,
                      
                    }).ToList(),
                RequestedDeliveryServiceOption = fields.DeliveryServiceOption,
                UtmSouce = trackingMarks == null ? null : JsonConvert.SerializeObject(trackingMarks)
            };
            return basket;
        }
    }
}
