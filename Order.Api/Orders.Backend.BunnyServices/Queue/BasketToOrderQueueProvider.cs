﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orders.Backend.Bunny.Dal;

namespace Orders.Backend.BunnyServices.Queue
{
    public class BasketToOrderQueueProvider
    {
        public ICollection<BasketToOrderQueueElement> GetUnprocessedBaskets()
        {
            using (var context=new BunnyQueueContext())
            {
                return context.BasketToOrderQueue.Where(t => !t.Processed).Take(100).ToList();
            }
        }

        public BasketToOrderQueueElement GetByBasketId(string basketId)
        {
            using (var context = new BunnyQueueContext())
            {
                return context.BasketToOrderQueue.FirstOrDefault(t => t.BasketId == basketId);
            }
        }

        public void Save(BasketToOrderQueueElement basketToOrderElement)
        {
            using (var context = new BunnyQueueContext())
            {
                context.BasketToOrderQueue.AddOrUpdate(basketToOrderElement);
                context.SaveChanges();
            }
        }
    }
}
