﻿using Orders.Backend.Contracts.Bunny;
using Orders.Backend.Contracts.Bunny.Basket3;

namespace Orders.Backend.BunnyServices.Strategy
{
    public interface IOrderStrategy
    {
        UpdateOrderResult UpdateOrder(Contracts.Bunny.Basket3.Basket basket);        
    }
}