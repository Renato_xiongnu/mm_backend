﻿using System;
using Orders.Backend.BunnyServices.IdGenerator;

namespace Orders.Backend.BunnyServices.Strategy
{
    public class OrdersStrategyFactory : IOrdersStrategyFactory
    {
        public IOrderStrategy CreateStrategy(string channelId)
        {
            if(channelId.ToLower().StartsWith("mm"))
            {
                return new MMOrderStrategy();
            }
            var error = string.Format("Incorrect channelId: {0}", channelId);
            throw new ArgumentOutOfRangeException("channelId", error);
        }
    }
}