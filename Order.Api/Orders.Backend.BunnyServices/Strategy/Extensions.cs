﻿using System;
using System.Globalization;
using Orders.Backend.BunnyServices.Proxy.RouterService;
using Orders.Backend.Contracts.Bunny.Basket3;

namespace Orders.Backend.BunnyServices.Strategy
{
    public static class Extensions
    {
        public static Proxy.RouterService.ArticleData BasketLineToArticleData(this BasketLine line)
        {
            var strLineType = line.IsVirtual ? line.VirtualType : line.LineType;
            return new Proxy.RouterService.ArticleData
            {
                ArticleNum = line.Article.ToString(CultureInfo.InvariantCulture),
                Qty = line.Quantity,
                Price = line.Price,
                Title = line.Title,
                BrandTitle = line.BrandTitle,
                IsVirtual = line.IsVirtual,
                OldPrice = line.OldPrice,
                Benefit = line.Benefit,
                CouponCode = line.CouponCode,
                CouponCompainId = line.CouponCompainId ,
                ArticleProductType = StringLineTypeToArticleProductType(strLineType),
                RefersToItem = line.RefersToItem,
            }; 
        }

        /// <summary>
        /// </summary>
        /// <param name="lineType"></param>
        /// <returns></returns>
        public static ArticleProductType StringLineTypeToArticleProductType(String lineType)
        {  
            ArticleProductType prodactType;
            switch (lineType)
            {
                case "delivery":
                    prodactType = ArticleProductType.DeliveryService;
                    break;
                case "service":
                    prodactType = ArticleProductType.InstallationService;
                    break;
                case "service_margin":
                    prodactType = ArticleProductType.InstallServiceMarging;
                    break;
                case "statistical":
                    prodactType = ArticleProductType.Statistical;
                    break;
                default:
                    prodactType = ArticleProductType.Product;
                    break;
            }
            return prodactType;
        }

        public static Proxy.OrderService13.ArticleProductType RouterServiceArticleProductTypeToOrderOnlineArticleProductType(this Proxy.RouterService.ArticleProductType articleProductType)
        {
            switch (articleProductType)
            {
                case Proxy.RouterService.ArticleProductType.DeliveryService:
                    return Proxy.OrderService13.ArticleProductType.DeliveryService;
                case Proxy.RouterService.ArticleProductType.InstallServiceMarging:
                    return Proxy.OrderService13.ArticleProductType.InstallServiceMarging;
                case Proxy.RouterService.ArticleProductType.InstallationService:
                    return Proxy.OrderService13.ArticleProductType.InstallationService;
                case Proxy.RouterService.ArticleProductType.Product:
                    return Proxy.OrderService13.ArticleProductType.Product;
                case Proxy.RouterService.ArticleProductType.Set:
                    return Proxy.OrderService13.ArticleProductType.Set;
                case Proxy.RouterService.ArticleProductType.Statistical:
                    return Proxy.OrderService13.ArticleProductType.Statistical;
                case Proxy.RouterService.ArticleProductType.WarrantyPlus:
                    return Proxy.OrderService13.ArticleProductType.WarrantyPlus;

            }
            return Proxy.OrderService13.ArticleProductType.Product;
        }
    }
}
