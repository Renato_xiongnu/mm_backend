﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Common.Logging;
using MMS.Cloud.Services.Clients;
using MMS.Cloud.Shared.Exceptions;
using Orders.Backend.BunnyServices.Properties;
using Orders.Backend.BunnyServices.Proxy.OrderService13;
using Orders.Backend.BunnyServices.Queue;
using Orders.Backend.Contracts.Bunny;
using Orders.Backend.Contracts.Bunny.Basket3;
using ClientData = Orders.Backend.BunnyServices.Proxy.RouterService.ClientData;
using CreateOrderData = Orders.Backend.BunnyServices.Proxy.RouterService.CreateOrderData;
using DeliveryInfo = Orders.Backend.BunnyServices.Proxy.RouterService.DeliveryInfo;
using OrderServiceClient = Orders.Backend.BunnyServices.Proxy.RouterService.OrderServiceClient;
using OrderSource = Orders.Backend.BunnyServices.Proxy.RouterService.OrderSource;
using PaymentType = Orders.Backend.BunnyServices.Proxy.RouterService.PaymentType;
using ReturnCode = Orders.Backend.BunnyServices.Proxy.RouterService.ReturnCode;
using StoreInfo = Orders.Backend.BunnyServices.Proxy.RouterService.StoreInfo;

namespace Orders.Backend.BunnyServices.Strategy
{
    public class MMOrderStrategy : IOrderStrategy
    {
        private static ILog _logger = LogManager.GetLogger("MMOrderStrategy");

        public CreateOrderResult CreateOrder(string basketId, string orderId)
        {
            var result = new CreateOrderResult();
            var basketWrapper = new BasketMultiplexorWrapper();
            var basket = basketWrapper.GetBasket(basketId);

            _logger.InfoFormat("Basket user: {0}",basket.CustomerUserId);
            
            var client = new OrderServiceClient();
            var proxyClient = new Proxy.OrderService13.OrderServiceClient();
            var orderData = proxyClient.GetOrderDataByBasketId(basket.BasketId);
            if (orderData != null && orderData.OrderInfo != null)
            {
                result.Status = OperationStatus.Ok;
                result.OrderId = orderData.OrderInfo.OrderId;
                return result;
            }

            var createOrderData = new CreateOrderData
            {
                BasketId = basket.BasketId,
                Created = DateTime.UtcNow,
                CreatedBy = basket.CallCenterAgentId,
                OrderSource = GetOrderSource(basket.OrderSource),
                PaymentType = GetPaymentType(basket.PaymentTypeId),
                IsOnlineOrder = true,
                IsTest = basket.IsTestOrder,
                StoreInfo = new StoreInfo
                {
                    SapCode = basket.SaleLocationId.Remove(0, "shop_".Length)
                },
                UtmSource = basket.UtmSouce,
                CouponCode = basket.CouponCode,
                InstallServiceAddress = basket.InstallServiceAddress,
                Prepay =
                    GetPaymentType(basket.PaymentTypeId) == PaymentType.Cash
                        ? 0
                        : basket.Lines.Sum(t => t.Price*t.Quantity) //TODO Move to basket3.get_basket.total_amount
            };
            var clientData = new ClientData
            {
                Name = basket.CustomerName ?? string.Empty,
                Surname = basket.CustomerSurname,
                Phone = basket.CustomerPhone,
                Email = basket.CustomerEmail
            };
            var deliveryInfo = new DeliveryInfo
            {
                HasDelivery = basket.DeliveryType == "delivery",
                PickupLocationId = basket.PickupLocationId,
                Address = basket.DeliveryAddress,
                City = " ", //TODO Fix in Router
                Floor = basket.DeliveryFloor,
                RequestedDeliveryTimeslot = basket.DeliveryInterval,
                DeliveryDate = basket.DeliveryDate,
                RequestedDeliveryServiceOption = basket.RequestedDeliveryServiceOption
            };
            var articlesData = basket.Lines.Select(x => x.BasketLineToArticleData()).ToArray();
          
            createOrderData.OrderId = orderId;

            if (!string.IsNullOrEmpty(basket.CustomerUserId))
            {
                CreateOrdersFromOnline(orderId, basket.CustomerUserId);
            }

            var result1 = client.CreateOrder(clientData, createOrderData, deliveryInfo, articlesData);
            switch(result1.ReturnCode)
            {
                case ReturnCode.Ok:
                    result.OrderId = result1.OrderId;
                    break;
                case ReturnCode.Error:
                    result.Status = OperationStatus.Error;
                    result.AddError(result1.ErrorMessage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            result.OrderId = result1.OrderId;
            return result;
        }

        private void CreateOrdersFromOnline(string orderId, string userId)
        {
            _logger.InfoFormat("Start attach order to user: {0}", userId);
            var client = new DataSyncServiceClient(new Uri(Settings.Default.DataSyncServiceClient))
            {
                DataSyncServiceUserName = Settings.Default.DataSyncServiceName,
                DataSyncServiceUserPassword = Settings.Default.DataSyncServicePassword,
                DataSyncCustomServiceName = Settings.Default.DataSyncCustomServiceName
            };

            var orderToUser = new MMS.Storage.DataModel.Orders.OrdersFromOnline
            {
                OrderId = orderId,
                UserId = userId
            };

            DataSyncException dataSyncException;
            if (!client.TryPutChanges("", new[] { orderToUser }, out dataSyncException)) //TODO
            {
                throw dataSyncException;
            }
            _logger.InfoFormat("Finish attach order to user: {0}", userId);
        }

        public UpdateOrderResult UpdateOrder(Basket basket)
        {
            var updateOrderResult = new UpdateOrderResult();
            var client = new Proxy.OrderService13.OrderServiceClient();
            var orderData = client.GerOrderDataByOrderId(basket.OrderId);
            if (orderData.OrderInfo.BasketId==basket.BasketId)
            {
                return updateOrderResult;
            }
            var clientData = new Proxy.OrderService13.ClientData
            {
                Name = basket.CustomerName,
                Surname = basket.CustomerSurname,
                Phone = basket.CustomerPhone,
                Email = basket.CustomerEmail
            };
            var deliveryInfo = new Proxy.OrderService13.DeliveryInfo
            {
                HasDelivery = basket.DeliveryType == "delivery",
                PickupLocationId = basket.PickupLocationId,
                Address = basket.DeliveryAddress,
                Floor = basket.DeliveryFloor,
                RequestedDeliveryTimeslot = basket.DeliveryInterval,
                DeliveryDate = basket.DeliveryDate,
                RequestedDeliveryServiceOption = basket.RequestedDeliveryServiceOption,
            };
            var updateOrderData = new UpdateOrderData
            {
                OrderId = basket.OrderId,
                ClientData = clientData,
                DeliveryInfo = deliveryInfo,
                BasketId = basket.BasketId,
                CouponCode = basket.CouponCode,
                InstallServiceAddress = basket.InstallServiceAddress,
            };

            _logger.InfoFormat(string.Format("OrderId: {0}. Updating lines: {1}", basket.OrderId,
                string.Join(",",basket.Lines.Select(
                    el => string.Format("Article:{0},Price:{1},Qty:{2}", el.Article, el.Price, el.Quantity)))));
            var newReserveInfo = new Proxy.OrderService13.ReserveInfo
            {
                ReserveLines = basket.Lines
                    .Select((l, i) =>
                    {
                        var articleLines = orderData.ReserveInfo == null
                            ? new Proxy.OrderService13.ReserveLine[0]
                            : orderData.ReserveInfo.ReserveLines;
                        return ToReserveLine(i, l, articleLines);
                    })
                    .ToArray()
            };

            _logger.InfoFormat(string.Format("OrderId: {0}. Updated lines: {1}", basket.OrderId,
                string.Join(",", newReserveInfo.ReserveLines.Select(
                    el =>
                        string.Format("Article:{0},Price:{1},Qty:{2}", el.ArticleData.ArticleNum, el.ArticleData.Price,
                            el.ArticleData.Qty)))));

            var result1 = client.UpdateOrder(updateOrderData, newReserveInfo);
            switch(result1.ReturnCode)
            {
                case Proxy.OrderService13.ReturnCode.Ok:
                    break;
                case Proxy.OrderService13.ReturnCode.Error:
                    updateOrderResult.Status = OperationStatus.Error;
                    updateOrderResult.AddError(result1.ErrorMessage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return updateOrderResult;
        }

        private static Proxy.OrderService13.ReserveLine ToReserveLine(int i, BasketLine l, IEnumerable<Proxy.OrderService13.ReserveLine> articleLines)
        {
            var articleNum = l.Article.ToString(CultureInfo.InvariantCulture);
            var existLine = articleLines.FirstOrDefault(al => al.ArticleData.ArticleNum == articleNum); 
            var strLineType = l.IsVirtual ? l.VirtualType : l.LineType;
            return new Proxy.OrderService13.ReserveLine
            {
                LineId = i,
                ReviewItemState = existLine == null
                                  || existLine.ArticleData.Qty != l.Quantity
                                  || existLine.ArticleData.Price != l.Price
                    ? Proxy.OrderService13.ReviewItemState.Empty
                    : existLine.ReviewItemState,

                ArticleData = new Proxy.OrderService13.ArticleData
                {
                    ArticleNum = articleNum,
                    Price = l.Price,
                    Qty = l.Quantity,
                    Title = l.Title,
                    BrandTitle = l.BrandTitle,
                    IsVirtual = l.IsVirtual,
                    OldPrice = l.OldPrice,
                    Benefit = l.Benefit,
                    CouponCode = l.CouponCode,
                    CouponCompainId = l.CouponCompainId,
                    ArticleProductType =
                        Extensions.StringLineTypeToArticleProductType(strLineType)
                            .RouterServiceArticleProductTypeToOrderOnlineArticleProductType(),
                    RefersToItem = l.RefersToItem,
                },
                ArticleProductType =
                    Extensions.StringLineTypeToArticleProductType(strLineType)
                        .RouterServiceArticleProductTypeToOrderOnlineArticleProductType()
            };
        }

        private OrderSource GetOrderSource(string orderSource)
        {
            if(string.IsNullOrEmpty(orderSource))
            {
                throw new ArgumentNullException("orderSource", "order_source_required");
            }
            switch(orderSource.ToLower())
            {
                case "callcenter":
                    return OrderSource.CallCenter;
                case "metro":
                    return OrderSource.Metro;
                case "metro_callcenter":
                    return OrderSource.MetroCallCenter;
                case "mobile_website":
                    return OrderSource.MobileWebSite;
                case "mobile_website_quick_order":
                    return OrderSource.MobileWebSiteQuickOrder;
                case "website":
                    return OrderSource.WebSite;
                case "website_quick_order":
                    return OrderSource.WebSiteQuickOrder;
                case "iphone":
                    return OrderSource.IPhone;
                case "iphone_quick_order":
                    return OrderSource.IPhoneQuickOrder;
                case "windows_8":
                    return OrderSource.Windows8;
                case "yandex":
                    return OrderSource.YandexMarket;
                case "kiosk_in_store":
                    return OrderSource.Store;
                default:
                    throw new ArgumentOutOfRangeException("orderSource",
                        string.Format("Unknown source order: {0}", orderSource));
            }
        }

        private static PaymentType GetPaymentType(string paymentTypeId)
        {
            if (string.IsNullOrEmpty(paymentTypeId))
            {
                return PaymentType.Cash;
            }

            switch(paymentTypeId.ToLower())
            {
                case "cash":
                case "credit_cards_at_pickup":
                    return PaymentType.Cash;
                case "raiffeisen_credit_cards":                                    
                case "robokassa":
                case "sberbank_credit_cards":
                case "yandex_prepaid":
                    return PaymentType.Online;                
                default:
                    return PaymentType.Cash;
            }
        }
    }
}