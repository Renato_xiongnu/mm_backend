namespace Orders.Backend.BunnyServices.Strategy
{
    public interface IOrdersStrategyFactory
    {
        IOrderStrategy CreateStrategy(string channelId);
    }
}