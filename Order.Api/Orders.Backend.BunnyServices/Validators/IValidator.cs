﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.BunnyServices.Validators
{
    public interface IValidator<in T>
    {
        bool Validate(T request, ref ICollection<ValidationResult> validationResults);
    }
}