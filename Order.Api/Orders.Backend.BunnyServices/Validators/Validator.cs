﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Orders.Backend.BunnyServices.Validators
{
    public class Validator<T> : IValidator<T>
    {
        public virtual bool Validate(T request, ref ICollection<ValidationResult> validationResults)
        {
            var newValidationResults = new List<ValidationResult>();
            var context = new ValidationContext(request, null, null);
            var validateObject = Validator.TryValidateObject(request, context, newValidationResults, true);
            validationResults = validationResults ?? newValidationResults;
            if (validationResults==null)
            {
                validationResults = newValidationResults;
            }
            else
            {
                foreach (var validationResult in newValidationResults)
                {
                    validationResults.Add(validationResult);
                }
            }
            return validateObject && !newValidationResults.Any();
        }
    }
}