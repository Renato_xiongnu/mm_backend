﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace Orders.Backend.BunnyServices.IdGenerator
{
    public class MMIdGenerator : IIdGenerator
    {
        public string GenerateId(string channelId, string saleLocationId)
        {
            int maxCount;
            int.TryParse(ConfigurationManager.AppSettings["IdGenerationTryCount"], out maxCount);
            if(maxCount == 0)
            {
                maxCount = 3;
            }
            var sapCode = Regex.Replace(saleLocationId, @"[^\d]", string.Empty);
            for(var i = 0; i < maxCount; i++)
            {
                try
                {
                    var uri =
                        new Uri(String.Format(ConfigurationManager.AppSettings["IdGenerationUrl"], sapCode));
                    var username = ConfigurationManager.AppSettings["IdGenerationLogin"];
                    var password = ConfigurationManager.AppSettings["IdGenerationPassword"];
                    var authInfo = username + ":" + password;
                    authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                    var request = (HttpWebRequest) WebRequest.Create(uri);
                    request.Headers["Authorization"] = "Basic " + authInfo;
                    using(var response = (HttpWebResponse) request.GetResponse())
                    {
                        using(var reader = new StreamReader(response.GetResponseStream()))
                        {
                            var js = new JavaScriptSerializer();
                            var result = reader.ReadToEnd();
                            var order = js.Deserialize<dynamic>(result);

                            var orderId = order["orderNum"];
                            if(string.IsNullOrEmpty(orderId))
                            {
                                throw new InvalidDataException();
                            }
                            return order["orderNum"];
                        }
                    }
                }
                    // ReSharper disable EmptyGeneralCatchClause
                catch(Exception)
                    // ReSharper restore EmptyGeneralCatchClause
                {
                }
            }
            var rand = new Random((int) (DateTime.Now.Ticks%int.MaxValue));

            var number = rand.Next(0, 99999999).ToString("D8");
            return string.Format("{0}-{1}-{2}",
                sapCode,
                number.Substring(0, 4), 
                number.Substring(4, 4));
            //TODO Исправить этот неебически феерический костыль
        }
    }
}