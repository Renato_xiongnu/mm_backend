﻿namespace Orders.Backend.BunnyServices.IdGenerator
{
    public interface IIdGenerator
    {
        string GenerateId(string channelId, string saleLocationId);
    }
}
