﻿using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Orders.Backend.BunnyServices.IdGenerator
{
    public class FakeIdGenerator : IIdGenerator
    {
        public string GenerateId(string channelId, string saleLocationId)
        {
            var rand = new Random();
            Thread.Sleep(TimeSpan.FromMilliseconds(15));
            return string.Format("{0}-{1:D8}", Regex.Replace(saleLocationId, @"[^\d]", string.Empty),
                rand.Next(0, 99999));
        }
    }
}