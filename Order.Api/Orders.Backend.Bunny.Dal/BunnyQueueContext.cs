﻿using System.Data.Entity;

namespace Orders.Backend.Bunny.Dal
{
    public class BunnyQueueContext:DbContext
    {
        public DbSet<BasketToOrderQueueElement> BasketToOrderQueue { get; set; }
    }
}
