namespace Orders.Backend.Bunny.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BasketToOrderQueueElements",
                c => new
                    {
                        BasketId = c.String(nullable: false, maxLength: 200),
                        OrderId = c.String(nullable: false, maxLength: 20),
                        Processed = c.Boolean(nullable: false),
                        TryCount = c.Int(nullable: false),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.BasketId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BasketToOrderQueueElements");
        }
    }
}
