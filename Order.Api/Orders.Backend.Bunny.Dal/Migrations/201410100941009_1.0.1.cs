namespace Orders.Backend.Bunny.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _101 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BasketToOrderQueueElements", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.BasketToOrderQueueElements", "UpdatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BasketToOrderQueueElements", "UpdatedDate");
            DropColumn("dbo.BasketToOrderQueueElements", "CreatedDate");
        }
    }
}
