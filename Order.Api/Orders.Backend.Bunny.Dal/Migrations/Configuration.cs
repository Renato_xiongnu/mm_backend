namespace Orders.Backend.Bunny.Dal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Orders.Backend.Bunny.Dal.BunnyQueueContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Orders.Backend.Bunny.Dal.BunnyQueueContext context)
        {
        }
    }
}
