﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Bunny.Dal
{
    public class BasketToOrderQueueElement
    {
        [Required]
        [MaxLength(200)]
        [Key]
        public string BasketId { get; private set; }

        [Required]
        [MaxLength(20)]
        public string OrderId { get; private set; }

        public bool Processed { get; private set; }

        public int TryCount { get; private set; }

        public string Message { get; private set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        private BasketToOrderQueueElement()
        {
        }

        public static BasketToOrderQueueElement CreateElement(string basketId,string orderId)
        {
            var currentDate = DateTime.UtcNow;
            return new BasketToOrderQueueElement
            {
                CreatedDate = currentDate,
                UpdatedDate = currentDate,
                BasketId = basketId,
                OrderId = orderId
            };
        }

        public void Complete()
        {
            UpdatedDate = DateTime.UtcNow;
            Processed = true;
        }

        public void CompleteWithMaxTryCount()
        {
            UpdatedDate = DateTime.UtcNow;
            Processed = true;
            Message = "Max try count";
        }

        public void AddError(string message)
        {
            UpdatedDate = DateTime.UtcNow;
            TryCount++;
            Message = message;
        }
    }
}
