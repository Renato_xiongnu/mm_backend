﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.Router.BackendOrdersNew;

namespace Orders.Backend.Services.Router
{
    [TestClass]
    public class SoapSelectorTest
    {
        [TestMethod]
        public void Select_ForOnlineCash_Test()
        {
            var selector = new SoapSelector();

            string result = selector.GetProcessService(OrderSource.MobileWebSite, PaymentType.Cash);

            Assert.IsNotNull(result);
            Assert.AreEqual("IOrderProcessingService", result);
        }

        [TestMethod]
        public void Select_ForOnlineCredit_Test()
        {
            var selector = new SoapSelector();

            string result = selector.GetProcessService(OrderSource.MobileWebSite, PaymentType.OnlineCredit);

            Assert.IsNotNull(result);
            Assert.AreEqual("IOrderProcessingService", result);
        }

        [TestMethod]
        public void Select_ForMetro_Test()
        {
            var selector = new SoapSelector();

            string result = selector.GetProcessService(OrderSource.Metro, PaymentType.Cash);

            Assert.IsNotNull(result);
            Assert.AreEqual("IMetroProcessingService", result);
        }

        [TestMethod]
        public void Select_ForOnlineOnlinePayment_Test()
        {
            var selector = new SoapSelector();

            string result = selector.GetProcessService(OrderSource.MobileWebSite, PaymentType.Online);

            Assert.IsNotNull(result);
            Assert.AreEqual("IOnlinePaymentProcessingService", result);
        }
    }
}
