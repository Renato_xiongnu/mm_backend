﻿using System;

namespace WwsTunnel.TimeStampStorage.Db
{
    public interface ITimeStampStorageRepository
    {
        void CreateOrUpdateTimeStamp(String storagName, Int64 timeStampValue);
        Int64 GetLastTimeStamp(String storagName);
    }
}
