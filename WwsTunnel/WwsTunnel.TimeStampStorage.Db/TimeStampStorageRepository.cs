﻿using System;
using System.Linq;

namespace WwsTunnel.TimeStampStorage.Db
{
    public class TimeStampStorageRepository : ITimeStampStorageRepository
    {
        private readonly String _connectionString;

        public TimeStampStorageRepository(String connectionString)
        {
            _connectionString = connectionString;
        }

        public void CreateOrUpdateTimeStamp(String storagName, Int64 timeStampValue)
        {
            using (var db = new TimeStampStorageDataModelContainer(_connectionString))
            {
                var isExist = db.TimeStampStorages.FirstOrDefault(x => x.StoragName == storagName);
                if (isExist != null)
                    isExist.LastSeq = timeStampValue;
                else
                {
                    db.TimeStampStorages.Add(new TimeStampStorage
                    {
                        StoragName = storagName,
                        LastSeq = timeStampValue
                    });
                }
                db.SaveChanges();
            }
            
        }

        public Int64 GetLastTimeStamp(String storagName)
        {
            using (var db = new TimeStampStorageDataModelContainer(_connectionString))
            {
                var isExist = db.TimeStampStorages.FirstOrDefault(x => x.StoragName == storagName);
                return isExist != null ? isExist.LastSeq : 0;
            }
        }
    }
}
