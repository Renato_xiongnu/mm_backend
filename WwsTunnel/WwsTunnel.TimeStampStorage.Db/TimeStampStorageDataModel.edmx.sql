
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/22/2015 17:44:33
-- Generated from EDMX file: C:\GIT\backend\WwsTunnel\WwsTunnel.TimeStampStorage.Db\TimeStampStorageDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [WwsTunnelTimeStampStorage];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[TimeStampStorages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TimeStampStorages];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'TimeStampStorages'
CREATE TABLE [dbo].[TimeStampStorages] (
    [StoragName] nvarchar(max)  NOT NULL,
    [LastSeq] bigint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [StoragName] in table 'TimeStampStorages'
ALTER TABLE [dbo].[TimeStampStorages]
ADD CONSTRAINT [PK_TimeStampStorages]
    PRIMARY KEY CLUSTERED ([StoragName] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------