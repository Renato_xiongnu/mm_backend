using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace Orders.Backend.Services.WWS
{
    [DataContract]
    public class GetSalesOrderRequest
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}