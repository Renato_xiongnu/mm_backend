﻿using System;
using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace Orders.Backend.Services.WWS
{
    public class ChangeDeliveryDateRequest
    {
        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public DateTime DeliveryDate { get; set; }
    }
}
