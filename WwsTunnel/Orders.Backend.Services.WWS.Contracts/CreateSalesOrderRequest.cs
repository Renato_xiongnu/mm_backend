﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace Orders.Backend.Services.WWS
{
    [DataContract]
    public class CreateSalesOrderRequest
    {

        ///// <summary>
        ///// Номер заказа в BO
        ///// </summary>
        //[DataMember]
        //public string OrderNumber { get; set; }

        ///// <summary>
        ///// Дата создания заказа (из BO)
        ///// </summary>
        //[DataMember]
        //public DateTime? OrderDate { get; set; }

        ///// <summary>
        ///// Тип документа
        ///// </summary>
        //[DataMember]
        //public SalesDocumentType SalesDocumentType { get; set; }

        ///// <summary>
        ///// Платежное поручение
        ///// </summary>
        //[DataMember]
        //public string PaymentOrder { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        [PrimaryKey]
        public string OnlineOrderId { get; set; }

        [DataMember]
        public bool IsOnlineOrder { get; set; }

        [DataMember]
        public string SourceSystem { get; set; }

        /// <summary>
        /// Способ доставки
        /// </summary>
        [DataMember]
        public ShippingMethod ShippingMethod { get; set; }

        /// <summary>
        /// Тип оплаты
        /// </summary>
        //[DataMember]
        //public PaymentType PaymentType { get; set; }

        [DataMember]
        public CustomerInfo Customer { get; set; }

        /// <summary>
        /// Комментарий к заказу (по доставке)
        /// </summary>
        [DataMember]
        public string Comment { get; set; }

        /// <summary>
        /// признак - надо создавать с/ф - документ на '6'
        /// </summary>
        [DataMember]
        public bool IsFiscalInvoice { get; set; }

        [DataMember]
        public List<OrderLine> OrderLines { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        [DataMember]
        public PaymentType PaymentType { get; set; }

        ///// <summary>
        ///// Атавизмы WWS
        ///// </summary>
        //[DataMember]
        //public string FinancingBankOID { get; set; }


        /// <summary>
        /// Магазин Инфо
        /// </summary>
        [DataMember]
        public string OutletInfo { get; set; }


        /// <summary>
        /// ВТИнфо
        /// </summary>
        [DataMember]
        public string ProductPickupInfo { get; set; }

        /// <summary>
        /// Принтер Инфо
        /// </summary>
        [DataMember]
        public string PrintableInfo { get; set; }

        /// <summary>
        /// Табельный номер продавца
        /// </summary>
        [DataMember]
        public int? SalesPersonId { get; set; }

        /// <summary>
        /// Предоплата
        /// </summary>
        [DataMember]
        public decimal? Downpayment { get; set; }

        /// <summary>
        /// Номер кредитного договора
        /// </summary>        
        [DataMember]
        public string CreditNo { get; set; }

        [DataMember]
        public DateTime? DeliveryDate { get; set; }
    }
    
}