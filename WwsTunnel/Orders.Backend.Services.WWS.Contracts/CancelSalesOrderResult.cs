﻿using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace Orders.Backend.Services.WWS
{
    [DataContract]
    public class CancelSalesOrderResult : OperationResult
    {
        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}