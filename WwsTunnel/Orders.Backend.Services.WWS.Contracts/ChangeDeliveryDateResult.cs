﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace Orders.Backend.Services.WWS
{
    [DataContract]
    public class ChangeDeliveryDateResult : OperationResult
    {
        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}
