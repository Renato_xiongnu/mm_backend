﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orders.Backend.Services.WWS
{
    public enum ShippingMethod
    {
        Pickup,
        Delivery
    }

    public enum ReturnCode
    {
        Ok,
        OkPartially,
        JBossException,
        ValidationException,
        Exception
    }

    public enum SalesOrderStatus
    {
        Unknown,
        ToPay,
        Paid,
        Canceled,
        Transformed,
        Prepaid,
        PaidBack
    }

    public enum PaymentType
    {
        Cash,
        CreditCard,
        SocialCard,
        Yandex,
        Online
    }

    public enum ProductType
    {
        Article,
        Set,
        WarrantyPlus
    }
}
