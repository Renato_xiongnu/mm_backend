﻿using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace Orders.Backend.Services.WWS
{
    [DataContract]
    public class GetSalesOrderResult : OperationResult
    {
        [DataMember]
        public OrderHeader OrderHeader { get; set; }

        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}