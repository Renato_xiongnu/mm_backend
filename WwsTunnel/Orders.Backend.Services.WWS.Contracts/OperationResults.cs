﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Configuration;
using MMS.Integration.Gms.WebServices.Interfaces;


namespace Orders.Backend.Services.WWS
{
    [DataContract]
    public class OperationCode
    {
        [DataMember]
        public ReturnCode Code { get; set; }

        [DataMember]
        public string SubCode { get; set; }

        [DataMember]
        public string MessageText { get; set; }
    }

    [DataContract]
    public class OperationResult
    {
        [DataMember]
        public OperationCode OperationCode { get; set; }

        public OperationResult()
        {
            OperationCode = new OperationCode()
            {
                Code = ReturnCode.Ok,
                SubCode = string.Empty
            };
        }
    }

    [DataContract]
    public class Delivery
    {
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Period { get; set; }

        [DataMember]
        public string Comment { get; set; }
    }

    [DataContract]
    public class ReservePrice
    {
        [DataMember]
        public decimal VAT { get; set; }

        [DataMember]
        public decimal NetPrice { get; set; }

        [DataMember]
        public decimal VatPrice { get; set; }

        [DataMember]
        public decimal GrossPrice { get; set; }
    }

    [DataContract]
    public class Customer
    {
        [DataMember]
        public string ClassificationNumber  { get; set; }
        
        [DataMember]
        public string Salutation { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Birthday { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Phone2 { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string CountryAbbreviation { get; set; }

        [DataMember]
        public string AddressIndex { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string INN { get; set; }

        [DataMember]
        public string KPP { get; set; }

    }

    [DataContract]
    public class OrderHeader
    {
        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public DateTime? CreateTime { get; set; }

        [DataMember]
        public ShippingMethod ShippingMethod { get; set; }

        [DataMember]
        public SalesOrderStatus OrderStatus { get; set; }

        [DataMember]
        public PaymentStatus WwsPaymentStatus { get; set; }

        [DataMember]
        public SalesDocumentStatus WwsSalesDocStatus { get; set; }

        [DataMember]
        public SalesDocumentType WwsSalesDocType { get; set; }

        [DataMember]
        public decimal OriginalSum { get; set; }

        [DataMember]
        public DateTime? LastUpdateTime { get; set; }

        [DataMember]
        public string SalesPersonName { get; set; }

        [DataMember]
        public ReservePrice TotalPrice { get; set; }

        [DataMember]
        public decimal DownpaymentPrice { get; set; }

        [DataMember]
        public ReservePrice DownpaymentPriceFull { get; set; }

        [DataMember]
        public ReservePrice LeftoverPrice { get; set; }

        [DataMember]
        public Delivery Delivery { get; set; }

        [DataMember]
        public Customer DeliveryCustomer { get; set; }

        [DataMember]
        public Customer InvoiceCustomer { get; set; }

        [DataMember]
        public ICollection<OrderLine> Lines { get; set; }

        [DataMember]
        public string DocBarcode { get; set; }

        [DataMember]
        public string DocBarcodeImage { get; set; }

        [DataMember]
        public string RefOrderNumber { get; set; }

        [DataMember]
        public string OutletInfo { get; set; }

        [DataMember]
        public string ProductPickupInfo { get; set; }

        [DataMember]
        public string PrintableInfo { get; set; }
    }

    [DataContract]
    public class OrderLine
    {
        [DataMember]
        public int PositionNumber { get; set; }

        [DataMember]
        public int ArticleNo { get; set; }

        [DataMember]
        public string ProductGroupName { get; set; }

        [DataMember]
        public int ProductGroupNo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int FreeQuantity { get; set; }

        [DataMember]
        public int ReservedQuantity { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public decimal PriceOrig { get; set; }

        [DataMember]
        public int StockNumber { get; set; }

        [DataMember]
        public int DepartmentNumber { get; set; }

        [DataMember]
        public ReservePrice TotalPrice { get; set; }

        [DataMember]
        public int StoreNumber { get; set; }

        [DataMember]
        public ProductType ProductType { get; set; }

        [DataMember]
        public int LineNumber { get; set; }

        [DataMember]
        public int? RefLineNumber { get; set; }

        [DataMember]
        public WarrantyInsurance WarrantyInsurance { get; set; }

        [DataMember]
        public bool HasShippedFromStock { get; set; } 

        [DataMember]
        public ICollection<OrderLine> SubLines { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public string ManufacturerName { get; set; }
    }

    [DataContract] 
    public class WarrantyInsurance
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public int DocumentPositionNumber { get; set; }

        [DataMember]
        public int RelatedArticleNo { get; set; }

        [DataMember]
        public decimal WarrantySum { get; set; }

        [DataMember]
        public string WwsCertificateState { get; set; }

        [DataMember]
        public string WwsExtensionPrint  { get; set; }
    }

}
