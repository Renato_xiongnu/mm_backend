﻿using System.Collections.Concurrent;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.Jobs
{
    public class CreateSalesDocPoolingJob : PoolingJobBase<CreateSalesOrderRequest>
    {
        protected override string FileName
        {
            get { return "CreateOrderLastSeq.txt"; }
        }
        protected override string StorageName
        {
            get { return "CreateOrderLastSeq"; }
        }

        protected override int MaxQueueLength
        {
            get { return Settings.Default.CreateRequestExecutorMaxQueueLength; }
        }
        
        protected override ConcurrentQueue<CreateSalesOrderRequest> RequestQueue
        {
            get { return Context.CreateRequestQueue; }
        }

        protected override void PutItemToQueue(CreateSalesOrderRequest item)
        {
            Logger.DebugFormat("Start put item to Queue OnlineOrderId={0}", item.OnlineOrderId);
            Context.CreateRequestQueue.Enqueue(item);
            Logger.DebugFormat("Finish put item to Queue OnlineOrderId={0}", item.OnlineOrderId);
        }
    }
}