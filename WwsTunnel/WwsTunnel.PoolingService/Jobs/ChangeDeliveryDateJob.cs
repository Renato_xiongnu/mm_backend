﻿using System.Collections.Concurrent;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.Jobs
{
    public class ChangeDeliveryDateJob : PoolingJobBase<ChangeDeliveryDateRequest>
    {
        protected override string FileName
        {
            get { return "ChangeDeliveryDateSeq.txt"; }
        }

        protected override string StorageName
        {
            get { return "ChangeDeliveryDateSeq"; }
        }

        protected override int MaxQueueLength
        {
            get { return Settings.Default.ChangeDeliveryDateRequestMaxQueueLength; }
        }

        protected override ConcurrentQueue<ChangeDeliveryDateRequest> RequestQueue
        {
            get { return Context.ChangeDeliveryDateQueue; }
        }

        protected override void PutItemToQueue(ChangeDeliveryDateRequest item)
        {
            Logger.DebugFormat("Start put item to Queue RequestId={0}", item.RequestId);
            Context.ChangeDeliveryDateQueue.Enqueue(item);
            Logger.DebugFormat("Finish put item to Queue RequestId={0}", item.RequestId);
        }
    }
}
