﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using Quartz;
using log4net;
using MMS.Storage.CouchDB.DreamSeat;
using System.Diagnostics;
using WwsTunnel.TimeStampStorage.Db;

namespace WwsTunnel.PoolingService.Jobs
{
    [DisallowConcurrentExecution]
    [PersistJobDataAfterExecution]
    public abstract class PoolingJobBase<T> : IJob where T : class, new()
    {
        protected readonly ILog Logger;
        protected readonly TimeStampStorageRepository _repository;

        protected PoolingJobBase()
        {
            Logger = LogManager.GetLogger(GetType().Name);
            _repository = new TimeStampStorageRepository(ConfigurationManager.ConnectionStrings["TimeStampStorageDataModelContainer"].ConnectionString);
        }

        protected abstract string StorageName { get; }

        protected abstract string FileName { get; }

        protected abstract int MaxQueueLength { get; }

        private int RequestQueueLength
        {
            get { return RequestQueue.Count; }
        }

        protected abstract ConcurrentQueue<T> RequestQueue { get; }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var getChangesLimit = MaxQueueLength - RequestQueueLength;
                if(getChangesLimit <= 0)
                {
                    return;
                }

                #region fileTimeStamp

                //var path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), FileName);
                //var fileInfo = new FileInfo(path);
                //if (!fileInfo.Exists)
                //{
                //    using (fileInfo.CreateText())
                //    {

                //    }
                //}
                //var file = File.ReadAllLines(path);
                //var lastSeq = file.LastOrDefault();



                //var sw = new Stopwatch();
                //sw.Start();

                //long lastCreateOrder = 0;
                //if (!string.IsNullOrEmpty(lastSeq) && long.TryParse(lastSeq, out lastCreateOrder))
                //{                    
                //    if (lastCreateOrder<0)
                //    {
                //        lastCreateOrder = 0;
                //    }
                //}

                //long newLastSeq;

                //Logger.InfoFormat("Start get changes LastSeq={0}", lastSeq);
                //var storage = new CouchDbStorage();
                //var changes = storage.GetChanges<T>(lastCreateOrder, getChangesLimit, out newLastSeq);
                //Logger.InfoFormat("Finish get changes  LastSeq={0} NumberOfChanges={1}", newLastSeq, changes.Count);

                //PutItemsToQueue(changes);

                //Logger.InfoFormat("Current queue length {0}", RequestQueueLength);
                //sw.Stop();
                //Logger.DebugFormat("Time to get {0} items of type {1} from couch: {2}", changes.Count, typeof(T).Name, sw.ElapsedMilliseconds / 1000);
                //File.WriteAllText(path, newLastSeq.ToString(CultureInfo.InvariantCulture));

                #endregion fileTimeStamp

                var lastSeq = _repository.GetLastTimeStamp(StorageName);
                var sw = new Stopwatch();
                sw.Start();

                long newLastSeq;

                Logger.InfoFormat("Start get changes LastSeq={0}", lastSeq);
                var storage = new CouchDbStorage();
                var changes = storage.GetChanges<T>(lastSeq, getChangesLimit, out newLastSeq);
                Logger.InfoFormat("Finish get changes  LastSeq={0} NumberOfChanges={1}", newLastSeq, changes.Count);
                PutItemsToQueue(changes);
                Logger.InfoFormat("Current queue length {0}", RequestQueueLength);
                sw.Stop();
                Logger.DebugFormat("Time to get {0} items of type {1} from couch: {2}", changes.Count, typeof(T).Name, sw.ElapsedMilliseconds / 1000);
                _repository.CreateOrUpdateTimeStamp(StorageName, newLastSeq);
            }
            catch(Exception e)
            {
                Logger.Error(e);
            }
        }

        private void PutItemsToQueue(ICollection<T> items)
        {
            if(items == null)
            {
                return;
            }
            Logger.DebugFormat("Start put {0} items to Queue", items.Count);
            foreach(var item in items)
            {
                PutItemToQueue(item);
            }
            Logger.DebugFormat("Finish put {0} items to Queue", items.Count);
        }

        protected abstract void PutItemToQueue(T item);
    }
}
