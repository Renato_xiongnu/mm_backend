﻿using System.Collections.Concurrent;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.Jobs
{
    public class GetSalesDocJob : PoolingJobBase<GetSalesOrderRequest>
    {
        protected override string FileName
        {
            get { return "GetOrderLastSeq.txt"; }
        }

        protected override string StorageName
        {
            get { return "GetOrderLastSeq"; }
        }

        protected override int MaxQueueLength
        {
            get { return Settings.Default.GetRequestExecutorMaxQueueLength; }
        }
        
        protected override ConcurrentQueue<GetSalesOrderRequest> RequestQueue
        {
            get { return Context.GetRequestQueue; }
        }

        protected override void PutItemToQueue(GetSalesOrderRequest item)
        {
            Logger.DebugFormat("Start put item to Queue RequestId={0}", item.RequestId);
            Context.GetRequestQueue.Enqueue(item);
            Logger.DebugFormat("Finish put item to Queue RequestId={0}", item.RequestId);
        }
    }
}