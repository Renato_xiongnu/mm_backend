﻿using System.Collections.Concurrent;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.Jobs
{
    public class CancelSalesDocJob:PoolingJobBase<CancelSalesOrderRequest>
    {
        protected override string FileName
        {
            get { return "CancelOrderLastSeq.txt"; }
        }

        protected override string StorageName
        {
            get { return "CancelOrderLastSeq"; }
        }

        protected override int MaxQueueLength
        {
            get { return Settings.Default.CancelRequestExecutorMaxQueueLength; }
        }

        protected override ConcurrentQueue<CancelSalesOrderRequest> RequestQueue
        {
            get { return Context.CancelRequestQueue; }
        }

        protected override void PutItemToQueue(CancelSalesOrderRequest item)
        {
            Logger.DebugFormat("Start put item to Queue RequestId={0}", item.RequestId);
            Context.CancelRequestQueue.Enqueue(item);
            Logger.DebugFormat("Finish put item to Queue RequestId={0}", item.RequestId);
        }
    }
}
