﻿using System;
using log4net;
using Quartz;
using Quartz.Impl;

namespace WwsTunnel.PoolingService
{
    public class JobSchedulerStartup
    {
        static readonly ILog Logger = LogManager.GetLogger("QuartzScheduler");
        IScheduler _scheduler;

        public void StartQuartz()
        {
            try
            {
                ISchedulerFactory factory = new StdSchedulerFactory();

                _scheduler = factory.GetScheduler();
                _scheduler.Start();

                Logger.Info("QuartzScheduler has been started");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void StopQuartz()
        {
            try
            {
                if (_scheduler != null)
                {
                    _scheduler.Shutdown(false);
                    _scheduler = null;
                }
                Logger.Info("QuartzScheduler has been stopped");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}