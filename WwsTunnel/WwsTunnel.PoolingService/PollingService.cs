using System;
using System.ServiceProcess;
using WwsTunnel.PoolingService.ParallelExecutors;

namespace WwsTunnel.PoolingService
{
    public class PollingService : ServiceBase
    {
        private readonly Action _startAction;

        private readonly Action _endAction;        

        private const string Name = "Wws Polling Service";

        public PollingService(Action startAction,Action endAction)
        {
            _startAction = startAction;
            _endAction = endAction;            
            ServiceName = Name;
        }

        protected override void OnStart(string[] args)
        {
            _startAction();
        }

        protected override void OnStop()
        {
            _endAction();
        }
    }
    
}