﻿using System;
using System.Threading;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public class GetRequestExecutor : RequestExecutor<GetSalesOrderRequest>
    {
        private void SafeCancelation()
        {
            GetSalesOrderRequest request;
            while(Context.GetRequestQueue.TryDequeue(out request))
            {
                Logger.InfoFormat("Start save the request {0}", request.RequestId);
                if(SafeSaveEntity(request))
                {
                    Logger.InfoFormat("Finish save the request {0}", request.RequestId);
                }
                else
                {
                    Logger.FatalFormat("Could not save the request {0}", request.RequestId);
                }
            }
        }

        protected override void InternalExecute(CancellationToken token)
        {
            while(!token.IsCancellationRequested)
            {
                GetSalesOrderRequest request;
                if(Context.GetRequestQueue.TryDequeue(out request))
                {
                    try
                    {
                        Logger.InfoFormat("Start geting {0}", request.RequestId);
                        var result = ExecuteWwsService(client => GetSalesOrder(client, request));
                        if(SafeSaveEntity(result, Settings.Default.SaveResultNumberOfAttemts,
                            Settings.Default.SaveResultTimeoutBetweenAttemts))
                        {
                            Logger.InfoFormat("Finish geting {0}", request.RequestId);
                        }
                        else
                        {
                            Logger.FatalFormat("Could not save the result {0}", result.RequestId);
                        }
                    }
                    catch(Exception e)
                    {
                        Logger.Error(e);

                        var result = new GetSalesOrderResult
                                     {
                                         OrderHeader = new OrderHeader
                                                       {
                                                           OrderNumber = request.OrderNumber,
                                                       },
                                         RequestId = request.RequestId,
                                         OperationCode = new OperationCode
                                                         {
                                                             Code = ReturnCode.Exception,
                                                             MessageText = Constants.ERROR_DURING_CREATION
                                                         }
                                     };
                        SafeSaveEntity(result);
                    }
                }
                else
                {
                    Thread.Sleep(500);
                }
            }
            if(token.IsCancellationRequested)
            {
                SafeCancelation();
            }
        }

        private GetSalesOrderResult GetSalesOrder(ISalesService client, GetSalesOrderRequest request)
        {
            var result = client.GetSalesOrder(request.ConvertTo<OrderNumberInfo>())
                .ConvertTo<GetSalesOrderResult>();

            result.RequestId = request.RequestId;
            result.OperationCode.MessageText =
                string.IsNullOrEmpty(result.OperationCode.MessageText)
                    ? ""
                    : result.OperationCode.MessageText;
            return result;
        }
    }
}