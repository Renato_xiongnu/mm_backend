﻿using System.Threading;
using System.Threading.Tasks;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public interface IParallelExecutor
    {
        void Execute(CancellationToken token);
    }
}
