using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public class RequestExecutorsManager<TParallelExecutor> : IRequestExecutorsManager
        where TParallelExecutor : IParallelExecutor, new()
    {
        private readonly int _executorInstanceCount;
        private readonly List<Thread> _threads;
        private CancellationTokenSource _cts;        

        private TParallelExecutor[] _executors;

        public RequestExecutorsManager(int executorInstanceCount)
        {
            _executorInstanceCount = executorInstanceCount;
            _threads = new List<Thread>();            
            
        }

        public void Start()
        {
            _cts = new CancellationTokenSource();
            var token = _cts.Token;
            _executors = Enumerable
                .Range(0, _executorInstanceCount)
                .Select(i => new TParallelExecutor())
                .ToArray();
            foreach(var executor in _executors)
            {
                var parallelExecutor = executor;
                var start = new ThreadStart(() => parallelExecutor.Execute(token));
                var thread = new Thread(start)
                {
                    IsBackground = true
                };
                thread.Start();
                _threads.Add(thread);
            }            
        }

        public void Stop()
        {
            _cts.Cancel();
            var timeSpane = TimeSpan.FromSeconds(60);
            foreach(var thread in _threads)
            {
                thread.Join(timeSpane);                
            }            
        }
    }
}