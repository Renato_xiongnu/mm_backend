﻿using System.Threading.Tasks;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public interface IRequestExecutorsManager
    {
        void Start();

        void Stop();
    }
}