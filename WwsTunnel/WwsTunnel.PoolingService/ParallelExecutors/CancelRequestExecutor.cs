﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public class CancelRequestExecutor : RequestExecutor<CancelSalesOrderRequest>
    {        

        private void SafeCancelation()
        {
            CancelSalesOrderRequest request;
            while(Context.CancelRequestQueue.TryDequeue(out request))
            {
                Logger.InfoFormat("Start save the request {0}", request.RequestId);
                if(SafeSaveEntity(request))
                {
                    Logger.InfoFormat("Finish save the request {0}", request.RequestId);
                }
                else
                {
                    Logger.FatalFormat("could not save the request {0}", request.RequestId);
                }                
            }
        }

        protected override void InternalExecute(CancellationToken token)
        {
            while(!token.IsCancellationRequested)
            {
                CancelSalesOrderRequest request;
                if(Context.CancelRequestQueue.TryDequeue(out request))
                {
                    try
                    {
                        Logger.InfoFormat("Start canceling {0}", request.RequestId);                        
                        var cancelSalesOrderResult = ExecuteWwsService(client => CancelSalesOrder(client, request));                                                
                        
                        if (SafeSaveEntity(cancelSalesOrderResult, Settings.Default.SaveResultNumberOfAttemts, Settings.Default.SaveResultTimeoutBetweenAttemts))
                        {
                            Logger.InfoFormat("Finish canceling {0}", cancelSalesOrderResult.RequestId);
                        }
                        else
                        {
                            Logger.FatalFormat("Could not save the result {0}", cancelSalesOrderResult.RequestId);
                        }                       
                    }
                    catch(Exception e)
                    {
                        Logger.Error(e);

                        var result = new CancelSalesOrderResult
                        {
                            RequestId = request.RequestId,
                            OperationCode = new OperationCode
                            {
                                Code = ReturnCode.Exception,
                                MessageText = Constants.ERROR_DURING_CREATION
                            }
                        };
                        SafeSaveEntity(result);                        
                    }
                }
                else
                {                    
                    Thread.Sleep(500);
                }
            }
            if(token.IsCancellationRequested)
            {
                SafeCancelation();
            }
        }

        private static CancelSalesOrderResult CancelSalesOrder(ISalesService client,
            CancelSalesOrderRequest request)
        {
            var result = client.CancelSalesOrder(request.ConvertTo<OrderNumberInfo>());
            var cancelSalesOrderResult = result.ConvertTo<CancelSalesOrderResult>();
            cancelSalesOrderResult.RequestId = request.RequestId;
            cancelSalesOrderResult.OperationCode.MessageText =
                string.IsNullOrEmpty(cancelSalesOrderResult.OperationCode.MessageText)
                    ? ""
                    : cancelSalesOrderResult.OperationCode.MessageText;
            return cancelSalesOrderResult;
        }
    }
}