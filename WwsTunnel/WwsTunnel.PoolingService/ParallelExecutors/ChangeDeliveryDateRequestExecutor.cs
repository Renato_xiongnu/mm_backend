﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public class ChangeDeliveryDateRequestExecutor : RequestExecutor<ChangeDeliveryDateRequest>
    {        

        private void SafeCancelation()
        {
            ChangeDeliveryDateRequest request;
            while(Context.ChangeDeliveryDateQueue.TryDequeue(out request))
            {
                Logger.InfoFormat("Start save the request {0}", request.RequestId);
                if(SafeSaveEntity(request))
                {
                    Logger.InfoFormat("Finish save the request {0}", request.RequestId);
                }
                else
                {
                    Logger.FatalFormat("could not save the request {0}", request.RequestId);
                }                
            }
        }

        protected override void InternalExecute(CancellationToken token)
        {
            while(!token.IsCancellationRequested)
            {
                ChangeDeliveryDateRequest request;
                if(Context.ChangeDeliveryDateQueue.TryDequeue(out request))
                {
                    try
                    {
                        Logger.InfoFormat("Start Change Delivery Date {0}", request.RequestId);
                        var result = ExecuteWwsService(client => ChangeDeliveryDate(client, request));                                                
                        
                        if (SafeSaveEntity(result, Settings.Default.SaveResultNumberOfAttemts, Settings.Default.SaveResultTimeoutBetweenAttemts))
                        {
                            Logger.InfoFormat("Finish Change Delivery Date {0}", result.RequestId);
                        }
                        else
                        {
                            Logger.FatalFormat("Could not save the result {0}", result.RequestId);
                        }                       
                    }
                    catch(Exception e)
                    {
                        Logger.Error(e);

                        var result = new ChangeDeliveryDateResult
                        {
                            RequestId = request.RequestId,
                            OperationCode = new OperationCode
                            {
                                Code = ReturnCode.Exception,
                                MessageText = Constants.ERROR_DURING_CREATION
                            }
                        };
                        SafeSaveEntity(result);                        
                    }
                }
                else
                {                    
                    Thread.Sleep(500);
                }
            }
            if(token.IsCancellationRequested)
            {
                SafeCancelation();
            }
        }

        private static ChangeDeliveryDateResult ChangeDeliveryDate(ISalesService client, ChangeDeliveryDateRequest request)
        {
            var result = client.ChangeDeliveryDate(request.ConvertTo<OrderNumberInfo>(), request.DeliveryDate);
            result.RequestId = request.RequestId;
            return result;
        }
    }
}