﻿using System;
using System.Threading;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public class CreateRequestExecutor : RequestExecutor<CreateSalesOrderRequest>
    {
        private void SafeCancelation()
        {
            CreateSalesOrderRequest request;
            while(Context.CreateRequestQueue.TryDequeue(out request))
            {
                Logger.InfoFormat("Start save the request {0}", request.OnlineOrderId);
                if (SafeSaveEntity(request))
                {
                    Logger.InfoFormat("Finish save the request {0}", request.OnlineOrderId);
                }
                else
                {
                    Logger.FatalFormat("could not save the request {0}", request.OnlineOrderId);
                } 
            }
        }

        protected override void InternalExecute(CancellationToken token)
        {
            while(!token.IsCancellationRequested)
            {
                CreateSalesOrderRequest request;
                if(Context.CreateRequestQueue.TryDequeue(out request))
                {
                    try
                    {
                        Logger.InfoFormat("Start creating {0}", request.OnlineOrderId);
                        var createSalesOrderResult = ExecuteWwsService(client => CreateSalesOrder(client, request));
                        if (SafeSaveEntity(createSalesOrderResult, Settings.Default.SaveResultNumberOfAttemts, Settings.Default.SaveResultTimeoutBetweenAttemts))
                        {
                            Logger.InfoFormat("Finish creating {0}", createSalesOrderResult.OrderId);
                        }
                        else
                        {
                            Logger.FatalFormat("Could not save the result {0}", createSalesOrderResult.OrderId);
                        }                      
                    }
                    catch(Exception e)
                    {
                        Logger.Error(e);

                        var result = new CreateSalesOrderResult
                        {
                            OrderHeader = new OrderHeader
                            {
                                OrderNumber = string.Empty,
                            },
                            OrderId = request.OnlineOrderId,
                            OperationCode = new OperationCode
                            {
                                Code = ReturnCode.Exception,
                                MessageText = Constants.ERROR_DURING_CREATION
                            }
                        };
                        SafeSaveEntity(result);
                    }
                }
                else
                {
                    Thread.Sleep(500);
                }
            }
            if(token.IsCancellationRequested)
            {
                SafeCancelation();
            }
        }

        private CreateSalesOrderResult CreateSalesOrder(ISalesService client, CreateSalesOrderRequest request)
        {
            var result = client.CreateSalesOrder(request.ConvertTo<OrderInfo>());
            var createSalesOrderResult = result.ConvertTo<CreateSalesOrderResult>();
            createSalesOrderResult.OrderId = request.OnlineOrderId;
            createSalesOrderResult.SapCode = request.SapCode;
            createSalesOrderResult.OperationCode.MessageText =
                string.IsNullOrEmpty(createSalesOrderResult.OperationCode.MessageText)
                    ? ""
                    : createSalesOrderResult.OperationCode.MessageText;
            return createSalesOrderResult;
        }
    }
}