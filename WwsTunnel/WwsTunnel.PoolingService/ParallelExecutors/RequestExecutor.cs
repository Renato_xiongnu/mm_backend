﻿using System;
using System.ServiceModel;
using System.Threading;
using MMS.Storage.CouchDB.DreamSeat;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public abstract class RequestExecutor<TRequest> : ParallelExecutorBase<TRequest>
    {
        protected ISalesService SalesService { get; private set; }
        protected RequestExecutor()
        {
            SalesService = new SalesService();
        }

        protected bool SafeSaveEntity<TEntity>(TEntity entity, int numberOfAttemts = 1, TimeSpan? timeoutBetweenAttemts = null)
            where TEntity : class, new()
        {
            var attempt = 0;
            do
            {
                attempt++;
                try
                {
                    var storage = new CouchDbStorage();
                    storage.SaveEntity(entity);
                    return true;
                }
                catch(Exception ex)
                {
                    Logger.Error(ex);
                    Thread.Sleep(timeoutBetweenAttemts ?? TimeSpan.Zero);
                }
            } while(attempt < numberOfAttemts);
            return false;
        }

        protected TResult ExecuteWwsService<TResult>(Func<ISalesService, TResult> func)
        {
            int numberOfAttemts = Settings.Default.ExecuteWwsServiceNumberOfAttemts;
            var timeoutBetweenAttemts = Settings.Default.ExecuteWwsServiceTimeoutBetweenAttemts;
            Exception lastEx;
            var attempt = 0;
            do
            {
                attempt++;
                try
                {
                    return func(SalesService);
                }
                catch(TimeoutException ex)
                {
                    Logger.Error("WWS TimeoutException", ex);
                    lastEx = ex;
                    Thread.Sleep(timeoutBetweenAttemts);
                }
            } while(attempt < numberOfAttemts);
            throw lastEx;
        }
    }
}