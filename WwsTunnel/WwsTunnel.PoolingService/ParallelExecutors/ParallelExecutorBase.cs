﻿using System;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace WwsTunnel.PoolingService.ParallelExecutors
{
    public abstract class ParallelExecutorBase<T>:IParallelExecutor
    {
        protected readonly ILog Logger;        

        protected ParallelExecutorBase()
        {
            Logger = LogManager.GetLogger(GetType().Name);
        }

        public void Execute(CancellationToken token)
        {
            try
            {
                InternalExecute(token);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        protected abstract void InternalExecute(CancellationToken token);
    }
}
