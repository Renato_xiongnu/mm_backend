﻿using AutoMapper;
using Orders.Backend.Services.WWS;
using SalesService = Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService
{
    public static class MappingExtensions
    {
        static MappingExtensions()
        {
            ConfigureMappings();
        }

        public static void ValidateMapping()
        {
            Mapper.AssertConfigurationIsValid();
        }

        private static void ConfigureMappings()
        {
            Mapper.CreateMap<CreateSalesOrderRequest, SalesService.OrderInfo>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<GetSalesOrderRequest, SalesService.OrderNumberInfo>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<CancelSalesOrderRequest, SalesService.OrderNumberInfo>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<CustomerInfo, SalesService.CustomerInfo>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<OrderLine, SalesService.OrderLine>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<ReservePrice, SalesService.ReservePrice>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<WarrantyInsurance, SalesService.WarrantyInsurance>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<OrderHeader, SalesService.OrderHeader>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<Delivery, SalesService.Delivery>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<Customer, SalesService.Customer>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<OperationCode, SalesService.OperationCode>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<CreateSalesOrderRequest, SalesService.OrderInfo>();
                  //.ForMember(el => el.ExtensionData, opt => opt.Ignore());

            Mapper.CreateMap<ChangeDeliveryDateRequest, SalesService.OrderNumberInfo>();            

            Mapper.CreateMap<SalesService.CreateSalesOrderResult, CreateSalesOrderResult>()                
                  .ForMember(el => el.OrderId, opt => opt.Ignore())
                  .ForMember(el => el.SapCode, opt => opt.Ignore());

            Mapper.CreateMap<SalesService.GetSalesOrderResult, GetSalesOrderResult>()
                .ForMember(el => el.OrderId, opt => opt.Ignore())
                .ForMember(el=>el.RequestId, opt=>opt.Ignore());

            Mapper.CreateMap<SalesService.OrderNumberInfo, OrderNumberInfo>();
            Mapper.CreateMap<SalesService.CancelSalesOrderResult, CancelSalesOrderResult>()
                .ForMember(el => el.RequestId, opt => opt.Ignore()); ;
            Mapper.CreateMap<SalesService.OrderHeader, OrderHeader>();
            Mapper.CreateMap<SalesService.CustomerInfo, CustomerInfo>();
            Mapper.CreateMap<SalesService.OrderLine, OrderLine>();
            Mapper.CreateMap<SalesService.ReservePrice, ReservePrice>();
            Mapper.CreateMap<SalesService.WarrantyInsurance, WarrantyInsurance>();
            Mapper.CreateMap<SalesService.OrderHeader, OrderHeader>();
            Mapper.CreateMap<SalesService.Delivery, Delivery>();
            Mapper.CreateMap<SalesService.Customer, Customer>();
            Mapper.CreateMap<SalesService.OperationCode, OperationCode>();                        
        }

        public static T ConvertTo<T>(this object obj)
        {            
            return Mapper.Map<T>(obj);
        }
    }
}
