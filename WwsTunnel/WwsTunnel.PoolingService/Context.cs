﻿using System.Collections.Concurrent;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService
{
    public static class Context
    {
        static Context()
        {
            CreateRequestQueue = new ConcurrentQueue<CreateSalesOrderRequest>();
            CancelRequestQueue = new ConcurrentQueue<CancelSalesOrderRequest>();
            GetRequestQueue = new ConcurrentQueue<GetSalesOrderRequest>();
            ChangeDeliveryDateQueue = new ConcurrentQueue<ChangeDeliveryDateRequest>();
        }

        public static ConcurrentQueue<CreateSalesOrderRequest> CreateRequestQueue { get; private set; }

        public static ConcurrentQueue<CancelSalesOrderRequest> CancelRequestQueue { get; private set; }

        public static ConcurrentQueue<GetSalesOrderRequest> GetRequestQueue { get; private set; }

        public static ConcurrentQueue<ChangeDeliveryDateRequest> ChangeDeliveryDateQueue { get; private set; }
    }
}