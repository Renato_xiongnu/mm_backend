﻿using System;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using Quartz;
using Quartz.Impl;
using WwsTunnel.PoolingService.ParallelExecutors;

namespace WwsTunnel.PoolingService
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger("QuartzScheduler");

        private static JobSchedulerStartup _scheduler;

        private static IRequestExecutorsManager[] _parallelExecutorManagers;

        private static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            MappingExtensions.ValidateMapping();
            _scheduler = new JobSchedulerStartup();
            _parallelExecutorManagers = new IRequestExecutorsManager[]
            {
                new RequestExecutorsManager<CreateRequestExecutor>(Settings.Default.CreateRequestExecutorCount),
                new RequestExecutorsManager<CancelRequestExecutor>(Settings.Default.CancelRequestExecutorCount),
                new RequestExecutorsManager<GetRequestExecutor>(Settings.Default.GetRequestExecutorCount),
                new RequestExecutorsManager<ChangeDeliveryDateRequestExecutor>(Settings.Default.ChangeDeliveryDateExecutorCount),
            };

            if (!Environment.UserInteractive)
            {
                using (var service = new PollingService(Start, Stop))
                {
                    ServiceBase.Run(service);
                }
            }
            else
            {
                Start();
                Console.WriteLine("Press any key to stop...");
                Console.ReadKey();
                Stop();
            }          
        }        

        private static void Start()
        {
            _scheduler.StartQuartz();
            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Start();
                }
                catch (Exception ex)
                {
                    Logger.Error("Start manager exception: ", ex);
                }
            }); 
        }

        private static void Stop()
        {
            _scheduler.StopQuartz();
            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Stop();
                }
                catch(Exception ex)
                {                    
                    Logger.Error("Stop manager exception: ",ex);
                }
            });            
        }
    }
}