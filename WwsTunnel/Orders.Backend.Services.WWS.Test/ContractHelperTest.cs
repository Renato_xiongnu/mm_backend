﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS.Helpers;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class ContractHelperTest
    {
        [TestMethod]
        public void ToWWsOnlineOrderId_OldFormat()
        {
            var orderId = "002-12-255";

            var orderIdParsed = ContractHelper.ToWWsOnlineOrderId(orderId);

            Assert.AreEqual(212255, orderIdParsed);
        }

        [TestMethod]
        public void ToWWsOnlineOrderId_NewFormat()
        {
            var orderId = "002-12345678";

            var orderIdParsed = ContractHelper.ToWWsOnlineOrderId(orderId);

            Assert.AreEqual(12345678, orderIdParsed);
        }
    }
}
