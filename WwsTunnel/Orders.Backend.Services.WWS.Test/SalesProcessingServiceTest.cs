﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMS.Integration.Gms.WebServices.Client;
using MMS.Integration.Gms.WebServices.Interfaces;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class SalesProcessingServiceTest
    {
        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            var logger = LogManager.GetLogger("ChangeDeliveryDateRequestHandler");
        }

        private OrderNumberInfo OrderNumberInfo()
        {
            return new OrderNumberInfo
            {
                OrderNumber = "40595575",
                SapCode = "R007",
            };
        }

        [TestMethod]
        public void SalesProcessingServiceTest_SearchSalesDocuments()
        {
            var client = new GmsClient("R007");
            var request = new SearchSalesDocumentsRequest
            {
                CreateTimeBegin = DateTime.Today.AddDays(-2),
                MaxResults = 100,
            };
            var response = client.SalesProcessing.SearchSalesDocuments(request);
            Assert.IsNotNull(response);
            Console.WriteLine("Response = {0}", response);
            foreach(var doc in response.SalesDocuments)
            {
                var response2 = client.SalesProcessing.GetSalesDocument(doc.SalesDocNo); // 40595575
                Assert.IsNotNull(response2);
                foreach(var line in response2.SalesDocumentLines)
                {
                    if (line.Product.ArticleNo > 10000000)
                    {
                        Console.WriteLine("ArticleNo = ", line.Product.ArticleNo);
                    }
                }
            }

        }

        [TestMethod]
        public void SalesProcessingServiceTest_GetSalesDocument()
        {
            var client = new GmsClient("R007");
            var response = client.SalesProcessing.GetSalesDocument(40595587); // 40595575
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SalesProcessingServiceTest_ChangeDeliveryDate()
        {
            var client = new GmsClient("R007");
            client.SalesProcessing.ChangeDeliveryDate(40595575, new DateTime(2015, 06, 24));
            //Assert.IsNotNull(response);
        }
    }
}
