﻿using System;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS;
using Orders.Backend.Services.WWS.Mapping;
using Orders.Backend.Services.WWS.Helpers;
using MMS.Integration.Gms.WebServices.Interfaces;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class ResultBuilderTest
    {
        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            var logger = LogManager.GetLogger("CancelSalesOrderRequestHandler");
        }

        [TestMethod]
        public void ResultBuilder_SetValidationExceptionOperationCode()
        {
            var orderInfo = Mocks.Mocks.OrderInfo();
            var resultBuilder = new CreateSalesOrderResultBuilder();
            resultBuilder.SetValidationExceptionOperationCode();
            var result = resultBuilder.BuildResult();
            Assert.AreEqual(ReturnCode.ValidationException, result.OperationCode.Code);
            Assert.AreEqual(SubCodes.ValidationError, result.OperationCode.SubCode);
        }

        [TestMethod]
        public void ResultBuilder_SetSystemExceptionOperationCode()
        {
            var orderInfo = Mocks.Mocks.OrderInfo();
            var resultBuilder = new CreateSalesOrderResultBuilder();
            resultBuilder.SetSystemExceptionOperationCode();
            var result = resultBuilder.BuildResult();
            Assert.AreEqual(ReturnCode.Exception, result.OperationCode.Code);
            Assert.AreEqual(SubCodes.SystemException, result.OperationCode.SubCode);
        }

        [TestMethod]
        public void ResultBuilder_GetOrderHeader()
        {
            var orderInfo = Mocks.Mocks.OrderInfo();
            var salesDocumentInfo = Mocks.Mocks.SalesDocumentInfo();
            var response = Mocks.Mocks.CreateSalesDocumentResponse();
            var header = CreateSalesOrderResultBuilder.GetOrderHeader(salesDocumentInfo, response);
            Assert.AreEqual(salesDocumentInfo.SalesDocumentLines.Count, orderInfo.OrderLines.Count);
        }

        [TestMethod]
        public void ResultBuilder_CreateHierarchy()
        {
            var orderInfo = Mocks.Mocks.OrderInfo();
            var salesDocumentInfo = Mocks.Mocks.SalesDocumentInfo();
            var response = Mocks.Mocks.CreateSalesDocumentResponse();
            var header = CreateSalesOrderResultBuilder.GetOrderHeader(salesDocumentInfo, response);
            CreateSalesOrderResultBuilder.CreateHierarchy(orderInfo, header);
            Assert.AreEqual(orderInfo.OrderLines.Count, orderInfo.OrderLines.Count);
        }


        [TestMethod]
        public void ResultBuilder_SetLogicErrorOperationCode()
        {            
            var orderInfo = Mocks.Mocks.OrderInfo();
            var resultBuilder = new CreateSalesOrderResultBuilder();
            List<string> diffLines = orderInfo.OrderLines.Select(orderLine =>
            {
                return string.Format("Номер артикула: {0}, цена {1}, кол-во {2}", orderLine.ArticleNo, orderLine.Price, orderLine.Quantity);
            }).ToList();
            resultBuilder.SetOrderInfo(orderInfo);
            resultBuilder.SetCreateSalesDocumentResponse(Mocks.Mocks.CreateSalesDocumentResponse());
            resultBuilder.SetSalesDocumentInfo(Mocks.Mocks.SalesDocumentInfo());
            resultBuilder.SetLogicErrorOperationCode(diffLines);
            var result = resultBuilder.BuildResult();
            Assert.AreEqual(ReturnCode.OkPartially, result.OperationCode.Code);
            Assert.AreEqual(SubCodes.LogicError, result.OperationCode.SubCode);
        }

        [TestMethod]
        public void ResultBuilder_BuildResult()
        {
            var orderInfo = Mocks.Mocks.OrderInfo();

            var resultBuilder = new CreateSalesOrderResultBuilder();
            resultBuilder.SetOrderInfo(orderInfo);
            resultBuilder.SetCreateSalesDocumentResponse(Mocks.Mocks.CreateSalesDocumentResponse());
            resultBuilder.SetSalesDocumentInfo(Mocks.Mocks.SalesDocumentInfo());
            var result = resultBuilder.BuildResult();
        }
    }
}
