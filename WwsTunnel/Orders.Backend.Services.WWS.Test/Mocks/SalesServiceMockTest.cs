﻿using System;
using System.Collections.Generic;
using System.Runtime;
using Orders.Backend.Services.WWS.Mocks;
using WWS.Services;
using NUnit.Framework;

namespace Orders.Backend.Services.WWS.Test.Mocks
{
    [TestFixture]
    public class SalesServiceMockTest
    {
        [Test]
        public void CreateSalesOrder_Test()
        {
            var orderInfo = new OrderInfo();
            orderInfo.Comment = "test";
            orderInfo.Customer = new CustomerInfo
            {
                Address = "улица тест, 1",
                City = "Москва",
                Email = "test@test.ru",
                FirstName = "Петр",
                Mobile = "+79111111111",
                Phone = "+792222222222",
                Surname = "Петров",
                ZipCode = "ZipCode1"
            };
            orderInfo.IsFiscalInvoice = false;
            orderInfo.OnlineOrderId = "007-02-03";
            orderInfo.OrderLines = new List<OrderLine>
            {
                new OrderLine
                {
                    ArticleNo = 1111122,
                    DepartmentNumber = 1,
                    Description = "2",
                    FreeQuantity = 10,
                    HasShippedFromStock = true,
                    LineNumber = 10,
                    ManufacturerName = "test",
                    PositionNumber = 0,
                    Price = 10000,
                    PriceOrig = 9000,
                    ProductGroupNo = 2
                },
                new OrderLine
                {
                    ArticleNo = 1111123,
                    DepartmentNumber = 2,
                    Description = "3",
                    FreeQuantity = 11,
                    HasShippedFromStock = false,
                    LineNumber = 11,
                    ManufacturerName = "test",
                    PositionNumber = 0,
                    Price = 10000,
                    PriceOrig = 9000,
                    ProductGroupNo = 2
                },
            };
            orderInfo.OutletInfo = "test";
            orderInfo.PrintableInfo = "test1";
            orderInfo.ProductPickupInfo = "test2";
            orderInfo.ShippingMethod=ShippingMethod.Pickup;
            orderInfo.SourceSystem = "MediaMarkt";
            orderInfo.PaymentType=PaymentType.Cash;

            var createWwsResult = new SalesServiceMock().CreateSalesOrder(orderInfo);

            Assert.IsNotNull(createWwsResult);
            Assert.IsNotNull(createWwsResult.OrderHeader);
            Assert.IsNotNull(createWwsResult.OrderHeader.OrderNumber);
            Assert.AreEqual(ReturnCode.Ok, createWwsResult.OperationCode.Code);
        }

        [Test]
        public void CreateSalesOrder_InvalidArticleNo_Test()
        {
            var orderInfo = new OrderInfo();
            orderInfo.Comment = "test";
            orderInfo.Customer = new CustomerInfo
            {
                Address = "улица тест, 1",
                City = "Москва",
                Email = "test@test.ru",
                FirstName = "Петр",
                Mobile = "+79111111111",
                Phone = "+792222222222",
                Surname = "Петров",
                ZipCode = "ZipCode1"
            };
            orderInfo.IsFiscalInvoice = false;
            orderInfo.OnlineOrderId = "007-02-03";
            orderInfo.SapCode = "R008";
            orderInfo.OrderLines = new List<OrderLine>
            {
                new OrderLine
                {
                    ArticleNo = 1111122,
                    DepartmentNumber = 1,
                    Description = "2",
                    FreeQuantity = 10,
                    HasShippedFromStock = true,
                    LineNumber = 10,
                    ManufacturerName = "test",
                    PositionNumber = 0,
                    Price = 10000,
                    PriceOrig = 9000,
                    ProductGroupNo = 2
                },
                new OrderLine
                {
                    ArticleNo = 2111123,
                    DepartmentNumber = 2,
                    Description = "3",
                    FreeQuantity = 11,
                    HasShippedFromStock = false,
                    LineNumber = 11,
                    ManufacturerName = "test",
                    PositionNumber = 0,
                    Price = 10000,
                    PriceOrig = 9000,
                    ProductGroupNo = 2
                },
            };
            orderInfo.OutletInfo = "test";
            orderInfo.PrintableInfo = "test1";
            orderInfo.ProductPickupInfo = "test2";
            orderInfo.ShippingMethod = ShippingMethod.Pickup;
            orderInfo.SourceSystem = "MediaMarkt";
            orderInfo.PaymentType = PaymentType.Cash;

            var createWwsResult = new SalesServiceMock().CreateSalesOrder(orderInfo);

            Assert.IsNotNull(createWwsResult);
            Assert.IsNotNull(createWwsResult.OrderHeader);
            Assert.IsNotNull(createWwsResult.OrderHeader.OrderNumber);
            Assert.AreEqual(ReturnCode.JBossException, createWwsResult.OperationCode.Code);
        }

        [Test]
        public void CreateSalesOrder_InvalidArgNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() => new SalesServiceMock().CreateSalesOrder(null));
            Assert.Throws<ArgumentNullException>(() => new SalesServiceMock().CreateSalesOrder(new OrderInfo()
            {
                Customer = null
            }));
            Assert.Throws<ArgumentNullException>(() => new SalesServiceMock().CreateSalesOrder(new OrderInfo()
            {
                Customer = new CustomerInfo(),
                OrderLines = null,
            }));
        }

        [Test]
        public void CancelSalesOrder_Test()
        {
            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "007-02-03",
                SapCode = "R007"
            };
            var cancelResult=new SalesServiceMock().CancelSalesOrder(orderNumberInfo);
            
            Assert.IsNotNull(cancelResult);
            Assert.AreEqual(ReturnCode.Ok, cancelResult.OperationCode.Code);
        }

        [Test]
        public void CancelSalesOrder_InvalidArgNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() => new SalesServiceMock().CancelSalesOrder(null));
        }

        [Test]
        public void GetSalesOrder_Test()
        {
            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "007-02-03",
                SapCode = "R007"
            };
            var getSalesOrderResult = new SalesServiceMock().GetSalesOrder(orderNumberInfo);

            Assert.IsNotNull(getSalesOrderResult);
            Assert.AreEqual(ReturnCode.Exception, getSalesOrderResult.OperationCode.Code); //because it must be created first
        }

        [Test]
        public void GetSalesOrder_InvalidArgNull_Test()
        {

            Assert.Throws<ArgumentNullException>(() => new SalesServiceMock().GetSalesOrder(null));
        }

        [Test]
        public void GetSalesOrder_CreateAndGet_Test()
        {
            var orderInfo = new OrderInfo();
            orderInfo.Comment = "test";
            orderInfo.Customer = new CustomerInfo
            {
                Address = "улица тест, 1",
                City = "Москва",
                Email = "test@test.ru",
                FirstName = "Петр",
                Mobile = "+79111111111",
                Phone = "+792222222222",
                Surname = "Петров",
                ZipCode = "ZipCode1"
            };
            orderInfo.IsFiscalInvoice = false;
            orderInfo.OnlineOrderId = "007-02-03";
            orderInfo.SapCode = "R007";
            orderInfo.OrderLines = new List<OrderLine>
            {
                new OrderLine
                {
                    ArticleNo = 1111122,
                    DepartmentNumber = 1,
                    Description = "2",
                    FreeQuantity = 10,
                    HasShippedFromStock = true,
                    LineNumber = 10,
                    ManufacturerName = "test",
                    PositionNumber = 0,
                    Price = 10000,
                    PriceOrig = 9000,
                    ProductGroupNo = 2
                },
                new OrderLine
                {
                    ArticleNo = 1111123,
                    DepartmentNumber = 2,
                    Description = "3",
                    FreeQuantity = 11,
                    HasShippedFromStock = false,
                    LineNumber = 11,
                    ManufacturerName = "test",
                    PositionNumber = 0,
                    Price = 10000,
                    PriceOrig = 9000,
                    ProductGroupNo = 2
                },
            };
            orderInfo.OutletInfo = "test";
            orderInfo.PrintableInfo = "test1";
            orderInfo.ProductPickupInfo = "test2";
            orderInfo.ShippingMethod = ShippingMethod.Pickup;
            orderInfo.SourceSystem = "MediaMarkt";
            orderInfo.PaymentType = PaymentType.Cash;

            var createWwsResult = new SalesServiceMock().CreateSalesOrder(orderInfo);

            Assert.IsNotNull(createWwsResult);
            Assert.IsNotNull(createWwsResult.OrderHeader);
            Assert.IsNotNull(createWwsResult.OrderHeader.OrderNumber);
            Assert.AreEqual(ReturnCode.Ok, createWwsResult.OperationCode.Code);

            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = createWwsResult.OrderHeader.OrderNumber,
                SapCode = "R007"
            };
            var getSalesOrder = new SalesServiceMock().GetSalesOrder(orderNumberInfo);
            Assert.IsNotNull(getSalesOrder);
            Assert.AreEqual(ReturnCode.Ok, getSalesOrder.OperationCode.Code);
            Assert.AreEqual(PaymentStatus.PAID, getSalesOrder.OrderHeader.WwsPaymentStatus);

            Assert.AreEqual(createWwsResult.OrderHeader, getSalesOrder.OrderHeader);

            orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "400010102",
                SapCode = "R007"
            }; 
            getSalesOrder = new SalesServiceMock().GetSalesOrder(orderNumberInfo);
            Assert.IsNotNull(getSalesOrder);
            Assert.AreEqual(ReturnCode.Exception, getSalesOrder.OperationCode.Code);
        }
    }
}
