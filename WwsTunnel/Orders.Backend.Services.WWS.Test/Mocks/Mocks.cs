﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Orders.Backend.Services.WWS;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;

namespace Orders.Backend.Services.WWS.Test.Mocks
{
    public static class Mocks
    {
        public static OrderInfo OrderInfo()
        {
            var orderInfo = new OrderInfo();
            orderInfo.Comment = "Test - Created from TicketTool";
            orderInfo.Customer = OrderCustomerInfo();
            orderInfo.IsFiscalInvoice = false;
            orderInfo.OnlineOrderId = "007-02-03";
            orderInfo.OrderLines = Enumerable.Range(0, 2).Select(i => OrderLine(i, 2)).ToList();
            orderInfo.OutletInfo = "test";
            orderInfo.PrintableInfo = "test1";
            orderInfo.ProductPickupInfo = "test2";
            orderInfo.ShippingMethod = ShippingMethod.Pickup;
            orderInfo.SourceSystem = "MediaMarkt";
            orderInfo.PaymentType = PaymentType.Cash;
            return orderInfo;
        }
        public static OrderInfo OrderInfoWithSet()
        {
            var orderInfo = new OrderInfo();
            orderInfo.Comment = "Test - Created from TicketTool";
            orderInfo.Customer = OrderCustomerInfo();
            orderInfo.IsFiscalInvoice = false;
            orderInfo.OnlineOrderId = "007-02-03";
            orderInfo.OrderLines = Enumerable.Range(0, 2).Select(i => OrderLine(i, 2, ProductType.Set)).ToList();
            orderInfo.OutletInfo = "test";
            orderInfo.PrintableInfo = "test1";
            orderInfo.ProductPickupInfo = "test2";
            orderInfo.ShippingMethod = ShippingMethod.Pickup;
            orderInfo.SourceSystem = "MediaMarkt";
            orderInfo.PaymentType = PaymentType.Cash;
            return orderInfo;
        }
        
        public static Customer Customer()
        {
            return new Customer
            {
                Address = "Address",
                AddressIndex = "AddressIndex",
                City = "City",
                ClassificationNumber = "9090",
                CountryAbbreviation = "dd",
                Email = "Email",
                INN = "111",
                KPP = "2222",
                Name = "Name",
                Phone = "1111111",
                Phone2 = "2222222",
                Salutation = "efewfew",
                Surname = "Surname"
            };
        }

        public static Interfaces.CustomerInfo CustomerInfo()
        {
            return new Interfaces.CustomerInfo
            {
                Address = new Interfaces.AddressInfo
                {
                    Address = "Address",
                    Country = "Ru",
                    Location = "Location",
                    ZipCode = "111222"
                },
                Email = "test@test.ru",
                FirstName = "Петр",
                Mobile = "+79111111111",
                Phone = "+792222222222",
                Surname = "Петров"
            };
        }

        public static Interfaces.CustomerInfo CustomerInfo2()
        {
            return new Interfaces.CustomerInfo
            {
                CustomerNo = 900249097,
                Salutation = "НЕТ",
                Surname = "ЮРОК",
                Email = "yelenskiy@media-saturn.com",
                Phone = "+7 (925) 870 34 31",
                Mobile = "+7 (925) 870 34 31",
                Address = new Interfaces.AddressInfo
                {
                    Country = "RU",
                    Location = "МОСКВА",
                    ZipCode = "141411",
                    Address = "ул.,НОВОСЕЛКИ 1-Я,д. 3,  "
                },
            };
        }

        public static CustomerInfo OrderCustomerInfo()
        {
            return new CustomerInfo
            {
                Address = "улица тест, 1",
                City = "Москва",
                Email = "test@test.ru",
                FirstName = "Петр",
                Mobile = "+79111111111",
                Phone = "+792222222222",
                Surname = "Петров",
                ZipCode = "ZipCode1"
            };
        }

        public static Interfaces.SalesDocumentLineInfo SalesDocumentLineInfo(int no = 1, int quantity = 1)
        {
            var lineInfo = new Interfaces.SalesDocumentLineInfo
            {
                Product = new Interfaces.ProductInfo
                {
                    ArticleNo = 11110000 + no,
                    CountryOfOrigin = "CountryOfOrigin",
                    DepartmentNo = 1,
                    ManufacturerName = "ManufacturerName",
                    ProductGroup = "ProductGroup",
                    ProductGroupNo = 1,
                    RetailPrice = 99.9m,
                    Title = "Title"
                },
                OriginalPrice = 100m,
                RetailPrice = 99.9m,
                FreeQuantity = 100,
                SerialNo = "SerialNo",
                Quantity = quantity,
            };
            return lineInfo;
        }

        public static OrderLine OrderLine(int no = 1, int quantity = 1, ProductType productType = ProductType.Article)
        {
            return new OrderLine
            {
                ProductType = productType,
                ArticleNo = 11110000 + no,
                DepartmentNumber = 1,
                Description = String.Format("Description {0}", no),
                FreeQuantity = 10,
                HasShippedFromStock = true,
                LineNumber = 1 + no,
                ManufacturerName = "test",
                PositionNumber = 0,
                Price = 99.9m,
                PriceOrig = 100m,
                ProductGroupNo = 2,
                Quantity = quantity
            };
        }

        public static List<SetItem> SetItemList(long no)
        {
            return Enumerable.Range(1, 3).Select(i =>
            {
                return new SetItem
                {
                    ArticleNo = 22220000 + no,
                    Price = 1000 * no,
                    Quantity = 1
                };
            }).ToList();
        }

        public static Interfaces.CreateSalesDocumentResponse CreateSalesDocumentResponse()
        {
            return new Interfaces.CreateSalesDocumentResponse
            {
                SalesDocNo = 41230752,
                SalesDocBarCode = "1234"
            };
        }

        public static Interfaces.SalesDocumentInfo SalesDocumentInfo()
        {
            return new Interfaces.SalesDocumentInfo
            {
                DeliveryCustomer = new Interfaces.DeliveryInfo
                {
                    Customer = CustomerInfo2(),
                    DateFrom = new DateTime(2000, 01, 01),
                    DateTo = new DateTime(2000, 01, 01),
                },
                DownPaymentPrice = 21544,
                FinancingBank = new Interfaces.BankInfo(),
                InvoiceCustomer = CustomerInfo2(),
                PaymentStatus = Interfaces.PaymentStatus.PAID,
                PickupType = Interfaces.PickupType.PICKUP,
                PrintInfo = "",
                ProductPickupInfo = "",
                PurchaseDate = new DateTime(2000, 01, 01),
                SalesDocDate = new DateTime(2000, 01, 01),
                SalesDocNo = 41230752,
                SalesDocStatus = Interfaces.SalesDocumentStatus.PRINTED,
                SalesDocType = Interfaces.SalesDocumentType.CASH_SALE,
                SalesDocumentLines = Enumerable.Range(0, 2).Select(i => SalesDocumentLineInfo(i, 1)).ToList(),
                SalesPersonName = "ИНТЕРНЕТ",
            };
        }
    }
}
