﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS.Mapping;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class MappingExtenisonsV2Test
    {
        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            var logger = LogManager.GetLogger("CancelSalesOrderRequestHandler");
        }

        [TestMethod]
        public void MapToWwsOnlineOrderId_OldFormat()
        {
            var orderId = "002-12-255";
            var orderIdParsed = orderId.MapToWwsOnlineOrderId();
            Assert.AreEqual(212255, orderIdParsed);
        }

        [TestMethod]
        public void MapToWwsOnlineOrderId_NewFormat()
        {
            var orderId = "002-12345678";
            var orderIdParsed = orderId.MapToWwsOnlineOrderId();
            Assert.AreEqual(12345678, orderIdParsed);
        }

        [TestMethod]
        public void MapToWwsCustomerInfo_Test()
        {
            var order = Mocks.Mocks.OrderInfo();
            var customer = order.Customer;
            var customerInfo = customer.MapToWwsCustomerInfo();
            Assert.IsNotNull(customerInfo);
            Assert.AreEqual(customer.FirstName, customerInfo.FirstName);
            Assert.AreEqual(customer.Address, customerInfo.Address.Address);
        }

        [TestMethod]
        public void MapToSalesDocumentType_Test()
        {
            Assert.AreEqual(Interfaces.SalesDocumentType.CASH_SALE, PaymentType.Cash.MapToSalesDocumentType());
            Assert.AreEqual(Interfaces.SalesDocumentType.CREDIT_NOTE, PaymentType.CreditCard.MapToSalesDocumentType());
            Assert.AreEqual(Interfaces.SalesDocumentType.CREDIT_NOTE, PaymentType.Online.MapToSalesDocumentType());
            Assert.AreEqual(Interfaces.SalesDocumentType.CREDIT_NOTE, PaymentType.SocialCard.MapToSalesDocumentType());
            Assert.AreEqual(Interfaces.SalesDocumentType.FINANCING, PaymentType.Yandex.MapToSalesDocumentType());
        }

        [TestMethod]
        public void MapToDelivery_Test()
        {
            var d = new DateTime(2010, 05, 05);
            var deliveryInfo = new Interfaces.DeliveryInfo
            {
                DateFrom = d.AddHours(8),
                DateTo = d.AddHours(16),
            };
            var delivery = deliveryInfo.MapToDelivery();
            Assert.AreEqual(deliveryInfo.DateFrom.Value, delivery.Date);
        }

        [TestMethod]
        public void MapToOrderLine_Test()
        {
            var lineInfo = Mocks.Mocks.SalesDocumentLineInfo();
            var orderLine = lineInfo.MapToOrderLine(0);
            Assert.AreEqual(ProductType.Article, orderLine.ProductType);
            Assert.AreEqual(lineInfo.Product.ArticleNo, orderLine.ArticleNo);
        }

        [TestMethod]
        public void MapToCustomer_Test()
        {
            var customerInfo = Mocks.Mocks.CustomerInfo();
            var customer = customerInfo.MapToCustomer();
            Assert.AreEqual(customerInfo.Address.ZipCode, customer.AddressIndex);
            Assert.AreEqual(customerInfo.Email, customer.Email);
        }

    }
}
