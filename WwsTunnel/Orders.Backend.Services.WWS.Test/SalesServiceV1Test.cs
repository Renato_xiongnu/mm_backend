﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMS.Integration.Gms.WebServices.Client;
using MMS.Integration.Gms.WebServices.Interfaces;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;
using MMS.Json;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class SalesServiceV1Test
    {
        private ILog _log;

        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            _log = LogManager.GetLogger("SalesServiceV1Test");
        }

        private OrderInfo OdrerInfo(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R007",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "111-11-111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 1106379, // 280000324
                        //Description = "Apple iPhone 6 16Gb Space Gray Смартфон",                        
                        Price = 780,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderInfo OdrerInfoWithSet(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R007",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "111-11-111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000292, // 280000324
                        //Description = "Apple iPhone 6 16Gb Space Gray Смартфон",                        
                        Price = 29990,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderNumberInfo OrderNumberInfo()
        {
            return new OrderNumberInfo
            {
                OrderNumber = "40595575",
                SapCode = "R007",
            };
        }
        [TestMethod]
        public void SalesService_Full()
        {
            var service = new SalesServiceV2();

            var orderInfo = OdrerInfoWithSet(1);
            _log.InfoFormat("OrderInfo = {0}", JsonConvert.SerializeObject(orderInfo));

            var response1 = service.CreateSalesOrder(orderInfo);
            _log.InfoFormat("CreateSalesOrder Response = {0}", JsonConvert.SerializeObject(response1));
            Assert.AreEqual(Orders.Backend.Services.WWS.ReturnCode.Ok, response1.OperationCode.Code);

            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = response1.OrderHeader.OrderNumber,
                SapCode = orderInfo.SapCode
            };
            _log.InfoFormat("OrderNumberInfo = {0}", JsonConvert.SerializeObject(orderNumberInfo));

            _log.InfoFormat("Test GetSalesOrder 1");
            var response2 = service.GetSalesOrder(orderNumberInfo);
            _log.InfoFormat("GetSalesOrder 1 Response = {0}", JsonConvert.SerializeObject(response2));
            Assert.AreEqual(ReturnCode.Ok, response2.OperationCode.Code);

            var response3 = service.ChangeDeliveryDate(orderNumberInfo, new DateTime(2015, 05, 05));
            _log.InfoFormat("ChangeDeliveryDate Response = {0}", JsonConvert.SerializeObject(response3));
            Assert.AreEqual(ReturnCode.Ok, response3.OperationCode.Code);

            Console.WriteLine("Test GetSalesOrder 2");
            var response4 = service.GetSalesOrder(orderNumberInfo);
            _log.InfoFormat("GetSalesOrder 2 Response = {0}", JsonConvert.SerializeObject(response4));
            Assert.AreEqual(ReturnCode.Ok, response4.OperationCode.Code);
            Assert.AreEqual(new DateTime(2015, 05, 05), response4.OrderHeader.Delivery.Date);

            var response5 = service.CancelSalesOrder(orderNumberInfo);
            _log.InfoFormat("CancelSalesOrder Response = {0}", JsonConvert.SerializeObject(response5));
            Assert.AreEqual(ReturnCode.Ok, response5.OperationCode.Code);

            Console.WriteLine("Test GetSalesOrder 3");
            var response6 = service.GetSalesOrder(orderNumberInfo);
            _log.InfoFormat("GetSalesOrder 3 Response = {0}", JsonConvert.SerializeObject(response6));
            Assert.AreEqual(ReturnCode.Ok, response6.OperationCode.Code);
            Assert.AreEqual(SalesDocumentStatus.CANCELED, response6.OrderHeader.WwsSalesDocStatus);
        }

        [TestMethod]
        public void SalesServiceV1Test_CreateSalesOrder()
        {
            var service = new SalesServiceV1();
            try
            {
                var response = service.CreateSalesOrder(OdrerInfo(2));
                Assert.IsNotNull(response);
                Console.WriteLine("Response = {0}", response);
            }
            catch (Exception ex)
            {
                Assert.Fail("Fail {0}", ex);
            }
        }

        [TestMethod]
        public void SalesServiceV1Test_GetSalesDocument()
        {
            var service = new SalesServiceV1();
            try
            {
                var response = service.GetSalesOrder(new OrderNumberInfo { OrderNumber = "40595588", SapCode = "R007" });
                Assert.IsNotNull(response);
                Console.WriteLine("Response = {0}", response);
            }
            catch(Exception ex)
            {
                Assert.Fail("Fail {0}", ex);
            }            
        }

        [TestMethod]
        public void SalesServiceV1Test_CancelSalesOrder()
        {
            var service = new SalesServiceV1();
            try
            {
                var response = service.CancelSalesOrder(new OrderNumberInfo { OrderNumber = "40595588", SapCode = "R007" });
                Assert.IsNotNull(response);
                Console.WriteLine("Response = {0}", response);
            }
            catch (Exception ex)
            {
                Assert.Fail("Fail {0}", ex);
            }
        }

        [TestMethod]
        public void SalesServiceV1Test_ChangeDeliveryDate()
        {
            var service = new SalesServiceV1();
            try
            {
                var response = service.ChangeDeliveryDate(new OrderNumberInfo { OrderNumber = "40595588", SapCode = "R007" }, new DateTime(2012, 01, 01));
                Assert.IsNotNull(response);
                Console.WriteLine("Response = {0}", response);
            }
            catch(NotImplementedException ex)
            {
                Assert.Inconclusive();
            }
            catch (Exception ex)
            {
                Assert.Fail("Fail {0}", ex);
            }
        }

    }
}
