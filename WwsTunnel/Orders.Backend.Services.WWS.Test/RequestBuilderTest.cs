﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS;
using Orders.Backend.Services.WWS.Mapping;
using Orders.Backend.Services.WWS.Helpers;
using MMS.Integration.Gms.WebServices.Interfaces;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class RequestBuilderTest
    {
        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            var logger = LogManager.GetLogger("CancelSalesOrderRequestHandler");
        }

        [TestMethod]
        public void FlatOrderLineWithSerial_Test()
        {
            var order = Mocks.Mocks.OrderInfo();
            var builder = new RequestBuilder(order, n => true, null, null);
            var orderLines = order.OrderLines.Select(l => builder.MapOrderLineSimple(l));
            var flatLines = orderLines.SelectMany(l => builder.FlatOrderLineWithSerial(l)).ToArray();
            Assert.IsNotNull(flatLines);
            Assert.AreEqual(4, flatLines.Length);
        }

        [TestMethod]
        public void MapToWwsOrderInfo_SimpleLines()
        {
            var order = Mocks.Mocks.OrderInfo();
            var builder = new RequestBuilder(order, n => true, null, null);
            var createRequest = builder.BuildCreateRequest();
            Assert.AreEqual(4, createRequest.Order.OrderLines.Count);
        }

        [TestMethod]
        public void MapToWwsOrderInfo_SetLines()
        {
            var order = Mocks.Mocks.OrderInfoWithSet();
            var builder = new RequestBuilder(order, n => true, Mocks.Mocks.SetItemList, Mocks.Mocks.SetItemList);
            var createRequest = builder.BuildCreateRequest();
            Assert.AreEqual(12, createRequest.Order.OrderLines.Count);
        }
    }
}
