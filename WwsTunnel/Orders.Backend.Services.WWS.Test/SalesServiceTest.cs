﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS;
using Orders.Backend.Services.WWS.Helpers;

namespace Orders.Backend.Services.WWS.Test
{
    [TestClass]
    public class SalesServiceTest
    {
        private OrderInfo TestOrderWithSet(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R217",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderInfo TestOrderWithSetMultyLines(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R217",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount
                    },
                     new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount,
                        
                    },
                    new OrderLine
                    {
                        ArticleNo = 1279376,
                        Price = 2300,
                        Quantity = 1
                    }
                }
            };
            return testOrderInfo;
        }

        public static void CreateHierarchy(Orders.Backend.Services.WWS.OrderInfo order, Orders.Backend.Services.WWS.OrderHeader header)
        {
            var orderRootLines = order.OrderLines;
            var headerLines = header.Lines.ToList();

            foreach(var orderRoorLine in orderRootLines)
            {
                var loops = orderRoorLine.Quantity;
                //if is set
                if(orderRoorLine.IsSet())
                {
                    var subIds = orderRoorLine.SubLines.Select(x => new { x.ArticleNo, x.Price, x.Quantity });
                    var headerSubLines = new List<Orders.Backend.Services.WWS.OrderLine>();
                    foreach(var subId in subIds)
                    {
                        var id = subId;
                        var byQty =
                            header.Lines.Where(
                                x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == loops).Take(loops);
                        if(!byQty.Any())
                            byQty = header.Lines.Where(x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == 1).Take(loops);

                        headerSubLines.AddRange(byQty);
                    }

                    foreach(var headerSubLine in headerSubLines)
                    {
                        header.Lines.Remove(headerSubLine);
                    }
                    var distSubLines = headerSubLines.GroupBy(x => x.ArticleNo).Select(x => x.First()).ToList();
                    foreach(var distSubLine in distSubLines)
                    {
                        distSubLine.Quantity = loops;
                    }

                    orderRoorLine.ProductType = Orders.Backend.Services.WWS.ProductType.Set;
                    orderRoorLine.FromModelToWwsModel();
                    orderRoorLine.SubLines = new Orders.Backend.Services.WWS.OrderLine[headerSubLines.Count];
                    orderRoorLine.SubLines = distSubLines.ToArray();
                    header.Lines.Add(orderRoorLine);
                }
                else
                {
                    var byQty =
                            header.Lines.Where(
                                x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == loops).Take(loops);
                    if(!byQty.Any())
                        byQty = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == 1).Take(loops);

                    //var headerRootLines = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price).Take(loops).ToList();
                    foreach(var headerRoorLine in byQty)
                    {
                        header.Lines.Remove(headerRoorLine);
                    }
                    var headerRootLine = byQty.FirstOrDefault();
                    if(headerRootLine != null)
                    {
                        headerRootLine.Quantity = loops;
                        headerRootLine.ProductType = Orders.Backend.Services.WWS.ProductType.Article;
                        header.Lines.Add(headerRootLine);
                    }
                }
            }

        }

        private List<SetItem> InitSet()
        {
            return new List<SetItem>
            {
                new SetItem {ArticleNo = 11111, Quantity = 3, Price = (decimal) 4400.39},
                new SetItem {ArticleNo = 2222, Quantity = 2, Price = (decimal)7000.59},
                new SetItem {ArticleNo = 3333, Quantity = 2, Price = (decimal)1000.88},
                new SetItem {ArticleNo = 4444, Quantity = 3, Price = (decimal)333.88}
            };
        }

        [TestMethod]
        public void CreateHierarchy_Test()
        {
            var order = new Orders.Backend.Services.WWS.OrderInfo();

            var setLine = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 123456789,
                Price = 555,
                Quantity = 2,
                SubLines = new Collection<Orders.Backend.Services.WWS.OrderLine>
                {
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 22222, Price = 333, Quantity = 2},
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 33333, Price = 222, Quantity = 2}

                }
            };

            var setLine2 = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 123456789,
                Price = 555,
                Quantity = 2,
                SubLines = new Collection<Orders.Backend.Services.WWS.OrderLine>
                {
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 22222, Price = 333, Quantity = 2},
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 33333, Price = 222, Quantity = 2},
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 4444, Price = 777, Quantity = 2}

                }
            };

            var setLine1 = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 22222,
                Price = 555,
                Quantity = 1,
            };
            var setLine3 = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 22222,
                Price = 555,
                Quantity = 1,
            };

            order.OrderLines = new List<Orders.Backend.Services.WWS.OrderLine> { setLine, setLine1, setLine2, setLine3 };


            var orderHeader = new Orders.Backend.Services.WWS.OrderHeader() { Lines = new List<Orders.Backend.Services.WWS.OrderLine>() };

            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 333, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 33333, Price = 222, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 333, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 33333, Price = 222, Quantity = 1 });

            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 333, Quantity = 2 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 33333, Price = 222, Quantity = 2 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 4444, Price = 777, Quantity = 2 });



            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 555, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 555, Quantity = 1 });

            CreateHierarchy(order, orderHeader);

            var foo = orderHeader;
        }
    }
}
