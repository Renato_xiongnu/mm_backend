﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WwsTunnel.Proxy.Test.ProxySales;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class FullStackTest
    {
        [TestMethod]
        public void CreateGetGetCancelOrder_WorksCorrectly()
        {
            Action fullStack = () =>
                {
                    Console.WriteLine("Start {0}", DateTime.Now);
                    var sw = new Stopwatch();
                    var service = new ProxySales.SalesServiceClient();

                    var rand = new Random();
                    var sw1 = Stopwatch.StartNew();
                    sw.Start();
                    var orderInfo = new OrderInfo
                    {
                        SapCode = "R007",
                        Customer = new CustomerInfo
                        {
                            FirstName = "Ivan",
                            Surname = "Ivanov",
                            Email = "mms.dev.test@gmail.com",
                            Mobile = "312312",
                            Address = "dasdasdas",
                        },
                        OnlineOrderId = rand.Next(10000000,99999999).ToString(),
                        IsOnlineOrder = true,
                        OrderLines = new List<OrderLine>
                        {
                            new OrderLine
                            {
                                ArticleNo = 1188761,
                                Price = 10000,
                                Quantity = 1
                            }
                        },
                        PaymentType = PaymentType.Cash,
                        SourceSystem = "MediaMarkt",
                    };
                    var createResult = service.CreateSalesOrder(orderInfo);
                    sw.Stop();
                    Console.WriteLine(sw.ElapsedMilliseconds);
                    Console.WriteLine(createResult.OperationCode.MessageText);
                    //sw.Restart();
                    //Assert.AreEqual(ReturnCode.Ok, createResult.OperationCode.Code);

                    //var getResult = service.GetSalesOrder(new OrderNumberInfo
                    //    {
                    //        OrderNumber = createResult.OrderHeader.OrderNumber,
                    //        SapCode = "R006"
                    //    });
                    //Console.WriteLine(sw.ElapsedMilliseconds);

                    //Console.WriteLine(getResult.OperationCode.MessageText);
                    //sw.Restart();
                    //Assert.AreEqual(ReturnCode.Ok, getResult.OperationCode.Code);

                    //sw.Restart();

                    //getResult = service.GetSalesOrder(new OrderNumberInfo
                    //    {
                    //        OrderNumber = createResult.OrderHeader.OrderNumber,
                    //        SapCode = "R006"
                    //    });
                    //Console.WriteLine(sw.ElapsedMilliseconds);
                    //Console.WriteLine(getResult.OperationCode.MessageText);
                    //sw.Restart();
                    //Assert.AreEqual(ReturnCode.Ok, getResult.OperationCode.Code);

                    //var cancelResult = service.CancelSalesOrder(new OrderNumberInfo
                    //    {
                    //        OrderNumber = createResult.OrderHeader.OrderNumber,
                    //        SapCode = "R006"
                    //    });
                    //Console.WriteLine(sw.ElapsedMilliseconds);
                    //Console.WriteLine(cancelResult.OperationCode.MessageText);
                    //sw.Restart();
                    //Assert.AreEqual(ReturnCode.Ok, cancelResult.OperationCode.Code);
                    //sw1.Stop();                    
                    //Console.WriteLine("Execution succeed {0}", sw1.ElapsedMilliseconds);
                };

            var actions = Enumerable.Repeat(fullStack, 10);
            try
            {
                Parallel.Invoke(new ParallelOptions {MaxDegreeOfParallelism = 100}, actions.ToArray());
            }
            catch (AggregateException ex)
            {
                foreach (var inner in ex.InnerExceptions)
                {
                    Console.WriteLine(inner.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
