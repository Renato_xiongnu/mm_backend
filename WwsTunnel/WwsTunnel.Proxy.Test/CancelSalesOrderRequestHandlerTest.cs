﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Orders.Backend.Services.WWS;

using WwsTunnel.Proxy;
using MMS.Cloud.Interfaces.Shared;
using System.Threading;
using NLog;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class CancelSalesOrderRequestHandlerTest
    {
        public CancelSalesOrderRequestHandler CancelSalesOrderRequestHandler;
        private IEntityStorage EntityStorage { get; set; }

        private OrderInfo TestOrderWithSet(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R217",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderInfo TestOrderWithSetMultyLines(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R217",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount
                    },
                     new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount,
                        
                    },
                    new OrderLine
                    {
                        ArticleNo = 1279376,
                        Price = 2300,
                        Quantity = 1
                    }
                }
            };
            return testOrderInfo;
        }

        [TestInitialize]
        public void Init()
        {
            //BasicConfigurator.Configure();
            //var appender = LogManager.GetRepository()
            //                         .GetAppenders()
            //                         .OfType<ConsoleAppender>()
            //                         .First();

            //appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            var logger = LogManager.GetLogger("CancelSalesOrderRequestHandler");

            //var responses = new [] {
            //    new CancelSalesOrderResult() {
            //        OperationCode = new OperationCode{ Code = ReturnCode.Ok }
            //    }
            //};
            CancelSalesOrderRequestHandler = new CancelSalesOrderRequestHandler(new LastSequanceProviderMock(), () => EntityStorage, logger);
            EntityStorage = new EntityStorageMock(); //Mock.Of<IEntityStorage>(s => s.GetChanges<CancelSalesOrderResult>(lastSequence, 100, out lastSequence) == responses);
        }

        [TestMethod]
        public void CancelSalesOrderRequestHandler_Process()
        {
            int _completes = 0;
            int _timeout = 0;
            var orders = Enumerable.Range(0, 5).Select(i =>
            {
                return new OrderNumberInfo
                {
                    OrderNumber = string.Format("007-02-{0:00}", i),
                    SapCode = "R007"
                };
            }).ToList();

            orders.Add(new OrderNumberInfo
                {
                    OrderNumber = "002-70020805",
                    SapCode = "R601"
                });

            var tasks = Enumerable.Range(0, 10)
                .Select(i =>
                {
                    Thread.Sleep(new Random(i).Next(10000));
                    return i;
                })
                .Select(i => Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var orderNumberInfo = orders[i % orders.Count];
                        var request = new CancelSalesOrderRequest
                        {
                            OrderNumber = orderNumberInfo.OrderNumber,
                            SapCode = orderNumberInfo.SapCode,
                            RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber)
                        };
                        var response = CancelSalesOrderRequestHandler.Process(request);
                        Interlocked.Increment(ref _completes);
                    }
                    catch (TimeoutException ex)
                    {
                        Interlocked.Increment(ref _timeout);
                    }
                }, TaskCreationOptions.LongRunning)).ToArray();
            Task.WaitAll(tasks);
            Console.WriteLine("_completes = {0}", _completes);
            Console.WriteLine("_timeout = {0}", _timeout);
        }

        [TestMethod]
        public void CancelSalesOrderRequestHandler_Process_Timeout_Cleanup()
        {
            int repeat = 20;
            int completes = 0;
            int timeouts = 0;
            int requests = 0;
            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "002-70020805",
                SapCode = "R601"
            };

            var tasks = Enumerable.Range(0, repeat)
                .Select(i =>
                {
                    Thread.Sleep(5000);
                    return i;
                })
                .Select(i => Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var request = new CancelSalesOrderRequest
                        {
                            OrderNumber = orderNumberInfo.OrderNumber,
                            SapCode = orderNumberInfo.SapCode,
                            RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber)
                        };
                        Interlocked.Increment(ref requests);
                        var response = CancelSalesOrderRequestHandler.Process(request);
                        Interlocked.Increment(ref completes);                        
                    }
                    catch(TimeoutException ex)
                    {                        
                        Interlocked.Decrement(ref requests);
                        Interlocked.Increment(ref timeouts);                        
                    }
                }, TaskCreationOptions.LongRunning)).ToArray();
            Task.WaitAll(tasks);
            Console.WriteLine("_completes = {0}", completes);
            Console.WriteLine("_timeout = {0}", timeouts);
            
            var active = CancelSalesOrderRequestHandler.GetCurrentActiveRequests();

            Assert.AreEqual(1, active);
            Assert.AreEqual(0, requests);
            Assert.AreEqual(0, completes);
            Assert.AreEqual(repeat, timeouts);
        }
    }
}