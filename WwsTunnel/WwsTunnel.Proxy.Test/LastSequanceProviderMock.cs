﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WwsTunnel.Proxy.Test
{
    public class LastSequanceProviderMock : ILastSequanceProvider
    {
        private long _lastSequance = 0;
        public long Get()
        {
            return _lastSequance;
        }
        public void Set(long value)
        {
            _lastSequance = value;
        }

    }
}
