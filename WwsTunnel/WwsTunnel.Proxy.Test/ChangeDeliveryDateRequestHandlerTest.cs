﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Orders.Backend.Services.WWS;

using WwsTunnel.Proxy;
using MMS.Cloud.Interfaces.Shared;
using System.Threading;
using NLog;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class ChangeDeliveryDateRequestHandlerTest
    {
        public ChangeDeliveryDateRequestHandler ChangeDeliveryDateRequestHandler;
        
        private IEntityStorage EntityStorage { get; set; }

        [TestInitialize]
        public void Init()
        {
            //BasicConfigurator.Configure();
            //var appender = LogManager.GetRepository()
            //                         .GetAppenders()
            //                         .OfType<ConsoleAppender>()
            //                         .First();

            //appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            var logger = LogManager.GetLogger("ChangeDeliveryDateRequestHandler");

            //var responses = new [] {
            //    new CancelSalesOrderResult() {
            //        OperationCode = new OperationCode{ Code = ReturnCode.Ok }
            //    }
            //};
            ChangeDeliveryDateRequestHandler = new ChangeDeliveryDateRequestHandler(new LastSequanceProviderMock(), () => EntityStorage, logger);
            EntityStorage = new EntityStorageMock(); //Mock.Of<IEntityStorage>(s => s.GetChanges<CancelSalesOrderResult>(lastSequence, 100, out lastSequence) == responses);
        }

        [TestMethod]
        public void ChangeDeliveryDateRequestHandlerTest_Process()
        {
            int _completes = 0;
            int _timeout = 0;
            var date = new DateTime(2000, 01, 01);
            var orders = Enumerable.Range(0, 5).Select(i =>
            {
                return new OrderNumberInfo
                {
                    OrderNumber = string.Format("007-02-{0:00}", i),
                    SapCode = "R007"
                };
            }).ToList();
            
            var tasks = Enumerable.Range(0, 10)
                .Select(i =>
                {
                    Thread.Sleep(new Random(i).Next(10000));
                    return i;
                })
                .Select(i => Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var orderNumberInfo = orders[i % orders.Count];
                        var request = new ChangeDeliveryDateRequest
                        {
                            OrderNumber = orderNumberInfo.OrderNumber,
                            SapCode = orderNumberInfo.SapCode,
                            RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber),
                            DeliveryDate = date,
                        };
                        var response = ChangeDeliveryDateRequestHandler.Process(request);
                        Interlocked.Increment(ref _completes);
                    }
                    catch(TimeoutException ex)
                    {
                        Interlocked.Increment(ref _timeout);
                    }
                }, TaskCreationOptions.LongRunning)).ToArray();
            Task.WaitAll(tasks);
            Console.WriteLine("_completes = {0}", _completes);
            Console.WriteLine("_timeout = {0}", _timeout);
        }
    }
}
