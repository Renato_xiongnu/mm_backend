﻿using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class FileLastSequanceProviderTest
    {
        private string _directory;
        private const string FilePath = @"Config\CancelSalesOrder.txt";
        private string _fullPath;

        [TestInitialize]
        public void Init()
        {
            _directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ??
                         string.Empty;
            _fullPath = Path.Combine(_directory, FilePath);
            var fileInfo = new FileInfo(_fullPath);
            if(fileInfo.Exists)
            {
                fileInfo.Delete();
            }

            
        }

        [TestMethod]
        public void FileExistAndEmpty_FileLastSequanceIsZero()
        {
            var fileLastSequanceProvider = new FileLastSequanceProvider(_fullPath);
            var lastSequance = fileLastSequanceProvider.Get();
            Assert.AreEqual(0, lastSequance);
        }

        [TestMethod]
        public void FileNotExistAndEmpty_FileLastSequanceIsZero()
        {
            var fileLastSequanceProvider = new FileLastSequanceProvider(_fullPath);
            var lastSequance = fileLastSequanceProvider.Get();
            Assert.AreEqual(0, lastSequance);
        }

        [TestMethod]
        public void FileNotExistAndEmpty_ReadAndWriteValue()
        {
            var fileLastSequanceProvider = new FileLastSequanceProvider(_fullPath);
            var lastSequance = fileLastSequanceProvider.Get();
            Assert.AreEqual(0, lastSequance);
            fileLastSequanceProvider.Set(5);
            lastSequance = fileLastSequanceProvider.Get();
            Assert.AreEqual(5, lastSequance);
        }
    }
}