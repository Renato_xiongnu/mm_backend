﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS;
using Orders.Backend.Services.WWS.Helpers;
using Ploeh.AutoFixture;
using WwsTunnel.Proxy.Test.ProxySales;

namespace WwsTunnel.Proxy.Test
{ 
    [TestClass]
    public class SalesServiceTest
    {
        private SalesServiceClient _service;

        [TestInitialize]
        public void Init()
        {
            _service = new SalesServiceClient();
        }

        private OrderInfo TestOrderWithSet(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R217",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderInfo TestOrderWithSetMultyLines(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R217",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount
                    },
                     new OrderLine
                    {
                        ArticleNo = 280000690,//280000324,
                        Price = 28000,
                        Quantity = setCount,
                        
                    },
                    new OrderLine
                    {
                        ArticleNo = 1279376,
                        Price = 2300,
                        Quantity = 1
                    }
                }
            };
            return testOrderInfo;
        }



        [TestMethod]
        public void Test1()
        {
            var order = new Orders.Backend.Services.WWS.OrderInfo();

            var setLine = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 123456789,
                Price = 555,
                Quantity = 2,
                SubLines = new Collection<Orders.Backend.Services.WWS.OrderLine>
                {
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 22222, Price = 333, Quantity = 2},
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 33333, Price = 222, Quantity = 2}

                }
            };

            var setLine2 = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 123456789,
                Price = 555,
                Quantity = 2,
                SubLines = new Collection<Orders.Backend.Services.WWS.OrderLine>
                {
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 22222, Price = 333, Quantity = 2},
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 33333, Price = 222, Quantity = 2},
                    new Orders.Backend.Services.WWS.OrderLine {ArticleNo = 4444, Price = 777, Quantity = 2}

                }
            };

            var setLine1 = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 22222,
                Price = 555,
                Quantity = 1,
            };
            var setLine3 = new Orders.Backend.Services.WWS.OrderLine
            {
                ArticleNo = 22222,
                Price = 555,
                Quantity = 1,
            };

            order.OrderLines = new List<Orders.Backend.Services.WWS.OrderLine> {setLine, setLine1, setLine2, setLine3};


            var orderHeader = new Orders.Backend.Services.WWS.OrderHeader() { Lines = new List<Orders.Backend.Services.WWS.OrderLine>() };

            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 333, Quantity = 1});
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 33333, Price = 222, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 333, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 33333, Price = 222, Quantity = 1 });

            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 333, Quantity = 2 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 33333, Price = 222, Quantity = 2 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 4444, Price = 777, Quantity = 2 });
      
          

            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 555, Quantity = 1 });
            orderHeader.Lines.Add(new Orders.Backend.Services.WWS.OrderLine { ArticleNo = 22222, Price = 555, Quantity = 1 });

            CreateHierarchy(order, orderHeader);

            var foo = orderHeader;


        }


        public static void CreateHierarchy(Orders.Backend.Services.WWS.OrderInfo order, Orders.Backend.Services.WWS.OrderHeader header)
        {
            var orderRootLines = order.OrderLines;
            var headerLines = header.Lines.ToList();

            foreach (var orderRoorLine in orderRootLines)
            {
                var loops = orderRoorLine.Quantity;
                //if is set
                if (orderRoorLine.IsSet())
                {
                    var subIds = orderRoorLine.SubLines.Select(x => new { x.ArticleNo, x.Price, x.Quantity });
                    var headerSubLines = new List<Orders.Backend.Services.WWS.OrderLine>();
                    foreach (var subId in subIds)
                    {
                        var id = subId;
                        var byQty =
                            header.Lines.Where(
                                x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == loops).Take(loops);
                        if (!byQty.Any())
                            byQty = header.Lines.Where(x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == 1).Take(loops);
                       
                        headerSubLines.AddRange(byQty);
                    }
                   
                    foreach (var headerSubLine in headerSubLines)
                    {
                        header.Lines.Remove(headerSubLine);
                    }
                    var distSubLines = headerSubLines.GroupBy(x=>x.ArticleNo).Select(x=>x.First()).ToList();
                    foreach (var distSubLine in distSubLines)
                    {
                        distSubLine.Quantity = loops;
                    }

                    orderRoorLine.ProductType = Orders.Backend.Services.WWS.ProductType.Set;
                    orderRoorLine.FromModelToWwsModel();
                    orderRoorLine.SubLines = new Orders.Backend.Services.WWS.OrderLine[headerSubLines.Count];
                    orderRoorLine.SubLines = distSubLines.ToArray();
                    header.Lines.Add(orderRoorLine);
                }
                else
                {
                    var byQty =
                            header.Lines.Where(
                                x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == loops).Take(loops);
                    if (!byQty.Any())
                        byQty = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == 1).Take(loops);
                   
                    //var headerRootLines = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price).Take(loops).ToList();
                    foreach (var headerRoorLine in byQty)
                    {
                        header.Lines.Remove(headerRoorLine);
                    }
                    var headerRootLine = byQty.FirstOrDefault();
                    if (headerRootLine != null)
                    {
                        headerRootLine.Quantity = loops;
                        headerRootLine.ProductType = Orders.Backend.Services.WWS.ProductType.Article;
                        header.Lines.Add(headerRootLine);
                    }
                }
            }

        }


     
      

        private List<SetItem> InitSet()
        {
            return new List<SetItem>
            {
                new SetItem {ArticleNo = 11111, Quantity = 3, Price = (decimal) 4400.39},
                new SetItem {ArticleNo = 2222, Quantity = 2, Price = (decimal)7000.59},
                new SetItem {ArticleNo = 3333, Quantity = 2, Price = (decimal)1000.88},
                new SetItem {ArticleNo = 4444, Quantity = 3, Price = (decimal)333.88}
            }; 
        }


        [TestMethod]
        public void TestRecalculatePrice()
        {
            var setPrice = 10500;
            var setLines = InitSet();
            Decimal sum = 0;

          
            SetPriceRecalculator.RecalculatePrices(setLines, setPrice);
            sum = setLines.Sum(x => x.Price * x.Quantity);
            Assert.AreEqual(setPrice, sum);

            setPrice = 15500;
            setLines = InitSet();

            SetPriceRecalculator.RecalculatePrices(setLines, setPrice);
            sum = setLines.Sum(x => x.Price * x.Quantity);
            Assert.AreEqual(setPrice, sum);

            setPrice = 17500;
            setLines = InitSet(); 
            SetPriceRecalculator.RecalculatePrices(setLines, setPrice);
            sum = setLines.Sum(x => x.Price * x.Quantity);
            Assert.AreEqual(setPrice, sum);

            setPrice = 20500;
            setLines = InitSet();
            SetPriceRecalculator.RecalculatePrices(setLines, setPrice);
            sum = setLines.Sum(x => x.Price * x.Quantity);
            Assert.AreEqual(setPrice, sum);

            setPrice = 25500;
            setLines = InitSet();
            SetPriceRecalculator.RecalculatePrices(setLines, setPrice);
            sum = setLines.Sum(x => x.Price * x.Quantity);
            Assert.AreEqual(setPrice, sum);

            setPrice = 78500;
            setLines = InitSet();
            SetPriceRecalculator.RecalculatePrices(setLines, setPrice);
            sum = setLines.Sum(x => x.Price * x.Quantity);
            Assert.AreEqual(setPrice, sum);

        }
   
        [TestMethod]
        public void CreateSaleOrderForSet_SingleSet()
        {
            var request = TestOrderWithSet(1);
            request.SapCode = "R007";
            request.OnlineOrderId = "111-22-24";
            var order = _service.CreateSalesOrder(request);
            Assert.IsNotNull(order);
            Assert.IsNotNull(order.OrderHeader);
            var orderLine = order.OrderHeader.Lines.FirstOrDefault();
            Assert.IsNotNull(orderLine);
            Assert.IsNotNull(orderLine.SubLines);
            var subLinesCount = orderLine.SubLines.Count();
            Assert.AreEqual(2, subLinesCount);
            var priceSum = order.OrderHeader.Lines.Sum(x => x.Price);
            var priceBeforeSend = request.OrderLines.Sum(x => x.Price);
            Assert.AreEqual(priceBeforeSend, priceSum);
            Console.WriteLine(order.OrderHeader.OrderNumber);
        }


        [TestMethod]
        public void CreateSaleOrderForSet_ManySetCount()
        {
            var setCount =1;
            var request = TestOrderWithSet(setCount);
            request.SapCode = "R007";
            request.OnlineOrderId = "111-22-24";
            var order = _service.CreateSalesOrder(request);
            Assert.IsNotNull(order);
            Assert.IsNotNull(order.OrderHeader);
            var orderLinesCount = order.OrderHeader.Lines.Count();
            Assert.AreEqual(1, orderLinesCount);
            var priceBeforeSend = request.OrderLines.Sum(x => x.Price)*setCount;
            Decimal subLinesSumm = order.OrderHeader.Lines.Sum(line => line.SubLines.Sum(subLine => subLine.Price));

            Assert.AreEqual(priceBeforeSend, subLinesSumm);
            Console.WriteLine(order.OrderHeader.OrderNumber);
        }

        [TestMethod]
        public void CreateSaleOrderForSet_ManySetManyLains()
        {
            var setCount = 2;
            var request = TestOrderWithSetMultyLines(setCount);
            request.SapCode = "R217";
            request.OnlineOrderId = "111-22-24";
            var order = _service.CreateSalesOrder(request);
            Assert.IsNotNull(order);
            Assert.IsNotNull(order.OrderHeader);
            var orderLinesCount = order.OrderHeader.Lines.Count();
            Assert.AreEqual(3, orderLinesCount);

            var priceBeforeSend = request.OrderLines.Sum(line => line.Price*line.Quantity);

            //var subLinesSumm = order.OrderHeader.Lines.Where(x => x.ProductType == ProductType.Set).Sum(line => line.SubLines.Sum(subLine => subLine.Price)) + order.OrderHeader.Lines.Where(x => x.ProductType != ProductType.Set).Sum(x=>x.Price);
            Decimal subLinesSumm = 0;
            var sets = order.OrderHeader.Lines.Where(x => x.ProductType == ProductType.Set);

            //foreach (var set in sets)
            //{

            //    subLinesSumm += set.SubLines.Select(x => x.Price).Sum();
            //}

            //Assert.AreEqual(priceBeforeSend, subLinesSumm);
            Console.WriteLine(order.OrderHeader.OrderNumber);
        }

        [TestMethod]
        public void CreateSalesOrder()
        {
            var request = CreateSalesOrderRequest();
            request.OrderLines = new List<OrderLine>()
            {
                new OrderLine
                {
                    ArticleNo = 280000379,
                    Price = 20999,
                    Quantity = 1
                }
            };
            request.SapCode = "R007";
            request.OnlineOrderId = "111-22-24";
            var order = _service.CreateSalesOrder(request);
            Assert.IsNotNull(order);
            Assert.IsNotNull(order.OrderHeader);
            Console.WriteLine(order.OrderHeader.OrderNumber);
            //GetSalesOrder(order);
            //CancelSalesOrder(order);
        }

        [TestMethod]
        public void CreateSalesOrder_WithDeliveryDate()
        {
            var deliveryDate = DateTime.UtcNow.AddDays(1);
            var request = CreateSalesOrderRequest();
            request.OrderLines = new List<OrderLine>()
            {
                new OrderLine
                {
                    ArticleNo = 280000379,
                    Price = 20999,
                    Quantity = 1
                }
            };
            request.SapCode = "R007";
            request.OnlineOrderId = "111-22-24";
            request.DeliveryDate = deliveryDate;
            var order = _service.CreateSalesOrder(request);            
            Assert.IsNotNull(order);            
            Assert.IsNotNull(order.OrderHeader);
            Assert.AreEqual(deliveryDate.Date, order.OrderHeader.Delivery.Date.Date);
            Console.WriteLine(order.OrderHeader.OrderNumber);
        }

        [TestMethod]
        public void GetSalesOrder()
        {
            var orderNumberInfo = new OrderNumberInfo()
            {
                OrderNumber = "40595740",
                SapCode = "R007"
            };
            var order = _service.GetSalesOrder(orderNumberInfo);
            Console.WriteLine(order.OrderId);
            Assert.IsNotNull(order);
            Assert.IsNotNull(order.OrderHeader);
            Console.WriteLine(order.OrderHeader.OrderNumber);
            Console.WriteLine(order.OrderHeader.Delivery.Date);
        }

        [TestMethod]
        public void CancelSalesOrder_TimeoutException()
        {
            var info = new OrderNumberInfo
            {
                OrderNumber = "40254398",
                SapCode = "R251"
            };
            try
            {
                _service.CancelSalesOrder(info);
            }
            catch(TimeoutException)
            {
                return;
            }
            //Assert.Fail();
        }

        //private void GetSalesOrder(CreateSalesOrderResult order)
        //{
        //    var order1 = _service.GetSalesOrder(new GetSalesOrderRequest
        //        {
        //            OrderNumber = order.OrderHeader.OrderNumber,
        //            SapCode = order.SapCode
        //        });
        //    Assert.AreEqual(order.OrderId, order1.OrderId);
        //}

        private static OrderInfo CreateSalesOrderRequest()
        {
            return new OrderInfo
            {
                SapCode = "R006",
                Customer = new CustomerInfo
                {
                    FirstName = "Ivan",
                    Surname = "Ivanov",
                    Email = "mms.dev.test@gmail.com",
                    Mobile = "312312",
                    Address = "dasdasdas",
                },
                OnlineOrderId = "11111",
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 111111,
                        Price = 31,
                        Quantity = 1
                    }
                }
            };
        }
    }
}