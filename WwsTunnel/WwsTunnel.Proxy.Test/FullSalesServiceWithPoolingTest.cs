﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orders.Backend.Services.WWS;
using Orders.Backend.Services.WWS.Helpers;
using WwsTunnel.Proxy;
//using WwsTunnel.Proxy.Test.ProxySales;
using NLog;
using MMS.Json;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class FullSalesServiceWithPoolingTest
    {
        private SalesService _service;
        private Logger _log;

        [TestInitialize]
        public void Init()
        {
            _log = LogManager.GetLogger("SalesServiceV2Test");

            _service = new SalesService();
        }

        private OrderInfo OrderInfo(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R007",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"),
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 1106379, // 280000324
                        //Description = "Apple iPhone 6 16Gb Space Gray Смартфон",                        
                        Price = 780,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderInfo OdrerInfoWithSet(Int32 setCount)
        {
            var testOrderInfo = new OrderInfo
            {
                SapCode = "R007",
                Customer = new CustomerInfo
                {
                    FirstName = "Акакий",
                    Surname = "Пупкин",
                    Email = "foo@bar.com",
                    Mobile = "80000000000",
                    Address = "dasdasdas",
                },
                OnlineOrderId = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"),
                PaymentType = PaymentType.Cash,
                SourceSystem = "MediaMarkt",
                OrderLines = new List<OrderLine>
                {
                    new OrderLine
                    {
                        ArticleNo = 280000292, // 280000324
                        //Description = "Apple iPhone 6 16Gb Space Gray Смартфон",                        
                        Price = 29990,
                        Quantity = setCount
                    }
                }
            };
            return testOrderInfo;
        }

        private OrderNumberInfo OrderNumberInfo()
        {
            var orderInfo = OrderInfo(2);
            return new OrderNumberInfo
            {
                OrderNumber = "40595575",
                SapCode = orderInfo.SapCode,
            };
        }

        public TResult RepeatTimeout<TResult>(Func<TResult> func, int number = 5, int timeout = 5000)
        {
            for (int i = 0; i < number; i++)
            {
                try
                {
                    return func();
                }
                catch (TimeoutException)
                {
                    Thread.Sleep(timeout);
                }
            }
            throw new TimeoutException();
        }

        [TestMethod]
        public void SalesService_Pooling_Full()
        {
            var service = _service;
            try
            {
                var orderInfo = OdrerInfoWithSet(1);
                Console.WriteLine("OrderInfo = {0}", JsonConvert.SerializeObject(orderInfo));

                var response1 = RepeatTimeout(() => service.CreateSalesOrder(orderInfo));
                Console.WriteLine("CreateSalesOrder Response = {0}", JsonConvert.SerializeObject(response1));
                Assert.AreEqual(ReturnCode.Ok, response1.OperationCode.Code);

                var orderNumberInfo = new OrderNumberInfo
                {
                    OrderNumber = response1.OrderHeader.OrderNumber,
                    SapCode = orderInfo.SapCode
                };
                Console.WriteLine("OrderNumberInfo = {0}", JsonConvert.SerializeObject(orderNumberInfo));

                Console.WriteLine("Test GetSalesOrder 1");
                var response2 = RepeatTimeout(() => service.GetSalesOrder(orderNumberInfo));
                Console.WriteLine("GetSalesOrder 1 Response = {0}", JsonConvert.SerializeObject(response2));
                Assert.AreEqual(ReturnCode.Ok, response2.OperationCode.Code);

                var response3 = RepeatTimeout(() => service.ChangeDeliveryDate(orderNumberInfo, new DateTime(2015, 05, 05)));
                Console.WriteLine("ChangeDeliveryDate Response = {0}", JsonConvert.SerializeObject(response3));
                Assert.AreEqual(ReturnCode.Ok, response3.OperationCode.Code);

                Console.WriteLine("Test GetSalesOrder 2");
                var response4 = RepeatTimeout(() => service.GetSalesOrder(orderNumberInfo));
                Console.WriteLine("GetSalesOrder 2 Response = {0}", JsonConvert.SerializeObject(response4));
                Assert.AreEqual(ReturnCode.Ok, response4.OperationCode.Code);

                var response5 = RepeatTimeout(() => service.CancelSalesOrder(orderNumberInfo));
                Console.WriteLine("CancelSalesOrder Response = {0}", JsonConvert.SerializeObject(response5));
                Assert.AreEqual(ReturnCode.Ok, response5.OperationCode.Code);

                Console.WriteLine("Test GetSalesOrder 3");
                var response6 = RepeatTimeout(() => service.GetSalesOrder(orderNumberInfo));
                Console.WriteLine("GetSalesOrder 3 Response = {0}", JsonConvert.SerializeObject(response6));
                Assert.AreEqual(ReturnCode.Ok, response6.OperationCode.Code);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [TestMethod]
        public void SalesService_CreateSalesOrder()
        {
            var orderInfo = OrderInfo(2);
            var response = _service.CreateSalesOrder(orderInfo);
            Console.WriteLine("Response = {0}", JsonConvert.SerializeObject(response));
        }

        [TestMethod]
        public void SalesService_GetSalesOrder()
        {
            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "40595602",
                SapCode = "R007",
            };
            var response = RepeatTimeout(() => _service.GetSalesOrder(orderNumberInfo));
            Console.WriteLine("Response = {0}", JsonConvert.SerializeObject(response));
        }

        [TestMethod]
        public void SalesService_ChangeDeliveryDate()
        {
            var orderInfo = OrderNumberInfo();
            var response = _service.ChangeDeliveryDate(orderInfo, new DateTime(2000, 01, 01));
            Console.WriteLine("Response = {0}", JsonConvert.SerializeObject(response));
        }

    }
}
