﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class WcfSalesServiceTest
    {
        private WwsTunnel.Proxy.Test.ProxySales.ISalesService _service;

        [TestInitialize]
        public void Init()
        {
            _service = new WwsTunnel.Proxy.Test.ProxySales.SalesServiceClient();
        }

        [TestMethod]
        public void CreateSalesOrder()
        {
            var request = new OrderInfo();
            //var fixture = new Fixture();
            //fixture.Register<ExtensionDataObject>(() => null);
            //fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            //request = fixture.Create<OrderInfo>();
            request = CreateSalesOrderRequest();
            request.SapCode = "R007";
            request.OnlineOrderId = "111-22-322";
            var order = _service.CreateSalesOrder(request);
            Assert.IsNotNull(order);
            Assert.IsNotNull(order.OrderHeader);
            //GetSalesOrder(order);
            //CancelSalesOrder(order);
        }

        //private void CancelSalesOrder(CreateSalesOrderResult order)
        //{
        //    var result3 = _service.CancelSalesOrder(new CancelSalesOrderRequest
        //        {
        //            OrderNumber = order.OrderHeader.OrderNumber,
        //            SapCode = order.SapCode
        //        });
        //    Assert.AreEqual("Ok", result3.OperationCode.Code);
        //}

        //private void GetSalesOrder(CreateSalesOrderResult order)
        //{
        //    var order1 = _service.GetSalesOrder(new GetSalesOrderRequest
        //        {
        //            OrderNumber = order.OrderHeader.OrderNumber,
        //            SapCode = order.SapCode
        //        });
        //    Assert.AreEqual(order.OrderId, order1.OrderId);
        //}

        private static OrderInfo CreateSalesOrderRequest()
        {
            return new OrderInfo
                {
                    SapCode = "R006",
                    Customer = new CustomerInfo
                        {
                            FirstName = "Ivan",
                            Surname = "Ivanov",
                            Email = "mms.dev.test@gmail.com",
                            Mobile = "312312",
                            Address = "dasdasdas",
                            ZipCode = "111111"
                        },
                    OnlineOrderId = "11111",
                    OrderLines = new List<OrderLine>
                        {
                            new OrderLine
                                {
                                    ArticleNo = 280000379,
                                    Price = 150000,
                                    Quantity = 2
                                }
                        }
                };
        }

        [TestMethod]
        public void CancelSalesOrder_TimeoutException()
        {
            var info = new OrderNumberInfo
            {
                OrderNumber = "40780184",
                SapCode = "R204"
            };
            try
            {
               var cancelResult= _service.CancelSalesOrder(info);
               Assert.AreEqual(ReturnCode.Ok, cancelResult.OperationCode.Code);
            }
            catch (TimeoutException)
            {
                Assert.Fail();
                return;
            }
        }

    }
}