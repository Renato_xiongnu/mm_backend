﻿using MMS.Cloud.Interfaces.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy.Test
{
    public class EntityStorageMock : IEntityStorage
    {
        List<CancelSalesOrderResult> _cancelResults = Enumerable.Range(0, 100).Select(i => 
        {
            return new CancelSalesOrderResult()
            {
                RequestId = string.Format("{0}_007-02-{1:00}", "R007", i),
                OperationCode = new OperationCode { Code = ReturnCode.Ok }
            };
        }).ToList();

        List<ChangeDeliveryDateResult> _changeDeliveryDateResults = Enumerable.Range(0, 100).Select(i =>
        {
            return new ChangeDeliveryDateResult()
            {
                RequestId = string.Format("{0}_007-02-{1:00}", "R007", i),
                OperationCode = new OperationCode { Code = ReturnCode.Ok }
            };
        }).ToList();


        public ICollection<TEntity> GetAllDocuments<TEntity>() where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<object> GetChanges(Type type, long since, int limit, out long lastSeq)
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetChanges<TEntity>(long since, int limit, out long lastSeq) where TEntity : class, new()
        {
            lastSeq = since + 1;
            if (typeof(TEntity) == typeof(CancelSalesOrderResult))
            {
                return (ICollection<TEntity>)_cancelResults.Where((e, i) => (since % _cancelResults.Count) == i).ToList();
            }
            if(typeof(TEntity) == typeof(ChangeDeliveryDateResult))
            {
                return (ICollection<TEntity>)_changeDeliveryDateResults.Where((e, i) => (since % _changeDeliveryDateResults.Count) == i).ToList();
            }
            return null;
        }

        public ICollection<TEntity> GetChanges<TEntity>(long since, int limit, ICollection<string> systemTypes, out long lastSeq, ICollection<string> excludeSystemTypes = null) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetChangesForSystem<TEntity>(string systemName, long since, int limit, out long lastSeq) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<object> GetEntities(Type type, string viewId, string viewName, string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetEntities<TEntity>(string designDocument, string viewName, ICollection<string> keys) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetEntities<TEntity>(string designDocument, string viewName, string key = null) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetEntities<TEntity>(ICollection<string> keys) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetEntitiesRanged<TEntity>(string designDocument, string viewName, ICollection<string> startKeys, ICollection<string> endKeys = null) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetEntitiesRanged<TEntity>(string designDocument, string viewName, string startKey, string endKey = null) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public TEntity GetEntity<TEntity>(string key) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public long GetLastSeq<TEntity>()
        {
            throw new NotImplementedException();
        }

        public void SaveEntity<TEntity>(TEntity entity, string systemType) where TEntity : class, new()
        {
        }

        public void SaveEntity<TEntity>(TEntity entity) where TEntity : class, new()
        {
        }

        public void SaveEntityReplicationBuffer<TEntity>(TEntity entity, string systemType) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public CancelSalesOrderResult TEntity { get; set; }
    }
}
