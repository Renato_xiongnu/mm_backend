﻿using System.Threading;
using NLog;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMS.Storage.CouchDB.DreamSeat;
using Moq;

namespace WwsTunnel.Proxy.Test
{
    [TestClass]
    public class ConcurrentExecutorTest
    {
        [TestMethod]
        public void ConcurrentExecutor_Test()
        {
            var executor = new ConcurrentExecutor<string>();
            var a = 0;
            executor.Execute("string1", () => { a += 1; });
        }
    }

    public class InMemoryLastSequanceProvider : ILastSequanceProvider
    {
        private long _sequance = 0;
        public long Get()
        {
            return _sequance;
        }

        public void Set(long value)
        {
            _sequance = value;
        }
    }
}
