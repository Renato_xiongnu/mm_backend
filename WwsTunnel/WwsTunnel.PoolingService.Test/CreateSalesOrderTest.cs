﻿using System;
using System.Text;
using System.Collections.Generic;
using MMS.Storage.CouchDB.DreamSeat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.Test
{
    [TestClass]
    public class CreateSalesOrderTest
    {
        [TestMethod]
        public void CreateSalesOrdersAttachesCorrectly()
        {
            var storage = new CouchDbStorage();
            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var createOrderRequest = fixture.Create<CreateSalesOrderRequest>();
            createOrderRequest.SapCode = "R006";

            storage.SaveEntity(createOrderRequest);
        }

        [TestMethod]
        public void CreateCorrectSalesOrdersAttachesFully()
        {
            var storage = new CouchDbStorage();
            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var createOrderRequest = fixture.Create<CreateSalesOrderRequest>();
            createOrderRequest.SapCode = "R006";
            createOrderRequest.OrderLines = new List<OrderLine>
                {
                    new OrderLine
                        {
                            ArticleNo = 1111111,
                            Price = 10000,
                            Quantity = 3
                        }
                };
            createOrderRequest.OnlineOrderId = "006-01-001";

            storage.SaveEntity(createOrderRequest);
        }
        
        [TestMethod]
        public void GetSalesOrdersAttachesCorrectly()
        {
            var storage = new CouchDbStorage();
            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var createOrderRequest = fixture.Create<GetSalesOrderRequest>();
            createOrderRequest.SapCode = "R006";

            storage.SaveEntity(createOrderRequest);
        }

        [TestMethod]
        public void CancelSalesOrdersAttachesCorrectly()
        {
            var storage = new CouchDbStorage();
            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var createOrderRequest = fixture.Create<CancelSalesOrderRequest>();
            createOrderRequest.SapCode = "R006";

            storage.SaveEntity(createOrderRequest);
        }

        [TestMethod]
        public void ChangeDeliveryDateAttachesCorrectly()
        {
            var storage = new CouchDbStorage();
            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var request = fixture.Create<ChangeDeliveryDateRequest>();
            request.SapCode = "R006";
            request.DeliveryDate = new DateTime(2000, 01, 01);
            storage.SaveEntity(request);
        }
    }
}
