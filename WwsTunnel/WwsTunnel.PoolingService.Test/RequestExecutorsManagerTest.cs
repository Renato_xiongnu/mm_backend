﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;
using MMS.Storage.CouchDB.DreamSeat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using WwsTunnel.PoolingService;
using WwsTunnel.PoolingService.ParallelExecutors;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.PoolingService.Test
{
    [TestClass]
    public class RequestExecutorsManagerTest
    {
        private IRequestExecutorsManager[] _parallelExecutorManagers;
        private ILog Logger;

        [TestInitialize]
        public void Init()
        {
            BasicConfigurator.Configure();
            var appender = LogManager.GetRepository()
                                     .GetAppenders()
                                     .OfType<ConsoleAppender>()
                                     .First();

            appender.Layout = new PatternLayout("[%d] %-5level %logger - %m%n"); // set pattern
            Logger = LogManager.GetLogger("RequestExecutorsManagerTest");

            _parallelExecutorManagers = new IRequestExecutorsManager[]
            {
                new RequestExecutorsManager<CreateRequestExecutor>(10),
                new RequestExecutorsManager<CancelRequestExecutor>(10),
                new RequestExecutorsManager<GetRequestExecutor>(10),
                new RequestExecutorsManager<ChangeDeliveryDateRequestExecutor>(10),
            };


        }

        [TestCleanup]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void RequestExecutorsManager_Start_Stop()
        {
            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Start();
                }
                catch(Exception ex)
                {
                    Logger.Error("Start manager exception: ", ex);
                }
            });

            Thread.Sleep(5000);

            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Stop();
                }
                catch(Exception ex)
                {
                    Logger.Error("Stop manager exception: ", ex);
                }
            });
        }

        [TestMethod]
        public void RequestExecutorsManager_CancelRequest()
        {
            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Start();
                }
                catch(Exception ex)
                {
                    Logger.Error("Start manager exception: ", ex);
                }
            });

            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "002-70020805",
                SapCode = "R601"
            };
            var item = new CancelSalesOrderRequest
            {
                OrderNumber = orderNumberInfo.OrderNumber,
                SapCode = orderNumberInfo.SapCode,
                RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber)

            };
            Context.CancelRequestQueue.Enqueue(item);

            Thread.Sleep(5000);

            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Stop();
                }
                catch(Exception ex)
                {
                    Logger.Error("Stop manager exception: ", ex);
                }
            });
        }

        [TestMethod]
        public void RequestExecutorsManager_ChangeDeliveryDateRequest()
        {
            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Start();
                }
                catch(Exception ex)
                {
                    Logger.Error("Start manager exception: ", ex);
                }
            });

            var date = new DateTime(2000, 01, 01);

            var orderNumberInfo = new OrderNumberInfo
            {
                OrderNumber = "002-70020805",
                SapCode = "R601"
            };
            var item = new ChangeDeliveryDateRequest
            {
                OrderNumber = orderNumberInfo.OrderNumber,
                SapCode = orderNumberInfo.SapCode,
                RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber),
                DeliveryDate = date,

            };
            Context.ChangeDeliveryDateQueue.Enqueue(item);

            Thread.Sleep(5000);

            Parallel.ForEach(_parallelExecutorManagers, manager =>
            {
                try
                {
                    manager.Stop();
                }
                catch(Exception ex)
                {
                    Logger.Error("Stop manager exception: ", ex);
                }
            });
        }

    }
}
