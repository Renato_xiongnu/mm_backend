﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    public interface IOperationResult
    {
        [DataMember]
        OperationCode OperationCode { get; set; }
    }
}