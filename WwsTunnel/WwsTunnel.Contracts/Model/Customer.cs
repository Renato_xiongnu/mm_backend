﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public string ClassificationNumber  { get; set; }
        
        [DataMember]
        public string Salutation { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Birthday { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Phone2 { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string CountryAbbreviation { get; set; }

        [DataMember]
        public string AddressIndex { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string INN { get; set; }

        [DataMember]
        public string KPP { get; set; }

    }
}