﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class OperationCode
    {
        [DataMember]
        public ReturnCode Code { get; set; }

        [DataMember]
        public string SubCode { get; set; }

        [DataMember]
        public string MessageText { get; set; }
    }
}