﻿namespace WwsTunnel.Contracts.Model
{
    public enum ProductType
    {
        Article,
        Set,
        WarrantyPlus
    }
}