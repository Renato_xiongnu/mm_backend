﻿using System;
using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class Delivery
    {
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Period { get; set; }

        [DataMember]
        public string Comment { get; set; }
    }
}