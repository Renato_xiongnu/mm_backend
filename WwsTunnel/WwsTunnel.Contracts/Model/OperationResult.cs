﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class OperationResult : IOperationResult
    {        

        [DataMember]
        public OperationCode OperationCode { get; set; }

        public OperationResult()
        {
            OperationCode = new OperationCode()
                {
                    Code = ReturnCode.Ok,
                    SubCode = string.Empty
                };
        }
    }
}