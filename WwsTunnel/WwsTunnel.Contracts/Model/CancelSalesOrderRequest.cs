﻿using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]    
    public class CancelSalesOrderRequest
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}