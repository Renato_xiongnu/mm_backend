﻿namespace WwsTunnel.Contracts.Model
{
    public enum SalesOrderStatus
    {
        Unknown,
        ToPay,
        Paid,
        Canceled,
        Transformed,
        Prepaid,
        PaidBack
    }
}