﻿namespace WwsTunnel.Contracts.Model
{
    public enum ReturnCode
    {
        Ok,
        OkPartially,
        JBossException,
        ValidationException,
        Exception
    }
}
