﻿namespace WwsTunnel.Contracts.Model
{
    public enum PaymentType
    {
        Cash,
        CreditCard,
        SocialCard
    }
}