﻿using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class CancelSalesOrderResult : OperationResult
    {
        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}