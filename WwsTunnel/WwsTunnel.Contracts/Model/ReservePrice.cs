﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class ReservePrice
    {
        [DataMember]
        public decimal VAT { get; set; }

        [DataMember]
        public decimal NetPrice { get; set; }

        [DataMember]
        public decimal VatPrice { get; set; }

        [DataMember]
        public decimal GrossPrice { get; set; }
    }
}