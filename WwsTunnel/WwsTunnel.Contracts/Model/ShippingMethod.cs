﻿namespace WwsTunnel.Contracts.Model
{
    public enum ShippingMethod
    {
        Pickup,
        Delivery
    }
}