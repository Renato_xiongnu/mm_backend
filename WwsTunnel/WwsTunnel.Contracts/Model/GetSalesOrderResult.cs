﻿using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class GetSalesOrderResult : OperationResult
    {
        [DataMember]
        public OrderHeader OrderHeader { get; set; }

        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        [PrimaryKey]
        public string RequestId { get; set; }
    }
}