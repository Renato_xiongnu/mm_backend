﻿using System;
using System.Runtime.Serialization;
using MMS.Cloud.Shared.Attributes;

namespace WwsTunnel.Contracts.Model
{
    [DataContract(Namespace = "http://schemas.datacontract.org/2004/07/Orders.Backend.Services.WWS")]
    public class CreateSalesOrderResult : OperationResult
    {
        [DataMember]
        public OrderHeader OrderHeader { get; set; }
        
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        [PrimaryKey]
        public string OrderId { get; set; }

        [DataMember]
        public DateTime? CreateTime { get; set; }
    }
}