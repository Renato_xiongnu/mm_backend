﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class CustomerInfo
    {
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        /// <summary>
        /// Location - City
        /// </summary>
        /// 
        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Mobile { get; set; }

        //public CustomerType CustomerType { get; set; }

    }
}
