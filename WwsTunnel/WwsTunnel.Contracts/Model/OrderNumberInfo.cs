﻿using System.Runtime.Serialization;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class OrderNumberInfo
    {
        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }
    }
}