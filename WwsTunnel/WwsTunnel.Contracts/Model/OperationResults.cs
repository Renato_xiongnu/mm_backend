﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WWS.Services;

namespace WwsTunnel.Contracts.Model
{
    [DataContract]
    public class OrderHeader
    {
        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public DateTime? CreateTime { get; set; }

        [DataMember]
        public ShippingMethod ShippingMethod { get; set; }

        [DataMember]
        public SalesOrderStatus OrderStatus { get; set; }

        [DataMember]
        public PaymentStatus WwsPaymentStatus { get; set; }

        [DataMember]
        public SalesDocStatus WwsSalesDocStatus { get; set; }

        [DataMember]
        public SalesDocumentType WwsSalesDocType { get; set; }

        [DataMember]
        public decimal OriginalSum { get; set; }

        [DataMember]
        public DateTime? LastUpdateTime { get; set; }

        [DataMember]
        public string SalesPersonName { get; set; }

        [DataMember]
        public ReservePrice TotalPrice { get; set; }

        [DataMember]
        public decimal DownpaymentPrice { get; set; }

        [DataMember]
        public ReservePrice DownpaymentPriceFull { get; set; }

        [DataMember]
        public ReservePrice LeftoverPrice { get; set; }

        [DataMember]
        public Delivery Delivery { get; set; }

        [DataMember]
        public Customer DeliveryCustomer { get; set; }

        [DataMember]
        public Customer InvoiceCustomer { get; set; }

        [DataMember]
        public ICollection<OrderLine> Lines { get; set; }

        [DataMember]
        public string DocBarcode { get; set; }

        [DataMember]
        public string DocBarcodeImage { get; set; }

        [DataMember]
        public string RefOrderNumber { get; set; }

        [DataMember]
        public string OutletInfo { get; set; }

        [DataMember]
        public string ProductPickupInfo { get; set; }

        [DataMember]
        public string PrintableInfo { get; set; }
    }

    [DataContract]
    public class OrderLine
    {
        [DataMember]
        public int PositionNumber { get; set; }

        [DataMember]
        public int ArticleNo { get; set; }

        [DataMember]
        public string ProductGroupName { get; set; }

        [DataMember]
        public int ProductGroupNo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int FreeQuantity { get; set; }

        [DataMember]
        public int ReservedQuantity { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public decimal PriceOrig { get; set; }

        [DataMember]
        public int StockNumber { get; set; }

        [DataMember]
        public int DepartmentNumber { get; set; }

        [DataMember]
        public ReservePrice TotalPrice { get; set; }

        [DataMember]
        public int StoreNumber { get; set; }

        [DataMember]
        public ProductType ProductType { get; set; }

        [DataMember]
        public int LineNumber { get; set; }

        [DataMember]
        public int? RefLineNumber { get; set; }

        [DataMember]
        public WarrantyInsurance WarrantyInsurance { get; set; }

        [DataMember]
        public bool HasShippedFromStock { get; set; } 

        [DataMember]
        public ICollection<OrderLine> SubLines { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public string ManufacturerName { get; set; }
    }

    [DataContract] 
    public class WarrantyInsurance
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public int DocumentPositionNumber { get; set; }

        [DataMember]
        public int RelatedArticleNo { get; set; }

        [DataMember]
        public decimal WarrantySum { get; set; }

        [DataMember]
        public string WwsCertificateState { get; set; }

        [DataMember]
        public string WwsExtensionPrint  { get; set; }
    }

}
