﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using MMS.Json;

namespace WwsTunnel.Common.Helpers
{
    public static class ExtensionHelpers
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> iEnumerable)
        {
            return iEnumerable == null || !iEnumerable.Any();
        }

        public static string ToJson(this object data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public static T DeepClone<T>(this T obj)
        {
            var serializer = new DataContractSerializer(typeof (T), null, int.MaxValue, false, true, null);
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, obj);
                ms.Position = 0;
                return (T) serializer.ReadObject(ms);
            }
        }

        public static string GetOrDefault(this Dictionary<string, string> dict, string key)
        {
            if (dict.ContainsKey(key))
            {
                return Convert.ToString(dict[key]);
            }

            return string.Empty;
        }

        public static T GetOrDefault<T>(this Dictionary<string, string> dict, string key) where T : struct
        {
            if (dict.ContainsKey(key) && CanChangeType(dict[key], typeof (T)))
            {
                try
                {
                    var val = (T) Convert.ChangeType(dict[key], typeof (T));
                    return val;
                }
// ReSharper disable EmptyGeneralCatchClause
                catch (Exception)
// ReSharper restore EmptyGeneralCatchClause
                {
                }
            }

            return default(T);
        }

        public static T? GetOrNull<T>(this Dictionary<string, string> dict, string key) where T : struct
        {
            if (dict.ContainsKey(key) && CanChangeType(dict[key], typeof (T)))
            {
                try
                {
                    var val = (T) Convert.ChangeType(dict[key], typeof (T));
                    return val;
                }
                    // ReSharper disable EmptyGeneralCatchClause
                catch (Exception)
                    // ReSharper restore EmptyGeneralCatchClause
                {
                }
            }

            return null;
        }

        public static bool CanChangeType(object value, Type conversionType)
        {
            if (conversionType == null)
            {
                return false;
            }

            if (value == null)
            {
                return false;
            }

            var convertible = value as IConvertible;

            return convertible != null;
        }

        public static T CopyObjectPropertiesTo<T>(this object value) where T : new()
        {
            return (T) value.CopyObjectPropertiesTo(typeof (T), new T());
        }

        public static object CopyObjectPropertiesTo(this object value, Type T, object retInstance)
        {
            Type valueType = value.GetType();
            PropertyInfo[] valueProperties = valueType.GetProperties();
            Type responseType = T;
            object response = retInstance;
            PropertyInfo[] properties = responseType.GetProperties();
            foreach (PropertyInfo respProperty in properties)
            {
                PropertyInfo prop = valueProperties.SingleOrDefault(t => t.Name == respProperty.Name);
                if (prop != null)
                {
                    responseType.GetProperty(respProperty.Name).SetValue(response, prop.GetValue(value,null),null);
                }
            }
            return response;
        }
    }
}