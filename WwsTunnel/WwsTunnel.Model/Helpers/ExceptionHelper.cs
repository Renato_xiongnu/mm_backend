﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace WwsTunnel.Common.Helpers
{
    public static class ExceptionHelper
    {
        public static IEnumerable<string> GetInnerExceptionsText(this Exception error)
        {
            ICollection<string> exceptionList = new LinkedList<string>();
            Exception realerror = error;
            while (realerror.InnerException != null)
            {
                realerror = realerror.InnerException;
                exceptionList.Add(realerror.Message);
            }

            return exceptionList;
        }

        public static string GetExceptionDetails(this Exception exception)
        {
            PropertyInfo[] properties = exception.GetType()
                                                 .GetProperties();
            IEnumerable<string> fields = properties
                .Select(property => new
                    {
                        property.Name,
                        Value = property.GetValue(exception, null)
                    })
                .Select(x => String.Format(
                    "{0} = {1}",
                    x.Name,
                    x.Value != null ? x.Value.ToString() : String.Empty
                                 ));

            return String.Join("\n", fields);
        }


        public static string GetExceptionFullInfoText(this Exception ex)
        {
            return string.Join("\n", new[]
                {
                    "Fields:",
                    ex.GetExceptionDetails(),
                    "Inner exceptions:"
                }
                                         .Union(ex.GetInnerExceptionsText()));
        }
    }
}