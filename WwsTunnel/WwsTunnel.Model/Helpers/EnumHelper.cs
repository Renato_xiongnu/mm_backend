﻿using System;
using System.Reflection;

namespace WwsTunnel.Common.Helpers
{
    public static class EnumHelper
    {
        public static short ToShort(this Enum en)
        {
            return Convert.ToInt16(en);
        }

        public static int ToInt(this Enum en)
        {
            return Convert.ToInt32(en);
        }

        public static T ConvertTo<T>(this Enum en) where T : struct
        {
            try
            {
                string stValue = en.ToString();
                return stValue.ToEnum<T>();
            }
            catch (InvalidCastException)
            {
            }

            int intValue = en.ToInt();

            return intValue.ToEnum<T>();
        }


        public static T ToEnum<T>(this string value) where T : struct
        {
            string cleanString = value.Replace("\"", "");

            T res = default(T);
            if (Enum.TryParse(cleanString, true, out res))
            {
                return res;
            }

            int num;
            if (int.TryParse(cleanString, out num))
            {
                if (Enum.IsDefined(typeof (T), num))
                    return (T) Enum.ToObject(typeof (T), num);
            }

            throw new InvalidCastException(string.Format("Value {0} is not valid enum value of {1}", value,
                                                         typeof (T).Name));
        }

        public static T ToEnum<T>(this string value, T defaultValue) where T : struct
        {
            try
            {
                return value.ToEnum<T>();
            }
            catch
            {
                return defaultValue;
            }
        }


        public static T ToEnum<T>(this short value) where T : struct
        {
            return ((int) value).ToEnum<T>();
        }

        public static T ToEnum<T>(this short value, T defaultValue) where T : struct
        {
            return ((int) value).ToEnum(defaultValue);
        }

        public static T ToEnum<T>(this int value) where T : struct
        {
            if (Enum.IsDefined(typeof (T), value))
                return (T) Enum.ToObject(typeof (T), value);

            throw new InvalidCastException(string.Format("Value {0} is not valid enum value of {1}", value,
                                                         typeof (T).Name));
        }

        public static T ToEnum<T>(this int value, T defaultValue) where T : struct
        {
            try
            {
                return value.ToEnum<T>();
            }
            catch
            {
                return defaultValue;
            }
        }


        public static T GetAttributeOfType<T>(this Enum enumVal) where T : Attribute
        {
            Type type = enumVal.GetType();
            MemberInfo[] memInfo = type.GetMember(enumVal.ToString());
            if (memInfo.Length > 0)
            {
                object[] attributes = memInfo[0].GetCustomAttributes(typeof (T), false);

                if (attributes.Length > 0)
                    return (T) attributes[0];
            }

            return null;
        }
    }
}