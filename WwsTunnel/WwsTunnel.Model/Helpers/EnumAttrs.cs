﻿using System;

namespace WwsTunnel.Common.Helpers
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class EnumFieldDescriptionAttribute : Attribute
    {
        public EnumFieldDescriptionAttribute(string name = "")
        {
            Name = name;
        }

        public string Name { get; set; }
    }


    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class EnumFieldLinkedStringAttribute : Attribute
    {
        public EnumFieldLinkedStringAttribute(string name = "")
        {
            Name = name;
        }

        public string Name { get; set; }
    }


    public static class EnumExtensions
    {
        public static string GetDescription(this Enum enumVal)
        {
            var attr = EnumHelper.GetAttributeOfType<EnumFieldDescriptionAttribute>(enumVal);

            return (attr == null) ? enumVal.ToString() : attr.Name;
        }


        public static string GetLinkedString(this Enum enumVal)
        {
            var attr = EnumHelper.GetAttributeOfType<EnumFieldLinkedStringAttribute>(enumVal);

            return (attr == null) ? enumVal.ToString() : attr.Name;
        }
    }
}