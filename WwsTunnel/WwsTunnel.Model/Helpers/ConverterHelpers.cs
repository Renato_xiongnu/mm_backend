﻿using System;

namespace WwsTunnel.Common.Helpers
{
    public static class ConverterHelpers
    {
        public static int? ParseInt(this string num)
        {
            int result;
            if (Int32.TryParse(num, out result))
            {
                return result;
            }

            return null;
        }

        public static long? ParseLong(this string num)
        {
            long result;
            if (Int64.TryParse(num, out result))
            {
                return result;
            }

            return null;
        }

        public static Decimal? ParseDecimal(this string num)
        {
            decimal result;
            if (Decimal.TryParse(num, out result))
            {
                return result;
            }

            return null;
        }

        public static bool? ParseBoolean(this string boolean)
        {
            bool result;
            if (bool.TryParse(boolean, out result))
            {
                return result;
            }

            return null;
        }

        public static DateTime? ParseDate(this string dateTime)
        {
            DateTime result;
            if (DateTime.TryParse(dateTime, out result))
            {
                return result;
            }

            return null;
        }
    }
}