﻿using System;
using System.Globalization;
using MMS.Json;

namespace WwsTunnel.Common.Helpers
{
    public class DataContractJsonHelper<T>
    {
        public static string SerializeData(T c)
        {
            return JsonConvert.SerializeObject(c, new TimeSpanToJsonConverter());
        }

        public static T DeserializeData(string value)
        {
            return JsonConvert.DeserializeObject<T>(value, new JsonToTimeSpanConverter());
        }
    }

    public class TimeSpanToJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (TimeSpan);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((TimeSpan) value).ToString("G", new CultureInfo("en-US")));
            writer.Flush();
        }
    }

    public class JsonToTimeSpanConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Object);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            if (reader.ValueType == typeof (string))
            {
                TimeSpan t;
                return TimeSpan.TryParse(reader.Value.ToString(), new CultureInfo("en-US"), out t)
                           ? t
                           : reader.Value;
            }
            return reader.Value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}