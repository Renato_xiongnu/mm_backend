﻿using System;
using System.Threading;

namespace WwsTunnel.Proxy
{
    public class Request<TRequest, TResponse>
        where TRequest : class
        where TResponse : class
    {
        private TResponse _response;
        private readonly ManualResetEvent _resetEvent;
        private long _numberOfRequest;

        public long NumberOfRequest
        {
            get
            {
                return Interlocked.Read(ref _numberOfRequest);
            }
        }

        public Request(TRequest body)
        {
            Created = DateTime.Now;
            Body = body;
            _resetEvent = new ManualResetEvent(false);
        }
        public DateTime Created { get; set; }

        public TRequest Body { get; private set; }

        public void AddRequest()
        {
            Interlocked.Increment(ref _numberOfRequest);
        }

        public bool WaitResponse(out TResponse response)
        {
            try
            {
                response = _response;
                if(response != null)
                {
                    return true;
                }
                if(!_resetEvent.WaitOne(Config.ResponseWaitTimeout))
                {
                    return false;
                }
                response = _response;
                return true;
            }
            finally
            {
                Interlocked.Decrement(ref _numberOfRequest);
            }
        }

        public void SetResponse(TResponse response)
        {
            _response = response;
            _resetEvent.Set();
        }

        public bool HasResponse
        {
            get { return _response != null; }
        }
    }
}