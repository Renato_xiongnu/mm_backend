using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using NLog;

namespace WwsTunnel.Proxy
{
    public class FileLastSequanceProvider : ILastSequanceProvider
    {
        private long _lastSequance = 0;

        private readonly FileInfo _fileInfo;

        private readonly ReaderWriterLockSlim _lockObject = new ReaderWriterLockSlim();

        private readonly Logger _logger = LogManager.GetLogger("FileLastSequanceProvider");

        public FileLastSequanceProvider(string filePath)
        {
            _fileInfo = new FileInfo(filePath);
            if(_fileInfo.Directory != null && !_fileInfo.Directory.Exists)
            {
                _fileInfo.Directory.Create();
            }
            if(!_fileInfo.Exists)
            {
                _fileInfo.Create();
            }
            else
            {
                _lastSequance = ReadFromFile();
            }
        }

        public long Get()
        {
            _lockObject.EnterReadLock();
            try
            {                
                return _lastSequance;
            }
            finally
            {
                _lockObject.ExitReadLock();
            }
        }

        public void Set(long value)
        {
            _lockObject.EnterWriteLock();
            try
            {
                if(_lastSequance == value)
                {
                    return;
                }                
                using(var sw = _fileInfo.CreateText())
                {
                    sw.Write(value);
                }
                _lastSequance = value;
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "Save lastSeq {0} error. ({1})", value, _fileInfo.FullName);
            }
            finally
            {
                _lockObject.ExitWriteLock();
            }
        }

        private long ReadFromFile()
        {
            _lockObject.EnterWriteLock();
            try
            {
                using(var sr = _fileInfo.OpenText())
                {
                    var str = sr.ReadToEnd();
                    var match = Regex.Match(str, @"(?<number>[1-9]\d*)");
                    if(match.Success)
                    {
                        return long.Parse(match.Groups["number"].Value);
                    }
                }
                return 0;
            }
            finally
            {
                _lockObject.ExitWriteLock();
            }
        }
    }
}