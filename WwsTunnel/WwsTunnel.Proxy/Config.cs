﻿using System;
using System.Configuration;

namespace WwsTunnel.Proxy
{
    public static class Config
    {
        static Config()
        {
            ResponcesFetchTimeout = ToTimeSpan(ConfigurationManager.AppSettings["ResponcesFetchTimeout"]);
            ResponsesGbLaunchTimeTimeout = ToTimeSpan(ConfigurationManager.AppSettings["ResponsesGbLaunchTimeTimeout"]);
            ResponsesLifeTime = ToTimeSpan(ConfigurationManager.AppSettings["ResponsesLifeTime"]);
            ResponceFetchLimit = ToInt(ConfigurationManager.AppSettings["ResponceFetchLimit"]);
            ResponseWaitTimeout = ToTimeSpan(ConfigurationManager.AppSettings["ResponseWaitTimeout"]);
            RequestLifeTime = ToTimeSpan(ConfigurationManager.AppSettings["RequestLifeTime"]);
        }

        public static readonly TimeSpan ResponcesFetchTimeout;
        public static readonly TimeSpan ResponsesGbLaunchTimeTimeout;
        public static readonly TimeSpan ResponsesLifeTime;
        public static readonly TimeSpan ResponseWaitTimeout;
        public static readonly TimeSpan RequestLifeTime;
         
        public static readonly int ResponceFetchLimit;

        private static TimeSpan ToTimeSpan(string appSetting)
        {
            return TimeSpan.Parse(appSetting);
        }

        private static int ToInt(string appSetting)
        {
            return int.Parse(appSetting);
        }

        private static long Tolong(string appSetting)
        {
            return long.Parse(appSetting);
        }
    }
}
