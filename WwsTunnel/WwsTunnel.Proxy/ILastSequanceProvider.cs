﻿namespace WwsTunnel.Proxy
{
    public interface ILastSequanceProvider
    {
        long Get();
        void Set(long value);
    }
}