﻿using System;
using MMS.Cloud.Interfaces.Shared;
using Orders.Backend.Services.WWS;
using NLog;

namespace WwsTunnel.Proxy
{
    public class GetSalesOrderRequestHandler : RequestHandler<GetSalesOrderRequest, GetSalesOrderResult>
    {
        public GetSalesOrderRequestHandler(ILastSequanceProvider lastSequanceProvider, Func<IEntityStorage> entityStorageFactory, Logger logger)
            : base(lastSequanceProvider, entityStorageFactory, logger)
        {
        }

        protected override string GetIdentityKey(GetSalesOrderRequest request)
        {            
            return request.RequestId;
        }

        protected override string GetIdentityKey(GetSalesOrderResult response)
        {
            return response.RequestId;
        }
    }
}