﻿using System;
using NLog;
using MMS.Cloud.Interfaces.Shared;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy
{
    public class CancelSalesOrderRequestHandler : RequestHandler<CancelSalesOrderRequest, CancelSalesOrderResult>
    {
        public CancelSalesOrderRequestHandler(ILastSequanceProvider lastSequanceProvider,
            Func<IEntityStorage> entityStorageFactory,
            Logger logger)
            : base(lastSequanceProvider, entityStorageFactory, logger)
        {
        }

        protected override string GetIdentityKey(CancelSalesOrderRequest request)
        {
            return request.RequestId;
        }

        protected override string GetIdentityKey(CancelSalesOrderResult response)
        {
            return response.RequestId;
        }
    }
}