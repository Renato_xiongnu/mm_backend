﻿using System;
using NLog;
using MMS.Cloud.Interfaces.Shared;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy
{
    public class ChangeDeliveryDateRequestHandler : RequestHandler<ChangeDeliveryDateRequest, ChangeDeliveryDateResult>
    {
        public ChangeDeliveryDateRequestHandler(ILastSequanceProvider lastSequanceProvider,
            Func<IEntityStorage> entityStorageFactory,
            Logger logger)
            : base(lastSequanceProvider, entityStorageFactory, logger)
        {
        }

        protected override string GetIdentityKey(ChangeDeliveryDateRequest request)
        {
            return request.RequestId;
        }

        protected override string GetIdentityKey(ChangeDeliveryDateResult response)
        {
            return response.RequestId;
        }
    }
}