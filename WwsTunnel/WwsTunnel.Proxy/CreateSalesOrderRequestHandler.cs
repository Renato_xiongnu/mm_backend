﻿using System;
using MMS.Cloud.Interfaces.Shared;
using Orders.Backend.Services.WWS;
using NLog;

namespace WwsTunnel.Proxy
{
    public class CreateSalesOrderRequestHandler : RequestHandler<CreateSalesOrderRequest, CreateSalesOrderResult>        
    {
        public CreateSalesOrderRequestHandler(ILastSequanceProvider lastSequanceProvider, Func<IEntityStorage> entityStorageFactory, Logger logger)
            : base(lastSequanceProvider, entityStorageFactory, logger)
        {
        }

        protected override string GetIdentityKey(CreateSalesOrderRequest request)
        {
            return string.Join("_", request.SapCode, request.OnlineOrderId);
        }

        protected override string GetIdentityKey(CreateSalesOrderResult response)
        {
            return string.Join("_", response.SapCode, response.OrderId);
        }
    }
}