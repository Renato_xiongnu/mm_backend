﻿using System;
using System.Configuration;
using NLog;
using WwsTunnel.TimeStampStorage.Db;

namespace WwsTunnel.Proxy
{
    public class DbLastSequanceProvider: ILastSequanceProvider
    { 
        private readonly string _storagName = String.Empty;
        protected readonly TimeStampStorageRepository TimeStampRepository; 
      

        private readonly Logger _logger = LogManager.GetLogger("DbLastSequanceProvider");

        public DbLastSequanceProvider(string storagName)
        {
            _storagName = storagName;
            TimeStampRepository = new TimeStampStorageRepository(ConfigurationManager.ConnectionStrings["TimeStampStorageDataModelContainer"].ConnectionString);
        }

       
        #region ILastSequanceProvider Members

        public long Get()
        {
            long lastSequance = 0;
            try
            {
               lastSequance = TimeStampRepository.GetLastTimeStamp(_storagName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lastSequance;
        }

        public void Set(long value)
        {
            try
            {
                TimeStampRepository.CreateOrUpdateTimeStamp(_storagName, value);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        #endregion
    }
}