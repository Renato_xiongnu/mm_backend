﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using NLog;
using MMS.Cloud.Interfaces.Shared;

namespace WwsTunnel.Proxy
{
    public class ResponsePooling<TResponse>
        where TResponse : class, new()
    {
        private readonly ILastSequanceProvider _lastSequanceProvider;
        private readonly Func<IEntityStorage> _entityStorageFactory;
        private readonly Logger _logger;
        private readonly Timer _timer;

        public ResponsePooling(ILastSequanceProvider lastSequanceProvider, Func<IEntityStorage> entityStorageFactory, Logger logger)
        {
            _lastSequanceProvider = lastSequanceProvider;
            _entityStorageFactory = entityStorageFactory;
            _logger = logger;
            _timer = new Timer
                {
                    Interval = Config.ResponcesFetchTimeout.TotalMilliseconds,
                    AutoReset = false
                };
            _timer.Elapsed += TimerElapsed;
            _timer.Start();
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var lastSequence = _lastSequanceProvider.Get();
                var entityStorage = _entityStorageFactory();
                var responses = entityStorage
                    .GetChanges<TResponse>(lastSequence, Config.ResponceFetchLimit, out lastSequence);
                if(responses != null && responses.Any())
                {
                    OnNewResponses(responses);
                }
                _lastSequanceProvider.Set(lastSequence);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                _timer.Start();
            }
        }

        public event EventHandler<NewResponsesArgs<TResponse>> NewResponses;

        protected virtual void OnNewResponses(IEnumerable<TResponse> responses)
        {
            var handler = NewResponses;
            if(handler != null)
            {
                handler(this, new NewResponsesArgs<TResponse>(responses));
            }
        }
    }
}