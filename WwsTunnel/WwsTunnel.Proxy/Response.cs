﻿using System;

namespace WwsTunnel.Proxy
{
    public class Response<T>
        where T : class
    {
        public Response(T body)
        {
            Body = body;
            Created = DateTime.Now;
        }

        public DateTime Created { get; set; }

        public T Body { get; set; }
        
    }
}