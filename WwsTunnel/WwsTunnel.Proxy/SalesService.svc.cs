﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Timers;
using System.Web.Hosting;
using MMS.Cloud.Interfaces.Shared;
using MMS.Monitoring.Statistics;
using MMS.Storage.CouchDB.DreamSeat;
using Orders.Backend.Services.WWS;
using WwsTunnel.Common.Helpers;
using NLog;

namespace WwsTunnel.Proxy
{
    public class SalesService : ISalesService
    {
        private static readonly CreateSalesOrderRequestHandler CreateSalesOrderRequestHandler;
        private static readonly CancelSalesOrderRequestHandler CancelSalesOrderRequestHandler;
        private static readonly GetSalesOrderRequestHandler GetSalesOrderRequestHandler;
        private static readonly ChangeDeliveryDateRequestHandler ChangeDeliveryDateRequestHandler;

        private static readonly Logger Logger;

        private static CouchDbStorage _storage;

        static SalesService()
        {
            MappingExtensions.ConfigureMapping();
            //XmlConfigurator.Configure();            
            Logger = LogManager.GetLogger("SalesService");
            Logger.Trace("Started");
            _storage = new CouchDbStorage();
            try
            {
                CreateSalesOrderRequestHandler = new CreateSalesOrderRequestHandler(
                    new DbLastSequanceProvider("CreateSalesOrder"), 
                    GetCouchDbStorage,
                    LogManager.GetLogger("CreateSalesOrderRequestHandler"));
                CancelSalesOrderRequestHandler = new CancelSalesOrderRequestHandler(
                    new DbLastSequanceProvider("CancelSalesOrder"),
                    GetCouchDbStorage,
                    LogManager.GetLogger("CancelSalesOrderRequestHandler"));
                GetSalesOrderRequestHandler = new GetSalesOrderRequestHandler(
                    new DbLastSequanceProvider("GetSalesOrder"),
                    GetCouchDbStorage,
                    LogManager.GetLogger("GetSalesOrderRequestHandler"));
                ChangeDeliveryDateRequestHandler = new ChangeDeliveryDateRequestHandler(
                    new DbLastSequanceProvider("ChangeDeliveryDate"),
                    GetCouchDbStorage,
                    LogManager.GetLogger("ChangeDeliveryDateRequestHandler"));

                var timer = new Timer();
                timer.Elapsed += TimerSendStatistics;
                int interval;
                if(!int.TryParse(ConfigurationManager.AppSettings["StatisticsInterval"],out interval))
                {
                    interval = 60;
                }
                timer.Interval = TimeSpan.FromSeconds(interval).TotalMilliseconds;
                timer.AutoReset = true;
                timer.Start();

                #region fileTimeStamp

                //var directory = HostingEnvironment.ApplicationPhysicalPath;
                //Logger.Info(string.Format("Service directory path: {0}", directory));

                //CreateSalesOrderRequestHandler = new CreateSalesOrderRequestHandler(
                //    new FileLastSequanceProvider(Path.Combine(directory, @"Config\CreateSalesOrder.txt")),
                //    GetCouchDbStorage,
                //    LogManager.GetLogger("CreateSalesOrderRequestHandler"));
                //CancelSalesOrderRequestHandler = new CancelSalesOrderRequestHandler(
                //    new FileLastSequanceProvider(Path.Combine(directory, @"Config\CancelSalesOrder.txt")),
                //    GetCouchDbStorage,
                //    LogManager.GetLogger("CancelSalesOrderRequestHandler"));
                //GetSalesOrderRequestHandler = new GetSalesOrderRequestHandler(
                //    new FileLastSequanceProvider(Path.Combine(directory, @"Config\GetSalesOrder.txt")),
                //    GetCouchDbStorage,
                //    LogManager.GetLogger("GetSalesOrderRequestHandler"));

                #endregion fileTimeStamp
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "Service initialize error");
                throw;
            }
        }

        private static void TimerSendStatistics(object sender, ElapsedEventArgs e)
        {
            try
            {
                var metrics = new List<StatsMetric>
                {
                    StatsMetric.Gauge("create_reserve_requests", CreateSalesOrderRequestHandler.GetCurrentActiveRequests()),
                    StatsMetric.Gauge("cancel_reserve_requests", CancelSalesOrderRequestHandler.GetCurrentActiveRequests()),
                    StatsMetric.Gauge("get_reserve_requests", GetSalesOrderRequestHandler.GetCurrentActiveRequests()),
                    StatsMetric.Gauge("change_delivery_date_requests", ChangeDeliveryDateRequestHandler.GetCurrentActiveRequests()),
                };
                
                StatsEngine.Send(metrics);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error during sending statistics");
            }
        }

        private static IEntityStorage GetCouchDbStorage()
        {
            return _storage;
        }

        public CreateSalesOrderResult CreateSalesOrder(OrderInfo order)
        {
            var request = order.ConvertTo<CreateSalesOrderRequest>();            
            
            return CreateSalesOrderRequestHandler
                .Process(request)
                .ConvertTo<CreateSalesOrderResult>();
        }

        public CancelSalesOrderResult CancelSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var request = new CancelSalesOrderRequest
                {
                    OrderNumber = orderNumberInfo.OrderNumber,
                    SapCode = orderNumberInfo.SapCode,
                    RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber)
                };            
            Logger.Info(request.RequestId);

            return CancelSalesOrderRequestHandler
                .Process(request)
                .ConvertTo<CancelSalesOrderResult>();
        }

        public GetSalesOrderResult GetSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var request = new GetSalesOrderRequest
                {
                    OrderNumber = orderNumberInfo.OrderNumber,
                    SapCode = orderNumberInfo.SapCode,
                    RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber)
                };            
            Logger.Info(request.RequestId);

            return GetSalesOrderRequestHandler
                .Process(request)
                .ConvertTo<GetSalesOrderResult>();
        }

        public ChangeDeliveryDateResult ChangeDeliveryDate(OrderNumberInfo orderNumberInfo, DateTime deliveryDate)
        {
            var request = new ChangeDeliveryDateRequest
            {
                OrderNumber = orderNumberInfo.OrderNumber,
                SapCode = orderNumberInfo.SapCode,
                RequestId = string.Format("{0}_{1}", orderNumberInfo.SapCode, orderNumberInfo.OrderNumber),
                DeliveryDate = deliveryDate,
            };
            Logger.Info("[{0}], Process ChangeDeliveryDate Request - {1}", GetType().FullName, request.RequestId);
            Logger.Debug("ChangeDeliveryDateRequest = {0}", request.ToJson());
            var result = ChangeDeliveryDateRequestHandler.Process(request);
            Logger.Debug("ChangeDeliveryDateResult = {0}", result.ToJson());
            return result;
        }
    }
}