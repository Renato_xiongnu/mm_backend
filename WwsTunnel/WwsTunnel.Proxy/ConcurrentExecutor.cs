﻿using System;
using System.Linq;

namespace WwsTunnel.Proxy
{
    public class ConcurrentExecutor<TKey>
    {
        private readonly int _concurrencyLevel;
        private readonly object[] _lockObjects;

        public ConcurrentExecutor(int concurrencyLevel = 100)
        {
            _concurrencyLevel = concurrencyLevel;
            if(concurrencyLevel <= 0)
            {
                throw new ArgumentOutOfRangeException("concurrencyLevel", concurrencyLevel, "Must be greater than zero");
            }
            _lockObjects = Enumerable
                .Range(0, concurrencyLevel)
                .Select(i => new object())
                .ToArray();
        }

        public TResult Execute<TResult>(TKey key, Func<TResult> func)
        {
            lock(GetLockObject(key))
            {
                return func();
            }
        }

        public void Execute(TKey key, Action action)
        {
            lock(GetLockObject(key))
            {
                action();
            }
        }

        private object GetLockObject(TKey key)
        {
            var hash = Math.Abs(key.GetHashCode());
            return _lockObjects[hash%_concurrencyLevel];
        }
    }
}