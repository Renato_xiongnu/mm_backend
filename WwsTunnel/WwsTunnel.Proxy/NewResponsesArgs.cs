﻿using System;
using System.Collections.Generic;

namespace WwsTunnel.Proxy
{
    public class NewResponsesArgs<TResponse> : EventArgs
    {
        public NewResponsesArgs(IEnumerable<TResponse> items)
        {
            Items = items;
        }

        public IEnumerable<TResponse> Items { get; private set; }
    }
}