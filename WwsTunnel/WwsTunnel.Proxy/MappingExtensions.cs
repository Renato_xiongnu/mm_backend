﻿using System.Threading;
using AutoMapper;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy
{
    public static class MappingExtensions
    {        
        static MappingExtensions()
        {
            ConfigureMapping();
        }

        public static void ConfigureMapping()
        {            
            Mapper.CreateMap<OrderInfo, CreateSalesOrderRequest>();
            Mapper.CreateMap<CustomerInfo, CustomerInfo>();
            Mapper.CreateMap<OrderLine, OrderLine>();
            Mapper.CreateMap<ReservePrice, ReservePrice>();
            Mapper.CreateMap<WarrantyInsurance, WarrantyInsurance>();
            Mapper.CreateMap<OrderHeader, OrderHeader>();
            Mapper.CreateMap<Delivery, Delivery>();
            Mapper.CreateMap<Customer, Customer>();
            Mapper.CreateMap<OperationCode, OperationCode>();
            

            Mapper.CreateMap<CreateSalesOrderResult, Orders.Backend.Services.WWS.CreateSalesOrderResult>();
            Mapper.CreateMap<CustomerInfo, CustomerInfo>();
            Mapper.CreateMap<OrderLine, OrderLine>();
            Mapper.CreateMap<ReservePrice, ReservePrice>();
            Mapper.CreateMap<WarrantyInsurance, WarrantyInsurance>();
            Mapper.CreateMap<OrderHeader, OrderHeader>();
            Mapper.CreateMap<Delivery, Delivery>();
            Mapper.CreateMap<Customer, Customer>();
            Mapper.CreateMap<OperationCode, OperationCode>();

            Mapper.CreateMap<CancelSalesOrderResult, Orders.Backend.Services.WWS.CancelSalesOrderResult>();
            Mapper.CreateMap<GetSalesOrderResult, Orders.Backend.Services.WWS.GetSalesOrderResult>();

            Mapper.CreateMap<OrderNumberInfo, CancelSalesOrderRequest>().ForMember(m=>m.RequestId,opt=>opt.Ignore());
            Mapper.CreateMap<OrderNumberInfo, GetSalesOrderRequest>().ForMember(m=>m.RequestId,opt=>opt.Ignore());

            Mapper.AssertConfigurationIsValid();
        }

        public static T ConvertTo<T>(this object obj)
        {            
            return Mapper.Map<T>(obj);
        }
    }
}