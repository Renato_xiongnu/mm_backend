﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Timers;
using MMS.Cloud.Interfaces.Shared;
using NLog;
using MMS.Json;
using Orders.Backend.Services.WWS;

namespace WwsTunnel.Proxy
{
    public abstract class RequestHandler<TRequest, TResponse>
        where TRequest : class, new()
        where TResponse : OperationResult, new()
    {
        private readonly Func<IEntityStorage> _entityStorageFactory;
        private readonly Logger _logger;
        private readonly ConcurrentDictionary<string, Request<TRequest, TResponse>> _requests;
        private readonly ConcurrentDictionary<string, Response<TResponse>> _responses;
        private readonly Timer _gBResponses;
        private readonly ResponsePooling<TResponse> _responsePooling;
        // ReSharper disable StaticFieldInGenericType
        private static readonly ConcurrentExecutor<string> Executor = new ConcurrentExecutor<string>(1000);
        // ReSharper restore StaticFieldInGenericType

        public RequestHandler(
            ILastSequanceProvider lastSequanceProvider,
            Func<IEntityStorage> entityStorageFactory,
            Logger logger)
        {
            _entityStorageFactory = entityStorageFactory;
            _logger = logger;
            _requests = new ConcurrentDictionary<string, Request<TRequest, TResponse>>();
            _responses = new ConcurrentDictionary<string, Response<TResponse>>();
            _responsePooling = new ResponsePooling<TResponse>(lastSequanceProvider, entityStorageFactory, logger);
            _responsePooling.NewResponses += ResponsePoolingOnNewResponses;
            _gBResponses = new Timer
                {
                    Interval = Config.ResponsesGbLaunchTimeTimeout.TotalMilliseconds,
                    AutoReset = false
                };
            _gBResponses.Elapsed += GbResponsesElapsed;
            _gBResponses.Start();
        }

        private void GbResponsesElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var now = DateTime.Now;
                foreach(var kv in _responses.ToArray())
                {
                    var key = kv.Key;
                    if(now - kv.Value.Created <= Config.ResponsesLifeTime)
                    {
                        continue;
                    }
                    _logger.Info("Response collect {0}", key);
                    Executor.Execute(key, () =>
                        {
                            Response<TResponse> response;
                            _responses.TryRemove(key, out response);
                        });
                }

                // Cleanup Request Dictionary
                _requests
                    .Where(kv => now - kv.Value.Created > Config.RequestLifeTime)
                    .Select(kv => kv.Key)
                    .ToList()
                    .ForEach(key =>
                {
                    _logger.Info("Cleanup Request {0}, _requests.Count = {1}", key, _requests.Count);
                    Executor.Execute(key, () =>
                    {
                        Request<TRequest, TResponse> request;
                        _requests.TryRemove(key, out request);
                    });
                });
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                _gBResponses.Start();
            }
        }

        private void ResponsePoolingOnNewResponses(object sender, NewResponsesArgs<TResponse> e)
        {
            foreach(var response in e.Items)
            {
                var resp = response;
                var key = GetIdentityKey(resp);
                Executor.Execute(key, () =>
                    {
                        Request<TRequest, TResponse> request;
                        if(_requests.TryGetValue(key, out request))
                        {
                            _logger.Info("Set response to request {0}", key);
                            request.SetResponse(resp);
                            Response<TResponse> innerResponse;
                            _responses.TryRemove(key, out innerResponse);
                        }
                        else
                        {
                            _logger.Info("Save response to responses {0}", key);
                            _responses.AddOrUpdate(key, new Response<TResponse>(resp), (s, r) =>
                                {
                                    r.Body = resp;
                                    r.Created = DateTime.Now;
                                    return r;
                                });
                        }
                    });
            }
        }

        public int GetCurrentActiveRequests()
        {
            return _requests.Count(el => !el.Value.HasResponse);
        }

        public TResponse Process(TRequest request)
        {
            var processed = false;
            var key = GetIdentityKey(request);
            try
            {
                _logger.Info("Start process request {0}", key);
                var innerRequest = Executor.Execute(key, () => GetOrAddRequest(request, key));
                try
                {
                    TResponse response;
                    if(!innerRequest.WaitResponse(out response))
                    {
                        throw new TimeoutException();
                    }
                    processed = true;
                    if(response.OperationCode.MessageText == Constants.ERROR_DURING_CREATION)
                    {
                        var message = string.Format("Process request error {0} : {1}", key, response.OperationCode.MessageText);
                        _logger.Info(message);
                        throw new ApplicationException(response.OperationCode.MessageText);
                    }
                    _logger.Info("Finish process request {0}", key);
                    return response;
                }
                finally
                {
                    if(processed)
                    {
                        Executor.Execute(key, () =>
                        {
                            if(innerRequest.NumberOfRequest <= 0)
                            {
                                _requests.TryRemove(key, out innerRequest);
                                _logger.Info("Request removed {0}", key);
                            }
                        });
                    }

                }
            }
            catch(TimeoutException ex)
            {
                _logger.Info(ex, "Process request timeout {0}", key);
                throw;
            }
            catch(Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(request));
                _logger.Error(ex, "Process request error {0}", key);
                throw;
            }
        }

        private Request<TRequest, TResponse> GetOrAddRequest(TRequest request, string key)
        {
            Request<TRequest, TResponse> innerRequest;
            Response<TResponse> response;
            _responses.TryRemove(key, out response);
            if(!_requests.TryGetValue(key, out innerRequest))
            {
                innerRequest = new Request<TRequest, TResponse>(request);
                if(response != null)
                {
                    innerRequest.SetResponse(response.Body);
                }
                else
                {
                    var entityStorage = _entityStorageFactory();
                    entityStorage.SaveEntity(innerRequest.Body);
                    _requests.TryAdd(key, innerRequest);
                    _logger.Info("Save request to store {0}", key);
                }
            }
            innerRequest.AddRequest();
            return innerRequest;
        }

        protected abstract string GetIdentityKey(TRequest request);

        protected abstract string GetIdentityKey(TResponse response);
    }
}