﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeBehind="Default.aspx.cs" Inherits="Orders.Backend.Clients.OrderMonitoring._Default" Culture="ru-RU" UICulture="ru-RU" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Data.Linq" Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>


<asp:Content ID="Content0" ContentPlaceHolderID="HeaderContent" runat="server">
    <asp:LinkButton runat="server" OnClick="btnExport_Click">экспорт</asp:LinkButton>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid">
    </dx:ASPxGridViewExporter>
</asp:Content>


<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">    
    <script type="text/javascript">
        function OnInit(s, e) {
            AdjustSize();
            s.OnColumnFilterInputChanged = onColumnFilterInputChanged;
        }
        function onColumnFilterInputChanged(e) {
        }
        function OnEndCallback(s, e) {
            AdjustSize();
        }
        function OnControlsInitialized(s, e) {
            ASPxClientUtils.AttachEventToElement(window, "resize", function (evt) {
                AdjustSize();
            });
        }
        function AdjustSize() {
            var height = Math.max(0, document.documentElement.clientHeight);
            grid.SetHeight(height - 100);
        }
    </script>

    <script type="text/javascript">
        function ShowConfirmWindow() {

            function onGetRowValues(value) {
                var orderId = value;
                $('#CancelOrderId').text(orderId);
                confirmRemovePopup.Show();
            };
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
        }

        function ShowAssignWindow() {

            function onGetRowValues(value) {
                var orderId = value;
                $('#AssignOrderId').text(orderId);
                confirmAssignPopup.Show();
            };
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
        }

        function ShowUpdateReserveWindow() {
            function onGetRowValues(value) {
                var orderId = value;
                $('#UpdateReserveOrderId').text(orderId);
                updateReservePopup.Show();
            };
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);

        }

        function ShowUpdateCommentWindow() {
            function onGetRowValues(value) {
                var orderId = value;
                $('#UpdateCommentOrderId').text(orderId);
                updateCommentPopup.Show();
            };
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);

        }
    </script>

    <asp:ObjectDataSource ID="odsPTypeStatuses" runat="server" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade"
        SelectMethod="GetPTypeStatuses" />

    <asp:ObjectDataSource ID="odsOrderStatuses" runat="server" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade"
        SelectMethod="GetOrderStatuses" />

    <asp:ObjectDataSource ID="odsChangedType" runat="server" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade"
        SelectMethod="GetChangedTypes" />

    <asp:ObjectDataSource ID="odsDeliveryType" runat="server" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade"
        SelectMethod="GetDeliveryTypes" />

    <asp:ObjectDataSource ID="odsReserveStatus" runat="server" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade"
        SelectMethod="GetReserveStatuses" />

    <dx:ASPxPopupControl ID="confirmRemovePopup" runat="server" ClientInstanceName="confirmRemovePopup"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        HeaderText="Отмена заказа" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False">
        <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup'); /*if(tbReson){tbReson.Focus();*/} }" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                    <td class="pcmCellCaption" valign="top">
                                        <label>Номер заказа:</label>                                        
                                    </td>
                                    <td class="pcmCellText">
                                        <strong id="CancelOrderId"></strong>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>                                
                                <tr>
                                    <%--<td rowspan="2">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>        --%>                            
                                    <td class="pcmCellCaption" valign="top">
                                        <dx:ASPxLabel runat="server" Text="Причина:" AssociatedControlID="tbReson">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td class="pcmCellText">
                                        <asp:DropDownList runat="server" ID="ddReason">
                                            <asp:ListItem>Витрина</asp:ListItem>
                                            <asp:ListItem>Другое</asp:ListItem>
                                            <asp:ListItem>Купил в другом магазине (не ММ)</asp:ListItem>
                                            <asp:ListItem>Не устроили условия доставки</asp:ListItem>
                                            <asp:ListItem>Неверная цена на сайте</asp:ListItem>
                                            <asp:ListItem>Невозможно связаться с покупателем</asp:ListItem>
                                            <asp:ListItem>Недействительный/ тестовый заказ/ заказ переоформлен</asp:ListItem>
                                            <asp:ListItem>Нет в наличии</asp:ListItem>
                                            <asp:ListItem>Ошибка ВВС/ТТ</asp:ListItem>
                                            <asp:ListItem>Передумал делать покупку</asp:ListItem>
                                            <asp:ListItem>Перекуп/ОПТ/Безнал</asp:ListItem>
                                            <asp:ListItem>Товар после ремонта/ сломан/ ожидает возврата поставщику</asp:ListItem>
                                            <asp:ListItem>Уже купил в ММ</asp:ListItem>
                                            <asp:ListItem>Хотел первоначальные условия доставки/самовывоза</asp:ListItem>
                                            <asp:ListItem>ЯМ: Изменился состав заказа</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="pcmButton">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="right">
                                                        <%--<dx:ASPxButton ID="btOK" runat="server" Text="OK" Width="80px" ValidationGroup="entryGroup" CausesValidation="True"
                                                            OnClick="CancelClick">
                                                        </dx:ASPxButton>--%>
                                                        <dx:ASPxButton ID="btOK" runat="server" Text="OK" Width="80px" AutoPostBack="False" ValidationGroup="entryGroup" CausesValidation="True">
                                                            <ClientSideEvents Click="function(s,e){ OnCancelClick(s,e); return false;}" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <div style="width: 8px;">
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxButton ID="btCancel" runat="server" Text="Отмена" Width="80px" AutoPostBack="False">
                                                            <ClientSideEvents Click="function(s, e) { confirmRemovePopup.Hide(); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    
    <dx:ASPxPopupControl ID="confirmAssignPopup" runat="server" ClientInstanceName="confirmAssignPopup"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        HeaderText="Отмена заказа" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False">
        <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup'); }" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <dx:ASPxPanel ID="ASPxPanel2" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent3" runat="server">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                    <td class="pcmCellCaption" valign="top">
                                        <label>Номер заказа:</label>                                        
                                    </td>
                                    <td class="pcmCellText">
                                        <strong id="AssignOrderId"></strong>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td colspan="2">
                                        <div class="pcmButton">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="right">
                                                        <%--<dx:ASPxButton ID="btOK" runat="server" Text="OK" Width="80px" ValidationGroup="entryGroup" CausesValidation="True"
                                                            OnClick="CancelClick">
                                                        </dx:ASPxButton>--%>
                                                        <dx:ASPxButton ID="ASPxButton3" runat="server" Text="OK" Width="80px" AutoPostBack="False" ValidationGroup="entryGroup" CausesValidation="True">
                                                            <ClientSideEvents Click="function(s,e){ OnAssignToMeClick(s,e); return false;}" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <div style="width: 8px;">
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxButton ID="ASPxButton4" runat="server" Text="Отмена" Width="80px" AutoPostBack="False">
                                                            <ClientSideEvents Click="function(s, e) { confirmAssignPopup.Hide(); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="updateReservePopup" runat="server" ClientInstanceName="updateReservePopup"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        HeaderText="Изменение резерва" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False">
        <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup1'); tbNewWWSNum.Focus(); }" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent2" runat="server">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                    <td class="pcmCellCaption" valign="top">
                                        <label>Номер заказа:</label>                                        
                                    </td>
                                    <td class="pcmCellText">
                                        <strong id="UpdateReserveOrderId"></strong>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="pcmCellCaption" valign="top">
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Номер:" AssociatedControlID="tbNewWWSNum">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td class="pcmCellText">
                                        <dx:ASPxTextBox ID="tbNewWWSNum" runat="server" Width="250px" ClientInstanceName="tbNewWWSNum">
                                            <ValidationSettings EnableCustomValidation="True" ValidationGroup="entryGroup1" SetFocusOnError="True"
                                                ErrorDisplayMode="Text" ErrorTextPosition="Bottom" CausesValidation="True">
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="True" />
                                                <RegularExpression ErrorText="Номер должен быть числом" ValidationExpression="\d+" />
                                                <ErrorFrameStyle Font-Size="10px">
                                                    <ErrorTextPaddings PaddingLeft="0px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="pcmButton">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="right">
                                                        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="OK" Width="80px" AutoPostBack="False" ValidationGroup="entryGroup1" CausesValidation="True">
                                                            <ClientSideEvents Click="function(s,e){ OnUpdateReserveClick(s,e); return false;}" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <div style="width: 8px;">
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Отмена" Width="80px" AutoPostBack="False">
                                                            <ClientSideEvents Click="function(s, e) { updateReservePopup.Hide(); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    
     <dx:ASPxPopupControl ID="updateCommentPopup" runat="server" ClientInstanceName="updateCommentPopup"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        HeaderText="Изменение комментария" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False">
        <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup4'); tbUpdateComment.Focus(); }" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <dx:ASPxPanel ID="ASPxPanel4" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent4" runat="server">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                    <td class="pcmCellCaption" valign="top">
                                        <label>Номер заказа:</label>                                        
                                    </td>
                                    <td class="pcmCellText">
                                        <strong id="UpdateCommentOrderId"></strong>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="pcmCellCaption" valign="top">
                                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Комментарий:" AssociatedControlID="tbNewComment"/>
                                        
                                    </td>
                                    <td class="pcmCellText">
                                        <dx:ASPxMemo ID="tbUpdateComment" runat="server" Width="250px" ClientInstanceName="tbUpdateComment" >
                                            <ValidationSettings EnableCustomValidation="True" ValidationGroup="entryGroup4" SetFocusOnError="True"
                                                ErrorDisplayMode="Text" ErrorTextPosition="Bottom" CausesValidation="True">
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="True" />
                                                <ErrorFrameStyle Font-Size="10px">
                                                    <ErrorTextPaddings PaddingLeft="0px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dx:ASPxMemo>
                                    </td>
                                    <td rowspan="4">
                                        <div class="pcmSideSpacer">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="pcmButton">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="right">
                                                        <dx:ASPxButton ID="ASPxButton41" runat="server" Text="OK" Width="80px" AutoPostBack="False" ValidationGroup="entryGroup4" CausesValidation="True">
                                                            <ClientSideEvents Click="function(s,e){ OnUpdateCommentClick(s,e); return false;}" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <div style="width: 8px;">
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxButton ID="ASPxButton42" runat="server" Text="Отмена" Width="80px" AutoPostBack="False">
                                                            <ClientSideEvents Click="function(s, e) { updateCommentPopup.Hide(); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl> 
    
    <script language="javascript" type="text/javascript">

        function ApplyDateFilter(dde, fieldName, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            grid.AutoFilterByColumn(fieldName, dde.GetText());
        }

        function OnDateDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                var today = new Date();
                var from = new Date(today.getFullYear(), today.getMonth(), today.getDate());
                var to = from;
                    
                dateFrom.SetDate(from);
                dateTo.SetDate(to);
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }

        function OnDetailRowExpanding(e, s) {
            window.DetailRow = {
                visibleIndex: s.visibleIndex
            };
            console.log(window.DetailRow);
        }

        function OnUpdateReserveClick(s, e) {
            loadingPanel.Show();
            function onGetRowValues(value) {
                var textBox = tbNewWWSNum;
                var wwsOrderNo = textBox.GetText();
                var orderId = $('#UpdateReserveOrderId').text();
                var data = {
                    orderId: orderId,
                    wwsOrderNo: wwsOrderNo
                };
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/UpdateReserve",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        console.log(response.d);
                        var result = response.d;
                        loadingPanel.Hide();
                        if (result.Success == true) {
                            if (result.Message) {
                                alert(result.Message);
                            }
                            updateReservePopup.Hide();
                        } else {
                            alert('Ошибка при изменении номер резерва:' + result.Message);
                        }
                        grid.Refresh();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                        loadingPanel.Hide();
                        alert(xhr);
                        grid.Refresh();
                    }
                });
            }
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
        }

        function OnUpdateCommentClick(s, e) {
            loadingPanel.Show();
            function onGetRowValues(value) {
                var textBox = tbUpdateComment;
                var comment = textBox.GetText();
                var orderId = $('#UpdateCommentOrderId').text();
                var data = {
                    orderId: orderId,
                    comment: comment
                };
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/UpdateComment",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        console.log(response.d);
                        var result = response.d;
                        loadingPanel.Hide();
                        if (result.Success == true) {
                            updateCommentPopup.Hide();
                        } else {
                            alert('Ошибка при изменении комментария к заказу:' + result.Message);
                        }
                        grid.Refresh();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                        loadingPanel.Hide();
                        alert(xhr);
                        grid.Refresh();
                    }
                });
            }
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
        }

        function OnCancelClick(s, e) {
            loadingPanel.Show();
            function onGetRowValues(value) {
                var dropDownList = document.getElementById('<%= ddReason.ClientID %>');
                var cancelReason = dropDownList.value;
                var orderId = $('#CancelOrderId').text();
                var data = {
                    orderId: orderId,
                    cancelReason: cancelReason
                };
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/CancelOrder",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        console.log(response.d);
                        var result = response.d;
                        loadingPanel.Hide();
                        if (result.Success == true) {
                            confirmRemovePopup.Hide();
                        } else {
                            alert('Ошибка при отмене заказа:' + result.Message);
                        }
                        grid.Refresh();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                        loadingPanel.Hide();
                        alert(xhr);
                        grid.Refresh();
                    }
                });
            }
                grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
        }

        function OnAssignToMeClick(s, e) {
            loadingPanel.Show();

            function onGetRowValues(value) {
                var orderId = $('#AssignOrderId').text();
                var data = {
                    orderId: orderId,
                };
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/AssignOrderToMe",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        console.log(response.d);
                        var result = response.d;
                        loadingPanel.Hide();
                        if (result.Success == true) {
                            var toolticketUrlTemplate = '<%= ConfigurationManager.AppSettings["TaskDetailsUrl"]  %>';
                            var toolticketUrl = toolticketUrlTemplate.replace("{taskId}", result.TaskId);
                            window.open(toolticketUrl, '_blank', 'resizable,scrollbars');
                        } else {
                            alert('Ошибка при изменения заказа:' + result.Message);
                        }
                        confirmAssignPopup.Hide();
                        grid.Refresh();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                        loadingPanel.Hide();
                        alert(xhr);
                        grid.Refresh();
                    }
                });
            }
            grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
        }

            function onConfirmButtonClick(s, e) {

                function onGetRowValues(value) {
                    var orderId = value;
                    var data = {
                        orderId: orderId
                    };
                    if (confirm("Подтвердить заказ?")) {
                        $.ajax({
                            type: "POST",
                            url: "Default.aspx/ConfirmOrder",
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                console.log(response);
                                console.log(response.d);
                                var result = response.d;
                                if (result.Success == true) {
                                } else {
                                    alert('Ошибка при подтверждении заказа:' + result.Message);
                                }
                                grid.Refresh();
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr);
                                alert(xhr);
                                grid.Refresh();
                            }
                        });
                    }
                }
                grid.GetRowValues(window.DetailRow.visibleIndex, 'OrderId', onGetRowValues);
            }


            function ApplyFilter(dde, from, to) {
                var d1 = from.GetText();
                var d2 = to.GetText();
                if (d1 == "" && d2 == "") {
                    dde.SetText("");
                } else {
                    dde.SetText(d1 + "|" + d2);
                }
                grid.AutoFilterByColumn("TotalPrice", dde.GetText());
            }

            function ClsFilter(dde, from, to) {
                from.SetText("");
                to.SetText("");
                dde.SetText("");
                grid.AutoFilterByColumn("TotalPrice", dde.GetText());
            }

            function OnDropDown(s, from, to) {
                var str = s.GetText();
                if (str == "") {
                    return;
                }
                var d = str.split("|");
                from.SetText(d[0]);
                to.SetText(d[1]);
            }
    </script>
    <div style="height: 20px;"></div>
    <div>
        <dx:ASPxCheckBox runat="server" Text="Показывать все записи" AutoPostBack="true" ID="cbShowAllOrders" OnCheckedChanged="CbShawAllOrdersCheked"/>
        <dx:ASPxGridView ID="grid" runat="server" ClientInstanceName="grid" AutoGenerateColumns="False" KeyFieldName="OrderId"
        Width="100%" DataSourceID="LinqDataSource1"
            OnInit="grid_Init"  OnDetailRowExpandedChanged="ASPxGridView1_DetailRowExpandedChanged"
        OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" 
        OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize" OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter">
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0" Width="55px">
                <ClearFilterButton Visible="True" />
            </dx:GridViewCommandColumn>
            <dx:GridViewDataDateColumn FieldName="CreateDate" Caption="Создан"
                SortOrder="Descending"
                PropertiesDateEdit-DisplayFormatString="dd.MM.yy HH:mm:ss" PropertiesDateEdit-Width="90px">
                <PropertiesDateEdit DisplayFormatString="dd.MM.yy HH:mm:ss" Width="90px"></PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="OrderSource" Caption="Канал">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="OrderId" Caption="№ заказа (сайт)" ToolTip="№ заказа (сайт)">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="WWSOrders" Caption="№ заказа (WWS)" ToolTip="№ заказа (WWS)">
                <Settings AutoFilterCondition="Contains" />
                <DataItemTemplate>
                    <%# Eval("WWSOrderId") %>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SapCode" Caption="Магазин">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="IsCallSuccessful" Caption="Сохранен">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="ShouldBeProcessed" Caption="Должен быть в тикет-туле" ToolTip="Должен быть в тикет-туле">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="IsProcessOrder" Caption="В тикет-туле" ToolTip="В тикет-туле">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="CustomerName" Caption="Клиент">
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CustomerPhone" Caption="Телефон">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CustomerEmail" Caption="Email">
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Delivery" Caption="Самовывоз/Доставка" ToolTip="Самовывоз/Доставка">
                <PropertiesComboBox DataSourceID="odsDeliveryType" ValueType="System.Int32" DropDownStyle="DropDown"
                    TextField="Name" ValueField="Value">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="Address" Caption="Адрес">
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="DeliveryDate" Caption="Дата доставки" ToolTip="Дата доставки">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataComboBoxColumn FieldName="PaymentTypeEnum" Caption="Тип оплаты">
                <Settings HeaderFilterMode="CheckedList" />
                <PropertiesComboBox DataSourceID="odsPTypeStatuses" ValueType="System.Int32" DropDownStyle="DropDown"
                    TextField="Name" ValueField="Value" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="OrderStatusEnum" Caption="Состояние заказа">
                <Settings HeaderFilterMode="CheckedList" />
                <PropertiesComboBox DataSourceID="odsOrderStatuses" ValueType="System.Int32" DropDownStyle="DropDown"
                    TextField="Name" ValueField="Value" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="GiftCertificateId" Caption="Купон/сертификат" ToolTip="Купон/сертификат">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="SmartCloseDate" Caption="Изменен" PropertiesDateEdit-DisplayFormatString="dd.MM.yy HH:mm:ss">
                <PropertiesDateEdit DisplayFormatString="dd.MM.yy HH:mm:ss"></PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Process" Caption="Статус обработки">
                <Settings HeaderFilterMode="CheckedList" ShowFilterRowMenu="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="HasOutcome" Caption="Завершена">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="TotalPrice" Caption="Сумма заказа" PropertiesTextEdit-DisplayFormatString="0.00">
                <PropertiesTextEdit DisplayFormatString="0.00"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Comment" Caption="Комментарий"  ShowInCustomizationForm="True">
                <PropertiesTextEdit EncodeHtml="False"/>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <DetailRow>
                <div style="padding: 3px 3px 2px 3px">
                    <dx:ASPxButton runat="server" Text="Отменить заказ" ID="btnCancel" Style="display: inline-table;" AutoPostBack="False" OnDataBinding="btnCancel_DataBinding">
                        <ClientSideEvents Click="function(s, e) { ShowConfirmWindow(); }" />
                    </dx:ASPxButton>
                    <dx:ASPxButton runat="server" Text="Взять в работу" ID="btnAssign" Style="display: inline-table;" AutoPostBack="False" OnDataBinding="btnAssign_DataBinding" >
                        <ClientSideEvents Click="function(s, e) { ShowAssignWindow(); }" />
                    </dx:ASPxButton>
                    <dx:ASPxButton runat="server" Text="Изменить резерв" ID="btnUpdateReserve" Style="display: inline-table;" AutoPostBack="False" OnDataBinding="btnUpdateReserve_DataBinding">
                        <ClientSideEvents Click="function(s, e) { ShowUpdateReserveWindow(); }" />
                    </dx:ASPxButton>
                    <dx:ASPxButton runat="server" Text="Изменить комментарий" ID="btnUpdateCommect" Style="display: inline-table;" AutoPostBack="False">
                        <ClientSideEvents Click="function(s, e) { ShowUpdateCommentWindow(); }" />
                    </dx:ASPxButton>
                    <%-- TODO --%>
                    <%--<dx:ASPxButton runat="server" Text="Подтвердить" ID="btnConfirm" Style="display: inline-table;" AutoPostBack="False" OnDataBinding="btnConfirm_DataBinding">
                        <ClientSideEvents Click="function(s, e) { onConfirmButtonClick; }" />
                    </dx:ASPxButton>--%>
                    <br />
                    <br />
                    <dx:ASPxPageControl runat="server" ID="pageControl" Width="100%" EnableCallBacks="true" ActiveTabIndex="0" OnDataBinding="pageControl_DataBinding">
                        <TabPages>
                            <dx:TabPage Text="Статусы" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl6" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView1" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsOrderStatusesH">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Width="150" FieldName="UpdateDate" ShowInCustomizationForm="True" Caption="Дата">
                                                    <PropertiesDateEdit DisplayFormatString="dd.MM.yy HH:mm:ss">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataComboBoxColumn Width="100" FieldName="FromStatusEnum" Caption="Из">
                                                    <PropertiesComboBox DataSourceID="odsOrderStatuses" ValueType="System.Int32" DropDownStyle="DropDown"
                                                        TextField="Name" ValueField="Value" />
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Width="100" FieldName="ToStatusEnum" Caption="В">
                                                    <PropertiesComboBox DataSourceID="odsOrderStatuses" ValueType="System.Int32" DropDownStyle="DropDown"
                                                        TextField="Name" ValueField="Value" />
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataTextColumn Width="150" FieldName="ReasonText" ShowInCustomizationForm="True" Caption="Комментарий">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataComboBoxColumn Width="100" FieldName="ChangedTypeEnum" Caption="Тип изменения">
                                                    <PropertiesComboBox DataSourceID="odsChangedType" ValueType="System.Int32" DropDownStyle="DropDown"
                                                        TextField="Name" ValueField="Value" />
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataTextColumn  Width="150" FieldName="WhoChanged" ShowInCustomizationForm="True" Caption="Пользователь">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" >
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsOrderStatusesH" runat="server" SelectMethod="GetOrderStatuses" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="Задачи" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl4" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView4" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsTicketTasks" OnDataBinding="ASPxGridView4_DataBinding">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Name" Caption="Название" ShowInCustomizationForm="True" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Description" Caption="Описание" ShowInCustomizationForm="True" VisibleIndex="2" Width="400px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHtmlEditor Height="50px" runat="server" Html='<%# Eval("Description") %>' Width="400px" ActiveView="Preview" Settings-AllowContextMenu="False" Settings-AllowDesignView="False" Settings-AllowHtmlView="False" SettingsResize-AllowResize="False"></dx:ASPxHtmlEditor>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Outcome" Caption="Исход" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <%--<dx:GridViewDataTextColumn FieldName="Comment" Caption="Комментарий" ShowInCustomizationForm="True" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>--%>
                                                <dx:GridViewDataDateColumn FieldName="Created" Caption="Дата создания" ShowInCustomizationForm="True" VisibleIndex="5" PropertiesDateEdit-DisplayFormatString="dd.MM.yy HH:mm:ss">
                                                    <PropertiesDateEdit DisplayFormatString="dd.MM.yy HH:mm:ss">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn FieldName="Completed" Caption="Дата завершения" ShowInCustomizationForm="True" VisibleIndex="7" PropertiesDateEdit-DisplayFormatString="dd.MM.yy HH:mm:ss">
                                                    <PropertiesDateEdit DisplayFormatString="dd.MM.yy HH:mm:ss">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataCheckColumn FieldName="Escalated" Caption="Эскалировано" ShowInCustomizationForm="True" VisibleIndex="8">
                                                </dx:GridViewDataCheckColumn>
                                                <dx:GridViewDataTextColumn FieldName="Performer" Name="User" Caption="Исполнитель" ShowInCustomizationForm="True" VisibleIndex="13">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AssignedTo" Name="AssignedTo" Caption="Взял в работу" ShowInCustomizationForm="True" VisibleIndex="13">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="SkillSet.Name" Caption="Скилсет" ShowInCustomizationForm="True" VisibleIndex="14">
                                                </dx:GridViewDataTextColumn>
                                                <%--<dx:GridViewDataTextColumn FieldName="RequiredFields" Caption="Параметры" ShowInCustomizationForm="True" VisibleIndex="2" Width="400px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHtmlEditor Height="50px" runat="server" Html='<%# Eval("Description") %>' Width="400px" ActiveView="Preview" Settings-AllowContextMenu="False" Settings-AllowDesignView="False" Settings-AllowHtmlView="False" SettingsResize-AllowResize="False"></dx:ASPxHtmlEditor>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>--%>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsTicketTasks" runat="server" SelectMethod="GetTicketTasks" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="История резервов" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl7" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView6" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsReserveHeaders">
                                            <Columns>
                                                <dx:GridViewDataComboBoxColumn  Width="150" FieldName="ReserveStatusEnum" Caption="Статус резерва">
                                                    <PropertiesComboBox DataSourceID="odsReserveStatus" ValueType="System.Int32" DropDownStyle="DropDown"
                                                        TextField="Name" ValueField="Value" />
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataTextColumn  Width="100" FieldName="WWSOrderId" Caption="№ заказа (WWS)" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn  Width="100" FieldName="SapCode" Caption="Магазин" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataCheckColumn  Width="100" FieldName="IsApproved" Caption="Подтвержден" ShowInCustomizationForm="True">
                                                </dx:GridViewDataCheckColumn>
                                                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" >
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsReserveHeaders" runat="server" SelectMethod="GetReserveHeaders" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="Строки резерва" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl5" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView5" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsReserveLine" OnDataBinding="ASPxGridView5_DataBinding">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="ArticleNum" Width="100" Caption="Артикул" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Title" Width="300" Caption="Название" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Qty"  Width="100" Caption="Кол-во" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Price"  Width="100" Caption="Цена" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LineTypeEnumStr"  Width="100" Caption="Тип" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="StockItemStateEnumStr"  Width="100" Caption="Состояние" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>
                                                <%--<dx:GridViewDataTextColumn FieldName="ReviewItemStateEnumStr" Caption="Ответ менеджера" ShowInCustomizationForm="True">
                                                </dx:GridViewDataTextColumn>--%>
                                                <dx:GridViewDataColumn  FieldName="ReviewItemStateEnumStr"  Width="100" Caption="Ответ менеджера" ShowInCustomizationForm="True">                                                    
                                                    <DataItemTemplate>
                                                        <b><%# Eval("ReviewItemStateEnumStr") %></b><br/>
                                                        <%# Eval("ArticleCondition") %>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>                                                
                                                <dx:GridViewDataTextColumn FieldName="Comment"  Width="250" Caption="Комментарий"  ShowInCustomizationForm="True">
                                                    <PropertiesTextEdit EncodeHtml="False"/>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" >
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsReserveLine" runat="server" SelectMethod="GetReserveLine" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                                <asp:SessionParameter Name="wwsOrderId" SessionField="wwsOrderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="Строки заказа" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl2" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView2" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsOrderLine" OnDataBinding="ASPxGridView1_DataBinding">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="ArticleNum" Caption="Артикул" ShowInCustomizationForm="True"   Width="100">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Qty" Caption="Кол-во" ShowInCustomizationForm="True"   Width="100">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Price" Caption="Цена" ShowInCustomizationForm="True"   Width="100">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LineTypeEnumStr" Caption="Тип" ShowInCustomizationForm="True"   Width="100">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" >
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsOrderLine" runat="server" SelectMethod="GetOrderLines" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="Исходы по магазинам" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl108" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView107" runat="server" Width="100%" AutoGenerateColumns="False"
                                             DataSourceID="outcomes" OnDataBinding="ASPxGridView6_DataBinding">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Width="50" FieldName="SapCode" Caption="Магазин" ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="150" FieldName="Created" Caption="Дата" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="300" FieldName="Outcome" Caption="Исход" ShowInCustomizationForm="True" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="200" FieldName="Performer" Caption="Исполнитель" ShowInCustomizationForm="True" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>   
                                                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>                                                
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="outcomes" runat="server" 
                                            SelectMethod="GetSaleLocationOutcomes" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="История роутинга">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                        <dx:ASPxGridView ID="historyGrid" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsRouterHistory">
                                            <Columns>
                                                <dx:GridViewDataDateColumn FieldName="UpdateDate" Caption="Дата обновления" ShowInCustomizationForm="True" VisibleIndex="1">
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="RecTypeEnumStr" Caption="Тип" ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Text" Caption="Текст" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsRouterHistory" runat="server" SelectMethod="GetRouterHistory" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="Ошибки обновления">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl3" runat="server">
                                        <dx:ASPxGridView ID="ASPxGridView3" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="odsOrderError">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="OrderStatusEnumStr" Caption="Статус" ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ErrorText" Caption="Текст" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="UpdateDate" Caption="Дата" ShowInCustomizationForm="True" VisibleIndex="4">
                                                </dx:GridViewDataDateColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:ObjectDataSource ID="odsOrderError" runat="server" SelectMethod="GetOrderErrors" TypeName="Orders.Backend.Clients.OrderMonitoring.DataFacade">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="orderId" SessionField="orderId" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                        </TabPages>
                    </dx:ASPxPageControl>
                </div>
            </DetailRow>
        </Templates>
        <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="True" />
        <SettingsBehavior EnableCustomizationWindow="true" />
        <SettingsPager PageSize="20" />
        <Settings VerticalScrollBarMode="Hidden" VerticalScrollableHeight="0" VerticalScrollBarStyle="Virtual"
            ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="true"/>
        <Paddings Padding="0px" />
        <ClientSideEvents Init="OnInit" EndCallback="OnEndCallback" DetailRowExpanding="OnDetailRowExpanding" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>
        <dx:ASPxLoadingPanel id="ASPxLoadingPanel1" Modal="True" runat="server" clientinstancename="loadingPanel"></dx:ASPxLoadingPanel>
    </div>
    <dx:ASPxGlobalEvents ID="ge" runat="server">
        <ClientSideEvents ControlsInitialized="OnControlsInitialized" />
    </dx:ASPxGlobalEvents>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="Orders.Backend.Router.Dal.Model.RoutableOrderDbEntities" OrderBy="OrderId"
        EntityTypeName="" TableName="OrderMonitorings" Where="" />     
</asp:Content>
