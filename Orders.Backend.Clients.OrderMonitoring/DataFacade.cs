﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Helpers;
using Order.Backend.Dal.Common;
using Orders.Backend.Clients.OrderMonitoring.Proxy.Sls;
using Orders.Backend.Clients.OrderMonitoring.Proxy.TicketTool;
using Orders.Backend.Dal;
using Orders.Backend.Router.Dal.Model;
using OrderLine = Orders.Backend.Dal.OrderLine;

namespace Orders.Backend.Clients.OrderMonitoring
{
    public static class DataFacade
    {

        public static SaleLocationOutcome[] GetSaleLocationOutcomes(string orderId)
        {
            using (var client = new SaleLocationServiceClient())
            {                
                var result = client.GetSaleLocationOutcomes(orderId);
                return result.ReturnCode == ReturnCode.Error ? new SaleLocationOutcome[0] : result.Result;
            }
        }

        public static History[] GetRouterHistory(string orderId)
        {
            using (var db = new RoutableOrderDbEntities())
            {
                return db.Histories.Where(h => h.OrderId == orderId)
                                   .OrderBy(h => h.UpdateDate).ToArray();
            }
        }

        public static OrderLine[] GetOrderLines(string orderId)
        {
            using (var db = new OnlineOrdersContainer())
            {
                return db.OrderLines.Where(h => h.OrderId == orderId).ToArray();
            }
        }

        public static OrderError[] GetOrderErrors(string orderId)
        {
            using (var db = new OnlineOrdersContainer())
            {
                return db.OrderErrors.Where(h => h.OrderId == orderId)
                    .OrderBy(e => e.UpdateDate).ToArray();
            }
        }

        public static TicketTask[] GetTicketTasks(string orderId)
        {
            using(var client = new TicketToolServiceClient())
            {
                return client.GetTasksByWorkitemId(orderId)
                    .OrderBy(t=>t.Created)
                    .ToArray();
            }
        }

        public static ReserveHeader[] GetReserveHeaders(string orderId)
        {
            using (var db = new OnlineOrdersContainer())
            {
                var lines = db.ReserveHeader.Where(tt => tt.OrderHeader.OrderId == orderId)
                            .OrderByDescending(tt => tt.WWSOrderId)
                            .ToArray();
                return lines;
            }
        }

        public static ReserveLine[] GetReserveLine(string orderId, string wwsOrderId)
        {
            try
            {

            using (var db = new OnlineOrdersContainer())
            {
                var lines = db.ReserveLine
                            .Where(tt =>
                                tt.ReserveHeader.OrderHeader.OrderId == orderId &&
                                tt.ReserveHeader.WWSOrderId == wwsOrderId)
                            .GroupBy(tt => tt.ReserveHeader.ReserveId)
                            .OrderByDescending(tt => tt.Key)
                            .FirstOrDefault();
                return lines == null ? null : lines.ToArray();
            }

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public static OrderStatusHistory[] GetOrderStatuses(string orderId)
        {
            using (var db = new OnlineOrdersContainer())
            {
                return db.OrderStatusHistories
                    .Where(tt => tt.OrderId == orderId)
                    .OrderByDescending(tt => tt.UpdateDate).ToArray();
            }
        }

        public class EVN
        {
            public int Value { get; set; }
            public string Name { get; set; }
        }

        //TODO: refactor

        public static EVN[] GetPTypeStatuses()
        {
            var values = Enum.GetValues(typeof(OrderPaymentType));
            var res = new List<OrderPaymentType>(values.Length);
            res.AddRange(values.Cast<OrderPaymentType>());
            return res.Select(i => new EVN() { Name = i.GetDescription(), Value = (int)i }).ToArray();
        }

        public static EVN[] GetOrderStatuses()
        {
            var values = Enum.GetValues(typeof(InternalOrderStatus));
            var res = new List<InternalOrderStatus>(values.Length);
            res.AddRange(values.Cast<InternalOrderStatus>());
            return res.Select(i => new EVN() { Name = i.GetDescription(), Value = (int)i }).ToArray();
        }

        public static EVN[] GetChangedTypes()
        {
            var values = Enum.GetValues(typeof(ChangedType));
            var res = new List<ChangedType>(values.Length);
            res.AddRange(values.Cast<ChangedType>());
            return res.Select(i => new EVN() { Name = i.GetDescription(), Value = (int)i }).ToArray();
        }

        public static EVN[] GetDeliveryTypes()
        {
            var values = Enum.GetValues(typeof(DeliveryTypeEnum));
            var res = new List<DeliveryTypeEnum>(values.Length);
            res.AddRange(values.Cast<DeliveryTypeEnum>());
            var deliveryTypes = res.Select(i => new EVN() { Name = i.GetDescription(), Value = (int)i }).ToArray();
            return deliveryTypes;
        }

        public static EVN[] GetReserveStatuses()
        {
            var values = Enum.GetValues(typeof(ReserveStatus));
            var res = new List<ReserveStatus>(values.Length);
            res.AddRange(values.Cast<ReserveStatus>());
            return res.Select(i => new EVN() { Name = i.GetDescription(), Value = (int)i }).ToArray();
        }
    }
}