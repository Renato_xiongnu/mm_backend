﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;

namespace Orders.Backend.Clients.OrderMonitoring.Filter
{
    public class DateSelectorTemplate : ITemplate
    {
        public readonly string FieldName;

        // client IDs of two ASPxDateEdit controls
        public String CIdFrom;

        public String CIdTo;

        public DateSelectorTemplate(string fieldName)
        {
            FieldName = fieldName;
        }

        public void InstantiateIn(Control container)
        {
            var table = new Table();
            container.Controls.Add(table);
            var row = new TableRow();
            table.Rows.Add(row);

            var cell = new TableCell();
            row.Cells.Add(cell);
            var lbl = new ASPxLabel
            {
                ID = "lblFrom", 
                Text = "С:"
            };
            cell.Controls.Add(lbl);

            cell = new TableCell();
            row.Cells.Add(cell);
            var dateFrom = new ASPxDateEdit
            {
                ID = "dateFrom", 
                EnableClientSideAPI = true
            };
            cell.Controls.Add(dateFrom);

            row = new TableRow();

            table.Rows.Add(row);

            cell = new TableCell();
            row.Cells.Add(cell);
            lbl = new ASPxLabel
            {
                ID = "lblTo", 
                Text = "До:"
            };
            cell.Controls.Add(lbl);

            cell = new TableCell();
            row.Cells.Add(cell);
            var dateTo = new ASPxDateEdit
            {
                ID = "dateTo", 
                EnableClientSideAPI = true
            };
            cell.Controls.Add(dateTo);

            CIdFrom = dateFrom.ClientID;
            CIdTo = dateTo.ClientID;

            row = new TableRow();

            table.Rows.Add(row);

            cell = new TableCell {ColumnSpan = 2};
            row.Cells.Add(cell);
            var lnk = new ASPxHyperLink
            {
                ID = "lnkApply", 
                Text = "Применить", 
                NavigateUrl = "javascript:void(0)"
            };
            lnk.ClientSideEvents.Click =
                String.Format("function (s, e) {{ {0}.HideDropDown(); ApplyDateFilter({0}, \"{1}\", {2}, {3}); }}",                    
                    container.NamingContainer.NamingContainer.ClientID,
                    FieldName,
                    CIdFrom,
                    CIdTo);
            cell.Controls.Add(lnk);
        }
    }
}