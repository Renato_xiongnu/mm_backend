﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

namespace Orders.Backend.Clients.OrderMonitoring.Filter
{
    public class RangeTemplate : ITemplate
    {
        // client IDs of two ASPxDateEdit controls
        public String cIdFrom;
        public String cIdTo;

        public void InstantiateIn(Control container)
        {
            var table = new Table();
            container.Controls.Add(table);
            var row = new TableRow();
            table.Rows.Add(row);

            var cell = new TableCell();
            row.Cells.Add(cell);
            var lbl = new ASPxLabel { ID = "lblFrom", Text = "От:" };
            cell.Controls.Add(lbl);

            cell = new TableCell();
            row.Cells.Add(cell);
            var dateFrom = new ASPxSpinEdit { ID = "dateFrom", EnableClientSideAPI = true };
            cell.Controls.Add(dateFrom);

            row = new TableRow();

            table.Rows.Add(row);

            cell = new TableCell();
            row.Cells.Add(cell);
            lbl = new ASPxLabel { ID = "lblTo", Text = "До:" };
            cell.Controls.Add(lbl);

            cell = new TableCell();
            row.Cells.Add(cell);
            var dateTo = new ASPxSpinEdit { ID = "dateTo", EnableClientSideAPI = true };
            cell.Controls.Add(dateTo);

            cIdFrom = dateFrom.ClientID;
            cIdTo = dateTo.ClientID;

            row = new TableRow();

            table.Rows.Add(row);

            cell = new TableCell() { ColumnSpan = 2};
            row.Cells.Add(cell);
            var lnk1 = new ASPxHyperLink { ID = "lnkCls", Text = "Сбросить", NavigateUrl = "javascript:void(0)" };
            lnk1.ClientSideEvents.Click = String.Format("function (s, e) {{ {0}.HideDropDown(); ClsFilter({0}, {1}, {2}); }}",
                container.NamingContainer.NamingContainer.ClientID,
                cIdFrom,
                cIdTo);            
            cell.Controls.Add(lnk1);

            //cell = new TableCell();
            //row.Cells.Add(cell);
            //cell.Controls.Add(new ASPxLabel { Text = "   " });
            //var lnk2 = new ASPxHyperLink { ID = "lnkApply", Text = "Применить", NavigateUrl = "javascript:void(0)" };
            //lnk2.ClientSideEvents.Click = String.Format("function (s, e) {{ {0}.HideDropDown(); ApplyFilter({0}, {1}, {2}); }}",
            //    container.NamingContainer.NamingContainer.ClientID,
            //    cIdFrom,
            //    cIdTo);
            //cell.Controls.Add(lnk2);
        }
    }
}