﻿using System.Web.Security;
using OnlineOrders.Common;
using TicketTool.Security.Membership;

namespace Orders.Backend.Clients.OrderMonitoring
{
    public static class AuthContext
    {
        public static bool IsLoggedIn
        {
            get { return Membership.GetUser() != null; }
        }

        public static bool IsAdmin
        {
            get { return Roles.IsUserInRole(UserRoles.Admin.ToString()); }
        }

        public static bool IsSuperviser
        {
            get { return Roles.IsUserInRole(UserRoles.Superviser.ToString()); }
        }

        public static bool IsStoreManager
        {
            get { return Roles.IsUserInRole(UserRoles.StoreManager.ToString()); }
        }

        public static bool IsAgent
        {
            get { return Roles.IsUserInRole(UserRoles.Agent.ToString()); }
        }

        // show additional fields on UI
        public static bool IsProUser
        {
            get { return IsAdmin || IsSuperviser; }
        }

        public static string UserName
        {
            get
            {
                var user = Membership.GetUser();
                return user != null ? user.UserName : string.Empty;
            }
        }

        public static string[] SapCodes
        {
            get
            {
                var user = Membership.GetUser() as TTMembershipUser;
                return user == null ? new string[0] : user.SapCodes;
            }
        }
    }
}