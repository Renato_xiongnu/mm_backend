﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq;
using DevExpress.Utils;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using Order.Backend.Dal.Common;
using Orders.Backend.Clients.OrderMonitoring.Filter;
using Orders.Backend.Clients.OrderMonitoring.Proxy.TicketTool;
using Orders.Backend.Clients.OrderMonitoring.RouterService;
using Orders.Backend.Router.Dal.Model;
using InternalOrderStatus = Order.Backend.Dal.Common.InternalOrderStatus;
using Parameter = System.Web.UI.WebControls.Parameter;

namespace Orders.Backend.Clients.OrderMonitoring
{
    public partial class _Default : Page
    {
        private const string IsProcessOrderPropertyName = "isProcessOrder";
        private const string OrderIdPropertyName = "orderId";

        protected void Page_Load(object sender, EventArgs e)
        {
            //grid.Columns["IsCallSuccessful"].Visible = AuthContext.IsProUser;
            //grid.Columns["OrderStatusEnum"].Visible = AuthContext.IsAdmin;
            //grid.Columns["GiftCertificateId"].Visible = AuthContext.IsProUser;
            //grid.Columns["HasOutcome"].Visible = AuthContext.IsAdmin;

            var rolesToLogin = ConfigurationManager.AppSettings["RolesToLogin"].Split(new[] {";"},
                StringSplitOptions.RemoveEmptyEntries);
            if (!Roles.GetRolesForUser().Any(t => rolesToLogin.Contains(t.ToLower())))
            {
                Session["FailAuth"] = "У Вас нет прав";
                Response.Redirect("Account/Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                var orderNoParam = Request["orderNo"];
                if (!string.IsNullOrWhiteSpace(orderNoParam))
                {
                    grid.FilterExpression =
                        new BinaryOperator("OrderId", orderNoParam, BinaryOperatorType.Equal).LegacyToString();
                }
            }

            if (!cbShowAllOrders.Checked)
            {
                LinqDataSource1.Where = "CreateDate>@FromDate ";
                var parameter = new Parameter("FromDate", DbType.Date)
                {
                    DefaultValue = DateTime.Today.AddDays(-30).ToString()
                };
                LinqDataSource1.WhereParameters.Add(parameter);
            }

            if (!AuthContext.IsProUser)
            {
                var @where = string.Join(" || ", AuthContext.SapCodes.Select(s => string.Format("SapCode = \"{0}\"", s)));
                if (!string.IsNullOrEmpty(@where))
                {
                    @where = string.Format("({0})", @where);
                    LinqDataSource1.Where += string.IsNullOrWhiteSpace(LinqDataSource1.Where) ||
                                             string.IsNullOrEmpty(LinqDataSource1.Where)
                        ? @where
                        : string.Format(" && {0}", @where);
                }
            }
            LinqDataSource1.AutoGenerateOrderByClause = false;
            foreach (var col in grid.Columns.OfType<GridViewDataColumn>())
            {
                col.Settings.AllowAutoFilterTextInputTimer = DefaultBoolean.False;
            }
            grid.Settings.ShowHeaderFilterButton = true;
        }

        [WebMethod]
        public static object AssignOrderToMe(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                return new
                {
                    Success = false,
                    Message = "OrderId can not be null"
                };
            }

            using (var c = new TicketToolServiceClient())
            {
                var currentTask = c.GetTasksByWorkitemId(orderId)
                    .OrderByDescending(t => t.Created)
                    .FirstOrDefault();

                if (currentTask == null /*&& currentTask.State == TaskState.Created*/)
                {
                    return new
                    {
                        Success = false,
                        Message = "Заказ не может быть взят в работу.",
                    };
                }

                var response = c.AssignTaskForMe(currentTask.TaskId);
                return new
                {
                    Success = !response.HasError,
                    Message = response.MessageText,
                    TaskId = currentTask.TaskId
                };

                /*RoutableOrder order;
                using (var context = new RoutableOrderDbEntities())
                {
                    order = context.RoutableOrders.First(_ => _.OrderId == orderId);
                }

                if (order.IsProcessOrder && string.IsNullOrEmpty(order.ProcessVersion))
                {
                    return new
                    {
                        Success = false,
                        Message = "Заказ был создан не корректоно.",
                    };
                }

                string productVersion = StartProcess(orderId);


                var ticketId = c.GetTicket(orderId);

                if (string.IsNullOrEmpty(ticketId))
                {
                    ticketId = c.CreateTicket(order.ProcessVersion, orderId, order.SapCode);
                }

                var taskId = c.CreateTask(new TicketTask()
                {
                    Created = DateTime.Now,
                    Name = "Подтвердите заказ",
                    Description = "Проверьте артикулы заказа. Позвоните покупателю и подтвердите получение его заказа. Скажите, что его заказ находится в обработке.",
                    Parameter = order.SapCode,
                    RequiredFields = new RequiredField[0],
                    WorkItemId = orderId,
                    State = TaskState.InProgress,
                    Type = "Approve_Order",
                    TicketId = ticketId,
                    Outcomes = new Outcome[]
                    {
                        new Outcome()
                        {
                            IsSuccess = false,
                            RequiredFields =  new RequiredField[0],
                            SkipTaskRequeriedFields = false,
                            Value = "Не дозвонился",
                        }, 
                        new Outcome()
                        {
                            IsSuccess = false,
                            RequiredFields =  new RequiredField[]
                            {
                                new RequiredField()
                                {
                                    Name = "Перезвонить мне через",
                                    Type = "System.TimeSpan"
                                }, 
                            },
                            SkipTaskRequeriedFields = true,
                            Value = "Перезвонить позже"
                        }, 
                        new Outcome()
                        {
                            IsSuccess = true,
                            RequiredFields =  new RequiredField[0],
                            SkipTaskRequeriedFields = false,
                            Value = "Подтверждено",
                        }, 
                        new Outcome()
                        {
                            IsSuccess = true,
                            RequiredFields =  new RequiredField[]
                            {
                                new RequiredFieldValueList()
                                {
                                    DefaultValue = "Витрина",
                                    Type = "Choice",
                                    PredefinedValuesList = new object []
                                    {
                                        "Другое",
                                        "Купил в другом магазине (не ММ)",
                                        "Не устроили условия доставки",
                                        "Неверная цена на сайте",
                                        "Невозможно связаться с покупателем",
                                        "Недействительный/ тестовый заказ/ заказ переоформлен",
                                        "Нет в наличии",
                                        "Ошибка ВВС/ТТ",
                                        "Передумал делать покупку",
                                        "Перекуп/ОПТ/Безнал",
                                        "Товар после ремонта/ сломан/ ожидает возврата поставщику",
                                        "Уже купил в ММ",
                                        "ЯМ: Изменился состав заказа"
                                    },
                                }, 
                            },
                            SkipTaskRequeriedFields = false,
                            Value = "Отклонено покупателем",
                        }
                    },
                    SkillSet = new SkillSet()
                    {
                        IsEscalation = false,
                        Name = "Call центр",
                        ParamType = SkillSetParamType.None,
                        TasksDeliveredByEmail = false,
                    }
                });

                var response2 = c.ForceAssignTaskForMe(taskId);
                return new
                {
                    Success = !response2.HasError,
                    Message = response2.MessageText,
                    TaskId = !response2.HasError ? taskId : null
                };*/
            }
        }

        [WebMethod]
        public static object CancelOrder(string orderId, string cancelReason)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                return new
                {
                    Success = false,
                    Message = "OrderId can not be null"
                };
            }
            if (string.IsNullOrEmpty(cancelReason))
            {
                return new
                {
                    Success = false,
                    Message = "CancelReason can not be null"
                };
            }
            try
            {
                using (var c = new OrderServiceClient())
                {
                    var result = c.CancelOrder(orderId, cancelReason, AuthContext.UserName);
                    return new
                    {
                        Success = result.ReturnCode == ReturnCode.Ok,
                        Message = string.IsNullOrEmpty(result.ErrorMessage)
                            ? string.Format("Заказ №{0} отменен по причине {1}.", orderId, cancelReason)
                            : result.ErrorMessage
                    };
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Success = false,
                    Message = "Exception:" + ex.Message
                };
            }
        }

        [WebMethod]
        public static object UpdateReserve(string orderId, string wwsOrderNo)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                return new
                {
                    Success = false,
                    Message = "OrderId can not be null"
                };
            }
            if (string.IsNullOrEmpty(wwsOrderNo))
            {
                return new
                {
                    Success = false,
                    Message = "WwsOrderNo can not be null"
                };
            }
            try
            {
                using (var c = new OrderServiceClient())
                {
                    var result = c.UpdateWWSOrderNumber(orderId, wwsOrderNo);
                    return new
                    {
                        Success = result.ReturnCode == ReturnCode.Ok,
                        Message = string.IsNullOrEmpty(result.ErrorMessage)
                            ? string.Format("Для заказа №{0} изменен номер резерва на {1}", orderId, wwsOrderNo)
                            : result.ErrorMessage
                    };
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Success = false,
                    Message = "Exception:" + ex.Message
                };
            }
        }

        [WebMethod]
        public static object UpdateComment(string orderId, string comment)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                return new
                {
                    Success = false,
                    Message = "OrderId can not be null"
                };
            }
            if (string.IsNullOrEmpty(comment))
            {
                return new
                {
                    Success = false,
                    Message = "Comment can not be null"
                };
            }
            try
            {
                using (var c = new OrderServiceClient())
                {
                    var result = c.UpdateOrderComment(orderId, comment);
                    return new
                    {
                        Success = result.ReturnCode == ReturnCode.Ok,
                        Message = string.IsNullOrEmpty(result.ErrorMessage)
                            ? string.Format("Для заказа №{0} изменен изменен комментарий на \"{1}\"", orderId, comment)
                            : result.ErrorMessage
                    };
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Success = false,
                    Message = "Exception:" + ex.Message
                };
            }
        }

        [WebMethod]
        public static object ConfirmOrder(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                return new
                {
                    Success = false,
                    Message = "OrderId can not be null"
                };
            }
            try
            {
                using (var c = new OrderServiceClient())
                {
                    var result = c.ConfirmOrder(orderId, AuthContext.UserName);
                    return new
                    {
                        Success = result.ReturnCode == ReturnCode.Ok,
                        Message = string.IsNullOrEmpty(result.ErrorMessage)
                            ? string.Format("Заказа №{0} подтвержден", orderId)
                            : result.ErrorMessage
                    };
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Success = false,
                    Message = "Exception:" + ex.Message
                };
            }
        }

        protected void CancelClick(object sender, EventArgs e)
        {
            try
            {
                var c = new OrderServiceClient();
                c.CancelOrder(Session[OrderIdPropertyName].ToString(), ddReason.Text, AuthContext.UserName);
            }
            finally
            {
                confirmRemovePopup.ShowOnPageLoad = false;
                grid.DataBind();
            }
        }

        protected void UpdateReserveClick(object sender, EventArgs e)
        {
            try
            {
                var c = new OrderServiceClient();
                c.UpdateWWSOrderNumber(Session[OrderIdPropertyName].ToString(), tbNewWWSNum.Text);
            }
            finally
            {
                updateReservePopup.ShowOnPageLoad = false;
                grid.DataBind();
            }
        }

        protected void ASPxGridView1_DetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                Session[OrderIdPropertyName] = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, new[] { "OrderId" });
                Session["wwsOrderId"] = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, new[] { "WWSOrderId" });
                Session["status"] = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, new[] { "OrderStatusEnum" });
                Session[IsProcessOrderPropertyName] = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, new[] { "IsProcessOrder" });
                Session[OrderPropertyName] = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex);

                tbUpdateComment.Text = Convert.ToString(((ASPxGridView)sender).GetRowValues(e.VisibleIndex, new[] { "Comment" }));
            }
        }

        protected void pageControl_DataBinding(object sender, EventArgs e)
        {
            var tabs = sender as ASPxPageControl;
            if (tabs != null)
            {
                //tabs.TabPages[5].Visible = tabs.TabPages[6].Visible = AuthContext.IsProUser;
            }
        }

        // reserve lines
        protected void ASPxGridView5_DataBinding(object sender, EventArgs e)
        {
            var tabs = sender as ASPxGridView;
            if (tabs != null)
            {
                //tabs.Columns["LineTypeEnumStr"].Visible = AuthContext.IsProUser;
            }
        }

        //Ticket Tasks
        protected void ASPxGridView4_DataBinding(object sender, EventArgs e)
        {
            var tabs = sender as ASPxGridView;
            if (tabs != null)
            {

                //tabs.Columns["Created"].Visible = AuthContext.IsProUser;
                //tabs.Columns["Escalated"].Visible = AuthContext.IsProUser;
                //tabs.Columns["SkillSet.Name"].Visible = AuthContext.IsProUser;
            }
        }

        //Order Lines
        protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
        {
            var tabs = sender as ASPxGridView;
            if (tabs != null)
            {
                //tabs.Columns["LineTypeEnumStr"].Visible = AuthContext.IsProUser;
            }
        }

        protected void ASPxGridView6_DataBinding(object sender, EventArgs e)
        {
            var tabs = sender as ASPxGridView;
            if (tabs != null)
            {
            }
        }

        protected void btnCancel_DataBinding(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            if (btn == null)
            {
                return;
            }
            var statusObj = Session["status"];
            var status = statusObj != null ? (InternalOrderStatus)statusObj : InternalOrderStatus.Blank;
            btn.Visible = EnumUtils.AllowCancelOrder(status);
        }

        protected void btnConfirm_DataBinding(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            if (btn == null)
            {
                return;
            }
            var statusObj = Session["status"];
            var status = statusObj != null ? (InternalOrderStatus)statusObj : InternalOrderStatus.Blank;
            btn.Visible = EnumUtils.AllowConfirmOrder(status);
        }

        protected void btnUpdateReserve_DataBinding(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            if (btn == null)
            {
                return;
            }
            var statusObj = Session["status"];
            var status = statusObj != null ? (InternalOrderStatus)statusObj : InternalOrderStatus.Blank;
            btn.Visible = EnumUtils.AllowUpdateWWSOrderNumber(status);
        }

        #region autofilter

        // global reference: required for client-side event handlers
        private RangeTemplate rangeTemplate = null;

        private DateSelectorTemplate _createDateTemplate = null;
        private const string OrderPropertyName = "Order";

        protected void grid_AutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
        {
            if (e.Column.FieldName == "TotalPrice")
            {
                var dde = new DropDownEditProperties { EnableClientSideAPI = true };
                rangeTemplate = new RangeTemplate();
                dde.DropDownWindowTemplate = rangeTemplate;
                e.EditorProperties = dde;
            }

            if (e.Column.FieldName == "CreateDate")
            {
                var dde = new DropDownEditProperties { EnableClientSideAPI = true };
                _createDateTemplate = new DateSelectorTemplate("CreateDate");
                dde.DropDownWindowTemplate = _createDateTemplate;
                e.EditorProperties = dde;
            }
        }

        protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "TotalPrice")
            {
                var dde = e.Editor as ASPxDropDownEdit;
                dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}",
                    rangeTemplate.cIdFrom, rangeTemplate.cIdTo);
                dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}",
                    rangeTemplate.cIdFrom, rangeTemplate.cIdTo);
                dde.ReadOnly = true;
            }

            if (e.Column.FieldName == "CreateDate")
            {
                var dde = e.Editor as ASPxDropDownEdit;
                dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyDateFilter(s, \"{0}\", {1}, {2} ); }}",
                    _createDateTemplate.FieldName, _createDateTemplate.CIdFrom, _createDateTemplate.CIdTo);
                dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDateDropDown(s, {0}, {1}); }}",
                    _createDateTemplate.CIdFrom, _createDateTemplate.CIdTo);
                dde.ReadOnly = true;
            }
        }

        protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
        {
            if (e.Value == "|" || e.Value == "")
            {
                return;
            }
            switch (e.Column.FieldName)
            {
                case "TotalPrice":
                    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
                    {
                        Session["TotalPriceFilter"] = e.Value;
                        var dates = e.Value.Split('|');
                        var dateFrom = string.IsNullOrWhiteSpace(dates[0]) ? double.MinValue : Convert.ToDouble(dates[0]);
                        var dateTo = string.IsNullOrWhiteSpace(dates[1]) ? double.MaxValue : Convert.ToDouble(dates[1]);
                        e.Criteria = (new OperandProperty("TotalPrice") >= dateFrom) &
                                     (new OperandProperty("TotalPrice") <= dateTo);
                    }
                    else
                    {
                        if (Session["TotalPriceFilter"] != null)
                        {
                            e.Value = Session["TotalPriceFilter"].ToString();
                        }
                    }
                    break;
                case "Process":
                    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
                    {
                        Session["TotalPriceFilter"] = e.Value;
                        var dates = e.Value.Split('|');
                        var dateFrom = string.IsNullOrWhiteSpace(dates[0]) ? double.MinValue : Convert.ToDouble(dates[0]);
                        var dateTo = string.IsNullOrWhiteSpace(dates[1]) ? double.MaxValue : Convert.ToDouble(dates[1]);
                        e.Criteria = (new OperandProperty("TotalPrice") >= dateFrom) &
                                     (new OperandProperty("TotalPrice") <= dateTo);
                    }
                    else
                    {
                        if (Session["TotalPriceFilter"] != null)
                        {
                            e.Value = Session["TotalPriceFilter"].ToString();
                        }
                    }
                    break;
                case "CreateDate":
                    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
                    {
                        Session["CreateDateFilter"] = e.Value;
                        String[] dates = e.Value.Split('|');
                        DateTime dateFrom = Convert.ToDateTime(dates[0]),
                            dateTo = Convert.ToDateTime(dates[1]);
                        e.Criteria = (new OperandProperty("CreateDate") >= dateFrom) &
                                     (new OperandProperty("CreateDate") <= dateTo.AddDays(1));
                    }
                    else
                    {
                        if (Session["CreateDateFilter"] != null)
                            e.Value = Session["CreateDateFilter"].ToString();
                    }
                    break;
            }
        }

        #endregion autofilter

        protected void btnExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsToResponse();
        }

        protected void CbShawAllOrdersCheked(object sender, EventArgs e)
        {
            grid.DataBind();
        }

        protected void LinqDataSource1_OnSelecting(object sender, LinqServerModeDataSourceSelectEventArgs e)
        {
            var entities = new RoutableOrderDbEntities();
            var query = entities.OrderMonitorings.Select(o => o);
            if (!cbShowAllOrders.Checked)
            {
                var minDate = DateTime.Today.AddDays(-30);
                //query = query.Where(o => o.CreateDate > minDate);
            }

            if (!AuthContext.IsProUser)
            {
                query = query.Where(o => AuthContext.SapCodes.Contains(o.SapCode));
            }
            e.DefaultSorting = "OrderId DESC";
            e.KeyExpression = "OrderId";
            e.QueryableSource = query;
        }

        protected void grid_OnHeaderFilterFillItems(object sender, ASPxGridViewHeaderFilterEventArgs e)
        {
            if (e.Column.FieldName == "Delivery")
            {
                e.AddValue("Доставка", string.Empty, "[Delivery] != null");
                e.AddValue("Самовывоз", string.Empty, "[Delivery] = null");
            }
        }

        protected void grid_Init(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the DataBinding event of the btnAssign control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnAssign_DataBinding(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            if (btn == null)
            {
                return;
            }

            var statusObj = Session["status"];
            var status = statusObj != null ? (InternalOrderStatus)statusObj : InternalOrderStatus.Blank;
            var isAllowChangeComment = EnumUtils.AllowAssingTask(status);

            var isProcessed = Convert.ToBoolean(Session[IsProcessOrderPropertyName]);


            bool taskAssigned = false;
            string orderId = Convert.ToString(Session[OrderIdPropertyName]);
            if (isProcessed && isAllowChangeComment && !string.IsNullOrEmpty(orderId))
            {
                var ticketTasks = DataFacade.GetTicketTasks(orderId);//.Where(task => task.State == TaskState.Created);
                var lastTask = ticketTasks.OrderByDescending(task => task.Created).FirstOrDefault();
                taskAssigned = lastTask != null && string.IsNullOrEmpty(lastTask.AssignedTo) && lastTask.State == TaskState.InProgress;
            }

            btn.Visible = isProcessed && isAllowChangeComment && taskAssigned;
        }
    }
}