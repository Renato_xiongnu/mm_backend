﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;

namespace Orders.Backend.WF.Activities.CustomSettings
{
    [TypeConverter(typeof(ListOfTimeSpanConverter))]
    [SettingsSerializeAs(SettingsSerializeAs.String)]
    public class ListOfTimeSpan:List<TimeSpan>
    {
        public ListOfTimeSpan(IEnumerable<TimeSpan> collection) : base(collection)
        {
        }
    }

    public class ListOfTimeSpanConverter : TypeConverter 
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {           
            if (destinationType == typeof(ListOfTimeSpan))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {            
            var strValue = value as string;
            if(strValue == null)
            {
                return base.ConvertFrom(context, culture, value);
            }
            var values = strValue.Split(';')
                .Select(v=>v.Trim())
                .Where(v=>!string.IsNullOrEmpty(v))
                .Select(TimeSpan.Parse).ToArray();
            var result = new ListOfTimeSpan(values);            
            return result;
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType==typeof(string))
            {
                var arrayOfTimeSpan = value as ListOfTimeSpan;
                if(arrayOfTimeSpan!=null)
                {
                    return string.Join(";", arrayOfTimeSpan.Select(t => t.ToString()).ToArray());
                }                
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }        
    }
}
