﻿using System.Activities.Presentation;
using System.ComponentModel;
using System.Windows;
using Orders.Backend.WF.Activities.TicketTool.Tasks.Design;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{
    [Designer(typeof(CallBackLaterOutcomeActivityDesigner))]
    public sealed class CallBackLaterOutcome : Outcome, IActivityTemplateFactory<Outcome>
    {
        [Browsable(false)]
        public CallAfterField CallMeAfter
        {
            get { return (CallAfterField)this.RequestedFields[0]; }
            set
            {
                if (this.RequestedFields.Count == 0)
                {
                    this.RequestedFields.Add(value);
                }
                else
                {
                    this.RequestedFields[0] = value;
                }
            }
        }

        internal override bool SkipTaskFormValidation
        {
            get { return true; }
        }

        internal static CallBackLaterOutcome NewCallBackLaterOutcome()
        {
            var result = new CallBackLaterOutcome();
            result.RequestedFields.Add(new CallAfterField {Name = Backend_Activities.CallMeLater});
            result.Name = Backend_Activities.CallBackLater;
            result.DisplayName = Backend_Activities.CallBackLater;
            return result;
        }

        public Outcome Create(DependencyObject target, IDataObject dataObject)
        {
            return NewCallBackLaterOutcome();
        }
    }
}
