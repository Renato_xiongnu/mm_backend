﻿using System;
using System.Activities.Presentation;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Orders.Backend.WF.Activities.Proxy.TicketTool;
using Orders.Backend.WF.Activities.TicketTool.Tasks.Design;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{
    [Designer(typeof(ChoiceFieldDesigner))]
    public class ChoiceDataField : RequestedDataField<string>, IActivityTemplateFactory<ChoiceDataField>
    {
        [Browsable(false)]
        public string Options { get; set; }

        public override RequiredField GetRequiredField()
        {
            var options = this.Options.Split(new[] {"\n"}, StringSplitOptions.RemoveEmptyEntries)
                              .Select(s => (object) s)
                              .ToArray();
            var field = new RequiredFieldValueList {Name = this.Name, Type = "Choice", PredefinedValuesList = options};
            if(options.Length > 0)
            {
                field.DefaultValue = options[0].ToString();
            }
            return field;
        }

        public ChoiceDataField Create(DependencyObject target, IDataObject dataObject)
        {
            return new ChoiceDataField { Options = String.Join(Environment.NewLine, "Option1", "Option2", "Option3") };
        }
    }
}
