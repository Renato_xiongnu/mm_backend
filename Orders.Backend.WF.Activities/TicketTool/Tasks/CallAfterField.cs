﻿using System;
using System.ComponentModel;
using Orders.Backend.WF.Activities.TicketTool.Tasks.Design;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{
    [Designer(typeof(CallAfterFieldActivityDesigner))]
    [ToolboxItem(false)]
    public sealed class CallAfterField : RequestedDataField<TimeSpan>
    {
    }
}
