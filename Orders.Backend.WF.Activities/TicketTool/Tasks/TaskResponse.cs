﻿using System.Runtime.Serialization;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{
    [DataContract]
    public class TaskResponse
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool CompleteSuccessful { get; set; }
    }
}
