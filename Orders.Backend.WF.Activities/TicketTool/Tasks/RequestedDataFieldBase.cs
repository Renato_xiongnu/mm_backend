﻿using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{
    public abstract class RequestedDataFieldBase : FieldBase
    {
        [Browsable(false)]
        public InArgument<Dictionary<string, object>> RequestedData { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument arg = new RuntimeArgument(Backend_Activities.Param_RequestedValue, typeof(Dictionary<string, object>), ArgumentDirection.In);
            metadata.Bind(this.RequestedData, arg);
            metadata.AddArgument(arg);
        }

        internal abstract bool Validate(object data);
    }
}