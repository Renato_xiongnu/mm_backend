﻿using System;
using System.Linq;
using System.Activities;
using System.ComponentModel;
using Orders.Backend.WF.Activities.Proxy.TicketTool;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{        
    [ToolboxItem(false)]
    public abstract class FieldBase : NativeActivity
    {        
        [Browsable(false)]
        public Type Type { get; set; }

        [Browsable(false)]
        public string Name { get; set; }

        [Browsable(false)]
        public InArgument<TicketTask> Task { get; set; }

        public virtual RequiredField GetRequiredField()
        {
            return new RequiredField {Name = this.Name, Type = this.Type.FullName};
        }

        protected static bool IsAllowedType(Type t)
        {
            var types = new[] {typeof (RequiredFieldValueList), typeof (DateTime), typeof (decimal), typeof (string), typeof(TimeSpan)};
            return t.IsPrimitive || types.Contains(t);
        }

        protected override void Execute(NativeActivityContext context)
        {            
        }
    }
}
