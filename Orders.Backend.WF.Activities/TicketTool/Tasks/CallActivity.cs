﻿using System;
using System.Activities.Presentation;
using System.Linq;
using System.Windows;
using Orders.Backend.WF.Activities.Tasks.Design;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks 
{
    [Designer(typeof(CallActivityDisigner))]
    public class CallActivity : NativeActivity, IActivityTemplateFactory
    {
        private ActivityAction<int> Call { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<int> Repetition { get; set; }

        [Browsable(false)]
        public TaskActivity Task { get; set; }

        [Browsable(false)]
        public Delay Delay { get; set; }        

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument repetition = new RuntimeArgument(Backend_Activities.Param_Repetition, typeof(int), ArgumentDirection.In, true);
            metadata.Bind(this.Repetition, repetition);
            metadata.AddArgument(repetition);

            var repeatCount = new Variable<int>("RepeatCount");
            var outcome = new Variable<string>("InternalOutcome");
            var repetitionInArg = new DelegateInArgument<int>();
            this.Task.InternalOutcome = new OutArgument<string>(outcome);

            var callBackAfter = new Variable<TimeSpan>("CallBackAfter", TimeSpan.Zero);
            var callBackOutcomes = this.Task.Outcomes.Where(o => o is CallBackLaterOutcome).ToList();
            Variable<string> callBackOutcomeName = new Variable<string>("CallBackOutcomeName");
            switch (callBackOutcomes.Count)
            {
                case 1:
                    ((CallBackLaterOutcome) callBackOutcomes[0]).CallMeAfter.Value = new OutArgument<TimeSpan>(callBackAfter);
                    callBackOutcomeName = new Variable<string>("CallBackOutcomeName", callBackOutcomes[0].Name);
                    break;
                case 0: break;
                default:
                    metadata.AddValidationError(Backend_Activities.Err_OnlyOneCallBackOutcomeAllowed);
                    break;
            }
            
// ReSharper disable ImplicitlyCapturedClosure
            this.Call = new ActivityAction<int>
                {
                    Handler = new DoWhile(c => repeatCount.Get(c) < repetitionInArg.Get(c) && outcome.Get(c) == this.Task.Outcomes[0].Name)
                        {
                            Variables = {repeatCount, outcome},
                            Body = new Sequence
                                {
                                    Activities =
                                        {
                                            new DoWhile(c => !String.IsNullOrEmpty(callBackOutcomeName.Get(c)) && outcome.Get(c) == callBackOutcomeName.Get(c))
                                                {
                                                    Variables = {callBackOutcomeName, callBackAfter},
                                                    Body = new Sequence
                                                        {
                                                            Activities =
                                                                {
                                                                    this.Task,
                                                                    new If(c => !String.IsNullOrEmpty(callBackOutcomeName.Get(c)) && outcome.Get(c) == callBackOutcomeName.Get(c))
                                                                        {
                                                                            Then = new Delay {Duration = new InArgument<TimeSpan>(callBackAfter)}
                                                                        },
                                                                }
                                                        }
                                                },                                           
                                            new Assign<int> {To = new OutArgument<int>(repeatCount), Value = new InArgument<int>(c => repeatCount.Get(c) + 1)},
                                            new If(c => repeatCount.Get(c) < repetitionInArg.Get(c) && outcome.Get(c) == this.Task.Outcomes[0].Name)
                                                {
                                                    Then = this.Delay
                                                }
                                        }
                                }
                        },
                    Argument = repetitionInArg
                };
// ReSharper restore ImplicitlyCapturedClosure
            metadata.AddDelegate(this.Call);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleAction(this.Call, this.Repetition.Get(context));
        }

        public Activity Create(DependencyObject target)
        {
            var task = new TaskActivity();
            task = (TaskActivity) task.Create(target);
            task.Outcomes.Clear();
            task.Outcomes.Add(new Outcome { DisplayName = Backend_Activities.IDidntGetThrough, Name = Backend_Activities.IDidntGetThrough });
            task.Outcomes.Add(CallBackLaterOutcome.NewCallBackLaterOutcome());
            return new CallActivity
                       {
                           Task = task, 
                           Delay = new Delay {Duration = new InArgument<TimeSpan>(TimeSpan.FromMinutes(5))}, 
                           Repetition = new InArgument<int>(1)
                       };
        }
    }

}
