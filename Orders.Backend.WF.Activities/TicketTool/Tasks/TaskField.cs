using System.Activities;
using System.ComponentModel;

namespace Orders.Backend.WF.Activities.TicketTool.Tasks
{
    [ToolboxItem(false)]
    public sealed class TaskField<T> : FieldBase
    {
        [Browsable(false)]
        public InArgument<T> Value { get; set; }        

        protected override void Execute(NativeActivityContext context)
        {
            if (this.Task == null || this.Value == null) return;
            var task = this.Task.Get(context);
            task.GetType().GetProperty(this.Name).SetValue(task, this.Value.Get(context));
        }
    }
}