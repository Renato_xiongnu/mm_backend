﻿namespace Orders.Backend.WF.Activities.TicketTool.Model
{
    public class User
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
