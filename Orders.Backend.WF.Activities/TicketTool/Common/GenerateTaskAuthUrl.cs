﻿using Common.Authentification;
using Orders.Backend.WF.Activities.TicketTool.Common.Design;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.TicketTool.Common
{
    [Designer(typeof(GenerateTaskAuthUrlActivityDesigner))]
    public class GenerateTaskAuthUrl:CodeActivity
    {
        [RequiredArgument]
        public InArgument<string> TaskUrlFormat { get; set; }

        [RequiredArgument]
        public InArgument<string> UserLogin { get; set; }

        [RequiredArgument]
        public InArgument<string> TaskId { get; set; }

        [RequiredArgument]
        public OutArgument<string> TaskUrl { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var taskUrl=AuthService.GenerateTaskAuthUrl(TaskUrlFormat.Get(context), TaskId.Get(context), UserLogin.Get(context));

            TaskUrl.Set(context, taskUrl);
        }
    }
}
