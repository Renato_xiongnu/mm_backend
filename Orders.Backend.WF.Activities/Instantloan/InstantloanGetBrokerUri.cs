﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Instantloan.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Instantloan;
using Orders.Backend.WF.Activities.Proxy.Online;

namespace Orders.Backend.WF.Activities.Instantloan
{
    [Designer(typeof(GetBrokerUriDesigner))]
    public sealed class InstantloanGetBrokerUri : NativeActivity
    {
        private ActivityFunc<Order, string> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        [Browsable(false)]
        public OutArgument<string> BrokerUrl { get; set; }

        public IInstantloan WebService { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>("Order");
            var resultArg = new DelegateOutArgument<string>("Result");

            this.Flow = new ActivityFunc<Order, string>
                {
                    Result = resultArg,
                    Argument = orderArg,
                    Handler = new Failover
                        {
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler = new Assign
                                                {
                                                    To = new OutArgument<string>(resultArg),
                                                    Value =
                                                        new InArgument<string>(
                                                            c => (WebService ?? new InstantloanClient()).GetBrokerUri(
                                                                orderArg.Get(c).ToBrokerInfo()))
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = this.Order.Get(context);

            context.ScheduleFunc(this.Flow, order);
        }
    }
}
