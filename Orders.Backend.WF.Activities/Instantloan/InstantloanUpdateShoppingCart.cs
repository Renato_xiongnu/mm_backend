﻿using System;
using System.Activities;
using System.Activities.Presentation.Converters;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Instantloan.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Online;
using Orders.Backend.WF.Activities.Proxy.Instantloan;
using Orders.Backend.WF.Activities.Proxy.TicketTool;

namespace Orders.Backend.WF.Activities.Instantloan
{
    [Designer(typeof(UpdateShoppingCartDesigner))]
    public sealed class InstantloanUpdateShoppingCart:NativeActivity
    {
        private ActivityAction<Order> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        public IInstantloan WebService { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>("Order");
            var client = new Variable<IInstantloan>("InstantloanClient");
            this.Flow = new ActivityAction<Order>
                {
                    Argument = orderArg,
                    Handler = new Failover
                        {
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler = new Sequence
                                                {
                                                    Variables = { client },
                                                    Activities =
                                                        {
                                                            new Assign
                                                                {
                                                                    To = new OutArgument<IInstantloan>(client),
                                                                    Value =
                                                                        new InArgument<IInstantloan>(c=> WebService ?? new InstantloanClient())
                                                                },
                                                            new InvokeMethod
                                                                {
                                                                    MethodName = "UpdateShoppingCart",
                                                                    Parameters = {new InArgument<ShoppingCartInfo>(c=>orderArg.Get(c).ToShoppingCartInfo())},
                                                                    TargetObject =
                                                                        new InArgument<IInstantloan>(client)
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = this.Order.Get(context);
            context.ScheduleAction(this.Flow, order);
        }
    }
}
