﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Instantloan.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Instantloan;
using Orders.Backend.WF.Activities.Proxy.Online;

namespace Orders.Backend.WF.Activities.Instantloan
{
    [Designer(typeof(SendRedirectUrlDesigner))]
    public sealed class InstantloanSendRedirectUrl : NativeActivity
    {
        private ActivityAction<Order> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        public IInstantloan WebService { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>("Order");
            var client = new Variable<IInstantloan>("InstantloanClient");

            this.Flow = new ActivityAction<Order>
                {
                    Argument = orderArg,
                    Handler = new Failover
                        {
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler = new Sequence
                                                {
                                                    Variables = {client},
                                                    Activities =
                                                        {
                                                            new Assign
                                                                {
                                                                    To = new OutArgument<IInstantloan>(client),
                                                                    Value =
                                                                        new InArgument<IInstantloan>(
                                                                            c => WebService ?? new InstantloanClient())
                                                                },
                                                            new InvokeMethod
                                                                {
                                                                    MethodName = "UpdateUriForRedirection",
                                                                    Parameters =
                                                                        {
                                                                            new InArgument<UriInfo>(
                                                                                c => orderArg.Get(c).ToUriInfo())
                                                                        },
                                                                    TargetObject =
                                                                        new InArgument<IInstantloan>(client)
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = this.Order.Get(context);
            context.ScheduleAction(this.Flow, order);
        }
    }
}
