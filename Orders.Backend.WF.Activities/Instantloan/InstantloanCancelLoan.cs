﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Instantloan.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Instantloan;

namespace Orders.Backend.WF.Activities.Instantloan
{
    [Designer(typeof(CancelLoanDesigner))]
    public sealed class InstantloanCancelLoan : NativeActivity
    {
        private ActivityAction<Order,string> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<string> RejectReason { get; set; }

        public IInstantloan WebService { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var rejectReason = new RuntimeArgument(Backend_Activities.Param_RejectReason, typeof(string), ArgumentDirection.In, true);
            metadata.Bind(this.RejectReason, rejectReason);
            metadata.AddArgument(rejectReason);

            var orderArg = new DelegateInArgument<Order>("Order");
            var rejectReasonArg = new DelegateInArgument<string>("RejectReason");
            var client = new Variable<IInstantloan>("InstantloanClient");
            
            this.Flow = new ActivityAction<Order,string>
                {
                    Argument1 = orderArg,
                    Argument2 = rejectReasonArg,
                    Handler = new Failover
                        {
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler = new Sequence
                                                {
                                                    Variables = {client},
                                                    Activities =
                                                        {
                                                            new Assign
                                                                {
                                                                    To = new OutArgument<IInstantloan>(client),
                                                                    Value =
                                                                        new InArgument<IInstantloan>(
                                                                            c => WebService ?? new InstantloanClient())
                                                                },
                                                            new InvokeMethod
                                                                {
                                                                    MethodName = "CancelLoan",
                                                                    Parameters =
                                                                        {
                                                                            new InArgument<CancelLoanInfo>(
                                                                                c =>
                                                                                new CancelLoanInfo
                                                                                    {
                                                                                        OrderId = orderArg.Get(c).Id,
                                                                                        ReasonText = rejectReasonArg.Get(c)
                                                                                    })
                                                                        },
                                                                    TargetObject =
                                                                        new InArgument<IInstantloan>(client)
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = this.Order.Get(context);
            var rejectReason = this.RejectReason.Get(context);
            context.ScheduleAction(this.Flow, order, rejectReason);
        }
    }
}
