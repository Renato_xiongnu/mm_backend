﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.ComponentModel;
using Orders.Backend.WF.Activities.Common.Design;
using Orders.Backend.WF.Activities.Properties;

namespace Orders.Backend.WF.Activities.Common
{
    [Designer(typeof(CallingElvisActivityDesigner))]
    public sealed class CallingElvis : NativeActivity, IActivityTemplateFactory
    {
        public Activity Action { get; set; }
        public Activity Success { get; set; }
        public Activity Fail { get; set; }
        public Activity<bool> Condition { get; set; }
        public Delay WaitFor { get; set; }
        public Delay Periodicity { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            if (this.Action != null)
            {
                metadata.AddChild(this.Action);
            }
            else
            {
                metadata.AddValidationError(Backend_Activities.Err_YouMustSpecifyActivity);
            }
            metadata.AddChild(this.WaitFor);
            metadata.AddChild(this.Periodicity);
            if (this.Condition != null)
            {
                metadata.AddChild(this.Condition);
            }
            if (this.Success != null)
            {
                metadata.AddChild(this.Success);
            }
            if (this.Fail != null)
            {
                metadata.AddChild(this.Fail);
            }
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleActivity(this.WaitFor, this.OnWaitCompleted);
            context.ScheduleActivity(this.Action, this.OnActionCompleted, this.OnActionFailed);
        }

        private void OnActionCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;

            if (this.Condition != null)
            {
                context.ScheduleActivity(this.Condition, this.OnConditionCompleted);
            }
            else
            {
                context.ScheduleActivity(this.Periodicity, this.OnDelayComplete);
            }
        }

        private void OnConditionCompleted(NativeActivityContext context, ActivityInstance completedinstance, bool result)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;

            if (result)
            {
                context.CancelChildren();
                if (this.Success != null)
                {
                    context.ScheduleActivity(this.Success);
                }
            }
            else
            {
                context.ScheduleActivity(this.Periodicity, this.OnDelayComplete);
            }
        }

        private void OnDelayComplete(NativeActivityContext context, ActivityInstance completedinstance)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;
            context.ScheduleActivity(this.Action, this.OnActionCompleted, this.OnActionFailed);
        }

        private void OnWaitCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;
            this.HandleFault(context);
        }

        private void OnActionFailed(NativeActivityFaultContext faultcontext, Exception propagatedexception, ActivityInstance propagatedfrom)
        {
            faultcontext.HandleFault();
            this.HandleFault(faultcontext);
        }

        private void HandleFault(NativeActivityContext context)
        {
            context.CancelChildren();
            if (this.Fail != null)
            {
                context.ScheduleActivity(this.Fail);
            }
        }

        public Activity Create(System.Windows.DependencyObject target)
        {
            return new CallingElvis
                {
                    WaitFor = new Delay {Duration = TimeSpan.FromMinutes(Settings.Default.LongOperationDefaultTimeout)},
                    Periodicity = new Delay{Duration = TimeSpan.FromMinutes(Settings.Default.LongOperationDefaultPeriodicity)}
                };
        }
    }
}
