﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Common.Interfaces
{
    public interface ICompletable
    {        
        string CompleteOperationName { get; }        
    }
}
