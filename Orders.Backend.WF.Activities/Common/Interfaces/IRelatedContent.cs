﻿using Orders.Backend.WF.Activities.Model;
namespace Orders.Backend.WF.Activities.Common.Interfaces
{
    public interface IRelatedContent
    {
        string GetUrl();

        Task CurrentTask { get; set; }
    }
}
