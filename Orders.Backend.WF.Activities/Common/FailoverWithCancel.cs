﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities.Extensions;
using MMS.Activities.Properties;
using MMS.Activities;

namespace Orders.Backend.WF.Activities.Common
{
    [ToolboxItem(false)]
    public class FailoverWithCancel : Failover
    {
        protected override void HandleFault(NativeActivityFaultContext faultContext, Exception propagatedException, ActivityInstance propagatedFrom)
        {
            faultContext.CancelChild(propagatedFrom);
            base.HandleFault(faultContext, propagatedException, propagatedFrom);
        }
    }
}