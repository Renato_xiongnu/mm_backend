﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Common
{
    public interface IReceivable
    {
        
        string Name { get; }

        string CompleteOperationName { get; }

        ICollection<string> Outcomes { get; }
    }
}
