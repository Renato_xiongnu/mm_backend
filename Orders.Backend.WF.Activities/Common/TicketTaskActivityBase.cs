using System.Activities;
using Orders.Backend.WF.Activities.Proxy.TicketTool;

namespace Orders.Backend.WF.Activities.Common
{
    public abstract class TicketTaskActivityBase<T1, T2> : ServiceActivityBase<string, T2> where T1 : TicketTask where T2 : ITicketToolService
    {   
        [RequiredArgument]
        public InArgument<T1> TicketTask { get; set; }
    }
}