﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activities;

using Orders.Backend.WF.Activities.Proxy.TicketTool;

namespace Orders.Backend.WF.Activities.Common
{
    public interface ITicketToolWFContext<T> //where T : ITicketToolService
    {
        CorrelationHandle CorrelationHandle { get; }

        T TicketService { get; }
    }
}
