﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Common.Design;

namespace Orders.Backend.WF.Activities.Common
{
    [Designer(typeof(TodoActivityDesigner))]
    public class Todo:NativeActivity
    {
        public InArgument<bool> ThrowIfExists { get; set; }

        public string TodoText { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {

        }

        protected override void Execute(NativeActivityContext context)
        {
        }
    }
}
