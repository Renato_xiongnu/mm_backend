﻿using System;
using System.Activities;
using System.Activities.Expressions;
using System.Activities.Presentation.Model;
using System.Globalization;
using System.Windows.Data;
using Microsoft.CSharp.Activities;

namespace Orders.Backend.WF.Activities.Common.ValueConverters
{
    public class ComboBoxItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var modelItem = value as ModelItem;
            if(modelItem != null)
            {
                var inArgument = modelItem.GetCurrentValue() as InArgument<string>;

                if(inArgument != null)
                {
                    var expression = inArgument.Expression;
                    var csexpression = expression as CSharpValue<string>;
                    var literal = expression as Literal<string>;

                    if(literal != null)
                    {
                        return "\"" + literal.Value + "\"";
                    }
                    if(csexpression != null)
                    {
                        return csexpression.ExpressionText.Replace("\"", String.Empty);
                    }
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var expression = "\"" + value + "\"";
            return new InArgument<string>(new CSharpValue<string>(expression));
        }
    }
}