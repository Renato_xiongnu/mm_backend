using System;
using System.Activities;
using System.Activities.Expressions;
using System.Globalization;
using System.Windows.Data;

namespace Orders.Backend.WF.Activities.Common.ValueConverters
{
    public class InArgumentBoolConverter : IValueConverter
    {
        public object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var argument = value as InArgument<bool>;
            if(argument == null)
            {
                return null;
            }
            var expression = argument.Expression;

            var literal = expression as Literal<bool>;
            if(literal != null)
            {
                return literal.Value;
            }

            return null;
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            if(value is bool)
            {
                return new InArgument<bool>(new Literal<bool>((bool) value));
            }

            return null;
        }
    }
}