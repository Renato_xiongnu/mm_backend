﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;


namespace Orders.Backend.WF.Activities.Common
{
    public class CreateTicketQuery<T> : MessageQuery
    {
        private const string NS = "http://schemas.microsoft.com/2003/10/Serialization/";

        public override TResult Evaluate<TResult>(Message message)
        {
            Debugger.Break();
            var x = XName.Get(this.GetTypeName(), NS);
            XDocument doc;
            using (var sr = new StringReader(message.GetReaderAtBodyContents().ReadOuterXml()))
                doc = XDocument.Load(sr);

            foreach (var result in doc.Descendants().Where(c => c.Name == x))
            {
                return (TResult)(object)result.Value;
            }
            return (TResult)(object)(null);
        }

        private string GetTypeName()
        {
            var dcs = new DataContractSerializer(typeof(T));
            object dataContract = dcs.GetType().GetProperty("RootContract", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(dcs);
            return dataContract.GetType().GetProperty("Name", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(dataContract).ToString();
        }

        public override TResult Evaluate<TResult>(MessageBuffer buffer)
        {
            Message message = buffer.CreateMessage();
            return this.Evaluate<TResult>(message);
        }
    }
}
