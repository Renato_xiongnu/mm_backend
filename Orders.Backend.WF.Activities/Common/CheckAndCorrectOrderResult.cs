﻿namespace Orders.Backend.WF.Activities.Common
{
    public class CheckAndCorrectOrderResult
    {
        public bool IsValid { get; set; }

        public string RejectReason { get; set; }        
    }
}