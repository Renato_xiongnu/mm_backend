﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using MMS.Activities;

namespace Orders.Backend.WF.Activities.Common
{
    public class ThrowableFailover : Failover
    {
        public ICollection<Type> ExceptionsToThrow { get; set; }

        public bool HandleSubExceptions { get; set; }

        protected override void HandleFault(NativeActivityFaultContext faultContext, Exception propagatedException, ActivityInstance propagatedFrom)
        {
            var needHandle = HandleSubExceptions
                ? !ExceptionsToThrow.Any(el => el.IsInstanceOfType(propagatedException))
                : ExceptionsToThrow.All(el => el != propagatedException.GetType());
            if (needHandle)
            {
                base.HandleFault(faultContext, propagatedException, propagatedFrom);
            }            
        }
    }
}