﻿namespace Orders.Backend.WF.Activities.Common
{
    public class OperationResult
    {
        public string Message { get; set; }
        public bool CompletedSuccessfully { get; set; }
    }
}
