﻿using System.IO;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace Orders.Backend.WF.Activities.Common
{
    public class TicketIdQuery : MessageQuery
    {
        public override TResult Evaluate<TResult>(Message message)
        {
            XmlDocument doc = new XmlDocument();
            using (StringReader sr = new StringReader(message.GetReaderAtBodyContents().ReadOuterXml()))
                doc.Load(sr);
            if (doc.DocumentElement != null)
                return (TResult) (object) doc.DocumentElement.FirstChild.InnerText;
            return default(TResult);
        }

        public override TResult Evaluate<TResult>(MessageBuffer buffer)
        {
            Message message = buffer.CreateMessage();
            return this.Evaluate<TResult>(message);
        }
    }
}
