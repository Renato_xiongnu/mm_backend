﻿using System.Activities;
using System.Activities.Statements;
using Orders.Backend.WF.Activities.Proxy.TicketTool;

namespace Orders.Backend.WF.Activities.Common
{
    public abstract class ServiceActivityBase<T1, T2> : NativeActivity<T1> where T2 : ITicketToolService
    {
        [RequiredArgument]
        public InArgument<ITicketToolWFContext<T2>> CurrentContext { get; set; }
        
        private Activity _internalSequence;
        protected Activity InternalSequence()
        {
            return this._internalSequence ?? (this._internalSequence = this.GetNewChild());
        }

        protected abstract Sequence GetNewChild();

        protected override void Execute(NativeActivityContext context)
        {            
            context.ScheduleActivity(this.InternalSequence());
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            metadata.AddImplementationChild(this.InternalSequence());
            base.CacheMetadata(metadata);
        }        
    }

    public abstract class ServiceActivityBase<T> : ServiceActivityBase<object, T> where T : ITicketToolService
    {
 
    }
}
