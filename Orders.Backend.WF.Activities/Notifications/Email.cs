﻿using System.Activities.Presentation;
using System.Windows;
using Orders.Backend.WF.Activities.Notifications.Model;

namespace Orders.Backend.WF.Activities.Notifications
{
    public sealed class Email : Delivery, IActivityTemplateFactory<Email>
    {
        public Email Create(DependencyObject target, IDataObject dataObject)
        {
            return new Email { DeliveryType = DeliveryType.EMail, DeliveryFields = GetDeliveryFields(target, DeliveryType.EMail) };
        }
    }
}
