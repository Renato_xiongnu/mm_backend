﻿using System.Activities.Presentation;
using System.Windows;
using Orders.Backend.WF.Activities.Notifications.Model;

namespace Orders.Backend.WF.Activities.Notifications
{
    public sealed class IPhone : Delivery, IActivityTemplateFactory<IPhone>
    {
        public IPhone Create(DependencyObject target, IDataObject dataObject)
        {
            return new IPhone { DeliveryType = DeliveryType.IPhone, DeliveryFields = GetDeliveryFields(target, DeliveryType.IPhone) };
        }
    }
}
