﻿namespace Orders.Backend.WF.Activities.Notifications.Model
{
    public enum DeliveryType
    {
        Site = 0,
        EMail = 1,
        SMS = 2,
        IPhone = 3,
    }
}
