﻿using System.Collections.ObjectModel;

namespace Orders.Backend.WF.Activities.Notifications.Model
{
    public class Event
    {
        public Event()
        {
            this.Deliveries = new Collection<Delivery>();
        }

        public string Name { get; set; }
        public string Provider { get; set; }
        public Collection<Delivery> Deliveries { get; set; }
    }
}
