﻿using System;
using Orders.Backend.WF.Activities.Notifications.Model;

namespace Orders.Backend.WF.Activities.Notifications.Design
{
    // Interaction logic for DeliveryActivityDesigner.xaml
    public partial class DeliveryActivityDesigner
    {
        public DeliveryActivityDesigner()
        {
            InitializeComponent();
        }

        public string[] Channels
        {
            get { return Enum.GetNames(typeof (DeliveryType)); }
        }
    }
}
