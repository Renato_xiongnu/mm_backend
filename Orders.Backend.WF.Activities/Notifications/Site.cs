﻿using System.Activities.Presentation;
using System.Windows;
using Orders.Backend.WF.Activities.Notifications.Model;

namespace Orders.Backend.WF.Activities.Notifications
{
    public sealed class Site : Delivery, IActivityTemplateFactory<Site>
    {
        public Site Create(DependencyObject target, IDataObject dataObject)
        {
            return new Site { DeliveryType = DeliveryType.Site, DeliveryFields = GetDeliveryFields(target, DeliveryType.Site) };
        }
    }
}
