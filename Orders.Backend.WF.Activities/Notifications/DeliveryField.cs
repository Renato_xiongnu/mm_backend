﻿using System.Activities;
using System.ComponentModel;
using Orders.Backend.WF.Activities.Notifications.Model;

namespace Orders.Backend.WF.Activities.Notifications
{
    public sealed class DeliveryField : CodeActivity
    {
        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Model.Delivery> Delivery { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<string> Value { get; set; }

        [Browsable(false)]
        public string Name { get; set; }

        [Browsable(false)]
        public string Description { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            if (this.Value == null) return;
            this.Delivery.Get(context).Parameters.Add(
                new DeliveryParameter {Name = this.Name, Value = this.Value.Get(context)});
        }
    }
}