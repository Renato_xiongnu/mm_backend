﻿using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Designer;
using Orders.Backend.WF.Activities.Designer.Common;
using Orders.Backend.WF.Activities.Proxy.TicketTool;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Orders.Backend.WF.Activities
{

    [Designer(typeof(PhoneCallActivityDesigner))]
    public class PhoneCallTaskActivity : ServiceActivityBase<ITicketToolService>, IReceivable    
    {
        public static readonly string DefaultOutcome = "Didn't call";

        public string Name
        {
            get { return this.DisplayName; }
        }

        public string CompleteOperationName
        {
            get { return TicketToolWFContext.GetCompleteOperationName(this.DisplayName); }
        }

        public ICollection<string> Outcomes
        {
            get
            {
                return OutcomeActivities.Select(t => t.Name).ToList();
            }
        }

        public ObservableCollection<OutcomeActivity> OutcomeActivities { get; set; } 

        public PhoneCallTaskActivity()
        {
            OutcomeActivities = new ObservableCollection<OutcomeActivity>();

            OutcomeActivities.Add(new OutcomeActivity
                                      {
                Name = DefaultOutcome,
                NeedComments = true
            });

            OutcomeActivities.CollectionChanged += Out_CollectionChanged;

            this.RepeatCount = 1;
        }

        void Out_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Replace:
                    var defaults = OutcomeActivities.Where(t=>t.Name == DefaultOutcome).ToList();
                    if (OutcomeActivities.Any(t => t.Name == DefaultOutcome))
                        OutcomeActivities.Remove(defaults.First());
                    break;
            }
        }               

        public int RepeatCount { get; set; }

        protected override Sequence GetNewChild()
        {
            var outcome = new Variable<string>();
            var currentRepeatCount = new Variable<int> { Default = this.RepeatCount };

            return new Sequence
                       {
                           Variables = {currentRepeatCount, outcome},
                           Activities =
                               {
                                   new DoWhile(c => currentRepeatCount.Get(c) <= RepeatCount && outcome.Get(c) == DefaultOutcome)
                                       {
                                           Body = new Sequence
                                                      {
                                                          Activities =
                                                              {
                                                                  new CreateTicketTaskActivity<PhoneCallTask>
                                                                      {
                                                                          CurrentContext = new InArgument<ITicketToolWFContext<ITicketToolService>>(c => this.CurrentContext.Get(c)),
                                                                          TicketTask = new InArgument<PhoneCallTask>(c => new PhoneCallTask {RepetitionsNumber = currentRepeatCount.Get(c)}),
                                                                          Outcome = new OutArgument<string>(outcome),
                                                                          CompleteOperationName = this.CompleteOperationName
                                                                      },
                                                                  new Assign<int> {To = currentRepeatCount, Value = new InArgument<int>(c => currentRepeatCount.Get(c) + 1)}
                                                              }
                                                      }
                                       }
                               }
                       };
        }      
    }
}
