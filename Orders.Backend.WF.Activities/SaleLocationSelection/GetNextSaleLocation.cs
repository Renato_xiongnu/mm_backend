﻿using System;
using MMS.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.SaleLocationSelection;

namespace Orders.Backend.WF.Activities.SaleLocationSelection
{
    public class GetNextSaleLocation : ExecuteExternalService<OrderInfo, string>
    {
        protected override ExecutingResult<string> ExecuteService(OrderInfo workitem)
        {
            using(var client = new SaleLocationServiceClient())
            {
                var result = client.GetSaleLocation(workitem);
                if(result.ReturnCode==ReturnCode.Error)
                {
                    throw new ApplicationException(result.ErrorMessage);
                }
                return new ExecutingResult<string>
                {
                    Success = true,
                    Result = result.SapCode
                };
            }
        }
    }
}
