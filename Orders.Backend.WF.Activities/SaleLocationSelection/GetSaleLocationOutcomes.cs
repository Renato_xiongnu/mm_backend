﻿using System;
using System.Linq;
using MMS.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.SaleLocationSelection;

namespace Orders.Backend.WF.Activities.SaleLocationSelection
{
    public class GetSaleLocationOutcomes : ExecuteExternalService<string,Model.SaleLocationOutcome[]>
    {
        protected override ExecutingResult<Model.SaleLocationOutcome[]> ExecuteService(string workitem)
        {
            using (var client = new SaleLocationServiceClient())
            {
                var result = client.GetSaleLocationOutcomes(workitem);
                if (result.ReturnCode == ReturnCode.Error)
                {
                    throw new ApplicationException(result.ErrorMessage);
                }
                return new ExecutingResult<Model.SaleLocationOutcome[]>
                {
                    Success = true,
                    Result = result.Result == null
                        ? new Model.SaleLocationOutcome[0]
                        : result.Result.Select(sl => new Model.SaleLocationOutcome
                        {
                            Outcome = sl.Outcome,
                            Performer = sl.Performer,
                            SapCode = sl.SapCode,
                            Created=sl.Created
                        }).ToArray()
                };
            }
        }
    }
}
