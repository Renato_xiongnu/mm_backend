﻿using System;
using System.Linq;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.SaleLocationSelection;
using ShippingMethod = Orders.Backend.WF.Activities.Model.ShippingMethod;

namespace Orders.Backend.WF.Activities.SaleLocationSelection
{
    public static class Extensions
    {
        public static Proxy.SaleLocationSelection.SaleLocationOutcome ToSaleLocationSelectionSaleLocationOutcome(this Order order, string outcome, string performer)
        {
            return order.ToSaleLocationSelectionSaleLocationOutcome(outcome, performer, order.Store.SapCode);
        }

        public static Proxy.SaleLocationSelection.SaleLocationOutcome ToSaleLocationSelectionSaleLocationOutcome(this Order order, string outcome, string performer, string sapCode)
        {
            sapCode = string.IsNullOrEmpty(sapCode) ? order.Store.SapCode : sapCode;
            return new Proxy.SaleLocationSelection.SaleLocationOutcome
            {
                OrderId = order.Id,
                SapCode = sapCode,
                Outcome = outcome,
                Performer = performer,
                Created = DateTime.UtcNow
            };
        }

        public static OrderInfo ToSaleLocationSelectionOrderInfo(this Order order)
        {
            var freeDeliveryArticleId = order.Store.FreeDeliveryArticleId ?? 0;
            var paidDeliveryArticleId = order.Store.PaidDeliveryArticleId ?? 0;
            return new OrderInfo
            {
                OrderId = order.Id,
                City = order.Store.CityName,
                HasDelivery = order.Delivery != null && order.Delivery.HasDelivery,
                
                OrderLines = order.Items
                    .Where(oi => oi.ArticleProductType == ArticleProductType.Product
                                 || oi.ArticleProductType == ArticleProductType.Set)
                    .Select(o => new OrderLine
                    {
                        Article = long.Parse(o.Number),
                        Quantity = o.Quantity
                    })
                    .Where(o => o.Article != freeDeliveryArticleId && o.Article != paidDeliveryArticleId)
                    .ToArray(),

                    ShippingMethod = order.Delivery != null ? order.Delivery.ShippingMethod.ToShippingMethod() : null
            };
        }

        public static Proxy.SaleLocationSelection.ShippingMethod? ToShippingMethod(this ShippingMethod shippingMethod)
        {
            switch (shippingMethod)
            {
                case  ShippingMethod.Delivery:
                    return Proxy.SaleLocationSelection.ShippingMethod.Delivery;

                case ShippingMethod.PickupExternal:
                    return Proxy.SaleLocationSelection.ShippingMethod.PickupExternal;

                case ShippingMethod.PickupShop:
                    return Proxy.SaleLocationSelection.ShippingMethod.PickupShop;
            }

            return null;
        }
    }
}