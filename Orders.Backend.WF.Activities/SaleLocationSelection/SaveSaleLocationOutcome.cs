﻿using System;
using MMS.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.SaleLocationSelection;

namespace Orders.Backend.WF.Activities.SaleLocationSelection
{
    public class SaveSaleLocationOutcome : ExecuteExternalService<SaleLocationOutcome, bool>
    {
        protected override ExecutingResult<bool> ExecuteService(SaleLocationOutcome workitem)
        {
            using (var client = new SaleLocationServiceClient())
            {
                var result = client.SaveSaleLocationOutcome(workitem);
                if(result.ReturnCode==ReturnCode.Error)
                {
                    throw new ApplicationException(result.ErrorMessage);
                }
                return new ExecutingResult<bool>
                {
                    Success = true,
                    Result = true
                };
            }
        }
    }
}