﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Types
{
    public enum OrderPickUpStates
    {
        Created = 1,
        NotifyCustomer = 2,
        Approved = 3,
        Rejected = 4,

        Cancled = 50,

        Closed = 100
    }


    public enum OrderDeliveryCashStates
    {
        Created = 1,
    }

    public enum OrderDeliveryOnlineStates
    {
        Created = 1,
    }
}
