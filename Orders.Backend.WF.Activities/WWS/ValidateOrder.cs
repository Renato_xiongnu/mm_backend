﻿using System;
using System.Activities;
using System.ComponentModel;
using MMS.Activities.Extensions;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.WWS
{
    public sealed class ValidateOrder : CodeActivity<bool>
    {
        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        protected override bool Execute(CodeActivityContext context)
        {
            try
            {
                var processOrder = Order.Get(context);
                // ReSharper disable UnusedVariable
                var order = processOrder.ToOrderInfo();
                // ReSharper restore UnusedVariable
                if(processOrder.Store.IsVirtual && !processOrder.Delivery.HasDelivery)
                {
                    throw new ValidationException("Нельзя создать резерв в виртуальном магазине");
                }
                Result.Set(context, true);
                return true;
            }
            catch(Exception ex)
            {
                context.LogWarn("ValidateOrder exception", ex);
                Result.Set(context, false);
                return false;
            }
        }
    }
}