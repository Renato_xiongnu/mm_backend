using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel.Activities;
using MMS.Activities;
using MMS.Activities.Properties;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online;
using Orders.Backend.WF.Activities.Stores;
using Orders.Backend.WF.Activities.WWS.Design;
using TicketTool.Activities.Tasks;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(CheckAndCorrectOrderActivityDesigner))]
    public sealed class CheckAndCorrectOrder : NativeActivity
    {
        private ActivityFunc<Order, CorrelationHandle, string, CheckAndCorrectOrderResult> CheckOrderFunc { get; set; }

        private Delay Delay { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<string> TicketId { get; set; }

        [Browsable(false)]
        public OutArgument<CheckAndCorrectOrderResult> OperationResult { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var result = new DelegateOutArgument<CheckAndCorrectOrderResult>();
            var orderArg = new DelegateInArgument<Order>();
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var ticketIdArg = new DelegateInArgument<string>();            

            var isValid = new Variable<bool>();
            var outcome = new Variable<string>();
            var rejectReason = new Variable<string>();

            var handler =
                new Sequence
                {
                    Variables = { isValid, outcome, rejectReason },
                    Activities =
                    {
                        new DoWhile(c => !isValid.Get(c) && outcome.Get(c) != Backend_Activities.Outcome_CancelOrder)
                        {
                            Body = new Sequence
                            {
                                Activities =
                                {
                                    new RefreshOrder {Order = orderArg},                                    
                                    new ValidateOrder {Order = orderArg, Result = new OutArgument<bool>(isValid)},
                                    new If(c => !isValid.Get(c))
                                    {
                                        Then = new TaskActivity
                                        {
                                            DisplayName = "Check And Correct Order",
                                            Outcome = new OutArgument<string>(outcome),
                                            Outcomes =
                                            {
                                                new Outcome {Name = Backend_Activities.Outcome_Checked},
                                                new Outcome
                                                {
                                                    Name = Backend_Activities.Outcome_CancelOrder,
                                                    RequestedFields = 
                                                    {
                                                        new ChoiceDataField
                                                        {
                                                            Name = Backend_Activities.Outcome_CancelOrder_Reason,
                                                            Options =  Backend_Activities.Outcome_CancelOrder_Options,
                                                            Value = new OutArgument<string>(c=>rejectReason.Get(c)),                                                            
                                                        }
                                                    }
                                                },
                                            },
                                            CorrelationHandle = handleArg,
                                            Fields =
                                            {
                                                new TaskField<string>
                                                {
                                                    Name = "Name",
                                                    Value =
                                                        new InArgument<string>(
                                                            Backend_Activities.FieldName_CheckAndCorrectOrder)
                                                },
                                                new TaskField<string>
                                                {
                                                    Name = "Description",
                                                    Value =
                                                        new InArgument<string>(
                                                            Backend_Activities.FieldDescription_CheckAndCorrectOrder)
                                                },
                                                new TaskField<string> {Name = "TicketId", Value = ticketIdArg}
                                            }
                                        }
                                    },
                                }
                            }
                        },
                        new Assign
                        {
                            To = new OutArgument<CheckAndCorrectOrderResult>(result),
                            Value = new InArgument<CheckAndCorrectOrderResult>(c=>new CheckAndCorrectOrderResult
                            {
                                IsValid = isValid.Get(c),
                                RejectReason = rejectReason.Get(c)
                            })
                        }
                    }
                };

            var failover = new Failover
            {
                DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                Delegate = new InvokeDelegate
                {
                    Delegate = new ActivityAction
                    {
                        Handler = handler
                    }
                }
            };
            CheckOrderFunc = new ActivityFunc<Order, CorrelationHandle, string, CheckAndCorrectOrderResult>
            {
                Argument1 = orderArg,
                Argument2 = handleArg,
                Argument3 = ticketIdArg,
                Result = result,
                Handler = failover
            };
            metadata.AddDelegate(CheckOrderFunc);

            Delay = new Delay { Duration = TimeSpan.FromSeconds(Settings.Default.FailoverTimeout) };
            metadata.AddImplementationChild(Delay);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var handle = new RuntimeArgument(Backend_Activities.Param_CorrelationHandle, typeof(CorrelationHandle),
                ArgumentDirection.In, true);
            metadata.Bind(CorrelationHandle, handle);
            metadata.AddArgument(handle);

            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);

            var ticketId = new RuntimeArgument(Backend_Activities.Param_TicketId, typeof(string), ArgumentDirection.In,
                true);
            metadata.Bind(TicketId, ticketId);
            metadata.AddArgument(ticketId);

            var operationResult = new RuntimeArgument(Backend_Activities.Param_OperationResult, typeof(CheckAndCorrectOrderResult),
                ArgumentDirection.Out);
            metadata.Bind(OperationResult, operationResult);
            metadata.AddArgument(operationResult);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(CheckOrderFunc, Order.Get(context), CorrelationHandle.Get(context),
                TicketId.Get(context), OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, CheckAndCorrectOrderResult result)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }            
            OperationResult.Set(context, result);
        }
    }
}