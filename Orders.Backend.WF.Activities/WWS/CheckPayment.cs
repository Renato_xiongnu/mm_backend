﻿using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.WWS;
using Orders.Backend.WF.Activities.WWS.Design;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(CheckPaymentActivityDesigner))]
    public sealed class CheckPayment : NativeActivity<bool>
    {
        private ActivityFunc<Order, GetSalesOrderResult> Flow { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        [Browsable(false)]
        public OutArgument<bool> Canceled { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            RuntimeArgument result = new RuntimeArgument(Backend_Activities.Param_Result, typeof(bool), ArgumentDirection.Out);
            metadata.Bind(this.Result, result);
            metadata.AddArgument(result);

            RuntimeArgument canceled = new RuntimeArgument(Backend_Activities.Param_Canceled, typeof(bool), ArgumentDirection.Out);
            metadata.Bind(this.Canceled, canceled);
            metadata.AddArgument(canceled);
            
            var orderArg = new DelegateInArgument<Order>("Order");
            var resultArg = new DelegateOutArgument<GetSalesOrderResult>("Result");
            this.Flow = new ActivityFunc<Order, GetSalesOrderResult>
                {
                    Argument = orderArg,
                    Result = resultArg,
                    Handler = new Failover
                        {
                            DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler = new Assign
                                                {
                                                    To = new OutArgument<GetSalesOrderResult>(resultArg),
                                                    Value = new InArgument<GetSalesOrderResult>(c => new SalesServiceClient().GetSalesOrder(orderArg.Get(c).ToOrderNumberInfo()))
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(this.Flow, this.Order.Get(context), this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, GetSalesOrderResult result)
        {
            if (result == null) return;
            if (result.OrderHeader == null) return;
            
            
            this.Result.Set(context, result.OrderHeader.OrderStatus == SalesOrderStatus.Paid);
            this.Canceled.Set(context, result.OrderHeader.OrderStatus == SalesOrderStatus.Canceled);
        }
    }
}
