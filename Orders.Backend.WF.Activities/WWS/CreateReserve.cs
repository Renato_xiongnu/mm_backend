﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using System.ServiceModel.Activities;
using MMS.Activities;
using MMS.Activities.Extensions;
using MMS.Activities.Properties;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online;
using Orders.Backend.WF.Activities.Proxy.WWS;
using Orders.Backend.WF.Activities.Stores;
using Orders.Backend.WF.Activities.WWS.Design;
using TicketTool.Activities.Tasks;
using OperationResult = Orders.Backend.WF.Activities.Common.OperationResult;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(CreateReserveActivityDesigner))]
    public sealed class CreateReserve : NativeActivity
    {
        private ActivityFunc<Order, CorrelationHandle, string, CreateSalesOrderResult> CreateWwsReserve { get; set; }

        private Delay Delay { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<string> TicketId { get; set; }

        [Browsable(false)]
        public OutArgument<OperationResult> OperationResult { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var result = new DelegateOutArgument<CreateSalesOrderResult>();
            var orderArg = new DelegateInArgument<Order>();
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var ticketIdArg = new DelegateInArgument<string>();

            var isValid = new Variable<bool>();
            var outcome = new Variable<string>();

            var handler =
                new Sequence
                {
                    Variables = {isValid, outcome},
                    Activities =
                    {
                        new DoWhile(c => !isValid.Get(c))
                        {
                            Body = new Sequence
                            {
                                Activities =
                                {
                                    new RefreshOrder {Order = orderArg},
                                    new GetStore
                                    {
                                        SapCode = new InArgument<string>(c => orderArg.Get(c).Store.SapCode),
                                        Store = new OutArgument<Store>(c => orderArg.Get(c).Store)
                                    },
                                    new ValidateOrder {Order = orderArg, Result = new OutArgument<bool>(isValid)},
                                    new If(c => !isValid.Get(c))
                                    {
                                        Then = new TaskActivity
                                        {
                                            DisplayName = "Check And Correct Order",
                                            Outcome = new OutArgument<string>(outcome),
                                            Outcomes = {new Outcome {Name = Backend_Activities.Outcome_Checked}},
                                            CorrelationHandle = handleArg,
                                            Fields =
                                            {
                                                new TaskField<string>
                                                {
                                                    Name = "Name",
                                                    Value =
                                                        new InArgument<string>(
                                                            Backend_Activities.FieldName_CheckAndCorrectOrder)
                                                },
                                                new TaskField<string>
                                                {
                                                    Name = "Description",
                                                    Value =
                                                        new InArgument<string>(
                                                            Backend_Activities.FieldDescription_CheckAndCorrectOrder)
                                                },
                                                new TaskField<string> {Name = "TicketId", Value = ticketIdArg}
                                            }
                                        }
                                    },
                                }
                            }
                        },
                        //new Throw { Exception = () },
                        //new Delay { Duration = new InArgument<TimeSpan>(ctx => TimeSpan.FromSeconds(30))},
                        new Persist(),
                        new Assign
                        {
                            To = new OutArgument<CreateSalesOrderResult>(result),
                            Value =
                                new InArgument<CreateSalesOrderResult>(
                                    c => new SalesServiceClient().CreateSalesOrder(orderArg.Get(c).ToOrderInfo()))
                        }
                    }
                };

            var createOrder = new FailoverWithCancel
            {
                DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                Delegate = new InvokeDelegate
                {
                    Delegate = new ActivityAction
                    {
                        Handler = handler
                    }
                }
            };
            CreateWwsReserve = new ActivityFunc<Order, CorrelationHandle, string, CreateSalesOrderResult>
            {
                Argument1 = orderArg,
                Argument2 = handleArg,
                Argument3 = ticketIdArg,
                Result = result,
                Handler = createOrder
            };
            metadata.AddDelegate(CreateWwsReserve);

            Delay = new Delay {Duration = TimeSpan.FromSeconds(Settings.Default.FailoverTimeout)};
            metadata.AddImplementationChild(Delay);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var handle = new RuntimeArgument(Backend_Activities.Param_CorrelationHandle, typeof(CorrelationHandle),
                ArgumentDirection.In, true);
            metadata.Bind(CorrelationHandle, handle);
            metadata.AddArgument(handle);

            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);

            var ticketId = new RuntimeArgument(Backend_Activities.Param_TicketId, typeof(string), ArgumentDirection.In,
                true);
            metadata.Bind(TicketId, ticketId);
            metadata.AddArgument(ticketId);

            var operationResult = new RuntimeArgument(Backend_Activities.Param_OperationResult, typeof(OperationResult),
                ArgumentDirection.Out);
            metadata.Bind(OperationResult, operationResult);
            metadata.AddArgument(operationResult);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(CreateWwsReserve, Order.Get(context), CorrelationHandle.Get(context),
                TicketId.Get(context), OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance,
            CreateSalesOrderResult result)
        {
            if(completedinstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }

            var order = Order.Get(context);
            OperationResult operationResult;
            switch(result.OperationCode.Code)
            {
                case ReturnCode.Ok:
                    Merge(order, result);
                    operationResult = new OperationResult {CompletedSuccessfully = true};
                    context.LogInfoFormat("CreateReserve return OK : {0}", result.OperationCode.MessageText);
                    break;
                case ReturnCode.OkPartially:
                    Merge(order, result);
                    operationResult = new OperationResult
                    {
                        CompletedSuccessfully = true,
                        Message = result.OperationCode.MessageText
                    };
                    context.LogInfoFormat("CreateReserve return OkPartially : {0}", result.OperationCode.MessageText);
                    break;
                case ReturnCode.JBossException:
                    operationResult = new OperationResult {Message = result.OperationCode.MessageText};
                    context.LogErrorFormat("CreateReserve return JBossException : {0}", result.OperationCode.MessageText);
                    break;
                case ReturnCode.Exception:
                    operationResult = new OperationResult {Message = result.OperationCode.MessageText};
                    context.LogErrorFormat("CreateReserve return Exception : {0}", result.OperationCode.MessageText);
                    context.ScheduleActivity(Delay, OnDelayCompleted);
                    break;
                case ReturnCode.ValidationException:
                    operationResult = new OperationResult {Message = result.OperationCode.MessageText};
                    context.LogErrorFormat("CreateReserve return ValidationException : {0}",
                        result.OperationCode.MessageText);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            OperationResult.Set(context, operationResult);
        }

        private static void Merge(Order order, CreateSalesOrderResult result)
        {
            var wwsOrder = result.ToWwsOrder();
            wwsOrder.SapCode = order.Store.SapCode;
            order.WwsOrders.Add(wwsOrder);

            order.MergeDelivery(result.OrderHeader.Delivery);
            order.MergeCustomer(result.OrderHeader.InvoiceCustomer);
            order.MergeConsignee(result.OrderHeader.DeliveryCustomer);
            order.MergeReserve(true);
        }

        private void OnDelayCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            context.ScheduleFunc(CreateWwsReserve, Order.Get(context), CorrelationHandle.Get(context),
                TicketId.Get(context), OnCompleted);
        }
    }
}