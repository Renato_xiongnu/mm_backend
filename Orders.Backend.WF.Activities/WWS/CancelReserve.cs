﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities;
using MMS.Activities.Extensions;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Properties;
using Orders.Backend.WF.Activities.Proxy.WWS;
using Orders.Backend.WF.Activities.WWS.Design;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(CancelReserveActivityDesigner))]
    public sealed class CancelReserve : NativeActivity
    {
        private ActivityFunc<Order, CancelSalesOrderResult> CancelWwsReserve { get; set; }

        private Delay Delay { get; set; }

        private Variable<int> Reps { get; set; }

        private Variable<int> CurrentIndex { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var orderArg = new DelegateInArgument<Order>();
            var result = new DelegateOutArgument<CancelSalesOrderResult>();

            var cancelReserve =
                new If(c => orderArg.Get(c).WwsOrders.Count > 0)
                {
                    Then = new Failover
                    {
                        DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                        Delegate = new InvokeDelegate
                        {
                            Delegate = new ActivityAction
                            {
                                Handler = new Assign
                                {
                                    To = new OutArgument<CancelSalesOrderResult>(result),
                                    Value =
                                        new InArgument<CancelSalesOrderResult>(
                                            c =>
                                                new SalesServiceClient().CancelSalesOrder(
                                                    orderArg.Get(c).ToOrderNumberInfo()))
                                }
                            }
                        }
                    }
                };
            CancelWwsReserve = new ActivityFunc<Order, CancelSalesOrderResult>
            {
                Argument = orderArg,
                Result = result,
                Handler = cancelReserve
            };
            metadata.AddDelegate(CancelWwsReserve);
            Delay = new Delay { Duration = TimeSpan.FromSeconds(MMS.Activities.Properties.Settings.Default.FailoverTimeout) };
            metadata.AddImplementationChild(Delay);
            Reps = new Variable<int>("Reps", 3);
            metadata.AddImplementationVariable(Reps);
            CurrentIndex = new Variable<int>("CurrentIndex", 0);
            metadata.AddImplementationVariable(CurrentIndex);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(CancelWwsReserve, Order.Get(context), OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance,
            CancelSalesOrderResult result)
        {
            if(result == null)
            {
                return;
            }
            switch(result.OperationCode.Code)
            {
                case ReturnCode.JBossException:
                    context.LogErrorFormat("CancelReserve return error : {0}", result.OperationCode.MessageText);
                    if(result.OperationCode.SubCode != Settings.Default.NotFoundCode)
                    {
                        context.ScheduleActivity(Delay, OnDelayCompleted);
                    }
                    break;
                case ReturnCode.Exception:
                    context.LogErrorFormat("CancelReserve return error : {0}", result.OperationCode.MessageText);
                    context.ScheduleActivity(Delay, OnDelayCompleted);
                    break;
            }
        }

        private void OnDelayCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            var index = CurrentIndex.Get(context);
            var reps = Reps.Get(context);
            if(index < reps)
            {
                CurrentIndex.Set(context, ++index);
                context.ScheduleFunc(CancelWwsReserve, Order.Get(context), OnCompleted);
            }
        }
    }
}