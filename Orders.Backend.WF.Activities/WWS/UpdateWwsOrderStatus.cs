﻿using System;
using System.Activities;
using System.ComponentModel;
using System.Activities.Statements;
using MMS.Activities;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.WWS;
using Common.Helpers;
using Orders.Backend.WF.Activities.WWS.Design;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(UpdateWwsOrderStatusActivityDesigner))]
    public class UpdateWwsOrderStatus:NativeActivity
    {
        private ActivityFunc<Order, GetSalesOrderResult> Flow { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>("Order");
            var resultArg = new DelegateOutArgument<GetSalesOrderResult>("Result");
            this.Flow = new ActivityFunc<Order, GetSalesOrderResult>
            {
                Argument = orderArg,
                Result = resultArg,
                Handler = new Failover
                {
                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                    Delegate = new InvokeDelegate
                    {
                        Delegate = new ActivityAction
                        {
                            Handler = new Assign
                            {
                                To = new OutArgument<GetSalesOrderResult>(resultArg),
                                Value = new InArgument<GetSalesOrderResult>(c => new SalesServiceClient().GetSalesOrder(orderArg.Get(c).ToOrderNumberInfo()))
                            }
                        }
                    }
                }
            };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(this.Flow, this.Order.Get(context), this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, GetSalesOrderResult result)
        {
            if (result == null) return;
            if (result.OrderHeader == null) return;

            var order = Order.Get(context);
            if(order.CurrentWwsOrder==null)
            {
                return;
            }

            try
            {
                order.CurrentWwsOrder.OrderStatus = result.OrderHeader.OrderStatus.ConvertTo<WwsSalesOrderStatus>(); //TODO
            }
            catch (Exception)
            {
                order.CurrentWwsOrder.OrderStatus = WwsSalesOrderStatus.None;
                //context.LogError("Convert SalesOrderStatus to WwsSalesOrderStatus error", e);
            }
            if (order.CurrentWwsOrder.OrderStatus == WwsSalesOrderStatus.Transformed)
            {
                order.AttachNewWwsOrder(result.OrderHeader.RefOrderNumber);
            }
        }
    }
}
