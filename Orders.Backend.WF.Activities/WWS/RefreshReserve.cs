﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using MMS.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.WWS;
using Orders.Backend.WF.Activities.WWS.Design;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(RefreshReserveActivityDesigner))]
    public sealed class RefreshReserve : NativeActivity
    {
        private ActivityFunc<Order, GetSalesOrderResult> Refresh { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof (Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>("Order");
            var resultArg = new DelegateOutArgument<GetSalesOrderResult>("Result");
            this.Refresh = new ActivityFunc<Order, GetSalesOrderResult>
                {
                    Argument = orderArg,
                    Result = resultArg,
                    Handler = new If(c => orderArg.Get(c).CurrentWwsOrder != null)
                        {
                            Then = new Failover
                                {
                                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                                    Delegate = new InvokeDelegate
                                        {
                                            Delegate = new ActivityAction
                                                {
                                                    Handler = new Assign
                                                        {
                                                            To = new OutArgument<GetSalesOrderResult>(resultArg),
                                                            Value = new InArgument<GetSalesOrderResult>(c => new SalesServiceClient().GetSalesOrder(orderArg.Get(c).ToOrderNumberInfo()))
                                                        }
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Refresh);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(this.Refresh, this.Order.Get(context), this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, GetSalesOrderResult result)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;
            if (result == null || result.OrderHeader == null) return;

            var order = this.Order.Get(context);
            var reserve = order.CurrentWwsOrder;
            if (reserve == null) return;

            var salesOrder = result.OrderHeader;
            reserve.CreationDate = salesOrder.CreateTime.HasValue ? salesOrder.CreateTime.Value : default(DateTime);
            reserve.DocBarcode = salesOrder.DocBarcode;
            reserve.DocBarcodeImage = salesOrder.DocBarcodeImage;
            reserve.LastUpdateTime = salesOrder.LastUpdateTime;
            
            reserve.LeftoverTotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.LeftoverPrice.GrossPrice,
                    NETPrice = salesOrder.LeftoverPrice.NetPrice,
                    VAT = (int)salesOrder.LeftoverPrice.VAT,
                    VATPrice = salesOrder.LeftoverPrice.VatPrice
                };
            reserve.OriginalSum = salesOrder.OriginalSum;
            reserve.PrepaymentTotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.DownpaymentPriceFull.GrossPrice,
                    NETPrice = salesOrder.DownpaymentPriceFull.NetPrice,
                    VAT = (int)salesOrder.DownpaymentPriceFull.VAT,
                    VATPrice = salesOrder.DownpaymentPriceFull.VatPrice
                };
            reserve.StoreManagerName = salesOrder.SalesPersonName;
            reserve.TotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.TotalPrice.GrossPrice,
                    NETPrice = salesOrder.TotalPrice.NetPrice,
                    VAT = (int)salesOrder.TotalPrice.VAT,
                    VATPrice = salesOrder.TotalPrice.VatPrice
                };
            reserve.Items = new Collection<WwsOrderItem>(salesOrder.Lines.Select(l => l.ToWwsOrderItem()).ToList());

            order.MergeDelivery(salesOrder.Delivery);
            order.MergeCustomer(salesOrder.InvoiceCustomer);
            order.MergeConsignee(salesOrder.DeliveryCustomer);
            order.MergeReserve(false);
        }
    }
}
