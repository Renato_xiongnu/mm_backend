using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using System.ServiceModel.Activities;
using MMS.Activities;
using MMS.Activities.Properties;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online;
using Orders.Backend.WF.Activities.WWS.Design;
using TicketTool.Activities.Tasks;

namespace Orders.Backend.WF.Activities.WWS
{
    [Designer(typeof(CheckArticlesActivityDesigner))]
    public sealed class CheckArticles : NativeActivity
    {
        private ActivityFunc<Order, CorrelationHandle, string, bool> CheckArticlesFunc { get; set; }

        private Delay Delay { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<string> TicketId { get; set; }

        [Browsable(false)]
        public OutArgument<OperationResult> OperationResult { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var result = new DelegateOutArgument<bool>();
            var orderArg = new DelegateInArgument<Order>();
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var ticketIdArg = new DelegateInArgument<string>();

            var isValid = new Variable<bool>();
            var outcome = new Variable<string>();

            var handler =
                new Sequence
                {
                    Variables = {isValid, outcome},
                    Activities =
                    {
                        new DoWhile(c => !isValid.Get(c))
                        {
                            Body = new Sequence
                            {
                                Activities =
                                {
                                    new RefreshOrder {Order = orderArg},
                                    new ValidateOrder {Order = orderArg, Result = new OutArgument<bool>(isValid)},
                                    new If(c => !isValid.Get(c))
                                    {
                                        Then = new TaskActivity
                                        {
                                            DisplayName = "Check And Replace Articles",
                                            Outcome = new OutArgument<string>(outcome),
                                            Outcomes = {new Outcome {Name = Backend_Activities.Outcome_Checked}},
                                            CorrelationHandle = handleArg,
                                            Fields =
                                            {
                                                new TaskField<string>
                                                {
                                                    Name = "Name",
                                                    Value =
                                                        new InArgument<string>(
                                                            Backend_Activities.FieldName_CheckArticles)
                                                },
                                                new TaskField<string>
                                                {
                                                    Name = "Description",
                                                    Value =
                                                        new InArgument<string>(
                                                            Backend_Activities.FieldDescription_CheckAndReplaceArticles)
                                                },
                                                new TaskField<string> {Name = "TicketId", Value = ticketIdArg}
                                            }
                                        }
                                    },
                                }
                            }
                        },
                        new Assign
                        {
                            To = new OutArgument<bool>(result),
                            Value = new InArgument<bool>(isValid)
                        }
                    }
                };

            var failover = new Failover
            {
                DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                Delegate = new InvokeDelegate
                {
                    Delegate = new ActivityAction
                    {
                        Handler = handler
                    }
                }
            };
            CheckArticlesFunc = new ActivityFunc<Order, CorrelationHandle, string, bool>
            {
                Argument1 = orderArg,
                Argument2 = handleArg,
                Argument3 = ticketIdArg,
                Result = result,
                Handler = failover
            };
            metadata.AddDelegate(CheckArticlesFunc);

            Delay = new Delay {Duration = TimeSpan.FromSeconds(Settings.Default.FailoverTimeout)};
            metadata.AddImplementationChild(Delay);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var handle = new RuntimeArgument(Backend_Activities.Param_CorrelationHandle, typeof(CorrelationHandle),
                ArgumentDirection.In, true);
            metadata.Bind(CorrelationHandle, handle);
            metadata.AddArgument(handle);

            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);

            var ticketId = new RuntimeArgument(Backend_Activities.Param_TicketId, typeof(string), ArgumentDirection.In,
                true);
            metadata.Bind(TicketId, ticketId);
            metadata.AddArgument(ticketId);

            var operationResult = new RuntimeArgument(Backend_Activities.Param_OperationResult, typeof(OperationResult),
                ArgumentDirection.Out);
            metadata.Bind(OperationResult, operationResult);
            metadata.AddArgument(operationResult);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(CheckArticlesFunc, Order.Get(context), CorrelationHandle.Get(context),
                TicketId.Get(context), OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, bool result)
        {
            if(completedinstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }
            var operationResult = new OperationResult()
            {
                CompletedSuccessfully = result
            };

            OperationResult.Set(context, operationResult);
        }
    }
}