﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities;
using MMS.Activities.Extensions;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Properties;
using Orders.Backend.WF.Activities.Proxy.WWS;
using Orders.Backend.WF.Activities.WWS.Design;
namespace Orders.Backend.WF.Activities.WWS
{

    [Designer(typeof(WwsChangeDeliveryDateActivityDesigner))]
    public sealed class WwsChangeDeliveryDate : NativeActivity
    {
        private ActivityFunc<Order, ChangeDeliveryDateResult> ChangeDeliveryDate { get; set; }

        private Delay Delay { get; set; }

        private Variable<int> Reps { get; set; }

        private Variable<int> CurrentIndex { get; set; }


        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var orderArg = new DelegateInArgument<Order>();
            var result = new DelegateOutArgument<ChangeDeliveryDateResult>();

            var changeDate =
                new If(c => orderArg.Get(c).WwsOrders.Count > 0 && orderArg.Get(c).Delivery.DeliveryDate.HasValue)
                {
                    Then = new Failover
                    {
                        DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                        Delegate = new InvokeDelegate
                        {
                            Delegate = new ActivityAction
                            {
                                Handler = new Assign
                                {
                                    To = new OutArgument<ChangeDeliveryDateResult>(result),
                                    Value = new InArgument<ChangeDeliveryDateResult>(c => new SalesServiceClient().ChangeDeliveryDate(orderArg.Get(c).ToOrderNumberInfo(), orderArg.Get(c).Delivery.DeliveryDate.Value))
                                }
                            }
                        }
                    }
                };
            ChangeDeliveryDate = new ActivityFunc<Order, ChangeDeliveryDateResult>
            {
                Argument = orderArg,
                Result = result,
                Handler = changeDate
            };
            metadata.AddDelegate(ChangeDeliveryDate);
            Delay = new Delay { Duration = TimeSpan.FromSeconds(MMS.Activities.Properties.Settings.Default.FailoverTimeout) };
            metadata.AddImplementationChild(Delay);
            Reps = new Variable<int>("Reps", 3);
            metadata.AddImplementationVariable(Reps);
            CurrentIndex = new Variable<int>("CurrentIndex", 0);
            metadata.AddImplementationVariable(CurrentIndex);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(ChangeDeliveryDate, Order.Get(context), OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, ChangeDeliveryDateResult result)
        {
            if (result == null)
            {
                return;
            }
            switch (result.OperationCode.Code)
            {
                case ReturnCode.JBossException:
                    context.LogErrorFormat("ChangeDeliveryDate return error : {0}", result.OperationCode.MessageText);
                    if (result.OperationCode.SubCode != Settings.Default.NotFoundCode)
                    {
                        context.ScheduleActivity(Delay, OnDelayCompleted);
                    }
                    break;
                case ReturnCode.Exception:
                    context.LogErrorFormat("ChangeDeliveryDate return error : {0}", result.OperationCode.MessageText);
                    context.ScheduleActivity(Delay, OnDelayCompleted);
                    break;
            }
        }

        private void OnDelayCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            var index = CurrentIndex.Get(context);
            var reps = Reps.Get(context);
            if (index < reps)
            {
                CurrentIndex.Set(context, ++index);
                context.ScheduleFunc(ChangeDeliveryDate, Order.Get(context), OnCompleted);
            }
        }
    }
}
