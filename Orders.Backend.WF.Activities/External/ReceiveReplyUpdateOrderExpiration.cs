﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activities;
using MMS.Activities;
using TicketTool.Activities.Common;
using TicketTool.Activities.Properties;

namespace Orders.Backend.WF.Activities.External
{
    public class ReceiveReplyUpdateOrderExpiration : ReceiveReplyOrderCommandBase<string>
    {
        protected override string CommandName
        {
            get { return "UpdateOrderExpiration_External"; }
        }

        protected override void CacheArguments(NativeActivityMetadata metadata)
        {
            var approvedBy = new RuntimeArgument("UpdatedBy", typeof (string),
                ArgumentDirection.Out, true);
            metadata.Bind(ApprovedBy, approvedBy);
            metadata.AddArgument(approvedBy);
            
            base.CacheArguments(metadata);
        }

        protected override void CacheChild(NativeActivityMetadata metadata)
        {
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var approvedByArg = new DelegateOutArgument<string>("UpdatedByInternal");
            var approvedBy = new Variable<string>("UpdatedBy");

            var recieve = new Receive
            {
                CanCreateInstance = false,
                ServiceContractName = Settings.Default.ServiceContractName,
                OperationName = CommandName,
                Content = ReceiveContent.Create(new Dictionary<string, OutArgument>
                {
                    {"UpdatedBy", new OutArgument<string>(approvedBy)},
                    {Settings.Default.WorkItemIdParam, new OutArgument<string>()}
                })
            };
            recieve.CorrelatesWith = handleArg;
            recieve.
                CorrelatesOn = new MessageQuerySet
                {
                    {Settings.Default.TicketIdKey, new CorrelationWorkItemIdQuery(Settings.Default.WorkItemIdParam)}
                };

            Child =
                new ActivityFunc<CorrelationHandle, string>
                {
                    Argument = handleArg,
                    Result = approvedByArg,
                    Handler = new Sequence
                    {
                        Variables = {approvedBy},
                        Activities =
                        {
                            recieve,
                            new Log
                            {
                                DisplayName = string.Format("{0} receive log", CommandName),
                                Text =
                                    new InArgument<string>(
                                        c =>
                                            string.Format("Start {0}", CommandName))
                            },
                            new Assign
                            {
                                To = new OutArgument<string>(approvedByArg),
                                Value = new InArgument<string>(approvedBy)
                            },
                            new SendReply
                            {
                                Request = recieve,
                            }
                        }
                    }
                };
            metadata.AddDelegate(Child);
        }
    }
}
