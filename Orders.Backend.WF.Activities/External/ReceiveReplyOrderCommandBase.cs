﻿using System;
using System.Activities;
using System.ServiceModel.Activities;
using MMS.Activities.Extensions;
using TicketTool.Activities.Properties;

namespace Orders.Backend.WF.Activities.External
{
    public abstract class ReceiveReplyOrderCommandBase<T> : NativeActivity
    {
        protected abstract string CommandName { get; }

        protected ActivityFunc<CorrelationHandle,T> Child { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        [RequiredArgument]
        public OutArgument<string> ApprovedBy { get; set; }

        protected sealed override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheChild(metadata);
        }

        protected abstract void CacheChild(NativeActivityMetadata metadata);

        protected virtual void CacheArguments(NativeActivityMetadata metadata)
        {
            var handle = new RuntimeArgument(Resources.Param_CorrelationHandle, typeof (CorrelationHandle),
                ArgumentDirection.In, true);
            metadata.Bind(CorrelationHandle, handle);
            metadata.AddArgument(handle);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(Child, CorrelationHandle.Get(context), OnCompleted, OnFaulted);
        }

        protected void OnCompleted(NativeActivityContext context, ActivityInstance completedInstance, T result)
        {
            if (completedInstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }

            ApprovedBy.Set(context, result);

            context.LogInfoFormat("Finish {0}, approved by {1}", CommandName, result);
        }

        private void OnFaulted(NativeActivityFaultContext faultcontext, Exception propagatedexception,
            ActivityInstance propagatedfrom)
        {
            faultcontext.LogErrorFormat("{0} Faulted", propagatedexception, DisplayName);
            faultcontext.HandleFault();
            faultcontext.Abort();
        }
    }
}
