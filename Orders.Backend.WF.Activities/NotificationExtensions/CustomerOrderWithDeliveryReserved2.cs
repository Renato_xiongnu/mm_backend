﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderWithDeliveryReserved2;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Properties;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderWithDeliveryReserved2
    {
        public static CustomerOrderWithDeliveryReserved2NotificationDto
            ToCustomerOrderWithDeliveryReserved2NotificationDto(this Order order)
        {
            return new CustomerOrderWithDeliveryReserved2NotificationDto
            {
                OrderId = order.Id,
                DeliveryDate =
                    order.Delivery.DeliveryDate.HasValue
                        ? BeautifulFormatter.FormatDate(order.Delivery.DeliveryDate.Value)
                        : string.Empty,
                DeliveryInterval =
                        string.IsNullOrEmpty(order.Delivery.RequestedDeliveryTimeslot)
                            ? Settings.Default.DefaultRequestedDeliveryTimeslotForDeliver
                            : order.Delivery.RequestedDeliveryTimeslot,
                SaleLocationPhone = BeautifulFormatter.FormatTelephone(order.Store.Telephone)
            };
        }
    }
}
