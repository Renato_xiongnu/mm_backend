﻿using Orders.Backend.Notification.Model.OnlineOrder.CheckPaymentForManager;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CheckPaymentForManager
    {
        public static CheckPaymentForManagerNotificationDto ToCheckPaymentForManagerNotificationDto(this Order order)
        {
            var dto = new CheckPaymentForManagerNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id                
            };
            return dto;
        }
    }
}