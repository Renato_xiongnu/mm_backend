﻿using System;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveExpirationUpdated;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveExpires;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveExpiresAfterUpdate;
using Orders.Backend.WF.Activities.Model;


namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerReserveExpiresAfterUpdate
    {
        public static CustomerReserveExpiresAfterUpdateNotificationDto ToCustomerReserveExpiresAfterUpdate(this Order order)
        {
            var dto = new CustomerReserveExpiresAfterUpdateNotificationDto
            {
                OrderId = order.Id,
                SaleLocationAddress = order.PickupAddress,
                ReserveLifeTime = string.Format(" через {0} часа", 24)
            };
            return dto;
        }
    }
}
