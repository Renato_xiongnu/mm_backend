﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderStoreCanceled;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderStoreCanceled
    {
        public static CustomerOrderStoreCanceledNotificationDto ToCustomerOrderStoreCanceledNotificationDto(this Order order)
        {
            var dto = new CustomerOrderStoreCanceledNotificationDto
            {
                OrderId = order.Id,                
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                },
                SaleLocationInfo = new SaleLocationInfo
                {
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),                    
                }
            };
            return dto;
        }
    }
}
