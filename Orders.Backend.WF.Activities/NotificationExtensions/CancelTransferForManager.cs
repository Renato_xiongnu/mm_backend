﻿using Orders.Backend.Notification.Model.OnlineOrder.CancelTransferForManager;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CancelTransferForManager
    {
        public static CancelTransferForManagerNotificationDto ToCancelTransferForManagerNotificationDto(this Order order, string transferSapCode, int transferNumber)
        {
            var dto = new CancelTransferForManagerNotificationDto
            {
                OrderId = order.Id,
                WwsOrder = new Notification.Model.OnlineOrder.CancelTransferForManager.WwsOrder()
                {
                    SapCode = order.CurrentWwsOrder.SapCode,
                    WwsOrderId = order.CurrentWwsOrder.Id,
                },
                TransferInfo = new TransferInfo()
                {
                    Number = transferNumber,
                    SapCode = transferSapCode
                }
               
            };
            return dto;
        }
    }
}