﻿using Orders.Backend.Notification.Model.OnlineOrder.ReturnArticlesToStoreForManager;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class ReturnArticlesToStoreForManager
    {
        public static ReturnArticlesToStoreForManagerNotificationDto ToReturnArticlesToStoreForManagerNotificationDto(
            this Order order, WwsOrder wwsOrder=null)
        {
            wwsOrder = wwsOrder ?? order.CurrentWwsOrder;
            return new ReturnArticlesToStoreForManagerNotificationDto {OrderId = order.Id, WwsOrderId = wwsOrder.Id};
        }
    }
}