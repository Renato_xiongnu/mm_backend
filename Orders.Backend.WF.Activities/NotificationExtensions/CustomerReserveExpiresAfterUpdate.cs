﻿using System;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveExpirationUpdated;
using Orders.Backend.WF.Activities.Model;


namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerReserveExpirationUpdated
    {
        public static CustomerReserveExpirationUpdatedNotificationDto ToCustomerReserveExpirationUpdated(this Order order)
        {
            var dto = new CustomerReserveExpirationUpdatedNotificationDto
            {
                OrderId = order.Id,
                SaleLocationAddress = order.PickupAddress,
                NewExpirationDate =
                    !order.ExpirationDate.HasValue
                        ? BeautifulFormatter.FormatDate(DateTime.Today.Add(order.Store.ReserveLifeTime))
                        : BeautifulFormatter.FormatDate(order.ExpirationDate.Value)
            };
            return dto;
        }
    }
}
