﻿using Orders.Backend.Notification.Model.OnlineOrder.AcknowledgementReceiptTransferForManager;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class AcknowledgementReceiptTransferForManager
    {
        public static AcknowledgementReceiptTransferForManagerNotificationDto
            ToAcknowledgementReceiptTransferForManagerNotificationDto(this Order order)
        {
            return new AcknowledgementReceiptTransferForManagerNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id
            };
        }
    }
}