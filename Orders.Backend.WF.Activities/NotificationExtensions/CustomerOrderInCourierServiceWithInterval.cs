﻿using System.Linq;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderInCourierServiceWithInterval;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Properties;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderInCourierServiceWithInterval
    {
        public static CustomerOrderInCourierServiceWithIntervalNotificationDto ToCustomerOrderInCourierServiceWithIntervalNotificationDto(
            this Order order)
        {
            var dto = new CustomerOrderInCourierServiceWithIntervalNotificationDto
            {
                OrderId = order.Id,
                SaleLocationPhone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                TotalPrice = order.Items.Sum(el=>el.Price*el.Quantity).ToString(""),
                DeliveryInfo = new DeliveryInfo
                {
                    DeliveryDate =
                        order.Delivery.DeliveryDate.HasValue
                            ? BeautifulFormatter.FormatDate(order.Delivery.DeliveryDate.Value)
                            : "",
                    DeliveryInterval =
                        string.IsNullOrEmpty(order.Delivery.RequestedDeliveryTimeslot)
                            ? Settings.Default.DefaultRequestedDeliveryTimeslotForDeliver
                            : order.Delivery.RequestedDeliveryTimeslot
                }
            };
            return dto;
        }
    }
}