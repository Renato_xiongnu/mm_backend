﻿using System;
using System.Text.RegularExpressions;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class BeautifulFormatter
    {
        public static string FormatTelephone(string telephone)
        {
            if(string.IsNullOrEmpty(telephone))
            {
                return string.Empty;
            }
            telephone = Regex.Replace(telephone, "[^0-9]", "");
            if (string.IsNullOrEmpty(telephone))
            {
                return string.Empty;
            }
            var math = Regex.Match(telephone, @"(\d)?(?<number>\d{10})$");
            if(!math.Success)
            {
                return telephone;
            }
            telephone = math.Groups["number"].Value;
            telephone = Regex.Replace(telephone, @"(\d{3})(\d{3})(\d{2})(\d{2})", "+7-$1-$2-$3-$4");
            return telephone;
        }

        public static string FormatDate(DateTime dateTime)
        {            
            return dateTime.ToString("dd.MM.yyyy");
        }

        public static string GetPaymentTypeName(PaymentType paymentType)
        {
            switch(paymentType)
            {
                case PaymentType.Cash:
                    return Backend_Activities.CashPayment;
                case PaymentType.Online:
                    return Backend_Activities.BankCard;
                case PaymentType.OnlineCredit:                    
                    return Backend_Activities.Credit;
                case PaymentType.SocialCard:
                    return Backend_Activities.SocialCard;
                default:
                    throw new ArgumentOutOfRangeException("paymentType");
            }
        }

        public static string GetOrderStateName(this OrderState orderState)
        {
            return orderState.ToString();
        }

        public static string GetHours(int hours)
        {
            var lastDig = hours%10;

            if (hours >= 10 && hours < 19) return string.Format("{0} часов", hours);
            if (lastDig == 1) return string.Format("{0} час", hours);
            if (lastDig >= 2 && lastDig <= 4) return string.Format("{0} часа", hours);
            if (lastDig >= 5 && lastDig <= 9) return string.Format("{0} часов", hours);

            return string.Format("{0} часов", hours);
        }
    }
}
