﻿using System;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveExpiresAfterCantGetThrough;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerReserveExpiresAfterCantGetThrough
    {
        public static CustomerReserveExpiresAfterCantGetThroughNotificationDto
            ToCustomerReserveExpiresAfterCantGetThroughNotificationDto(this Order order)
        {
            return new CustomerReserveExpiresAfterCantGetThroughNotificationDto
            {
                OrderId = order.Id,
                ReserveLifeTime = !order.ExpirationDate.HasValue
                    ? BeautifulFormatter.FormatDate(DateTime.Today.Add(order.Store.ReserveLifeTime))
                    : BeautifulFormatter.FormatDate(order.ExpirationDate.Value),
                SaleLocationAddress = order.PickupAddress,
                SaleLocationPhone = BeautifulFormatter.FormatTelephone(order.Store.Telephone)
            };
        }
    }
}
