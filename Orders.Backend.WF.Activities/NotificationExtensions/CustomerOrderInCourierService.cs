﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderInCourierService;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderInCourierService
    {
        public static CustomerOrderInCourierServiceNotificationDto ToCustomerOrderInCourierServiceNotificationDto(
            this Order order)
        {
            var dto = new CustomerOrderInCourierServiceNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname
                },
                SaleLocationInfo = new SaleLocationInfo
                {
                    City = order.Store.CityName,
                    SapCode = order.Store.SapCode,
                    OfficiallyStoreName = order.Store.OfficiallyStoreName,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone)
                },
                DeliveryInfo = new DeliveryInfo
                {
                    DeliveryDate =
                        order.Delivery.DeliveryDate.HasValue
                            ? order.Delivery.DeliveryDate.Value.ToString("dd.MM.yyyy")
                            : ""
                }
            };
            return dto;
        }
    }
}