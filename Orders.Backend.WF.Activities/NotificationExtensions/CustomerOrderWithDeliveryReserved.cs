﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderWithDeliveryReserved;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderWithDeliveryReserved
    {
        public static CustomerOrderWithDeliveryReservedNotificationDto
            ToCustomerOrderWithDeliveryReservedNotificationDto(this Order order)
        {
            return new CustomerOrderWithDeliveryReservedNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                },
                SaleLocationInfo = new SaleLocationInfo
                {
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                },
                TotalCost = order.CurrentWwsOrder.TotalPrice.GrossPrice.ToString("##.##")               
            };
        }
    }
}
