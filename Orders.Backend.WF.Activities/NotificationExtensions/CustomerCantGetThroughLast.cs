﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerCantGetThroughLast;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerCantGetThroughLast
    {
        public static CustomerCantGetThroughLastDto ToCustomerCantGetThroughLastDto(this Order order, int hoursToCancelOrder)
        {
            return new CustomerCantGetThroughLastDto
            {
                OrderId = order.Id,
                SaleLocationTelephone =
                    order.Store != null ? BeautifulFormatter.FormatTelephone(order.Store.Telephone) : string.Empty,
                TimeToCancelOrder = BeautifulFormatter.GetHours(hoursToCancelOrder)
            };
        }
    }
}
