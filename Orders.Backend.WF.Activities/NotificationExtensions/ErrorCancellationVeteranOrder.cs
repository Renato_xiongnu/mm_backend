﻿using Orders.Backend.Notification.Model.OnlineOrder.ErrorCancellationVeteranOrder;
using Orders.Backend.WF.Activities.Model;


namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class ErrorCancellationVeteranOrder
    {
        public static ErrorCancellationVeteranOrderNotificationDto ToErrorCancellationVeteranOrderNotificationDto(this Order order, string cancelBy, string cancelReason)
        {
            var dto = new ErrorCancellationVeteranOrderNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                CancelBy = cancelBy,
                CancelReason = cancelReason,
                OrderStatus = order.State.GetOrderStateName()                
            };
            return dto;
        }
    }
}
