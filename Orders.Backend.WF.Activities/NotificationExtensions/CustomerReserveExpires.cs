﻿using System;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveExpires;
using Orders.Backend.WF.Activities.Model;


namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerReserveExpires
    {
        public static CustomerReserveExpiresNotificationDto ToCustomerReserveExpiresNotificationDto(this Order order)
        {
            var dto = new CustomerReserveExpiresNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                ReserveLifeTime = string.Format(" через {0} часа", 24),
                SaleLocationInfo = new SaleLocationInfo
                {
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                    Address = order.PickupAddress
                },
                CustomerInfo = new CustomerInfo
                {
                    FirstName =
                       string.IsNullOrEmpty(order.Customer.Name)
                           ? order.Customer.Surname
                           : order.Customer.Name,
                    LastName = string.IsNullOrEmpty(order.Customer.Name)
                        ? ""
                        : order.Customer.Surname,
                }
            };
            return dto;
        }
    }
}
