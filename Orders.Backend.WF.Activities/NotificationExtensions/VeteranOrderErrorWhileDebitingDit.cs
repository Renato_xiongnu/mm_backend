using Orders.Backend.Notification.Model.OnlineOrder.VeteranOrderErrorWhileDebitingDit;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class VeteranOrderErrorWhileDebitingDit
    {
        public static VeteranOrderErrorWhileDebitingDitNotificationDto ToVeteranOrderErrorWhileDebitingDitNotificationDto(this Order order, string ditErrorCode, string ditErrorMessage)
        {
            var dto = new VeteranOrderErrorWhileDebitingDitNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,                
                OrderStatus = order.State.GetOrderStateName(),
                DitErrorCode = ditErrorCode,
                DitErrorMessage = ditErrorMessage
            };
            return dto;
        }
    }
}