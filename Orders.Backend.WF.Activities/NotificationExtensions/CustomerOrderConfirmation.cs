﻿using System.Globalization;
using System.Linq;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderConfirmation;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderConfirmationBase;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderWithDeliveryConfirmation;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerQuickOrderConfirmation;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderConfirmation
    {
        public static CustomerOrderConfirmationNotificationDto ToCustomerOrderConfirmationNotificationDto(
            this Order order)
        {
            var dto = new CustomerOrderConfirmationNotificationDto
            {
                OrderId = order.Id,
                SaleLocationInfo = new SaleLocationInfo
                {
                    City = order.Store.CityName,
                    SapCode = order.Store.SapCode,
                    OfficiallyStoreName = order.Store.OfficiallyStoreName,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                },
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                    EMail = order.Customer.Email,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Customer.Phone)
                },
                DeliveryInfo = new DeliveryInfo
                {
                    DeliveryType = order.DeliveryName
                },
                OrderInfo = new OrderInfo
                {
                    PaymentType = BeautifulFormatter.GetPaymentTypeName(order.Payment)
                },
                OrderLines = order.Items.Select(ToOrderLine).ToList(),
                TotalCost = order.Items
                    .Sum(oi => oi.Price*oi.Quantity)
                    .ToString("##.##"),
                Comment =
                    order.Customer.CustomerType == CustomerType.Staff
                        ? Backend_Activities.CustomerTypeStaffEmailMessage
                        : string.Empty
            };
            return dto;
        }

        public static CustomerQuickOrderConfirmationNotificationDto ToCustomerQuickOrderConfirmationNotificationDto(
            this Order order)
        {
            var dto = new CustomerQuickOrderConfirmationNotificationDto
            {
                OrderId = order.Id,
                SaleLocationInfo = new SaleLocationInfo
                {
                    City = order.Store.CityName,
                    SapCode = order.Store.SapCode,
                    OfficiallyStoreName = order.Store.OfficiallyStoreName,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                },
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                    EMail = order.Customer.Email,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Customer.Phone)
                },
                DeliveryInfo = new DeliveryInfo
                {
                    DeliveryType = order.DeliveryName
                },
                OrderInfo = new OrderInfo
                {
                    PaymentType = BeautifulFormatter.GetPaymentTypeName(order.Payment)
                },
                OrderLines = order.Items.Select(ToOrderLine).ToList(),
                TotalCost = order.Items
                    .Sum(oi => oi.Price*oi.Quantity)
                    .ToString("##.##"),
                Comment =
                    order.Customer.CustomerType == CustomerType.Staff
                        ? Backend_Activities.CustomerTypeStaffEmailMessage
                        : string.Empty
            };
            return dto;
        }

        public static CustomerOrderWithDeliveryConfirmationNotificationDto
            ToCustomerOrderWithDeliveryConfirmationNotificationDto(this Order order)
        {
            var dto = new CustomerOrderWithDeliveryConfirmationNotificationDto
            {
                OrderId = order.Id,
                SaleLocationInfo = new SaleLocationInfo
                {
                    City = order.Store.CityName,
                    SapCode = order.Store.SapCode,
                    OfficiallyStoreName = order.Store.OfficiallyStoreName,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                },
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                    EMail = order.Customer.Email,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Customer.Phone)
                },
                DeliveryInfo = new DeliveryInfo
                {
                    DeliveryType = order.DeliveryName
                },
                OrderInfo = new OrderInfo
                {
                    PaymentType = BeautifulFormatter.GetPaymentTypeName(order.Payment)
                },
                OrderLines = order.Items
                    .Where(oi => !IsDeliveryArticle(order, oi))
                    .Select(ToOrderLine).ToList(),
                NoLinkOrderLines = order.Items
                    .Where(oi => IsDeliveryArticle(order, oi))
                    .Select(oi => new OrderLine
                    {
                        Article = oi.Number,
                        Cost = (oi.Price*oi.Quantity).ToString("##.##"),
                        Price = oi.Price.ToString("##.##"),
                        Quantity = oi.Quantity.ToString(CultureInfo.InvariantCulture),
                        Title = "Доставка"
                    }).ToList(),
                TotalCostWithoutDelivery = order.Items
                    .Where(oi => !IsDeliveryArticle(order, oi))
                    .Sum(oi => oi.Price*oi.Quantity)
                    .ToString("##.##"),
                TotalCost = order.Items
                    .Sum(oi => oi.Price*oi.Quantity)
                    .ToString("##.##"),
                Comment =
                    order.Customer.CustomerType == CustomerType.Staff
                        ? Backend_Activities.CustomerTypeStaffEmailMessage
                        : string.Empty
            };
            return dto;
        }

        private static OrderLine ToOrderLine(OrderItem oi)
        {
            return new OrderLine
            {
                Article = oi.Number,
                Cost = (oi.Price*oi.Quantity).ToString("##.##"),
                Price = oi.Price.ToString("##.##"),
                Quantity = oi.Quantity.ToString(CultureInfo.InvariantCulture),
                Title = string.IsNullOrEmpty(oi.Title) ? oi.WwsDescription : oi.Title
            };
        }

        private static bool IsDeliveryArticle(Order order, OrderItem oi)
        {
            if(order.Store == null)
            {
                return false;
            }
            if (oi.ArticleProductType==ArticleProductType.DeliveryService)
            {
                return true;
            }
            if(order.Store.FreeDeliveryArticleId.HasValue &&
               oi.Number == order.Store.FreeDeliveryArticleId.Value.ToString(CultureInfo.InvariantCulture))
            {
                return true;
            }
            if(order.Store.PaidDeliveryArticleId.HasValue &&
               oi.Number == order.Store.PaidDeliveryArticleId.Value.ToString(CultureInfo.InvariantCulture))
            {
                return true;
            }
            return false;
        }
    }
}