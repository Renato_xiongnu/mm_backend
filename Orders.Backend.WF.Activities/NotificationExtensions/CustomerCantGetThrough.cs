﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerCantGetThrough;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerCantGetThrough
    {
        public static CustomerCantGetThroughDto ToCustomerCantGetThroughDto(this Order order)
        {
            return new CustomerCantGetThroughDto
            {
                OrderId = order.Id,
                SaleLocationTelephone = order.Store != null ? BeautifulFormatter.FormatTelephone(order.Store.Telephone) : string.Empty
            };
        }
    }
}
