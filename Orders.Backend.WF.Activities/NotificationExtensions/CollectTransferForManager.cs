﻿using Orders.Backend.Notification.Model.OnlineOrder.CollectTransferForManager;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CollectTransferForManager
    {
        public static CollectTransferForManagerNotificationDto ToCollectTransferForManagerNotificationDto(this Order order,string transferSapCode, int transferNumber)
        {
            var dto = new CollectTransferForManagerNotificationDto
            {
                OrderId = order.Id,
                WwsOrder = new Orders.Backend.Notification.Model.OnlineOrder.CollectTransferForManager.WwsOrder()
                {
                    SapCode = order.CurrentWwsOrder.SapCode,
                    WwsOrderId = order.CurrentWwsOrder.Id,
                },
                TransferInfo = new TransferInfo()
                {
                    Number = transferNumber,
                    SapCode = transferSapCode
                }
               
            };
            return dto;
        }
    }
}