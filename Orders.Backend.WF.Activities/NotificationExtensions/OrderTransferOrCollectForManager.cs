﻿using Orders.Backend.Notification.Model.OnlineOrder.OrderTransferOrCollectForManager;
using Orders.Backend.WF.Activities.Model;
using WwsOrder = Orders.Backend.Notification.Model.OnlineOrder.OrderTransferOrCollectForManager.WwsOrder;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class OrderTransferOrCollectForManager
    {
        public static OrderTransferOrCollectForManagerNotificationDto ToOrderTransferOrCollectForManagerNotificationDto(
            this Order order, Model.WwsOrder orderCollectedInWws)
        {
            orderCollectedInWws = orderCollectedInWws??order.PreviousWwsOrder;
            return new OrderTransferOrCollectForManagerNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                OrderCollectedInWws =
                    new WwsOrder
                    {
                        SapCode = orderCollectedInWws.SapCode,
                        WwsOrderId = orderCollectedInWws.Id
                    }
            };
        }
    }
}