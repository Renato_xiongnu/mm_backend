﻿using System.Globalization;
using System.Linq;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerFeedback;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerFeedback
    {
        public static CustomerFeedbackNotificationDto ToCustomerFeedbackNotificationDto(this Order order)
        {
            var dto = new CustomerFeedbackNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                YandexMarketUrl = order.YandexMarketUrl,
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                },
                SaleLocationInfo = new SaleLocationInfo
                {
                    City = order.Store.CityName,
                    SapCode = order.Store.SapCode,
                    OfficiallyStoreName = order.Store.OfficiallyStoreName,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone)
                },
                OrderLines = order.CurrentWwsOrder
                    .Items
                    .Where(oi => !string.IsNullOrEmpty(oi.MediaMarketUrl))
                    .Select(oi => new OrderLine
                    {
                        Article = oi.Article.ToString(CultureInfo.InvariantCulture),
                        MediaMarketUrl = oi.MediaMarketUrl,
                        Title = oi.Title
                    }).ToList()
            };
            return dto;
        }
    }
}