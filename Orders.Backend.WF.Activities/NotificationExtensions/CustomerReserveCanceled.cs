﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerReserveCanceled;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerReserveCanceled
    {
        public static CustomerReserveCanceledNotificationDto ToCustomerReserveCanceledNotificationDto(this Order order)
        {
            var dto = new CustomerReserveCanceledNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                SaleLocationInfo = new SaleLocationInfo
                {
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone)
                },
                CustomerInfo = new CustomerInfo
                {
                    FirstName =
                       string.IsNullOrEmpty(order.Customer.Name)
                           ? order.Customer.Surname
                           : order.Customer.Name,
                    LastName = string.IsNullOrEmpty(order.Customer.Name)
                        ? ""
                        : order.Customer.Surname,
                }
            };
            return dto;
        }
    }
}
