﻿using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderCanceled;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderCanceled
    {
        public static CustomerOrderCanceledNotificationDto ToCustomerOrderCanceledNotificationDto(this Order order)
        {
            var dto = new CustomerOrderCanceledNotificationDto
            {
                OrderId = order.Id,
                CustomerInfo = new CustomerInfo
                {
                    FirstName =
                        string.IsNullOrEmpty(order.Customer.Name)
                            ? order.Customer.Surname
                            : order.Customer.Name,
                    LastName = string.IsNullOrEmpty(order.Customer.Name)
                        ? ""
                        : order.Customer.Surname,
                },
                SaleLocationInfo = new SaleLocationInfo
                {
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                }
            };
            return dto;
        }
    }
}