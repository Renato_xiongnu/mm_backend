﻿using Orders.Backend.Notification.Model.OnlineOrder.VeteranOrderErrorWhenCancelingReserveDit;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class VeteranOrderErrorWhenCancelingReserveDit
    {
        public static VeteranOrderErrorWhenCancelingReserveDitNotificationDto ToVeteranOrderErrorWhenCancelingReserveDitNotificationDto(this Order order, string ditErrorCode, string ditErrorMessage, string cancelBy, string cancelReason)
        {
            var dto = new VeteranOrderErrorWhenCancelingReserveDitNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder!=null ? order.CurrentWwsOrder.Id: "Резерв не создан",
                CancelBy = cancelBy,
                CancelReason = cancelReason,
                OrderStatus = order.State.GetOrderStateName(),
                DitErrorCode = ditErrorCode,
                DitErrorMessage = ditErrorMessage
            };
            return dto;
        }
    }
}