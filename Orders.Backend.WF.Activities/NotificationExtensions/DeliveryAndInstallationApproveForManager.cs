﻿using System;
using Orders.Backend.Notification.Model.OnlineOrder.DeliveryAndInstallationApproveForManager;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class DeliveryAndInstallationApproveForManager
    {
        public static DeliveryAndInstallationApproveForManagerNotificationDto ToDeliveryAndInstallationApproveForManagerNotificationDto(this Order order)
        {
            var dto = new DeliveryAndInstallationApproveForManagerNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,                
                Customer = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                    Phone = BeautifulFormatter.FormatTelephone(order.Customer.Phone)
                }
                
            };
            return dto;
        }
    }
}