﻿using System;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.CustomerOrderAtPickup;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class CustomerOrderAtPickup
    {
        public static CustomerOrderAtPickupNotificationDto ToCustomerOrderAtPickupNotificationDto(this Order order)
        {
            var dto = new CustomerOrderAtPickupNotificationDto
            {
                OrderId = order.Id,
                WwsOrderId = order.CurrentWwsOrder.Id,
                ReserveLifeTime = !order.ExpirationDate.HasValue
                    ? BeautifulFormatter.FormatDate(DateTime.Today.Add(order.Store.ReserveLifeTime))
                    : BeautifulFormatter.FormatDate(order.ExpirationDate.Value),
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                },
                SaleLocationInfo = new SaleLocationInfo
                {
                    City = order.Store.CityName,
                    SapCode = order.Store.SapCode,
                    OfficiallyStoreName = order.Store.OfficiallyStoreName,
                    Telephone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                    Address = order.PickupAddress
                },
                TotalCost = order.CurrentWwsOrder.TotalPrice.GrossPrice.ToString("##.##")
            };
            return dto;
        }
    }
}