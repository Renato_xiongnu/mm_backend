﻿using System.Globalization;
using System.Linq;
using Orders.Backend.Notification.Model.BunnyOnlineOrder.DiscontOrderCreated;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.NotificationExtensions
{
    public static class DiscountOrderCreated
    {
        public static DiscountOrderCreatedNotificationDto ToDiscountOrderCreatedNotificationDto(this Order order)
        {
            var dto = new DiscountOrderCreatedNotificationDto
            {
                OrderId = order.Id,
                CustomerInfo = new CustomerInfo
                {
                    FirstName = string.Format("{0} {1}",order.Customer.Surname,order.Customer.Name),
                    Email = order.Customer.Email,
                    Phone = order.Customer.Phone
                },
                OrderLines = order
                    .Items
                    .Select(oi => new OrderLine
                    {
                        Article = oi.Number,
                        TotalPrice = (oi.Price*oi.Quantity).ToString("##.##"),
                        Price = oi.Price.ToString("##.##"),
                        Qty = oi.Quantity,
                        Title = oi.ArticleProductType==ArticleProductType.DeliveryService ? "Артикул Доставки/Самовывоза" : oi.Title
                    }).ToList(),
                DeliveryType = order.DeliveryName,
                TotalPrice = order.Items
                    .Sum(oi => oi.Price*oi.Quantity)
                    .ToString("##.##"),
            };
            return dto;
        }
    }
}