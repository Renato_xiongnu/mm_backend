﻿using System;
using System.Linq;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Commands.SF;
using Newtonsoft.Json;
using Orders.Backend.WF.Activities.Model;
using Customer = MMS.Cloud.Commands.SF.Customer;

namespace Orders.Backend.WF.Activities.CommandExtensions.SF
{
    public static class ReserveProductsExtensions
    {
        public static ReserveProducts ToReserveProducts(this Order order)
        {
            return new ReserveProducts
            {
                SapCode = order.Store.SapCode,
                Contact = new Customer
                {
                    AdditionalPhone = order.Customer.Phone2,
                    Email = order.Customer.Email,
                    FirstName = order.Customer.Name ?? " ",
                    LastName = order.Customer.Surname ?? " ",
                    Phone = order.Customer.Phone ?? " ",
                },
                CustomerOrder = new CustomerOrderInfo
                {
                    Downpayment = order.Prepay,
                    IsOnlineOrder = order.IsOnlineOrder,
                    OrderSource = order.Source.ToString(),
                    PaymentType = order.Payment.ToString(),
                    ShippingMethod = order.Delivery.ShippingMethod.ToString(),
                    SocialCardNumber = order.Customer.SocialCard.Number,
                    Address = order.Delivery.Address,
                    City = order.Delivery.City,
                    DeliveryService = order.Delivery.RequestedDeliveryServiceOption,
                    Floor = order.Delivery.Floor.GetValueOrDefault(),
                    ZipCode = order.Delivery.ZIPCode,
                    DeliveryTimeslot = order.Delivery.RequestedDeliveryTimeslot,
                    RequestedDeliveryDate = order.Delivery.DeliveryDate
                },
                CustomerOrderId = order.Id,
                Items = order.Items.Select(MapItem).ToArray()
            };
        }

        public static CancelReserveProducts ToCancelReserveProducts(this Order order, string storeFulfillmentId)
        {
            return new CancelReserveProducts
            {
                CustomerOrderId = order.Id,
                RequestId = storeFulfillmentId
            };
        }

        public static OrderItemState CalculateItemState(this OrderItem orderItem, ReserveProductsResponse response)
        {
            var reservedItem = response.Items.FirstOrDefault(t => t.ItemId == orderItem.Id);
            if (reservedItem == null) return OrderItemState.NotFound;
            if (reservedItem.ReservedSucesfully) return OrderItemState.Reserved;

            OrderItemState state;
            if (!Enum.TryParse(reservedItem.ReserveStatus, out state))
            {
                state = OrderItemState.NotFound; //TODO Or exception?
            }

            return state;
        }

        private static Item MapItem(this OrderItem orderItem)
        {
            return new Item
            {
                Article = long.Parse(orderItem.Number),
                ArticleProductType = orderItem.ArticleProductType.ToString(),
                Price = orderItem.Price,
                Qty = orderItem.Quantity,
                SerialNumber = orderItem.SerialNumber,
                SubItems = orderItem.SubOrderItems != null ? orderItem.SubOrderItems.Select(MapItem).ToArray() : null,
                ItemId = orderItem.Id,
                Title = orderItem.Title,
            };
        }

        public static string Print(this ReserveProductsProcessingInfo deliverOrderProcessingInfo)
        {
            if (deliverOrderProcessingInfo == null)
            {
                return string.Empty;
            }
            return JsonConvert.SerializeObject(deliverOrderProcessingInfo);
        }
    }
}
