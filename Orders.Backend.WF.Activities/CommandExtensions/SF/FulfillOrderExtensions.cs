﻿using System;
using System.Linq;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Commands.SF;
using Newtonsoft.Json;
using Orders.Backend.WF.Activities.CommandExtensions.Lips;
using Orders.Backend.WF.Activities.Model;
using Customer = MMS.Cloud.Commands.SF.Customer;

namespace Orders.Backend.WF.Activities.CommandExtensions.SF
{
    public static class FulfillOrderExtensions
    {
        public static FulfillOrder ToFulfillOrder(this Order order)
        {
            return new FulfillOrder
            {
                SapCode = order.Store.SapCode,
                Contact = new Customer
                {
                    AdditionalPhone = order.Customer.Phone2,
                    Email = order.Customer.Email,
                    FirstName = order.Customer.Name ?? " ",
                    LastName = order.Customer.Surname ?? " ",
                    Phone = order.Customer.Phone ?? " ",
                },
                CustomerOrder = new CustomerOrderInfo
                {
                    Downpayment = order.Prepay,
                    IsOnlineOrder = order.IsOnlineOrder,
                    OrderSource = order.Source.ToString(),
                    PaymentType = order.Payment.ToString(),
                    ShippingMethod = order.Delivery.ShippingMethod.ToString(),
                    SocialCardNumber = order.Customer.SocialCard.Number,
                    Address = order.Delivery.Address,
                    City = order.Delivery.City,
                    DeliveryService = order.Delivery.RequestedDeliveryServiceOption,
                    Floor = order.Delivery.Floor.GetValueOrDefault(),
                    ZipCode = order.Delivery.ZIPCode,
                    DeliveryTimeslot = DeliverOrderExtensions.ToDeliveryTimeSlot(order.Delivery.RequestedDeliveryTimeslot),
                    RequestedDeliveryDate = order.Delivery.DeliveryDate,
                    PickupLocationId = order.Delivery.PickupLocationId
                },
                CustomerOrderId = order.Id,
                Items = order.Items.Select(MapItem).ToArray()
            };
        }

        public static MMS.Cloud.Commands.SF.Incident LastIncident(this FulfillOrderResponse fulfillCommand)
        {
            return fulfillCommand.Incidents.LastOrDefault();
        }

        public static CancelFulfillOrder ToCancelFulfillOrder(this Order order, string storeFulfillmentId)
        {
            return new CancelFulfillOrder
            {
                CustomerOrderId = order.Id,
                RequestId = storeFulfillmentId
            };
        }

        public static OrderItemState CalculateItemState(this OrderItem orderItem, FulfillOrderResponse response)
        {
            var reservedItem = response.Items.FirstOrDefault(t => t.ItemId == orderItem.Id);
            if (reservedItem == null) return OrderItemState.NotFound;
            if (reservedItem.ReservedSucesfully) return OrderItemState.Reserved;

            OrderItemState state;
            if (!Enum.TryParse(reservedItem.ReserveStatus, out state))
            {
                state = OrderItemState.NotFound; //TODO Or exception?
            }

            return state;
        }

        private static Item MapItem(this OrderItem orderItem)
        {
            return new Item
            {
                Article = long.Parse(orderItem.Number),
                ArticleProductType = orderItem.ArticleProductType.ToString(),
                Price = orderItem.Price,
                Qty = orderItem.Quantity,
                SerialNumber = orderItem.SerialNumber,
                SubItems = orderItem.SubOrderItems != null ? orderItem.SubOrderItems.Select(MapItem).ToArray() : null,
                ItemId = orderItem.Id,
                Title = orderItem.Title,
            };
        }
        
        public static string Print(this FulfillOrderProcessingInfo fulfillOrderProcessingInfo)
        {
            if (fulfillOrderProcessingInfo == null)
            {
                return string.Empty;
            }
            return JsonConvert.SerializeObject(fulfillOrderProcessingInfo);
        }

        public static PostponeExpirationDate ToPostponeExpirationDate(this Order order, string requestId)
        {
            return new PostponeExpirationDate
            {
                CustomerOrderId = order.Id,
                RequestId = requestId,
                NewExpirationDate = order.ExpirationDate.Value
            };
        }
    }
}
