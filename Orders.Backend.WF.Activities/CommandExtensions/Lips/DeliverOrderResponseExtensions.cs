﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using MMS.Cloud.Commands.LIPS;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public static class DeliverOrderResponseExtensions
    {
        private const string LipsErrorsConfigSectionName = "LipsErrors";
        private static readonly Dictionary<int, string> Incidents = new Dictionary<int, string>
        {
            {10001, "Заявка отклонена ТК / не заполнен адрес"},
            {10002, "Заявка отклонена ТК / заказ уже существует"},
            {10003, "Заявка ТК на ручном разборе"},
            {10004, "Комплектация невозможна"},
            {10005, "Упаковка невозможна"},
            {10006, "Не отгружен"},
            {10007, "Невозможно запланировать к запрошенной дате"},
            {10008, "Невалидные данные"},
            {10009, "Посылка не проходит валидацию по требованиям ТК"},
            {10010, "Невозможно назначить перевозчика"},
            {10011, "Невозможно автоматически отменить LRCO"},
            {10012, "Системная ошибка LIPS"},
            {10013, " "},
            {10014, " "},
            {10015, " "},
            {10016, " "},
            {20001, "Неизвестное исключение"}
        }; 

        public static string PrintIncidientsAsHtml(this DeliverOrderResponse response)
        {
            var sb = new StringBuilder();
            if (response.Incidents.Any())
            {
                sb.Append(@"<ol>");
                foreach (var incident in response.Incidents)
                {
                    sb.AppendFormat("<li>{0} ({1}) {2} </li>",
                        GetIncidientDescription(incident.Code),
                        incident.Code,
                        GetMessage(incident.Description));
                }
                sb.Append(@"</ol>");
            }
            else
            {
                sb.Append("<strong>Нет инцидентов</strong>");
            }         
            return sb.ToString();
        }


        public static string PrintIncidientsAsHtml(this TransferGoodsResponse response)
        {
            var sb = new StringBuilder();
            if (response.Incidents.Any())
            {
                sb.Append(@"<ol>");
                foreach (var incident in response.Incidents)
                {
                    sb.AppendFormat("<li>{0} ({1}) {2} </li>",
                        GetIncidientDescription(incident.Code),
                        incident.Code,
                        GetMessage(incident.Description));
                }
                sb.Append(@"</ol>");
            }
            else
            {
                sb.Append("<strong>Нет инцидентов</strong>");
            }
            return sb.ToString();
        }

        private static string GetMessage(string description)
        {
            var messages = description.Split(';')
                .Where(s=>!string.IsNullOrEmpty(s))
                .ToArray();
            var sb = new StringBuilder();
            if (messages.Any())
            {
                sb.Append(@"<ul>");
                foreach (var message in messages)
                {
                    sb.AppendFormat("<li>{0}</li>",message);
                }
                sb.Append(@"</ul>");
            }
            else
            {
                sb.Append("<strong>Нет описания</strong>");
            }
            return sb.ToString();
        }

        private static string GetIncidientDescription(int code)
        {
            var incidents = GetIncidientDictionaryFromConfig();
            return incidents.ContainsKey(code) ? incidents[code] : "Нейзвестный код ошибки";
        }

        private static Dictionary<int, string> GetIncidientDictionaryFromConfig()
        {
           
            var result = new Dictionary<int, string>();

            var section = ConfigurationManager.GetSection(LipsErrorsConfigSectionName) as LipsErrorsConfigSection;
            if (section == null) return result;
            for (var i = 0; i < section.LipsErrorsItems.Count; i++)
            {
                if(!result.ContainsKey(section.LipsErrorsItems[i].LipsErrorKey))
                    result.Add(section.LipsErrorsItems[i].LipsErrorKey, section.LipsErrorsItems[i].LipsErrorDescription);
            }

            return result;
        }
    }
}
