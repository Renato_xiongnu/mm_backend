﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public static class LipsExtensions
    {
        public static string ToStock(this string sapCode)
        {
            return string.Format("MM_stock_{0}", sapCode);
        }
    }
}
