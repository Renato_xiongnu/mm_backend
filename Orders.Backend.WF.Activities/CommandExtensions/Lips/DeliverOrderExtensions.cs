﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using MMS.Cloud.Commands.LIPS;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Properties;
using PaymentType = MMS.Cloud.Commands.LIPS.PaymentType;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public static class DeliverOrderExtensions
    {
        public static DeliverOrder ToDeliverOrder(this Order order)
        {
            //DeliverOrderResponse
            var products = order.Items
                .Where(oi => oi.ArticleProductType != ArticleProductType.DeliveryService)
                .ToArray();
            var delivery = order.Delivery;
            return new DeliverOrder
            {
                RequestingUnit = order.Store.SapCode,
                RequestingSalesChnl = "MMO",
                RequesterName = "",
                LRSourceSystem = "TicketTool",
                CustomerOrder = order.Id,
                COCreatedDate = order.Created,
                COStatus = order.State.ToString(),
                COStatusLastChanged = order.Updated,
                ClientType = ClientType.B2C,
                DestinationType = order.Delivery.HasDelivery ? DeliveryDestinationType.CAD : DeliveryDestinationType.PuP,
                DesiredLSP = "",
                Address = ToAddressInfo(delivery),
                ContactName = order.Customer.Name,
                ContactSurname = order.Customer.Surname,
                ContactPhone = order.Customer.Phone,
                ContactPhone2 = order.Customer.Phone2,
                ContactEmail = order.Customer.Email,
                ReceivingPerson = string.Join(" ", order.Customer.Surname, order.Customer.Name),
                RequestedDeliveryDate = delivery.DeliveryDate.HasValue
                    ? delivery.DeliveryDate.Value.Date
                    : DateTime.MinValue,
                RequestedDeliveryTimeslot = ToDeliveryTimeSlot(delivery.RequestedDeliveryTimeslot),
                PaymentType = order.Payment == Model.PaymentType.Cash ? PaymentType.Cod : PaymentType.Pre,
                RequestedDeliveryServiceOptions =
                    delivery.ShippingMethod == ShippingMethod.Delivery
                        ? ToRequestedServiceOption(delivery.RequestedDeliveryServiceOption)
                        : ServiceOption.None, //todo
                AmountProducts = products.Sum(oi => oi.Quantity*oi.Price),
                AmountDelivery = order.Items
                    .Where(oi => oi.ArticleProductType == ArticleProductType.DeliveryService)
                    .Sum(oi => oi.Quantity*oi.Price),
                AmountLifting = 0,
                CODAmount = order.Payment == Model.PaymentType.Cash
                    ? order.Items
                        .Sum(oi => oi.Quantity*oi.Price)
                    : 0, //TODO Better use order.Prepay, but can't because of SocialProcess bug
                ProductsQty = products.Sum(p => p.Quantity),
                PartialDeliveryAllowed = false,
                Lines = products.SelectMany(oi => ToMultipleOrderLine(oi, order)).ToArray(),
                WWSNumber = order.CurrentWwsOrder.Id,
                WWSSapCode = order.CurrentWwsOrder.SapCode
            };
        }

        /// <summary>
        /// ToDo: когда будет точный формат который должен прийти из корзины пофиксить.
        /// Какая-то уебищная херня, но как нормально сделать, пока не знаю
        /// </summary>
        /// <param name="timeSlot"></param>
        /// <returns></returns>
        public static string ToDeliveryTimeSlot(string timeSlot)
        {
            var defaultTimeSlot = Settings.Default.DefaultRequestedDeliveryTimeslotForDeliver;
            if (string.IsNullOrEmpty(timeSlot))
                return defaultTimeSlot;

            var items = timeSlot.ToUpperInvariant().Split(new[] { "С", "ДО" }, StringSplitOptions.RemoveEmptyEntries);
            if (items.Length != 2)
                return defaultTimeSlot; 
            var first = GetHourValue(items[0]);
            var second = GetHourValue(items[1]);
            if (!string.IsNullOrEmpty(first) && !string.IsNullOrEmpty(second))
            {
                if (second.Length == 1 && second[0] == '0')
                    second = "24";
                return string.Format("{0}-{1}", first, second);
            }
            return defaultTimeSlot;
        }

        private static string GetHourValue(String value)
        {
            var item = value.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
            if (!string.IsNullOrEmpty(item))
            {
                item = item.Trim();
                if (item.Length == 2 && item[0] == '0')
                    return item[1].ToString(CultureInfo.InvariantCulture);
                return item;
            }
            return string.Empty;
        }

        private static AddressInfo ToAddressInfo(Delivery delivery)
        {
            var originalHouse = delivery.House??string.Empty;
            originalHouse = Regex.Replace(originalHouse, @"\\/", "/");
            var house = originalHouse;
            string houseExt = null;
            string building = null;
            string housing = null;
            var houseMath = Regex.Match(originalHouse, @"(ДОМ\s(?<House>[/\d\\-]+)(?<HouseExt>\S+)?)");
            if (houseMath.Success)
            {
                house = houseMath.Groups["House"].Value;
                houseExt = houseMath.Groups["HouseExt"].Value;
                originalHouse = originalHouse.Replace(houseMath.Value, string.Empty);
            }
            var buildingMath = Regex.Match(originalHouse, @"((СТРОЕНИЕ|СООРУЖЕНИЕ|ЛИТЕРА)\s(?<Building>\S+))");
            if (buildingMath.Success)
            {
                building = buildingMath.Groups["Building"].Value;
                originalHouse = originalHouse.Replace(buildingMath.Value, string.Empty);
            }
            var housingMath = Regex.Match(originalHouse, @"(КОРПУС\s(?<Housing>\S+))");
            if (housingMath.Success)
            {
                housing = housingMath.Groups["Housing"].Value;
                originalHouse = originalHouse.Replace(housingMath.Value, string.Empty);
            }
            var apartment = string.IsNullOrEmpty(delivery.Apartment)
                ? string.Empty
                : Regex.Replace(delivery.Apartment, "(ОФИС|КВАРТИРА)", "").Trim();
            if (delivery.HasDelivery)
            {
                return new AddressInfo
                {
                    LocationId = null,
                    AddressPlain = delivery.Address,
                    KLADR = delivery.Kladr,
                    ZIPCode = delivery.ZIPCode,
                    Country = null,
                    Region = delivery.Region,
                    City = delivery.City,
                    Street = delivery.Street,
                    StreetAbbr = delivery.StreetAbbr,
                    Building1 = house,
                    BuildingExt = houseExt,
                    Building2 = housing,
                    Building3 = building,
                    Apartment = apartment,
                    AdditionalInstructions = originalHouse.Trim(),
                    Entrance = delivery.Entrance,
                    EntranceCode = delivery.EntranceCode,
                    Floor = delivery.Floor.HasValue ? delivery.Floor.Value : 0,
                    HasServiceLift = delivery.HasServiceLift.HasValue && delivery.HasServiceLift.Value,
                    SubwayStationName = delivery.SubwayStationName,
                    District = delivery.District
                };
            }
            return new AddressInfo
            {
                LocationId = delivery.PickupLocation.PickupLocationId
            };
        }

        private static ServiceOption ToRequestedServiceOption(string serviceOption)
        {
            ServiceOption serviceOptionEnum;
            Enum.TryParse(serviceOption, out serviceOptionEnum);

            return serviceOptionEnum;
        }

        public static IEnumerable<OrderLine> ToMultipleOrderLine(OrderItem orderItem, Order order)
        {
            if (orderItem.SubOrderItems != null && orderItem.SubOrderItems.Any())
            {
                return orderItem.SubOrderItems.Distinct(new OrderItemComparer()).Select(el => ToOrderLine(el, order));
            }
            return new List<OrderLine>
            {
                ToOrderLine(orderItem,order)
            };
        }

        private static OrderLine ToOrderLine(OrderItem orderItem, Order order)
        {
            return new OrderLine
            {
                ProductNumber = long.Parse(orderItem.Number),
                ProductName = orderItem.Title,
                Manufacturer = null,
                QtyRequested = orderItem.Quantity,
                LineAmount = orderItem.Quantity*orderItem.Price,
                ProductAvailiability = ProductAvailiability.ST,
                StockLocation = order.CurrentWwsOrder.SapCode.ToStock(),
                SapCode = order.CurrentWwsOrder.SapCode,
                WWSOrderId = order.CurrentWwsOrder.Id,
            };
        }
    }
}
