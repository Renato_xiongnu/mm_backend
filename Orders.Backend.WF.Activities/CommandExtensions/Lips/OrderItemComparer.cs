using System;
using System.Collections.Generic;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public class OrderItemComparer : IEqualityComparer<OrderItem>
    {
        public bool Equals(OrderItem x, OrderItem y)
        {
            if (ReferenceEquals(x, y)) return true;

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            return x.Equals(y);
        }

        public int GetHashCode(OrderItem obj)
        {
            return obj == null ? 0 : obj.GetHashCode();
        }
    }
}