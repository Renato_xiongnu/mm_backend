using MMS.Cloud.Commands.LIPS;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public static class CancelDeliveryExtensions
    {
        public static CancelDelivery ToCancelDelivery(this Order order)
        {            
            return new CancelDelivery
            {
                CustomerOrder = order.Id
            };
        }
    }
}