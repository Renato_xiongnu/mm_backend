﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Cloud.Commands.LIPS;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public static class TransferGoodsExtensions
    {
        public static TransferGoods ToTransferOrder(this Order order, string transfer, string transferOut, string sapCode)
        {
            //DeliverOrderResponse
            var products = order.Items 
                .Where(oi => oi.ArticleProductType != ArticleProductType.DeliveryService)
                .ToArray();
            var delivery = order.Delivery;
            return new TransferGoods
            {
                RequestingUnit = order.CurrentWwsOrder.SapCode,
                RequestingSalesChnl = "MMO",
                RequesterName = "",
                LRSourceSystem = "TicketTool",
                OrderId = order.Id,
                OrderCreatedDateTime = order.Created,
                OrderStatus = order.State.ToString(),
                OrderStatusLastChanged = order.Updated,
                ReceivingStockLocation = order.Store.SapCode.ToStock(),
                ContactName = "-",
                ContactSurname = "-",
                ContactPhone = "-",
                ContactPhone2 = string.Empty,
                ContactEmail = "-",
                ReceivingPerson = "-",
                RequestedDeliveryDate = delivery.DeliveryDate.HasValue
                    ? delivery.DeliveryDate.Value.Date
                    : DateTime.MinValue,
                RequestedDeliveryTimeslot = Properties.Settings.Default.DefaultRequestedDeliveryTimeslot,
                TotalAmount = products.Sum(p => p.Price*p.Quantity),
                ProductsQty = products.Sum(p => p.Quantity),
                Lines =
                    products.SelectMany(oi => ToMultipleTransferOrderLine(oi, order, transferOut, sapCode))
                        .ToArray(),
                WWSNumber = transfer,
                WWSSapCode = order.Store.SapCode
                //TODO Уточнить нужно ли считать сервисные артикулы
            };
        }

        public static IEnumerable<OrderLine> ToMultipleTransferOrderLine(OrderItem orderItem, Order order, string transfer,
           string sapCode)
        {
            if (orderItem.SubOrderItems != null && orderItem.SubOrderItems.Any())
            {
                return orderItem.SubOrderItems.Distinct(new OrderItemComparer()).Select(el => ToTransferOrderLine(el, order, transfer, sapCode));
            }
            return new List<OrderLine>
            {
                ToTransferOrderLine(orderItem, order, transfer, sapCode)
            };
        }

        private static OrderLine ToTransferOrderLine(OrderItem orderItem, Order order, string transfer, string sapCode)
        {
            return new OrderLine
            {
                ProductNumber = long.Parse(orderItem.Number),
                ProductName = orderItem.Title,
                Manufacturer = null,
                QtyRequested = orderItem.Quantity,
                LineAmount = orderItem.Quantity * orderItem.Price,
                ProductAvailiability = ProductAvailiability.ST,
                StockLocation = sapCode.ToStock(),
                SapCode = sapCode,
                WWSOrderId = string.IsNullOrEmpty(transfer) ? order.CurrentWwsOrder.Id : transfer,
            };
        }
    }
}
