using MMS.Cloud.Commands.FORD;
using Newtonsoft.Json;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public static class DeliverOrderProcessingInfoExtensions
    {
        public static string Print(this DeliverOrderProcessingInfo deliverOrderProcessingInfo)
        {
            if (deliverOrderProcessingInfo==null)
            {
                return string.Empty;
            }
            return JsonConvert.SerializeObject(deliverOrderProcessingInfo);
        }        
    }
}