﻿using System.Configuration;

namespace Orders.Backend.WF.Activities.CommandExtensions.Lips
{
    public class LipsErrorsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Errors")]
        public LipsErrorsCollection LipsErrorsItems
        {
            get { return ((LipsErrorsCollection)(base["Errors"])); }
        }
    }

    [ConfigurationCollection(typeof(LispErrorElement), AddItemName = "Error")]
    public class LipsErrorsCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LispErrorElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LispErrorElement)(element)).LipsErrorKey;
        }

        public LispErrorElement this[int idx]
        {
            get { return (LispErrorElement)BaseGet(idx); }
        }

        //// Slight hack to look up the direct value property of the ConfigConfigurationElement from the collection indexer
        //public new string this[string key]
        //{
        //    get
        //    {
        //        var lispErrorElement = base[key] as LispErrorElement;
        //        if (lispErrorElement != null)
        //            return lispErrorElement.LipsErrorDescription;
        //        return 
        //    }
        //}
    }


    public class LispErrorElement : ConfigurationElement
    {

        [ConfigurationProperty("lipsErrorKey", DefaultValue="-1", IsKey=true, IsRequired=true)]
        public int LipsErrorKey
        {
            get { return ((int)(base["lipsErrorKey"])); }
            set { base["lipsErrorKey"] = value; }
        }

        [ConfigurationProperty("lipsErrorDescription", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string LipsErrorDescription
        {
            get { return ((string)(base["lipsErrorDescription"])); }
            set { base["lipsErrorDescription"] = value; }
        }
    }
}
