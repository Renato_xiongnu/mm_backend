﻿using System;
using System.Activities;
using System.ComponentModel;
using Common.Proxy.Interfaces;
using Common.Proxy.Octopus;
using Orders.Backend.WF.Activities.Integration.Octopus.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Model.Integration.Octopus;

namespace Orders.Backend.WF.Activities.Integration.Octopus
{
    [Designer(typeof(OctopusGetShortCampaingInfoAsyncActivityDesigner))]
    public class OctopusGetShortCampaingInfoAsyncActivity : AsyncCodeActivity<ShortCampaingInfo>
    {
        private readonly IOctopusProvider _octopusProvider;

        [Browsable(false)]
        public InArgument<string> CampaignId { get; set; }

        public OctopusGetShortCampaingInfoAsyncActivity(IOctopusProvider octopusProvider)
        {
            _octopusProvider = octopusProvider;
        }

        public OctopusGetShortCampaingInfoAsyncActivity()
            : this(new OctopusProvider())
        {
        }

        protected override IAsyncResult BeginExecute(AsyncCodeActivityContext context, AsyncCallback callback, object state)
        {
            var campaignId = CampaignId.Get(context);
            return _octopusProvider.BeginGetShortCampaingInfo(campaignId, callback, state);
        }

        protected override ShortCampaingInfo EndExecute(AsyncCodeActivityContext context, IAsyncResult result)
        {
            return _octopusProvider.EndGetShortCampaingInfo(result).ConvertTo<ShortCampaingInfo>();
        }
    }
}