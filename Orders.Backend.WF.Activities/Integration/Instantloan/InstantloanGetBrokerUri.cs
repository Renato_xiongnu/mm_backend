﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Integration.Instantloan.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Instantloan;

namespace Orders.Backend.WF.Activities.Integration.Instantloan
{
    [Designer(typeof(GetBrokerUriDesigner))]
    public class InstantloanGetBrokerUri : InstantloanApiCallBase, IActivityTemplateFactory
    {
        private ActivityFunc<Order,string, Proxy.Instantloan.GetBrokerUriResponse> Flow { get; set; }

        private ActivityFunc<Proxy.Instantloan.GetBrokerUriResponse, string> SetBrokerUrl { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        [Browsable(false)]
        public InArgument<string> UserLogin { get; set; }

        [Browsable(false)]
        public OutArgument<string> BrokerUrl { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
       
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var userLogin = new RuntimeArgument("UserLogin", typeof(string), ArgumentDirection.In, false);
            metadata.Bind(this.UserLogin, userLogin);
            metadata.AddArgument(userLogin);

            var brokerUrl = new RuntimeArgument("BrokerUrl", typeof(string), ArgumentDirection.Out, true);
            metadata.Bind(this.BrokerUrl, brokerUrl);
            metadata.AddArgument(brokerUrl);

            var orderArg = new DelegateInArgument<Order>("Order");
            var userLoginArg = new DelegateInArgument<string>("UserLogin");
            var client = new Variable<IInstantLoan_MM_TT>("InstantloanClient");
            var resultOutArg = new DelegateOutArgument<GetBrokerUriResponse>("ResultOut");

            this.Flow = new ActivityFunc<Order,string, GetBrokerUriResponse>
                {
                    Result = resultOutArg,
                    Argument1 = orderArg,
                    Argument2 = userLoginArg,
                    Handler = new ThrowableFailover()
                    {
                        DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                        ExceptionsToThrow = new Collection<Type>() { typeof(FaultException) },
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler =
                                                new Sequence
                                                    {
                                                        Variables = {client},
                                                        Activities =
                                                            {
                                                                new Assign
                                                                    {
                                                                        To =
                                                                            new OutArgument<IInstantLoan_MM_TT>(
                                                                                client),
                                                                        Value =
                                                                            new InArgument<IInstantLoan_MM_TT>(
                                                                                c =>
                                                                                WebService ??
                                                                                new InstantLoan_MM_TTClient())
                                                                    },
                                                                new Assign()
                                                                    {
                                                                        To =
                                                                            new OutArgument<GetBrokerUriResponse>(
                                                                                resultOutArg),
                                                                        Value =
                                                                            new InArgument<GetBrokerUriResponse>(
                                                                                c =>
                                                                                client.Get(c)
                                                                                      .GetBrokerUri(
                                                                                          orderArg.Get(c)
                                                                                                  .ToGetBrokerUriRequest
                                                                                              (userLoginArg.Get(c))
                                                                                    ))
                                                                    },
                                                            }
                                                    }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = this.Order.Get(context);
            var userLogin = this.UserLogin != null ? this.UserLogin.Get(context) : string.Empty;

            context.ScheduleFunc(this.Flow, order, userLogin, OnGetUriCompleted, OnFaulted);
        }

        private void OnGetUriCompleted(NativeActivityContext context, ActivityInstance completedinstance, GetBrokerUriResponse result)
        {
            switch (result.Body.@return.ResultCode)
            {
                case TypeResultCode.Success:
                    {
                        BrokerUrl.Set(context,result.Body.@return.Uri);
                        break;
                    }
                case TypeResultCode.Warning:
                    {
                        BrokerUrl.Set(context, result.Body.@return.Uri);
                        var order = Order.Get(context);
                        var sendMessage = new InstantloanNotificationMessage()
                        {
                            OrderId = order.Id,
                            Text = result.Body.@return.ErrorMessage
                        };
                        context.ScheduleAction(WarningNotificationAction, sendMessage);
                        break;
                    }
                case TypeResultCode.Error:
                    {
                        BrokerUrl.Set(context, result.Body.@return.Uri);
                        var order = Order.Get(context);
                        var sendMessage = new InstantloanNotificationMessage()
                        {
                            OrderId = order.Id,
                            Text = result.Body.@return.ErrorMessage
                        };
                        context.ScheduleAction(WarningNotificationAction, sendMessage);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        protected void OnFaulted(NativeActivityFaultContext faultContext, Exception propagatedException,
                               ActivityInstance propagatedFrom)
        {
            var order = Order.Get(faultContext);
            var sendMessage = new InstantloanNotificationMessage()
            {
                OrderId = order.Id,
                Text = propagatedException.Message
            };
            faultContext.ScheduleAction(ExceptionNotificationAction, sendMessage);
        }

        public Activity Create(DependencyObject target)
        {
            var update = new InstantloanGetBrokerUri();
            var notification = new MMS.Notification.Activities.Notification();
            update.WarningNotificationAction = new ActivityAction<InstantloanNotificationMessage>
            {
                Handler = notification.Create(target, null),
                Argument = new DelegateInArgument<InstantloanNotificationMessage>()
            };

            update.ErrorNotificationAction = new ActivityAction<InstantloanNotificationMessage>
            {
                Handler = notification.Create(target, null),
                Argument = new DelegateInArgument<InstantloanNotificationMessage>()
            };

            update.ExceptionNotificationAction = new ActivityAction<InstantloanNotificationMessage>
            {
                Handler = notification.Create(target, null),
                Argument = new DelegateInArgument<InstantloanNotificationMessage>()
            };

            return update;
        }
    }
}
