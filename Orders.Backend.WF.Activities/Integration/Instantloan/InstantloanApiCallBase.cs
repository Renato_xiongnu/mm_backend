﻿using System.Activities;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Instantloan;

namespace Orders.Backend.WF.Activities.Integration.Instantloan
{
    public abstract class InstantloanApiCallBase : NativeActivity
    {
        public IInstantLoan_MM_TT WebService { get; set; }

        public ActivityAction<InstantloanNotificationMessage> WarningNotificationAction { get; set; }

        public ActivityAction<InstantloanNotificationMessage> ErrorNotificationAction { get; set; }

        public ActivityAction<InstantloanNotificationMessage> ExceptionNotificationAction { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {       
            WarningNotificationAction.Argument.Name = "InstantloanCallResponse";
            metadata.AddDelegate(WarningNotificationAction);

            ErrorNotificationAction.Argument.Name = "InstantloanCallResponse";
            metadata.AddDelegate(ErrorNotificationAction);

            ExceptionNotificationAction.Argument.Name = "InstantloanCallResponse";
            metadata.AddDelegate(ExceptionNotificationAction);
        }
    }
}
