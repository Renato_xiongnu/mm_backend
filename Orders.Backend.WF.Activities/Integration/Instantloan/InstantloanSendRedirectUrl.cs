﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Integration.Instantloan.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Instantloan;

namespace Orders.Backend.WF.Activities.Integration.Instantloan
{
    [Designer(typeof(SendRedirectUrlDesigner))]
    public class InstantloanSendRedirectUrl : InstantloanApiCallBase,IActivityTemplateFactory
    {
        private ActivityFunc<Order, string, Proxy.Instantloan.Result> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<string> RedirectUrl { get; set; }
        
        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
       
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var redirectUrl = new RuntimeArgument("RedirectUrl", typeof(string), ArgumentDirection.In, true);
            metadata.Bind(this.RedirectUrl, redirectUrl);
            metadata.AddArgument(redirectUrl);

            var orderArg = new DelegateInArgument<Order>("Order");
            var redirectUrlArg = new DelegateInArgument<string>("RedirectUrl");
            var client = new Variable<IInstantLoan_MM_TT>("InstantloanClient");

            var result = new DelegateOutArgument<Result>("Result");

            this.Flow = new ActivityFunc<Order, string, Proxy.Instantloan.Result>()
                {
                    Argument1 = orderArg,
                    Argument2=redirectUrlArg,
                    Result = result,
                    Handler = new ThrowableFailover()
                    {
                        DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                        ExceptionsToThrow = new Collection<Type>() { typeof(FaultException) },
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler =
                                                new Sequence
                                                    {
                                                        Variables = {client},
                                                        Activities =
                                                            {
                                                                new Assign
                                                                    {
                                                                        To =
                                                                            new OutArgument<IInstantLoan_MM_TT>(
                                                                                client),
                                                                        Value =
                                                                            new InArgument<IInstantLoan_MM_TT>(
                                                                                c =>
                                                                                WebService ??
                                                                                new InstantLoan_MM_TTClient())
                                                                    },
                                                                new Assign()
                                                                    {
                                                                        To =
                                                                            new OutArgument<Result>(
                                                                                result),
                                                                        Value =
                                                                            new InArgument<Result>(
                                                                                c =>
                                                                                client.Get(c)
                                                                                      .UpdateUriForRedirection(
                                                                                          orderArg.Get(c)
                                                                                                  .ToUpdateUriForRedirectionRequest
                                                                                              (redirectUrlArg.Get(c))
                                                                                    ).Body.retun)
                                                                    },
                                                            }
                                                    }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = this.Order.Get(context);
            var redirectUrl = this.RedirectUrl.Get(context);
            context.ScheduleFunc(this.Flow, order, redirectUrl, OnUpdateCompleted, OnFaulted);
        }

        protected void OnUpdateCompleted(NativeActivityContext context, ActivityInstance completedinstance,
                            Result result)
        {
            switch (result.ResultCode)
            {
                case TypeResultCode.Success:
                    {
                        break;
                    }
                case TypeResultCode.Warning:
                    {
                        var order = Order.Get(context);
                        var sendMessage = new InstantloanNotificationMessage()
                        {
                            OrderId = order.Id,
                            Text = result.ErrorMessage
                        };
                        context.ScheduleAction(WarningNotificationAction, sendMessage);
                        break;
                    }
                case TypeResultCode.Error:
                    {
                        var order = Order.Get(context);
                        var sendMessage = new InstantloanNotificationMessage()
                        {
                            OrderId = order.Id,
                            Text = result.ErrorMessage
                        };
                        context.ScheduleAction(ErrorNotificationAction, sendMessage);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        protected void OnFaulted(NativeActivityFaultContext faultContext, Exception propagatedException,
                               ActivityInstance propagatedFrom)
        {
            var order = Order.Get(faultContext);
            var sendMessage = new InstantloanNotificationMessage()
            {
                OrderId = order.Id,
                Text = propagatedException.Message
            };
            faultContext.ScheduleAction(ExceptionNotificationAction, sendMessage);
        }

        public Activity Create(DependencyObject target)
        {
            var update = new InstantloanSendRedirectUrl();
            var notification = new MMS.Notification.Activities.Notification();
            update.WarningNotificationAction = new ActivityAction<InstantloanNotificationMessage>
            {
                Handler = notification.Create(target, null),
                Argument = new DelegateInArgument<InstantloanNotificationMessage>()
            };

            update.ErrorNotificationAction = new ActivityAction<InstantloanNotificationMessage>
            {
                Handler = notification.Create(target, null),
                Argument = new DelegateInArgument<InstantloanNotificationMessage>()
            };

            update.ExceptionNotificationAction = new ActivityAction<InstantloanNotificationMessage>
            {
                Handler = notification.Create(target, null),
                Argument = new DelegateInArgument<InstantloanNotificationMessage>()
            };

            return update;
        }
    }
}
