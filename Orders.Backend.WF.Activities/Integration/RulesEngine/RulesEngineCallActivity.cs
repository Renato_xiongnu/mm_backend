﻿using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using MMS.Activities;
using MMS.Activities.Model;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http.Formatting;
using Orders.Backend.WF.Activities.Model;
using MMS.Activities.Logging;

namespace Orders.Backend.WF.Activities.Integration.RulesEngine
{
    public class RulesEngineCallActivity : ExecuteExternalServiceWithTwoArguments<string, OrderContext, string>
    {
        public override string Workitem1Name
        {
            get { return "MethodUrl"; }
        }

        public override string Workitem2Name
        {
            get { return "OrderContext"; }
        }

        protected override ExecutingResult<string> ExecuteService(string methodUrl, OrderContext order)
        {            
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(Properties.Settings.Default.RulesEngineHostUrl);
                var url = new Uri(new Uri(Properties.Settings.Default.RulesEngineHostUrl), methodUrl);
                Logger.InfoFormat("{0} : RulesEngineCallActivity Call Service - Url: {1}", order.Order.Id, url);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.PostAsJsonAsync(url.AbsoluteUri, order).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<string>().Result;
                    Logger.InfoFormat("{0} : RulesEngineCallActivity Call Service - Result: {1}", order.Order.Id, result);
                    return new ExecutingResult<string>
                    {
                        Success = true,
                        Result = result
                    };
                }
                else
                {
                    Logger.ErrorFormat("{0} : RulesEngineCallActivity error - Code: {1}, ReasonPhrase: {2}", order.Order.Id, response.StatusCode, response.ReasonPhrase);
                    throw new Exception(response.ReasonPhrase);
                    //return new ExecutingResult<string>
                    //{
                    //    Success = false,
                    //    Result = null
                    //};
                }
            }
        }

        //public static async Task<T> CallService<T>(string url, HttpMethod method, string contentType)
        //{
        //    var client = new HttpClient();
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));

        //    if (method == HttpMethod.Post)
        //    {
        //    }
        //    else
        //    {
        //    }
        //    HttpResponseMessage response = await client.GetAsync(url);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        var result = await response.Content.ReadAsAsync<T>();
        //        return result;
        //    }
        //    else
        //    {
        //        return default(T);
        //    }
        //}
    }
}
