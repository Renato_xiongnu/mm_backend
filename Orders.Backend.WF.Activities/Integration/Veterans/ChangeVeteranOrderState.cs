using System;
using System.Activities;
using MMS.Activities;
using MMS.Activities.Extensions;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.VeteransService;
using OrderState = Orders.Backend.WF.Activities.Proxy.VeteransService.OrderState;

namespace Orders.Backend.WF.Activities.Integration.Veterans
{
    public abstract class ChangeVeteranOrderState : ExecuteExternalService<Order, string>
    {
        public override string WorkitemName
        {
            get { return "Order"; }
        }

        protected override ExecutingResult<string> ExecuteService(Order workitem)
        {
            var result = new ExecutingResult<string>
            {
                Success = true
            };
            using(var client = new VeteransServiceClient())
            {
                var newState = GetNewState(workitem);
                var changeStateResult = client.ChangeState(new ChangeStateRequest
                {
                    OrderId = workitem.Id,
                    OrderState = newState
                });
                switch(changeStateResult.Status)
                {
                    case Status.Ok:                        
                        result.Message = changeStateResult.ErrorMessage;
                        result.Result = "OK";
                        result.Success = true;
                        break;
                    case Status.Error:
                        result.Message = changeStateResult.ErrorMessage;
                        result.Result = changeStateResult.Error;
                        result.Success = false;
                        break;                    
                    default:
                        throw new ApplicationException(string.Format("ChangeState to {0} exception. Error: {1}. Message: {2}", 
                            newState,
                            changeStateResult.Error,
                            changeStateResult.ErrorMessage));
                }
            }
            return result;
        }

        protected abstract OrderState GetNewState(Order workitem);

        protected override void Execute(NativeActivityContext context)
        {
            var newState = GetNewState(Workitem.Get(context));
            context.LogInfoFormat( "Begin ChangeVeteranOrderState to {0}.", newState);
            base.Execute(context);
        }

        protected override void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, ExecutingResult<string> result)
        {
            var newState = GetNewState(Workitem.Get(context));
            context.LogInfoFormat(
                result.Success ? "ChangeVeteranOrderState to {0} Success. Message: {1}" : "ChangeState to {0} Fail. Message: {1}",
                newState,
                result.Message);

            base.OnCompleted(context, completedinstance, result);
        }
    }


    public abstract class ChangeVeteranOrderState2 : CodeActivity<ExecutingResult<string>>
    {
        public InArgument<Order> Order { get; set; } 

        protected override ExecutingResult<string> Execute(CodeActivityContext context)
        {
            var result = new ExecutingResult<string>
            {
                Success = true
            };
            var workitem = context.GetValue(Order);
            using (var client = new VeteransServiceClient())
            {
                var newState = GetNewState(workitem);
                try
                {
                    var changeStateResult = client.ChangeState(new ChangeStateRequest
                    {
                        OrderId = workitem.Id,
                        OrderState = newState
                    });
                    switch (changeStateResult.Status)
                    {
                        case Status.Ok:
                            result.Message = changeStateResult.ErrorMessage;
                            result.Result = "OK";
                            result.Success = true;
                            break;
                        case Status.Error:
                            result.Message = changeStateResult.ErrorMessage;
                            result.Result = changeStateResult.Error;
                            result.Success = false;
                            break;
                        default:
                            result.Message = changeStateResult.ErrorMessage;
                            result.Result = "Exception";
                            result.Success = false;
                            break;
                    }

                    return result;
                }
                catch (Exception e)
                {
                    result.Message = e.Message;
                    result.Result = "Exception";
                    result.Success = false;

                    return result;
                }
            }
        }

        protected abstract OrderState GetNewState(Order workitem);
    }

    public class ChangeVeteranOrderStateToCanceled2 : ChangeVeteranOrderState2
    {
        protected override OrderState GetNewState(Order workitem)
        {
            return OrderState.Canceled;
        }
    }

    public class ChangeVeteranOrderStateToPaid2 : ChangeVeteranOrderState2
    {
        protected override OrderState GetNewState(Order workitem)
        {
            return OrderState.Paid;
        }
    }
}