﻿using Orders.Backend.WF.Activities.Model;
using OrderState = Orders.Backend.WF.Activities.Proxy.VeteransService.OrderState;

namespace Orders.Backend.WF.Activities.Integration.Veterans
{
    public class ChangeVeteranOrderStateToCanceled : ChangeVeteranOrderState
    {
        protected override OrderState GetNewState(Order workitem)
        {
            return OrderState.Canceled;
        }
    }
}