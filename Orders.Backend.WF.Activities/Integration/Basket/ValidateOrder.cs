﻿using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Integration.Basket.Design;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.Integration.Basket
{
    [Designer(typeof(ValidateOrderActivityDesigner))]
    public class ValidateOrder:NativeActivity
    {
        private ActivityFunc<Order, ValidationResult> ValidateOrderBasket { get; set; } 

        public InArgument<Order> Order { get; set; }

        public OutArgument<ValidationResult> ValidationResult { get; set; }
        
        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof (Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);

            var validationResult = new RuntimeArgument("ValidationResult", typeof(ValidationResult), ArgumentDirection.Out);
            metadata.Bind(ValidationResult, validationResult);
            metadata.AddArgument(validationResult);

            var orderArg = new DelegateInArgument<Order>("Order");
            var validationResultArg = new DelegateOutArgument<ValidationResult>("ValidationResult");
            ValidateOrderBasket = new ActivityFunc<Order, ValidationResult>()
                {
                    Argument = orderArg,
                    Result = validationResultArg,
                    Handler = new Failover()
                        {
                            DisplayName = string.IsNullOrEmpty(DisplayName)?GetType().FullName:DisplayName,
                            Delegate = new InvokeDelegate()
                                {
                                    Delegate = new ActivityAction()
                                        {
                                            Handler = new Assign()
                                                {
                                                    To = new OutArgument<ValidationResult>(validationResultArg),
                                                    Value = new InArgument<ValidationResult>(c =>
                                                            (new Proxy.Basket.BasketServiceClient()
                                                                .Validate(orderArg.Get(c).ToValidateRequest())
                                                                .ToValidationResult())
                                                        )
                                                }
                                        }
                                }
                        }
                };

            metadata.AddDelegate(ValidateOrderBasket);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(ValidateOrderBasket, Order.Get(context), OnValidateCompleted);
        }

        protected void OnValidateCompleted(NativeActivityContext context, ActivityInstance completedInstance,
                                           ValidationResult result)
        {
            this.ValidationResult.Set(context, result);
        }
    }
}
