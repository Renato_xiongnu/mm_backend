﻿using System.Activities;
using System.Runtime.Remoting.Proxies;
using MMS.Activities;
using MMS.Activities.Logging;
using MMS.Activities.Model;
using OnlineOrders.Common.Helpers;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.ExpertSenderService;

namespace Orders.Backend.WF.Activities.Integration.ExpertSender
{
    public class SendExpertSenderMail:ExecuteExternalServiceWithTwoArguments<Model.Order,string,long>
    {
        public override string Workitem1Name
        {
            get { return "Order"; }
        }

        public override string Workitem2Name
        {
            get { return "MailType"; }
        }

        protected override ExecutingResult<long> ExecuteService(Model.Order order, string mailType)
        {
            using (var client = new NotificationServiceClient())
            {
                var resulteMessage = order.ToExpertSenderOrder().ToJson();
                Logger.InfoFormat("Start sending ES {0}, mail type {1}", resulteMessage, mailType);
                var result = client.Send(order.ToExpertSenderOrder().ToJson(), mailType);
                Logger.InfoFormat("Sending to ES result: {0}", result);

                return new ExecutingResult<long> {Success = true, Result = result};
            }
        }
    }
}
