﻿using System;
using System.Linq;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.NotificationExtensions;

namespace Orders.Backend.WF.Activities.Integration.ExpertSender
{
    public static class ExpertSenderExtensions
    {
        public static Order ToExpertSenderOrder(this Model.Order order)
        {
            return new Order
            {
                OrderId = order.Id,
                Created = order.Created,
                Customer = new Customer
                {
                    Email = order.Customer.Email,
                    FirstName = order.Customer.Name,
                    LastName = order.Customer.Surname,
                    IsEmployee = false, //false,
                    Phone = order.Customer.Phone,
                },
                Delivery = new Delivery
                {
                    Address = order.Delivery.Address,
                    City = order.Delivery.City,
                    DeliveryDate = order.Delivery.DeliveryDate,
                    PickupLocationId =
                        order.Delivery.PickupLocation != null
                            ? order.Delivery.PickupLocation.PickupLocationId
                            : string.Format("pickup_{0}",
                                order.Store.SapCode)
                },
                DeliveryType =
                    order.Delivery.HasDelivery ? DeliveryType.Delivery.ToString() : DeliveryType.Pickup.ToString(),
                OrderLines = order.Items.Select(ToExpertSenderOrderLine).ToList(),
                OrderQuantity = order.Items.Sum(el => el.Quantity),
                OrderStatus = order.State.ToString(),
                OrderStatusComment = order.NewStateReason,
                Value = order.Items.Sum(el => el.Quantity*el.Price),
                PaymentStatus = order.State == OrderState.Paid || order.State == OrderState.Closed ? "Paid" : "NotPaid",
                PaymentType = order.Payment.ToString(),
                ReservedUntilDate =
                    !order.ExpirationDate.HasValue
                        ? DateTime.UtcNow.Add(order.Store.ReserveLifeTime)
                        : order.ExpirationDate.Value,
                SaleLocation = new SaleLocation
                {
                    City = order.Store.CityName,
                    Name = order.Store.OfficiallyStoreName,
                    Phone = BeautifulFormatter.FormatTelephone(order.Store.Telephone),
                    SaleLocationId = string.Format("shop_{0}", order.Store.SapCode), //InitnalSapCode
                    YandexMarketReviewUrl = order.YandexMarketUrl
                },
                OrderSource = order.Source.ToString()
            };
        }

        private static OrderLine ToExpertSenderOrderLine(OrderItem orderLine, int number)
        {
            return new OrderLine
            {
                Article = orderLine.Number,
                Number = number,
                Price = orderLine.Price,
                Quantity = orderLine.Quantity,
                Title = orderLine.Title,
                Value = orderLine.Price*orderLine.Quantity,
                ArticleProductType = orderLine.ArticleProductType.ToString()
            };
        }
    }
}
