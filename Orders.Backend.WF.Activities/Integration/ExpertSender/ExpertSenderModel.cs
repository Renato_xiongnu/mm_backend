﻿using System;
using System.Collections.Generic;

namespace Orders.Backend.WF.Activities.Integration.ExpertSender
{
    public class Order
    {
        public string OrderId { get; set; }
        public DateTime Created { get; set; }
        public int OrderQuantity { get; set; }
        public decimal Value { get; set; }
        public string OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string OrderSource { get; set; }
        public string OrderStatusComment { get; set; }
        public string PaymentType { get; set; }
        public string DeliveryType { get; set; }
        public DateTime? ReservedUntilDate { get; set; }
        public Delivery Delivery { get; set; }
        public SaleLocation SaleLocation { get; set; }
        public Customer Customer { get; set; }
        public List<OrderLine> OrderLines { get; set; }
    }

    public enum PaymentType
    {
        Cash = 0,
        Online = 1,
        OnlineCredit = 2,
        SocialCard = 3
    }

    public enum PaymentStatus
    {
    }

    public enum DeliveryType
    {
        Delivery,
        Pickup
    }

    public class Delivery
    {
        public DateTime? DeliveryDate { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PickupLocationId { get; set; }
    }

    public class SaleLocation
    {
        public string SaleLocationId { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string YandexMarketReviewUrl { get; set; }
    }

    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsEmployee { get; set; }
    }

    public class OrderLine
    {
        public string Article { get; set; }
        public int Number { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Value { get; set; }
        public string Title { get; set; }
        public string ArticleProductType { get; set; }
    }
}
