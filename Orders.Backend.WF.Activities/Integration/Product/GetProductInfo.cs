﻿using MMS.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.ProductInfoService;

namespace Orders.Backend.WF.Activities.Integration.Product
{
    public class GetProductInfo : ExecuteExternalService<long[], ProductInfo[]>
    {
        public override string WorkitemName
        {
            get { return "Articles"; }
        }

        protected override ExecutingResult<ProductInfo[]> ExecuteService(long[] articles)
        {
            using(var client = new ProductInfoServiceClient())
            {
                return new ExecutingResult<ProductInfo[]>
                {
                    Result = client.GetProductInfo(articles)
                };
            }
        }
    }
}