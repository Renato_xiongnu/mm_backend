﻿using System;
using System.Linq;
using MMS.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Online;
using Orders.Backend.WF.Activities.Proxy.ProductBackend2;
using Orders.Backend.WF.Activities.Proxy.SaleLocationSelection;
using ArticleProductType = Orders.Backend.WF.Activities.Model.ArticleProductType;
using ReturnCode = Orders.Backend.WF.Activities.Proxy.Online.ReturnCode;
using SaleLocationOutcome = Orders.Backend.WF.Activities.Proxy.SaleLocationSelection.SaleLocationOutcome;

namespace Orders.Backend.WF.Activities.Integration.Product
{
    public class CheckProductAvailability : ExecuteExternalService<Order, bool>
    {
        public override string WorkitemName
        {
            get { return "Order"; }
        }

        protected override ExecutingResult<bool> ExecuteService(Order order)
        {
            var result = new ExecutingResult<bool>
            {
                Result = true
            };
            if(order.Store.IsVirtual)
            {
                result.Result = false;
                result.Message = string.Format("{0} выбран виртуальный магазин", order.Store.SapCode);
                return result;
            }
            int article;
            GetArticleStockStatusResponse getArticleStockStatusResult;
            var productOrderItems = order
                   .Items
                   .Where(
                       i =>
                           i.ArticleProductType == ArticleProductType.Product ||
                           i.ArticleProductType == ArticleProductType.Set)
                           .Where(i => int.TryParse(i.Number, out article))
                           .Where(i => int.Parse(i.Number) != order.Store.PaidDeliveryArticleId)
                           .Where(i => int.Parse(i.Number) != order.Store.FreeDeliveryArticleId)
                           .ToArray();
            using(var client = new OrdersBackendServiceClient())
            {               
                var getItemsRequest = new GetItemsRequest
                {
                    Channel = "MM",
                    SaleLocation = string.Format("shop_{0}", order.Store.SapCode),
                    Articles = productOrderItems
                        .Select(i => i.Number).ToArray(),
                };
                getArticleStockStatusResult = client.GetArticleStockStatus(getItemsRequest);
            }
            var enough =
                productOrderItems                
                .All(
                    ol => getArticleStockStatusResult.Items.Any(obi =>
                        obi.Article == long.Parse(ol.Number)
                        && obi.Qty >= ol.Quantity
                        && obi.Status == StockStatusEnum.InStock));
            if(!enough)
            {
                result.Result = false;
                result.Message = string.Format("{0} нет некоторых артиколов в нужном количестве, согласно сервису остатков", order.Store.SapCode);
                return result;
            }
            ReserveLine[] stockReserveLines;
            using(var client = new OrderServiceClient())
            {
                var gerReservesResult = client.GetReservesByOrderId(order.Id);
                if(gerReservesResult.ReturnCode != ReturnCode.Ok)
                {
                    throw new ApplicationException(gerReservesResult.ErrorMessage);
                }
                stockReserveLines = gerReservesResult.Reserves
                    .Where(r => r.SapCode == order.Store.SapCode)
                    .SelectMany(r => r.ReserveLines)
                    .Where(rl => rl.ReviewItemState != ReviewItemState.Reserved)
                    .Where(rl => rl.ReviewItemState != ReviewItemState.Empty)
                    .ToArray();
            }
            enough = order.Items.All(ol => stockReserveLines.All(obi => obi.ArticleData.ArticleNum != ol.Number));
            if (!enough)
            {
                result.Result = false;
                result.Message = string.Format("{0} не подтвердил наличие некоторых артикулов", order.Store.SapCode);
                return result;
            }
            SaleLocationOutcome[] outcomes;
            using (var client = new SaleLocationServiceClient())
            {
                var locationOutcomesResult = client.GetSaleLocationOutcomes(order.Id);
                if (locationOutcomesResult.ReturnCode == Proxy.SaleLocationSelection.ReturnCode.Error)
                {
                    throw new ApplicationException(locationOutcomesResult.ErrorMessage);
                }
                outcomes = locationOutcomesResult
                    .Result
                    .Where(o => o.SapCode == order.Store.SapCode)
                    .ToArray();
            }
            enough = outcomes.All(o => o.Outcome != "Время истекло");
            if (!enough)
            {
                result.Result = false;
                result.Message = string.Format("{0} есть исход \"Время истекло\"", order.Store.SapCode);
                return result;
            }
            return result;
        }
    }
}