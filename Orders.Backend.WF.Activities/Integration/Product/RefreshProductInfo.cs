﻿using System.Globalization;
using System.Linq;
using MMS.Activities;
using MMS.Activities.Logging;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.ProductInfoService;
using ProductType = Orders.Backend.WF.Activities.Proxy.ProductInfoService.ProductType;

namespace Orders.Backend.WF.Activities.Integration.Product
{
    public class RefreshProductInfo : ExecuteExternalService<Order, ProductInfo[]>
    {
        public override string WorkitemName
        {
            get { return "Order"; }
        }

        protected override ExecutingResult<ProductInfo[]> ExecuteService(Order order)
        {
            long number;
            var articles = order.Items
                .Where(oi => long.TryParse(oi.Number,out number))
                .Select(oi => long.Parse(oi.Number))
                .ToArray();
            using(var client = new ProductInfoServiceClient())
            {
                var productInfos = client.GetProductInfo(articles);
                foreach(var productInfo in productInfos)
                {
                    var orderItem =
                        order.Items
                            .FirstOrDefault(
                                oi => oi.Number == productInfo.Article.ToString(CultureInfo.InvariantCulture));
                    if(orderItem == null)
                    {
                        continue;
                    }
                    orderItem.Title = string.IsNullOrEmpty(productInfo.Title)
                        ? orderItem.Title
                        : productInfo.Title;
                    if (orderItem.ArticleProductType != ArticleProductType.Product) continue; //Если знаем тип товара, то не меняем его
                    orderItem.ArticleProductType = ToArticleProductType(productInfo.ProductType);
                }
                return new ExecutingResult<ProductInfo[]>
                {
                    Result = productInfos
                };
            }
        }

        private ArticleProductType ToArticleProductType(ProductType productType)
        {
            switch(productType)
            {
                case ProductType.Product:
                    return ArticleProductType.Product;
                case ProductType.Set:
                    return ArticleProductType.Set;
                case ProductType.WarrantyPlus:
                    return ArticleProductType.WarrantyPlus;
                case ProductType.InstallationService:
                    return ArticleProductType.InstallationService;
                case ProductType.DeliveryService:
                    return ArticleProductType.DeliveryService;
                default:
                {
                    Logger.ErrorFormat("Can't cast {0} to ArticleProductType", productType);
                    return ArticleProductType.Product;
                }
            }
        }
    }
}