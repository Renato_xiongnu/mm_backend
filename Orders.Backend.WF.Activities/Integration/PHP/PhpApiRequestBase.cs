﻿using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.Integration.PHP
{
    public abstract class PhpApiRequestBase<T,P> : NativeActivity, IBasketApiCall 
    {
        public ActivityFunc<P> SetResult { get; set; }

        public T ApiCallParameterValue { get; set; }
        [Browsable(false)]
        public InArgument<T> ApiCallParameter { get; set; }

        public InArgument<ICollection<BasketYandexMarketResponse>> InternalValue { get; set; }
        [Browsable(false)]
        public OutArgument<P> CallResult { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument apiCalls = new RuntimeArgument("ApiCallParameter", typeof (T),
                                                           ArgumentDirection.In, false);
            metadata.Bind(ApiCallParameter, apiCalls);
            metadata.AddArgument(apiCalls);

            RuntimeArgument internalValue = new RuntimeArgument("InternalValue",
                                                                typeof (ICollection<BasketYandexMarketResponse>),
                                                                ArgumentDirection.In, false);
            metadata.Bind(InternalValue, internalValue);
            metadata.AddArgument(internalValue);

            RuntimeArgument result = new RuntimeArgument("CallResult", typeof (P),
                                                         ArgumentDirection.Out, false);
            metadata.Bind(CallResult, result);
            metadata.AddArgument(result);
        }

        public abstract string GetRequest();
    }    
}
