﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Integration.PHP;
using Orders.Backend.WF.Activities.Integration.PHP.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;

namespace Orders.Backend.WF.Activities.Integration.PHP
{
    [Designer(typeof(PhpYandexMarketReviewRequestPhpActivityDesigner))]
    public class PhpYandexMarketReviewRequest:PhpApiRequestBase<string,string>
    {
        private const string GetLinkTypeRequestName = "YandexMarketReview";

        public override string GetRequest()
        {
            var request = string.Format("{{\"getLink\":\"{0}\",\"sapCode\":\"{1}\"}}", GetLinkTypeRequestName,
                                        this.ApiCallParameterValue);

            return request;
        }

        protected override void Execute(NativeActivityContext context)
        {
            var apiParameter = ApiCallParameter.Get(context);
            ApiCallParameterValue = apiParameter;
            if (this.InternalValue == null)
            {
                return;
            }
            var response = this.InternalValue.Get(context);

            if (response == null || !response.Any())
            {
                return;
            }
            var yandexMarketUrl = response.First(el => el.Request.GetLinkType == LinkType.YandexMarketReview);
            this.CallResult.Set(context, yandexMarketUrl.Link);
        }
    }
}
