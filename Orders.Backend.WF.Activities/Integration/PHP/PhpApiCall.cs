﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using MMS.Activities;
using MMS.Activities.Model;
using Newtonsoft.Json;
using Orders.Backend.WF.Activities.Integration.PHP.Design;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.Integration.PHP
{
    [Designer(typeof(PhpApiCallActivityDesigner))]
    public class PhpApiCall : NativeActivity
    {
        private ActivityFunc<string, UserCredentials, List<BasketYandexMarketResponse>> CallPhpBasketApiFlow { get; set; }

        private ActivityAction<ICollection<BasketYandexMarketResponse>> SetApiParameters { get; set; }

        private ActivityFunc<UserCredentials> CredentialsObtaining { get; set; }

        [Browsable(false)]
        public InArgument<string> ApiUrl { get; set; }

        [Browsable(false)]
        public Credentials Credentials { get; set; }

        private Collection<NativeActivity> _basketApiCalls;

        [Browsable(false)]
        public Collection<NativeActivity> BasketApiCalls
        {
            get { return _basketApiCalls ?? (_basketApiCalls = new Collection<NativeActivity>()); }
// ReSharper disable ValueParameterNotUsed
            set { _basketApiCalls = value; }
// ReSharper restore ValueParameterNotUsed
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheExecutionFlow(metadata);
            CacheCredentials(metadata);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var apiUrl = new RuntimeArgument("ApiUrl", typeof(string),
                ArgumentDirection.In, false);
            metadata.Bind(ApiUrl, apiUrl);
            metadata.AddArgument(apiUrl);
        }

        private void CacheCredentials(NativeActivityMetadata metadata)
        {
            var credentials = new DelegateOutArgument<UserCredentials>("UserCredentials");
            Credentials.UserCredentials = credentials;
            CredentialsObtaining = new ActivityFunc<UserCredentials>
            {
                Handler = Credentials,
                Result = credentials
            };
            metadata.AddDelegate(CredentialsObtaining);
        }

        private void CacheExecutionFlow(NativeActivityMetadata metadata)
        {
            var credentials = new DelegateInArgument<UserCredentials>("Credentials");
            var result = new DelegateInArgument<ICollection<BasketYandexMarketResponse>>("RESULT");

            var setApiParametersCalls = new Sequence() {DisplayName = "SetApiParameters"};
            foreach(var basketApiCallResultBase in BasketApiCalls)
            {
                ((IBasketApiCall) basketApiCallResultBase).InternalValue = result;
                setApiParametersCalls.Activities.Add(basketApiCallResultBase);
            }
            SetApiParameters = new ActivityAction<ICollection<BasketYandexMarketResponse>>
            {                
                Argument = result,
                Handler = setApiParametersCalls
            };
            metadata.AddDelegate(SetApiParameters);

            var apiUrlArg = new DelegateInArgument<string>("ApiUrl");
            var resultArg = new DelegateOutArgument<List<BasketYandexMarketResponse>>("Result");
            CallPhpBasketApiFlow = new ActivityFunc<string, UserCredentials, List<BasketYandexMarketResponse>>()
            {
                Result = resultArg,
                Argument1 = apiUrlArg,
                Argument2 = credentials,
                Handler = new Failover()
                {
                    Delegate = new InvokeDelegate()
                    {
                        Delegate = new ActivityAction()
                        {
                            Handler =
                                new Assign()
                                {
                                    To = new OutArgument<List<BasketYandexMarketResponse>>(resultArg),
                                    Value =
                                        new InArgument<List<BasketYandexMarketResponse>>(
                                            c => GetBasketApiResponse(apiUrlArg.Get(c), GenerateAggregateRequest(), credentials.Get(c)))
                                }
                        }
                    }
                }
            };
            metadata.AddDelegate(CallPhpBasketApiFlow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleAction(SetApiParameters, new Collection<BasketYandexMarketResponse>(), OnSetApiParametersCompleted);
            
        }

        private void OnCredentialsObtained(NativeActivityContext context, ActivityInstance completedinstance,
            UserCredentials result)
        {
            context.ScheduleFunc(CallPhpBasketApiFlow, ApiUrl.Get(context), result, OnCompleted);
        }

        private void OnSetApiParametersCompleted(NativeActivityContext context, ActivityInstance completedinstance)
        {
            context.ScheduleFunc(CredentialsObtaining, OnCredentialsObtained);            
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, List<BasketYandexMarketResponse> result)
        {
            context.ScheduleAction(SetApiParameters, result);
        }

        public List<BasketYandexMarketResponse> GetBasketApiResponse(string apiUrl, string basketRequest, UserCredentials user)
        {
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true; //Fix bug with certificate

            if(Credentials != null)
            {
                request.Credentials = new NetworkCredential(user.UserName, user.Password);
            }

            request.Method = "POST";
            request.ContentType = "text/json";

            using(var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(basketRequest);
                streamWriter.Flush();
                streamWriter.Close();

                var response = (HttpWebResponse) request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return JsonConvert.DeserializeObject<List<BasketYandexMarketResponse>>(responseString);
            }
        }

        private string GenerateAggregateRequest()
        {
            var result = new StringBuilder("[");
            foreach(var basketApiCallResultBase in BasketApiCalls)
            {
                var basketApiCall = basketApiCallResultBase as IBasketApiCall;
                if(basketApiCall != null)
                {
                    result.Append(basketApiCall.GetRequest());
                    result.Append(",");
                }
            }

            result.Remove(result.Length - 1, 1);
            result.Append("]");

            return result.ToString();
        }
    }
}