﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Integration.PHP;
using Orders.Backend.WF.Activities.Integration.PHP.Design;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;

namespace Orders.Backend.WF.Activities.Integration.PHP
{
    [Designer(typeof(PhpItemReviewRequestActivityDesigner))]
    public class PhpItemReviewRequest : PhpApiRequestBase<IEnumerable<string>,IDictionary<string,string>>
    {
        [Browsable(false)]
        public Type InCollectionType
        {
            get { return typeof(IEnumerable<string>); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [Browsable(false)]
        public Type OutCollectionType
        {
            get { return typeof(IDictionary<string,string>); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        private const string GetLinkTypeRequestName = "ItemReview";

        public override string GetRequest()
        {
            var request = new StringBuilder();
            foreach (var callParameter in ApiCallParameterValue)
            {
                request.Append(string.Format("{{\"getLink\":\"{0}\",\"article\":\"{1}\"}},", GetLinkTypeRequestName, callParameter));
            }
            request.Remove(request.Length - 1, 1);

            return request.ToString();
        }

        protected override void Execute(NativeActivityContext context)
        {
            var apiParameter = ApiCallParameter.Get(context);
            ApiCallParameterValue = apiParameter;
            if (this.InternalValue == null)
            {
                return;
            }
            var resultLinks = this.InternalValue.Get(context);

            if (resultLinks == null || !resultLinks.Any())
            {
                return;
            }

            var result = new Dictionary<string, string>();
            foreach (var basketYandexMarketResponse in resultLinks.Where(el=>el.Request.GetLinkType==LinkType.ItemReview))
            {
                 if (result.Keys.Contains(basketYandexMarketResponse.Request.Article))
                 {
                     continue;
                 }
                result.Add(basketYandexMarketResponse.Request.Article, basketYandexMarketResponse.Link);
            }
            this.CallResult.Set(context, result);
        }
    }
}
