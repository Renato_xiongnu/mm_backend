﻿using System.Activities;
using System.Collections.Generic;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.Integration.PHP
{
    public interface IBasketApiCall
    {
        InArgument<ICollection<BasketYandexMarketResponse>> InternalValue { get; set; }
        string GetRequest();
    }    
}