﻿using System;
using System.Activities;
using System.ComponentModel;
using Orders.Backend.WF.Activities.Integration.WebApi.Design;

namespace Orders.Backend.WF.Activities.Integration.WebApi
{
    [Designer(typeof(WebApiCallHandlerActivityBaseDesigner))]
    public abstract class WebApiCallHandlerActivityBase<TCallParameter, TResultParameter> : NativeActivity,
        IWebApiCallHandler
    {
        [Browsable(false)]
        public ActivityFunc<TResultParameter> SetResult { get; set; }

        [Browsable(false)]
        public TCallParameter CallParameterValue { get; set; }

        [Browsable(false)]
        public InArgument<TCallParameter> CallParameter { get; set; }

        [Browsable(false)]
        public Type CallParameterType
        {
            get { return typeof(TCallParameter); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }
        
        public OutArgument<TResultParameter> CallResult { get; set; }

        [Browsable(false)]
        public Type CallResultType
        {
            get { return typeof(TCallParameter); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var apiCalls = new RuntimeArgument("CallParameter", typeof(TCallParameter), ArgumentDirection.In, false);
            metadata.Bind(CallParameter, apiCalls);
            metadata.AddArgument(apiCalls);

            var response = new RuntimeArgument("Response", typeof(object), ArgumentDirection.In, false);
            metadata.Bind(Response, response);
            metadata.AddArgument(response);

            var result = new RuntimeArgument("CallResult", typeof(TResultParameter), ArgumentDirection.Out, false);
            metadata.Bind(CallResult, result);
            metadata.AddArgument(result);
        }

        public InArgument<object> Response { get; set; }

        public abstract string GetRequest(string method, string contentType);

        public abstract object ParseResponse(string response);

        protected override void Execute(NativeActivityContext context)
        {
            var apiParameter = CallParameter.Get(context);
            CallParameterValue = apiParameter;
            var response = Response.Get(context);
            if (Response == null || response == null || response.GetType() != typeof(TResultParameter))
            {
                return;
            }
            CallResult.Set(context, response);
        }
    }
}