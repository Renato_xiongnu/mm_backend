﻿using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using MMS.Activities;
using MMS.Activities.Model;
using Orders.Backend.WF.Activities.Integration.WebApi.Design;

namespace Orders.Backend.WF.Activities.Integration.WebApi
{
    [Designer(typeof(WebApiCallActivityDesigner))]
    public class WebApiCallActivity : NativeActivity
    {
        private ActivityFunc<string, string, string, UserCredentials, Encoding, object> CallWebApiFlow { get; set; }

        private ActivityFunc<UserCredentials> CredentialsObtaining { get; set; }

        private ActivityAction<object> SetApiParameters { get; set; }

        [Browsable(false)]
        public InArgument<string> ApiUrl { get; set; }

        [Browsable(false)]
        public InArgument<string> Method { get; set; }

        [Browsable(false)]
        public InArgument<string> ContentType { get; set; }

        [Browsable(false)]
        public InArgument<Encoding> Encoding { get; set; }

        [Browsable(false)]
        public Credentials Credentials { get; set; }

        [Browsable(false)]
        public NativeActivity Handler { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheExecutionFlow(metadata);
            CacheCredentials(metadata);
        }

        private void CacheCredentials(NativeActivityMetadata metadata)
        {
            var credentials = new DelegateOutArgument<UserCredentials>("UserCredentials");
            Credentials.UserCredentials = credentials;
            CredentialsObtaining = new ActivityFunc<UserCredentials>
            {
                Handler = Credentials,
                Result = credentials
            };
            metadata.AddDelegate(CredentialsObtaining);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var apiUrl = new RuntimeArgument("ApiUrl", typeof(string), ArgumentDirection.In, true);
            metadata.Bind(ApiUrl, apiUrl);
            metadata.AddArgument(apiUrl);

            var method = new RuntimeArgument("Method", typeof(string), ArgumentDirection.In, false);
            metadata.Bind(Method, method);
            metadata.AddArgument(method);

            var contentType = new RuntimeArgument("ContentType", typeof(string), ArgumentDirection.In, false);
            metadata.Bind(ContentType, contentType);
            metadata.AddArgument(contentType);

            var encoding = new RuntimeArgument("Encoding", typeof(Encoding), ArgumentDirection.In, false);
            metadata.Bind(Encoding, encoding);
            metadata.AddArgument(encoding);
        }

        private void CacheExecutionFlow(NativeActivityMetadata metadata)
        {
            var credentials = new DelegateInArgument<UserCredentials>("Credentials");
            var result = new DelegateInArgument<object>("RESULT");

            var setApiParametersCalls = new Sequence() {DisplayName = "SetApiParameters"};
            ((IWebApiCallHandler) Handler).Response = result;
            setApiParametersCalls.Activities.Add(Handler);
            SetApiParameters = new ActivityAction<object>
            {
                Argument = result,
                Handler = setApiParametersCalls
            };
            metadata.AddDelegate(SetApiParameters);

            var apiUrlArg = new DelegateInArgument<string>("ApiUrl");
            var methodArg = new DelegateInArgument<string>("Method");
            var contentTypeArg = new DelegateInArgument<string>("ContentType");
            var encodingArg = new DelegateInArgument<Encoding>("Encoding");
            var resultArg = new DelegateOutArgument<object>("Result");
            CallWebApiFlow = new ActivityFunc<string, string, string, UserCredentials, Encoding, object>()
            {
                Result = resultArg,
                Argument1 = apiUrlArg,
                Argument2 = methodArg,
                Argument3 = contentTypeArg,
                Argument4 = credentials,
                Argument5 = encodingArg,
                Handler = new Failover()
                {
                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                    Delegate = new InvokeDelegate()
                    {
                        Delegate = new ActivityAction()
                        {
                            Handler =
                                new Assign()
                                {
                                    To = new OutArgument<object>(resultArg),
                                    Value =
                                        new InArgument<object>(
                                            c =>
                                                GetBasketApiResponse(
                                                    apiUrlArg.Get(c),
                                                    methodArg.Get(c),
                                                    contentTypeArg.Get(c),
                                                    credentials.Get(c),
                                                    encodingArg.Get(c)))
                                }
                        }
                    }
                }
            };
            metadata.AddDelegate(CallWebApiFlow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleAction(SetApiParameters, string.Empty, OnSetApiParametersCompleted);
        }

        private void OnCredentialsObtained(NativeActivityContext c, ActivityInstance completedinstance,
            UserCredentials userCredentials)
        {
            var method = Method.Get(c) ?? "POST";
            var contentType = ContentType.Get(c) ?? "text/json";
            var encoding = Encoding.Get(c) ?? System.Text.Encoding.Default;
            c.ScheduleFunc(CallWebApiFlow, ApiUrl.Get(c), method, contentType, userCredentials, encoding, OnCompleted);
        }

        private void OnSetApiParametersCompleted(NativeActivityContext c, ActivityInstance completedinstance)
        {
            c.ScheduleFunc(CredentialsObtaining, OnCredentialsObtained);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, object result)
        {
            context.ScheduleAction(SetApiParameters, result);
        }

        private object GetBasketApiResponse(string apiUrl, string method, string contentType,
            UserCredentials credentionals, Encoding encoding)
        {
            var requstBody = GenerateRequest(method, contentType);
            var request = (HttpWebRequest) WebRequest.Create(apiUrl);
            if(Credentials != null)
            {
                request.Credentials = new NetworkCredential(
                    credentionals.UserName,
                    credentionals.Password);
            }
            request.Method = method;
            request.ContentType = contentType;
            using(var streamWriter = new StreamWriter(request.GetRequestStream(), encoding))
            {
                streamWriter.Write(requstBody);
                streamWriter.Flush();
                streamWriter.Close();

                var response = (HttpWebResponse) request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream(), encoding).ReadToEnd();
                return ((IWebApiCallHandler) Handler).ParseResponse(responseString);
            }
        }

        private string GenerateRequest(string method, string contentType)
        {
            var request = ((IWebApiCallHandler) Handler).GetRequest(method, contentType);
            return request;
        }
    }
}