﻿using System.Activities;
using System.IO.Packaging;

namespace Orders.Backend.WF.Activities.Integration.WebApi
{
    public interface IWebApiCallHandler
    {
        InArgument<object> Response { get; set; }

        string GetRequest(string method, string contentType);

        object ParseResponse(string response);
    }    
}