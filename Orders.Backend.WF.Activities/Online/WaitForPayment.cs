﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.Activities;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Online
{
    public sealed class WaitForPayment : NativeActivity
    {
        public enum PaymentStatus
        {
            Paid,
            Cancelled,
            Held
        }

        private ActivityFunc<CorrelationHandle, PaymentStatus> Flow { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        [Browsable(false)]
        public OutArgument<PaymentStatus> Result { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            this.Flow = new ActivityFunc<CorrelationHandle, PaymentStatus>();
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            RuntimeArgument handle = new RuntimeArgument(Backend_Activities.Param_CorrelationHandle, typeof (CorrelationHandle), ArgumentDirection.In, true);
            metadata.Bind(this.CorrelationHandle, handle);
            metadata.AddArgument(handle);
        }

        protected override void Execute(NativeActivityContext context)
        {
            
        }
    }
}
