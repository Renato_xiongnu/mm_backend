﻿using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities;
using MMS.Activities.Extensions;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;
using Orders.Backend.WF.Activities.Proxy.Online;

namespace Orders.Backend.WF.Activities.Online
{
    [Designer(typeof(SaveOrderActivityDesigner))]
    public sealed class SaveOrder : NativeActivity
    {
        private ActivityFunc<Order, UpdateOrderOperationResult> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>("Order");
            var resultArg = new DelegateOutArgument<UpdateOrderOperationResult>("Result");

            this.Flow = new ActivityFunc<Order, UpdateOrderOperationResult>
                {
                    Result = resultArg,
                    Argument = orderArg,
                    Handler = new Failover
                        {
                            DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                            Delegate = new InvokeDelegate
                                {
                                    Delegate = new ActivityAction
                                        {
                                            Handler = new Assign
                                                {
                                                    To = new OutArgument<UpdateOrderOperationResult>(resultArg),
                                                    Value = new InArgument<UpdateOrderOperationResult>(c => new OrderServiceClient().UpdateOrder(orderArg.Get(c).ToUpdateOrderData(), orderArg.Get(c).ToReserveInfo()))
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = Order.Get(context);
            order.MergeReserve(false);
            context.ScheduleFunc(this.Flow, order, this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, UpdateOrderOperationResult result)
        {
            if (result == null) return;
            if(result.ReturnCode != ReturnCode.Error)
            {
                return;
            }
            var order = Order.Get(context);
            context.LogErrorFormat("SaveOrder {0} error : {1}", order.Id, result.ErrorMessage);
        }
    }
}
