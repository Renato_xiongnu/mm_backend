﻿using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;

namespace Orders.Backend.WF.Activities.Online
{
    [Designer(typeof(GetOrderActivityDesigner))]
    public sealed class GetOrder : NativeActivity
    {
        private ActivityFunc<Order, Order> GetOrderFlow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<string> OrderId { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public OutArgument<Order> Order { get; set; }
        
        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var orderIn = new DelegateInArgument<Order>();
            var orderOut = new DelegateOutArgument<Order>();
            var flow = new Sequence
                           {
                               Activities =
                                   {
                                       new RefreshOrder {Order = orderIn},
                                       new Assign
                                           {
                                               To = new OutArgument<Order>(orderOut), 
                                               Value = new InArgument<Order>(orderIn)
                                           }
                                   }
                           };
                
            this.GetOrderFlow = new ActivityFunc<Order, Order> {Argument = orderIn, Result = orderOut, Handler = flow};
            metadata.AddDelegate(this.GetOrderFlow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            RuntimeArgument orderId = new RuntimeArgument(Backend_Activities.Param_OrderId, typeof(string), ArgumentDirection.In, true);
            metadata.Bind(this.OrderId, orderId);
            metadata.AddArgument(orderId);

            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.Out, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var order = new Order {Id = this.OrderId.Get(context)};
            context.ScheduleFunc(this.GetOrderFlow, order, this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, Order result)
        {
            this.Order.Set(context, result);
        }
    }
}
