﻿using System.Activities;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.WF.Activities.Online
{
    public class AttachWwsOrder:CodeActivity
    {
        public InArgument<Order> Order { get; set; }

        public InArgument<string> WwsOrderId { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var order = Order.Get(context);
            var wwsOrderId = WwsOrderId.Get(context);
            order.AttachNewWwsOrder(wwsOrderId);
        }
    }
}
