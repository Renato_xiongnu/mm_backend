﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using MMS.Activities;
using Orders.Backend.WF.Activities.Integration.Product;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;
using Orders.Backend.WF.Activities.Proxy.Online;
using Orders.Backend.WF.Activities.Stores;
using CardInfo = Orders.Backend.WF.Activities.Model.CardInfo;
using PickupPointType = Orders.Backend.WF.Activities.Model.PickupPointType;
using Orders.Backend.WF.Activities.Common;

namespace Orders.Backend.WF.Activities.Online
{
    [Designer(typeof(RefreshOrderActivityDesigner))]
    public class RefreshOrder : NativeActivity
    {
        private ActivityFunc<Order, GerOrderDataResult> Refresh { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(Order, order);
            metadata.AddArgument(order);

            var orderArg = new DelegateInArgument<Order>();
            var getOrderArg = new DelegateOutArgument<GerOrderDataResult>();
            var callRefreshOrder =
                new FailoverWithCancel
                {
                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                    Delegate = new InvokeDelegate
                    {
                        Delegate = new ActivityAction
                        {
                            Handler = new Sequence
                            {
                                Activities =
                                {
                                    new Assign
                                    {
                                        To = new OutArgument<GerOrderDataResult>(getOrderArg),
                                        Value =
                                            new InArgument<GerOrderDataResult>(
                                                c => new OrderServiceClient().GerOrderDataByOrderId(orderArg.Get(c).Id))
                                    },
                                    new Assign
                                    {
                                        To = new OutArgument<Order>(c=>orderArg.Get(c)),
                                        Value = new InArgument<Order>(c=>FillOrder(getOrderArg.Get(c),orderArg.Get(c)))
                                    },
                                    new GetStore
                                    {
                                        SapCode = new InArgument<string>(c=>orderArg.Get(c).Store.SapCode),
                                        Store = new OutArgument<Store>(c=>orderArg.Get(c).Store)
                                    },
                                    new RefreshProductInfo
                                    {
                                        Workitem = new InArgument<Order>(c=>orderArg.Get(c)),                                        
                                    }
                                }
                            }
                        }
                    }
                };
            Refresh = new ActivityFunc<Order, GerOrderDataResult>
            {
                Argument = orderArg,     
                Result = getOrderArg,
                Handler = callRefreshOrder
            };
            metadata.AddDelegate(Refresh);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(Refresh, Order.Get(context), OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance,
            GerOrderDataResult result)
        {            
            
        }

        private static Order FillOrder(GerOrderDataResult result, Order order)
        {
            order.Source = result.OrderInfo.OrderSource.ToSourceType();
            order.Created = result.OrderInfo.Created;
            order.Updated = result.OrderInfo.UpdateDate;
            order.State = result.OrderInfo.StatusInfo.Status.ToOrderState();
            order.IsPreOrder = result.OrderInfo.IsPreorder;
            order.Comment = result.OrderInfo.Comment;
            order.IsOnlineOrder = order.Source != SourceType.Store;
            order.ExpirationDate = result.OrderInfo.ExpirationDate;

            if(order.Customer == null)
            {
                order.Customer = new Customer();
            }
            if(order.Customer.SocialCard == null)
            {
                order.Customer.SocialCard = new CardInfo();
            }
            order.Customer.Email = result.ClientData.Email;
            order.Customer.Name = result.ClientData.Name;
            order.Customer.Phone = result.ClientData.Phone;
            order.Customer.Surname = result.ClientData.Surname;
            order.Customer.Phone2 = result.ClientData.Phone2;
            order.Customer.Surname = result.ClientData.Surname;
            order.Customer.BirthDay = result.ClientData.Birthday;
            order.Customer.CustomerType = order.GetCustomerType();
            
            if(result.ClientData.SocialCard != null)
            {
                order.Customer.SocialCard.Number = result.ClientData.SocialCard.Number;
            }

            order.Certificate = new OrderCertificate();
            if(result.OrderInfo.certificateInfo != null)
            {
                order.Certificate.KuponAmount = result.OrderInfo.certificateInfo.Amount;
                order.Certificate.KuponNo = result.OrderInfo.certificateInfo.CertificateId;
                order.Certificate.KuponNoImage = result.OrderInfo.certificateInfo.Content;
            }
            if(result.ReserveInfo.SalesDocumentData != null)
            {
                order.InitDocumentInfo.OutletInfo = result.ReserveInfo.SalesDocumentData.OutletInfo;
                order.InitDocumentInfo.ProductPickupInfo = result.ReserveInfo.SalesDocumentData.ProductPickupInfo;
                order.InitDocumentInfo.PrintableInfo = result.ReserveInfo.SalesDocumentData.PrintableInfo;
                order.InitDocumentInfo.SalesPersonId = result.ReserveInfo.SalesDocumentData.SalesPersonId;
            }

            if(order.Store == null)
            {
                order.Store = new Store();
            }
            order.Store.SapCode = result.OrderInfo.SapCode;
            order.SaleLocationSapCode = result.OrderInfo.SaleLocationSapCode;
            order.Payment = result.OrderInfo.PaymentType.ToPaymentType();
            order.Prepay = result.OrderInfo.Prepay;
            order.Delivery = result.DeliveryInfo.ToDelivery();

            if (result.PickupLocation != null)
            {
                order.Delivery.PickupLocation = new Model.PickupLocation
                {
                    PickupLocationId = result.PickupLocation.PickupLocationId,
                    Address = result.PickupLocation.Address,
                    PickupPointType = (PickupPointType)result.PickupLocation.PickupPointType,
                    Title = result.PickupLocation.Title,
                    OperatorPuPId = result.PickupLocation.OperatorPuPId,
                    OperatorName = result.PickupLocation.OperatorName,
                    City = result.PickupLocation.City,
                    ZipCode = result.PickupLocation.ZipCode
                };
            }

            if(!String.IsNullOrEmpty(result.OrderInfo.WWSOrderId))
            {
                if(order.WwsOrders.All(w => w.Id != result.OrderInfo.WWSOrderId))
                {
                    order.WwsOrders.Add(new WwsOrder {Id = result.OrderInfo.WWSOrderId});
                }
            }

            order.ReserveState = result.ReserveInfo.Status.ToReserveState();
            if(order.Items == null)
            {
                order.Items = new Collection<OrderItem>();
            }
            order.Items.Clear();

            foreach(var reserveItem in result.ReserveInfo.ReserveLines)
            {
                order.Items.Add(reserveItem.ToOrderItem());
            }

            foreach(var orderItem in order.Items.Where(el => string.IsNullOrEmpty(el.Title)))
            {
                var articleData = result.ArticleLines.FirstOrDefault(el => el.ArticleNum == orderItem.Number);
                if(articleData != null)
                {
                    orderItem.Title = articleData.Title; //Если резерв не создан, пытаемсчя подргузить названия
                    //из базы OnlineOrders
                }
            }
            order.DocumentPrintingStatus = result.ReserveInfo.PrintingStatus.ToPrintingStatus();

            return order;
        }
    }
}