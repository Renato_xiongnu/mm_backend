﻿using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activities;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;
using Orders.Backend.WF.Activities.Properties;

namespace Orders.Backend.WF.Activities.Online
{
    [Designer(typeof(WaitForUpdateActivityDesigner))]
    public sealed class WaitForUpdate : NativeActivity
    {
        private ActivityAction<Order, CorrelationHandle> Flow { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public InArgument<Order> Order { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var orderArg = new DelegateInArgument<Order>("Order");
            var handleArg = new DelegateInArgument<CorrelationHandle>("Handle");
            var workItemId = new Variable<string>("WorkitemId");
            var receive = new Receive
                {
                    CanCreateInstance = false,
                    ServiceContractName = Settings.Default.ServiceContractName,
                    OperationName = "UpdateOrder",
                    Content = ReceiveContent.Create(
                        new Dictionary<string, OutArgument>
                            {
                                {Settings.Default.WorkItemIdParam, new OutArgument<string>(workItemId)}
                            }),
                    CorrelatesWith = handleArg,
                    CorrelatesOn = new MessageQuerySet {{Settings.Default.QueryKey, new CorrelationWorkItemIdQuery(Settings.Default.WorkItemIdParam)}}
                };

            this.Flow = new ActivityAction<Order, CorrelationHandle>
                {
                    Argument1 = orderArg,
                    Argument2 = handleArg,
                    Handler = new Sequence
                        {
                            Variables = {workItemId},
                            Activities =
                                {
                                    receive,
                                    new SendReply {Request = receive, Content = new SendParametersContent(new Dictionary<string, InArgument> {{"Result", new InArgument<bool>(true)}})},
                                    new RefreshOrder {Order = orderArg}
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof (Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            RuntimeArgument handle = new RuntimeArgument(Backend_Activities.Param_CorrelationHandle, typeof(CorrelationHandle), ArgumentDirection.In, true);
            metadata.Bind(this.CorrelationHandle, handle);
            metadata.AddArgument(handle);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleAction(this.Flow, this.Order.Get(context), this.CorrelationHandle.Get(context));
        }
    }
}
