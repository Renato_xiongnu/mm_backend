using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;

namespace Orders.Backend.WF.Activities.Online
{
    [Designer(typeof(CompareOrderLinesActivityDesigner))]
    public sealed class CompareOrderLines : NativeActivity<bool>
    {
        public CompareOrderLines()
        {
            ArgumentType = typeof(IEnumerable<OrderItem>);
        }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<IEnumerable<OrderItem>> OrderLines1 { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<IEnumerable<OrderItem>> OrderLines2 { get; set; }
        
        [Browsable(false)]
        [RequiredArgument]
        public Type ArgumentType { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            var lines1 = OrderLines1.Get(context);
            var lines2 = OrderLines2.Get(context);
            if (lines1 == null || lines2 == null)
            {
                Result.Set(context, false);
                return;
            }
            var lines1Array = lines1.ToArray();
            var lines2Array = lines2.ToArray();
            if (lines1Array.Length != lines2Array.Length)
            {
                Result.Set(context, false);
                return;
            }

            var linesEquals = lines1Array.All(l1=>lines2Array.Any(l1.Equals));
            Result.Set(context, linesEquals);
        }
    }
}