﻿using System.Activities;
using System.ComponentModel;
using System.Linq;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Online.Design;

namespace Orders.Backend.WF.Activities.Online
{
    [Designer(typeof(CompareOrdersActivityDesigner))]
    public sealed class CompareOrders : NativeActivity<bool>
    {
        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order1 { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order2 { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            var order1 = Order1.Get(context);
            var order2 = Order2.Get(context);
            if(order1 == null || order2 == null)
            {
                Result.Set(context, false);
                return;
            }

            var lines1 = order1.Items;
            var lines2 = order2.Items;
            if(lines1 == null || lines2 == null || lines1.Count != lines2.Count)
            {
                Result.Set(context, false);
                return;
            }

            if(lines1.Select((t, i) => t.Equals(lines2[i])).Any(r => !r))
            {
                Result.Set(context, false);
                return;
            }

            Result.Set(context, true);
        }
    }
}