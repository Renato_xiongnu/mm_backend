﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using MMS.Activities;
using MMS.Activities.Extensions;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Online;
using Orders.Backend.WF.Activities.Stores.Design;

namespace Orders.Backend.WF.Activities.Stores
{
    [Designer(typeof(GetStoreActivityDesigner))]
    public class GetStore : NativeActivity
    {
        private ActivityFunc<string, Store> Flow { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<string> SapCode { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public OutArgument<Store> Store { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var sapCode = new RuntimeArgument(Backend_Activities.Param_SAPCode, typeof(string), ArgumentDirection.In,
                true);
            metadata.Bind(SapCode, sapCode);
            metadata.AddArgument(sapCode);

            var store = new RuntimeArgument(Backend_Activities.Param_Store, typeof(Store), ArgumentDirection.Out, true);
            metadata.Bind(Store, store);
            metadata.AddArgument(store);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var sapCode = new DelegateInArgument<string>("SapCode");
            var store = new DelegateOutArgument<Store>("Store");
            Flow = new ActivityFunc<string, Store>
            {
                Argument = sapCode,
                Result = store,
                Handler = new Failover
                {
                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                    Delegate = new InvokeDelegate
                    {
                        Delegate = new ActivityAction
                        {
                            Handler = new Assign
                            {
                                To = new OutArgument<Store>(store),
                                Value = new InArgument<Store>(c => GetStoreInternal(sapCode.Get(c)))
                            }
                        }
                    }
                }
            };
            metadata.AddDelegate(Flow);
        }

        private static Store GetStoreInternal(string sapCode)
        {
            using(var client = new OrderServiceClient())
            {
                var result = client.GetStoreData(sapCode);
                if(result.ReturnCode == ReturnCode.Error)
                {
                    var error = string.Format("Get store data error(SapCode:{0}): {1}", sapCode, result.ErrorMessage);
                    throw new ApplicationException(error);
                }
                var store = new OrderServiceClient()
                    .GetStoreData(sapCode).StoreData
                    .ToStore();
                return store;
            }
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(Flow, SapCode.Get(context), OnCompleted, OnFaulted);
        }

        private void OnFaulted(NativeActivityFaultContext faultcontext, Exception propagatedexception,
            ActivityInstance propagatedfrom)
        {
            faultcontext.LogError("GetStore faulted", propagatedexception);
            faultcontext.HandleFault();
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, Store result)
        {
            if(completedinstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }
            if(result != null)
            {
                Store.Set(context, result);
            }
        }
    }
}