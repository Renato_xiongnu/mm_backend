﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using Orders.Backend.WF.Activities.Proxy.GiftCard.GiftCards.Interfaces.Contracts.OnlineService.Requests;
using Orders.Backend.WF.Activities.Proxy.GiftCard.GiftCards.Interfaces.Contracts.OnlineService.Responses;

namespace Orders.Backend.WF.Activities.Proxy.GiftCard
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [ServiceContractAttribute(ConfigurationName = "IOnlineService")]
    public interface IOnlineService
    {

        [WebInvoke(UriTemplate = "/GetCertificateTypes", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(GetCertificateTypesResponse))]
        GetCertificateTypesResponse GetCertificateTypes();

        //2 Запрос номера сертификата
        [WebInvoke(UriTemplate = "/GetCertificateCode", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(GetCertifiacateCodeResponse))]
        GetCertifiacateCodeResponse GetCertificateCode(string certificateType, decimal amount);

        //3 Активация сертификата
        [WebInvoke(UriTemplate = "/ActivateCertificateCode", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(ActivateCertifiacateCodeResponse))]
        ActivateCertifiacateCodeResponse ActivateCertificateCode(string nickName, string firstName, string lastName, string phone, string email, string city, string address, DateTime birthday, string certificateId);

        //4 Запрос данных по сертификатам
        //[WebGet]
        //[OperationContract]
        //PollCertificatesResponse GetCertificateDetails(string certificateId);

        [WebInvoke(UriTemplate = "/GetCertificatesDetails", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(GetCertificatesDetailsResponse))]
        GetCertificatesDetailsResponse GetCertificatesDetails(string[] certificateIds);

        //5 Запрос информации по сертификатам
        //[WebGet]
        //[OperationContract]
        //GetCertificateBalanceResponse GetCertificateBalance(string certificateId);

        [WebInvoke(UriTemplate = "/GetCertificatesBalance", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(GetCertificatesBalanceResponse))]
        GetCertificatesBalanceResponse GetCertificatesBalance(string[] certificateIds);

        [WebInvoke(UriTemplate = "/LoadCustomers", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(LoadCustomersResponse))]
        LoadCustomersResponse LoadCustomers(LoadCustomersRequest request);

        [WebInvoke(UriTemplate = "/CreateCertificate", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(CreateCertificateResponse))]
        CreateCertificateResponse CreateCertificate(CreateCertificateRequest request);

        [WebInvoke(UriTemplate = "/GetUserCertificates", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(GetUserCertificatesResponse))]
        GetUserCertificatesResponse GetUserCertificates(GetUserCertificatesRequest request);

        [WebInvoke(UriTemplate = "/GetStores", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        [ServiceKnownType(typeof(StoreInfo))]
        GetStoresResponse GetStores();
    }
}