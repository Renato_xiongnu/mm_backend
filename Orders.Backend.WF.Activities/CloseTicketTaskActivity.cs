﻿

using Orders.Backend.WF.Activities.Proxy.TicketTool;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities
{
    class CloseTicketTaskActivity<T> : NativeActivity<T> where T : TicketTask
    {
        public InArgument<T> TicketTask { get; set; }

        protected override bool CanInduceIdle { get { return true; } }

        protected override void Execute(NativeActivityContext context)
        {
            var task = TicketTask.Get(context);
        }
    }
}
