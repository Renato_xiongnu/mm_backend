﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MMS.Activities.Extensions;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.WF.Activities.Proxy.Tablet;
using Orders.Backend.WF.Activities.Tablet.Design;

namespace Orders.Backend.WF.Activities.Tablet
{
    [Designer(typeof(UpdateOrderStatusActivityDesigner))]
    public sealed class UpdateOrderStatus : NativeActivity, IActivityTemplateFactory
    {
        private ActivityAction<Order, string> UpdateFlow { get; set; }

        [Browsable(false)]
        public string State { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Order> Order { get; set; }

        [Browsable(false)]
        public InArgument<string> Message { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var client = new Variable<StoreServiceClient>("StoreServiceClient");
            var request = new Variable<UpdateOrderStatusRequest>("UpdateOrderStatusRequest");
            var wwsNo = new Variable<string>("WwsNo");
            var order = new DelegateInArgument<Order>("Order");
            var message = new DelegateInArgument<string>(Backend_Activities.Param_Message);
            this.UpdateFlow = new ActivityAction<Order, string>
                {
                    Argument1 = order,
                    Argument2 = message,
                    Handler = new Sequence
                        {
                            Variables = {client, request, wwsNo},
                            Activities =
                                {
// ReSharper disable ImplicitlyCapturedClosure
                                    new If(c => order.Get(c).CurrentWwsOrder != null)
                                        {
                                            Then = new Assign
                                                {
                                                    To = new OutArgument<string>(wwsNo),
                                                    Value = new InArgument<string>(c => order.Get(c).CurrentWwsOrder.Id)
// ReSharper restore ImplicitlyCapturedClosure
                                                }
                                        },
                                    new Assign
                                        {
                                            To = new OutArgument<StoreServiceClient>(client),
                                            Value = new InArgument<StoreServiceClient>(c => new StoreServiceClient())
                                        },
                                    new Assign
                                        {
                                            To = new OutArgument<UpdateOrderStatusRequest>(request),
                                            Value = new InArgument<UpdateOrderStatusRequest>(
                                                c => new UpdateOrderStatusRequest
                                                    {
                                                        OrderId = order.Get(c).Id,
                                                        Status = (StoreOrderStatus) Enum.Parse(typeof (StoreOrderStatus), this.State),
                                                        WwsReservNo = wwsNo.Get(c),
                                                        Message = message.Get(c)
                                                    })
                                        },
                                    new InvokeMethod
                                        {
                                            MethodName = "UpdateOrderStatus",
                                            TargetObject = new InArgument<StoreServiceClient>(client),
                                            Parameters = {new InArgument<UpdateOrderStatusRequest>(request)}
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.UpdateFlow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            RuntimeArgument order = new RuntimeArgument(Backend_Activities.Param_Order, typeof(Order), ArgumentDirection.In, true);
            metadata.Bind(this.Order, order);
            metadata.AddArgument(order);

            RuntimeArgument message = new RuntimeArgument(Backend_Activities.Param_Message, typeof(string), ArgumentDirection.In);
            metadata.Bind(this.Message, message);
            metadata.AddArgument(message);

            StoreOrderStatus status;
            if (!Enum.TryParse(this.State, out status))
            {
                metadata.AddValidationError(Backend_Activities.Err_InvalidOrderState);
            }
        }

        protected override void Execute(NativeActivityContext context)
        {
            string message = this.Message != null ? this.Message.Get(context) : String.Empty;
            context.ScheduleAction(this.UpdateFlow, this.Order.Get(context), message, onFaulted: OnFaulted);
        }

        private static void OnFaulted(NativeActivityFaultContext faultcontext, Exception propagatedexception, ActivityInstance propagatedfrom)
        {            
            faultcontext.LogError("UpdateOrderStatus faulted", propagatedexception);
            faultcontext.HandleFault();
        }

        public Activity Create(DependencyObject target)
        {
            return new UpdateOrderStatus {State = Enum.GetNames(typeof (StoreOrderStatus)).FirstOrDefault()};
        }
    }
}
