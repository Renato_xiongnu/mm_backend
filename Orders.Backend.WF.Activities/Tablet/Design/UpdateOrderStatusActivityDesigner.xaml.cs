﻿using System;
using Orders.Backend.WF.Activities.Proxy.Tablet;

namespace Orders.Backend.WF.Activities.Tablet.Design
{
    // Interaction logic for UpdateOrderStatusActivityDesigner.xaml
    public partial class UpdateOrderStatusActivityDesigner
    {
        public UpdateOrderStatusActivityDesigner()
        {
            InitializeComponent();

            this.States = Enum.GetNames(typeof (StoreOrderStatus));
        }

        public string[] States { get; private set; }
    }
}
