﻿using System;
using System.ServiceModel.Dispatcher;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Proxy.TicketTool;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activities;

namespace Orders.Backend.WF.Activities
{
    public class CreateTicketTaskActivity<T> : TicketTaskActivityBase<T, ITicketToolService> where T : TicketTask //: NativeActivity<string> where T : TicketTask, new()
    {
        public OutArgument<string> Outcome { get; set; }

        public string CompleteOperationName { get; set; }
                    
        protected override bool CanInduceIdle { get { return true; } }

        protected override Sequence GetNewChild()
        {
            var outcome = new Variable<string>();
            var recieve = new Receive
                    {
                        CanCreateInstance = false,
                        ServiceContractName = TicketToolWFContext.ServiceName,
                        OperationName = "CompleteTask",
                        Content = ReceiveContent.Create(new Dictionary<string, OutArgument>
                                                            {
                                                                {TicketToolWFContext.QueryKey, new OutArgument<string>()},
                                                                {"outcome", new OutArgument<string>(outcome)}
                                                            }),
                        CorrelationInitializers =
                            {
                                new QueryCorrelationInitializer
                                    {
                                        CorrelationHandle = new InArgument<CorrelationHandle>(c => this.CurrentContext.Get(c).CorrelationHandle),
                                        MessageQuerySet = new MessageQuerySet {{TicketToolWFContext.QueryKey, new TicketIdQuery()}}
                                    }
                            }
                    };

            return new Sequence
            {
                Variables = { outcome },
                Activities = { 
                    recieve,
                    new SendReply {Request = recieve, Content = new SendParametersContent(), PersistBeforeSend = true}, 
                    new Assign {To = new OutArgument<string>(c => this.Outcome.Get(c)), Value = new InArgument<string>(outcome)}
                }
            };
        }

        protected override void Execute(NativeActivityContext context)
        {            
            TicketTask task = TicketTask.Get(context);
            var tickeId = CurrentContext.Get(context).TicketService.CreateTicketTask(task);
            Result.Set(context, tickeId);
            base.Execute(context);
        }        
    }
}
