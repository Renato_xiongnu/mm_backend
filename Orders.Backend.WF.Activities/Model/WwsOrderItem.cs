﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Orders.Backend.WF.Activities.Proxy.WWS;

namespace Orders.Backend.WF.Activities.Model
{
    public class WwsOrderItem
    {
        public WwsOrderItem()
        {
            SubOrderItems = new Collection<WwsOrderItem>();
            WarrantyInsurance=new WarrantyInsurance();
        }
        
        public int Article { get; set; }
        public int DepartmentNumber { get; set; }
        public string Description { get; set; }
        public int FreeQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal PriceOrig { get; set; }
        public string ProductGroupName { get; set; }
        public int ProductGroup { get; set; }
        public int Quantity { get; set; }
        public int ReservedQuantity { get; set; }
        public int StockNumber { get; set; }

        public int PositionNumber { get; set; }
        public string Title { get; set; }
        public int StoreNumber { get; set; }
        public int VAT { get; set; }
        public string MediaMarketUrl { get; set; }
        public bool HasShippedFromStock { get; set; }
        public string SerialNumber { get; set; }
        public ProductType ProductType { get; set; }        

        public WarrantyInsurance WarrantyInsurance { get; set; }

        public ICollection<WwsOrderItem> SubOrderItems { get; set; }
    }
}