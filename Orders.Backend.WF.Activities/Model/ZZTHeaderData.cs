﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Backend.WF.Activities.Model
{
    public class ZZTHeaderData
    {
        public ZZTDeliveryAddress DeliveryAddress { get; set; }
        
        public ZZTDeliveryInfo DeliveryInfo { get; set; }

        public string CampaignId { get; set; }

        public string ConsumerId { get; set; }

        public string RecipientName { get; set; }

        public ZztPayment Payment { get; set; } //TODO Устанавливать в единицу перед процессом

        public string OrderId { get; set; }
    }

     
    public class ZZTDeliveryAddress
    {
        public string PostalCode { get; set; }
        
        public string Country { get; set; }
        
        public string Region { get; set; }
        
        public string District { get; set; }
        
        public string MetroStation { get; set; }
        
        public string City { get; set; }

        public string CityId { get; set; }

        public string RegionId { get; set; }
        
        public string Street { get; set; }
        
        public string House { get; set; }
        
        public string Housing { get; set; }
        
        public string Building { get; set; }
        
        public string Flat { get; set; }
        
        public string Entrance { get; set; }
        
        public string EntranceCode { get; set; }
        
        public string Floor { get; set; }
        
        public bool? HasElevator { get; set; }
        
        public string Additional { get; set; }
    }
    
    public class ZZTDeliveryInfo
    {
        public string DeliveryTime { get; set; }
        
        public decimal? ClimbPrice { get; set; }
        
        public decimal? DeliveryPrice { get; set; }
        
        public DateTime? DeliveryDate { get; set; }
        
        public string DeliveryType { get; set; }
        
        public string DeliveryPlaceSapCode { get; set; }

        public string DeliveryPlaceId { get; set; }
    }

    public class ZztPayment
    {
        public string Id { get; set; }
    }
}
