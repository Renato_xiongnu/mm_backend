﻿namespace Orders.Backend.WF.Activities.Model
{
    public enum ArticleProductType
    {
        Product = 0,
        Set = 1,
        WarrantyPlus = 2,
        InstallationService = 3,
        DeliveryService = 4,
        Statistical = 5,
        InstallServiceMarging = 6
    }
}