﻿using System;
using AutoMapper;
using Common.Proxy.Interfaces.TransferObjects.Octopus;

namespace Orders.Backend.WF.Activities.Model
{
    public static class MapperConfiguration
    {
        private static bool _configured;

        static MapperConfiguration()
        {
            ConfigureMappings();
        }

        public static void ConfigureMappings()
        {
            if (_configured)
            {
                return;
            }

            Mapper.CreateMap<Proxy.Online.ZZTDeliveryAddress, ZZTDeliveryAddress>();
            Mapper.CreateMap<Proxy.Online.ZZTDeliveryInfo, ZZTDeliveryInfo>();
            Mapper.CreateMap<ZZTDeliveryInfo, Proxy.Online.ZZTDeliveryInfo>()
                  .ForMember(el=>el.ExtensionData, opt=>opt.UseValue(null));
            Mapper.CreateMap<ZZTDeliveryAddress, Proxy.Online.ZZTDeliveryAddress>()
                  .ForMember(el => el.ExtensionData, opt => opt.UseValue(null));
            Mapper.CreateMap<ShortCampaingInfo, Integration.Octopus.ShortCampaingInfo>();
            Mapper.AssertConfigurationIsValid();

            _configured = true;
        }

        public static bool CanMap(object from, object to)
        {
            ConfigureIfNotConfigured();
            if (from == null)
            {
                throw new ArgumentNullException("from");
            }
            if (to == null)
            {
                throw new ArgumentNullException("to");
            }

            return CanMap(from.GetType(), to.GetType());
        }

        public static bool CanMap(Type from, Type to)
        {
            ConfigureIfNotConfigured();
            if (from == null)
            {
                throw new ArgumentNullException("from");
            }
            if (to == null)
            {
                throw new ArgumentNullException("to");
            }

            var map = Mapper.FindTypeMapFor(from, to);
            return map != null;
        }

        private static void ConfigureIfNotConfigured()
        {
            if (!_configured)
            {
                ConfigureMappings();
            }
        }

        public static T ConvertTo<T>(this object obj)
        {
            return Mapper.Map<T>(obj);
        }
    }
}
