namespace Orders.Backend.WF.Activities.Model
{
    public enum CustomerType
    {
        NotSet,
        //(200K+ and 1+ items)
        //(6+ items per order)
        B2B,

        B2C,

        Staff,

        Unknown,

        Competition,

        Test
    }
}