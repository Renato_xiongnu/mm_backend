﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Model
{
    public class WarrantyInsurance
    {
        public string WarrantyNumber { get; set; }
        public int WarrantyDocumentPositionNumber { get; set; }
        public int WarrantyRelatedArticleNo { get; set; }
        public decimal WarrantySum { get; set; }
        public string WarrantyWwsCertificateState { get; set; }
        public string WarrantyWwsExtensionPrint { get; set; }
    }
}
