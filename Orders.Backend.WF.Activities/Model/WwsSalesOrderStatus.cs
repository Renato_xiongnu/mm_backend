﻿namespace Orders.Backend.WF.Activities.Model
{
    //TODO  Добавить оставшиеся статусы        
    public enum WwsSalesOrderStatus
    {
        None,

        ToPay = 1,        

        Paid = 2,

        Canceled = 3,

        Transformed = 4,

        Prepaid = 5,
    }
}