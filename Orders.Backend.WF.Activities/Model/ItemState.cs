﻿namespace Orders.Backend.WF.Activities.Model
{
    public enum ItemState
    {
        New = 0,
        DisplayItem = 1,
        Repaired = 2
    }
}