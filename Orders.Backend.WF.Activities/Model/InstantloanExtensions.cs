﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.WF.Activities.Proxy.Instantloan;

namespace Orders.Backend.WF.Activities.Model
{
    public static class InstantloanExtensions
    {
        public static ShoppingCartItem ToShoppingCartItem(this OrderItem orderLine)
        {
            return new ShoppingCartItem
                {
                    ArticleId = Convert.ToInt64(orderLine.Number), 
                    Price = orderLine.Price,
                    ProductGroupId = Convert.ToString(orderLine.WWSProductGroupNumber),
                    Qty = orderLine.Quantity,
                    Title = orderLine.WwsDescription
                };
        }

        public static GetBrokerUriRequest ToGetBrokerUriRequest(this Order order, string brokerUri)
        {
            return new GetBrokerUriRequest
                {
                    Body = new GetBrokerUriRequestBody(order.Id, brokerUri)
                };
        }
        
        public static UpdateShoppingCartRequest ToUpdateShoppingCartRequest(this Order order)
        {
            return new UpdateShoppingCartRequest()
                {
                    Body = new UpdateShoppingCartRequestBody()
                        {
                            OrderId = order.Id,
                            StoreId = order.Store.SapCode,
                            CartItems = order.Items.Select(el => el.ToShoppingCartItem()).ToArray(),
                            TotalPrice = order.Items.Sum(el => el.Price)
                        }
                };
        }

        public static UpdateUriForRedirectionRequest ToUpdateUriForRedirectionRequest(this Order order, string redirectUrl)
        {
            return new UpdateUriForRedirectionRequest()
                {
                    Body = new UpdateUriForRedirectionRequestBody()
                        {
                            OrderId = order.Id,
                            Uri = redirectUrl
                        }
                };
        }

        public static CancelLoanRequest ToCancelLoanRequest(this Order order, string rejectReason)
        {
            return new CancelLoanRequest()
                {
                    Body = new CancelLoanRequestBody()
                        {
                            OrderId = order.Id,
                            Reason = rejectReason
                        }
                };
        }

        public static InstantloanNotificationMessage ToInstantloanNotification(this string message, string orderId)
        {
            return new InstantloanNotificationMessage()
                {
                    OrderId = orderId,
                    Text = message
                };
        }

        public static CreditBrokerApprovalMessage ToCreditBrokerApprovalMessage(this Order order, string creditBrokerUrl)
        {
            return new CreditBrokerApprovalMessage()
                {
                    Id = order.Id,
                    CreditBrokerUrl = creditBrokerUrl
                };
        }
    }

    public class InstantloanNotificationMessage
    {
        public string OrderId { get; set; }

        public string Text { get; set; }
    }

    public class CreditBrokerApprovalMessage
    {
        public string Id { get; set; }

        public string CreditBrokerUrl { get; set; }
    }
}
