﻿using System;
using Orders.Backend.WF.Activities.Properties;

namespace Orders.Backend.WF.Activities.Model
{
    public class Delivery
    {
        public Delivery()
        {
            PickupLocation = new PickupLocation();
        }

        public string Address { get; set; }

        public string AddressWithRequestedService
        {
            get
            {
                string translatedServiceOption;
                if (RequestedDeliveryServiceOption == null)
                {
                    RequestedDeliveryServiceOption = string.Empty;
                }
                switch (RequestedDeliveryServiceOption.ToLower())
                {
                    case "curbside":
                    {
                        translatedServiceOption = "до обочины";
                        break;
                    }

                    case "doorstepembatted":
                    {
                        translatedServiceOption = "до двери с возможностью подъезда";
                        break;
                    }
                    case "doorstepunfixed":
                    {
                        translatedServiceOption = "до двери БЕЗ возможности подъезда";
                        break;
                    }
                    case "apartmentwithoutlift":
                    {
                        translatedServiceOption = "до квартиры БЕЗ лифта";
                        break;
                    }
                    case "apartmentwithlift":
                    {
                        translatedServiceOption = "до квартиры на лифте";
                        break;
                    }
                    default:
                    {
                        translatedServiceOption = RequestedDeliveryServiceOption;
                        break;
                    }
                }
                return string.Format("{0}, эт.{1}, {2}, {3}", Address, Floor, translatedServiceOption,
                    string.IsNullOrEmpty(RequestedDeliveryTimeslot)
                        ? Settings.Default.DefaultRequestedDeliveryTimeslotForDeliver
                        : RequestedDeliveryTimeslot);
            }
        }

        public string City { get; set; }

        public string DeliveryCity
        {
            get { return ShippingMethod == ShippingMethod.Delivery ? City : PickupLocation.City; }
        }

        public bool HasDelivery { get; set; }

        public ShippingMethod ShippingMethod
        {
            get
            {
                if (HasDelivery) return ShippingMethod.Delivery;
                if (PickupLocation != null && PickupLocation.PickupPointType != PickupPointType.Shop)
                    return ShippingMethod.PickupExternal;
                return ShippingMethod.PickupShop;
            }
        }

        public DateTime? DeliveryDate { get; set; }

        public string DeliveryPeriod { get; set; }

        public string DeliveryComment { get; set; }

        public string ExactAddress { get; set; }

        public bool TransferAgreementWithCustomer { get; set; }

        public string ZIPCode { get; set; }

        public string DeliveryZIPCode
        {
            get { return ShippingMethod == ShippingMethod.Delivery ? ZIPCode : PickupLocation.ZipCode; }
        }


        public string Region { get; set; }

        public string House { get; set; }

        public string Housing { get; set; }

        public string Building { get; set; }

        public string Apartment { get; set; }

        public string Entrance { get; set; }

        public string EntranceCode { get; set; }

        public int? Floor { get; set; }

        public string SubwayStationName { get; set; }

        public string District { get; set; }

        public bool? HasServiceLift { get; set; }

        public string RequestedDeliveryTimeslot { get; set; }

        public string Street { get; set; }

        public string Kladr { get; set; }

        public string PickupLocationId { get; set; }

        public string StreetAbbr { get; set; }

        public PickupLocation PickupLocation { get; set; }

        public string RequestedDeliveryServiceOption { get; set; }
    }

    public enum ShippingMethod
    {
        PickupShop,
        PickupExternal,
        Delivery
    }
}