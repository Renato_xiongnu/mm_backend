﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Model
{
    public class DocumentInfo
    {
        public string OutletInfo { get; set; }

        public string ProductPickupInfo { get; set; }

        public string PrintableInfo { get; set; }

        public int? SalesPersonId { get; set; }
    }
}
