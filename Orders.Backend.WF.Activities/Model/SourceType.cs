namespace Orders.Backend.WF.Activities.Model
{
    public enum SourceType
    {
        CallCenter = 0,
        // ReSharper disable InconsistentNaming
        IPhone = 1,
        // ReSharper restore InconsistentNaming
        MobileWebSite = 2,

        WebSite = 3,

        Windows8 = 4,

        Store = 5,

        Avito = 6,

        Metro = 7,

        MetroCallCenter = 8,

        WebSiteQuickOrder = 9,

        MobileWebSiteQuickOrder = 10,

        YandexMarket = 11,

        IPhoneQuickOrder = 12
    }
}