﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Model
{
    public class OrderCertificate
    {
        public string KuponNo { get; set; }
        public string KuponNoImage { get; set; }
        public decimal KuponAmount { get; set; }
    }
}
