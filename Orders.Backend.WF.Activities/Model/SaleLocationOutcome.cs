﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Model
{
    public class SaleLocationOutcome
    {        
        public string SapCode { get; set; }
        
        public string Outcome { get; set; }
       
        public string Performer { get; set; }
        
        public DateTime Created { get; set; }
    }
}
