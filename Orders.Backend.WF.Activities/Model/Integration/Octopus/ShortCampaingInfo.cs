﻿using System;

namespace Orders.Backend.WF.Activities.Model.Integration.Octopus
{
    public class ShortCampaingInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime Finish { get; set; }
        public DateTime Start { get; set; }
        public string CampaignTypeCode { get; set; }
        public string CampaignTypeName { get; set; }
    }
}