﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;

namespace Orders.Backend.WF.Activities.Model
{
    public class Order
    {
        private Customer _consigneeInfo;

        private Delivery _delivery;

        public Order()
        {
            WwsOrders = new Collection<WwsOrder>();
        }

        public string Comment { get; set; }

        public string Id { get; set; }

        public OrderState State { get; set; }

        public bool IsPreOrder { get; set; }

        public string NewStateReason { get; set; }

        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public bool IsTest { get; set; }

        public Store Store { get; set; }

        public PaymentType Payment { get; set; }

        public PaymentType FinalPayment { get; set; }

        public string OrderBarcodeImage { get; set; }

        public OrderCertificate Certificate { get; set; }

        public string Document { get; set; }

        public string YandexMarketUrl { get; set; }

        public DocumentInfo InitDocumentInfo { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public SourceType Source { get; set; }

        public Collection<WwsOrder> WwsOrders { get; set; }

        public string SaleLocationSapCode { get; set; }

        public WwsOrder CurrentWwsOrder
        {
            get { return WwsOrders.LastOrDefault(); }
        }

        public WwsOrder PreviousWwsOrder
        {
            get
            {
                if(WwsOrders != null && WwsOrders.Any())
                {
                    return WwsOrders.Reverse().Skip(1).FirstOrDefault(i => i.Kept);
                }
                return null;
            }
        }

        public Delivery Delivery
        {
            get { return _delivery??(_delivery=new Delivery()); }
            set { _delivery = value; }
        }

        public string DeliveryName
        {
            get
            {
                return Delivery == null || !Delivery.HasDelivery
                    ? Backend_Activities.Pickup
                    : Backend_Activities.Delivery;
            }
        }

        public Collection<OrderItem> Items { get; set; }

        public Customer Customer { get; set; }

        public Customer ConsigneeInfo
        {
            get { return _consigneeInfo??(_consigneeInfo=new Customer()); }
            set { _consigneeInfo = value; }
        }

        public ReserveState ReserveState { get; set; }

        public bool DeliveryDateApproval { get; set; }

        public PrintingStatus DocumentPrintingStatus { get; set; }

        public bool IsOnlineOrder { get; set; }

        public ZZTHeaderData ZztData { get; set; }

        public bool CanProcessed
        {
            get
            {
                return Items != null && Items.All(i =>
                    i.ReviewState == OrderItemState.Reserved ||
                    i.ReviewState == OrderItemState.ReservedDisplayItem ||
                    i.ReviewState == OrderItemState.ReservedPart);
            }
        }

        public bool NeedRecreateReserve
        {
            get
            {
                var wwsReserve = CurrentWwsOrder;
                return
                    wwsReserve == null ||
                    Items.Count != wwsReserve.Items.Count ||
                    !Items.All(i =>
                        wwsReserve.Items.Any(r =>
                            r.Article.ToString(CultureInfo.InvariantCulture) == i.Number &&
                            r.Price == i.Price &&
                            r.Quantity == i.Quantity));
            }
        }

        public bool AnyItemKept
        {
            get { return WwsOrders != null && WwsOrders.Any(i => i.Kept); }
        }

        public bool NeedRetry
        {
            get { return Items != null && Items.Any(i => i.ReviewState == OrderItemState.NotFound); }
        }

        public bool HasInstallationService
        {
            get { return Items != null && Items.Any(i => i.ArticleProductType == ArticleProductType.InstallationService); }
        }

        public bool CreatedSuccessfully { get; set; }

        public DateTime? ExternalExpirationDate { get; set; }

        public string ExternalSystem { get; set; }

        public string ExternalNumber { get; set; }

        public string PickupAddress
        {
            get
            {
                return Delivery != null && !Delivery.HasDelivery && Delivery.PickupLocation != null &&
                       Delivery.ShippingMethod == ShippingMethod.PickupExternal
                    ? Delivery.PickupLocation.Address
                    : (Store != null ? Store.Address : null);
            }
        }

        public decimal TotalPrice
        {
            get { return Items != null ? Items.Sum(el => el.Price*el.Quantity) : 0M; }
        }

        public decimal Prepay { get; set; }

        public DateTime Updated { get; set; }

        public void AttachNewWwsOrder(string wwsOrderId)
        {
            WwsOrders.Add(new WwsOrder
            {
                Id = wwsOrderId,
                SapCode = Store.SapCode
            });
        }

        public override string ToString()
        {
            return String.Format(Backend_Activities.OrderToString, Id);
        }
        
        internal void MergeReserve(bool updateWwsStatus)
        {
            var reserve = WwsOrders.LastOrDefault();
            if(reserve == null || Items == null)
            {
                return;
            }

            foreach(var item in Items)
            {
                var merged = false;
                foreach(var wwsOrderItem in reserve.Items)
                {
                    if(item.Number == wwsOrderItem.Article.ToString(CultureInfo.InvariantCulture) &&
                       item.Quantity == wwsOrderItem.Quantity &&
                       item.Price == wwsOrderItem.Price)
                    {
                        item.MergeItem(wwsOrderItem);

                        foreach(var subWwsOrderItem in wwsOrderItem.SubOrderItems) //Items of sets
                        {
                            var subOrderItem =
                                item.SubOrderItems.FirstOrDefault(
                                    el =>
                                        el.Number == subWwsOrderItem.Article.ToString(CultureInfo.InvariantCulture));
                            if(subOrderItem == null)
                            {
                                subOrderItem = new OrderItem();
                                item.SubOrderItems.Add(subOrderItem);
                            }
                            subOrderItem.MergeSubItem(subWwsOrderItem);
                        }
                        merged = true;
                    }
                }
                if(!merged && updateWwsStatus)
                {
                    item.Comment += String.Concat(Environment.NewLine, Backend_Activities.Err_ReserveNotFound);
                }
            }

            var newReserveItems =
                reserve.Items.Where(
                    wwsItem =>
                        wwsItem.ProductType == ProductType.WarrantyPlus); //new items, 
            //created by CreateReserve activity (warranty)

            newReserveItems =
                newReserveItems.Where(
                    wwsItem =>
                        Items.Any(
                            orderItem =>
                                orderItem.SubOrderItems.Any(
                                    subOrderItem =>
                                        subOrderItem.WarrantyInsurance != null &&
                                        subOrderItem.WarrantyInsurance.WarrantyRelatedArticleNo == wwsItem.Article)))
                    .ToList();

            foreach(var wwsOrderItem in newReserveItems)
            {
                Items.Add(wwsOrderItem.ToOrderItem());
            }
        }

        internal void MergeDelivery(Proxy.WWS.Delivery delivery)
        {
            if(Delivery == null)
            {
                Delivery = new Delivery();
            }
            //Delivery.DeliveryComment = delivery.Comment;            
            //Delivery.DeliveryPeriod = delivery.Period;
            //Delivery.DeliveryDate = delivery.Date;
        }

        internal void MergeCustomer(Proxy.WWS.Customer customer)
        {
            if(Customer == null)
            {
                Customer = new Customer();
            }
            MergeCustomerInternal(Customer, customer);
        }

        internal void MergeConsignee(Proxy.WWS.Customer customer)
        {
            if(ConsigneeInfo == null)
            {
                ConsigneeInfo = new Customer();
            }
            MergeCustomerInternal(ConsigneeInfo, customer);
        }

        private void MergeCustomerInternal(Customer customer, Proxy.WWS.Customer customer1)
        {
            customer.Address = customer1.Address;
            customer.AddressIndex = customer1.AddressIndex;
            customer.City = customer1.City;
            customer.ClassificationNumber = customer1.ClassificationNumber;
            customer.CountryAbbreviation = customer1.CountryAbbreviation;
            customer.Email = customer1.Email;
            customer.INN = customer1.INN;
            customer.KPP = customer1.KPP;
            customer.Name = customer1.Name;
            customer.Phone = customer1.Phone;
            customer.Phone2 = customer1.Phone2;
            customer.Salutation = customer1.Salutation;
            customer.Surname = customer1.Surname;
        }
    }
}