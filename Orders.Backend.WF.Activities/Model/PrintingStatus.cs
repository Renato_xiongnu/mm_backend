﻿namespace Orders.Backend.WF.Activities.Model
{
    public enum PrintingStatus
    {
        None = 0,
        Printed = 1,
        Error = 2,
    }
}
