namespace Orders.Backend.WF.Activities.Model
{
    public enum PaymentType
    {
        Cash = 0,

        Online = 1,

        OnlineCredit = 2,

        SocialCard = 3
    }
}