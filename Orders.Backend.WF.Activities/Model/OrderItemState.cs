namespace Orders.Backend.WF.Activities.Model
{
    public enum OrderItemState
    {
        Empty = 0,
        Reserved = 1,
        ReservedPart = 2,
        ReservedDisplayItem = 3,
        NotFound = 4,
        IncorrectPrice = 5,
        Transfer = 6,        
        DisplayItem = 7,        
        TooLowPrice = 8,
    }
}