namespace Orders.Backend.WF.Activities.Model
{
    public enum OrderState
    {
        Created = 0,

        Confirmed = 1,

        Rejected = 2,

        RejectedByCustomer = 3,

        ConfirmedWithGiftCertificate = 4,

        GiftCertificateNumberReservedNotifyCustomer = 5,

        OnlinePaymentReceived = 6,

        OnlinePaymentDeclined = 7,

        Paid = 8,

        Closed = 9,

        GiftCertificateCredited = 10,

        GiftCertificateCreditedNotifyCustomer = 11,

        GiftCertificateCreditedError = 12,

        Shipped = 13
    }
}