using System;

namespace Orders.Backend.WF.Activities.Model
{
    public class Customer
    {
        public string ClassificationNumber { get; set; }

        public string Salutation { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Phone2 { get; set; }

        public string Address { get; set; }

        public string CountryAbbreviation { get; set; }

        public string AddressIndex { get; set; }

        public string City { get; set; }

        public string INN { get; set; }

        public string KPP { get; set; }

        public string Fax { get; set; }

        public CustomerType CustomerType { get; set; }

        public string EFirstName { get; set; }

        public string ELastName { get; set; }

        public string Gender { get; set; }

        public DateTime? BirthDay { get; set; }

        public string ZztCustomerId { get; set; }

        public override string ToString()
        {
            return String.Concat(Name, " ", Surname);
        }        

        public CardInfo SocialCard { get; set; }
    }
}