﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Model
{
    public class Task
    {
        public string TaskId { get; set; }

        public TaskStatus CurrentState { get; set; }

        public string TaskType { get; set; }
    }

    public enum TaskStatus
    {
        New,
        Created,
        Canceled,
        Completed
    }
}
