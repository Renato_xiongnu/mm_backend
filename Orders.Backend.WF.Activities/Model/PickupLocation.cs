﻿namespace Orders.Backend.WF.Activities.Model
{
    public class PickupLocation
    {
        public string PickupLocationId { get; set; }

        public string Title { get; set; }

        public string Address { get; set; }

        public PickupPointType PickupPointType { get; set; }

        public string OperatorPuPId { get; set; }

        public string OperatorName { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }
    }

    public enum PickupPointType
    {
        Shop,
        Postomat
    }
}
