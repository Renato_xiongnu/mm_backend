﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Orders.Backend.WF.Activities.Model
{
    [DataContract]
    public class BasketYandexMarketRequest
    {
        [DataMember(Name = "getLink", IsRequired = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public LinkType GetLinkType { get; set; }

        [DataMember(Name = "article", EmitDefaultValue = false)]
        public string Article { get; set; }

        [DataMember(Name = "sapCode", EmitDefaultValue = false)]
        public string SapCode { get; set; }
    }

    [DataContract]
    public class BasketYandexMarketResponse
    {
        [DataMember(Name = "request")]
        public BasketYandexMarketRequest Request { get; set; }

        [DataMember(Name = "link")]
        public string Link { get; set; }
    }

    public enum LinkType
    {
        [EnumMember]
        ItemReview,

        [EnumMember]
        YandexMarketReview
    }
}