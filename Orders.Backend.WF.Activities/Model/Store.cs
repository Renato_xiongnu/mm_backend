using System;

namespace Orders.Backend.WF.Activities.Model
{
    public class Store
    {
        [Obsolete("Please use OfficiallyStoreName instead")]
        public string DisplayName
        {
            get { return OfficiallyStoreName; }
        }

        public string McsId { get; set; }

        public string SapCode { get; set; }

        public string OfficiallyStoreName { get; set; }

        public TimeSpan ReserveLifeTime { get; set; }

        public string Address { get; set; }

        public string CityName { get; set; }

        public string Telephone { get; set; }

        public int? PaidDeliveryArticleId { get; set; }

        public int? FreeDeliveryArticleId { get; set; }

        public TimeZoneInfo TimeZone { get; set; }

        public TimeSpan StartWorkingTime { get; set; }

        public TimeSpan EndWorkingTime { get; set; }

        public string StoreCentralOfficeName { get; set; }

        public string LegalAddress { get; set; }

        public string Index { get; set; }

        public string KPP { get; set; }

        public string INN { get; set; }

        public string Fax { get; set; }

        public string OGRN { get; set; }

        public string TaxAuthority { get; set; }

        public string BankName { get; set; }

        public string BankBIK { get; set; }

        public string BankCurrentAccount { get; set; }

        public string BankCorrespondentAccount { get; set; }

        public string ManagingDirector { get; set; }

        public bool IsVirtual { get; set; }

        public bool IsActive { get; set; }

        public bool IsHub { get; set; }
    }
}