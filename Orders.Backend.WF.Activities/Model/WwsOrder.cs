using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Orders.Backend.WF.Activities.Properties;

namespace Orders.Backend.WF.Activities.Model
{
    public class WwsOrder
    {
        public WwsOrder()
        {
            this.Items = new Collection<WwsOrderItem>();
        }

        public string Id { get; set; }
        public Collection<WwsOrderItem> Items { get; set; }
        public bool Kept { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreationDateString { get { return CreationDate.ToString(Settings.Default.ReportDateFormat, CultureInfo.InvariantCulture); } }

        public string StoreManagerName { get; set; }
        public WwsOrderTotalPrice TotalPrice { get; set; }
        public WwsOrderTotalPrice PrepaymentTotalPrice { get; set; }
        public WwsOrderTotalPrice LeftoverTotalPrice { get; set; }
        public DocumentInfo DocumentInfo { get; set; }

        public string DocBarcode { get; set; }
        public string DocBarcodeImage { get; set; }
        //public decimal DownpaymentPrice { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public decimal OriginalSum { get; set; }
        public WwsSalesOrderStatus OrderStatus { get; set; }

        public string SapCode { get; set; }
    }
}