﻿using System;
using System.Activities.Expressions;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using OnlineOrders.Common.Helpers;
using Orders.Backend.WF.Activities.Proxy.Online;
using Orders.Backend.WF.Activities.Proxy.WWS;
using OrderInfo = Orders.Backend.WF.Activities.Proxy.WWS.OrderInfo;
using ReservePrice = Orders.Backend.WF.Activities.Proxy.Online.ReservePrice;

namespace Orders.Backend.WF.Activities.Model
{
    public static class Extensions
    {
        private static readonly TimeZoneInfo DefaultTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
        private static readonly string[] StaffDomains;

        static Extensions()
        {
            StaffDomains = (Backend_Activities.CustomerTypeStaffEmailDomains ?? string.Empty)
                .Split(';')
                .Select(d => d.Trim())
                .Where(d => !d.IsNullOrEmpty())
                .Select(d => d.ToLower())
                .ToArray();
        }

        public static OrderState ToOrderState(this InternalOrderStatus internalOrderStatus)
        {
            switch (internalOrderStatus)
            {
                case InternalOrderStatus.Blank:
                    break;
                case InternalOrderStatus.Created:
                    return OrderState.Created;
                case InternalOrderStatus.Confirmed:
                    return OrderState.Confirmed;
                case InternalOrderStatus.GiftCertificateNumberReservedNotifyCustomer:
                    return OrderState.GiftCertificateNumberReservedNotifyCustomer;
                case InternalOrderStatus.Paid:
                    return OrderState.Paid;
                case InternalOrderStatus.GiftCertificateCredited:
                    return OrderState.GiftCertificateCredited;
                case InternalOrderStatus.GiftCertificateCreditedNotifyCustomer:
                    return OrderState.GiftCertificateCreditedNotifyCustomer;
                case InternalOrderStatus.GiftCertificateCreditedError:
                    return OrderState.GiftCertificateCreditedError;
                case InternalOrderStatus.LifeCycleIgnored:
                    break;
                case InternalOrderStatus.Closed:
                    return OrderState.Closed;
                case InternalOrderStatus.Rejected:
                    return OrderState.Rejected;
                case InternalOrderStatus.RejectedByCustomer:
                    return OrderState.RejectedByCustomer;
                case InternalOrderStatus.Shipped:
                    return OrderState.Shipped;
                default:
                    throw new ArgumentOutOfRangeException("internalOrderStatus");
            }
            throw new ArgumentOutOfRangeException("internalOrderStatus");
        }


        private static string CreateShortAddress(Delivery delivery)
        {
            var sb = new StringBuilder();

            if (delivery.ShippingMethod != ShippingMethod.Delivery) return delivery.PickupLocation.Address;

            if (!string.IsNullOrEmpty(delivery.StreetAbbr))
                sb.AppendFormat("{0}.,", delivery.StreetAbbr);
            if (!string.IsNullOrEmpty(delivery.Street))
                sb.AppendFormat("{0},", delivery.Street);
            if (!string.IsNullOrEmpty(delivery.House))
            {
                var house = delivery.House.ToUpperInvariant().Replace("ДОМ", "д.");
                house = house.Replace("КОРПУС", "к.");
                sb.AppendFormat("{0},", house);
            }
            if (!string.IsNullOrEmpty(delivery.Apartment))
            {
                var apartment = delivery.Apartment.ToUpperInvariant().Replace("КВАРТИРА", "кв.");
                sb.Append(apartment);
            }

            return sb.ToString();
        }

        public static OrderInfo ToOrderInfo(this Order order)
        {
            var result = new OrderInfo
            {
                SapCode = order.Store.SapCode,
                Customer = new CustomerInfo
                {
                    Address = CreateShortAddress(order.Delivery),//order.Delivery.Address,
                    City = order.Delivery.DeliveryCity,
                    Email = order.Customer.Email,
                    FirstName = order.Customer.Name,
                    Surname = order.Customer.Surname,
                    Mobile = order.Customer.Phone,
                    Phone = order.Customer.Phone,
                    ZipCode = order.Delivery.DeliveryZIPCode//Fake for WWS
                },
                IsFiscalInvoice = false,
                IsOnlineOrder = order.IsOnlineOrder,
                Comment = "Created from TicketTool",
                OnlineOrderId = order.Id,
                ShippingMethod =
                    order.Delivery.HasDelivery
                        ? Proxy.WWS.ShippingMethod.Delivery
                        : Proxy.WWS.ShippingMethod.Pickup,
                OrderLines = order.Items.Select(i => i.ToOrderLine()).ToArray(),
                SourceSystem = "MediaMarkt",
                PaymentType = order.Payment.ToWwsPaymentType(),
                DeliveryDate = order.Delivery.DeliveryDate,
            };
            if (order.InitDocumentInfo != null)
            {
                result.OutletInfo = order.InitDocumentInfo.OutletInfo;
                result.ProductPickupInfo = order.InitDocumentInfo.ProductPickupInfo;
                result.PrintableInfo = order.InitDocumentInfo.PrintableInfo;
                result.SalesPersonId = order.InitDocumentInfo.SalesPersonId;
            }

            if (order.Delivery.ShippingMethod != ShippingMethod.PickupShop)
            {
                result.OutletInfo = !string.IsNullOrEmpty(result.OutletInfo)
                    ? string.Format("{0}; {1}", result.OutletInfo, order.Delivery.AddressWithRequestedService)
                    : order.Delivery.AddressWithRequestedService;
                result.PrintableInfo += order.Delivery.AddressWithRequestedService;
            }

            if (order.Payment == PaymentType.OnlineCredit) //TODO Говнецо
            {
                result.PrintableInfo += "Онлайн-кредит";
            }
            if (order.Payment == PaymentType.SocialCard)
            {
                result.PrintableInfo += string.Format("Социальная карта {0}", order.Customer.SocialCard.Number);
                result.Downpayment = order.Prepay;
            }
            if (order.Payment == PaymentType.Online)
            {
                result.Downpayment = order.TotalPrice - order.Prepay; //Отличается из-за бага в SocialCard
            }
            if (order.Store.IsHub)
            {
                result.ShippingMethod = Proxy.WWS.ShippingMethod.Pickup;
            }

            return result;
        }

        public static UpdateOrderData ToUpdateOrderData(this Order order)
        {
            var result = new UpdateOrderData
            {
                SapCode = order.Store == null || string.IsNullOrEmpty(order.Store.SapCode) ? null : order.Store.SapCode,
                OrderId = order.Id,
                ReasonText = order.NewStateReason,
                UpdatedBy = order.UpdatedBy,
                DeliveryInfo = order.Delivery.ToDeliveryInfo(),
                WWSOrderId = order.WwsOrders.Select(w => w.Id).LastOrDefault(),
                FinalPaymentType = order.FinalPayment.ToPaymentType(),
                ChangeStatusTo = order.State.ToGeneralOrderState(),
                ClientData = order.Customer == null
                    ? new ClientData()
                    : order.Customer.ToClientData(),
                ReserveContent = order.Document,
                IsPreorder = order.IsPreOrder,
                Comment = order.Comment,
                ExpirationDate = order.ExpirationDate
            };
            return result;
        }

        public static WwsOrderItem ToWwsOrderItem(this OrderLine orderLine)
        {
            var orderItem =
                new WwsOrderItem
                {
                    Article = orderLine.ArticleNo,
                    DepartmentNumber = orderLine.DepartmentNumber,
                    Title = orderLine.Description,
                    Description = "", //TODO: гарантия + или описание акция
                    FreeQuantity = orderLine.FreeQuantity,
                    PriceOrig = orderLine.PriceOrig,
                    ProductGroup = orderLine.ProductGroupNo,
                    ProductGroupName = orderLine.ProductGroupName,
                    Quantity = orderLine.Quantity,
                    ReservedQuantity = orderLine.ReservedQuantity,
                    StockNumber = orderLine.StockNumber,
                    PositionNumber = orderLine.PositionNumber,
                    StoreNumber = orderLine.StoreNumber,
                    VAT = (int)orderLine.TotalPrice.VAT,
                    Price = orderLine.Price,
                    HasShippedFromStock = orderLine.HasShippedFromStock,
                    SerialNumber = orderLine.SerialNumber,
                    ProductType = orderLine.ProductType.ToProductType()
                };
            if (orderLine.WarrantyInsurance != null)
            {
                orderItem.WarrantyInsurance.WarrantyNumber = orderLine.WarrantyInsurance.Number;
                orderItem.WarrantyInsurance.WarrantyDocumentPositionNumber =
                    orderLine.WarrantyInsurance.DocumentPositionNumber;
                orderItem.WarrantyInsurance.WarrantyRelatedArticleNo = orderLine.WarrantyInsurance.RelatedArticleNo;
                orderItem.WarrantyInsurance.WarrantySum = orderLine.WarrantyInsurance.WarrantySum;
                orderItem.WarrantyInsurance.WarrantyWwsCertificateState =
                    orderLine.WarrantyInsurance.WwsCertificateState;
                orderItem.WarrantyInsurance.WarrantyWwsExtensionPrint = orderLine.WarrantyInsurance.WwsExtensionPrint;
            }
            if (orderLine.SubLines != null)
            {
                foreach (var subLine in orderLine.SubLines)
                {
                    orderItem.SubOrderItems.Add(subLine.ToWwsOrderItem());
                }
            }

            return orderItem;
        }

        public static ReserveInfo ToReserveInfo(this Order order)
        {
            var wwsOrder = order.WwsOrders == null
                ? null
                : order.WwsOrders.LastOrDefault();
            var reserveLines = order.ToReserveLines();
            var deliveryPeriod = order.Delivery.ToDeliveryPeriod();
            var reserveCustomerInfo = order.ConsigneeInfo.ToReserveCustomerInfo();
            var result = new ReserveInfo
            {
                ReserveLines = reserveLines,
                CustomerInfo = reserveCustomerInfo,
                DeliveryPeriod = deliveryPeriod,
            };
            if (wwsOrder != null)
            {
                result.SapCode = wwsOrder.SapCode;
                result.CreationDate = wwsOrder.CreationDate;
                result.StoreManagerName = wwsOrder.StoreManagerName;
                result.LeftoverPrice = new ReservePrice
                {
                    GrossPrice = wwsOrder.LeftoverTotalPrice.GrossPrice,
                    NetPrice = wwsOrder.LeftoverTotalPrice.NETPrice,
                    Vat = wwsOrder.LeftoverTotalPrice.VAT,
                    VatPrice = wwsOrder.LeftoverTotalPrice.VATPrice,
                };
                result.PrepaymentPrice = new ReservePrice
                {
                    GrossPrice = wwsOrder.PrepaymentTotalPrice.GrossPrice,
                    NetPrice = wwsOrder.PrepaymentTotalPrice.NETPrice,
                    Vat = wwsOrder.PrepaymentTotalPrice.VAT,
                    VatPrice = wwsOrder.PrepaymentTotalPrice.VATPrice
                };
                result.TotalPrice = new ReservePrice
                {
                    GrossPrice = wwsOrder.TotalPrice.GrossPrice,
                    NetPrice = wwsOrder.TotalPrice.NETPrice,
                    Vat = wwsOrder.TotalPrice.VAT,
                    VatPrice = wwsOrder.TotalPrice.VATPrice
                };
            }

            if (order.InitDocumentInfo != null)
            {
                result.SalesDocumentData = new SalesDocumentData
                {
                    OutletInfo = order.InitDocumentInfo.OutletInfo,
                    ProductPickupInfo = order.InitDocumentInfo.ProductPickupInfo,
                    PrintableInfo = order.InitDocumentInfo.PrintableInfo,
                    SalesPersonId = order.InitDocumentInfo.SalesPersonId
                };
            }
            return result;
        }

        public static ReserveLine[] ToReserveLines(this Order order)
        {
            return order.Items
                .Select(i => i.ToReserveLine())
                .ToArray();
        }

        public static ReserveLine ToReserveLine(this OrderItem orderItem)
        {
            var reserveLine = new ReserveLine
            {
                ReviewItemState = orderItem.ReviewState.ToReviewItemState(),
                WWSDepartmentNumber = orderItem.WWSDepartmentNumber,
                WWSProductGroupName = orderItem.WWSProductGroupName,
                WWSProductGroupNumber = orderItem.WWSProductGroupNumber,
                WWSStockNumber = orderItem.WWSStockNumber,
                WWSFreeQty = orderItem.WWSFreeQty,
                WWSPriceOrig = orderItem.WWSPriceOrig,
                WWSReservedQty = orderItem.WWSReservedQty,
                Title = orderItem.WwsDescription,
                Comment = orderItem.Comment,
                ArticleData = orderItem.ToArticleData(),
                LineId = orderItem.Id,
                ArticleCondition = orderItem.ArticleCondition,
                StockItemState = orderItem.State.ToItemState(),
                WWSVAT = orderItem.WWSVat,
                WWSPositionNumber = orderItem.WWSPositionNumber,
                WWSStoreNumber = orderItem.WWSStoreNumber,
                HasShippedFromStock = orderItem.HasShippedFromStock,
                SerialNumber = orderItem.SerialNumber,
                ProductType = orderItem.ProductType.ToOnlineProductType(),
                ArticleProductType = orderItem.ArticleProductType.ToOnlineArticleProductType(),
                TransferSapCode = orderItem.TransferSapCode,
                TransferNumber = orderItem.TransferNumber,
                Benefit = orderItem.Benefit,
                CouponCode = orderItem.CouponCode,
                CouponCompainId = orderItem.CouponCompainId,
                OldPrice = orderItem.OldPrice,
                WarrantyInsurance = new Proxy.Online.WarrantyInsurance
                {
                    Number = orderItem.WarrantyInsurance.WarrantyNumber,
                    DocumentPositionNumber = orderItem.WarrantyInsurance.WarrantyDocumentPositionNumber,
                    RelatedArticleNo = orderItem.WarrantyInsurance.WarrantyRelatedArticleNo,
                    WarrantySum = orderItem.WarrantyInsurance.WarrantySum,
                    WwsCertificateState = orderItem.WarrantyInsurance.WarrantyWwsCertificateState,
                    WwsExtensionPrint = orderItem.WarrantyInsurance.WarrantyWwsExtensionPrint
                },
                SubLines = orderItem.SubOrderItems.Select(subOrderItem => subOrderItem.ToReserveLine()).ToArray()
            };

            return reserveLine;
        }

        public static ReserveCustomerInfo ToReserveCustomerInfo(this Customer customer)
        {
            return customer != null
                ? new ReserveCustomerInfo
                {
                    Address = customer.Address,
                    AddressIndex = customer.AddressIndex,
                    City = customer.City,
                    ClassificationNumber = customer.ClassificationNumber,
                    CountryAbbreviation = customer.CountryAbbreviation,
                    Email = customer.Email,
                    INN = customer.INN,
                    KPP = customer.KPP,
                    Name = customer.Name,
                    Phone = customer.Phone,
                    Phone2 = customer.Phone2,
                    Salutation = customer.Salutation,
                    Surname = customer.Surname
                }
                : new ReserveCustomerInfo();
        }

        public static DeliveryPeriod ToDeliveryPeriod(this Delivery delivery)
        {
            return delivery == null
                ? new DeliveryPeriod()
                : new DeliveryPeriod
                {
                    Comment = delivery.DeliveryComment,
                    DeliveryDate = delivery.DeliveryDate,
                    Period = delivery.DeliveryPeriod
                };
        }

        public static Store ToStore(this StoreData storeData)
        {
            var store = new Store
            {
                PaidDeliveryArticleId = storeData.PaidDeliveryArticleId,
                FreeDeliveryArticleId = storeData.FreeDeliveryArticleId,
                ReserveLifeTime = storeData.ReserveLifeTime,
                Address = storeData.Address,
                OfficiallyStoreName = storeData.Name,
                Telephone = storeData.Phone,
                CityName = storeData.City,
                SapCode = storeData.SapCode,
                TimeZone = FindSystemTimeZoneById(storeData.TimeZome),
                StartWorkingTime = storeData.StartWorkingTime,
                EndWorkingTime = storeData.EndWorkingTime,
                IsVirtual = storeData.IsVirtual,
                IsActive = storeData.IsActive,
                IsHub = storeData.IsHub,
            };
            return store;
        }

        public static DateTime GetStoreDateTimeNow(this Store store)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, store.TimeZone);
        }

        public static DateTime GetStoreStartWorkingDateTime(this Store store, DateTime date)
        {
            date = date.Date;
            return date.Add(store.StartWorkingTime);
        }

        public static DateTime GetStoreEndWorkingDateTime(this Store store, DateTime date)
        {
            date = date.Date;
            return date.Add(store.EndWorkingTime);
        }

        public static TimeSpan GetStoreTimeBeforeEndWorkingDay(this Store store)
        {
            var now = store.GetStoreDateTimeNow();
            var end = store.GetStoreEndWorkingDateTime(now);
            var time = end - now;
            time = time < TimeSpan.Zero ? TimeSpan.Zero : time;
            return time;
        }

        public static bool StoreIsOpenNow(this Store store, DateTime? now = null)
        {
            now = !now.HasValue ? store.GetStoreDateTimeNow() : now.Value;
            var start = store.GetStoreStartWorkingDateTime(now.Value);
            var end = store.GetStoreEndWorkingDateTime(now.Value);
            return now >= start && now <= end;
        }

        public static DateTime GetTaskCompletionDateTime(this Store store, TimeSpan taskTimeout)
        {
            var now = store.GetStoreDateTimeNow();
            var start = store.GetStoreStartWorkingDateTime(now);
            var end = store.GetStoreEndWorkingDateTime(now);
            var completionDateTime = now.Add(taskTimeout);
            if (now == end && now == start)
            {
                return completionDateTime;
            }
            if (store.StoreIsOpenNow(now) && store.StoreIsOpenNow(completionDateTime))
            {
                return completionDateTime;
            }
            if (now > end || completionDateTime > end)
            {
                return store.GetStoreStartWorkingDateTime(now.AddDays(1)).Add(taskTimeout);
            }
            return store.GetStoreStartWorkingDateTime(now).Add(taskTimeout);
        }

        private static TimeZoneInfo FindSystemTimeZoneById(string timeZomeId)
        {
            if (string.IsNullOrEmpty(timeZomeId))
            {
                return DefaultTimeZoneInfo;
            }
            try
            {
                return TimeZoneInfo.FindSystemTimeZoneById(timeZomeId);
            }
            catch (Exception)
            {
                return DefaultTimeZoneInfo;
            }
        }

        public static ReviewItemState ToReviewItemState(this OrderItemState orderItemState)
        {
            switch (orderItemState)
            {
                case OrderItemState.Empty:
                    return ReviewItemState.Empty;
                case OrderItemState.Reserved:
                    return ReviewItemState.Reserved;
                case OrderItemState.ReservedPart:
                    return ReviewItemState.ReservedPart;
                case OrderItemState.ReservedDisplayItem:
                    return ReviewItemState.ReservedDisplayItem;
                case OrderItemState.NotFound:
                    return ReviewItemState.NotFound;
                case OrderItemState.IncorrectPrice:
                    return ReviewItemState.IncorrectPrice;
                case OrderItemState.Transfer:
                    return ReviewItemState.Transfer;
                case OrderItemState.DisplayItem:
                    return ReviewItemState.DisplayItem;
                case OrderItemState.TooLowPrice:
                    return ReviewItemState.TooLowPrice;
                default:
                    throw new ArgumentOutOfRangeException("orderItemState");
            }
        }

        public static Proxy.WWS.ProductType ToWwsProductType(this ProductType productType)
        {
            switch (productType)
            {
                case ProductType.Article:
                    return Proxy.WWS.ProductType.Article;
                case ProductType.Set:
                    return Proxy.WWS.ProductType.Set;
                case ProductType.WarrantyPlus:
                    return Proxy.WWS.ProductType.WarrantyPlus;
                default:
                    throw new ArgumentOutOfRangeException("productType");
            }
        }

        public static ProductType ToProductType(this Proxy.WWS.ProductType productType)
        {
            switch (productType)
            {
                case Proxy.WWS.ProductType.Article:
                    return ProductType.Article;
                case Proxy.WWS.ProductType.Set:
                    return ProductType.Set;
                case Proxy.WWS.ProductType.WarrantyPlus:
                    return ProductType.WarrantyPlus;
                default:
                    throw new ArgumentOutOfRangeException("productType");
            }
        }

        public static ProductType ToProductType(this Proxy.Online.ProductType productType)
        {
            switch (productType)
            {
                case Proxy.Online.ProductType.Article:
                    return ProductType.Article;
                case Proxy.Online.ProductType.Set:
                    return ProductType.Set;
                case Proxy.Online.ProductType.WarrantyPlus:
                    return ProductType.WarrantyPlus;
                default:
                    throw new ArgumentOutOfRangeException("productType");
            }
        }

        public static ArticleProductType ToArticleProductType(this Proxy.Online.ArticleProductType articleProductType)
        {
            switch (articleProductType)
            {
                case Proxy.Online.ArticleProductType.Product:
                    return ArticleProductType.Product;
                case Proxy.Online.ArticleProductType.Set:
                    return ArticleProductType.Set;
                case Proxy.Online.ArticleProductType.WarrantyPlus:
                    return ArticleProductType.WarrantyPlus;
                case Proxy.Online.ArticleProductType.InstallationService:
                    return ArticleProductType.InstallationService;
                case Proxy.Online.ArticleProductType.DeliveryService:
                    return ArticleProductType.DeliveryService;
                case Proxy.Online.ArticleProductType.Statistical:
                    return ArticleProductType.Statistical;
                case Proxy.Online.ArticleProductType.InstallServiceMarging:
                    return ArticleProductType.InstallServiceMarging;
                default:
                    throw new ArgumentOutOfRangeException("articleProductType");
            }
        }

        public static Proxy.Online.ProductType ToOnlineProductType(this ProductType productType)
        {
            switch (productType)
            {
                case ProductType.Article:
                    return Proxy.Online.ProductType.Article;
                case ProductType.Set:
                    return Proxy.Online.ProductType.Set;
                case ProductType.WarrantyPlus:
                    return Proxy.Online.ProductType.WarrantyPlus;
                default:
                    throw new ArgumentOutOfRangeException("productType");
            }
        }

        public static Proxy.Online.ArticleProductType ToOnlineArticleProductType(this ArticleProductType articleProductType)
        {
            switch (articleProductType)
            {
                case ArticleProductType.Product:
                    return Proxy.Online.ArticleProductType.Product;
                case ArticleProductType.Set:
                    return Proxy.Online.ArticleProductType.Set;
                case ArticleProductType.WarrantyPlus:
                    return Proxy.Online.ArticleProductType.WarrantyPlus;
                case ArticleProductType.InstallationService:
                    return Proxy.Online.ArticleProductType.InstallationService;
                case ArticleProductType.DeliveryService:
                    return Proxy.Online.ArticleProductType.DeliveryService;
                case ArticleProductType.InstallServiceMarging:
                    return Proxy.Online.ArticleProductType.InstallServiceMarging;
                case ArticleProductType.Statistical:
                    return Proxy.Online.ArticleProductType.Statistical;
                default:
                    throw new ArgumentOutOfRangeException("articleProductType");
            }
        }

        public static PrintingStatus ToPrintingStatus(this Proxy.Online.PrintingStatus status)
        {
            switch (status)
            {
                case Proxy.Online.PrintingStatus.None:
                    return PrintingStatus.None;
                case Proxy.Online.PrintingStatus.Printed:
                    return PrintingStatus.Printed;
                case Proxy.Online.PrintingStatus.Error:
                    return PrintingStatus.Error;
                default:
                    throw new ArgumentOutOfRangeException("status");
            }
        }

        public static GeneralOrderStatus ToGeneralOrderState(this OrderState orderState)
        {
            switch (orderState)
            {
                case OrderState.Created:
                    return GeneralOrderStatus.Created;
                case OrderState.Confirmed:
                    return GeneralOrderStatus.Confirmed;
                case OrderState.Rejected:
                    return GeneralOrderStatus.Rejected;
                case OrderState.RejectedByCustomer:
                    return GeneralOrderStatus.RejectedByCustomer;
                case OrderState.ConfirmedWithGiftCertificate:
                    return GeneralOrderStatus.ConfirmedWithGiftCertificate;
                case OrderState.GiftCertificateNumberReservedNotifyCustomer:
                    return GeneralOrderStatus.GiftCertificateNumberReservedNotifyCustomer;
                case OrderState.OnlinePaymentReceived:
                    return GeneralOrderStatus.OnlinePaymentReceived;
                case OrderState.OnlinePaymentDeclined:
                    return GeneralOrderStatus.OnlinePaymentDeclined;
                case OrderState.Paid:
                    return GeneralOrderStatus.Paid;
                case OrderState.Closed:
                    return GeneralOrderStatus.Closed;
                case OrderState.Shipped:
                    return GeneralOrderStatus.Shipped;
                default:
                    throw new ArgumentOutOfRangeException("orderState");
            }
        }

        public static ClientData ToClientData(this Order order)
        {
            if (order.Customer == null)
            {
                return null;
            }

            return order.Customer.ToClientData();
        }

        public static ClientData ToClientData(this Customer customer)
        {
            return new ClientData
            {
                Email = customer.Email,
                Name = customer.Name,
                Phone = customer.Phone,
                Surname = customer.Surname,
                Phone2 = customer.Phone2,
                Birthday = customer.BirthDay,
                Gender = customer.Gender,
                Fax = customer.Fax,
                LastName = customer.LastName,
                ZZTCustomerId = customer.ZztCustomerId.ParseLong()
            };
        }

        public static DeliveryInfo ToDeliveryInfo(this Delivery delivery)
        {
            return new DeliveryInfo
            {
                Address = delivery.Address,
                City = delivery.City,
                HasDelivery = delivery.HasDelivery,
                DeliveryDate = delivery.HasDelivery ? delivery.DeliveryDate : null,
                PickupLocationId = delivery.PickupLocationId,
                Comment = delivery.DeliveryComment,
                ExactAddress = delivery.ExactAddress,
                Period = delivery.DeliveryPeriod,
                TransferAgreementWithCustomer = delivery.TransferAgreementWithCustomer,
                ZIPCode = delivery.ZIPCode,
                Region = delivery.Region,
                StreetAbbr = delivery.StreetAbbr,
                Street = delivery.Street,
                House = delivery.House,
                Housing = delivery.Housing,
                Building = delivery.Building,
                Apartment = delivery.Apartment,
                Entrance = delivery.Entrance,
                EntranceCode = delivery.EntranceCode,
                Floor = delivery.Floor,
                Kladr = delivery.Kladr,
                SubwayStationName = delivery.SubwayStationName,
                District = delivery.District,
                HasServiceLift = delivery.HasServiceLift,
                RequestedDeliveryTimeslot = delivery.RequestedDeliveryTimeslot,
                RequestedDeliveryServiceOption = delivery.RequestedDeliveryServiceOption
            };
        }

        public static Delivery ToDelivery(this DeliveryInfo deliveryInfo)
        {
            return new Delivery
            {
                Address = deliveryInfo.Address,
                City = deliveryInfo.City,
                HasDelivery = deliveryInfo.HasDelivery,
                ExactAddress = deliveryInfo.ExactAddress,
                DeliveryComment = deliveryInfo.Comment,
                DeliveryDate = deliveryInfo.DeliveryDate,
                DeliveryPeriod = deliveryInfo.Period,
                TransferAgreementWithCustomer =
                    deliveryInfo.TransferAgreementWithCustomer.HasValue &&
                    deliveryInfo.TransferAgreementWithCustomer.Value,
                ZIPCode = deliveryInfo.ZIPCode,
                Region = deliveryInfo.Region,
                StreetAbbr = deliveryInfo.StreetAbbr,
                Street = deliveryInfo.Street,
                House = deliveryInfo.House,
                Housing = deliveryInfo.Housing,
                Building = deliveryInfo.Building,
                Apartment = deliveryInfo.Apartment,
                Entrance = deliveryInfo.Entrance,
                EntranceCode = deliveryInfo.EntranceCode,
                Floor = deliveryInfo.Floor,
                Kladr = deliveryInfo.Kladr,
                SubwayStationName = deliveryInfo.SubwayStationName,
                District = deliveryInfo.District,
                HasServiceLift = deliveryInfo.HasServiceLift,
                RequestedDeliveryTimeslot = deliveryInfo.RequestedDeliveryTimeslot,
                PickupLocationId = deliveryInfo.PickupLocationId,
                RequestedDeliveryServiceOption = deliveryInfo.RequestedDeliveryServiceOption
            };
        }

        public static ArticleData ToArticleData(this OrderItem orderItem)
        {
            var initArticleData = new InitArticleData
            {
                ArticleNum = orderItem.Number,
                Price = orderItem.Price,
                Qty = orderItem.Quantity,
                ItemState = orderItem.State.ToItemState(),
                Promotions = orderItem.Promotions,
                HasShippedFromStock = orderItem.HasShippedFromStock,
                SerialNumber = orderItem.SerialNumber,
                StockNumber = orderItem.WWSStockNumber,
                Title = orderItem.Title,
                Benefit = orderItem.Benefit,
                CouponCode = orderItem.CouponCode,
                CouponCompainId = orderItem.CouponCompainId,
                OldPrice = orderItem.OldPrice,
                WarrantyInsuranceArticle =
                    orderItem.WarrantyInsurance != null
                        ? Convert.ToString(orderItem.WarrantyInsurance.WarrantyRelatedArticleNo)
                        : string.Empty,
                RefersToItem = orderItem.RefersToItem
            };
            var subArticleData = new Collection<InitArticleData>();
            foreach (var subOrderItem in orderItem.SubOrderItems)
            {
                var subInitArticleData = new InitArticleData
                {
                    ArticleNum = subOrderItem.Number.ToString(CultureInfo.InvariantCulture),
                    Price = subOrderItem.Price,
                    Qty = subOrderItem.Quantity,
                    ItemState = subOrderItem.State.ToItemState(),
                    Promotions = subOrderItem.Promotions,
                    HasShippedFromStock = subOrderItem.HasShippedFromStock,
                    SerialNumber = subOrderItem.SerialNumber,
                    StockNumber = subOrderItem.WWSStockNumber,
                    Benefit = orderItem.Benefit,
                    CouponCode = orderItem.CouponCode,
                    CouponCompainId = orderItem.CouponCompainId,
                    OldPrice = orderItem.OldPrice,
                    WarrantyInsuranceArticle =
                        Convert.ToString(subOrderItem.WarrantyInsurance.WarrantyRelatedArticleNo)
                };
                subArticleData.Add(subInitArticleData);
            }
            initArticleData.SubArticleData = subArticleData.ToArray();

            return initArticleData;
        }

        public static Proxy.Online.ItemState ToItemState(this ItemState itemState)
        {
            switch (itemState)
            {
                case ItemState.New:
                    return Proxy.Online.ItemState.New;
                case ItemState.DisplayItem:
                    return Proxy.Online.ItemState.DisplayItem;
                case ItemState.Repaired:
                    return Proxy.Online.ItemState.Repaired;
                default:
                    throw new ArgumentOutOfRangeException("itemState");
            }
        }

        public static ItemState ToItemState(this Proxy.Online.ItemState itemState)
        {
            switch (itemState)
            {
                case Proxy.Online.ItemState.New:
                    return ItemState.New;
                case Proxy.Online.ItemState.DisplayItem:
                    return ItemState.DisplayItem;
                case Proxy.Online.ItemState.Repaired:
                    return ItemState.Repaired;
                default:
                    throw new ArgumentOutOfRangeException("itemState");
            }
        }

        public static OrderState ToOrderState(this GeneralOrderStatus orderStatus)
        {
            switch (orderStatus)
            {
                case GeneralOrderStatus.Created:
                    return OrderState.Created;
                case GeneralOrderStatus.Confirmed:
                    return OrderState.Confirmed;
                case GeneralOrderStatus.Rejected:
                    return OrderState.Rejected;
                case GeneralOrderStatus.RejectedByCustomer:
                    return OrderState.RejectedByCustomer;
                case GeneralOrderStatus.GiftCertificateNumberReservedNotifyCustomer:
                    return OrderState.GiftCertificateNumberReservedNotifyCustomer;
                default:
                    throw new ArgumentOutOfRangeException("orderStatus");
            }
        }

        public static OrderLine ToOrderLine(this OrderItem orderItem)
        {
            return new OrderLine
            {
                ArticleNo = Int32.Parse(orderItem.Number),
                Price = orderItem.Price,
                Quantity = orderItem.Quantity,
                HasShippedFromStock = orderItem.HasShippedFromStock,
                SerialNumber = orderItem.SerialNumber,
                WarrantyInsurance = new Proxy.WWS.WarrantyInsurance
                {
                    RelatedArticleNo = orderItem.WarrantyInsurance.WarrantyRelatedArticleNo
                },
                StockNumber = orderItem.WWSStockNumber,
                SubLines = orderItem.SubOrderItems.Select(el => el.ToOrderLine()).ToArray(),
            };
        }

        public static StoreInfo ToStoreInfo(this Store store)
        {
            return new StoreInfo { McsId = store.McsId, SapCode = store.SapCode };
        }

        public static SalesDocumentData ToSalesDocumentData(this DocumentInfo salesDocument)
        {
            return new SalesDocumentData
            {
                OutletInfo = salesDocument.OutletInfo,
                ProductPickupInfo = salesDocument.ProductPickupInfo,
                PrintableInfo = salesDocument.PrintableInfo,
                SalesPersonId = salesDocument.SalesPersonId
            };
        }

        public static OrderNumberInfo ToOrderNumberInfo(this Order order)
        {
            var salesOrder = order.CurrentWwsOrder;
            if (salesOrder == null)
            {
                return new OrderNumberInfo
                {
                    OrderNumber = default(string),
                    SapCode = order.Store.SapCode
                };
            }
            return new OrderNumberInfo
            {
                OrderNumber = salesOrder.Id,
                SapCode = string.IsNullOrEmpty(salesOrder.SapCode)
                    ? order.Store.SapCode
                    : salesOrder.SapCode
            };
        }

        public static OrderNumberInfo ToOrderNumberInfo(this WwsOrder salesOrder)
        {
            return new OrderNumberInfo
            {
                OrderNumber = salesOrder.Id,
                SapCode = salesOrder.SapCode
            };
        }

        public static Notification.OrderItem ToNotificationOrderItem(this OrderItem orderItem)
        {
            var article = orderItem.Number.ParseLong().HasValue ? orderItem.Number.ParseLong().Value : 0;

            return new Notification.OrderItem
            {
                Article = article,
                Price = orderItem.Price,
                Qty = orderItem.Quantity,
                Title = orderItem.Title,
                TotalPrice = orderItem.Price * orderItem.Quantity
            };
        }

        private static OrderSource ToOrderSource(this SourceType source)
        {
            switch (source)
            {
                case SourceType.CallCenter:
                    return OrderSource.CallCenter;
                case SourceType.IPhone:
                    return OrderSource.IPhone;
                case SourceType.MobileWebSite:
                    return OrderSource.MobileWebSite;
                case SourceType.WebSite:
                    return OrderSource.WebSite;
                case SourceType.Windows8:
                    return OrderSource.Windows8;
                case SourceType.Store:
                    return OrderSource.Store;
                case SourceType.Avito:
                    return OrderSource.Avito;
                case SourceType.Metro:
                    return OrderSource.Metro;
                case SourceType.MetroCallCenter:
                    return OrderSource.MetroCallCenter;
                case SourceType.WebSiteQuickOrder:
                    return OrderSource.WebSiteQuickOrder;
                case SourceType.MobileWebSiteQuickOrder:
                    return OrderSource.MobileWebSiteQuickOrder;
                case SourceType.YandexMarket:
                    return OrderSource.YandexMarket;
                case SourceType.IPhoneQuickOrder:
                    return OrderSource.IPhoneQuickOrder;
                default:
                    throw new ArgumentOutOfRangeException("source");
            }
        }

        public static SourceType ToSourceType(this OrderSource source)
        {
            switch (source)
            {
                case OrderSource.CallCenter:
                    return SourceType.CallCenter;
                case OrderSource.IPhone:
                    return SourceType.IPhone;
                case OrderSource.MobileWebSite:
                    return SourceType.MobileWebSite;
                case OrderSource.WebSite:
                    return SourceType.WebSite;
                case OrderSource.Windows8:
                    return SourceType.Windows8;
                case OrderSource.Store:
                    return SourceType.Store;
                case OrderSource.Avito:
                    return SourceType.Avito;
                case OrderSource.Metro:
                    return SourceType.Metro;
                case OrderSource.MetroCallCenter:
                    return SourceType.MetroCallCenter;
                case OrderSource.WebSiteQuickOrder:
                    return SourceType.WebSiteQuickOrder;
                case OrderSource.MobileWebSiteQuickOrder:
                    return SourceType.MobileWebSiteQuickOrder;
                case OrderSource.YandexMarket:
                    return SourceType.YandexMarket;
                case OrderSource.IPhoneQuickOrder:
                    return SourceType.IPhoneQuickOrder;
                default:
                    throw new ArgumentOutOfRangeException("source");
            }
        }

        private static Proxy.Online.PaymentType ToPaymentType(this PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.Cash:
                    return Proxy.Online.PaymentType.Cash;
                case PaymentType.Online:
                    return Proxy.Online.PaymentType.Online;
                case PaymentType.OnlineCredit:
                    return Proxy.Online.PaymentType.OnlineCredit;
                case PaymentType.SocialCard:
                    return Proxy.Online.PaymentType.SocialCard;
                default:
                    throw new ArgumentOutOfRangeException("paymentType", paymentType.ToString());
            }
        }


        private static Proxy.WWS.PaymentType ToWwsPaymentType(this PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.Cash:
                    return Proxy.WWS.PaymentType.Cash;
                case PaymentType.SocialCard:
                    return Proxy.WWS.PaymentType.SocialCard;
                case PaymentType.Online:
                    return Proxy.WWS.PaymentType.Online;
                default:
                    throw new ArgumentOutOfRangeException("paymentType", paymentType.ToString());
            }
        }

        public static PaymentType ToPaymentType(this Proxy.Online.PaymentType paymentType)
        {
            switch (paymentType)
            {
                case Proxy.Online.PaymentType.Cash:
                    return PaymentType.Cash;
                case Proxy.Online.PaymentType.Online:
                    return PaymentType.Online;
                case Proxy.Online.PaymentType.OnlineCredit:
                    return PaymentType.OnlineCredit;
                case Proxy.Online.PaymentType.SocialCard:
                    return PaymentType.SocialCard;
                default:
                    throw new ArgumentOutOfRangeException("paymentType", paymentType.ToString());
            }
        }

        public static ReserveState ToReserveState(this ReserveStatus reserveStatus)
        {
            switch (reserveStatus)
            {
                case ReserveStatus.Prepared:
                    return ReserveState.Prepared;
                case ReserveStatus.IsEditing:
                    return ReserveState.IsEditing;
                case ReserveStatus.Created:
                    return ReserveState.Created;
                default:
                    throw new ArgumentOutOfRangeException("reserveStatus");
            }
        }

        public static OrderItemState ToOrderItemReviewState(this ReviewItemState itemState)
        {
            switch (itemState)
            {
                case ReviewItemState.Empty:
                    return OrderItemState.Empty;
                case ReviewItemState.Reserved:
                    return OrderItemState.Reserved;
                case ReviewItemState.ReservedPart:
                    return OrderItemState.ReservedPart;
                case ReviewItemState.ReservedDisplayItem:
                    return OrderItemState.ReservedDisplayItem;
                case ReviewItemState.NotFound:
                    return OrderItemState.NotFound;
                case ReviewItemState.IncorrectPrice:
                    return OrderItemState.IncorrectPrice;
                case ReviewItemState.Transfer:
                    return OrderItemState.Transfer;
                case ReviewItemState.DisplayItem:
                    return OrderItemState.DisplayItem;
                case ReviewItemState.TooLowPrice:
                    return OrderItemState.TooLowPrice;
                default:
                    throw new ArgumentOutOfRangeException("itemState");
            }
        }

        public static OrderItem ToOrderItem(this ReserveLine reserveItem)
        {
            var result = new OrderItem
            {
                Id = reserveItem.LineId,
                Number = reserveItem.ArticleData.ArticleNum,
                Price = reserveItem.ArticleData.Price,
                Quantity = reserveItem.ArticleData.Qty,
                ReviewState = reserveItem.ReviewItemState.ToOrderItemReviewState(),
                ArticleCondition = reserveItem.ArticleCondition,
                Comment = reserveItem.Comment,
                State = reserveItem.StockItemState.ToItemState(),
                Promotions = reserveItem.ArticleData.Promotions,
                RefersToItem = reserveItem.ArticleData.RefersToItem,
                WWSDepartmentNumber = reserveItem.WWSDepartmentNumber,
                WWSFreeQty = reserveItem.WWSFreeQty,
                WWSPriceOrig = reserveItem.WWSPriceOrig,
                WWSProductGroupName = reserveItem.WWSProductGroupName,
                WWSProductGroupNumber = reserveItem.WWSProductGroupNumber,
                WWSReservedQty = reserveItem.WWSReservedQty,
                WWSStockNumber = reserveItem.WWSStockNumber,
                WwsDescription = reserveItem.Title,
                WWSPositionNumber = reserveItem.WWSPositionNumber,
                WWSStoreNumber = reserveItem.WWSStoreNumber,
                WWSVat = reserveItem.WWSVAT,
                HasShippedFromStock = reserveItem.HasShippedFromStock,
                SerialNumber = reserveItem.SerialNumber,
                ProductType = reserveItem.ProductType.ToProductType(),
                ArticleProductType = reserveItem.ArticleProductType.ToArticleProductType(),
                TransferSapCode = reserveItem.TransferSapCode,
                TransferNumber = reserveItem.TransferNumber,
                Benefit = reserveItem.Benefit,
                CouponCode = reserveItem.CouponCode,
                CouponCompainId = reserveItem.CouponCompainId,
                OldPrice = reserveItem.OldPrice,
                Title = reserveItem.Title,
                WarrantyInsurance = new WarrantyInsurance
                {
                    WarrantyNumber = reserveItem.WarrantyInsurance.Number,
                    WarrantyRelatedArticleNo = reserveItem.WarrantyInsurance.RelatedArticleNo,
                    WarrantyDocumentPositionNumber = reserveItem.WarrantyInsurance.DocumentPositionNumber,
                    WarrantySum = reserveItem.WarrantyInsurance.WarrantySum,
                    WarrantyWwsCertificateState = reserveItem.WarrantyInsurance.WwsCertificateState,
                    WarrantyWwsExtensionPrint = reserveItem.WarrantyInsurance.WwsExtensionPrint
                }
            };

            if (reserveItem.SubLines != null)
            {
                foreach (var subReserveLine in reserveItem.SubLines)
                {
                    result.SubOrderItems.Add(subReserveLine.ToOrderItem());
                }
            }

            return result;
        }

        public static OrderItem ToOrderItem(this WwsOrderItem wwsOrderItem)
        {
            var result = new OrderItem
            {
                Number = wwsOrderItem.Article.ToString(CultureInfo.InvariantCulture),
                Price = wwsOrderItem.Price,
                Quantity = wwsOrderItem.Quantity,
                WWSDepartmentNumber = wwsOrderItem.DepartmentNumber,
                WWSFreeQty = wwsOrderItem.FreeQuantity,
                WWSProductGroupName = wwsOrderItem.ProductGroupName,
                WWSProductGroupNumber = wwsOrderItem.ProductGroup,
                WWSReservedQty = wwsOrderItem.ReservedQuantity,
                WWSStockNumber = wwsOrderItem.StockNumber,
                WwsDescription = wwsOrderItem.Description,
                WWSPriceOrig = wwsOrderItem.PriceOrig,
                WWSVat = wwsOrderItem.VAT,
                WWSPositionNumber = wwsOrderItem.PositionNumber,
                WWSStoreNumber = wwsOrderItem.StoreNumber,
                WarrantyInsurance = new WarrantyInsurance
                {
                    WarrantyNumber = wwsOrderItem.WarrantyInsurance.WarrantyNumber,
                    WarrantyDocumentPositionNumber =
                        wwsOrderItem.WarrantyInsurance.WarrantyDocumentPositionNumber,
                    WarrantyRelatedArticleNo =
                        wwsOrderItem.WarrantyInsurance.WarrantyRelatedArticleNo,
                    WarrantySum = wwsOrderItem.WarrantyInsurance.WarrantySum,
                    WarrantyWwsCertificateState =
                        wwsOrderItem.WarrantyInsurance.WarrantyWwsCertificateState,
                    WarrantyWwsExtensionPrint =
                        wwsOrderItem.WarrantyInsurance.WarrantyWwsExtensionPrint,
                }
            };

            return result;
        }

        public static WwsOrder ToWwsOrder(this CreateSalesOrderResult salesOrder)
        {
            return new WwsOrder
            {
                Id = salesOrder.OrderHeader.OrderNumber,
                Items =
                    new Collection<WwsOrderItem>(salesOrder.OrderHeader.Lines.Select(l => l.ToWwsOrderItem()).ToList()),
                CreationDate = salesOrder.CreateTime.HasValue ? salesOrder.CreateTime.Value : default(DateTime),
                LastUpdateTime = salesOrder.OrderHeader.LastUpdateTime,
                OriginalSum = salesOrder.OrderHeader.OriginalSum,
                StoreManagerName = salesOrder.OrderHeader.SalesPersonName,
                DocBarcode = salesOrder.OrderHeader.DocBarcode,
                DocBarcodeImage = salesOrder.OrderHeader.DocBarcodeImage,
                TotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.OrderHeader.TotalPrice.GrossPrice,
                    NETPrice = salesOrder.OrderHeader.TotalPrice.NetPrice,
                    VAT = (int)salesOrder.OrderHeader.TotalPrice.VAT,
                    VATPrice = salesOrder.OrderHeader.TotalPrice.VatPrice
                },
                LeftoverTotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.OrderHeader.LeftoverPrice.GrossPrice,
                    NETPrice = salesOrder.OrderHeader.LeftoverPrice.NetPrice,
                    VAT = (int)salesOrder.OrderHeader.LeftoverPrice.VAT,
                    VATPrice = salesOrder.OrderHeader.LeftoverPrice.VatPrice
                },
                PrepaymentTotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.OrderHeader.DownpaymentPriceFull.GrossPrice,
                    NETPrice = salesOrder.OrderHeader.DownpaymentPriceFull.NetPrice,
                    VAT = (int)salesOrder.OrderHeader.DownpaymentPriceFull.VAT,
                    VATPrice = salesOrder.OrderHeader.DownpaymentPriceFull.VatPrice,
                }
            };
        }

        public static WwsOrder ToWwsOrder(this GetSalesOrderResult salesOrder)
        {
            return new WwsOrder
            {
                Id = salesOrder.OrderHeader.OrderNumber,
                Items =
                    new Collection<WwsOrderItem>(salesOrder.OrderHeader.Lines.Select(l => l.ToWwsOrderItem()).ToList()),
                //CreationDate = salesOrder.CreateTime.HasValue ? salesOrder.CreateTime.Value : default(DateTime),
                LastUpdateTime = salesOrder.OrderHeader.LastUpdateTime,
                OriginalSum = salesOrder.OrderHeader.OriginalSum,
                StoreManagerName = salesOrder.OrderHeader.SalesPersonName,
                DocBarcode = salesOrder.OrderHeader.DocBarcode,
                DocBarcodeImage = salesOrder.OrderHeader.DocBarcodeImage,
                TotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.OrderHeader.TotalPrice.GrossPrice,
                    NETPrice = salesOrder.OrderHeader.TotalPrice.NetPrice,
                    VAT = (int)salesOrder.OrderHeader.TotalPrice.VAT,
                    VATPrice = salesOrder.OrderHeader.TotalPrice.VatPrice
                },
                LeftoverTotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.OrderHeader.LeftoverPrice.GrossPrice,
                    NETPrice = salesOrder.OrderHeader.LeftoverPrice.NetPrice,
                    VAT = (int)salesOrder.OrderHeader.LeftoverPrice.VAT,
                    VATPrice = salesOrder.OrderHeader.LeftoverPrice.VatPrice
                },
                PrepaymentTotalPrice = new WwsOrderTotalPrice
                {
                    GrossPrice = salesOrder.OrderHeader.DownpaymentPriceFull.GrossPrice,
                    NETPrice = salesOrder.OrderHeader.DownpaymentPriceFull.NetPrice,
                    VAT = (int)salesOrder.OrderHeader.DownpaymentPriceFull.VAT,
                    VATPrice = salesOrder.OrderHeader.DownpaymentPriceFull.VatPrice,
                }
            };
        }

        public static CustomerType GetCustomerType(this Order result)
        {
            if (result.Customer == null)
            {
                return CustomerType.Unknown;
            }
            if (string.IsNullOrEmpty(result.Customer.Email))
            {
                return CustomerType.Unknown;
            }
            const string domainRegexp = "[^@]+@(?<domain>[^@]{2,})";
            var match = Regex.Match(result.Customer.Email, domainRegexp);
            if (match.Success)
            {
                var domain = match.Groups["domain"].Value.Trim().ToLower();
                if (StaffDomains.Contains(domain))
                {
                    return CustomerType.Staff;
                }
            }
            return CustomerType.Unknown;
        }
    }
}