namespace Orders.Backend.WF.Activities.Model
{
    public enum ReserveState
    {       
        Prepared = 0,
        IsEditing = 1,
        Created = 2
    }
}