﻿using System;
using System.Globalization;
using System.Linq;
using Orders.Backend.WF.Activities.Common;
using Orders.Backend.WF.Activities.Proxy.Basket;

namespace Orders.Backend.WF.Activities.Model
{
    public static class BasketExtensions
    {
        public static ValidateRequest ToValidateRequest(this Order order)
        {
            if(order.ZztData == null)
            {
                throw new ArgumentException("Order.ZztData must not be null!");
            }

            return new ValidateRequest
            {
                Consumer = (order.ZztData != null ? order.ZztData.ConsumerId : string.Empty),
                Articles = order.Items.Select(el => el.ToArticleData(order.ZztData.CampaignId)).ToArray()
            };
        }

        public static ArticleBaseData ToArticleData(this OrderItem item, string campaignId)
        {
            return new ArticleBaseData
            {
                CampaignId = campaignId,
                //AvailableQty = item.Quantity,
                Id = item.Id.ToString(CultureInfo.InvariantCulture),
                Price = item.Price,
                Qty = item.Quantity
            };
        }

        public static ValidationResult ToValidationResult(this ValidateResponse response)
        {
            return new ValidationResult //TODO Узнать про все поля
            {
                IsValid = (response.Result == ResponseResult.OK),
                ErrorMessage = response.ErrorMessage
            };
        }
    }
}