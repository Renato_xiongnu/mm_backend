﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Activities.Model
{
    public class OrderContext
    {
        public Order Order { get; set; }

        public bool? ProductAvaliableInStore { get; set; }

        public string ApproveOutcome { get; set; }

        public bool OrderAvailableForDelivery
        {
            get
            {
                return Order != null && (Order.Delivery.ShippingMethod == ShippingMethod.PickupShop || Order.Delivery.ShippingMethod == ShippingMethod.PickupExternal) && Order.Delivery.DeliveryDate.HasValue;
            }
        }
    }
}
