﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace Orders.Backend.WF.Activities.Model
{
    public class OrderItem
    {
        public OrderItem()
        {
            SubOrderItems = new Collection<OrderItem>();
            WarrantyInsurance = new WarrantyInsurance();
        }

        protected bool Equals(OrderItem other)
        {
            return string.Equals(Number, other.Number) && Price == other.Price &&
                   Quantity == other.Quantity && ReviewState == other.ReviewState;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            return obj.GetType() == GetType() && Equals((OrderItem) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Number != null ? Number.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Price.GetHashCode();
                hashCode = (hashCode*397) ^ Quantity;
                hashCode = (hashCode*397) ^ (int) ReviewState;
                return hashCode;
            }
        }

        public void MergeItem(WwsOrderItem wwsOrderItem)
        {
            ProductType = wwsOrderItem.ProductType;
            WWSDepartmentNumber = wwsOrderItem.DepartmentNumber;
            WWSFreeQty = wwsOrderItem.FreeQuantity;
            WWSProductGroupName = wwsOrderItem.ProductGroupName;
            WWSProductGroupNumber = wwsOrderItem.ProductGroup;
            WWSReservedQty = wwsOrderItem.ReservedQuantity;
            WWSStockNumber = WWSStockNumber == 0 ? wwsOrderItem.StockNumber : WWSStockNumber; //TODO Проверить
            WwsDescription = wwsOrderItem.Description;
            WWSPriceOrig = wwsOrderItem.PriceOrig;
            WWSVat = wwsOrderItem.VAT;
            WWSPositionNumber = wwsOrderItem.PositionNumber;
            WWSStoreNumber = wwsOrderItem.StoreNumber;
            if (wwsOrderItem.ProductType == ProductType.WarrantyPlus)
            {
                ReviewState = OrderItemState.Reserved;
            }
        }

        public void MergeSubItem(WwsOrderItem subWwsOrderItem)
        {
            Number = subWwsOrderItem.Article.ToString(CultureInfo.InvariantCulture);
            Price = subWwsOrderItem.Price;
            Quantity = subWwsOrderItem.Quantity;
            MergeItem(subWwsOrderItem);
        }

        public long Id { get; set; }

        public string InitLineId { get; set; }

        public string Number { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public OrderItemState ReviewState { get; set; }

        public int WWSDepartmentNumber { get; set; }

        public string WWSProductGroupName { get; set; }

        public int WWSProductGroupNumber { get; set; }

        public int WWSStockNumber { get; set; }

        public int WWSFreeQty { get; set; }

        public int WWSReservedQty { get; set; }

        public string WwsDescription { get; set; }

        public string Comment { get; set; }

        public string ArticleCondition { get; set; }

        public decimal WWSPriceOrig { get; set; }

        public string[] Promotions { get; set; }

        public ItemState State { get; set; }

        public int WWSVat { get; set; }

        public int WWSPositionNumber { get; set; }

        public int WWSStoreNumber { get; set; }

        public bool HasShippedFromStock { get; set; }

        public ICollection<OrderItem> SubOrderItems { get; set; }

        public string SerialNumber { get; set; }

        public ProductType ProductType { get; set; }

        public ArticleProductType ArticleProductType { get; set; }

        public string Title { get; set; }

        public WarrantyInsurance WarrantyInsurance { get; set; }

        public string TransferSapCode { get; set; }

        public int TransferNumber { get; set; }

        public decimal? OldPrice { get; set; }

        public decimal? Benefit { get; set; }

        public string CouponCompainId { get; set; }

        public string CouponCode { get; set; }

        public long? RefersToItem { get; set; }
    }
}