﻿namespace Orders.Backend.WF.Activities.Model
{
    public enum ProductType
    {
        Article = 0,
        Set = 1,
        WarrantyPlus = 2,
    }
}
