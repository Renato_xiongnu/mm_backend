﻿namespace Orders.Backend.WF.Activities.Model
{
    public class WwsOrderTotalPrice
    {
        public int VAT { get; set; }
        public decimal NETPrice { get; set; }
        public decimal VATPrice { get; set; }
        public decimal GrossPrice { get; set; }
    }
}
