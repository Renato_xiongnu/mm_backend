﻿using System.Web;
using System.Web.Optimization;

namespace Orders.Backend.Rules.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.1.4.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/react/react-with-addons.js",
                "~/Scripts/react/ReactRouter.js",
                "~/Scripts/rx.lite.js",
                "~/Scripts/mdl/material.js",
                "~/Scripts/react-material.js",
                "~/Scripts/ts/RulesStore.js",
                "~/Scripts/ts/RulesActions.js",
                "~/Scripts/ts/RulesComponents.js",
                "~/Scripts/ts/PolicyComponents.js",
                "~/Scripts/ts/Rules.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(                
                //"~/Content/bootstrap.css",
                "~/Scripts/mdl/material.css",
                "~/Content/Site.css"
            ));

        }
    }
}
