﻿using Orders.Backend.Rules.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.WF.Activities.Model;

namespace Orders.Backend.Rules.Web.Extensions
{
    public static class Tests
    {
        public static Order TestOrder()
        {
            var order = new Order
            {
                Id = "006-534-508",
                DocumentPrintingStatus = (PrintingStatus)int.MaxValue,
                Comment = "old comment",
                Prepay = 12.0M
            };
            return order;
        }

        public static PolicyVersion TestPolicy()
        {
            var policy = new PolicyVersion
            {
                Id = 1,
                Created = DateTime.UtcNow,
                PolicyRules = new List<PolicyRule>
                {
                    new PolicyRule
                    {
                        PolicyVersionId = 1,
                        Created = DateTime.UtcNow,
                        Order = 1,
                        Condition = "Order.Prepay <= 0",
                        Value = "Value 1"
                    },
                    new PolicyRule
                    {
                        PolicyVersionId = 1,
                        Created = DateTime.UtcNow,
                        Order = 2,
                        Condition = "Order.Prepay > 0",
                        Value = "Value 2"
                    }
                }
            };
            return policy;
        }
    }
}