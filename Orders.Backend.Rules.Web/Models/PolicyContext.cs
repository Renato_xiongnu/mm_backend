namespace Orders.Backend.Rules.Web.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PolicyContext : DbContext
    {
        public PolicyContext()
            : base("name=PolicyContext1")
        {
        }

        public virtual DbSet<Policy> Policies { get; set; }
        public virtual DbSet<PolicyLog> PolicyLogs { get; set; }
        public virtual DbSet<PolicyModel> PolicyModels { get; set; }
        public virtual DbSet<PolicyRule> PolicyRules { get; set; }
        public virtual DbSet<PolicyVersion> PolicyVersions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Policy>()
                .HasMany(e => e.PolicyVersions)
                .WithRequired(e => e.Policy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PolicyModel>()
                .HasMany(e => e.Policies)
                .WithRequired(e => e.PolicyModel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PolicyVersion>()
                .HasMany(e => e.PolicyRules)
                .WithRequired(e => e.PolicyVersion)
                .WillCascadeOnDelete(true);
        }
    }
}
