namespace Orders.Backend.Rules.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PolicyLog")]
    public partial class PolicyLog
    {
        public int Id { get; set; }

        public int PolicyVersionId { get; set; }

        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Created { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        [StringLength(50)]
        public string Value { get; set; }

        [Required]
        public TimeSpan Duration { get; set; }

    }
}
