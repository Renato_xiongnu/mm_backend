﻿/// <reference path="../typings/_references.d.ts" />
"use strict";

module Rules {
    export interface IPolicyModel {
        Id: number;
        Created: Date;
        Name: string;
        SystemName: string;
        TypeName: string;
        Policies?: IPolicy[];
        EditMode?: boolean;
    };

    export interface IPolicy {
        Id: number;
        PolicyModelId: number;
        Created: Date;
        Name: string;
        PolicyVersions?: IPolicyVersion[];
        EditMode?: boolean;
    };

    export interface IPolicyVersion {
        Id?: number;
        PolicyId?: number;
        Created?: Date;
        //Status: string;
        PolicyRules?: IPolicyRule[];
    };

    export interface IPolicyRule {
        PolicyVersionId?: number;
        Order?: number;
        Created?: Date;
        Condition?: string;
        Value?: string;
        ErrorMessage?: string;
    };

    export interface IRulesStore {
        Models: IPolicyModel[];
        Model: string;
        Policies: IPolicy[];
        Policy: IPolicy;
        PolicyVersion: IPolicyVersion;
    };

    var data: IRulesStore = {
        Models: [],
        Model: null,
        Policies: [],
        Policy: null,
        PolicyVersion: null,
    };

    export var RulesStore = new Rx.BehaviorSubject<IRulesStore>(data);

    export var RulesStoreActions = {
        assign: new Rx.Subject()
    };

    RulesStoreActions.assign
        .map(a => {
            var value = RulesStore.getValue();
            var newValue: IRulesStore = Object.assign({}, value, a);
            return newValue;
        })
        .subscribe(RulesStore);
}