﻿/// <reference path="../typings/_references.d.ts" />
"use strict";

module Rules {

    function getModels() {
        return $.ajax({
            url: "./api/model",
            dataType: 'json'
        });
    }

    function updateModel(model: IPolicyModel) {
        return $.ajax({
            url: "./api/model",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(model)
        });
    }

    function deleteModel(id: number) {
        return $.ajax({
            url: `./api/model/${id}`,
            method: "DELETE",
            dataType: "json"
        });
    }

    function getPolicies(model: string) {
        return $.ajax({
            url: `./api/model/${model}/policy`,
            dataType: 'json'
        });
    }

    function getPolicy(model: string, policy: string) {
        return $.ajax({
            url: `./api/model/${model}/policy/${policy}`,
            dataType: 'json'
        });
    }

    function updatePolicy(model: string, policy: IPolicy) {
        return $.ajax({
            url: `./api/model/${model}/policy`,
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(policy)
        });
    }

    function deletePolicy(model: string, id: number) {
        return $.ajax({
            url: `./api/model/${model}/policy/${id}`,
            method: "DELETE",
            dataType: "json",
            contentType: "application/json"
        });
    }

    function getPolicyVersion(model: string, policy: string, id: number) {
        return $.ajax({
            url: `./api/model/${model}/policy/${policy}/version/${id}`,
            dataType: 'json'
        });
    }

    function putPolicyVersion(model: string, policy: string, pv: IPolicyVersion) {
        return $.ajax({
            url: `./api/model/${model}/policy/${policy}/version`,
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(pv)
        });
    }

    function deletePolicyVersion(model: string, policy: string, id: number) {
        return $.ajax({
            url: `./api/model/${model}/policy/${policy}/version/${id}`,
            method: "DELETE",
            dataType: "json",
            contentType: "application/json"
        });
    }

    function validateCondition(model: string, condition: string) {
        return $.ajax({
            url: `./api/validate/${model}`,
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(condition)
        });
    }

    function cloneVersion(pv: IPolicyVersion) {
        var rules = pv.PolicyRules.map(r => Object.assign({}, r));
        return Object.assign({}, pv, { PolicyRules: rules });
    }

    export var ModelActions = {
        getModels: new Rx.Subject<any>(),
        createModel: new Rx.Subject<any>(),
        updateModel: new Rx.Subject<IPolicyModel>(),
        deleteModel: new Rx.Subject<number>(),
        cancelEditModel: new Rx.Subject<IPolicyModel>(),
        exportModel: new Rx.Subject<IPolicyModel>(),
        importModel: new Rx.Subject<any>(),
    }

    export var RulesActions = {
        getPolicies: new Rx.Subject<string>(),        
        
        selectPolicy: new Rx.Subject<IPolicy>(),
        createPolicy: new Rx.Subject<any>(),
        updatePolicy: new Rx.Subject<IPolicy>(),
        deletePolicy: new Rx.Subject<number>(),
        cancelEditPolicy: new Rx.Subject<IPolicy>(),

        selectPolicyVersion: new Rx.Subject<number>(),
        getPolicyVersion: new Rx.Subject<string>(),
        savePolicyVersion: new Rx.Subject<any>(),
        deletePolicyVersion: new Rx.Subject<any>(),
        updatePolicyVersion: new Rx.Subject<IPolicyVersion>(),

        validateRule: new Rx.Subject<IPolicyRule>(),
    }

    ModelActions.getModels
        .flatMapLatest((id) => {
            var value = RulesStore.getValue();
            return Rx.Observable.fromPromise(getModels()).catch(e => Rx.Observable.empty());
        })
        .map(models => {
            return { Models: models };
        })
        .subscribe(RulesStoreActions.assign);

    ModelActions.createModel
        .map(() => {
            var value = RulesStore.getValue();
            value.Models.push({ Id: 0, Created: new Date(), Name: null, SystemName: null, TypeName: null, EditMode: true })
            return value;
        })
        .subscribe(RulesStore);

    ModelActions.updateModel
        .flatMapLatest((model) => {
            return Rx.Observable.fromPromise(updateModel(model)).catch(e => Rx.Observable.empty());
        })
        .map((model) => {
            var value = RulesStore.getValue();
            var models = value.Models.filter(m => m.Id > 0);
            models.push(model);
            return { Models: models };
        })
        .subscribe(RulesStoreActions.assign);

    ModelActions.deleteModel
        .flatMapLatest((id) => {
            return Rx.Observable.fromPromise(deleteModel(id)).catch(e => Rx.Observable.empty());
        })
        .map((model) => {
            var value = RulesStore.getValue();
            var models = value.Models.filter(m => m.Id != model.Id);
            return { Models: models };
        })
        .subscribe(RulesStoreActions.assign);

    ModelActions.cancelEditModel
        .map((model) => {
            var value = RulesStore.getValue();
            var models = value.Models.filter(m => m.Id > 0);
            models.forEach(m => m.EditMode = false);
            return { Models: models };
        })
        .subscribe(RulesStoreActions.assign);

    var modelFlow = RulesActions.getPolicies;

    var policiesFlow = modelFlow.flatMapLatest((name) => {
        return Rx.Observable.fromPromise(getPolicies(name)).catch(e => Rx.Observable.empty());
    });

    Rx.Observable.zip(modelFlow, policiesFlow, (name, ps) => {
        return { Model: name, Policies: ps };
    }).subscribe(RulesStoreActions.assign);


    ModelActions.exportModel
        .subscribe((model) => {
            var url = 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(model, null, '  '));
            var a = <any>document.createElement("a");
            a.href = url;
            a.download = model.Name + ".json";
            setTimeout(() => { a.click(); });
        });

    ModelActions.importModel
        .subscribe((model) => {
            var input = <HTMLInputElement>document.createElement("input");
            input.type = "file";
            input.onchange = () => {
                var file = input.files[0];
                if (file) {
                    var reader = new FileReader();
                    reader.onload = (e) => {
                        //console.log((<any>e.target).result)
                        var m = JSON.parse((<any>e.target).result);
                        ModelActions.updateModel.onNext(m);
                    };
                    reader.readAsText(file);
                }
            };
            setTimeout(() => { input.click(); });
        });

    RulesActions.selectPolicy
        .map((p) => {
            var value = RulesStore.getValue();
            var pv = p.PolicyVersions[p.PolicyVersions.length - 1];
            pv = cloneVersion(pv);
            return { Policy: p, PolicyVersion: pv };
        })
        .subscribe(RulesStoreActions.assign);;


    RulesActions.updatePolicy
        .flatMapLatest((policy) => {
            var value = RulesStore.getValue();
            return Rx.Observable.fromPromise(updatePolicy(value.Model, policy)).catch(e => Rx.Observable.empty());
        })
        .map((policy: IPolicy) => {
            var value = RulesStore.getValue();
            var policies = value.Policies.filter(m => m.Id > 0);
            policies.push(policy);
            return { Policies: policies };
        })
        .subscribe(RulesStoreActions.assign);

    RulesActions.deletePolicy
        .flatMapLatest((id) => {
            var value = RulesStore.getValue();
            return Rx.Observable.fromPromise(deletePolicy(value.Model, id)).catch(e => Rx.Observable.empty());
        })
        .map((model) => {
            var value = RulesStore.getValue();
            var ps = value.Policies.filter(p => p.Id != model.Id);
            return { Policies: ps };
        })
        .subscribe(RulesStoreActions.assign);

    RulesActions.cancelEditPolicy
        .map((policy: IPolicy) => {
            var value = RulesStore.getValue();
            var ps = value.Policies.filter(p => p.Id > 0);
            ps.forEach(p => p.EditMode = false);
            return { Policies: ps };
        })
        .subscribe(RulesStoreActions.assign);

    RulesActions.selectPolicyVersion
        .map((id) => {
            var value = RulesStore.getValue();
            var pv = value.Policy.PolicyVersions.filter(p => p.Id === id)[0];
            pv = cloneVersion(pv);
            return { PolicyVersion: pv }
        })
        .subscribe(RulesStoreActions.assign);;


    RulesActions.updatePolicyVersion
        .map(pv => {
            var value = RulesStore.getValue();
            pv = Object.assign({}, value.PolicyVersion, pv);
            console.log("PolicyVersion:", value.PolicyVersion);
            return { PolicyVersion: pv };
        })
        .subscribe(RulesStoreActions.assign);

    RulesActions.getPolicyVersion
        .flatMapLatest((policyName) => {
            var value = RulesStore.getValue();
            return Rx.Observable.fromPromise(getPolicy(value.Model, policyName)).catch(e => Rx.Observable.empty());
        })
        .subscribe(RulesActions.updatePolicyVersion);

    RulesActions.createPolicy
        .map(() => {
            var value = RulesStore.getValue();
            var model = value.Models.filter(m => m.Name === value.Model)[0];
            value.Policies.push({ Id: 0, PolicyModelId: model.Id, Created: new Date(), Name: null, EditMode: true })
            return value;
        })
        .subscribe(RulesStore);

    RulesActions.savePolicyVersion
        .flatMapLatest(() => {
            var value = RulesStore.getValue();
            console.log("putPolicyVersion:", value.PolicyVersion);
            return Rx.Observable.fromPromise(putPolicyVersion(value.Model, value.Policy.Name, value.PolicyVersion)).catch(e => Rx.Observable.empty());
        })
        .map(pv => {
            var value = RulesStore.getValue();           
            //pv = Object.assign({}, value.PolicyVersion, pv);
            value.Policy.PolicyVersions.push(pv);
            console.log("PolicyVersion:", pv);
            return { PolicyVersion: pv };
        })
        .subscribe(RulesStoreActions.assign);


    RulesActions.deletePolicyVersion
        .flatMapLatest(() => {
            var value = RulesStore.getValue();
            console.log("deletePolicyVersion:", value.PolicyVersion);
            return Rx.Observable.fromPromise(deletePolicyVersion(value.Model, value.Policy.Name, value.PolicyVersion.Id)).catch(e => Rx.Observable.empty());
        })
        .map(pv => {
            var value = RulesStore.getValue();
            var versions = value.Policy.PolicyVersions.filter(v => v.Id !== pv.Id);
            value.Policy.PolicyVersions = versions;
            return { PolicyVersion: null };
        })
        .subscribe(RulesStoreActions.assign);

    var validateResults = RulesActions.validateRule
        .flatMapLatest(rule => {
            var value = RulesStore.getValue();
            return Rx.Observable.fromPromise(validateCondition(value.Model, rule.Condition))
                .map(r => { return { ErrorMessage: null } })
                .catch(e => Rx.Observable.just({ ErrorMessage: e.responseJSON.Message }));                
        });

    Rx.Observable.zip(RulesActions.validateRule, validateResults, (rule, result) => {
            Object.assign(rule, result);
            return true;
        })
        .subscribe(RulesStoreActions.assign);

}