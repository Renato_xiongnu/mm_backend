/// <reference path="../typings/_references.d.ts" />
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Rules;
(function (Rules_1) {
    var d = React.DOM;
    var $ = jQuery;
    var RulesTable = (function (_super) {
        __extends(RulesTable, _super);
        function RulesTable() {
            _super.apply(this, arguments);
        }
        RulesTable.prototype.updateVersion = function (index, value) {
            var rules = this.props.PolicyRules;
            var rule = rules[index];
            rules[index] = Object.assign({}, rule, value);
            Rules_1.RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        };
        RulesTable.prototype.deleteVersion = function (index, value) {
            var rules = this.props.PolicyRules;
            rules.splice(index, 1);
            Rules_1.RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        };
        RulesTable.prototype.render = function () {
            var _this = this;
            var Table = React.createFactory(MaterialDataTable);
            var Column = React.createFactory(MaterialTableColumn);
            var TextField = React.createFactory(MaterialTextField);
            var Button = React.createFactory(MaterialButton);
            var conditionRender = function (item, column, rowIndex) {
                return d.div({}, d.textarea({ style: { width: "100%" }, rows: 3, value: item.Condition, onChange: function (e) { return _this.updateVersion(rowIndex, { Condition: e.target.value }); }, onBlur: function (e) { return Rules_1.RulesActions.validateRule.onNext(item); } }), d.div({ style: { color: "red" } }, item.ErrorMessage));
                //return TextField({ value: item.Condition, onChange: (e) => this.updateVersion(rowIndex, { Condition: e.target.value }), pattern: "", errorMessage: item.ErrorMessage, onBlur: e => RulesActions.validateRule.onNext(item) }, "Condition");
            };
            var valueRender = function (item, column, rowIndex) {
                return TextField({ value: item.Value, onChange: function (e) { return _this.updateVersion(rowIndex, { Value: e.target.value }); } }, "Value");
            };
            var actionsRender = function (item, column, rowIndex) {
                return d.div({}, Button({ colored: true, onClick: function (e) { return _this.deleteVersion(rowIndex, item); } }, "Delete"));
            };
            return (Table({ data: this.props.PolicyRules, keySelector: function (item, index) { return index; }, style: { width: "100%" } }, Column({ style: { width: "5%" }, dataField: "Order" }, "Order"), 
            //Column({ dataField: "Created", nonNumeric: true }, "Created"),
            Column({ style: { width: "55%" }, dataField: "Condition", nonNumeric: true, valueRender: conditionRender }, "Condition"), Column({ style: { width: "20%" }, dataField: "Value", nonNumeric: true, valueRender: valueRender }, "Value"), Column({ style: { width: "20%" }, valueRender: actionsRender }, "Actions")));
        };
        return RulesTable;
    })(React.Component);
    Rules_1.RulesTable = RulesTable;
    var RulesEditor = (function (_super) {
        __extends(RulesEditor, _super);
        function RulesEditor() {
            _super.apply(this, arguments);
        }
        RulesEditor.prototype.addRule = function () {
            var pv = this.props.PolicyVersion;
            var rules = pv.PolicyRules;
            rules.push({ Order: rules.length + 1 });
            Rules_1.RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        };
        RulesEditor.prototype.addPolicyVersion = function () {
            var pv = this.props.PolicyVersion;
            var rules = pv.PolicyRules;
            rules.push({ Order: rules.length + 1 });
            Rules_1.RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        };
        RulesEditor.prototype.render = function () {
            var _this = this;
            //var Policies = React.createFactory(PolicyTable);
            var Rules = React.createFactory(RulesTable);
            var Button = React.createFactory(MaterialButton);
            var model = this.props.Model;
            var policy = this.props.Policy;
            var pv = this.props.PolicyVersion;
            return (pv ? d.div({ style: {} }, d.h4({}, policy.Name + " - Version " + pv.Id + " - Rules", Button({ accent: true, onClick: function (e) { return _this.addRule(); } }, "Add Rule"), Button({ accent: true, onClick: function (e) { return Rules_1.RulesActions.savePolicyVersion.onNext({}); } }, "Save New Version"), Button({ accent: true, onClick: function (e) { return Rules_1.RulesActions.deletePolicyVersion.onNext({}); } }, "Delete Version")), Rules({ PolicyRules: pv.PolicyRules })) : null);
        };
        return RulesEditor;
    })(React.Component);
    Rules_1.RulesEditor = RulesEditor;
    ;
})(Rules || (Rules = {}));
//# sourceMappingURL=RulesComponents.js.map