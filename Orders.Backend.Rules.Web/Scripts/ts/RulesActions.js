/// <reference path="../typings/_references.d.ts" />
"use strict";
var Rules;
(function (Rules) {
    function getModels() {
        return $.ajax({
            url: "./api/model",
            dataType: 'json'
        });
    }
    function updateModel(model) {
        return $.ajax({
            url: "./api/model",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(model)
        });
    }
    function deleteModel(id) {
        return $.ajax({
            url: "./api/model/" + id,
            method: "DELETE",
            dataType: "json"
        });
    }
    function getPolicies(model) {
        return $.ajax({
            url: "./api/model/" + model + "/policy",
            dataType: 'json'
        });
    }
    function getPolicy(model, policy) {
        return $.ajax({
            url: "./api/model/" + model + "/policy/" + policy,
            dataType: 'json'
        });
    }
    function updatePolicy(model, policy) {
        return $.ajax({
            url: "./api/model/" + model + "/policy",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(policy)
        });
    }
    function deletePolicy(model, id) {
        return $.ajax({
            url: "./api/model/" + model + "/policy/" + id,
            method: "DELETE",
            dataType: "json",
            contentType: "application/json"
        });
    }
    function getPolicyVersion(model, policy, id) {
        return $.ajax({
            url: "./api/model/" + model + "/policy/" + policy + "/version/" + id,
            dataType: 'json'
        });
    }
    function putPolicyVersion(model, policy, pv) {
        return $.ajax({
            url: "./api/model/" + model + "/policy/" + policy + "/version",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(pv)
        });
    }
    function deletePolicyVersion(model, policy, id) {
        return $.ajax({
            url: "./api/model/" + model + "/policy/" + policy + "/version/" + id,
            method: "DELETE",
            dataType: "json",
            contentType: "application/json"
        });
    }
    function validateCondition(model, condition) {
        return $.ajax({
            url: "./api/validate/" + model,
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(condition)
        });
    }
    function cloneVersion(pv) {
        var rules = pv.PolicyRules.map(function (r) { return Object.assign({}, r); });
        return Object.assign({}, pv, { PolicyRules: rules });
    }
    Rules.ModelActions = {
        getModels: new Rx.Subject(),
        createModel: new Rx.Subject(),
        updateModel: new Rx.Subject(),
        deleteModel: new Rx.Subject(),
        cancelEditModel: new Rx.Subject(),
        exportModel: new Rx.Subject(),
        importModel: new Rx.Subject(),
    };
    Rules.RulesActions = {
        getPolicies: new Rx.Subject(),
        selectPolicy: new Rx.Subject(),
        createPolicy: new Rx.Subject(),
        updatePolicy: new Rx.Subject(),
        deletePolicy: new Rx.Subject(),
        cancelEditPolicy: new Rx.Subject(),
        selectPolicyVersion: new Rx.Subject(),
        getPolicyVersion: new Rx.Subject(),
        savePolicyVersion: new Rx.Subject(),
        deletePolicyVersion: new Rx.Subject(),
        updatePolicyVersion: new Rx.Subject(),
        validateRule: new Rx.Subject(),
    };
    Rules.ModelActions.getModels
        .flatMapLatest(function (id) {
        var value = Rules.RulesStore.getValue();
        return Rx.Observable.fromPromise(getModels()).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (models) {
        return { Models: models };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.ModelActions.createModel
        .map(function () {
        var value = Rules.RulesStore.getValue();
        value.Models.push({ Id: 0, Created: new Date(), Name: null, SystemName: null, TypeName: null, EditMode: true });
        return value;
    })
        .subscribe(Rules.RulesStore);
    Rules.ModelActions.updateModel
        .flatMapLatest(function (model) {
        return Rx.Observable.fromPromise(updateModel(model)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (model) {
        var value = Rules.RulesStore.getValue();
        var models = value.Models.filter(function (m) { return m.Id > 0; });
        models.push(model);
        return { Models: models };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.ModelActions.deleteModel
        .flatMapLatest(function (id) {
        return Rx.Observable.fromPromise(deleteModel(id)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (model) {
        var value = Rules.RulesStore.getValue();
        var models = value.Models.filter(function (m) { return m.Id != model.Id; });
        return { Models: models };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.ModelActions.cancelEditModel
        .map(function (model) {
        var value = Rules.RulesStore.getValue();
        var models = value.Models.filter(function (m) { return m.Id > 0; });
        models.forEach(function (m) { return m.EditMode = false; });
        return { Models: models };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    var modelFlow = Rules.RulesActions.getPolicies;
    var policiesFlow = modelFlow.flatMapLatest(function (name) {
        return Rx.Observable.fromPromise(getPolicies(name)).catch(function (e) { return Rx.Observable.empty(); });
    });
    Rx.Observable.zip(modelFlow, policiesFlow, function (name, ps) {
        return { Model: name, Policies: ps };
    }).subscribe(Rules.RulesStoreActions.assign);
    Rules.ModelActions.exportModel
        .subscribe(function (model) {
        var url = 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(model, null, '  '));
        var a = document.createElement("a");
        a.href = url;
        a.download = model.Name + ".json";
        setTimeout(function () { a.click(); });
    });
    Rules.ModelActions.importModel
        .subscribe(function (model) {
        var input = document.createElement("input");
        input.type = "file";
        input.onchange = function () {
            var file = input.files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //console.log((<any>e.target).result)
                    var m = JSON.parse(e.target.result);
                    Rules.ModelActions.updateModel.onNext(m);
                };
                reader.readAsText(file);
            }
        };
        setTimeout(function () { input.click(); });
    });
    Rules.RulesActions.selectPolicy
        .map(function (p) {
        var value = Rules.RulesStore.getValue();
        var pv = p.PolicyVersions[p.PolicyVersions.length - 1];
        pv = cloneVersion(pv);
        return { Policy: p, PolicyVersion: pv };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    ;
    Rules.RulesActions.updatePolicy
        .flatMapLatest(function (policy) {
        var value = Rules.RulesStore.getValue();
        return Rx.Observable.fromPromise(updatePolicy(value.Model, policy)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (policy) {
        var value = Rules.RulesStore.getValue();
        var policies = value.Policies.filter(function (m) { return m.Id > 0; });
        policies.push(policy);
        return { Policies: policies };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.RulesActions.deletePolicy
        .flatMapLatest(function (id) {
        var value = Rules.RulesStore.getValue();
        return Rx.Observable.fromPromise(deletePolicy(value.Model, id)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (model) {
        var value = Rules.RulesStore.getValue();
        var ps = value.Policies.filter(function (p) { return p.Id != model.Id; });
        return { Policies: ps };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.RulesActions.cancelEditPolicy
        .map(function (policy) {
        var value = Rules.RulesStore.getValue();
        var ps = value.Policies.filter(function (p) { return p.Id > 0; });
        ps.forEach(function (p) { return p.EditMode = false; });
        return { Policies: ps };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.RulesActions.selectPolicyVersion
        .map(function (id) {
        var value = Rules.RulesStore.getValue();
        var pv = value.Policy.PolicyVersions.filter(function (p) { return p.Id === id; })[0];
        pv = cloneVersion(pv);
        return { PolicyVersion: pv };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    ;
    Rules.RulesActions.updatePolicyVersion
        .map(function (pv) {
        var value = Rules.RulesStore.getValue();
        pv = Object.assign({}, value.PolicyVersion, pv);
        console.log("PolicyVersion:", value.PolicyVersion);
        return { PolicyVersion: pv };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.RulesActions.getPolicyVersion
        .flatMapLatest(function (policyName) {
        var value = Rules.RulesStore.getValue();
        return Rx.Observable.fromPromise(getPolicy(value.Model, policyName)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .subscribe(Rules.RulesActions.updatePolicyVersion);
    Rules.RulesActions.createPolicy
        .map(function () {
        var value = Rules.RulesStore.getValue();
        var model = value.Models.filter(function (m) { return m.Name === value.Model; })[0];
        value.Policies.push({ Id: 0, PolicyModelId: model.Id, Created: new Date(), Name: null, EditMode: true });
        return value;
    })
        .subscribe(Rules.RulesStore);
    Rules.RulesActions.savePolicyVersion
        .flatMapLatest(function () {
        var value = Rules.RulesStore.getValue();
        console.log("putPolicyVersion:", value.PolicyVersion);
        return Rx.Observable.fromPromise(putPolicyVersion(value.Model, value.Policy.Name, value.PolicyVersion)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (pv) {
        var value = Rules.RulesStore.getValue();
        //pv = Object.assign({}, value.PolicyVersion, pv);
        value.Policy.PolicyVersions.push(pv);
        console.log("PolicyVersion:", pv);
        return { PolicyVersion: pv };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    Rules.RulesActions.deletePolicyVersion
        .flatMapLatest(function () {
        var value = Rules.RulesStore.getValue();
        console.log("deletePolicyVersion:", value.PolicyVersion);
        return Rx.Observable.fromPromise(deletePolicyVersion(value.Model, value.Policy.Name, value.PolicyVersion.Id)).catch(function (e) { return Rx.Observable.empty(); });
    })
        .map(function (pv) {
        var value = Rules.RulesStore.getValue();
        var versions = value.Policy.PolicyVersions.filter(function (v) { return v.Id !== pv.Id; });
        value.Policy.PolicyVersions = versions;
        return { PolicyVersion: null };
    })
        .subscribe(Rules.RulesStoreActions.assign);
    var validateResults = Rules.RulesActions.validateRule
        .flatMapLatest(function (rule) {
        var value = Rules.RulesStore.getValue();
        return Rx.Observable.fromPromise(validateCondition(value.Model, rule.Condition))
            .map(function (r) { return { ErrorMessage: null }; })
            .catch(function (e) { return Rx.Observable.just({ ErrorMessage: e.responseJSON.Message }); });
    });
    Rx.Observable.zip(Rules.RulesActions.validateRule, validateResults, function (rule, result) {
        Object.assign(rule, result);
        return true;
    })
        .subscribe(Rules.RulesStoreActions.assign);
})(Rules || (Rules = {}));
//# sourceMappingURL=RulesActions.js.map