﻿/// <reference path="../typings/_references.d.ts" />
"use strict";

module Rules {
    var d = React.DOM;
    var $ = jQuery;

    interface IRulesTable {
        PolicyRules: IPolicyRule[];
    }

    export class RulesTable extends React.Component<IRulesTable, any> {
        updateVersion(index, value) {
            var rules = this.props.PolicyRules;
            var rule = rules[index];
            rules[index] = Object.assign({}, rule, value);
            RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        }

        deleteVersion(index, value) {
            var rules = this.props.PolicyRules;
            rules.splice(index, 1);
            RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        }

        render() {
            var Table = React.createFactory(MaterialDataTable);
            var Column = React.createFactory(MaterialTableColumn);
            var TextField = React.createFactory(MaterialTextField);
            var Button = React.createFactory(MaterialButton);

            var conditionRender = (item: IPolicyRule, column, rowIndex) => {
                return d.div({},
                    d.textarea({ style: { width: "100%" }, rows: 3, value: item.Condition, onChange: (e) => this.updateVersion(rowIndex, { Condition: (<any>e.target).value }), onBlur: e => RulesActions.validateRule.onNext(item) }),
                    d.div({ style: { color: "red" } }, item.ErrorMessage)
                );
                //return TextField({ value: item.Condition, onChange: (e) => this.updateVersion(rowIndex, { Condition: e.target.value }), pattern: "", errorMessage: item.ErrorMessage, onBlur: e => RulesActions.validateRule.onNext(item) }, "Condition");
            };
            var valueRender = (item: IPolicyRule, column, rowIndex) => {
                return TextField({ value: item.Value, onChange: (e) => this.updateVersion(rowIndex, { Value: e.target.value }) }, "Value");
            };
            var actionsRender = (item, column, rowIndex) => {
                return d.div({},
                    Button({ colored: true, onClick: (e) => this.deleteVersion(rowIndex, item) }, "Delete")
                );
            }

            return (
                Table({ data: this.props.PolicyRules, keySelector: (item, index) => index, style: { width: "100%" } },
                    Column({ style: { width: "5%" }, dataField: "Order" }, "Order"),
                    //Column({ dataField: "Created", nonNumeric: true }, "Created"),
                    Column({ style: { width: "55%" }, dataField: "Condition", nonNumeric: true, valueRender: conditionRender }, "Condition"),
                    Column({ style: { width: "20%" }, dataField: "Value", nonNumeric: true, valueRender: valueRender }, "Value"),
                    Column({ style: { width: "20%" }, valueRender: actionsRender }, "Actions")
                )
            );
        }
    }

    export class RulesEditor extends React.Component<IRulesStore, any> {
        addRule() {
            var pv = this.props.PolicyVersion;
            var rules = pv.PolicyRules;
            rules.push({ Order: rules.length + 1 });
            RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        }

        addPolicyVersion() {
            var pv = this.props.PolicyVersion;
            var rules = pv.PolicyRules;
            rules.push({ Order: rules.length + 1 });
            RulesActions.updatePolicyVersion.onNext({ PolicyRules: rules });
        }

        render() {
            //var Policies = React.createFactory(PolicyTable);
            var Rules = React.createFactory(RulesTable);
            var Button = React.createFactory(MaterialButton);

            var model = this.props.Model;
            var policy = this.props.Policy;
            var pv = this.props.PolicyVersion;
            return (
                pv ? d.div({ style: {} },
                    d.h4({}, policy.Name + " - Version " + pv.Id + " - Rules",
                        Button({ accent: true, onClick: (e) => this.addRule() }, "Add Rule"),
                        Button({ accent: true, onClick: (e) => RulesActions.savePolicyVersion.onNext({}) }, "Save New Version"),
                        Button({ accent: true, onClick: (e) => RulesActions.deletePolicyVersion.onNext({}) }, "Delete Version")
                    ),
                    Rules({ PolicyRules: pv.PolicyRules })
            ) : null
            );
        }
    };
}