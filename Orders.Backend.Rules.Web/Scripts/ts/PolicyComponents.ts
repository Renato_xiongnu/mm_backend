﻿/// <reference path="../typings/_references.d.ts" />
"use strict";

module Rules {
    var d = React.DOM;
    var $ = jQuery;

    var Table = React.createFactory(MaterialDataTable);
    var Column = React.createFactory(MaterialTableColumn);
    var CheckBox = React.createFactory(MaterialCheckBox);
    var TextField = React.createFactory(MaterialTextField);
    var Button = React.createFactory(MaterialButton);    

    export interface IPolicyProps {
        store: IRulesStore;
    }

    export interface IModelsProps {
        store: IRulesStore;
    }

    export interface IPolicyVersions {
        policy: IPolicy;
        version: IPolicyVersion;
    }

    export class PolicyVersions extends React.Component<IPolicyVersions, any> {
        render() {
            var item = this.props.policy;
            var version = this.props.version;
            var options = item.PolicyVersions                
                .map(pv => {
                    return d.option({ value: pv.Id.toString() }, pv.Id);
                })
                .reverse();
            return d.select({ value: version && version.Id ? version.Id.toString() : null, onChange: (e) => RulesActions.selectPolicyVersion.onNext(parseInt((<HTMLInputElement>e.target).value)) }, options);
        }
    }

    export class PolicyTable extends React.Component<IPolicyProps, any> {
        render() {
            var Versions = React.createFactory(PolicyVersions);

            var store = this.props.store;
            var rowSelector = (item, index) => {
                return store.Policy != null && item.Id === store.Policy.Id;
            };

            //var checkbox = function (item: IPolicy, header, rowIndex) {
            //    return CheckBox({ ripple: true, id: "selected" + rowIndex, checked: store.Policy != null && item.Id === store.Policy.Id, onChange: (e) => { RulesStoreActions.assign.onNext(e.target.checked ? { Policy: item, PolicyVersion: item.PolicyVersions[item.PolicyVersions.length - 1] || {} } : { Policy: null, PolicyVersion: null }) }, isUpgraded: true });
            //};

            var actionsRender = (item: IPolicy, header, rowIndex: number) => {
                return item.EditMode
                    ? d.div({},
                        Button({ colored: true, onClick: (e) => RulesActions.updatePolicy.onNext(item) }, "Save"),
                        Button({ colored: true, onClick: (e) => RulesActions.cancelEditPolicy.onNext(item) }, "Cancel")
                    )
                    : d.div({},
                        Button({ colored: true, onClick: (e) => RulesActions.selectPolicy.onNext(item) }, "View"),
                        Button({ colored: true, onClick: (e) => RulesActions.deletePolicy.onNext(item.Id) }, "Delete")
                    )
            };

            var versionRender = (item: IPolicy, header, rowIndex) => {
                return store.Policy && store.Policy.Id === item.Id
                    ? Versions({ policy: item, version: store.PolicyVersion })
                    : item.PolicyVersions && item.PolicyVersions.length > 0 ? item.PolicyVersions[item.PolicyVersions.length - 1].Id : "нет";
            };

            var inputRender = (item: IPolicyModel, header, rowIndex: number) => {
                return item.EditMode
                    ? TextField({ value: item[header.props.dataField], onChange: (e) => { item[header.props.dataField] = e.target.value } }, item[header.props.dataField])
                    : item[header.props.dataField];
            };

            return (
                Table({ data: store.Policies, keySelector: (item, index) => item.Id, rowSelector: rowSelector, style: { width: "100%" } },
                    //Column({ dataField: "selected", valueRender: checkbox }, ""),                    
                    Column({ dataField: "Name", nonNumeric: true, valueRender: inputRender }, "Name"),
                    Column({ dataField: "Created" }, "Created"),
                    Column({ valueRender: versionRender }, "Version"),
                    Column({ dataField: "Id", valueRender: actionsRender }, "Actions")
                )
            );
        }
    }

    export class ModelTable extends React.Component<IModelsProps, any> {
        render() {
            var store = this.props.store;
            var rowSelector = (item: IPolicyModel, index: number) => {
                return store.Model != null && item.Name === store.Model;
            };

            var actionsRender = (item: IPolicyModel, header, rowIndex: number) => {
                return item.EditMode
                    ? d.div({},
                        Button({ colored: true, onClick: (e) => ModelActions.updateModel.onNext(item) }, "Save"),
                        Button({ colored: true, onClick: (e) => ModelActions.cancelEditModel.onNext(item) }, "Cancel")
                    )
                    : d.div({},
                        Button({ colored: true, onClick: (e) => RulesActions.getPolicies.onNext(item.Name) }, "View"),
                        Button({ colored: true, onClick: (e) => ModelActions.exportModel.onNext(item) }, "Export"),
                        Button({ colored: true, onClick: (e) => ModelActions.deleteModel.onNext(item.Id) }, "Delete")
                    )
            };

            var inputRender = (item: IPolicyModel, header, rowIndex: number) => {
                return item.EditMode
                    ? TextField({ value: item[header.props.dataField], onChange: (e) => { item[header.props.dataField] = e.target.value } }, item[header.props.dataField])
                    : item[header.props.dataField];
            };

            return (
                Table({ data: store.Models, keySelector: (item, index) => item.Id, rowSelector: rowSelector, style: { width: "100%" }  },
                    Column({ dataField: "Name", nonNumeric: true, valueRender: inputRender }, "Name"),
                    Column({ dataField: "TypeName", nonNumeric: true, valueRender: inputRender }, "TypeName"),
                    Column({ dataField: "Created" }, "Created"),
                    Column({ valueRender: (item: IPolicyModel) => item.Policies ? item.Policies.length : 0 }, "Policy Count"),
                    Column({ dataField: "Id", valueRender: actionsRender }, "Actions")
                )
            );
        }
    }

    export class PolicyView extends React.Component<IPolicyProps, any> {
        render() {
            var Table = React.createFactory(PolicyTable);

            return d.div({},
                Table(this.props)
            );
        }
    }
}
