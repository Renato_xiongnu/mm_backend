/// <reference path="../typings/_references.d.ts" />
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Rules;
(function (Rules_1) {
    var d = React.DOM;
    var $ = jQuery;
    var Models = React.createFactory(Rules_1.ModelTable);
    var Rules = React.createFactory(Rules_1.RulesEditor);
    var Policies = React.createFactory(Rules_1.PolicyTable);
    //var Route = React.createFactory(ReactRouter.Route);
    //var DefaultRoute = React.createFactory(ReactRouter.DefaultRoute);
    //var NotFoundRoute = React.createFactory(ReactRouter.NotFoundRoute);
    //var RouteHandler = React.createFactory(ReactRouter.RouteHandler);
    //var routes = (
    //    Route({ path: "/", handler: App },
    //        DefaultRoute({ handler: RulesEditor }),
    //        Route({ name: "rules", path: "/rules", handler: RulesEditor })
    //    )
    //);
    //export class App extends React.Component<any, any> {
    //    render() {
    //        return RouteHandler();
    //    }
    //}
    //ReactRouter.run(routes, ReactRouter.HistoryLocation, (handler) => {
    //    React.render(React.createElement(handler, {}), document.getElementById('app'));
    //});
    var RulesApp = (function (_super) {
        __extends(RulesApp, _super);
        function RulesApp() {
            _super.apply(this, arguments);
        }
        RulesApp.prototype.render = function () {
            var Button = React.createFactory(MaterialButton);
            return d.div({}, d.h4({}, "Models", Button({ accent: true, onClick: function (e) { return Rules_1.ModelActions.createModel.onNext({}); } }, "Add"), Button({ accent: true, onClick: function (e) { return Rules_1.ModelActions.importModel.onNext({}); } }, "Import")), Models({ store: this.props }), this.props.Model != null
                ? d.div({}, d.h4({}, this.props.Model + " - Policies", Button({ accent: true, onClick: function (e) { return Rules_1.RulesActions.createPolicy.onNext({}); } }, "Add")), Policies({ store: this.props }), Rules(this.props))
                : null);
        };
        return RulesApp;
    })(React.Component);
    Rules_1.RulesApp = RulesApp;
    var App = React.createFactory(RulesApp);
    Rules_1.RulesStore.subscribe(function (value) {
        console.log(value);
        React.render(App(value), document.getElementById('app'));
    }, function (error) {
        console.log(error);
    });
    Rules_1.ModelActions.getModels.onNext({});
})(Rules || (Rules = {}));
//# sourceMappingURL=Rules.js.map