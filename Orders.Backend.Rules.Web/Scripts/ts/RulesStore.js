/// <reference path="../typings/_references.d.ts" />
"use strict";
var Rules;
(function (Rules) {
    ;
    ;
    ;
    ;
    ;
    var data = {
        Models: [],
        Model: null,
        Policies: [],
        Policy: null,
        PolicyVersion: null,
    };
    Rules.RulesStore = new Rx.BehaviorSubject(data);
    Rules.RulesStoreActions = {
        assign: new Rx.Subject()
    };
    Rules.RulesStoreActions.assign
        .map(function (a) {
        var value = Rules.RulesStore.getValue();
        var newValue = Object.assign({}, value, a);
        return newValue;
    })
        .subscribe(Rules.RulesStore);
})(Rules || (Rules = {}));
//# sourceMappingURL=RulesStore.js.map