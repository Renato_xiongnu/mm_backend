/// <reference path="../typings/_references.d.ts" />
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Rules;
(function (Rules) {
    var d = React.DOM;
    var $ = jQuery;
    var Table = React.createFactory(MaterialDataTable);
    var Column = React.createFactory(MaterialTableColumn);
    var CheckBox = React.createFactory(MaterialCheckBox);
    var TextField = React.createFactory(MaterialTextField);
    var Button = React.createFactory(MaterialButton);
    var PolicyVersions = (function (_super) {
        __extends(PolicyVersions, _super);
        function PolicyVersions() {
            _super.apply(this, arguments);
        }
        PolicyVersions.prototype.render = function () {
            var item = this.props.policy;
            var version = this.props.version;
            var options = item.PolicyVersions
                .map(function (pv) {
                return d.option({ value: pv.Id.toString() }, pv.Id);
            })
                .reverse();
            return d.select({ value: version && version.Id ? version.Id.toString() : null, onChange: function (e) { return Rules.RulesActions.selectPolicyVersion.onNext(parseInt(e.target.value)); } }, options);
        };
        return PolicyVersions;
    })(React.Component);
    Rules.PolicyVersions = PolicyVersions;
    var PolicyTable = (function (_super) {
        __extends(PolicyTable, _super);
        function PolicyTable() {
            _super.apply(this, arguments);
        }
        PolicyTable.prototype.render = function () {
            var Versions = React.createFactory(PolicyVersions);
            var store = this.props.store;
            var rowSelector = function (item, index) {
                return store.Policy != null && item.Id === store.Policy.Id;
            };
            //var checkbox = function (item: IPolicy, header, rowIndex) {
            //    return CheckBox({ ripple: true, id: "selected" + rowIndex, checked: store.Policy != null && item.Id === store.Policy.Id, onChange: (e) => { RulesStoreActions.assign.onNext(e.target.checked ? { Policy: item, PolicyVersion: item.PolicyVersions[item.PolicyVersions.length - 1] || {} } : { Policy: null, PolicyVersion: null }) }, isUpgraded: true });
            //};
            var actionsRender = function (item, header, rowIndex) {
                return item.EditMode
                    ? d.div({}, Button({ colored: true, onClick: function (e) { return Rules.RulesActions.updatePolicy.onNext(item); } }, "Save"), Button({ colored: true, onClick: function (e) { return Rules.RulesActions.cancelEditPolicy.onNext(item); } }, "Cancel"))
                    : d.div({}, Button({ colored: true, onClick: function (e) { return Rules.RulesActions.selectPolicy.onNext(item); } }, "View"), Button({ colored: true, onClick: function (e) { return Rules.RulesActions.deletePolicy.onNext(item.Id); } }, "Delete"));
            };
            var versionRender = function (item, header, rowIndex) {
                return store.Policy && store.Policy.Id === item.Id
                    ? Versions({ policy: item, version: store.PolicyVersion })
                    : item.PolicyVersions && item.PolicyVersions.length > 0 ? item.PolicyVersions[item.PolicyVersions.length - 1].Id : "нет";
            };
            var inputRender = function (item, header, rowIndex) {
                return item.EditMode
                    ? TextField({ value: item[header.props.dataField], onChange: function (e) { item[header.props.dataField] = e.target.value; } }, item[header.props.dataField])
                    : item[header.props.dataField];
            };
            return (Table({ data: store.Policies, keySelector: function (item, index) { return item.Id; }, rowSelector: rowSelector, style: { width: "100%" } }, 
            //Column({ dataField: "selected", valueRender: checkbox }, ""),                    
            Column({ dataField: "Name", nonNumeric: true, valueRender: inputRender }, "Name"), Column({ dataField: "Created" }, "Created"), Column({ valueRender: versionRender }, "Version"), Column({ dataField: "Id", valueRender: actionsRender }, "Actions")));
        };
        return PolicyTable;
    })(React.Component);
    Rules.PolicyTable = PolicyTable;
    var ModelTable = (function (_super) {
        __extends(ModelTable, _super);
        function ModelTable() {
            _super.apply(this, arguments);
        }
        ModelTable.prototype.render = function () {
            var store = this.props.store;
            var rowSelector = function (item, index) {
                return store.Model != null && item.Name === store.Model;
            };
            var actionsRender = function (item, header, rowIndex) {
                return item.EditMode
                    ? d.div({}, Button({ colored: true, onClick: function (e) { return Rules.ModelActions.updateModel.onNext(item); } }, "Save"), Button({ colored: true, onClick: function (e) { return Rules.ModelActions.cancelEditModel.onNext(item); } }, "Cancel"))
                    : d.div({}, Button({ colored: true, onClick: function (e) { return Rules.RulesActions.getPolicies.onNext(item.Name); } }, "View"), Button({ colored: true, onClick: function (e) { return Rules.ModelActions.exportModel.onNext(item); } }, "Export"), Button({ colored: true, onClick: function (e) { return Rules.ModelActions.deleteModel.onNext(item.Id); } }, "Delete"));
            };
            var inputRender = function (item, header, rowIndex) {
                return item.EditMode
                    ? TextField({ value: item[header.props.dataField], onChange: function (e) { item[header.props.dataField] = e.target.value; } }, item[header.props.dataField])
                    : item[header.props.dataField];
            };
            return (Table({ data: store.Models, keySelector: function (item, index) { return item.Id; }, rowSelector: rowSelector, style: { width: "100%" } }, Column({ dataField: "Name", nonNumeric: true, valueRender: inputRender }, "Name"), Column({ dataField: "TypeName", nonNumeric: true, valueRender: inputRender }, "TypeName"), Column({ dataField: "Created" }, "Created"), Column({ valueRender: function (item) { return item.Policies ? item.Policies.length : 0; } }, "Policy Count"), Column({ dataField: "Id", valueRender: actionsRender }, "Actions")));
        };
        return ModelTable;
    })(React.Component);
    Rules.ModelTable = ModelTable;
    var PolicyView = (function (_super) {
        __extends(PolicyView, _super);
        function PolicyView() {
            _super.apply(this, arguments);
        }
        PolicyView.prototype.render = function () {
            var Table = React.createFactory(PolicyTable);
            return d.div({}, Table(this.props));
        };
        return PolicyView;
    })(React.Component);
    Rules.PolicyView = PolicyView;
})(Rules || (Rules = {}));
//# sourceMappingURL=PolicyComponents.js.map