﻿/// <reference path="../typings/_references.d.ts" />
"use strict";

module Rules {
    var d = React.DOM;
    var $ = jQuery;

    var Models = React.createFactory(ModelTable);
    var Rules = React.createFactory(RulesEditor);
    var Policies = React.createFactory(PolicyTable);
    

    //var Route = React.createFactory(ReactRouter.Route);
    //var DefaultRoute = React.createFactory(ReactRouter.DefaultRoute);
    //var NotFoundRoute = React.createFactory(ReactRouter.NotFoundRoute);
    //var RouteHandler = React.createFactory(ReactRouter.RouteHandler);

    //var routes = (
    //    Route({ path: "/", handler: App },
    //        DefaultRoute({ handler: RulesEditor }),
    //        Route({ name: "rules", path: "/rules", handler: RulesEditor })
    //    )
    //);

    //export class App extends React.Component<any, any> {
    //    render() {
    //        return RouteHandler();
    //    }
    //}

    //ReactRouter.run(routes, ReactRouter.HistoryLocation, (handler) => {
    //    React.render(React.createElement(handler, {}), document.getElementById('app'));
    //});

    export class RulesApp extends React.Component<IRulesStore, any> {
        render() {
            var Button = React.createFactory(MaterialButton);

            return d.div({},
                d.h4({}, "Models",
                    Button({ accent: true, onClick: (e) => ModelActions.createModel.onNext({}) }, "Add"),
                    Button({ accent: true, onClick: (e) => ModelActions.importModel.onNext({}) }, "Import")
                ),
                Models({ store: this.props }),
                this.props.Model != null
                    ? d.div({},
                        d.h4({}, this.props.Model + " - Policies",
                            Button({ accent: true, onClick: (e) => RulesActions.createPolicy.onNext({}) }, "Add")
                        ),
                        Policies({ store: this.props }),
                        Rules(this.props)
                        )
                    : null              
            );
        }
    }
    var App = React.createFactory(RulesApp);

    RulesStore.subscribe(
        value => {
            console.log(value);
            React.render(App(value), document.getElementById('app'));
        },
        error => {
            console.log(error);
        }
    );

    ModelActions.getModels.onNext({});
}