﻿/// <reference path="react.d.ts" />

declare class MaterialButton<P, S> extends React.Component<P, S> {
}

declare class MaterialCard<P, S> extends React.Component<P, S> {
}

declare class MaterialCardTitle<P, S> extends React.Component<P, S> {
}

declare class MaterialCardTitleText<P, S> extends React.Component<P, S> {
}

declare class MaterialCardMedia<P, S> extends React.Component<P, S> {
}

declare class MaterialCardSupportingText<P, S> extends React.Component<P, S> {
}

declare class MaterialCardActions<P, S> extends React.Component<P, S> {
}

declare class MaterialCheckBox<P, S> extends React.Component<P, S> {
}

declare class MaterialTable<P, S> extends React.Component<P, S> {
}

declare class MaterialDataTable<P, S> extends React.Component<P, S> {
}

declare class MaterialTableColumn<P, S> extends React.Component<P, S> {
}

declare class MaterialSpinner<P, S> extends React.Component<P, S> {
}

declare class MaterialTextField<P, S> extends React.Component<P, S> {
}