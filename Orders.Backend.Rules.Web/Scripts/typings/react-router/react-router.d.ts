/// <reference path="../react/react-addons-global.d.ts" />
// Type definitions for React Router 0.13.3
// Project: https://github.com/rackt/react-router
// Definitions by: Yuichi Murata <https://github.com/mrk21>, Václav Ostrožlík <https://github.com/vasek17>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

declare module ReactRouter {
    //
    // Transition
    // ----------------------------------------------------------------------
    export interface Transition {
        path: string;
        abortReason: any;
        retry(): void;
        abort(reason?: any): void;
        redirect(to: string, params?: {}, query?: {}): void;
        cancel(): void;
        from: (transition: Transition, routes: Route[], components?: React.ReactElement<any>[], callback?: (error?: any) => void) => void;
        to: (transition: Transition, routes: Route[], params?: {}, query?: {}, callback?: (error?: any) => void) => void;
    }

    export interface TransitionStaticLifecycle {
        willTransitionTo?(
            transition: Transition,
            params: {},
            query: {},
            callback: Function
        ): void;

        willTransitionFrom?(
            transition: Transition,
            component: React.ReactElement<any>,
            callback: Function
        ): void;
    }

    //
    // Route Configuration
    // ----------------------------------------------------------------------
    // DefaultRoute
    export interface DefaultRouteProp {
        name?: string;
        handler: React.ComponentClass<any>;
    }
    export interface DefaultRoute extends React.ReactElement<DefaultRouteProp> {}
    export interface DefaultRouteClass extends React.ComponentClass<DefaultRouteProp> {}

    // NotFoundRoute
    export interface NotFoundRouteProp {
        name?: string;
        handler: React.ComponentClass<any>;
    }
    export interface NotFoundRoute extends React.ReactElement<NotFoundRouteProp> {}
    export interface NotFoundRouteClass extends React.ComponentClass<NotFoundRouteProp> {}

    // Redirect
    export interface RedirectProp {
        path?: string;
        from?: string;
        to?: string;
    }
    export interface Redirect extends React.ReactElement<RedirectProp> {}
    export interface RedirectClass extends React.ComponentClass<RedirectProp> {}

    // Route
    export interface RouteProp {
        name?: string;
        path?: string;
        handler?: React.ComponentClass<any>;
        ignoreScrollBehavior?: boolean;
    }
    export interface Route extends React.ReactElement<RouteProp> {}
    export interface RouteClass extends React.ComponentClass<RouteProp> {}

    export var DefaultRoute: DefaultRouteClass;
    export var NotFoundRoute: NotFoundRouteClass;
    export var Redirect: RedirectClass;
    export var Route: RouteClass;

    export interface CreateRouteOptions {
        name?: string;
        path?: string;
        ignoreScrollBehavior?: boolean;
        isDefault?: boolean;
        isNotFound?: boolean;
        onEnter?: (transition: Transition, params: {}, query: {}, callback: Function) => void;
        onLeave?: (transition: Transition, wtf: any, callback: Function) => void;
        handler?: Function;
        parentRoute?: Route;
    }

    export type CreateRouteCallback = (route: Route) => void;

    export function createRoute(callback: CreateRouteCallback): Route;
    export function createRoute(options: CreateRouteOptions | string, callback: CreateRouteCallback): Route;
    export function createDefaultRoute(options?: CreateRouteOptions | string): Route;
    export function createNotFoundRoute(options?: CreateRouteOptions | string): Route;

    export interface CreateRedirectOptions extends CreateRouteOptions {
        path?: string;
        from?: string;
        to: string;
        params?: {};
        query?: {};
    }
    export function createRedirect(options: CreateRedirectOptions): Redirect;
    export function createRoutesFromReactChildren(children: Route): Route[];

    //
    // Components
    // ----------------------------------------------------------------------
    // Link
    export interface LinkProp {
        activeClassName?: string;
        activeStyle?: {};
        to: string;
        params?: {};
        query?: {};
        onClick?: Function;
    }
    export interface Link extends React.ReactElement<LinkProp>, Navigation, State {
        handleClick(event: any): void;
        getHref(): string;
        getClassName(): string;
        getActiveState(): boolean;
    }
    export interface LinkClass extends React.ComponentClass<LinkProp> {}

    // RouteHandler
    export interface RouteHandlerProp { }
    export interface RouteHandlerChildContext {
        routeDepth: number;
    }
    export interface RouteHandler extends React.ReactElement<RouteHandlerProp> {
        getChildContext(): RouteHandlerChildContext;
        getRouteDepth(): number;
        createChildRouteHandler(props: {}): RouteHandler;
    }
    export interface RouteHandlerClass extends React.ComponentClass<RouteHandlerProp> {}

    export var Link: LinkClass;
    export var RouteHandler: RouteHandlerClass;


    //
    // Top-Level
    // ----------------------------------------------------------------------
    export interface Router extends React.ReactElement<any> {
        run(callback: RouterRunCallback): void;
    }

    export interface RouterState {
        path: string;
        action: string;
        pathname: string;
        params: {};
        query: {};
        routes: Route[];
    }

    export interface RouterCreateOption {
        routes: Route;
        location?: LocationBase;
        scrollBehavior?: ScrollBehaviorBase;
        onError?: (error: any) => void;
        onAbort?: (error: any) => void;
    }

    export type RouterRunCallback = (Handler: RouteClass, state: RouterState) => void;

    export function create(options: RouterCreateOption): Router;
    export function run(routes: Route, callback: RouterRunCallback): Router;
    export function run(routes: Route, location: LocationBase, callback: RouterRunCallback): Router;


    //
    // Location
    // ----------------------------------------------------------------------
    export interface LocationBase {
        getCurrentPath(): void;
        toString(): string;
    }
    export interface Location extends LocationBase {
        push(path: string): void;
        replace(path: string): void;
        pop(): void;
    }

    export interface LocationListener {
        addChangeListener(listener: Function): void;
        removeChangeListener(listener: Function): void;
    }

    export interface HashLocation extends Location, LocationListener { }
    export interface HistoryLocation extends Location, LocationListener { }
    export interface RefreshLocation extends Location { }
    export interface StaticLocation extends LocationBase { }
    export interface TestLocation extends Location, LocationListener { }

    export var HashLocation: HashLocation;
    export var HistoryLocation: HistoryLocation;
    export var RefreshLocation: RefreshLocation;
    export var StaticLocation: StaticLocation;
    export var TestLocation: TestLocation;


    //
    // Behavior
    // ----------------------------------------------------------------------
    export interface ScrollBehaviorBase {
        updateScrollPosition(position: { x: number; y: number; }, actionType: string): void;
    }
    export interface ImitateBrowserBehavior extends ScrollBehaviorBase { }
    export interface ScrollToTopBehavior extends ScrollBehaviorBase { }

    export var ImitateBrowserBehavior: ImitateBrowserBehavior;
    export var ScrollToTopBehavior: ScrollToTopBehavior;


    //
    // Mixin
    // ----------------------------------------------------------------------
    export interface Navigation {
        makePath(to: string, params?: {}, query?: {}): string;
        makeHref(to: string, params?: {}, query?: {}): string;
        transitionTo(to: string, params?: {}, query?: {}): void;
        replaceWith(to: string, params?: {}, query?: {}): void;
        goBack(): void;
    }

    export interface State {
        getPath(): string;
        getRoutes(): Route[];
        getPathname(): string;
        getParams(): {};
        getQuery(): {};
        isActive(to: string, params?: {}, query?: {}): boolean;
    }

    export var Navigation: Navigation;
    export var State: State;


    //
    // History
    // ----------------------------------------------------------------------
    export interface History {
        back(): void;
        length: number;
    }
    export var History: History;


    //
    // Context
    // ----------------------------------------------------------------------
    export interface Context {
        makePath(to: string, params?: {}, query?: {}): string;
        makeHref(to: string, params?: {}, query?: {}): string;
        transitionTo(to: string, params?: {}, query?: {}): void;
        replaceWith(to: string, params?: {}, query?: {}): void;
        goBack(): void;

        getCurrentPath(): string;
        getCurrentRoutes(): Route[];
        getCurrentPathname(): string;
        getCurrentParams(): {};
        getCurrentQuery(): {};
        isActive(to: string, params?: {}, query?: {}): boolean;
    }
}

//declare module "react-router" {
//    export = ReactRouter;
//}

//declare module __React {

//  // for DefaultRoute
//  function createElement(
//    type: ReactRouter.DefaultRouteClass,
//    props: ReactRouter.DefaultRouteProp,
//    ...children: __React.ReactNode[]): ReactRouter.DefaultRoute;

//  // for Link
//  function createElement(
//    type: ReactRouter.LinkClass,
//    props: ReactRouter.LinkProp,
//    ...children: __React.ReactNode[]): ReactRouter.Link;

//  // for NotFoundRoute
//  function createElement(
//    type: ReactRouter.NotFoundRouteClass,
//    props: ReactRouter.NotFoundRouteProp,
//    ...children: __React.ReactNode[]): ReactRouter.NotFoundRoute;

//  // for Redirect
//  function createElement(
//    type: ReactRouter.RedirectClass,
//    props: ReactRouter.RedirectProp,
//    ...children: __React.ReactNode[]): ReactRouter.Redirect;

//  // for Route
//  function createElement(
//    type: ReactRouter.RouteClass,
//    props: ReactRouter.RouteProp,
//    ...children: __React.ReactNode[]): ReactRouter.Route;

//  // for RouteHandler
//  function createElement(
//    type: ReactRouter.RouteHandlerClass,
//    props: ReactRouter.RouteHandlerProp,
//    ...children: __React.ReactNode[]): ReactRouter.RouteHandler;
//}

//declare module "react/addons" {
//  // for DefaultRoute
//  function createElement(
//    type: ReactRouter.DefaultRouteClass,
//    props: ReactRouter.DefaultRouteProp,
//    ...children: __React.ReactNode[]): ReactRouter.DefaultRoute;

//  // for Link
//  function createElement(
//    type: ReactRouter.LinkClass,
//    props: ReactRouter.LinkProp,
//    ...children: __React.ReactNode[]): ReactRouter.Link;

//  // for NotFoundRoute
//  function createElement(
//    type: ReactRouter.NotFoundRouteClass,
//    props: ReactRouter.NotFoundRouteProp,
//    ...children: __React.ReactNode[]): ReactRouter.NotFoundRoute;

//  // for Redirect
//  function createElement(
//    type: ReactRouter.RedirectClass,
//    props: ReactRouter.RedirectProp,
//    ...children: __React.ReactNode[]): ReactRouter.Redirect;

//  // for Route
//  function createElement(
//    type: ReactRouter.RouteClass,
//    props: ReactRouter.RouteProp,
//    ...children: __React.ReactNode[]): ReactRouter.Route;

//  // for RouteHandler
//  function createElement(
//    type: ReactRouter.RouteHandlerClass,
//    props: ReactRouter.RouteHandlerProp,
//    ...children: __React.ReactNode[]): ReactRouter.RouteHandler;
//}
