﻿using Orders.Backend.Rules.Web.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Orders.Backend.Rules.Web.Controllers
{
    public class ModelApiController : ApiController
    {
        private PolicyContext db = new PolicyContext();

        [Route("api/model")]
        public IQueryable<PolicyModel> Get()
        {
            return db.PolicyModels;
        }

        //[Route("api/{id}")]
        //public async Task<IHttpActionResult> GetModel(int id)
        //{
        //    var model = await db.PolicyModels.FindAsync(id);
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(model);
        //}

        [Route("api/model/{name}", Name = "GetModel")]
        public async Task<IHttpActionResult> GetModel(string name)
        {
            var model = await db.PolicyModels.SingleOrDefaultAsync(m => m.Name == name);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [Route("api/model")]
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]PolicyModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.PolicyModels.Add(model);
            await db.SaveChangesAsync();
            return CreatedAtRoute("GetModel", new { name = model.Name }, model);
        }

        [Route("api/model/{id}")]
        [HttpPut]
        public async Task<IHttpActionResult> Put(int id, [FromBody]PolicyModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("api/model/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var model = await db.PolicyModels.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            db.PolicyModels.Remove(model);
            await db.SaveChangesAsync();
            return Ok(model);
        }

        private bool ModelExists(int id)
        {
            return db.PolicyModels.Count(e => e.Id == id) > 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
