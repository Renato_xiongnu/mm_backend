﻿using DynamicExpresso;
using Orders.Backend.Rules.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Orders.Backend.Rules.Web.Controllers
{
    public class GenericRulesController<T> : ApiController
    {
        private PolicyContext db = new PolicyContext();

        public async Task<IHttpActionResult> Run(int id, [FromBody]T parameter)
        {
            var policy = await db.PolicyVersions.FindAsync(id);

            var target = new Interpreter()
                .SetVariable("Order", parameter);
            try
            {
                var result = policy.PolicyRules
                    .OrderBy(r => r.Order)
                    .Where(r => target.Eval<bool>(r.Condition))
                    .Select(r => r.Value)
                    .FirstOrDefault();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
