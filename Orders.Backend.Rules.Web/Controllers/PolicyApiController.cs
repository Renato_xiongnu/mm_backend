﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.Rules.Web.Extensions;
using DynamicExpresso;
using Orders.Backend.Rules.Web.Models;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Orders.Backend.Rules.Web.Controllers
{
    public class PolicyApiController : ApiController
    {
        private PolicyContext db = new PolicyContext();

        // GET: api/Policy
        [Route("api/model/{model}/policy")]
        public async Task<IQueryable<Policy>> GetPolicies(string model)
        {            
            return db.Policies.Where(p => p.PolicyModel.Name == model);
        }

        // GET: api/Policy/5
        [Route("api/model/{model}/policy/{name}", Name = "GetPolicy")]
        public async Task<IHttpActionResult> GetPolicy(string name)
        {
            var policy = await db.Policies.SingleOrDefaultAsync(p => p.Name == name);
            if (policy == null)
            {
                return NotFound();
            }
            return Ok(policy);
        }

        [Route("api/model/{model}/policy/{id}/validate", Name = "ValidatePolicy")]
        [HttpGet]
        public async Task<IHttpActionResult> ValidatePolicy(int id)
        {
            var order = Tests.TestOrder();
            var policy = await db.Policies.FindAsync(id);
            if (policy == null)
            {
                return NotFound();
            }
            var version = policy.PolicyVersions.LastOrDefault();
            var target = new Interpreter()
                .SetVariable("Order", order);
            try
            {
                var result = version.PolicyRules
                    .OrderBy(r => r.Order)
                    .Where(r => target.Eval<bool>(r.Condition))
                    .Select(r => r.Value)
                    .FirstOrDefault();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }            
        }

        [Route("api/model/{model}/validaterule", Name = "ValidateRule")]
        [HttpPost]
        public async Task<IHttpActionResult> ValidateRule([FromBody]string condition)
        {
            var order = Tests.TestOrder();
            var target = new Interpreter()
                .SetVariable("Order", order);
            try
            {
                var value = target.Eval<bool>(condition);
                return Ok(value);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/model/{model}/policy")]
        [HttpPost]
        public async Task<IHttpActionResult> PostPolicy([FromBody]Policy policy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            policy.PolicyVersions.Add(new PolicyVersion());
            db.Policies.Add(policy);
            await db.SaveChangesAsync();
            return CreatedAtRoute("GetPolicy", new { name = policy.Name }, policy);
        }

        [Route("api/model/{model}/policy/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeletePolicy(int id)
        {
            var policy = await db.Policies.FindAsync(id);
            if (policy == null)
            {
                return NotFound();
            }
            db.Policies.Remove(policy);
            await db.SaveChangesAsync();
            return Ok(policy);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PolicyExists(int id)
        {
            return db.PolicyVersions.Count(e => e.Id == id) > 0;
        }
    }
}
