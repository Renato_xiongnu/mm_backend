﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.Rules.Web.Extensions;
using DynamicExpresso;
using Orders.Backend.Rules.Web.Models;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Orders.Backend.Rules.Web.Controllers
{
    public class OrderController : GenericModelController<Order>
    {

        [Route("api/validate/order")]
        [HttpPost]
        public async Task<IHttpActionResult> ValidateRule([FromBody]string condition)
        {
            return await ValidateRule("Order", condition);
        }

        [Route("api/run/order/{policyName}")]
        [HttpGet]
        public async Task<IHttpActionResult> Test(string policyName)
        {
            var order = Tests.TestOrder();
            //var policy = Tests.TestPolicy();
            var policy = await db.Policies.SingleOrDefaultAsync(p => p.Name == policyName);
            if (policy == null || policy.PolicyVersions.Count == 0)
            {
                return NotFound();
            }
            var policyVersion = policy.PolicyVersions.LastOrDefault();
            if (policyVersion == null)
            {
                return NotFound();
            }
            return await Test(policyName, policyVersion.Id);
        }

        [Route("api/run/order/{policyName}/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Test(string policyName, int id)
        {
            var order = Tests.TestOrder();
            var policyVersion = await db.PolicyVersions.FindAsync(id);
            if (policyVersion == null)
            {
                return NotFound();
            }
            try
            {
                var result = RunPolicy("Order", policyVersion, order);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/run/order/{policyName}")]
        [HttpPost]
        public async Task<IHttpActionResult> Run(string policyName, [FromBody]Order parameter)
        {
            return await Run("Order", policyName, parameter);
        }

        [Route("api/run/order/{policyName}/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> RunVersion(string policyName, int id, [FromBody]Order parameter)
        {
            return await RunVersion("Order", policyName, id, parameter);
        }

    }
}
