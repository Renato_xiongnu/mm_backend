﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.Rules.Web.Extensions;
using DynamicExpresso;
using Orders.Backend.Rules.Web.Models;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Orders.Backend.Rules.Web.Controllers
{
    public class OrderContextController : GenericModelController<OrderContext>
    {
        [Route("api/validate/OrderContext")]
        [HttpPost]
        public async Task<IHttpActionResult> ValidateRule([FromBody]string condition)
        {
            return await ValidateRule("OrderContext", condition);
        }

        [Route("api/run/OrderContext/{policyName}")]
        [HttpPost]
        public async Task<IHttpActionResult> Run(string policyName, [FromBody]OrderContext parameter)
        {
            return await Run("OrderContext", policyName, parameter);
        }

        [Route("api/run/OrderContext/{policyName}/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> RunVersion(string policyName, int id, [FromBody]OrderContext parameter)
        {
            return await RunVersion("OrderContext", policyName, id, parameter);
        }
    }
}
