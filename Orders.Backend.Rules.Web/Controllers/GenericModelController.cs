﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Orders.Backend.WF.Activities.Model;
using Orders.Backend.Rules.Web.Extensions;
using DynamicExpresso;
using Orders.Backend.Rules.Web.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Orders.Backend.Rules.Web.Controllers
{
    public abstract class GenericModelController<T> : ApiController where T: new()
    {
        protected PolicyContext db = new PolicyContext();
        public virtual async Task<IHttpActionResult> ValidateRule(string modelName, [FromBody]string condition)
        {
            var target = new Interpreter()
                .SetVariable(modelName, new T());
            try
            {
                var value = target.Parse(condition);
                return Ok(value != null);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public virtual async Task<IHttpActionResult> Run(string modelName, string policyName, [FromBody]T parameter)
        {
            var sw = new Stopwatch();
            sw.Start();

            var policy = await db.Policies.SingleOrDefaultAsync(p => p.Name == policyName);
            if (policy == null)
            {
                return NotFound();
            }
            var policyVersion = policy.PolicyVersions.LastOrDefault();
            if (policyVersion == null)
            {
                return NotFound();
            }
            string result = string.Empty;
            try
            {
                result = RunPolicy(modelName, policyVersion, parameter);
                return Ok(result);
            }
            catch (Exception ex)
            {                
                return BadRequest(ex.Message);
            }
            finally
            {
                sw.Stop();
                db.PolicyLogs.Add(new PolicyLog
                {
                    PolicyVersionId = policyVersion.Id,
                    Model = JsonConvert.SerializeObject(parameter),
                    Value = result,
                    Duration = sw.Elapsed
                });
                await db.SaveChangesAsync();
            }
        }

        public virtual async Task<IHttpActionResult> RunVersion(string modelName, string policyName, int id, [FromBody]T parameter)
        {
            var policy = await db.Policies.SingleOrDefaultAsync(p => p.Name == policyName);
            if (policy == null)
            {
                return NotFound();
            }
            var policyVersion = await db.PolicyVersions.FindAsync(id);
            if (policyVersion == null)
            {
                return NotFound();
            }
            try
            {
                var result = RunPolicy(modelName, policyVersion, parameter);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        protected string RunPolicy(string modelName, PolicyVersion policyVersion, T parameter)
        {
            var target = new Interpreter()
                .Reference(typeof(Enum))
                .SetVariable(modelName, parameter);
            var result = policyVersion.PolicyRules
                .OrderBy(r => r.Order)
                .Where(r => target.Eval<bool>(r.Condition))
                .Select(r => r.Value)
                .FirstOrDefault();
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
