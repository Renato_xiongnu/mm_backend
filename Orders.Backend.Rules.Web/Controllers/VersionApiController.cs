﻿using DynamicExpresso;
using Orders.Backend.Rules.Web.Extensions;
using Orders.Backend.Rules.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Orders.Backend.Rules.Web.Controllers
{
    public class VersionApiController : ApiController
    {
        private PolicyContext db = new PolicyContext();

        [Route("api/model/{model}/policy/{policy}/version/{id}", Name = "GetVersion")]
        public async Task<IHttpActionResult> GetVersion(int id)
        {
            var policy = await db.PolicyVersions.FindAsync(id);
            if (policy == null)
            {
                return NotFound();
            }
            return Ok(policy);
        }

        [Route("api/model/{model}/policy/{policy}/version")]
        [HttpPost]
        public async Task<IHttpActionResult> PostVersion([FromBody]PolicyVersion policyVersion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            policyVersion.Id = 0;
            policyVersion.PolicyRules.ToList().ForEach(r => r.PolicyVersionId = 0);
            db.PolicyVersions.Add(policyVersion);
            await db.SaveChangesAsync();
            return CreatedAtRoute("GetVersion", new { id = policyVersion.Id }, policyVersion);
        }
        
        [Route("api/model/{model}/policy/{policy}/version/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteVersion(int id)
        {
            var policy = await db.PolicyVersions.FindAsync(id);
            if (policy == null)
            {
                return NotFound();
            }

            db.PolicyVersions.Remove(policy);
            await db.SaveChangesAsync();
            return Ok(policy);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
