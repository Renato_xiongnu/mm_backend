﻿using Orders.Backend.Log.Web.Models;
using Orders.Backend.Log.Web.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Orders.Backend.Log.Web.Controllers
{    
    public class CorrelationController : ApiController
    {
        // GET: api/correlations
        [Route("api/correlations")]
        public async Task<IEnumerable<CorrelationEntry>> Get()
        {
            using (var provider = new LogProvider())
            {
                return await provider.GetLattestCorrelationIds("%");
            };
        }

        // GET: api/correlations/logs
        [Route("api/correlations/logs")]
        public async Task<IEnumerable<LogEntry>> GetLogsByCorrelationId()
        {
            using (var provider = new LogProvider())
            {
                return await provider.GetLogsByCorrelationId(string.Empty);
            };
        }

        // GET: api/correlations/5
        [Route("api/correlations/{id}")]
        public async Task<IEnumerable<CorrelationEntry>> Get(string id)
        {
            using (var provider = new LogProvider())
            {
                return await provider.GetLattestCorrelationIds(id + "%");
            };
        }

        // GET: api/correlations/111-111-111/logs
        [Route("api/correlations/{id}/logs")]
        public async Task<IEnumerable<LogEntry>> GetLogsByCorrelationId(string id)
        {
            using (var provider = new LogProvider())
            {
                return await provider.GetLogsByCorrelationId(id);
            };
        }

    }
}
