﻿using Orders.Backend.Log.Web.Models;
using Orders.Backend.Log.Web.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Orders.Backend.Log.Web.Controllers
{
    public class LogController : ApiController
    {
        // GET: api/logs
        [Route("api/logs")]
        public async Task<IEnumerable<LogEntry>> Get()
        {
            return Enumerable.Empty<LogEntry>();
        }

        // GET: api/logs/5
        [Route("api/logs/{id:long}")]
        public async Task<LogEntry> Get(long id)
        {
            using (var provider = new LogProvider())
            {
                return await provider.GetLog(id);
            }
        }

        // POST: api/logs
        [Route("api/logs")]
        [HttpPost]
        public async Task<int> Post([FromBody]LogEntry value)
        {
            using (var provider = new LogProvider())
            {
                return await provider.CreateLog(value);
            }
        }

        [Route("api/logs/{id:long}")]
        // PUT: api/logs/5
        public void Put(int id, [FromBody]LogEntry value)
        {
            throw new NotImplementedException();
        }

        [Route("api/logs/{id:long}")]
        // DELETE: api/logs/5
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
