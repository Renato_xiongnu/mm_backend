﻿using Dapper;
using Orders.Backend.Log.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Orders.Backend.Log.Web.Providers
{
    public class LogProvider : IDisposable
    {
        public const string SelectById = @"SELECT TOP 1 [Id], [EventDateTime], [EventLevel], [UserName], [MachineName], [ApplicationName], [Message], [CorrelationId], [Exception] FROM [dbo].[Log] WHERE [Id] = @Id";

        public const string SelectByCorrelationId = @"SELECT TOP 1000 [Id], [EventDateTime], [EventLevel], [UserName], [MachineName], [ApplicationName], [Message], [CorrelationId], [Exception] FROM [dbo].[Log] WHERE [CorrelationId] = @CorrelationId ORDER BY [EventDateTime] DESC";

        public const string SelectLatestCorrelationId = @"SELECT TOP 10 [CorrelationId], MAX([EventDateTime]) AS [LastEventDateTime], COUNT(0) AS [EventCount] FROM [dbo].[Log] WHERE [CorrelationId] LIKE @CorrelationId GROUP BY [CorrelationId] ORDER BY [LastEventDateTime] DESC";

        public const string Insert = @"INSERT INTO [dbo].[Log] (EventDateTime, EventLevel, UserName, MachineName, ApplicationName, Message, CorrelationId, Exception) VALUES (@EventDateTime, @EventLevel, @UserName, @MachineName, @ApplicationName, @Message, @CorrelationId, @Exception)";

        SqlConnection _connection;
        public LogProvider()
        {
            var connectionString = WebConfigurationManager.ConnectionStrings["LogConnection"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }

        protected async Task<T> WithConnection<T>(Func<SqlConnection, Task<T>> getData)
        {
            try
            {
                await _connection.OpenAsync(); // Asynchronously open a connection to the database
                return await getData(_connection); // Asynchronously execute getData, which has been passed in as a Func<IDBConnection, Task<T>>
            }
            catch (TimeoutException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
            }
        }

        public async Task<LogEntry> GetLog(long id)
        {
            return await WithConnection(async c => {
                var p = new DynamicParameters();
                p.Add("Id", id, DbType.Int64);
                var logs = await c.QueryAsync<LogEntry>(
                    sql: SelectById, 
                    param: p, 
                    commandType: CommandType.Text);
                return logs.FirstOrDefault();    
            });
        }

        public async Task<IEnumerable<LogEntry>> GetLogsByCorrelationId(string correlationId)
        {
            return await WithConnection(async c =>
            {
                var p = new DynamicParameters();
                p.Add("CorrelationId", correlationId, DbType.String);
                var logs = await c.QueryAsync<LogEntry>(
                    sql: SelectByCorrelationId,
                    param: p,
                    commandType: CommandType.Text);
                return logs;
            });
        }

        public async Task<IEnumerable<CorrelationEntry>> GetLattestCorrelationIds(string correlationId)
        {
            return await WithConnection(async c =>
            {
                var p = new DynamicParameters();
                p.Add("CorrelationId", correlationId, DbType.String);
                var ids = await c.QueryAsync<CorrelationEntry>(
                    sql: SelectLatestCorrelationId,
                    param: p,
                    commandType: CommandType.Text);
                return ids;
            });
        }

        public async Task<int> CreateLog(LogEntry log)
        {
            return await WithConnection(async c =>
            {
                var p = new DynamicParameters(log);
                //p.Add("CorrelationId", correlationId, DbType.String);
                var rows = await c.ExecuteAsync(
                    sql: Insert,
                    param: p,
                    commandType: CommandType.Text);
                return rows;
            });
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_connection != null)
                {                 
                    _connection.Dispose();
                    _connection = null;
                }
            }            
        }        
    }
}