﻿/// <reference path="d/_references.d.ts" />
var d = React.DOM;
var $ = jQuery;

declare var LogsRootUrl;

function distinct(arr, keySelector = null) {
    var n = {}, r = [];
    for (var i = 0; i < arr.length; i++) {
        var key = keySelector ? keySelector(arr[i]) : arr[i];
        if (!n[key]) {
            n[key] = true;
            r.push(arr[i]);
        }
    }
    return r;
}

function GetByCorrelationId(id: string) {
    return $.ajax({
        url: LogsRootUrl + '/api/correlations/' + id + '/logs/',
        dataType: 'json',
        data: {}
    });
}

function GetLattestCorrelationIds(id) {
    return $.ajax({
        url: LogsRootUrl + '/api/correlations/' + id,
        dataType: 'json',
        data: {}
    });
}

interface ILogEntry {
    Id: number,
    EventDateTime: string,
    EventLevel: string,
    UserName: string,
    MachineName: string,
    ApplicationName: string,
    Message: string,
    CorrelationId: string,
    Exception: string
}

interface ICorrelation {
    CorrelationId: string,
    LastEventDateTime: string,
    EventCount: number
}

interface ILogsStore {
    logs: Array<ILogEntry>,
    filteredLogs: Array<ILogEntry>,
    correlations: Array<ICorrelation>,
    selectedCorrelations: Array<ICorrelation>,
    applications: Array<string>,
    selectedApplications: Array<string>,
    eventLevels: Array<string>,
    selectedEventLevels: Array<string>,
    searchText: string
}

var data: ILogsStore = {
    logs: [],
    filteredLogs: [],
    correlations: [],
    selectedCorrelations: [],
    applications: [],
    selectedApplications: [],
    eventLevels: [],
    selectedEventLevels: [],
    searchText: ""
};

var Loading = new Rx.BehaviorSubject<boolean>(false);

var LogsStore = new Rx.BehaviorSubject<ILogsStore>(data);

function applyFilters(store: ILogsStore) {
    store.filteredLogs = store.logs
        .filter(l => store.selectedEventLevels.length == 0 || store.selectedEventLevels.indexOf(l.EventLevel) !== -1)
        .filter(l => store.selectedApplications.length == 0 || store.selectedApplications.indexOf(l.ApplicationName) !== -1)
    store.filteredLogs.sort((a, b) => Date.parse(b.EventDateTime) - Date.parse(a.EventDateTime));
}

var LogsActions = {
    reload: new Rx.Subject<any>(),
    getByCorrelationId: new Rx.Subject<string>(),
    getLattestCorrelationIds: new Rx.Subject<string>(),
    searchByCorrelationIds: new Rx.Subject<string>(),    

    addCorrelation: new Rx.Subject<ICorrelation>(),
    removeCorrelation: new Rx.Subject<ICorrelation>(),

    addApplication: new Rx.Subject<string>(),
    removeApplication: new Rx.Subject<string>(),

    addEventLevel: new Rx.Subject<string>(),
    removeEventLevel: new Rx.Subject<string>(),

    subscribe: function (store: Rx.BehaviorSubject<ILogsStore>) {
        this.reload
            .sample(1000)
            .flatMap(() => store.getValue().selectedCorrelations)
            .map(cor => cor.CorrelationId)
            .subscribe(this.getByCorrelationId);

        this.reload
            .sample(1000)
            .map(() => {
                return store.getValue().searchText;
            })
            .subscribe(this.getLattestCorrelationIds);

        this.getByCorrelationId
            .map(id => {
                Loading.onNext(true);
                return id;
            })
            .flatMapLatest((id) => Rx.Observable.fromPromise(GetByCorrelationId(id)).catch(e => Rx.Observable.empty()))
            .map(logs => {
                var value = store.getValue();
                value.logs = distinct(value.logs.concat(logs), l => l.Id);
                value.applications = distinct(value.logs.map(l => l.ApplicationName));
                value.eventLevels = distinct(value.logs.map(l => l.EventLevel));
                applyFilters(value);
                Loading.onNext(false);
                return value;
            })
            .subscribe(store);

        this.addCorrelation
            .filter((cor: ICorrelation) => {
                var value = store.getValue();
                var idx = value.selectedCorrelations.map(c => c.CorrelationId).indexOf(cor.CorrelationId);
                return idx === -1;
            })
            .map((cor: ICorrelation) => {
                var value = store.getValue();
                value.selectedCorrelations.push(cor);
                return cor.CorrelationId;
            })
            .subscribe(this.getByCorrelationId);

        this.removeCorrelation
            .map((cor: ICorrelation) => {
                var value = store.getValue();
                var idx = value.selectedCorrelations.indexOf(cor);
                value.selectedCorrelations.splice(idx, 1);
                value.logs = value.logs.filter(l => l.CorrelationId !== cor.CorrelationId);
                value.applications = distinct(value.logs.map(l => l.ApplicationName));
                value.eventLevels = distinct(value.logs.map(l => l.EventLevel));
                applyFilters(value);
                return value;
            })
            .subscribe(store);

        this.getLattestCorrelationIds
            .flatMapLatest((id) => Rx.Observable.fromPromise(GetLattestCorrelationIds(id)).catch(e => Rx.Observable.empty()))
            .map(correlations => {
                var value = store.getValue();
                value.correlations = correlations;
                return value;
            })
            .subscribe(store);

        var searchEvents = this.searchByCorrelationIds
            .debounce(100)
            //.filter(id => id != null && id.length > 2)
            .distinctUntilChanged()
        
        searchEvents.map(id => {
                var value = store.getValue();
                value.searchText = id;
                return value;
            })
            .subscribe(store);            

        searchEvents.subscribe(this.getLattestCorrelationIds);

        this.addEventLevel
            .filter((tag: string) => store.getValue().selectedEventLevels.indexOf(tag) === -1)
            .map((tag: string) => {
                var value = store.getValue();
                value.selectedEventLevels.push(tag);
                applyFilters(value);
                return value;
            })
            .subscribe(store);

        this.removeEventLevel
            .map((tag: string) => {
                var value = store.getValue();
                var idx = value.selectedEventLevels.indexOf(tag);
                value.selectedEventLevels.splice(idx, 1);
                applyFilters(value);
                return value;
            })
            .subscribe(store);


        this.addApplication
            .filter((tag: string) => store.getValue().selectedApplications.indexOf(tag) === -1)
            .map((tag: string) => {
                var value = store.getValue();
                value.selectedApplications.push(tag);
                applyFilters(value);
                return value;
            })
            .subscribe(store);

        this.removeApplication
            .map((tag: string) => {
                var value = store.getValue();
                var idx = value.selectedApplications.indexOf(tag);
                value.selectedApplications.splice(idx, 1);
                applyFilters(value);
                return value;
            })
            .subscribe(store);
    }
};

LogsActions.subscribe(LogsStore);

class LogsTable extends React.Component<any, any> {
    render() {
        var Table = React.createFactory(MaterialDataTable);
        var Column = React.createFactory(MaterialTableColumn);

        var messageRenderer = function (item, column, rowIndex) {
            return d.div({},
                d.div({}, item.Message),
                d.pre({ className: "mdl-typography--caption mdl-color-text--red" }, item.Exception)
            );
        };

        return (
            Table({ data: this.props.filteredLogs, keySelector: (item, index) => item.Id },
                Column({ dataField: "EventDateTime", nonNumeric: true }, "EventDateTime"),
                Column({ dataField: "EventLevel", nonNumeric: true }, "EventLevel"),
                //Column({ dataField: "UserName", nonNumeric: true }, "UserName"),
                //Column({ dataField: "MachineName", nonNumeric: true }, "MachineName"),
                Column({ dataField: "ApplicationName", nonNumeric: true }, "ApplicationName"),
                Column({ dataField: "Message", nonNumeric: true, valueRender: messageRenderer }, "Message"),
                Column({ dataField: "CorrelationId", nonNumeric: true }, "CorrelationId")
            )
        );
    }
};

class MaterialInput extends React.Component<any, any> {
    render() {
        return (
            d.div({ className: "mdl-textfield mdl-js-textfield", style: this.props.style },
                d.input({ className: "mdl-textfield__input", type: "text", id: this.props.id, onChange: this.props.onChange, onBlur: this.props.onBlur }),
                d.label({ className: "mdl-textfield__label", htmlFor: this.props.id }, this.props.children)
            )
        );
    }
}

class CorrelationList extends React.Component<ILogsStore, any> {
    linkClick(index) {
        var cor = this.props.correlations[index];
        LogsActions.addCorrelation.onNext(cor);
    }

    searchCorrelation(e) {
        LogsActions.searchByCorrelationIds.onNext(e.target.value);
    }

    render() {
        var Input = React.createFactory(MaterialInput);
        var Link = React.createFactory(MaterialLink);
        var links = this.props.correlations.map((cor, index) => Link({ key: cor.CorrelationId, href: "#", onClick: this.linkClick.bind(this, index), badge: cor.EventCount }, cor.CorrelationId || "Empty"));

        return (
            d.div({ className: "mdl-navigation" },
                d.div({ className: "mdl-card__supporting-text" }, "Correlations"),                
                Input({ id: "", style: { width: "auto", margin: "0 16px 0 16px" }, onChange: this.searchCorrelation }, "Search CorrelationId"),
                links
            )
        );
    }
};

class ApplicationList extends React.Component<ILogsStore, any> {
    render() {
        var Link = React.createFactory(MaterialLink);
        var links = this.props.applications.map((app, index) => Link({ key: app, href: "#", onClick: (e) => LogsActions.addApplication.onNext(app) }, app));

        return (links.length === 0 ? null :
            d.div({ className: "mdl-navigation" },
                d.div({ className: "mdl-card__supporting-text" }, "Applications"),
                links
            )
        );
    }
};

class EventLevelList extends React.Component<ILogsStore, any> {
    render() {
        var Link = React.createFactory(MaterialLink);
        var links = this.props.eventLevels.map((level, index) => Link({ key: level, href: "#", onClick: (e) => LogsActions.addEventLevel.onNext(level) }, level));

        return (links.length === 0 ? null :
            d.div({ className: "mdl-navigation" },
                d.div({ className: "mdl-card__supporting-text" }, "Levels"),
                links
            )
        );
    }
};

class MaterialLink extends React.Component<any, any> {
    render() {
        return d.a({ className: "mdl-navigation__link", href: this.props.href, onClick: this.props.onClick },
            d.div({ className: "mdl-badge", "data-badge": this.props.badge },
                this.props.children
            )
        );
    };
}

class TagList extends React.Component<any, any> {
    render() {
        var keySelector = this.props.keySelector || (tag => tag);
        var labelSelector = this.props.labelSelector || (tag => tag);
        var tags = this.props.tags.map(tag => {
            return d.span({ className: "mdl-tag", key: keySelector(tag), onClick: (e) => { return this.props.onClick(tag); } }, labelSelector(tag));
        });
        return d.span({}, tags);
    }
}

class LogsFilters extends React.Component<ILogsStore, any> {
    render() {
        var filters = this.props.selectedCorrelations.map(cor => {
            return d.span({ className: "mdl-tag", key: cor.CorrelationId, onClick: (e) => { return LogsActions.removeCorrelation.onNext(cor); }, badge: cor.EventCount }, cor.CorrelationId || "Empty");
        });
        return d.span({}, filters);
    }
}

class LogsApp extends React.Component<ILogsStore, any> {
    componentWillMount() {
    }
    render() {
        var Logs = React.createFactory(LogsTable);
        var Filters = React.createFactory(LogsFilters);
        var Tags = React.createFactory(TagList);        

        return (
            d.div({ style: { margin: "16px" } },
                d.button({ className: "mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent", id: "reload", onClick: LogsActions.reload.onNext.bind(LogsActions.reload) },
                    d.i({ className: "material-icons" }, "refresh")
                ),
                d.div({ className: "logs-filters" },
                    Filters(this.props),
                    Tags({ className: "mdl-tag", tags: this.props.selectedApplications, onClick: LogsActions.removeApplication.onNext.bind(LogsActions.removeApplication) }),
                    Tags({ className: "mdl-tag", tags: this.props.selectedEventLevels, onClick: LogsActions.removeEventLevel.onNext.bind(LogsActions.removeEventLevel) })
                ),
                Logs(this.props)
            )
        );
    }
};

class LogsDrawer extends React.Component<ILogsStore, any> {
    render() {
        var Correlations = React.createFactory(CorrelationList);
        var Application = React.createFactory(ApplicationList);
        var Levels = React.createFactory(EventLevelList);

        return (
            d.div({},
                Correlations(this.props),
                Application(this.props),
                Levels(this.props)
            )
        );
    }
};

// Init
//LogsActions.getByCorrelationId.onNext("");
LogsActions.getLattestCorrelationIds.onNext("");

var Spinner = React.createFactory(MaterialSpinner);
var App = React.createFactory(LogsApp);
var Drawer = React.createFactory(LogsDrawer);

LogsStore.subscribe(
    function (value) {
        React.render(App(value), document.getElementById('app'));
        React.render(Drawer(value), document.getElementById('drawer'));
    },
    function (error) {
        console.log(error);
    }
);

Loading.subscribe(
    function (value) {
        React.render(Spinner({ isActive: value, isUpgraded: true }), document.getElementById('loading'));
    },
    function (error) {
        console.log(error);
    }
);
