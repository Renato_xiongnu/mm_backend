var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="d/_references.d.ts" />
var d = React.DOM;
var $ = jQuery;
function distinct(arr, keySelector) {
    if (keySelector === void 0) { keySelector = null; }
    var n = {}, r = [];
    for (var i = 0; i < arr.length; i++) {
        var key = keySelector ? keySelector(arr[i]) : arr[i];
        if (!n[key]) {
            n[key] = true;
            r.push(arr[i]);
        }
    }
    return r;
}
function GetByCorrelationId(id) {
    return $.ajax({
        url: LogsRootUrl + '/api/correlations/' + id + '/logs/',
        dataType: 'json',
        data: {}
    });
}
function GetLattestCorrelationIds(id) {
    return $.ajax({
        url: LogsRootUrl + '/api/correlations/' + id,
        dataType: 'json',
        data: {}
    });
}
var data = {
    logs: [],
    filteredLogs: [],
    correlations: [],
    selectedCorrelations: [],
    applications: [],
    selectedApplications: [],
    eventLevels: [],
    selectedEventLevels: [],
    searchText: ""
};
var Loading = new Rx.BehaviorSubject(false);
var LogsStore = new Rx.BehaviorSubject(data);
function applyFilters(store) {
    store.filteredLogs = store.logs
        .filter(function (l) { return store.selectedEventLevels.length == 0 || store.selectedEventLevels.indexOf(l.EventLevel) !== -1; })
        .filter(function (l) { return store.selectedApplications.length == 0 || store.selectedApplications.indexOf(l.ApplicationName) !== -1; });
    store.filteredLogs.sort(function (a, b) { return Date.parse(b.EventDateTime) - Date.parse(a.EventDateTime); });
}
var LogsActions = {
    reload: new Rx.Subject(),
    getByCorrelationId: new Rx.Subject(),
    getLattestCorrelationIds: new Rx.Subject(),
    searchByCorrelationIds: new Rx.Subject(),
    addCorrelation: new Rx.Subject(),
    removeCorrelation: new Rx.Subject(),
    addApplication: new Rx.Subject(),
    removeApplication: new Rx.Subject(),
    addEventLevel: new Rx.Subject(),
    removeEventLevel: new Rx.Subject(),
    subscribe: function (store) {
        this.reload
            .sample(1000)
            .flatMap(function () { return store.getValue().selectedCorrelations; })
            .map(function (cor) { return cor.CorrelationId; })
            .subscribe(this.getByCorrelationId);
        this.reload
            .sample(1000)
            .map(function () {
            return store.getValue().searchText;
        })
            .subscribe(this.getLattestCorrelationIds);
        this.getByCorrelationId
            .map(function (id) {
            Loading.onNext(true);
            return id;
        })
            .flatMapLatest(function (id) { return Rx.Observable.fromPromise(GetByCorrelationId(id)).catch(function (e) { return Rx.Observable.empty(); }); })
            .map(function (logs) {
            var value = store.getValue();
            value.logs = distinct(value.logs.concat(logs), function (l) { return l.Id; });
            value.applications = distinct(value.logs.map(function (l) { return l.ApplicationName; }));
            value.eventLevels = distinct(value.logs.map(function (l) { return l.EventLevel; }));
            applyFilters(value);
            Loading.onNext(false);
            return value;
        })
            .subscribe(store);
        this.addCorrelation
            .filter(function (cor) {
            var value = store.getValue();
            var idx = value.selectedCorrelations.map(function (c) { return c.CorrelationId; }).indexOf(cor.CorrelationId);
            return idx === -1;
        })
            .map(function (cor) {
            var value = store.getValue();
            value.selectedCorrelations.push(cor);
            return cor.CorrelationId;
        })
            .subscribe(this.getByCorrelationId);
        this.removeCorrelation
            .map(function (cor) {
            var value = store.getValue();
            var idx = value.selectedCorrelations.indexOf(cor);
            value.selectedCorrelations.splice(idx, 1);
            value.logs = value.logs.filter(function (l) { return l.CorrelationId !== cor.CorrelationId; });
            value.applications = distinct(value.logs.map(function (l) { return l.ApplicationName; }));
            value.eventLevels = distinct(value.logs.map(function (l) { return l.EventLevel; }));
            applyFilters(value);
            return value;
        })
            .subscribe(store);
        this.getLattestCorrelationIds
            .flatMapLatest(function (id) { return Rx.Observable.fromPromise(GetLattestCorrelationIds(id)).catch(function (e) { return Rx.Observable.empty(); }); })
            .map(function (correlations) {
            var value = store.getValue();
            value.correlations = correlations;
            return value;
        })
            .subscribe(store);
        var searchEvents = this.searchByCorrelationIds
            .debounce(100)
            .distinctUntilChanged();
        searchEvents.map(function (id) {
            var value = store.getValue();
            value.searchText = id;
            return value;
        })
            .subscribe(store);
        searchEvents.subscribe(this.getLattestCorrelationIds);
        this.addEventLevel
            .filter(function (tag) { return store.getValue().selectedEventLevels.indexOf(tag) === -1; })
            .map(function (tag) {
            var value = store.getValue();
            value.selectedEventLevels.push(tag);
            applyFilters(value);
            return value;
        })
            .subscribe(store);
        this.removeEventLevel
            .map(function (tag) {
            var value = store.getValue();
            var idx = value.selectedEventLevels.indexOf(tag);
            value.selectedEventLevels.splice(idx, 1);
            applyFilters(value);
            return value;
        })
            .subscribe(store);
        this.addApplication
            .filter(function (tag) { return store.getValue().selectedApplications.indexOf(tag) === -1; })
            .map(function (tag) {
            var value = store.getValue();
            value.selectedApplications.push(tag);
            applyFilters(value);
            return value;
        })
            .subscribe(store);
        this.removeApplication
            .map(function (tag) {
            var value = store.getValue();
            var idx = value.selectedApplications.indexOf(tag);
            value.selectedApplications.splice(idx, 1);
            applyFilters(value);
            return value;
        })
            .subscribe(store);
    }
};
LogsActions.subscribe(LogsStore);
var LogsTable = (function (_super) {
    __extends(LogsTable, _super);
    function LogsTable() {
        _super.apply(this, arguments);
    }
    LogsTable.prototype.render = function () {
        var Table = React.createFactory(MaterialDataTable);
        var Column = React.createFactory(MaterialTableColumn);
        var messageRenderer = function (item, column, rowIndex) {
            return d.div({}, d.div({}, item.Message), d.pre({ className: "mdl-typography--caption mdl-color-text--red" }, item.Exception));
        };
        return (Table({ data: this.props.filteredLogs, keySelector: function (item, index) { return item.Id; } }, Column({ dataField: "EventDateTime", nonNumeric: true }, "EventDateTime"), Column({ dataField: "EventLevel", nonNumeric: true }, "EventLevel"), 
        //Column({ dataField: "UserName", nonNumeric: true }, "UserName"),
        //Column({ dataField: "MachineName", nonNumeric: true }, "MachineName"),
        Column({ dataField: "ApplicationName", nonNumeric: true }, "ApplicationName"), Column({ dataField: "Message", nonNumeric: true, valueRender: messageRenderer }, "Message"), Column({ dataField: "CorrelationId", nonNumeric: true }, "CorrelationId")));
    };
    return LogsTable;
})(React.Component);
;
var MaterialInput = (function (_super) {
    __extends(MaterialInput, _super);
    function MaterialInput() {
        _super.apply(this, arguments);
    }
    MaterialInput.prototype.render = function () {
        return (d.div({ className: "mdl-textfield mdl-js-textfield", style: this.props.style }, d.input({ className: "mdl-textfield__input", type: "text", id: this.props.id, onChange: this.props.onChange, onBlur: this.props.onBlur }), d.label({ className: "mdl-textfield__label", htmlFor: this.props.id }, this.props.children)));
    };
    return MaterialInput;
})(React.Component);
var CorrelationList = (function (_super) {
    __extends(CorrelationList, _super);
    function CorrelationList() {
        _super.apply(this, arguments);
    }
    CorrelationList.prototype.linkClick = function (index) {
        var cor = this.props.correlations[index];
        LogsActions.addCorrelation.onNext(cor);
    };
    CorrelationList.prototype.searchCorrelation = function (e) {
        LogsActions.searchByCorrelationIds.onNext(e.target.value);
    };
    CorrelationList.prototype.render = function () {
        var _this = this;
        var Input = React.createFactory(MaterialInput);
        var Link = React.createFactory(MaterialLink);
        var links = this.props.correlations.map(function (cor, index) { return Link({ key: cor.CorrelationId, href: "#", onClick: _this.linkClick.bind(_this, index), badge: cor.EventCount }, cor.CorrelationId || "Empty"); });
        return (d.div({ className: "mdl-navigation" }, d.div({ className: "mdl-card__supporting-text" }, "Correlations"), Input({ id: "", style: { width: "auto", margin: "0 16px 0 16px" }, onChange: this.searchCorrelation }, "Search CorrelationId"), links));
    };
    return CorrelationList;
})(React.Component);
;
var ApplicationList = (function (_super) {
    __extends(ApplicationList, _super);
    function ApplicationList() {
        _super.apply(this, arguments);
    }
    ApplicationList.prototype.render = function () {
        var Link = React.createFactory(MaterialLink);
        var links = this.props.applications.map(function (app, index) { return Link({ key: app, href: "#", onClick: function (e) { return LogsActions.addApplication.onNext(app); } }, app); });
        return (links.length === 0 ? null :
            d.div({ className: "mdl-navigation" }, d.div({ className: "mdl-card__supporting-text" }, "Applications"), links));
    };
    return ApplicationList;
})(React.Component);
;
var EventLevelList = (function (_super) {
    __extends(EventLevelList, _super);
    function EventLevelList() {
        _super.apply(this, arguments);
    }
    EventLevelList.prototype.render = function () {
        var Link = React.createFactory(MaterialLink);
        var links = this.props.eventLevels.map(function (level, index) { return Link({ key: level, href: "#", onClick: function (e) { return LogsActions.addEventLevel.onNext(level); } }, level); });
        return (links.length === 0 ? null :
            d.div({ className: "mdl-navigation" }, d.div({ className: "mdl-card__supporting-text" }, "Levels"), links));
    };
    return EventLevelList;
})(React.Component);
;
var MaterialLink = (function (_super) {
    __extends(MaterialLink, _super);
    function MaterialLink() {
        _super.apply(this, arguments);
    }
    MaterialLink.prototype.render = function () {
        return d.a({ className: "mdl-navigation__link", href: this.props.href, onClick: this.props.onClick }, d.div({ className: "mdl-badge", "data-badge": this.props.badge }, this.props.children));
    };
    ;
    return MaterialLink;
})(React.Component);
var TagList = (function (_super) {
    __extends(TagList, _super);
    function TagList() {
        _super.apply(this, arguments);
    }
    TagList.prototype.render = function () {
        var _this = this;
        var keySelector = this.props.keySelector || (function (tag) { return tag; });
        var labelSelector = this.props.labelSelector || (function (tag) { return tag; });
        var tags = this.props.tags.map(function (tag) {
            return d.span({ className: "mdl-tag", key: keySelector(tag), onClick: function (e) { return _this.props.onClick(tag); } }, labelSelector(tag));
        });
        return d.span({}, tags);
    };
    return TagList;
})(React.Component);
var LogsFilters = (function (_super) {
    __extends(LogsFilters, _super);
    function LogsFilters() {
        _super.apply(this, arguments);
    }
    LogsFilters.prototype.render = function () {
        var filters = this.props.selectedCorrelations.map(function (cor) {
            return d.span({ className: "mdl-tag", key: cor.CorrelationId, onClick: function (e) { return LogsActions.removeCorrelation.onNext(cor); }, badge: cor.EventCount }, cor.CorrelationId || "Empty");
        });
        return d.span({}, filters);
    };
    return LogsFilters;
})(React.Component);
var LogsApp = (function (_super) {
    __extends(LogsApp, _super);
    function LogsApp() {
        _super.apply(this, arguments);
    }
    LogsApp.prototype.componentWillMount = function () {
    };
    LogsApp.prototype.render = function () {
        var Logs = React.createFactory(LogsTable);
        var Filters = React.createFactory(LogsFilters);
        var Tags = React.createFactory(TagList);
        return (d.div({ style: { margin: "16px" } }, d.button({ className: "mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent", id: "reload", onClick: LogsActions.reload.onNext.bind(LogsActions.reload) }, d.i({ className: "material-icons" }, "refresh")), d.div({ className: "logs-filters" }, Filters(this.props), Tags({ className: "mdl-tag", tags: this.props.selectedApplications, onClick: LogsActions.removeApplication.onNext.bind(LogsActions.removeApplication) }), Tags({ className: "mdl-tag", tags: this.props.selectedEventLevels, onClick: LogsActions.removeEventLevel.onNext.bind(LogsActions.removeEventLevel) })), Logs(this.props)));
    };
    return LogsApp;
})(React.Component);
;
var LogsDrawer = (function (_super) {
    __extends(LogsDrawer, _super);
    function LogsDrawer() {
        _super.apply(this, arguments);
    }
    LogsDrawer.prototype.render = function () {
        var Correlations = React.createFactory(CorrelationList);
        var Application = React.createFactory(ApplicationList);
        var Levels = React.createFactory(EventLevelList);
        return (d.div({}, Correlations(this.props), Application(this.props), Levels(this.props)));
    };
    return LogsDrawer;
})(React.Component);
;
// Init
//LogsActions.getByCorrelationId.onNext("");
LogsActions.getLattestCorrelationIds.onNext("");
var Spinner = React.createFactory(MaterialSpinner);
var App = React.createFactory(LogsApp);
var Drawer = React.createFactory(LogsDrawer);
LogsStore.subscribe(function (value) {
    React.render(App(value), document.getElementById('app'));
    React.render(Drawer(value), document.getElementById('drawer'));
}, function (error) {
    console.log(error);
});
Loading.subscribe(function (value) {
    React.render(Spinner({ isActive: value, isUpgraded: true }), document.getElementById('loading'));
}, function (error) {
    console.log(error);
});
//# sourceMappingURL=Logs.js.map