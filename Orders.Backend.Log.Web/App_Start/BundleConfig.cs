﻿using System.Web;
using System.Web.Optimization;

namespace Orders.Backend.Log.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/react/react-with-addons.js",
                "~/Scripts/rx/rx.lite.js",
                "~/Scripts/mdl/material.js",
                "~/Scripts/react-material.js",
                "~/Scripts/ts/Logs.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/logs-filters.css"
            ));

        }
    }
}
