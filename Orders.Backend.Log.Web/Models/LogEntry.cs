﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Log.Web.Models
{
    public class LogEntry
    {
        public long Id { get; set; }

        public DateTime EventDateTime { get; set; }

        public string EventLevel { get; set; }

        public string UserName { get; set; }

        public string MachineName { get; set; }

        public string ApplicationName { get; set; }

        public string Message { get; set; }

        public string CorrelationId { get; set; }

        public string Exception { get; set; }
    }
}

