﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Log.Web.Models
{
    public class CorrelationEntry
    {
        public string CorrelationId { get; set; }

        public DateTime LastEventDateTime { get; set; }

        public long EventCount { get; set; }
    }
}