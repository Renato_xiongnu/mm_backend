using System.ServiceModel;
using MMS.Cloud.Commands.LIPS;
using MMS.Cloud.Shared.Commands;

namespace Orders.Backend.Services.LipsMock
{
    [ServiceContract]
    public interface ILipsMockAutoTestHelper
    {
        [OperationContract]
        Key SendCommandDeliverOrderProcessingInfo(
            string customerOrder,
            string status,
            string statusReason,
            MMS.Cloud.Commands.FORD.Incident[] incidents);

        [OperationContract]
        void SendCommandResponseDeliverOrder(string customerOrder, Result result, Incident[] incidents);

        [OperationContract]
        void SendCommandResponseCancelDelivery(string customerOrder, Result result, string message);
    }
}