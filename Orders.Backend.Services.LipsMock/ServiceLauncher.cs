﻿using System;
using Autofac;
using Autofac.Configuration;
using Common.Logging;
using MMS.Cloud.Impl.Clients.Commands.WcfService;
using MMS.Cloud.Interfaces.Clients.Commands.Storage;
using MMS.Shared;

namespace Orders.Backend.Services.LipsMock
{
    static class ServiceLauncher
    {
        private static ILog _logger; 
        static void Main(string[] args)
        {
            _logger = LogManager.GetLogger("Orders.Backend.Services.LipsMock");
            _logger.Info("Application started");
            var builder = new ContainerBuilder();
            var module = new ConfigurationSettingsReader("autofac");
            builder.RegisterModule(module);
            builder.RegisterInstance(_logger).As<ILog>();
            var container = builder.Build();            
            var clientStorage = container.Resolve<IClientCommandStorage>();
            var clientSender = ClientCommandServiceFactory.CreateFromConfiguration<ClientCommandService>(clientStorage);            
            clientSender.ProcessingInterval = TimeSpan.FromSeconds(20);                        
#if DEBUG
            DebugHelper.RunServiceAsWinForm(clientSender);
#else
            DebugHelper.RunServiceAsWinForm(clientSender);
            //DebugHelper.RunService(clientSender);
#endif        
        }
    }
}
