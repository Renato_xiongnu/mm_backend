﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Common.Logging;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Commands.LIPS;
using MMS.Cloud.Impl.Clients.Commands.WcfService;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.Cloud.Shared.Dto.Commands;
using MMS.Cloud.Shared.Serialization;
using Incident = MMS.Cloud.Commands.FORD.Incident;

namespace Orders.Backend.Services.LipsMock
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ClientCommandService : SimplifiedClientCommandService, ILipsMockAutoTestHelper
    {
        private static readonly Dictionary<string, ReceiverCommand> ReceiverCommands =
            new Dictionary<string, ReceiverCommand>();

        private static readonly Dictionary<string, Tuple<string, ReceiverCommand, DeliverOrder>> DeliverOrders =
            new Dictionary<string, Tuple<string, ReceiverCommand, DeliverOrder>>();

        public ClientCommandService()
        {
            LogManager.GetLogger("Orders.Backend.Services.LipsMock").Info("TestLog ClientCommandService Ctr");
        }
        
        protected override ReceivedObjectProcessingResult ProcessExternalCommand(ReceiverCommand command, int attempts)
        {
            ServiceLogger.Info("ProcessExternalCommand");
            ReceivedObjectProcessingResult result;
            var commandName = command.CommandName;
            ServiceLogger.InfoFormat("{0} : Begin ProcessExternalCommand. CreatorSystemId = {1},  CommandName = {2}, RelatedEntityId = {3}",
                command.CommandKey,
                command.CreatorSystemId,
                command.CommandName,
                command.RelatedEntityId);
            switch (commandName)
            {
                case "DeliverOrder":
                    result = ProcessExternalCommandDeliveryOrder(command);
                    break;
                case "CancelDelivery":
                    result = ProcessExternalCommandCancelDelivery(command);
                    break;
                default:
                    ServiceLogger.InfoFormat("TestLog {0} : ArgumentOutOfRangeException", commandName);
                    throw new ArgumentOutOfRangeException("commandName");
                    break;
            }
            ServiceLogger.InfoFormat("{0} : Result ProcessExternalCommand. Status = {1}, Error = {2}, Result = {3} ",
                command.CommandKey,
                result.Status,
                result.Error,
                ObjectSerializator.SerialazeObject(result.Result));
            return result;
        }

        protected override ReceivedObjectProcessingResult ProcessExternalResponse(ReceiverCommandResponse response, int attempts)
        {
            ServiceLogger.Info("ProcessExternalResponse");
            return ReceivedObjectProcessingResult.Success(null);
        }

        protected override ReceivedObjectProcessingResult ValidateExternalCommandInProcess(ReceiverCommand command)
        {
            ServiceLogger.Info("ValidateExternalCommandInProcess");
            return ReceivedObjectProcessingResult.InProcess;
        }

        protected override ReceivedObjectProcessingResult ValidateExternalResponseInProcess(
            ReceiverCommandResponse response)
        {
            ServiceLogger.Info("ValidateExternalResponseInProcess");
            return ReceivedObjectProcessingResult.InProcess;
        }

        protected override void ProcessOwnCommandError(SenderCommand targetCommand, CommandErrorData commandErrorData)
        {
            var info = ObjectSerializator.SerialazeObject(new
            {
                TargetCommand = targetCommand,
                CommandErrorData = commandErrorData
            });
            ServiceLogger.ErrorFormat("ProcessOwnCommandError: {0}", info);
        }

        protected override void ProcessOwnResponseError(SenderCommandResponse targetResponse,
            ResponseErrorData responseErrorData)
        {
            var info = ObjectSerializator.SerialazeObject(new
            {
                TargetResponse = targetResponse,
                ResponseErrorData = responseErrorData
            });
            ServiceLogger.ErrorFormat("ProcessOwnResponseError: {0}", info);
        }

        protected override ReceivedObjectProcessingResult ProcessAutoResponse(ReceiverCommandResponse response, AutoResponseReason reason,
            int attempts)
        {
            throw new NotImplementedException();
        }

        private ReceivedObjectProcessingResult ProcessExternalCommandDeliveryOrder(ReceiverCommand command)
        {
            var deliverOrder = ObjectSerializator.DeserialazeObject<DeliverOrder>(command.SerializedData);
            DeliverOrders[deliverOrder.CustomerOrder] =
                new Tuple<string, ReceiverCommand, DeliverOrder>(Guid.NewGuid().ToString(), command, deliverOrder);
            ServiceLogger.InfoFormat("ProcessExternalCommandDeliveryOrder: DeliverOrder {0} saved", deliverOrder.CustomerOrder);
            return ReceivedObjectProcessingResult.InProcess;
        }

        private ReceivedObjectProcessingResult ProcessExternalCommandCancelDelivery(ReceiverCommand command)
        {
            try
            {
                var cancelDelivery = ObjectSerializator.DeserialazeObject<CancelDelivery>(command.SerializedData);
                if (!DeliverOrders.ContainsKey(cancelDelivery.CustomerOrder))
                {
                    throw new ApplicationException(string.Format("DeliverOrder {0} not found", cancelDelivery.CustomerOrder));
                }
                if (!string.IsNullOrEmpty(cancelDelivery.LogisticRequestId) &&
                   DeliverOrders[cancelDelivery.CustomerOrder].Item1 != cancelDelivery.LogisticRequestId)
                {
                    throw new ApplicationException(string.Format("DeliverOrder {0}: LogisticRequestId {1} not found",
                        cancelDelivery.CustomerOrder, cancelDelivery.LogisticRequestId));
                }
                return ReceivedObjectProcessingResult.InProcess;
            }
            catch (Exception ex )
            {
                ServiceLogger.ErrorFormat("ProcessExternalCommandCancelDelivery : {0}",ex, command.CommandKey);
                throw;
            }
        }

        public Key SendCommandDeliverOrderProcessingInfo(
            string customerOrder,
            string status,
            string statusReason,
            Incident[] incidents)
        {
            try
            {
                if (!DeliverOrders.ContainsKey(customerOrder))
                {
                    throw new ApplicationException(string.Format("DeliverOrder {0} not found", customerOrder));
                }
                var logisticRequestId = DeliverOrders[customerOrder].Item1;
                var receiverCommand = DeliverOrders[customerOrder].Item2;
                var info = new DeliverOrderProcessingInfo
                {
                    CustomerOrder = customerOrder,
                    LogisticRequestId = logisticRequestId,
                    Status = status,
                    StatusReason = statusReason,
                    StatusDateTime = DateTime.Now,
                    Incidents = incidents
                };
                var command = ClientCommandManager.CreateCommand("DeliverOrderProcessingInfo", info, info.CustomerOrder,
                    parentCommandKey: receiverCommand.CommandKey);
                return command.CommandKey;
            }
            catch (Exception ex)
            {
                ServiceLogger.ErrorFormat("SendCommandDeliverOrderProcessingInfo : {0}",ex, customerOrder);
                throw;
            }                        
        }

        public void SendCommandResponseDeliverOrder(
            string customerOrder,
            Result result,
            MMS.Cloud.Commands.LIPS.Incident[] incidents
            )
        {
            try
            {
                if (!DeliverOrders.ContainsKey(customerOrder))
                {
                    throw new ApplicationException(string.Format("DeliverOrder {0} not found", customerOrder));
                }
                var logisticRequestId = DeliverOrders[customerOrder].Item1;
                var receiverCommand = DeliverOrders[customerOrder].Item2;
                var deliverOrder = DeliverOrders[customerOrder].Item3;
                var response = new DeliverOrderResponse
                {
                    LogisticRequestId = logisticRequestId,
                    Result = result,
                    Incidents = incidents
                };
                ClientCommandManager.CreateResponse(receiverCommand.CommandKey, response, deliverOrder.CustomerOrder);
            }
            catch (Exception ex)
            {

                ServiceLogger.ErrorFormat("SendCommandResponseDeliverOrder : {0}", ex, customerOrder);
                throw;
            }
        }

        public void SendCommandResponseCancelDelivery(
            string customerOrder,
            Result result,
            string message
            )
        {
            try
            {
                if (!DeliverOrders.ContainsKey(customerOrder))
                {
                    throw new ApplicationException(string.Format("DeliverOrder {0} not found", customerOrder));
                }
                var receiverCommand = DeliverOrders[customerOrder].Item2;
                var deliverOrder = DeliverOrders[customerOrder].Item3;
                var response = new CancelDeliveryResponse
                {
                    Result = result,
                    Message = message
                };
                ClientCommandManager.CreateResponse(receiverCommand.CommandKey, response, deliverOrder.CustomerOrder);
            }
            catch (Exception ex)
            {

                ServiceLogger.ErrorFormat("SendCommandResponseDeliverOrder : {0}",ex, customerOrder);
                throw;
            }
        }
    }

    public class DeliverOrderLog
    {
        public string CustomerOrder { get; set; }

        public string LogisticRequestId { get; set; }

        public DeliverOrder DeliverOrder { get; set; }

        public List<ICommandBase> CommandLog { get; private set; }
    }
}