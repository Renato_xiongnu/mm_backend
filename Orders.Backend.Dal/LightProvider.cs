﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using Order.Backend.Dal.Common;
using Orders.Backend.Dal.Entities;

namespace Orders.Backend.Dal
{

    [Flags]
    public enum RelatedEntity
    {
        None = 0x0,
        OrderLines = 0x1,
        Customer = 0x2,
        GiftCert = 0x4,
        Delivery = 0x8,
        Installation = 0x16,
        PickupLocation = 0x32
    }


    public sealed class LightProvider : IDisposable
    {
        public static readonly DateTime MinDbDateTime = new DateTime(1900, 1, 1);

        private OnlineOrdersContainer _ordersContext { get; set; }
        private bool _forceOpenConnection;

        public ObjectSet<OrderHeader> OrderHeader { get { return _ordersContext.OrderHeader; } }
        public ObjectSet<AvitoOrders> AvitoOrders { get { return _ordersContext.AvitoOrders; } }

        public ObjectSet<ReserveHeader> ReserveHeader { get { return _ordersContext.ReserveHeader; } }

        public ObjectSet<ReserveLine> ReserveLine { get { return _ordersContext.ReserveLine; } }

        public ObjectSet<PickupLocation> PickupLocation { get { return _ordersContext.PickupLocation; } }

        public ObjectSet<Customer> Customer { get { return _ordersContext.Customers; } }
        public ObjectSet<OrderLine> OrderLine { get { return _ordersContext.OrderLines; } }

        public ObjectSet<GiftCertificate> GiftCertificates { get { return _ordersContext.GiftCertificates; } }

        public ObjectSet<GiftCertificateList> GiftCertificateLists { get { return _ordersContext.GiftCertificateLists; } }

        public ObjectSet<Store> Stores { get { return _ordersContext.Stores; } }

        public ObjectSet<OrderError> OrderErrors { get { return _ordersContext.OrderErrors; } }

        public ObjectSet<SysMailerSetting> SysMailerSettings { get { return _ordersContext.SysMailerSettings; } }

        public ObjectSet<ReserveContent> ReserveContent { get { return _ordersContext.ReserveContent; } }

        public ObjectSet<ReserveCustInfo> ReserveCusts { get { return _ordersContext.ReserveCustInfo; } }

        public ObjectSet<CouponsUseQueue> CouponsUseQueue { get { return _ordersContext.CouponsUseQueue; } }

        public ObjectSet<InstallationCreationQueue> InstallationCreationQueue { get { return _ordersContext.InstallationCreationQueue; } }

        public static TimeSpan GetTimeSpan(DateTime dateTime)
        {
            return dateTime - MinDbDateTime;
        }

        public static DateTime? GetValidDbDateTime(DateTime? dateTime)
        {
            DateTime? validDbDateTime = null;
            if (dateTime != null)
            {
                validDbDateTime = dateTime < MinDbDateTime ? null : dateTime;
            }

            return validDbDateTime;
        }
        
        private void ThrowIfDisposed()
        {
            if (_ordersContext == null)
                throw new ObjectDisposedException(this.GetType().Name);
        }

        private ObjectQuery<OrderHeader> PrepairOrderQuery(RelatedEntity relEntity)
        {
            ObjectQuery<OrderHeader> query = OrderHeader;
  
            if ((relEntity & RelatedEntity.OrderLines) == RelatedEntity.OrderLines)
            {
                query = query.Include("OrderLines");
                query = query.Include("ReserveHeaders.ReserveLine");
            }

            if ((relEntity & RelatedEntity.Customer) == RelatedEntity.Customer)
                query = query.Include("Customer");

            if ((relEntity & RelatedEntity.GiftCert) == RelatedEntity.GiftCert)
                query = query.Include("GiftCertificate");

            if ((relEntity & RelatedEntity.Delivery) == RelatedEntity.Delivery)
                query = query.Include("Deliveries");

            if ((relEntity & RelatedEntity.Installation) == RelatedEntity.Installation)
                query = query.Include("Installation");

            if ((relEntity & RelatedEntity.PickupLocation) == RelatedEntity.PickupLocation)
                query = query.Include("PickupLocation");            
            return query;
        }

        private ObjectQuery<OrderHeader> PrepairOrderQuery(bool includeLines, bool includeCustomer, bool includeGiftCert)
        {
            ObjectQuery<OrderHeader> query = OrderHeader;

            if (includeLines)
                query = query.Include("OrderLines");

            if (includeCustomer)
                query = query.Include("Customer");

            if (includeGiftCert)
                query = query.Include("GiftCertificate");

            return query;
        }

        public void Save()
        {
            ThrowIfDisposed();
            _ordersContext.SaveChanges();            
        }

        public LightProvider(bool forceOpenConnection = false)
        {
            _forceOpenConnection = forceOpenConnection;
            _ordersContext = new OnlineOrdersContainer();

            if (_forceOpenConnection)
                _ordersContext.Connection.Open();
        }

        public LightProvider(OnlineOrdersContainer container, bool forceOpenConnection = false)
        {
            _forceOpenConnection = forceOpenConnection;
            _ordersContext = container;

            if (_forceOpenConnection)
                _ordersContext.Connection.Open();
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            if (_forceOpenConnection)
                _ordersContext.Connection.Close();

            _ordersContext.Dispose();
            _ordersContext = null;
        }

        public string ReserveNextGiftCertNumber(out string pinCode, out string barcodeValue)
        {
            ObjectParameter certIdRes = new ObjectParameter("GiftCertId", typeof(string));

            ObjectParameter pinCodeRes = new ObjectParameter("PinCode", typeof(string));

            ObjectParameter barcodeValueRes = new ObjectParameter("BarcodeValue", typeof(string));

            _ordersContext.ReserveNextGiftCertNumber(certIdRes, pinCodeRes, barcodeValueRes);
            pinCode = pinCodeRes.Value.ToString();
            barcodeValue = barcodeValueRes.Value.ToString();

            return certIdRes.Value.ToString();
        }

        public bool OrderExists(string orderId)
        {
            ThrowIfDisposed();
            return OrderHeader.Where(t => t.OrderId == orderId).Select(t => t.OrderId).SingleOrDefault() != null;
        }

        public IQueryable<AvitoOrders> GetAvitoOrders(RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return AvitoOrders;
        }

        public IQueryable<OrderHeader> GetOrderDataByType(short systemType, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).Where(t => t.ExternalSystem == systemType);
        }


        public OrderHeader GetOrderDataByOrderId(string orderId, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).SingleOrDefault(t => t.OrderId == orderId);
        }

        public IQueryable<OrderHeader> GetOrderDataByOrderId(ICollection<string> orderIds, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).Where(t => orderIds.Contains(t.OrderId));
        }

        public OrderHeader GetOrderDataByWWSOrderId(string sapCode, string wwsOrderId, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).SingleOrDefault(t => t.SapCode == sapCode && t.WWSOrderId == wwsOrderId);
        }

        public OrderHeader GetOrderDataByCertificateId(string certificateId, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).SingleOrDefault(t => t.GiftCertificateId == certificateId);
        }

        public IQueryable<OrderHeader> GetOrderDataByCertificates(ICollection<string> certificates, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).Where(t => certificates.Contains(t.GiftCertificateId));
        }


        public IQueryable<OrderHeader> GetOrdersByStatus(short orderStatus, RelatedEntity relEntity = RelatedEntity.None)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relEntity).Where(t => t.OrderStatus == orderStatus);
        }

        public ReserveContent GetReserveContent(string orderId, string wwsOrderId)
        {
            ThrowIfDisposed();
            return _ordersContext.ReserveContent.SingleOrDefault(t => t.OrderId == orderId && t.WWSOrderId == wwsOrderId);
        }


        public ReserveCustInfo GetReserveCust(long reserveCustId)
        {
            ThrowIfDisposed();
            return _ordersContext.ReserveCustInfo.SingleOrDefault(t => t.ReserveCustInfoId == reserveCustId);
        }

        public ReserveCustInfo GetReserveCust(string email, string name, string surname)
        {
            ThrowIfDisposed();
            //TODO Было SingleOrDefault и падало, так как связка Email,Name,Surname неуникальны
            return _ordersContext.ReserveCustInfo.FirstOrDefault(t => t.Email == email && t.Name == name && t.Surname == surname);
        }

        public Store GetStore(string sapCode)
        {
            ThrowIfDisposed();
            return _ordersContext.Stores.SingleOrDefault(t => t.SapCode == sapCode);
        }

        public DateTime GetLastReserveCreationTime()
        {
            ThrowIfDisposed();
            return _ordersContext.GetLastReserveCreationTime().First().Value;
        }

        public OrderStatusHistory[] GetOrderStatusHistoryByOrderId(string orderId)
        {
            return _ordersContext
                .OrderStatusHistories
                .Where(osh => osh.OrderId == orderId)
                .ToArray();
        }
       public PickupLocation GetPickupLocation(string pickupLocationId)
        {
            ThrowIfDisposed();
            return _ordersContext.PickupLocation.SingleOrDefault(t => t.PickupLocationId == pickupLocationId);
        } 
       
        public OrderHeader GetOrderDataByBasketId(string basket3Id, RelatedEntity relatedEntity)
        {
            ThrowIfDisposed();
            return PrepairOrderQuery(relatedEntity).SingleOrDefault(t => t.BasketId == basket3Id);
        }

        public IEnumerable<StockLocationOrderCount> GetOrderCountInStockBySaleLocationAndStockLoations(String saleLocationSapCode, IEnumerable<String> stockLocationSapCodes, Int32 daysRange, RelatedEntity relatedEntity = RelatedEntity.Delivery)
        {
            ThrowIfDisposed();
            var date = DateTime.Now.AddDays(-daysRange);
            return PrepairOrderQuery(relatedEntity)
                                        .Where(x =>
                                               (x.SaleLocationSapCode != null && x.SaleLocationSapCode.Equals(saleLocationSapCode, StringComparison.InvariantCultureIgnoreCase))
                                               && (x.SapCode != null && stockLocationSapCodes.Any(s => s != null && s.Equals(x.SapCode, StringComparison.InvariantCultureIgnoreCase)))
                                               && x.UpdateDate >= date
                                               && (x.OrderStatus == (Int16)InternalOrderStatus.Created || x.OrderStatus == (Int16)InternalOrderStatus.Confirmed)
                                               && x.Deliveries.Any(d => d.OrderId != null && d.OrderId.Equals(x.OrderId, StringComparison.InvariantCultureIgnoreCase)))
                                            .GroupBy(x => x.SapCode)
                                            .Select(item => new StockLocationOrderCount { StockLocationSapCode = item.Key, OrdersCount = item.Count() }).ToList();

        }

      
     }
}
