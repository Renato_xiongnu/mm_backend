﻿namespace Orders.Backend.Dal.Entities
{
    public enum InstallationCreationOperationType
    {
        Create = 1,
        Cancel = 2,
    }
}