﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.Helpers;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Dal
{
    public partial class ReserveLine
    {
        [DataMember]
        public LineType LineTypeEnum
        {
            get { return LineType.ToEnum<LineType>(); }
        }

        [DataMember]
        public StockItemState StockItemStateEnum
        {
            get
            {
                return StockItemState.HasValue
                           ? StockItemState.Value.ToEnum<StockItemState>()
                           : Order.Backend.Dal.Common.StockItemState.New;
            }
        }

        [DataMember]
        public ReviewItemState ReviewItemStateEnum
        {
            get { return ItemState.ToEnum<ReviewItemState>(); }
        }

        [DataMember]
        public string LineTypeEnumStr
        {
            get { return LineTypeEnum.GetDescription(); }
        }

        [DataMember]
        public string StockItemStateEnumStr
        {
            get { return StockItemStateEnum.GetDescription(); }
        }

        [DataMember]
        public string ReviewItemStateEnumStr
        {
            get { return ReviewItemStateEnum.GetDescription(); }
        }

        [DataMember]
        public string ArticleProductTypeEnumStr
        {
            get { return ArticleProductType.ToEnum<ArticleProductType>().ToString(); }
        }
    }
}
