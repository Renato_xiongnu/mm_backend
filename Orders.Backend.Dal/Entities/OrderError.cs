﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Common.Helpers;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Dal
{
    public partial class OrderError
    {
        [DataMember]
        public InternalOrderStatus OrderStatusEnum
        {
            get { return (InternalOrderStatus)OrderStatus; }
        }

        [DataMember]
        public string OrderStatusEnumStr
        {
            get { return OrderStatusEnum.GetDescription(); }
        }
    }
}
