﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.Helpers;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Dal
{
    public partial class OrderStatusHistory
    {
        [DataMember]
        public InternalOrderStatus FromStatusEnum
        {
            get { return FromStatus.ToEnum<InternalOrderStatus>(); }
        }

        [DataMember]
        public InternalOrderStatus ToStatusEnum
        {
            get { return ToStatus.ToEnum<InternalOrderStatus>(); }
        }

        [DataMember]
        public ChangedType ChangedTypeEnum
        {
            get { return ChangedType.ToEnum<ChangedType>(); }
        }


        private DateTime _updateDateTime;
        [DataMember]
        public DateTime UpdateDateTime
        {
            get
            {
                return _updateDateTime != DateTime.MinValue
                    ? _updateDateTime
                    : new DateTime(UpdateDate.Year, UpdateDate.Month, UpdateDate.Day, UpdateDate.Hour, UpdateDate.Minute,
                        UpdateDate.Second);
            }
            set { _updateDateTime = value; }
        }
    }
}
