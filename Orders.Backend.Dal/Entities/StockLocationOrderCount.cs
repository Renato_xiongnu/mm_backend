﻿using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Dal.Entities
{
    [DataContract]
    public class StockLocationOrderCount
    {
        [DataMember]
        public String StockLocationSapCode { get; set; }

        [DataMember]
        public Int32? OrdersCount { get; set; }
    }
}
