﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Common.Helpers;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Dal
{
    public partial class ReserveHeader
    {
        [DataMember]
        public ReserveStatus ReserveStatusEnum
        {
            get { return ReserveStatus.ToEnum<ReserveStatus>(); }
        }   
    }
}
