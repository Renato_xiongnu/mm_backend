﻿namespace Orders.Backend.Dal.Entities
{
    public enum CouponsUseQueueOperationType
    {
        UseCoupon = 1,
        UnfreezCoupon = 2,
    }
}
