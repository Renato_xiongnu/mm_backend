﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Common.Helpers;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Dal
{
    public partial class OrderLine
    {
        [DataMember]
        public LineType LineTypeEnum
        {
            get { return LineType.ToEnum<LineType>(); }
        }

        [DataMember]
        public string LineTypeEnumStr
        {
            get { return LineTypeEnum.GetDescription(); }
        }       
    }
}
