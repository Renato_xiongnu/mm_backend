﻿BEGIN TRANSACTION;

BEGIN TRY
--	ALTER TABLE [dbo].[Store] ADD IsHub BIT NOT NULL DEFAULT(0);
	
	ALTER TABLE [dbo].[Delivery] ADD ZIPCode NVARCHAR(64) NULL;
	ALTER TABLE [dbo].[Delivery] ADD Region NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD StreetAbbr NVARCHAR(64) NULL;
	ALTER TABLE [dbo].[Delivery] ADD Street NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD House NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD Housing NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD Building NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD Apartment NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD Entrance NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD EntranceCode NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD [Floor] int NULL;
	ALTER TABLE [dbo].[Delivery] ADD [SubwayStationName] NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD [District] NVARCHAR(255) NULL;
	ALTER TABLE [dbo].[Delivery] ADD [HasServiceLift] bit NULL;
	ALTER TABLE [dbo].[Delivery] ADD RequestedDeliveryTimeslot NVARCHAR(255) NULL
	ALTER TABLE [dbo].[Delivery] ADD Kladr NVARCHAR(255) NULL
	ALTER TABLE [dbo].[OrderHeader] ADD Created datetime2(7) NOT NULL DEFAULT (GETUTCDATE())
	--UPDATE [dbo].[OrderHeader] SET [Created] = UpdateDate;
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO


--BEGIN TRANSACTION
--ALTER TABLE [dbo].[Store] ADD IsHub BIT NOT NULL DEFAULT(0)
--GO
--ALTER TABLE [dbo].[Delivery] ADD ZIPCode NVARCHAR(64) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Region NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD StreetAbbr NVARCHAR(64) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Street NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD House NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Housing NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Building NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Apartment NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Entrance NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD EntranceCode NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD [Floor] int NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD [SubwayStationName] NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD [District] NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD [HasServiceLift] bit NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD RequestedDeliveryTimeslot NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[Delivery] ADD Kladr NVARCHAR(255) NULL
--GO
--ALTER TABLE [dbo].[OrderHeader] ADD Created datetime2(7) NOT NULL DEFAULT (GETUTCDATE())
--GO
--UPDATE [dbo].[OrderHeader] SET Created = UpdateDate

--COMMIT