﻿ALTER TABLE [dbo].[OrderHeader] ADD BasketId NVARCHAR(128) NULL
GO
ALTER TABLE [dbo].[ReserveLine] ADD IsVirtual bit NULL
GO
ALTER TABLE [dbo].[OrderLine] ADD IsVirtual bit NULL
GO
ALTER TABLE [dbo].[Customer] ADD UserId NVARCHAR(64) NULL


