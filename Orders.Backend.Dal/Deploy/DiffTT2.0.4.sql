﻿ALTER TABLE [dbo].[Delivery] ADD HasDelivery BIT NULL  
  GO
  UPDATE [dbo].[Delivery] SET HasDelivery = 1
  ALTER TABLE [dbo].[Delivery] ALTER COLUMN HasDelivery BIT NOT NULL  
  ALTER TABLE [dbo].[Delivery] ADD PickupLocationId nvarchar(64) NULL
  GO

  INSERT [dbo].[Delivery] ([CustomerId],[OrderId],[City], [Address], [HasDelivery], [PickupLocationId])
  SELECT oh.[CustomerId], oh.[OrderId], s.City, s.[Address] AS [Address], 0 as [HasDelivery], 'pickup_' + oh.SapCode as [PickupLocationId]
  FROM dbo.OrderHeader as oh
  LEFT JOIN dbo.Delivery as d ON d.OrderId=oh.OrderId
  INNER JOIN dbo.Store AS s ON s.SapCode = oh.SapCode
  WHERE d.OrderId is null 

  ALTER TABLE [dbo].[OrderHeader] ADD UtmSource nvarchar(512) NULL

 -- DROP INDEX IX_Customer ON dbo.Customer