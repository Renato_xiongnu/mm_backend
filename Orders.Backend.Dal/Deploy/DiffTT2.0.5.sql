﻿ALTER TABLE [dbo].[Customer] ADD SocialCardNumber nvarchar(64) NULL

ALTER TABLE [dbo].[OrderHeader] ADD Prepay decimal(18,2) NOT NULL DEFAULT(0)

ALTER TABLE [dbo].[ReserveLine] ADD ArticleProductType smallint NOT NULL DEFAULT(0)

ALTER TABLE [dbo].[Delivery] ADD TransferAgreementWithCustomer BIT NOT NULL DEFAULT(0)

ALTER TABLE [dbo].[Store] ADD IsVirtual BIT NOT NULL DEFAULT(0)
