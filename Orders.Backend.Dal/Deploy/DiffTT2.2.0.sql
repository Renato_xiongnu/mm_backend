﻿ALTER TABLE PickupLocation
ALTER COLUMN [Title] [nvarchar](400) NULL
GO

ALTER TABLE PickupLocation
ADD [City] nvarchar(100) NULL,
    [ZipCode] nvarchar(10) NULL