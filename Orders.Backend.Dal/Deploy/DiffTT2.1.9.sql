﻿ALTER TABLE dbo.OrderHeader
ADD InstallServiceAddress NVARCHAR(4000) NULL
 
GO
ALTER TABLE dbo.OrderLine
ADD	ArticleProductType SMALLINT NULL  

GO
ALTER TABLE dbo.OrderLine
ADD RefersToItem BIGINT NULL

GO
ALTER TABLE dbo.ReserveLine
ADD RefersToItem BIGINT NULL

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InstallationCreationQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [nvarchar](255) NOT NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[TryCount] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[Processed] [bit] NOT NULL,
	[OperationType] [smallint] NOT NULL,
 CONSTRAINT [PK_InstallationCreationQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


