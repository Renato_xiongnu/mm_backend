﻿ALTER TABLE dbo.OrderHeader ADD
	CouponCode nvarchar(250) NULL
GO
ALTER TABLE dbo.OrderLine ADD
	OldPrice money NULL	
GO
ALTER TABLE dbo.ReserveLine ADD
	OldPrice money NULL

ALTER TABLE dbo.OrderLine ADD
	Benefit money NULL,
	CouponCompainId nvarchar(50) NULL,
	CouponCode nvarchar(50) NULL

ALTER TABLE dbo.ReserveLine ADD
	Benefit money NULL,
	CouponCompainId nvarchar(50) NULL,
	CouponCode nvarchar(50) NULL
