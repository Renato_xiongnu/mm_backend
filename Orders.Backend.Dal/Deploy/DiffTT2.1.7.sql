﻿CREATE TABLE [dbo].[CouponsUseQueue](
    [Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [nvarchar](255) NOT NULL,
	[CouponCode] [nvarchar](250) NOT NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[TryCount] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[OperationType] [int] NOT NULL,
	[Processed] [bit] NOT NULL,
	
 CONSTRAINT [PK_CouponsUseQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]