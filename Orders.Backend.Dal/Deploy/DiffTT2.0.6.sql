﻿BEGIN TRANSACTION
GO
CREATE TABLE dbo.Installation
	(
	OrderId nvarchar(255) NOT NULL,
	HasInstallation bit NOT NULL,
	ExpectedInstallation datetime2(7) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Installation ADD CONSTRAINT
	DF_Installation_HasInstallation DEFAULT 0 FOR HasInstallation
GO
ALTER TABLE dbo.Installation ADD CONSTRAINT
	PK_Installation PRIMARY KEY CLUSTERED 
	(
	OrderId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Installation ADD CONSTRAINT
	FK_Installation_OrderHeader FOREIGN KEY
	(
	OrderId
	) REFERENCES dbo.OrderHeader
	(
	OrderId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.Installation SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE [dbo].[Store] ADD IsActive BIT NOT NULL DEFAULT(0)
GO

UPDATE [dbo].[Store] SET IsActive = 1
GO

ALTER TABLE [dbo].[Store] ADD IsHub BIT NOT NULL DEFAULT(0)
GO

COMMIT