﻿
CREATE TABLE [dbo].[PickupLocation](
	[PickupLocationId] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](4000) NOT NULL,
	[PickupPointType] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[Title] [nvarchar](200) NULL,
 CONSTRAINT [PK_PickupLocation] PRIMARY KEY CLUSTERED 
(
	[PickupLocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PickupLocation] ADD  CONSTRAINT [DF_PickupLocation_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[Delivery] 
	ALTER COLUMN [PickupLocationId] [nvarchar](100) NULL
	
GO
ALTER TABLE [dbo].[PickupLocation] ADD  CONSTRAINT [DF_PickupLocation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_PickupLocation]
GO
