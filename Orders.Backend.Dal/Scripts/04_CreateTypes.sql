﻿print 'Create types'
go
USE [OnlineOrders$(country)]
GO
/****** Object:  UserDefinedTableType [dbo].[DateList]    Script Date: 06/19/2012 16:27:00 ******/
CREATE TYPE [dbo].[DateList] AS TABLE(
	[Value] [date] NOT NULL
)
GO
