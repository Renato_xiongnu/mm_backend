﻿print 'insert general data'
USE [OnlineOrders$(country)]
GO

SET IDENTITY_INSERT [dbo].[City] ON
INSERT [dbo].[City] ([CityId], [Name], [ClassificatorId], [CountryId]) VALUES (1, N'Москва', N'45', N'RU')
SET IDENTITY_INSERT [dbo].[City] OFF

GO

USE [OnlineOrders$(country)]
GO
SET IDENTITY_INSERT [dbo].[SysMailerSetting] ON
INSERT [dbo].[SysMailerSetting] ([Id], [SendingType], [Emails]) VALUES (1, N'HQ_ResponsibleForCertificate', N'vasilevig@media-saturn.com;prokofevs@media-saturn.com')
SET IDENTITY_INSERT [dbo].[SysMailerSetting] OFF

