﻿print 'Create Dbo tables'
USE [OnlineOrders$(country)]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Surname] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](150) NOT NULL,
	[Phone2] [nvarchar](50) NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZZTCustomer]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZTCustomer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Birthday] [datetime] NULL,
	[CustomerId] [bigint] NOT NULL,
	[LastName] [nvarchar](100) NULL,
	[Gender] [nvarchar](50) NULL,
	[ZZTCustomerId] [bigint] NOT NULL,
	[Fax] [nvarchar](150) NULL,
 CONSTRAINT [PK_ZZTCustomer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[CityId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ClassificatorId] [nvarchar](50) NOT NULL,
	[CountryId] [nvarchar](4) NOT NULL,
	[TimeZone] [nvarchar](255) NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiftCertificateList]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftCertificateList](
	[GiftCertificateId] [nvarchar](50) NOT NULL,
	[PinCode] [nvarchar](50) NOT NULL,
	[BarcodeValue] [nvarchar](255) NOT NULL,
	[IsReserved] [bit] NOT NULL,
 CONSTRAINT [PK_GiftCertificateList] PRIMARY KEY CLUSTERED 
(
	[GiftCertificateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiftCertificate]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftCertificate](
	[GiftCertificateId] [nvarchar](50) NOT NULL,
	[PinCode] [nvarchar](50) NOT NULL,
	[BarcodeValue] [nvarchar](255) NOT NULL,
	[Status] [smallint] NOT NULL,
	[Amount] [money] NULL,
 CONSTRAINT [PK_GiftCertificate] PRIMARY KEY CLUSTERED 
(
	[GiftCertificateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReserveContent]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReserveContent](
	[OrderId] [nvarchar](255) NOT NULL,
	[WWSOrderId] [nvarchar](50) NOT NULL,
	[Status] [smallint] NOT NULL,
	[DocContent] [nvarchar](max) NOT NULL,
	[MessageText] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReserveContent] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC,
	[WWSOrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysMailerSetting]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysMailerSetting](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SendingType] [nvarchar](50) NOT NULL,
	[Emails] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_SysMailerSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Store]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Store](
	[SapCode] [nchar](4) NOT NULL,
	[ChainType] [nvarchar](50) NOT NULL,
	[City] [nvarchar](255) NOT NULL,
	[Phone] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](4000) NOT NULL,
	[CertificateAmount] [money] NOT NULL,
	[CityId] [bigint] NOT NULL,
	[ReserveLifeTime] [datetime] NULL,
	[StartWorkingTime] [time](7) NULL,
	[EndWorkingTime] [time](7) NULL,
	[Name] [nvarchar](4000) NOT NULL,
	[PaidDeliveryArticleId] int NULL,
	[FreeDeliveryArticleId] int NULL,
	OrderMail nvarchar(MAX) NULL,
	OutOfCatalogEmail nvarchar(MAX) NULL,
	SpecialOfferEmail nvarchar(MAX) NULL
 CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED 
(
	[SapCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReserveCustInfo]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReserveCustInfo](
	[ReserveCustInfoId] [bigint] IDENTITY(1,1) NOT NULL,
	[ClassificationNumber] [nvarchar](255) NULL,
	[Salutation] [nvarchar](255) NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Surname] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](20) NULL,
	[Phone2] [nvarchar](20) NULL,
	[Address] [nvarchar](4000) NULL,
	[CountryAbbreviation] [nvarchar](20) NULL,
	[AddressIndex] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[INN] [nvarchar](50) NULL,
	[KPP] [nvarchar](50) NULL,
	[Email] [nvarchar](150) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
 CONSTRAINT [PK_ReserveCustInfo] PRIMARY KEY CLUSTERED 
(
	[ReserveCustInfoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHeader](
	[OrderId] [nvarchar](255) NOT NULL,
	[WWSOrderId] [nvarchar](50) NULL,
	[SapCode] [nchar](4) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[PaymentType] [smallint] NOT NULL,
	[FinalPaymentType] [smallint] NOT NULL,
	[OrderStatus] [smallint] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[GiftCertificateId] [nvarchar](50) NULL,
	[OrderSource] [nvarchar](255) NOT NULL,
	[IsPreorder] [bit] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[BasketComment] [nvarchar](max) NULL,
	[ExternalNumber] [nvarchar](4000) NULL,
	[ExternalSystem] [smallint] NOT NULL,
	[ExpirationDate] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_OrderHeader] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZZTOrderHeader]]   Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZTOrderHeader](
	[OrderId] [nvarchar](255) NOT NULL,
	[CampaignId] [nvarchar](255) NOT NULL,
	[ConsumerId] [nvarchar](255) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[RecipientName] [nvarchar](255) NULL,
	[ZztOrderId] [nvarchar](255) NULL
 CONSTRAINT [PK_ZZTOrderHeader] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderError]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderError](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [nvarchar](255) NOT NULL,
	[OrderStatus] [smallint] NOT NULL,
	[ErrorText] [nvarchar](4000) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OrderError] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Delivery]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Delivery](
	[CustomerId] [bigint] NOT NULL,
	[OrderId] [nvarchar](255) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](4000) NOT NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryPeriod] [nvarchar](4000) NULL,
	[DeliveryComment] [nvarchar](4000) NULL,
	[ExactAddress] [nvarchar](4000) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
 CONSTRAINT [PK_Delivery_1] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC,
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZZTDeliveryInfo]  Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZTDeliveryInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [nvarchar](255) NOT NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryTime] [nvarchar](255) NULL,
	[DeliveryType] [nvarchar](255) NULL,
	[DeliveryPlaceSapCode] [nvarchar](255) NULL,
	[DeliveryPlaceId] [nvarchar](255) NULL,
	[DeliveryPrice] [money] NULL,
	[ClimbPrice] [money] NULL,
CONSTRAINT [PK_ZZTDeliveryInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZZTDeliveryAddress]  Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZTDeliveryAddress](
	[OrderId] [nvarchar](255) NOT NULL,
	[ZZTCustomerId] [bigint] NOT NULL,
	[PostalCode] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Region] [nvarchar](255) NULL,
	[District] [nvarchar](255) NULL,
	[CityId] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Street] [nvarchar](4000) NULL,
	[House] [nvarchar](50) NULL,
	[Housing] [nvarchar](50) NULL,
	[Building] [nvarchar](50) NULL,
	[Flat] [nvarchar](50) NULL,
	[Floor] [nvarchar](50) NULL,
	[Entrance] [nvarchar](50) NULL,
	[EntranceCode] [nvarchar](50) NULL,
	[HasElevator] [bit] NULL,
	[MetroStation] [nvarchar](255) NULL,
	[Additional] [nvarchar](4000) NULL,
 CONSTRAINT [PK_ZZTDeliveryAddress] PRIMARY KEY CLUSTERED 
(
	[ZZTCustomerId] ASC,
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ActualOrderLine]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActualOrderLine](
	[OrderId] [nvarchar](255) NOT NULL,
	[LineId] [bigint] IDENTITY(1,1) NOT NULL,
	[ArticleNum] [nvarchar](4000) NOT NULL,
	[Qty] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[LineType] [smallint] NOT NULL,
 CONSTRAINT [PK_ActualOrderLine_1] PRIMARY KEY CLUSTERED 
(
	[LineId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatusHistory]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatusHistory](
	[OrderId] [nvarchar](255) NOT NULL,
	[FromStatus] [smallint] NOT NULL,
	[ToStatus] [smallint] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[ChangedType] [smallint] NOT NULL,
	[WhoChanged] [nvarchar](255) NULL,
	[ReasonText] [nvarchar](4000) NULL,
 CONSTRAINT [PK_OrderStatusHistory] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC,
	[FromStatus] ASC,
	[ToStatus] ASC,
	[UpdateDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLine]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLine](
	[OrderId] [nvarchar](255) NOT NULL,
	[LineId] [bigint] IDENTITY(1,1) NOT NULL,
	[ArticleNum] [nvarchar](4000) NOT NULL,
	[Qty] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[LineType] [smallint] NOT NULL,
	[ParentLineId] [bigint] NULL,
	[Promotions] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderLine_1] PRIMARY KEY CLUSTERED 
(
	[LineId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReserveHeader]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReserveHeader](
	[ReserveId] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [nvarchar](255) NOT NULL,
	[ReserveStatus] [smallint] NOT NULL,
	[WWSOrderId] [nvarchar](50) NULL,
	[SapCode] [nchar](4) NOT NULL,
	[IsApproved] [bit] NOT NULL,
	[StoreManagerName] [nvarchar](255) NULL,
	[CreationDate] [datetime] NULL,
	[TotalVat] [int] NULL,
	[TotalNetPrice] [money] NULL,
	[TotalVatPrice] [money] NULL,
	[TotalGrossPrice] [money] NULL,
	[PrepaymentVat] [int] NULL,
	[PrepaymentNetPrice] [money] NULL,
	[PrepaymentVatPrice] [money] NULL,
	[PrepaymentGrossPrice] [money] NULL,
	[LeftoverVat] [int] NULL,
	[LeftoverNetPrice] [money] NULL,
	[LeftoverVatPrice] [money] NULL,
	[LeftoverGrossPrice] [money] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryPeriod] [nvarchar](4000) NULL,
	[DeliveryComment] [nvarchar](4000) NULL,
	[ReserveCustInfoId] [bigint] NULL,
	[OutletInfo] [nvarchar](max) NULL,
	[ProductPickupInfo] [nvarchar](max) NULL,
	[PrintableInfo] [nvarchar](max) NULL,
	[SalesPersonId] [int] NULL,
	[IsOnlineOrder] [bit] NOT NULL,
 CONSTRAINT [PK_ReserveHeader] PRIMARY KEY CLUSTERED 
(
	[ReserveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReserveLine]    Script Date: 06/11/2013 19:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReserveLine](
	[ReserveId] [bigint] NOT NULL,
	[LineId] [bigint] IDENTITY(1,1) NOT NULL,
	[ArticleNum] [nvarchar](4000) NOT NULL,
	[Qty] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[LineType] [smallint] NOT NULL,
	[ItemState] [smallint] NOT NULL,
	[IsOffered] [bit] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[WWSDepartmentNo] [int] NULL,
	[WWSProductGroup] [nvarchar](4000) NULL,
	[WWSProductGroupNo] [int] NULL,
	[WWSStockNo] [int] NULL,
	[WWSFreeQty] [int] NULL,
	[WWSReservedQty] [int] NULL,
	[WWSPriceOrig] [money] NULL,
	[ArticleCondition] [nvarchar](max) NULL,
	[StockItemState] [smallint] NULL,
	[Title] [nvarchar](max) NULL,
	[Promotions] [nvarchar](max) NULL,
	[WWSPositionNumber] [int] NULL,
	[WWSStoreNumber] [int] NULL,
	[WWSVAT] [int] NULL,
	[ProductType] [smallint] NULL,
	[LineNumber] [int] NULL,
	[RefLineNumber] [int] NULL,
	[StockNumner] [int] NULL,
	[HasShippedFromStock] [bit] NULL,
	[SerialNumber] [nvarchar](max) NULL,
	[WI_Number] [nvarchar](max) NULL,
	[WI_DocumentPositionNumber] [int] NULL,
	[WI_RelatedArticleNo] [int] NULL,
	[WI_WarrantySum] [decimal](19, 4) NULL,
	[WI_WwsCertificateState] [nvarchar](max) NULL,
	[WI_WwsExtensionPrint] [nvarchar](max) NULL,
	[ParentLineId] [bigint] NULL,
 CONSTRAINT [PK_ReserveLine] PRIMARY KEY CLUSTERED 
(
	[LineId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Customer_IsDefault]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[Customer] ADD  CONSTRAINT [DF_Customer_IsDefault]  DEFAULT (0) FOR [IsDefault]
GO
/****** Object:  Default [DF_GiftCertificateList_PinCode]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[GiftCertificateList] ADD  CONSTRAINT [DF_GiftCertificateList_PinCode]  DEFAULT ('') FOR [PinCode]
GO
/****** Object:  Default [DF_OrderHeader_ExternalSystem]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderHeader] ADD  CONSTRAINT [DF_OrderHeader_ExternalSystem]  DEFAULT (0) FOR [ExternalSystem]
GO
/****** Object:  ForeignKey [FK_ActualOrderLine_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ActualOrderLine]  WITH CHECK ADD  CONSTRAINT [FK_ActualOrderLine_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[ActualOrderLine] CHECK CONSTRAINT [FK_ActualOrderLine_OrderHeader]
GO
/****** Object:  ForeignKey [FK_ZZTDeliveryAddress_ZZTOrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ZZTDeliveryAddress]  WITH CHECK ADD  CONSTRAINT [FK_ZZTDeliveryAddress_ZZTOrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ZZTOrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[ZZTDeliveryAddress] CHECK CONSTRAINT [FK_ZZTDeliveryAddress_ZZTOrderHeader]
GO
/****** Object:  ForeignKey [FK_ZZTDeliveryInfo_ZZTOrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ZZTDeliveryInfo]  WITH CHECK ADD  CONSTRAINT [FK_ZZTDeliveryInfo_ZZTOrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ZZTOrderHeader] ([OrderId])
GO
/****** Object:  ForeignKey [FK_Delivery_Customer]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_Customer]
GO
/****** Object:  ForeignKey [FK_Delivery_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[Delivery]  WITH CHECK ADD  CONSTRAINT [FK_Delivery_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[Delivery] CHECK CONSTRAINT [FK_Delivery_OrderHeader]
GO
/****** Object:  ForeignKey [FK_OrderError_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderError]  WITH CHECK ADD  CONSTRAINT [FK_OrderError_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[OrderError] CHECK CONSTRAINT [FK_OrderError_OrderHeader]
GO
/****** Object:  ForeignKey [FK_ZZTOrderHeader_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ZZTOrderHeader]  WITH CHECK ADD  CONSTRAINT [FK_ZZTOrderHeader_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[ZZTOrderHeader] CHECK CONSTRAINT [FK_ZZTOrderHeader_OrderHeader]
GO
/****** Object:  ForeignKey [FK_ZZTOrderHeader_ZZTCustomer]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ZZTOrderHeader]  WITH CHECK ADD  CONSTRAINT [FK_ZZTOrderHeader_ZZTCustomer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[ZZTCustomer] ([Id])
GO
ALTER TABLE [dbo].[ZZTOrderHeader] CHECK CONSTRAINT [FK_ZZTOrderHeader_ZZTCustomer]
GO
/****** Object:  ForeignKey [FK_ZZTCustomer_Customer]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ZZTCustomer]  WITH CHECK ADD  CONSTRAINT [FK_ZZTCustomer_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[ZZTCustomer] CHECK CONSTRAINT [FK_ZZTCustomer_Customer]
GO
/****** Object:  ForeignKey [FK_OrderHeader_Customer]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderHeader]  WITH CHECK ADD  CONSTRAINT [FK_OrderHeader_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[OrderHeader] CHECK CONSTRAINT [FK_OrderHeader_Customer]
GO
/****** Object:  ForeignKey [FK_OrderHeader_GiftCertificate]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderHeader]  WITH CHECK ADD  CONSTRAINT [FK_OrderHeader_GiftCertificate] FOREIGN KEY([GiftCertificateId])
REFERENCES [dbo].[GiftCertificate] ([GiftCertificateId])
GO
ALTER TABLE [dbo].[OrderHeader] CHECK CONSTRAINT [FK_OrderHeader_GiftCertificate]
GO
/****** Object:  ForeignKey [FK_OrderLine_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderLine]  WITH CHECK ADD  CONSTRAINT [FK_OrderLine_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[OrderLine] CHECK CONSTRAINT [FK_OrderLine_OrderHeader]
GO
/****** Object:  ForeignKey [FK_OrderLine_OrderLine]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderLine]  WITH CHECK ADD  CONSTRAINT [FK_OrderLine_OrderLine] FOREIGN KEY([ParentLineId])
REFERENCES [dbo].[OrderLine] ([LineId])
GO
ALTER TABLE [dbo].[OrderLine] CHECK CONSTRAINT [FK_OrderLine_OrderLine]
GO
/****** Object:  ForeignKey [FK_OrderStatusHistory_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[OrderStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderStatusHistory_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[OrderStatusHistory] CHECK CONSTRAINT [FK_OrderStatusHistory_OrderHeader]
GO
/****** Object:  ForeignKey [FK_ReserveCustInfo_Customer]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ReserveCustInfo]  WITH CHECK ADD  CONSTRAINT [FK_ReserveCustInfo_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[ReserveCustInfo] CHECK CONSTRAINT [FK_ReserveCustInfo_Customer]
GO
/****** Object:  ForeignKey [FK_ReserveHeader_OrderHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ReserveHeader]  WITH CHECK ADD  CONSTRAINT [FK_ReserveHeader_OrderHeader] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeader] ([OrderId])
GO
ALTER TABLE [dbo].[ReserveHeader] CHECK CONSTRAINT [FK_ReserveHeader_OrderHeader]
GO
/****** Object:  ForeignKey [FK_ReserveHeader_ReserveCustInfo]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ReserveHeader]  WITH CHECK ADD  CONSTRAINT [FK_ReserveHeader_ReserveCustInfo] FOREIGN KEY([ReserveCustInfoId])
REFERENCES [dbo].[ReserveCustInfo] ([ReserveCustInfoId])
GO
ALTER TABLE [dbo].[ReserveHeader] CHECK CONSTRAINT [FK_ReserveHeader_ReserveCustInfo]
GO
/****** Object:  ForeignKey [FK_ReserveLine_ReserveHeader]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ReserveLine]  WITH CHECK ADD  CONSTRAINT [FK_ReserveLine_ReserveHeader] FOREIGN KEY([ReserveId])
REFERENCES [dbo].[ReserveHeader] ([ReserveId])
GO
ALTER TABLE [dbo].[ReserveLine] CHECK CONSTRAINT [FK_ReserveLine_ReserveHeader]
GO
/****** Object:  ForeignKey [FK_ReserveLine_ReserveLine]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[ReserveLine]  WITH CHECK ADD  CONSTRAINT [FK_ReserveLine_ParentReserveLine] FOREIGN KEY([ParentLineId])
REFERENCES [dbo].[ReserveLine] ([LineId])
GO
ALTER TABLE [dbo].[ReserveLine] CHECK CONSTRAINT [FK_ReserveLine_ParentReserveLine]
GO
/****** Object:  ForeignKey [FK_Store_City]    Script Date: 06/11/2013 19:16:59 ******/
ALTER TABLE [dbo].[Store]  WITH CHECK ADD  CONSTRAINT [FK_Store_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO
ALTER TABLE [dbo].[Store] CHECK CONSTRAINT [FK_Store_City]
GO
