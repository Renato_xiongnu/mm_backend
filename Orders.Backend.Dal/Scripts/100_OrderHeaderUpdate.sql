﻿alter table OrderHeader
add [TimeStamp] timestamp null

go

alter table OrderHeader
add Sequence as (CONVERT([bigint],[TimeStamp],0))

go

CREATE NONCLUSTERED INDEX [UI_TimeStamp] ON [dbo].[OrderHeader] 
(
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

go