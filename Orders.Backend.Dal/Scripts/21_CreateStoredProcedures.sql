﻿print 'Create stored procedures'

USE [OnlineOrders$(country)]
GO
/****** Object:  StoredProcedure [dbo].[ReserveNextGiftCertNumber]    Script Date: 07/12/2012 13:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReserveNextGiftCertNumber] 
	 @GiftCertId NVARCHAR(50) OUT,
	 @PinCode NVARCHAR(50) OUT,
	 @BarcodeValue NVARCHAR(255) OUT
AS
BEGIN
	SET NOCOUNT ON;
   	BEGIN TRY
		BEGIN TRAN
		
		SELECT  TOP 1  @GiftCertId = GiftCertificateId, @PinCode = PinCode, @BarcodeValue = BarcodeValue from dbo.GiftCertificateList gclist WITH (READPAST, updlock)
			WHERE gclist.IsReserved = 0
			
		 UPDATE dbo.GiftCertificateList
			SET IsReserved = 1 
		 WHERE GiftCertificateId = @GiftCertId		
				
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @Msg NVARCHAR(MAX)  
		SELECT @Msg=ERROR_MESSAGE() 
		RAISERROR('Error Occured in OnlineOrders: %s', 20, 101,@msg)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[RSOrderConversion]    Script Date: 07/12/2012 13:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RSOrderConversion]
	@DaysCount as smallint,
	@currentDate as DATE 
AS
BEGIN
	DECLARE @Dates as dbo.DateList
	DECLARE @lowDate as DATE = DATEADD(day, -(@DaysCount - 1), @currentDate)
	DECLARE @dt as DATE = @lowDate
	DECLARE @curDay as smallInt = @DaysCount
	WHILE @curDay > 0 
	BEGIN
	 SET @dt = DATEADD(day, -(@curDay - 1), @currentDate)
	 SET @curDay = @curDay - 1 
	 INSERT INTO @Dates (Value) VALUES(@dt) 
	END
	
	SELECT * FROM dbo.ConversionOnDate(@currentDate, @lowDate, @Dates)
END
GO


/****** Object:  StoredProcedure [dbo].[OrderCorrection]    Script Date: 10/16/2012 16:24:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[OrderCorrection] 
    -- Add the parameters for the stored procedure here
     @OrderId NVARCHAR(255),
     @updateDate DATETIME
AS
BEGIN
BEGIN TRY
    begin tran
    update [OnlineOrders_RU].[dbo].[OrderHeader]
    Set OrderStatus = 4,
    UpdateDate = @updateDate
         where OrderId = @OrderId
  
  insert into [OnlineOrders_RU].[dbo].[OrderStatusHistory] 
     ([OrderId]
      ,[FromStatus]
      ,[ToStatus]
      ,[UpdateDate]
      ,[ChangedType]
      ,[ReasonText])
  values(@OrderId, 3, 4, @updateDate, 0, 'System correction')
  
 commit tran
END TRY
    BEGIN CATCH
    ROLLBACK TRANSACTION
    DECLARE @Msg NVARCHAR(MAX)  
    SELECT @Msg=ERROR_MESSAGE() 
    RAISERROR('Error Occured in OnlineOrders_RU: %s', 20, 101,@msg) 
END CATCH

END

GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLastReserveCreationTime]
AS
BEGIN
    SELECT TOP 1 UpdateDate
        FROM [OrderHeader]
        WHERE NOT WWSOrderId IS NULL
        ORDER BY UpdateDate DESC
END

GO

