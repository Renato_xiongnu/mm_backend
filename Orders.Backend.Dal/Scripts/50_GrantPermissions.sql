﻿print 'grant permissions'

USE [OnlineOrders$(country)]
GO

GRANT EXECUTE ON [dbo].[ReserveNextGiftCertNumber] TO [OnlineOrderUser];
GRANT EXECUTE ON [dbo].[RSOrderConversion] TO [OnlineOrderUser];

GRANT SELECT ON [dbo].ConversionOnDate TO [OnlineOrderUser];

GRANT CONTROL ON TYPE::[dbo].[DateList] TO [OnlineOrderUser]

