﻿print 'Create functions'
USE [OnlineOrders$(country)]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertToDate]    Script Date: 07/12/2012 13:07:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ConvertToDate]
(
	-- Add the parameters for the function here
	@CurrentDateTime as DateTime
)
RETURNS DATE
AS
BEGIN
	
	RETURN CAST( (STR( YEAR( @CurrentDateTime ) ) + '/' + STR( MONTH( @CurrentDateTime ) ) + '/' + STR( DAY( @CurrentDateTime ) )) AS DATE)

END
GO
/****** Object:  UserDefinedFunction [dbo].[ConversionOnDate]    Script Date: 07/12/2012 13:07:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[ConversionOnDate]
(	
	@currentDate as DATE,
	@lowDate as DATE,
	@Dates as dbo.DateList READONLY
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
	Store.SapCode, 
	store.CreateDate, 
	IsNULL(c.Cnt, 0) as CreatedCount, 
	IsNull(c.IsPaidCount,0) as PaidCount,
	IsNULL(c.OnlinePayment, 0) as OnlinePayment,
	IsNULL(c.HasDelivery, 0) as HasDelivery
	
	from
	(
	 SELECT store.SapCode, d.Value as CreateDate from store
		CROSS JOIN @Dates as d
	)
	 Store
		LEFT JOIN 
		(  
		--, OnlinePayment, HasDelivery
		SELECT SapCode, CreateDate, Sum(Cnt) as cnt, Sum(IsPaidCount) as IsPaidCount, SUM(OnlinePayment) as OnlinePayment, SUM(HasDelivery) as HasDelivery
		from (
			SELECT SapCode, Count(OrderId) as Cnt, CreateDate, PaidDate, SUM(OnlinePayment) as OnlinePayment, SUM(HasDelivery) as HasDelivery,
			 (CASE WHEN PaidDate IS NOT NULL AND PaidDate <= @currentDate THEN 1 ELSE 0 END)*Count(OrderId) as IsPaidCount
			 from 
				( SELECT SapCode, OrderId, OnlinePayment, HasDelivery , dbo.ConvertToDate(CreateDate) as CreateDate, 
														 dbo.ConvertToDate(PaidDate) as PaidDate
					  FROM dbo.RSCreatedOrderView ) as q 
					  where CreateDate <= @currentDate and CreateDate >= @lowDate
					  GROUP BY SapCode, CreateDate, PaidDate  
				  ) as d  
			  Group by  SapCode, CreateDate) c 		  
		  on c.SapCode = Store.SapCode AND c.CreateDate = Store.CreateDate	
	)
GO
