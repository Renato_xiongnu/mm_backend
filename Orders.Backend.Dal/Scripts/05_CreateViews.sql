﻿print 'Create views'
USE [OnlineOrders$(country)]
GO
/****** Object:  View [dbo].[RSNumberOfDays]    Script Date: 06/19/2012 18:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RSNumberOfDays]
AS
WITH Numbers(DayNum) AS (SELECT     7 AS Expr1
                                                UNION ALL
                                                SELECT     DayNum + 7 AS Expr1
                                                FROM         Numbers AS Numbers_2
                                                WHERE     (DayNum < 28))
    SELECT     DayNum
     FROM         Numbers AS Numbers_1
union ALL 
SELECT 90 as DayNum
union ALL 
SELECT 180 as DayNum
union ALL 
SELECT 365 as DayNum
GO

/****** Object:  View [dbo].[RSCreatedOrderView]    Script Date: 06/19/2012 18:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RSCreatedOrderView]
AS
SELECT     header.OrderId, WWSOrderId, SapCode, UpdateDate as LastUpdateDate,
		  (SELECT TOP 1 UpdateDate from dbo.OrderStatusHistory history WHERE 
					history.OrderId = header.OrderId AND history.FromStatus = 0 AND history.ToStatus = 1)
					AS CreateDate,
		   (SELECT TOP 1 UpdateDate from dbo.OrderStatusHistory history WHERE 
					history.OrderId = header.OrderId AND history.FromStatus = 3 AND history.ToStatus = 4)
					AS PaidDate,
					(CASE  WHEN PaymentType = 1 
							THEN 1
						ELSE 0
					 END) as OnlinePayment,					 	
					 CASE  WHEN delivery.CustomerId IS NULL THEN 0
					 ELSE 1 END as HasDelivery 
FROM         dbo.OrderHeader header 
LEFT JOIN dbo.Delivery delivery on delivery.CustomerId = header.CustomerId AND
										  delivery.OrderId = header.OrderId
WHERE     (OrderStatus >= 1) AND (OrderStatus <= 102)
GO

/****** Object:  View [dbo].[AvitoOrders]    Script Date: 14/06/2013 14:20:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AvitoOrders]
AS
SELECT head.OrderId, head.WWSOrderId, head.SapCode, stor.Name as StoreName, head.OrderStatus, head.Comment, head.ExternalNumber,
		head.ExpirationDate, cust.IsDefault as CustomerIsDefault, cust.Name as CustomerName, cust.Surname as CustomerSurname, cust.Phone as CustomerPhone, cust.Phone2 as CustomerPhone2, cust.Email as CustomerEmail,
		rHeadModified.ArticleNum, rHeadModified.Title as ArticleTitle, rHeadModified.Price as ArticlePrice, rHeadModified.Qty as ArticleQty
	FROM dbo.OrderHeader AS head
	LEFT JOIN dbo.Store AS stor ON head.SapCode = stor.SapCode
	LEFT JOIN dbo.Customer AS cust ON head.CustomerId = cust.CustomerId
	LEFT JOIN (SELECT rHead.OrderId, rHead.WWSOrderId, rHead.ReserveId, line.Title, 
			line.ArticleNum, line.Price, line.Qty
			FROM dbo.ReserveHeader AS rHead
			LEFT JOIN dbo.ReserveLine AS line ON rHead.ReserveId = line.ReserveId) AS rHeadModified 
	ON head.OrderId = rHeadModified.OrderId
	AND (head.WWSOrderId = rHeadModified.WWSOrderId 
	OR (head.WWSOrderId IS NULL AND rHeadModified.WWSOrderId IS NULL))
	WHERE head.ExternalSystem = 1
GO