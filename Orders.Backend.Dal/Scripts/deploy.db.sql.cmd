@rem working dir
@cd %1
@set server=%2
@set country=%3

@echo  create local db
@sqlcmd -S %server% -E -v country="%country%" -i 01_CreateDb.sql, 02_CreateSchema.sql, 03_CreateDboTables.sql, 04_CreateTypes.sql, 05_CreateViews.sql, 20_CreateFunctions.sql, 21_CreateStoredProcedures.sql, 50_GrantPermissions.sql, 90_InsertGeneralData.sql, 100_OrderHeaderUpdate.sql

@echo  test data
@sqlcmd -S %server% -E -v country="%country%" -i 99_InsertTestData.sql

@pause