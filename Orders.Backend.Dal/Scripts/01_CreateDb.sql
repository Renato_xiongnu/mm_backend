﻿print 'CreateDb OnlineOrders$(country)'
USE [master]
if db_id('OnlineOrders$(country)') is not null
	BEGIN
		EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'OnlineOrders$(country)'
		ALTER DATABASE [OnlineOrders$(country)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		ALTER DATABASE [OnlineOrders$(country)] SET SINGLE_USER
		DROP DATABASE [OnlineOrders$(country)]
	END
GO

CREATE DATABASE [OnlineOrders$(country)]
GO

ALTER DATABASE [OnlineOrders$(country)] SET RECOVERY SIMPLE WITH NO_WAIT
GO

ALTER DATABASE [OnlineOrders$(country)] SET ALLOW_SNAPSHOT_ISOLATION ON
GO

ALTER DATABASE [OnlineOrders$(country)] SET READ_COMMITTED_SNAPSHOT ON
GO

-- NOTE [sg]: in order to fix http://mediamarkt.jira.com/browse/GMSREPORTING-164 we need to enable deadlocks logging
DBCC TRACEON (1222, 3605, -1)
GO


--IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'OnlineOrderLogin$(country)')
--	DROP LOGIN [OnlineOrderLogin$(country)]
--GO

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'OnlineOrderLogin$(country)')
CREATE LOGIN [OnlineOrderLogin$(country)] WITH PASSWORD=N'Rep@rting1', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO




USE [OnlineOrders$(country)]
go
CREATE USER [OnlineOrderUser] FOR LOGIN OnlineOrderLogin$(country) WITH DEFAULT_SCHEMA=dbo
EXEC sp_addrolemember N'db_datareader', OnlineOrderUser
EXEC sp_addrolemember N'db_datawriter', OnlineOrderUser

GO