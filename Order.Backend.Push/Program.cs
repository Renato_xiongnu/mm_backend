﻿using System;
using log4net;
using log4net.Config;
using MMS.Shared;
using Orders.Backend.Services.PushService.Services;

namespace Orders.Backend.Push.PickupLocation
{
    internal class Program
    {

        private static readonly ILog Logger = LogManager.GetLogger("PickupLocationPush"); 

        private static void Main()
        {
            XmlConfigurator.Configure();

            Logger.Info("PickupLocationPush has been started");

            try
            {
                if (!Environment.UserInteractive)
                {                    
                    DebugHelper.RunService<PickupLocationService>();
                }
                else
                { 
                    DebugHelper.RunServiceAsWinForm<PickupLocationService>();
                    Console.WriteLine("Press any key to stop...");
                    Console.ReadKey(); 
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        } 
       
    }
}
