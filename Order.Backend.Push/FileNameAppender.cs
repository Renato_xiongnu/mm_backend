﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;

namespace Orders.Backend.Push.PickupLocation
{
    /// <summary>
    /// Пользовательский аппендер для log4net.
    /// Логирует в файл зависящий от названия логгера.
    /// </summary>
    public class FileNameAppender : AppenderSkeleton
    {
        /// <summary>
        /// Строка форматирования для пути к файлу.
        /// </summary>
        public string FileFormat { get; private set; }

        /// <summary>
        /// Добавляет информацию о логируемом событии в файл.
        /// </summary>
        /// <param name="loggingEvent">Логируемое событие.</param>
        protected override void Append(LoggingEvent loggingEvent)
        {
            var logFile = string.Format(FileFormat, loggingEvent.LoggerName, DateTime.Now);
            try
            {
                var str = RenderLoggingEvent(loggingEvent);
                logFile = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar + logFile;

                if (!File.Exists(logFile))
                    File.Create(logFile).Close();

                File.AppendAllText(logFile, str);
            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger("Logger").Error(e.Message + logFile + System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }

        protected override bool RequiresLayout
        {
            get
            {
                return true;
            }
        }
    }
}
