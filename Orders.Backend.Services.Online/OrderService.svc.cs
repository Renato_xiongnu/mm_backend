﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Transactions;
using NLog;
using Orders.Backend.Services.Contracts.OrderService;


namespace Orders.Backend.Services.Online
{
    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OrderService : IOrderService
    {
        private static readonly Logger Logger;

        static OrderService()
        {
            Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);
        }

        private readonly Order.OrderManager _orderManager;
        public OrderService()
        {
            _orderManager = new Order.OrderManager();
        }

        public ValidateOrderOperationResult ValidateOrder(StoreInfo storeInfo, DeliveryInfo delivery, ICollection<ArticleData> articlesData)
        {
            return /*_orderManager.ValidateOrder(storeInfo, delivery, articlesData);*/ null;
        }

        public CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData, DeliveryInfo deliveryInfo, ICollection<ArticleData> articlesData)
        {
            Logger.Info("CreateOrder starts orderId {0}", orderData.OrderId);
            var result = _orderManager.CreateOrder(clientData, orderData, deliveryInfo, articlesData);
            if (result.ReturnCode == ReturnCode.Error)
                Logger.Error(result.ErrorMessage);
            Logger.Info("CreateOrder stops orderId {0}", orderData.OrderId);
            return result;
        }

        public CreateOrderOperationResult CreateOrder2(ClientData clientData, CreateOrderData orderData, DeliveryInfo deliveryInfo, ICollection<InitArticleData> articlesData)
        {
            return CreateOrder(clientData, orderData, deliveryInfo, (ICollection<ArticleData>)articlesData);
        }

        public UpdateOrderOperationResult UpdateOrderByEmployee(UpdateOrderData orderData, ReserveInfo newReserveInfo)
        {
            var toStatus = orderData.ChangeStatusTo.HasValue ? orderData.ChangeStatusTo.Value.ToString() : "unchanged";

            Logger.Info("{0} : UpdateOrderByEmployee starts to status {1}", orderData.OrderId, toStatus);
            var result = _orderManager.UpdateOrderByEmployee(orderData);
            if (result.ReturnCode == ReturnCode.Error)
            {
                Logger.Error(result.ErrorMessage);
                Logger.Info("{0} : UpdateOrderByEmployee  stops to status with error {1}", orderData.OrderId, result.ToStatus);
                return result;
            }

            if (newReserveInfo != null && newReserveInfo.ReserveLines != null && newReserveInfo.ReserveLines.Count > 0)
            {
                Logger.Trace("{0} : UpdateReserveLine starts to status", orderData.OrderId);

                var updateReserveResult = _orderManager.UpdateReserveLinesByEmployee(orderData.OrderId, newReserveInfo);

                if (updateReserveResult.ReturnCode == ReturnCode.Error)
                {
                    Logger.Error(updateReserveResult.ErrorMessage);

                    result.ReturnCode = updateReserveResult.ReturnCode;
                    result.ErrorMessages = updateReserveResult.ErrorMessages;
                }
                Logger.Trace("{0} : UpdateReserveLine stops", orderData.OrderId);
            }

            return result;
        }

        public UpdateOrderOperationResult UpdateOrder(UpdateOrderData orderData, ReserveInfo newReserveInfo)
        {
            var toStatus = orderData.ChangeStatusTo.HasValue ? orderData.ChangeStatusTo.Value.ToString() : "unchanged";

            Logger.Info("{0} : UpdateOrder starts to status {1}", orderData.OrderId, toStatus);

            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var result = _orderManager.UpdateOrderWithReserve(orderData, newReserveInfo);
                if (result.ReturnCode == ReturnCode.Error)
                {
                    Logger.Error(result.ErrorMessage);
                    Logger.Info("{0} : UpdateOrder  stops to status with error {1}", orderData.OrderId, result.ToStatus);
                    return result;
                }

                //if (newReserveInfo != null
                //    /*&& newReserveInfo.ReserveLines != null && newReserveInfo.ReserveLines.Count > 0*/)
                //{
                //    Logger.Info(string.Format("UpdateReserveLine starts to status"));
                //    var updateReserveResult = _orderManager.UpdateReserveInfo(orderData.OrderId, newReserveInfo,
                //        orderData.ReserveContent);

                //    if (updateReserveResult.ReturnCode == ReturnCode.Error)
                //    {
                //        Logger.Error(updateReserveResult.ErrorMessage);

                //        result.ReturnCode = updateReserveResult.ReturnCode;
                //        result.ErrorMessages = updateReserveResult.ErrorMessages;
                //    }
                //    Logger.Info(string.Format("UpdateReserveLine stops"));
                //}

                if (result.ReturnCode == ReturnCode.Ok)
                {
                    transaction.Complete();
                }
                Logger.Info("{0} : UpdateOrder  stops to status with success {1}", orderData.OrderId, result.ToStatus);
                return result;
            }
        }

        public ICollection<GerOrderDataResult> GerOrderDataByTypeId(short SystemTypeId, ICollection<string> sapCodes = null)
        {
            return _orderManager.GetOrderInfoByType(SystemTypeId, sapCodes);
        }

        public GerOrderDataResult GerOrderDataByOrderId(string orderId)
        {
            return _orderManager.GetOrderInfoByOrderId(orderId);
        }

        public GerOrderStatusHistoryResult GetOrderStatusHistoryByOrderId(string orderId)
        {
            return _orderManager.GetOrderStatusHistoryByOrderId(orderId);
        }

        public GerReservesResult GetReservesByOrderId(string orderId)
        {
            return _orderManager.GetReservesByOrderId(orderId);
        }

        public ICollection<GerOrderDataResult> GerOrderDataByOrderIds(ICollection<string> orderIds)
        {
            return _orderManager.GetOrderInfoByOrderIds(orderIds);
        }

        public OperationResult UpdateReservesContentStatus(UpdateReservesContentData contentData)
        {
            return _orderManager.UpdateReserveContentForPrinting(contentData);
        }

        public GetReservesContentResult GetReservesContent(string sapCode, int maxQty)
        {
            return _orderManager.GetReservesContentForPrinting(sapCode, maxQty);
        }


        public ImplContractsStatusOperationResult ImplContractsStatus()
        {
            return new ImplContractsStatusOperationResult()
            {
                ValidateOrder = ContractStatus.NotImplemented,
                CreateOrder = ContractStatus.OK,
                UpdateOrderStatus = ContractStatus.OK
            };
        }

        public GetStoreDataResult GetStoreData(string sapCode)
        {
            return _orderManager.GetStoreData(sapCode);
        }

        public GetAllStoresDataResult GetAllStoresData()
        {
            return _orderManager.GetAllStoresData();
        }

        public GerOrderDataResult GetOrderDataByCertificate(string certificateId)
        {
            Logger.Info("GetOrderDataByCertificate starts certificateId {0}", certificateId);
            var result = _orderManager.GetOrderInfoByCertificate(certificateId);
            Logger.Info("GetOrderDataByCertificate  stops certificateId {0}", certificateId);
            return result;
        }

        public GetOrderDataResult GetOrderDataByBasketId(string basket3Id)
        {
            return _orderManager.GetOrderDataByBasketId(basket3Id);
        }

        public IEnumerable<StockLocationOrderCount> GetOrderCountInStockBySaleLocationAndStockLoations(String saleLocationSapCode, IEnumerable<String> stockLocationSapCodes, Int32 daysRange)
        {
            return _orderManager.GetOrderCountInStockBySaleLocationAndStockLoations(saleLocationSapCode, stockLocationSapCodes, daysRange);
        }

    }
}
