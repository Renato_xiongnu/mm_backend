﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Services.Online
{
    [ServiceContract]
    public interface IOrderService
    {
        [OperationContract]
        [Obsolete]
        ValidateOrderOperationResult ValidateOrder(StoreInfo storeInfo, DeliveryInfo delivery,
            ICollection<ArticleData> articlesData);

        [OperationContract]
        CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<ArticleData> articlesData);

        [OperationContract]
        CreateOrderOperationResult CreateOrder2(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<InitArticleData> articlesData);

        [OperationContract]
        UpdateOrderOperationResult UpdateOrderByEmployee(UpdateOrderData orderData, ReserveInfo newReserveInfo);

        [OperationContract]
        UpdateOrderOperationResult UpdateOrder(UpdateOrderData orderData, ReserveInfo newReserveInfo);

        [OperationContract]
        GerOrderDataResult GerOrderDataByOrderId(string orderId);

        [OperationContract]
        ICollection<GerOrderDataResult> GerOrderDataByOrderIds(ICollection<string> orderIds);

        [OperationContract]
        ICollection<GerOrderDataResult> GerOrderDataByTypeId(short SystemTypeId, ICollection<string> sapCodes = null);

        [OperationContract]
        GetReservesContentResult GetReservesContent(string sapCode, int maxQty);

        [OperationContract]
        OperationResult UpdateReservesContentStatus(UpdateReservesContentData contentData);

        [OperationContract]
        [Obsolete]
        ImplContractsStatusOperationResult ImplContractsStatus();

        [OperationContract]
        GetStoreDataResult GetStoreData(string sapCode);

        [OperationContract]
        GetAllStoresDataResult GetAllStoresData();

        [OperationContract]
        GerOrderStatusHistoryResult GetOrderStatusHistoryByOrderId(string orderId);

        [OperationContract]
        GerReservesResult GetReservesByOrderId(string orderId);
        
        [OperationContract]
        GetOrderDataResult GetOrderDataByBasketId(string basket3Id);

        [OperationContract]
        IEnumerable<StockLocationOrderCount> GetOrderCountInStockBySaleLocationAndStockLoations(String saleLocationSapCode, IEnumerable<String> stockLocationSapCodes, Int32 daysRange = 14);
    }
}