﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Orders.Backend.Services.Contracts.OrderService;
using Orders.Backend.Services.Contracts.RequestFormService;

namespace Orders.Backend.Services.Online
{    
    [ServiceContract]
    public interface IRequestFormService
    {
        [OperationContract]
        OperationResult CreateRequestForm(ClientData clientData, CreateRequestFormData requestFormData,
                                          ICollection<ArticleInfo> articles);
    }
}
