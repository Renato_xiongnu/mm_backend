﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Orders.Backend.Services.Contracts.CustomerService;

namespace Orders.Backend.Services.Online
{
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "FindByPhone")]
        GetCustomerResult FindByPhone(string token,string phone);
    }
}
