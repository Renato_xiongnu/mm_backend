﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Web;
using NLog;
using Orders.Backend.Services.Contracts.CustomerService;
using Orders.Backend.Services.Order.Facades;

namespace Orders.Backend.Services.Online
{
    public class CustomerService : ICustomerService
    {
        private static readonly Logger Logger;

        private static readonly string[] AvaliableTokens =
            ConfigurationManager.AppSettings["CustomerServiceTokens"].Split(new []{";"}, StringSplitOptions.RemoveEmptyEntries);
        
        static CustomerService()
        {
            Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);
        }

        public GetCustomerResult FindByPhone(string token, string phone)
        {
            Logger.Info("FindByPhone starts phone {0}, token {1}", phone, token);
            if (!AvaliableTokens.Contains(token))
            {
                Set403();
                Logger.Info("FindByPhone stops phone {0}, token {1} unathorized", phone, token);
                return null;
            }
            var result = OrderHeaderFacade.GetCustomerByPhone(phone);
            Logger.Info("FindByPhone stops phone {0}, token {1}", phone, token);
            if (result == null)
            {
                Set404();
                return null;
            }

            return result;
        }

        private void Set403()
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Forbidden;
        }

        private void Set404()
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
        }
    }
}
