﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineOrders.Common
{
    public enum UserRoles
    {
        [EnumFieldDescription("Agent")]
        Agent,
        [EnumFieldDescription("Superviser")]
        Superviser,
        [EnumFieldDescription("Manager")]
        StoreManager,
        [EnumFieldDescription("Admin")]
        Admin,
        [EnumFieldDescription("AvitoManager")]
        AvitoManager,
        [EnumFieldDescription("Broker")]
        Broker
    }
}
