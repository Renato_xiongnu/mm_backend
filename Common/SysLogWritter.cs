﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace OnlineOrders.Common
{
    public static class SysLogWritter
    {
        public static void WriteToEventLog(string eventSource, Exception e, string action)
        {
            var log = new EventLog { Source = eventSource, Log = "Application" };
            var message = "An exception occurred communicating with the data source.\n\n";
            message += "Action: " + action + "\n\n";
            message += "Exception: " + e;
            log.WriteEntry(message);
        }
    }
}
