﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace OnlineOrders.Common
{
    public class Transliteration
    {
        public List<TransliterationRule> Import(string resourceName)
        {
            var rules = new List<TransliterationRule>();

            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var file = new StreamReader(stream))
            {
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();
                    string[] param = line.Split('\t');
                    if (string.IsNullOrEmpty(param[(int)TrFields.Source])
                        || string.IsNullOrEmpty(param[(int)TrFields.Target])
                        || string.IsNullOrEmpty(param[(int)TrFields.Prior])
                        || string.IsNullOrEmpty(param[(int)TrFields.From]))
                    {
                        continue;
                    }
                    int prior;
                    if (!Int32.TryParse(param[(int)TrFields.Prior], out prior))
                    {
                        continue;
                    }
                    if (prior >= 0)
                    {
                        var rule = new TransliterationRule
                        {
                            Source = param[(int)TrFields.Source],
                            Target = param[(int)TrFields.Target],
                            Prior = prior,
                            From = param[(int)TrFields.From],
                            To = param[(int)TrFields.To]
                        };
                        rules.Add(rule);
                    }
                }
            }
            return rules;
        }

        public string TransliterateUserName(string userName)
        {
            var rules = Import("transliteration.txt");
            rules = rules.OrderBy(x => x.Prior).ToList();
            return rules.Aggregate(userName, (current, rule) => current.Replace(rule.From, rule.To));
        }
    }
    public enum TrFields
    {
        Source = 0,
        Target = 1,
        Prior = 2,
        From = 3,
        To = 4
    }

    public class TransliterationRule
    {
        public string Source { get; set; }
        public string Target { get; set; }
        public int Prior { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
