﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace OnlineOrders.Common.ServiceExtensions
{
    public class CookieBehaviorExtension : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get { return typeof(CookieEndpointBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new CookieEndpointBehavior();
        }
    }
}