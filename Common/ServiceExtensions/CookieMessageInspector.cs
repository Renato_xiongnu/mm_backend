﻿using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text.RegularExpressions;
using System.Web;

namespace OnlineOrders.Common.ServiceExtensions
{
    /// <summary>
    /// Используется для получения, хранения и дальнейшей передачи cookie от wcf сервиса
    /// </summary>
    public class CookieMessageInspector : IClientMessageInspector
    {
        #region [Singletone]

        private static CookieMessageInspector instance;
        private CookieMessageInspector()
        {
        }
        public static CookieMessageInspector Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CookieMessageInspector();
                }
                return instance;
            }
        }

        #endregion

        /// <summary>
        /// получить и записать cookie из сообщения
        /// </summary>
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            HttpResponseMessageProperty httpResponse = reply.Properties[HttpResponseMessageProperty.Name] as HttpResponseMessageProperty;
            if (httpResponse != null)
            {
                string cookie = httpResponse.Headers[HttpResponseHeader.SetCookie];
                if (!string.IsNullOrEmpty(cookie) && Regex.IsMatch(cookie, @".ASPXAUTH=[A-Z0-9]*"))
                {
                    var parsedCookie = Regex.Match(cookie, @".ASPXAUTH=([A-Z0-9]*)").Groups[1].Value;
                    HttpContext.Current.Response.AppendCookie(new HttpCookie(".ASPXAUTH", parsedCookie));
                }
            }
        }

        /// <summary>
        /// добавить cookie к запросу 
        /// </summary>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            var cookie = HttpContext.Current.Request.Headers[HttpRequestHeader.Cookie.ToString()];
            HttpRequestMessageProperty httpRequest;
            if (!request.Properties.ContainsKey(HttpRequestMessageProperty.Name))
            {
                request.Properties.Add(HttpRequestMessageProperty.Name, new HttpRequestMessageProperty());
            }
            httpRequest = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
            httpRequest.Headers.Add(HttpRequestHeader.Cookie, cookie);
            return null;
        }
    }
}