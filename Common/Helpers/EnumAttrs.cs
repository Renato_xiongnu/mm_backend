﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Common.Helpers
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class EnumFieldDescriptionAttribute : Attribute
    {
        public EnumFieldDescriptionAttribute(string name = "")
        {
            Name = name;
        }

        public string Name { get; set; }
    }


    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class EnumFieldLinkedStringAttribute : Attribute
    {
        public EnumFieldLinkedStringAttribute(string name = "")
        {
            Name = name;
        }

        public string Name { get; set; }
    }


    public static class EnumExtensions
    {
        public static string GetDescription(this Enum enumVal)
        {
            var attr = enumVal.GetAttributeOfType<EnumFieldDescriptionAttribute>();

            return (attr == null) ? enumVal.ToString() : attr.Name;
        }


        public static string GetLinkedString(this Enum enumVal)
        {
            var attr = enumVal.GetAttributeOfType<EnumFieldLinkedStringAttribute>();

            return (attr == null) ? enumVal.ToString() : attr.Name;
        }

        public static string GetDisplayValue(this Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
        }
    }
}
