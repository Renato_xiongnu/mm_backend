﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Helpers;

namespace OnlineOrders.Common.Helpers
{
    internal enum RussianTimeZoneEnum
    {
        // ReSharper disable InconsistentNaming    
        [EnumFieldDescription("Калининградское время	MSK–1 (UTC+3)")]
        [EnumFieldLinkedString("Azores Standard Time")]
        USZ1,

        [EnumFieldDescription("Московское время	 MSK (UTC+4)")]
        [EnumFieldLinkedString("Russian Standard Time")]
        MSK,

        [EnumFieldDescription("Екатеринбургское время	MSK+2 (UTC+6)")]
        [EnumFieldLinkedString("Ekaterinburg Standard Time")]
        YEKT,

        [EnumFieldDescription("Омское время	MSK+3 (UTC+7)")]
        [EnumFieldLinkedString("N. Central Asia Standard Time")]
        OMST,

        [EnumFieldDescription("Красноярское время	MSK+4 (UTC+8)")]
        [EnumFieldLinkedString("North Asia Standard Time")]
        KRAT,

        [EnumFieldDescription("Иркутское время	MSK+5 (UTC+9)")]
        [EnumFieldLinkedString("North Asia East Standard Time")]
        IRKT,

        [EnumFieldDescription("Якутское время	MSK+6 (UTC+10)")]
        [EnumFieldLinkedString("Yakutsk Standard Time")]
        YAKT,

        [EnumFieldDescription("Владивостокское время	MSK+7 (UTC+11)")]
        [EnumFieldLinkedString("Vladivostok Standard Time")]
        VLAT,

        [EnumFieldDescription("Магаданское время	MSK+8 (UTC+12)")]
        [EnumFieldLinkedString("Magadan Standard Time")]
        MAGT
        // ReSharper restore InconsistentNaming
    }

    public static class TimeZoneHelper
    {
        public static string DefaultTimeZoneStr { get { return RussianTimeZoneEnum.MSK.ToString(); } }

        public static TimeZoneInfo GetTimeZoneInfo(string timeZoneStr)
        {            
            if (string.IsNullOrEmpty(timeZoneStr))
                throw new InvalidOperationException("timeZoneStr must be filled");

            //InvalidCastException
            var timeZome = timeZoneStr.ToEnum<RussianTimeZoneEnum>();

            var timeZoneId = timeZome.GetLinkedString();

            return  TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
        }

        public static DateTime ConvertToUtc(DateTime dateTime, string timeZoneStr)
        {
            var timeZoneInfo = GetTimeZoneInfo(timeZoneStr);
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZoneInfo);
        }

        public static DateTime ConvertToUtc(DateTime dateTime, TimeZoneInfo specificTimeZone)
        {            
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, specificTimeZone);
        }
    }
}
