﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Common.Helpers
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class PropertyFieldDescriptionAttribute : Attribute
    {
        public string Name { get; set; }
    }


    public static class Extensions
    {
        public static T GetAttributeOfType<T>(this object val) where T : System.Attribute
        {
            var type = val.GetType();
            var memInfo = type.GetMember(val.ToString());
            if (memInfo.Length > 0)
            {
                var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);

                if (attributes.Length > 0)
                    return (T)attributes[0];
            }
            return null;
        }

        public static string GetDescription(this PropertyInfo property)
        {
            var type = typeof(PropertyFieldDescriptionAttribute);
            var attributes = property.GetCustomAttributes(type, true);

            if (attributes.Length > 0)
            {
                var attr =  attributes.Where(t => t.GetType() == type).FirstOrDefault();

                return ((PropertyFieldDescriptionAttribute)attr).Name;
            }

            return null;
        }

        public static string GetDescription(this object val)
        {
            var attr = val.GetAttributeOfType<PropertyFieldDescriptionAttribute>();

            if (attr == null)
                return string.Empty;

            return attr.Name;
        }
    }
}
