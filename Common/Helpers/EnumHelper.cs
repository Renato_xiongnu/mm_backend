﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Helpers
{
    public static class EnumHelper
    {
        public static short ToShort(this Enum en)
        {
            return Convert.ToInt16(en);
        }

        public static int ToInt(this Enum en)
        {
            return Convert.ToInt32(en);
        }

        public static T ConvertTo<T>(this Enum en) where T : struct
        {
            try
            {
                var stValue = en.ToString();
                return stValue.ToEnum<T>();
            }
            catch (InvalidCastException)
            {
            }

            var intValue = en.ToInt();

            return intValue.ToEnum<T>();;
        }



        public static T ToEnum<T>(this string value) where T : struct
        {
            var cleanString = value.Replace("\"", "");

            T res = default(T);
            if (Enum.TryParse(cleanString, true, out res))
            {
                return res;
            }

            int num;
            if (int.TryParse(cleanString, out num))
            {
                if (Enum.IsDefined(typeof(T), num))
                    return (T)Enum.ToObject(typeof(T), num);
            }

            throw new InvalidCastException(string.Format("Value {0} is not valid enum value of {1}", value, typeof(T).Name));
        }

        public static T ToEnum<T>(this string value, T defaultValue) where T : struct
        {
            try
            {
                return EnumHelper.ToEnum<T>(value);
            }
            catch
            {
                return defaultValue;
            }
        }


        public static T ToEnum<T>(this short value) where T : struct
        {
            return EnumHelper.ToEnum<T>((int)value);
        }

        public static T ToEnum<T>(this short value, T defaultValue) where T : struct
        {
            return EnumHelper.ToEnum<T>((int)value, defaultValue);
        }

        public static T ToEnum<T>(this int value) where T : struct
        {
            if (Enum.IsDefined(typeof(T), value))
                return (T)Enum.ToObject(typeof(T), value);

            throw new InvalidCastException(string.Format("Value {0} is not valid enum value of {1}", value, typeof(T).Name));
        }

        public static T ToEnum<T>(this int value, T defaultValue) where T : struct
        {
            try
            {
                return EnumHelper.ToEnum<T>(value);
            }
            catch
            {
                return defaultValue;
            }
        }


        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute 
        { 
            var type = enumVal.GetType(); 
            var memInfo = type.GetMember(enumVal.ToString());
            if (memInfo.Length > 0)
            {
                var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);

                if (attributes.Length > 0)
                    return (T)attributes[0];
            }

            return null;
        } 
    }
}
