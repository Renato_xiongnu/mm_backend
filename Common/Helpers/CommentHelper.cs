﻿using System;
using System.Web;

namespace OnlineOrders.Common.Helpers
{
    public class CommentHelper
    {
        public static string FormatComment(string comment, string previousComment, string author)
        {
            var now = DateTime.Now;
            if (string.IsNullOrEmpty(comment))
            {
                return comment;
            }
            var newComment = HttpUtility.HtmlEncode(comment.Trim()).Replace("\r\n", @"</br>");
            newComment = string.Format(@"<b>{0} ({1:yyyy-MM-dd HH:mm}):</b><br/>{2}", author, now, newComment);
            newComment = string.IsNullOrEmpty(previousComment)
                ? newComment
                : string.Join("<br/>", previousComment, newComment);
            return newComment;
        }
    }
}
