﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Common.Helpers
{
    public static class ExceptionHelper
    {
        public static IEnumerable<string> GetInnerExceptionsText(this Exception error, bool withStackTrace = false)
        {
            ICollection<string> exceptionList = new LinkedList<string>();
            Exception realerror = error;
            while (realerror.InnerException != null)
            {
                realerror = realerror.InnerException;
                exceptionList.Add(realerror.Message);
            }

            return exceptionList;
        }

        public static string GetExceptionDetails(this Exception exception, bool withStackTrace = false)
        {
            var properties = exception.GetType()
                                    .GetProperties();
            var fields = properties
                             .Select(property => new
                             {
                                 Name = property.Name,
                                 Value = property.GetValue(exception, null)
                             })
                             .Select(x => String.Format(
                                 "{0} = {1}",
                                 x.Name,
                                 x.Value != null ? x.Value.ToString() : String.Empty
                             ));
            
            return String.Join("\n", fields);
        }


        public static string GetExceptionFullInfoText(this Exception ex, bool withStackTrace=false)
        {           
            return string.Join("\n", new[]
                                         {
                                             "Fields:",
                                             ex.GetExceptionDetails(withStackTrace),
                                             "Inner exceptions:"
                                         }
                                         .Union(ex.GetInnerExceptionsText(withStackTrace)));
        }
    }


  
}
