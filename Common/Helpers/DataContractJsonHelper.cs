﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json;

namespace OnlineOrders.Common.Helpers
{
    public class DataContractJsonHelper<T>
    {
      /*  public static string SerializeData(T objectValue)
        {
            var memoryStream = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(T));
            ser.WriteObject(memoryStream, objectValue);
            memoryStream.Position = 0;
            var sr = new StreamReader(memoryStream, Encoding.UTF8);
            return sr.ReadToEnd();
        }

        public static T DeserializeData(string value)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            Stream s = new MemoryStream(Encoding.UTF8.GetBytes(value));
            s.Position = 0;
            return (T)ser.ReadObject(s);
        }*/

        public static string SerializeData(T c)
        {
            return JsonConvert.SerializeObject(c, new TimeSpanToJsonConverter());
        }

        public static T DeserializeData(string value)
        {
            return JsonConvert.DeserializeObject<T>(value, new JsonToTimeSpanConverter());
        }
    }

    public class TimeSpanToJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TimeSpan);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((TimeSpan)value).ToString("G", new CultureInfo("en-US")));
            writer.Flush();
        }
    }

    public class JsonToTimeSpanConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Object);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            if (reader.ValueType == typeof(string))
            {
                TimeSpan t;
                return TimeSpan.TryParse(reader.Value.ToString(), new CultureInfo("en-US"), out t)
                ? t : reader.Value;
            }
            return reader.Value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
