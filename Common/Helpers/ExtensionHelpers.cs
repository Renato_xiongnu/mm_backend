﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace OnlineOrders.Common.Helpers
{
    public static class ExtensionHelpers
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> iEnumerable)
        {
            return iEnumerable == null || !iEnumerable.Any();
        }

        public static string ToJson(this object data)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(data);
        }

        public static T DeepClone<T>(this T obj)
        {
            var serializer = new DataContractSerializer(typeof (T), null, int.MaxValue, false, true, null);
            using (var ms = new System.IO.MemoryStream())
            {
                serializer.WriteObject(ms, obj);
                ms.Position = 0;
                return (T) serializer.ReadObject(ms);
            }
        }

        public static string GetOrDefault(this Dictionary<string, string> dict, string key)
        {
            if (dict.ContainsKey(key))
            {
                return Convert.ToString(dict[key]);
            }
            
            return string.Empty;
        }

        public static T GetOrDefault<T>(this Dictionary<string, string> dict, string key) where T : struct
        {
            if (dict.ContainsKey(key) && CanChangeType(dict[key], typeof (T)))
            {
                try
                {
                    var val = (T) Convert.ChangeType(dict[key], typeof(T));
                    return val;
                }
                catch(Exception)
                {
                    return default(T);
                }
            }
            return default(T);
        }

        public static T? GetOrNull<T>(this Dictionary<string, string> dict, string key) where T : struct
        {
            if (dict.ContainsKey(key) && CanChangeType(dict[key], typeof(T)))
            {
                try
                {
                    var val = (T)Convert.ChangeType(dict[key], typeof(T));
                    return val;
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch (Exception)
                // ReSharper restore EmptyGeneralCatchClause
                { }
            }

            return null;
        }

        public static bool IsGuid(this string s)
        {
            Guid result;
            return Guid.TryParse(s, out result);
        }

        public static bool CanChangeType(object value, Type conversionType)
        {
            if (conversionType == null)
            {
                return false;
            }

            if (value == null)
            {
                return false;
            }

            var convertible = value as IConvertible;

            return convertible != null;
        }
    }
}
