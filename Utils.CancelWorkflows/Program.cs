﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.CancelWorkflows
{
    class Program
    {
        static void Main(string[] args)
        {
            var failderOrders = File.ReadAllLines("FailderOrders.txt");

            foreach (var failderOrder in failderOrders)
            {
                try
                {
                    var client = new Proxy.WF.OrderProcessingServiceV1_3Client();
                    client.Cancel(failderOrder, "WF is broken", "system");

                    Console.WriteLine("Order with OrderId={0} canceled", failderOrder);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Can't cancel order with OrderId={0}, Error:{1}", failderOrder, e);
                }
            }
        }
    }
}
