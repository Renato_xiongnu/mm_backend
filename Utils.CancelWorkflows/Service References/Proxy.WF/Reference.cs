﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Utils.CancelWorkflows.Proxy.WF {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TaskResponse", Namespace="http://schemas.datacontract.org/2004/07/Orders.Backend.WF.Activities.TicketTool.T" +
        "asks")]
    [System.SerializableAttribute()]
    public partial class TaskResponse : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool CompleteSuccessfulField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool CompleteSuccessful {
            get {
                return this.CompleteSuccessfulField;
            }
            set {
                if ((this.CompleteSuccessfulField.Equals(value) != true)) {
                    this.CompleteSuccessfulField = value;
                    this.RaisePropertyChanged("CompleteSuccessful");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Start", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class Start : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string sapCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.Dictionary<string, string> initArgsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string sapCode {
            get {
                return this.sapCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.sapCodeField, value) != true)) {
                    this.sapCodeField = value;
                    this.RaisePropertyChanged("sapCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public System.Collections.Generic.Dictionary<string, string> initArgs {
            get {
                return this.initArgsField;
            }
            set {
                if ((object.ReferenceEquals(this.initArgsField, value) != true)) {
                    this.initArgsField = value;
                    this.RaisePropertyChanged("initArgs");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Proxy.WF.IOrderProcessingServiceV1_3")]
    public interface IOrderProcessingServiceV1_3 {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/CancelResponse")]
        bool Cancel(string WorkItemId, string reasonText, string cancelBy);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/CancelResponse")]
        System.Threading.Tasks.Task<bool> CancelAsync(string WorkItemId, string reasonText, string cancelBy);
        
        // CODEGEN: Generating message contract since the operation Start is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Start", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/StartResponse")]
        Utils.CancelWorkflows.Proxy.WF.StartResponse Start(Utils.CancelWorkflows.Proxy.WF.StartRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Start", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/StartResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.StartResponse> StartAsync(Utils.CancelWorkflows.Proxy.WF.StartRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Order_Creation_FailedContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Order_Creation_FailedContinueRespo" +
            "nse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Order_Creation_FailedContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Order_Creation_FailedContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Order_Creation_FailedContinueRespo" +
            "nse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Order_Creation_FailedContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_and_Return_ArticlesCo" +
            "ntinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_and_Return_ArticlesCo" +
            "ntinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Cancel_Order_and_Return_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_and_Return_ArticlesCo" +
            "ntinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_and_Return_ArticlesCo" +
            "ntinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Cancel_Order_and_Return_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Check_Reserved_ItemsContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Check_Reserved_ItemsContinueRespon" +
            "se")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Check_Reserved_ItemsContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Check_Reserved_ItemsContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Check_Reserved_ItemsContinueRespon" +
            "se")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Check_Reserved_ItemsContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_In_Process_And_Return" +
            "_ArticlesContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_In_Process_And_Return" +
            "_ArticlesContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Cancel_Order_In_Process_And_Return_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_In_Process_And_Return" +
            "_ArticlesContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Cancel_Order_In_Process_And_Return" +
            "_ArticlesContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Cancel_Order_In_Process_And_Return_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Return_ArticlesContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Return_ArticlesContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Return_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Return_ArticlesContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Return_ArticlesContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Return_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Check_PaymentContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Check_PaymentContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Check_PaymentContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Check_PaymentContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Check_PaymentContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Check_PaymentContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Approve_OrderContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Approve_OrderContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Approve_OrderContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Approve_OrderContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Approve_OrderContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Approve_OrderContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Bad_Reserve_ApprovalContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Bad_Reserve_ApprovalContinueRespon" +
            "se")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Bad_Reserve_ApprovalContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Bad_Reserve_ApprovalContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Bad_Reserve_ApprovalContinueRespon" +
            "se")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Bad_Reserve_ApprovalContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Notify_CustomerContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Notify_CustomerContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Notify_CustomerContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Notify_CustomerContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Notify_CustomerContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Notify_CustomerContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Review_ReserveContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Review_ReserveContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Review_ReserveContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Review_ReserveContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Review_ReserveContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Review_ReserveContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Delivery_ApprovalContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Delivery_ApprovalContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Delivery_ApprovalContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Delivery_ApprovalContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Delivery_ApprovalContinueResponse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Delivery_ApprovalContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Check_And_Replace_ArticlesContinue" +
            "", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Check_And_Replace_ArticlesContinue" +
            "Response")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Check_And_Replace_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Check_And_Replace_ArticlesContinue" +
            "", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Check_And_Replace_ArticlesContinue" +
            "Response")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Check_And_Replace_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Credit_Broker_ApprovalContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Credit_Broker_ApprovalContinueResp" +
            "onse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        Utils.CancelWorkflows.Proxy.WF.TaskResponse Credit_Broker_ApprovalContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOrderProcessingServiceV1_3/Credit_Broker_ApprovalContinue", ReplyAction="http://tempuri.org/IOrderProcessingServiceV1_3/Credit_Broker_ApprovalContinueResp" +
            "onse")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="taskResponse")]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Credit_Broker_ApprovalContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class StartRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public Utils.CancelWorkflows.Proxy.WF.Start Start;
        
        public StartRequest() {
        }
        
        public StartRequest(Utils.CancelWorkflows.Proxy.WF.Start Start) {
            this.Start = Start;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class StartResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/", Order=0)]
        public string @string;
        
        public StartResponse() {
        }
        
        public StartResponse(string @string) {
            this.@string = @string;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IOrderProcessingServiceV1_3Channel : Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class OrderProcessingServiceV1_3Client : System.ServiceModel.ClientBase<Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3>, Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3 {
        
        public OrderProcessingServiceV1_3Client() {
        }
        
        public OrderProcessingServiceV1_3Client(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public OrderProcessingServiceV1_3Client(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public OrderProcessingServiceV1_3Client(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public OrderProcessingServiceV1_3Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool Cancel(string WorkItemId, string reasonText, string cancelBy) {
            return base.Channel.Cancel(WorkItemId, reasonText, cancelBy);
        }
        
        public System.Threading.Tasks.Task<bool> CancelAsync(string WorkItemId, string reasonText, string cancelBy) {
            return base.Channel.CancelAsync(WorkItemId, reasonText, cancelBy);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Utils.CancelWorkflows.Proxy.WF.StartResponse Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3.Start(Utils.CancelWorkflows.Proxy.WF.StartRequest request) {
            return base.Channel.Start(request);
        }
        
        public string Start(Utils.CancelWorkflows.Proxy.WF.Start Start1) {
            Utils.CancelWorkflows.Proxy.WF.StartRequest inValue = new Utils.CancelWorkflows.Proxy.WF.StartRequest();
            inValue.Start = Start1;
            Utils.CancelWorkflows.Proxy.WF.StartResponse retVal = ((Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3)(this)).Start(inValue);
            return retVal.@string;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.StartResponse> Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3.StartAsync(Utils.CancelWorkflows.Proxy.WF.StartRequest request) {
            return base.Channel.StartAsync(request);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.StartResponse> StartAsync(Utils.CancelWorkflows.Proxy.WF.Start Start) {
            Utils.CancelWorkflows.Proxy.WF.StartRequest inValue = new Utils.CancelWorkflows.Proxy.WF.StartRequest();
            inValue.Start = Start;
            return ((Utils.CancelWorkflows.Proxy.WF.IOrderProcessingServiceV1_3)(this)).StartAsync(inValue);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Order_Creation_FailedContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Order_Creation_FailedContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Order_Creation_FailedContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Order_Creation_FailedContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Cancel_Order_and_Return_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Cancel_Order_and_Return_ArticlesContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Cancel_Order_and_Return_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Cancel_Order_and_Return_ArticlesContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Check_Reserved_ItemsContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Check_Reserved_ItemsContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Check_Reserved_ItemsContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Check_Reserved_ItemsContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Cancel_Order_In_Process_And_Return_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Cancel_Order_In_Process_And_Return_ArticlesContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Cancel_Order_In_Process_And_Return_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Cancel_Order_In_Process_And_Return_ArticlesContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Return_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Return_ArticlesContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Return_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Return_ArticlesContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Check_PaymentContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Check_PaymentContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Check_PaymentContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Check_PaymentContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Approve_OrderContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Approve_OrderContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Approve_OrderContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Approve_OrderContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Bad_Reserve_ApprovalContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Bad_Reserve_ApprovalContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Bad_Reserve_ApprovalContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Bad_Reserve_ApprovalContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Notify_CustomerContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Notify_CustomerContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Notify_CustomerContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Notify_CustomerContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Review_ReserveContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Review_ReserveContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Review_ReserveContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Review_ReserveContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Delivery_ApprovalContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Delivery_ApprovalContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Delivery_ApprovalContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Delivery_ApprovalContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Check_And_Replace_ArticlesContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Check_And_Replace_ArticlesContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Check_And_Replace_ArticlesContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Check_And_Replace_ArticlesContinueAsync(TicketId, outcome, requestedFields);
        }
        
        public Utils.CancelWorkflows.Proxy.WF.TaskResponse Credit_Broker_ApprovalContinue(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Credit_Broker_ApprovalContinue(TicketId, outcome, requestedFields);
        }
        
        public System.Threading.Tasks.Task<Utils.CancelWorkflows.Proxy.WF.TaskResponse> Credit_Broker_ApprovalContinueAsync(string TicketId, string outcome, System.Collections.Generic.Dictionary<string, object> requestedFields) {
            return base.Channel.Credit_Broker_ApprovalContinueAsync(TicketId, outcome, requestedFields);
        }
    }
}
