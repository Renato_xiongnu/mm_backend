﻿using System.Runtime.Serialization;

namespace Orders.Backend.Services.Product.Contracts
{
    public enum ProductType
    {
        [EnumMember]
        Product = 0,

        [EnumMember]
        Set = 1,

        [EnumMember]
        WarrantyPlus = 2,

        [EnumMember]
        InstallationService = 3,

        [EnumMember]
        DeliveryService = 4,
    }
}