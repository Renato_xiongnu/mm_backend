﻿using System.Runtime.Serialization;

namespace Orders.Backend.Services.Product.Contracts
{
    [DataContract]
    public class ProductInfo
    {
        [DataMember]
        public long Article { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public ProductType ProductType { get; set; }
    }
}