﻿using System.ServiceModel;
using Orders.Backend.Services.Product.Contracts;

namespace Orders.Backend.Services.Product
{
    [ServiceContract]
    public interface IProductInfoService
    {
        [OperationContract]
        ProductInfo[] GetProductInfo(long[] articles);
    }
}