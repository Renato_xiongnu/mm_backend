﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Orders.Backend.Services.Product.Contracts;

namespace Orders.Backend.Services.Product.Bll
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ProductInfoService : IProductInfoService
    {
        private readonly IProductTypeQualifier _productTypeQualifier;

        public ProductInfoService(IProductTypeQualifierFactory productTypeQualifierFactory)
        {
            _productTypeQualifier = productTypeQualifierFactory.Create();
        }

        public ProductInfo[] GetProductInfo(long[] articles)
        {
            var result = new List<ProductInfo>();            
            foreach(var article in articles)
            {
                var productType = _productTypeQualifier.GetProductType(article);
                if(!productType.HasValue)
                {
                    throw new ApplicationException("Unknow product type");
                }                
                var productInfo = new ProductInfo
                {
                    Article = article,
                    ProductType = productType.Value,                    
                };
                result.Add(productInfo);
            }
            return result.ToArray();
        }
    }
}