﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Orders.Backend.Services.Product.Contracts;

namespace Orders.Backend.Services.Product.Bll
{
    public interface IProductTypeQualifier
    {
        ProductType? GetProductType(long article);
    }

    public class ProductTypeQualifier : IProductTypeQualifier
    {
        private readonly IProductTypeQualifier[] _qualifiers;

        public ProductTypeQualifier(params IProductTypeQualifier[] qualifiers)
        {
            _qualifiers = qualifiers;
        }

        public ProductType? GetProductType(long article)
        {
            foreach (var productTypeQualifier in _qualifiers)
            {
                var productType = productTypeQualifier.GetProductType(article);
                if(productType.HasValue)
                {
                    return productType;
                }
            }
            throw new ApplicationException("Unknown product type");
        }
    }

    public class DefaultProductTypeQualifier : IProductTypeQualifier
    {
        public ProductType? GetProductType(long article)
        {
            return ProductType.Product;
        }
    }

    public class SetProductTypeQualifier : IProductTypeQualifier
    {
        public ProductType? GetProductType(long article)
        {
            var articleString = article.ToString(CultureInfo.InvariantCulture);
            return Regex.IsMatch(articleString, @"28\d{7}") ? ProductType.Set : (ProductType?)null;
        }
    }

    public class ProductTypeQualifierByDir : IProductTypeQualifier
    {
        private readonly Dictionary<long, ProductType> _knowProducts;        

        public ProductTypeQualifierByDir(string articlesDirectory)
        {
            _knowProducts = new Dictionary<long, ProductType>();
            var articlesDirectoryAbsolutePath = HttpContext.Current.Server.MapPath(articlesDirectory);
            var directory = new DirectoryInfo(articlesDirectoryAbsolutePath);
            if (!directory.Exists)
            {
                throw new ArgumentException("Directory not exist");
            }
            var files = directory.GetFiles()                
                .ToArray();
            foreach (var file in files)
            {
                ProductType productType;
                var match = Regex.Match(file.Name, @"^(?<productType>\w+)Articles");
                if (!match.Success || !Enum.TryParse(match.Groups["productType"].Value, true, out productType))
                {
                    continue;
                }
                var articleLines = File.ReadAllLines(file.FullName);
                foreach(var articleLine in articleLines)
                {
                    long article;
                    if (!long.TryParse(articleLine, out article))
                    {
                        continue;
                    }
                    _knowProducts[article] = productType;
                }                                
            }
        }

        public ProductType? GetProductType(long article)
        {
            if (_knowProducts.ContainsKey(article))
            {
                return _knowProducts[article];
            }
            return null;
        }
    }

    public interface IProductTypeQualifierFactory
    {
        IProductTypeQualifier Create();
    }

    public class ProductTypeQualifierFactory : IProductTypeQualifierFactory
    {
        public IProductTypeQualifier Create()
        {
            return new ProductTypeQualifier(
                new ProductTypeQualifierByDir("~/Data"),
                new SetProductTypeQualifier(),
                new DefaultProductTypeQualifier());
        }
    }
}