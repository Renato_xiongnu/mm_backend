﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using Newtonsoft.Json;
using Product.Backend2.OrdersBackendServiceMock.Proxy.Obs;

namespace Product.Backend2.OrdersBackendServiceMock
{    
    public class OrdersBackendService : IOrdersBackendService
    {
        private readonly Dictionary<string, Dictionary<string, IList<OrdersBackendItem>>> _store =
            new Dictionary<string, Dictionary<string, IList<OrdersBackendItem>>>();

        static OrdersBackendService()
        {
            log4net.Config.XmlConfigurator.Configure();   
        }

        public OrdersBackendService()
        {
            var mmStore = new Dictionary<string, IList<OrdersBackendItem>>();
            _store["MM"] = mmStore;
            mmStore["shop_R201"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1000000,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть везде 10 штук"
                },
                new OrdersBackendItem
                {
                    Article = 1000201,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 201"
                },
                new OrdersBackendItem
                {
                    Article = 1000202,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 202"
                },
                new OrdersBackendItem
                {
                    Article = 1000206,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 206"
                },
                new OrdersBackendItem
                {
                    Article = 1000208,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 208"
                },
                new OrdersBackendItem
                {
                    Article = 1201601,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 201 и 601"
                },
                new OrdersBackendItem
                {
                    Article = 3000000,
                    Price = 12000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Не продается (ошибка создания WWS резерва)"
                },
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 1,
                    Status = StockStatusEnum.InStock,
                    Title = "Iphone"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1226732,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},                new OrdersBackendItem
                //{
                //    Article = 1276309,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 1,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1020674,
                //    Price = 10000,
                //    Qty = 5,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Микроволновка"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1135370,
                //    Price = 10000,
                //    Qty = 6,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Микроволновка"
                //},
                new OrdersBackendItem
                {
                    Article = 1271976,
                    Price = 10000,
                    Qty = 3,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1271977,
                //    Price = 10000,
                //    Qty = 3,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271978,
                //    Price = 10000,
                //    Qty = 3,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                new OrdersBackendItem
                {
                    Article = 280000379,
                    Price = 12300,
                    Qty = 2,
                    Status = StockStatusEnum.InStock,
                    Title = "Набор"
                }
            };
            mmStore["shop_R202"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1000000,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть везде 10 штук"
                },
                new OrdersBackendItem
                {
                    Article = 1000201,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 201"
                },
                new OrdersBackendItem
                {
                    Article = 1000202,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 202"
                },
                new OrdersBackendItem
                {
                    Article = 1000206,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 206"
                },
                new OrdersBackendItem
                {
                    Article = 1000208,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 208"
                },                
                new OrdersBackendItem
                {
                    Article = 3000000,
                    Price = 12000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Не продается (ошибка создания WWS резерва)"
                },
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 1,
                    Status = StockStatusEnum.InStock,
                    Title = "Iphone"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1226732,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},                
                //new OrdersBackendItem
                //{
                //    Article = 1225746,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1276309,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1027516,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1080434,
                //    Price = 10000,
                //    Qty = 2,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 1,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1020674,
                //    Price = 10000,
                //    Qty = 5,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Микроволновка"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1135370,
                //    Price = 10000,
                //    Qty = 6,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Микроволновка"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271976,
                //    Price = 10000,
                //    Qty = 3,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271977,
                //    Price = 10000,
                //    Qty = 3,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271978,
                //    Price = 10000,
                //    Qty = 3,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                new OrdersBackendItem
                {
                    Article = 280000379,
                    Price = 12300,
                    Qty = 2,
                    Status = StockStatusEnum.InStock,
                    Title = "Набор"
                }
            };
            mmStore["shop_R206"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1000000,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть везде 10 штук"
                },
                new OrdersBackendItem
                {
                    Article = 1000201,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 201"
                },
                new OrdersBackendItem
                {
                    Article = 1000202,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 202"
                },
                new OrdersBackendItem
                {
                    Article = 1000206,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 206"
                },
                new OrdersBackendItem
                {
                    Article = 1000208,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 208"
                },      
            };
            mmStore["shop_R208"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1000000,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть везде 10 штук"
                },
                new OrdersBackendItem
                {
                    Article = 1000201,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 201"
                },
                new OrdersBackendItem
                {
                    Article = 1000202,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 202"
                },
                new OrdersBackendItem
                {
                    Article = 1000206,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 206"
                },
                new OrdersBackendItem
                {
                    Article = 1000208,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 208"
                },      
            }; 
            mmStore["shop_R601"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1000000,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть везде 10 штук"
                },
                new OrdersBackendItem
                {
                    Article = 1000201,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 201"
                },
                new OrdersBackendItem
                {
                    Article = 1000202,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 202"
                },
                new OrdersBackendItem
                {
                    Article = 1000206,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 206"
                },
                new OrdersBackendItem
                {
                    Article = 1000208,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Есть только в 208"
                },      
                new OrdersBackendItem
                {
                    Article = 1000601,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 601"
                },
                new OrdersBackendItem
                {
                    Article = 1201601,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Есть только в 201 и 601"
                },
                new OrdersBackendItem
                {
                    Article = 2000601,
                    Price = 12000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Не продается (ошибка создания WWS резерва)"
                },
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Iphone"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1226732,
                //    Price = 10000,
                //    Qty = 10,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                //},
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },
                //new OrdersBackendItem
                //{
                //    Article = 1020674,
                //    Price = 10000,
                //    Qty = 10,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Микроволновка"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1135370,
                //    Price = 10000,
                //    Qty = 10,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Микроволновка"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271976,
                //    Price = 10000,
                //    Qty = 10,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271977,
                //    Price = 10000,
                //    Qty = 10,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                //new OrdersBackendItem
                //{
                //    Article = 1271978,
                //    Price = 10000,
                //    Qty = 10,
                //    Status = StockStatusEnum.InStock,
                //    Title = "Диктофон"
                //},
                new OrdersBackendItem
                {
                    Article = 280000379,
                    Price = 12300,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Набор"
                }
            };
            mmStore["shop_R004"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Iphone"
                },
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Микроволновка"
                }
            };
            mmStore["shop_R005"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.InStock,
                    Title = "Iphone"
                },
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },
            };
            mmStore["shop_R006"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Iphone"
                },
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },               
                new OrdersBackendItem
                {
                    Article = 1141764,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.IsBlocked,
                    Title = "Apple iPhone 4S 16Gb White Смартфон"
                },
                new OrdersBackendItem
                {
                    Article = 1226732,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.DisplayItem,
                    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                },                
                new OrdersBackendItem
                {
                    Article = 2111111,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                },
                new OrdersBackendItem
                {
                    Article = 1271976,
                    Price = 10000,
                    Qty = 7,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1271977,
                    Price = 10000,
                    Qty = 8,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1271978,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                }
            };
            mmStore["shop_R007"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.NotInStock,
                    Title = "Iphone"
                },
                new OrdersBackendItem
                {
                    Article = 1226732,
                    Price = 10000,
                    Qty = 2,
                    Status = StockStatusEnum.InStock,
                    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                },
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },
                new OrdersBackendItem
                {
                    Article = 1271976,
                    Price = 10000,
                    Qty = 3,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1271977,
                    Price = 10000,
                    Qty = 3,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1271978,
                    Price = 10000,
                    Qty = 3,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 280000379,
                    Price = 12300,
                    Qty = 3,
                    Status = StockStatusEnum.InStock,
                    Title = "Набор"
                },
                new OrdersBackendItem
                {
                    Article = 1205667,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },                
                new OrdersBackendItem
                {
                    Article = 1272277,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1134056,
                    Price = 10000,
                    Qty = 10,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },  
            };
            mmStore["shop_R002"] = new[]
            {
                new OrdersBackendItem
                {
                    Article = 1111111,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Iphone"
                },
                new OrdersBackendItem
                {
                    Article = 1159035,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Микроволновка"
                },               
                new OrdersBackendItem
                {
                    Article = 1141764,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.IsBlocked,
                    Title = "Apple iPhone 4S 16Gb White Смартфон"
                },
                new OrdersBackendItem
                {
                    Article = 1226732,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.DisplayItem,
                    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                },                
                new OrdersBackendItem
                {
                    Article = 2111111,
                    Price = 10000,
                    Qty = 5,
                    Status = StockStatusEnum.InStock,
                    Title = "Apple iPhone 5C 16Gb Pink Смартфон"
                },
                new OrdersBackendItem
                {
                    Article = 1271976,
                    Price = 10000,
                    Qty = 7,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1271977,
                    Price = 10000,
                    Qty = 8,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                },
                new OrdersBackendItem
                {
                    Article = 1271978,
                    Price = 10000,
                    Qty = 0,
                    Status = StockStatusEnum.InStock,
                    Title = "Диктофон"
                }
            };
        }

        public GetArticleStockStatusResponse GetArticleStockStatus(GetItemsRequest request)
        {
            var log = LogManager.GetLogger("OrdersBackendService");
            log.InfoFormat("Incoming request: {0}", JsonConvert.SerializeObject(request));

            var response = new GetArticleStockStatusResponse();
            if (!_store.ContainsKey(request.Channel) || !_store[request.Channel].ContainsKey(request.SaleLocation))
            {
                response.Items = request.Articles.Select(a => new OrdersBackendItem { Article = long.Parse(a) }).ToArray();

                log.InfoFormat("Out response: {0}", JsonConvert.SerializeObject(response));
                return response;
            }
            var articles = request.Articles.Select(long.Parse).ToArray();
            var store = _store[request.Channel][request.SaleLocation];
            response.Items = articles
                .Select(
                    a =>
                        store.FirstOrDefault(s => s.Article == a) ??
                        new OrdersBackendItem { Article = a })
                .ToArray();

            log.InfoFormat("Out response: {0}", JsonConvert.SerializeObject(response));
            return response;
        }

        public GetArticleMultipleStockStatusResponse GetArticleStocksStatus(GetItemsMultipleStocksRequest request)
        {
            throw new System.NotImplementedException();
        }

        public GetProductsBySaleLocationCountResponse GetProductsCountBySaleLocation()
        {
            throw new System.NotImplementedException();
        }

        public GetProductsByChannelCountResponse GetProductsCountByChannel()
        {
            throw new System.NotImplementedException();
        }

        public GetSalableArticlesResponse GetSalableArticles(GetSalableArticlesRequest request)
        {
            throw new System.NotImplementedException();
        }
    }
}
