﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Helpers;

namespace Orders.Backend.Router.Dal.Model
{
    public partial class History
    {
        public RecTypeEnum RecTypeEnum
        {
            get { return (RecTypeEnum) RecType; }
        }

        public string RecTypeEnumStr
        {
            get { return RecTypeEnum.GetDescription(); }
        }
    }
}
