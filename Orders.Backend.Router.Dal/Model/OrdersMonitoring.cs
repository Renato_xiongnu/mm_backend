﻿using System;
using System.Linq;
using Common.Helpers;
using Order.Backend.Dal.Common;

namespace Orders.Backend.Router.Dal.Model
{
    public partial class OrderMonitoring
    {
        public const string PROCESS_END_STATE = "Завершено";

        public DateTime CreateDay
        {
            get { return CreateDate.Date; }
        }

        public OrderPaymentType PaymentTypeEnum
        {
            get { return PaymentType == null ? OrderPaymentType.Unknown : (OrderPaymentType) PaymentType; }
        }

        public InternalOrderStatus OrderStatusEnum
        {
            get { return OrderStatus == null ? InternalOrderStatus.Blank : (InternalOrderStatus) OrderStatus; }
        }

        public string Process
        {
            get { return CloseDate == null ? TaskName : PROCESS_END_STATE; }
        }

        public DateTime? SmartCloseDate
        {
            get { return CloseDate ?? TaskCreationDate; }
        }

        public DeliveryTypeEnum Delivery
        {
            get { return HasDelivery != 0 ? DeliveryTypeEnum.Delivery : DeliveryTypeEnum.SelfDelivery; }
        }

        public bool HasOutcome
        {
            get { return ! string.IsNullOrEmpty(TaskName) && TaskName.Contains(':'); }
        }

        public string PaymentTypeDescription
        {
            get { return PaymentType == null ? OrderPaymentType.Unknown.GetDescription(): ((OrderPaymentType)PaymentType).GetDescription(); }
        }

        public string OrderStatusDescription
        {
            get { return OrderStatus == null ? InternalOrderStatus.Blank.GetDescription() : ((InternalOrderStatus)OrderStatus).GetDescription(); }
        }
        
        public string DeliveryDescription
        {
            get { return HasDelivery !=0 ? DeliveryTypeEnum.Delivery.GetDescription() : DeliveryTypeEnum.SelfDelivery.GetDescription(); }
        }
    }
}