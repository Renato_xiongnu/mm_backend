﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Orders.Backend.Services.Router.Dal.Model
{
    public class RouterDbContext : DbContext
    {
        public DbSet<RoutableOrder> RoutableOrders { get; set; }

        public DbSet<History> Histories { get; set; }
    }
}