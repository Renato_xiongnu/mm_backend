﻿using Common.Helpers;

namespace Orders.Backend.Router.Dal.Model
{
    public enum RecTypeEnum
    {
        [EnumFieldDescription("Ошибка")]
        MistakeCall,
        [EnumFieldDescription("ОК")]
        Ok,
        [EnumFieldDescription("Исключение")]
        Exception
    }

    public enum DeliveryTypeEnum
    {
        [EnumFieldDescription("Доставка")]
        Delivery,
        [EnumFieldDescription("Самовывоз")]
        SelfDelivery
    }

}