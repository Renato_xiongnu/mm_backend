@rem working dir
@cd %1
@set server=%2
@set country=%3

@echo  create local db
@sqlcmd -S %server% -E -v country="%country%" -i 01_CreateDb.sql, 02_CreateTables.sql, 03_CreateViews.sql, 21_CreateStoredProcedures.sql, 50_GrantPermissions.sql

@pause