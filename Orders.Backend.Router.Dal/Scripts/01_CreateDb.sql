﻿
print 'CreateDb RoutableOrderDb$(country)'
USE [master]
if db_id('RoutableOrderDb$(country)') is not null
	BEGIN
		EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'RoutableOrderDb$(country)'
		ALTER DATABASE [RoutableOrderDb$(country)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		ALTER DATABASE [RoutableOrderDb$(country)] SET SINGLE_USER
		DROP DATABASE [RoutableOrderDb$(country)]
	END
GO

CREATE DATABASE [RoutableOrderDb$(country)]
GO

ALTER DATABASE [RoutableOrderDb$(country)] SET RECOVERY SIMPLE WITH NO_WAIT
GO

ALTER DATABASE [RoutableOrderDb$(country)] SET ALLOW_SNAPSHOT_ISOLATION ON
GO

ALTER DATABASE [RoutableOrderDb$(country)] SET READ_COMMITTED_SNAPSHOT ON
GO

-- NOTE [sg]: in order to fix http://mediamarkt.jira.com/browse/GMSREPORTING-164 we need to enable deadlocks logging
DBCC TRACEON (1222, 3605, -1)
GO


--IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'OnlineOrderLogin$(country)')
--	DROP LOGIN [OnlineOrderLogin$(country)]
--GO

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'RoutableOrderLogin$(country)')
CREATE LOGIN [RoutableOrderLogin$(country)] WITH PASSWORD=N'Rep@rting1', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO




USE [RoutableOrderDb$(country)]
go
CREATE USER [RoutableOrderUser] FOR LOGIN RoutableOrderLogin$(country) WITH DEFAULT_SCHEMA=dbo
EXEC sp_addrolemember N'db_datareader', RoutableOrderUser
EXEC sp_addrolemember N'db_datawriter', RoutableOrderUser
EXEC sp_addrolemember N'db_owner', RoutableOrderUser
GO


USE [TicketToolDb$(country)]
go
CREATE USER [RoutableOrderUser] FOR LOGIN RoutableOrderLogin$(country) WITH DEFAULT_SCHEMA=dbo
EXEC sp_addrolemember N'db_datareader', RoutableOrderUser
GO

USE [OnlineOrders$(country)]
go
CREATE USER [RoutableOrderUser] FOR LOGIN RoutableOrderLogin$(country) WITH DEFAULT_SCHEMA=dbo
EXEC sp_addrolemember N'db_datareader', RoutableOrderUser
GO