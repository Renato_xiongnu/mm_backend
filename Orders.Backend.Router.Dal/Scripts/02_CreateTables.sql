﻿print 'CreateTables'

USE [RoutableOrderDb$(country)]
GO
/****** Object:  Table [dbo].[RoutableOrders]    Script Date: 12/18/2012 16:46:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoutableOrders](
	[OrderId] [nvarchar](128) NOT NULL,
	[SapCode] [nvarchar](4) NULL,
	[IsCallSuccessful] [bit] NOT NULL,
	[IsProcessOrder] [bit] NOT NULL,
	[OrderSource] [nvarchar](255) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[SerializedOrder] [nvarchar](max) NULL,
	[ProcessVersion] [nvarchar](255) NULL,
	[ShouldBeProcessed] bit NOT NULL DEFAULT(0) 
 CONSTRAINT [PK_dbo.RoutableOrders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Histories]    Script Date: 12/18/2012 16:46:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Histories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[RecType] [int] NOT NULL,
	[Text] [nvarchar](max) NULL,
	[OrderId] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.Histories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Histories_RoutableOrders]    Script Date: 12/18/2012 16:46:27 ******/
ALTER TABLE [dbo].[Histories]  WITH CHECK ADD  CONSTRAINT [FK_Histories_RoutableOrders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[RoutableOrders] ([OrderId])
GO
ALTER TABLE [dbo].[Histories] CHECK CONSTRAINT [FK_Histories_RoutableOrders]
GO
