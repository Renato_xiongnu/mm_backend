﻿print 'GrantPermissions'
USE [RoutableOrderDb$(country)]
GO

GRANT EXECUTE ON [dbo].[ConfirmedOrders] TO [RoutableOrderUser];
GRANT EXECUTE ON [dbo].[OrderLines] TO [RoutableOrderUser];
GRANT EXECUTE ON [dbo].[Orders] TO [RoutableOrderUser];
GRANT EXECUTE ON [dbo].[rp_OrdersByDayOfMonth] TO [RoutableOrderUser];
GRANT EXECUTE ON [dbo].[rp_OrdersByDayOfWeek] TO [RoutableOrderUser];
GRANT EXECUTE ON [dbo].[rp_OrdersByHours] TO [RoutableOrderUser];

GRANT SELECT ON [dbo].[ProcessedOrders] TO [RoutableOrderUser];
RECONFIGURE

GRANT EXECUTE ON [TicketToolDb$(country)].[dbo].[rp_Tasks] TO [RoutableOrderUser];
GRANT EXECUTE ON [TicketToolDb$(country)].[dbo].[rp_TaskStatistics] TO [RoutableOrderUser];
GRANT EXECUTE ON [TicketToolDb$(country)].[dbo].[rp_UserSkillSet] TO [RoutableOrderUser];
GRANT EXECUTE ON [TicketToolDb$(country)].[dbo].[rp_TasksByHours] TO [RoutableOrderUser];

GO
