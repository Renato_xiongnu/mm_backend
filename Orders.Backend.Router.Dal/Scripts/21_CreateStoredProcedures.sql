﻿print 'Create stored procedures'

USE [RoutableOrderDb$(country)]
GO
/****** Object:  StoredProcedure [dbo].[ConfirmedOrders]    Script Date: 04/01/2013 17:16:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConfirmedOrders] 
@date1 AS DATETIMEOFFSET,
@date2 AS DATETIMEOFFSET
AS
BEGIN
SELECT h.OrderId as OnlineOrder,
		 h.WWSOrderId as ReserveNumber,
		 h.OrderStatus,
		 r.CreateDate,
		 (CASE 
		 WHEN h.OrderStatus >=2 AND h.OrderStatus < 4 THEN N'Confirmed' 
		 WHEN h.OrderStatus >=4 AND h.OrderStatus <= 100 THEN N'Paid'
		 ELSE N'Canceled'
		 END) AS StatusStr,		 
  SUM(rl.Price * rl.Qty) as Amount
  FROM OnlineOrders$(country).dbo.OrderHeader h
  INNER JOIN RoutableOrderDb$(country).dbo.RoutableOrders r ON h.OrderId = r.OrderId and r.IsProcessOrder = 1
  INNER JOIN  OnlineOrders$(country).dbo.ReserveHeader rh ON rh.OrderId = h.OrderId
  INNER JOIN  OnlineOrders$(country).dbo.ReserveLine rl ON rl.ReserveId = rh.ReserveId  
  WHERE  r.CreateDate > SWITCHOFFSET(@date1, '+00:00') AND r.CreateDate <= SWITCHOFFSET(@date2, '+00:00') AND h.WWSOrderId IS NOT NULL
  AND EXISTS(SELECT NULL FROM OnlineOrders$(country).dbo.OrderStatusHistory WHERE OrderId = h.OrderId AND ToStatus = 2)
  GROUP BY h.OrderId, h.WWSOrderId, h.OrderStatus, r.CreateDate
 END
GO
/****** Object:  StoredProcedure [dbo].[rp_OrdersByDayOfMonth]    Script Date: 09/06/2013 14:47:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rp_OrdersByDayOfMonth] 
-- TicketTool.OrdersByDayOfMonth
@StartDate AS DATETIME,
@EndDate AS DATETIME,
@OffsetInMinutes AS INTEGER = 240
AS
BEGIN 
SELECT @StartDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartDate) AS DATETIME)
SELECT @EndDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndDate) AS DATETIME)
SELECT 
      OrderSource,
      DATEPART(DAY, DATEADD(MINUTE, @OffsetInMinutes, CreateDate)) CreateDay,
      COUNT(*) OrdersCount
  FROM RoutableOrders
  WHERE CreateDate >= @StartDate AND CreateDate < @EndDate
  GROUP BY OrderSource, DATEPART(DAY, DATEADD(MINUTE, @OffsetInMinutes, CreateDate))
  ORDER BY 1, 2
END
GO
/****** Object:  StoredProcedure [dbo].[rp_OrdersByDayOfWeek]    Script Date: 09/06/2013 14:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rp_OrdersByDayOfWeek]
-- TicketTool.OrdersByDayOfWeek
@StartDate AS DATETIME,
@EndDate AS DATETIME,
@OffsetInMinutes AS INTEGER = 240
AS
BEGIN 
SELECT @StartDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartDate) AS DATETIME)
SELECT @EndDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndDate) AS DATETIME)
SELECT 
      OrderSource,
      DATEPART(WEEKDAY, DATEADD(MINUTE, @OffsetInMinutes, CreateDate)) CreateDay,
      COUNT(*) OrdersCount
  FROM RoutableOrders
  WHERE CreateDate >= @StartDate AND CreateDate < @EndDate
  GROUP BY OrderSource, DATEPART(WEEKDAY, DATEADD(MINUTE, @OffsetInMinutes, CreateDate))
  ORDER BY 1, 2
END
GO
/****** Object:  StoredProcedure [dbo].[rp_OrdersByHours]    Script Date: 09/06/2013 14:48:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rp_OrdersByHours] 
-- TicketTool.OrdersByHours
@StartDate AS DATETIME,
@EndDate AS DATETIME,
@OffsetInMinutes AS INTEGER = 240
AS
BEGIN 
SELECT @StartDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartDate) AS DATETIME)
SELECT @EndDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndDate) AS DATETIME)

;WITH tt AS (
    SELECT 
	    OrderSource,
	    DATEPART(HOUR, CreateDate)+FLOOR(@OffsetInMinutes/60) CreateHour,
	    COUNT(*) OrdersCount
    FROM RoutableOrders
    WHERE CreateDate >= @StartDate AND CreateDate < @EndDate
    GROUP BY OrderSource, DATEPART(HOUR, CreateDate)
    )
SELECT o.OrderSource, h.v CreateHour, ISNULL(tt.OrdersCount, 0) OrdersCount 
FROM (VALUES (0), (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), 
		(12), (13), (14), (15), (16), (17), (18), (19), (20), (21), (22), (23)) AS h (v)
CROSS JOIN (SELECT DISTINCT OrderSource FROM tt) o
LEFT JOIN tt ON tt.CreateHour = h.v AND tt.OrderSource = o.OrderSource

END
GO