﻿print 'CreateViews'

USE [RoutableOrderDb$(country)]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  View [dbo].[OrderMonitoring]    Script Date: 05/08/2014 10:48:13 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[OrderMonitoring]'))
DROP VIEW [dbo].[OrderMonitoring]
GO

/****** Object:  View [dbo].[OrderMonitoring]    Script Date: 01/30/2014 15:15:13 ******/
CREATE view [dbo].[OrderMonitoring] 
AS
SELECT 
		 ro.[OrderId]		 
		,oh.[SapCode]
		,ro.[IsCallSuccessful]
		,ro.[IsProcessOrder]
		,ro.ShouldBeProcessed
		,ro.CreateDate
		,ro.OrderSource
		
		,c.Name + ' ' + c.Surname as CustomerName
		,c.Phone as CustomerPhone
		,c.Email as CustomerEmail
		
		,d.CustomerId as HasDelivery
		,d.Address
		,d.DeliveryDate
		
		,oh.PaymentType
		,oh.WWSOrderId
		,oh.OrderStatus
		,oh.Comment

		,(select WWSOrderId + ' '
		  from [OnlineOrders$(country)].dbo.ReserveHeader rh WITH (NOLOCK)
		  where rh.OrderId = oh.OrderId
		  FOR XML PATH('')
		  ) as WWSOrders
		
		,oh.GiftCertificateId
		
		,t.CloseDate
		,ts2.CurrentOutcome as TaskName
		,ts2.CreateDate as TaskCreationDate	
		,(select SUM(Price * Qty)
		  from [OnlineOrders$(country)].dbo.ReserveLine rl WITH (NOLOCK)
		  inner join [OnlineOrders$(country)].dbo.ReserveHeader rh WITH (NOLOCK) on rh.ReserveId=rl.ReserveId
		  where rh.OrderId = oh.OrderId and rh.WWSOrderId = oh.WWSOrderId) as TotalPrice

	FROM [RoutableOrderDb$(country)].[dbo].[RoutableOrders] ro WITH (NOLOCK)
	left join [OnlineOrders$(country)].dbo.OrderHeader oh WITH (NOLOCK) on oh.OrderId=ro.[OrderId]
	left join [OnlineOrders$(country)].dbo.Customer c  WITH (NOLOCK)on c.CustomerId=oh.CustomerId	
	left join (
		select
			CustomerId, OrderId, DeliveryDate,
			isnull(City + ' ', '') + isnull([Address], '') as [Address]
			from [OnlineOrders$(country)].dbo.Delivery WITH (NOLOCK)
		) d on d.CustomerId = c.CustomerId and d.OrderId = ro.OrderId
	left join [TicketToolDb$(country)].dbo.Tickets t WITH (NOLOCK) on t.WorkItemId=ro.[OrderId]	
	left join (select CASE
				WHEN tt.CurrentOutcome IS NULL THEN tt.Name
				ELSE CAST(tt.Name AS NVARCHAR(1000)) + ' : ' + tt.CurrentOutcome END CurrentOutcome, tt.CreateDate, tt.Ticket_TicketId 
		  from [TicketToolDb$(country)].dbo.TicketTasks tt WITH (NOLOCK) 
		  INNER JOIN (SELECT Ticket_TicketId, MAX(UpdateDate) UpdateDate FROM [TicketToolDb$(country)].dbo.TicketTasks WITH (NOLOCK) GROUP BY Ticket_TicketId) tt2 
		    ON tt.Ticket_TicketId = tt2.Ticket_TicketId AND tt.UpdateDate = tt2.UpdateDate) ts2 ON ts2.Ticket_TicketId = t.TicketId
    WHERE ro.CreateDate > '2014-02-01';
GO

/****** Object:  View [dbo].[ProcessedOrders]    Script Date: 09/04/2013 15:11:27 ******/
CREATE VIEW [dbo].[ProcessedOrders]
-- orders with tickets (processed by TicketTool) with UTC Time
-- base for some reports 
AS
SELECT
h.SapCode, 
s.Name ShopName,
r.CreateDate,
(SELECT DATEADD(HOUR, -1, MIN(UpdateDate)) FROM OnlineOrders$(country).dbo.OrderStatusHistory sh2 WITH (NOLOCK) WHERE sh2.OrderId = h.OrderId 
	AND ((sh2.ToStatus = 4 AND h.OrderStatus IN (4, 100)) OR (sh2.ToStatus = h.OrderStatus AND h.OrderStatus IN (101, 102)))) AS CloseDate,
h.OrderId,
rl.ArticleNum,
rl.Qty AS Quantity,
rl.Qty * rl.Price AS Value,
h.WWSOrderId,
rl.Promotions,
sh.ReasonText RejectionReason,
r.OrderSource,
h.CustomerId,
h.GiftCertificateId,
OrderStatus = CASE h.OrderStatus WHEN 4 THEN N'Оплачен' WHEN 100 THEN N'Оплачен' WHEN 101 THEN N'Отменен' WHEN 102 THEN N'Отменен' ELSE N'Незавершен' END,
rl.WWSProductGroupNo,
rl.WWSProductGroup,
rl.Title AS ArticleTitle
FROM OnlineOrders$(country).dbo.OrderHeader h WITH (NOLOCK)
INNER JOIN RoutableOrderDb$(country).dbo.RoutableOrders r WITH (NOLOCK) ON h.OrderId = r.OrderId
INNER JOIN OnlineOrders$(country).dbo.ReserveHeader rh WITH (NOLOCK) ON rh.OrderId = h.OrderId AND (h.WWSOrderId = rh.WWSOrderId OR h.WWSOrderId IS NULL)
INNER JOIN OnlineOrders$(country).dbo.ReserveLine rl WITH (NOLOCK) ON rl.ReserveId = rh.ReserveId  
INNER JOIN OnlineOrders$(country).dbo.OrderStatusHistory sh WITH (NOLOCK) ON sh.OrderId = h.OrderId AND sh.ToStatus = h.OrderStatus AND sh.UpdateDate = h.UpdateDate
INNER JOIN TicketToolDb$(country).dbo.Stores s WITH (NOLOCK) ON s.SapCode = h.SapCode
WHERE h.OrderStatus <> 75 AND r.IsProcessOrder = 1
GO
/****** Object:  View [dbo].[RoutableAvitoOrders]    Script Date: 09/04/2013 15:11:27 ******/
CREATE VIEW [dbo].[RoutableAvitoOrders]
AS
SELECT rout.OrderId as RoutableOrderId, rout.CreateDate AS RoutableCreateDate, rout.SapCode AS RoutableSapCode, avito.*
  FROM [RoutableOrderDb$(country)].[dbo].[RoutableOrders] AS rout
  LEFT OUTER JOIN [OnlineOrders$(country)].[dbo].[AvitoOrders] AS avito ON rout.OrderId = avito.OrderId
  WHERE rout.OrderSource = N'Avito'
GO
/****** Object:  View [dbo].[Orders]    Script Date: 09/04/2013 15:38:31 ******/
CREATE VIEW [dbo].[Orders]
-- Orders.Report2
AS 
SELECT * FROM 
(SELECT  
    o.SapCode,
    o.ShopName,
    CAST(DATEADD(HOUR, 4, CreateDate) AS DATE) CreateDate,
    CAST(DATEADD(HOUR, 4, CloseDate)  AS DATE) CloseDate,
    o.OrderId,
    SUM(Quantity) Quantity,
    SUM(Value) Value,
    WWSOrderId,
    CASE WHEN o.OrderStatus <> N'Отменен' THEN NULL 
         WHEN RejectionReason IN (SELECT ReasonText FROM OnlineOrders$(country).dbo.OrderStatusHistory WITH (NOLOCK) 
                                  WHERE ToStatus > 100 GROUP BY ReasonText HAVING COUNT(*) > 100) 
                THEN RejectionReason 
        ELSE N'Другое' END RejectionReason,
    OrderSource,
    Delivery = CASE WHEN isnull(d.CustomerId, zd.CustomerId) IS NULL 
            THEN N'Самовывоз' 
            ELSE N'Доставка' END,
    OrderStatus ,
	(SELECT TOP 1 tt.Name 
		FROM (SELECT Ticket_TicketId, MAX(q.CreateDate) TaskCreateDate FROM TicketToolDb$(country).dbo.TicketTasks q WITH (NOLOCK) 
            WHERE q.CreateDate < ISNULL(o.CloseDate, '1900-01-01')
            GROUP BY q.Ticket_TicketId) tt1
    INNER JOIN TicketToolDb$(country).dbo.TicketTasks tt WITH (NOLOCK) ON tt1.Ticket_TicketId = tt.Ticket_TicketId AND tt.CreateDate = tt1.TaskCreateDate
    INNER JOIN TicketToolDb$(country).dbo.Tickets t WITH (NOLOCK) ON t.TicketId = tt.Ticket_TicketId 
    WHERE t.WorkItemId = o.OrderId AND o.OrderStatus = N'Отменен') TaskName,
    g.Amount+1 AS GiftCertificateAmount
FROM RoutableOrderDb$(country).dbo.ProcessedOrders o
LEFT JOIN OnlineOrders$(country).dbo.GiftCertificate g WITH (NOLOCK) ON g.GiftCertificateId = o.GiftCertificateId
	LEFT JOIN OnlineOrders$(country).dbo.Delivery d WITH (NOLOCK) on d.CustomerId = o.CustomerId and d.OrderId = o.OrderId
	LEFT JOIN (
		SELECT
			zc.CustomerId, zo.OrderId
			FROM [OnlineOrders$(country)].dbo.ZZTOrderHeader zo WITH (NOLOCK)
			INNER JOIN [OnlineOrders$(country)].dbo.ZZTCustomer zc  WITH (NOLOCK)ON zc.Id = zo.CustomerId
			INNER JOIN [OnlineOrders$(country)].dbo.ZZTDeliveryInfo di  WITH (NOLOCK)ON di.OrderId = zo.OrderId AND ISNULL(di.DeliveryType, 20) <> 20
		) zd ON zd.CustomerId = o.CustomerId AND zd.OrderId = o.OrderId	
GROUP BY o.SapCode, o.CreateDate, o.CloseDate, o.OrderId, o.WWSOrderId, o.RejectionReason, o.OrderSource, OrderStatus, g.Amount, d.CustomerId, zd.CustomerId, o.ShopName ) t
GO
/****** Object:  View [dbo].[OrderLines]    Script Date: 09/04/2013 15:40:43 ******/
CREATE VIEW [dbo].[OrderLines] 
--Orders.Report1
AS
SELECT 
    SapCode,
    ShopName, 
    CAST(DATEADD(HOUR, 4, CreateDate) AS DATE) CreateDate,
    CAST(DATEADD(HOUR, 4, CloseDate)  AS DATE) CloseDate,
    OrderId,
    OrderSource,
    ArticleNum,
    Quantity,
    Value,
    WWSOrderId,
    Promotions,
    OrderStatus,
    WWSProductGroupNo,
    WWSProductGroup,
    ArticleTitle
FROM ProcessedOrders
GO
