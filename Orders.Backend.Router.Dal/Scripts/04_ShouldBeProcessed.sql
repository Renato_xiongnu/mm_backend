﻿ALTER TABLE [RoutableOrders] ADD ShouldBeProcessed bit NOT NULL DEFAULT(0)

UPDATE [RoutableOrders]
SET	ShouldBeProcessed = 1
WHERE IsProcessOrder=1