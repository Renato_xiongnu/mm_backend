﻿USE [RoutableOrderDb_TEST]
GO

/****** Object:  View [dbo].[OrderMonitoring]    Script Date: 12/08/2014 13:08:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[OrderMonitoring] 
AS
SELECT     ro.[OrderId], oh.[SapCode], ro.[IsCallSuccessful], ro.[IsProcessOrder], ro.ShouldBeProcessed, ro.CreateDate, ISNULL(oh.OrderSource, ro.OrderSource) AS OrderSource, 
                      c.Name + ' ' + c.Surname AS CustomerName, c.Phone AS CustomerPhone, c.Email AS CustomerEmail, CAST(d .HasDelivery AS BIGINT) AS HasDelivery, d .Address, d .DeliveryDate, 
                      oh.PaymentType, oh.WWSOrderId, oh.OrderStatus, oh.Comment,
                          (SELECT     WWSOrderId + ' '
                            FROM          [OnlineOrders_TEST].dbo.ReserveHeader rh WITH (NOLOCK)
                            WHERE      rh.OrderId = oh.OrderId FOR XML PATH('')) AS WWSOrders, oh.GiftCertificateId, t .CloseDate, ts2.CurrentOutcome AS TaskName, ts2.CreateDate AS TaskCreationDate,
                          (SELECT     SUM(Price * Qty)
                            FROM          [OnlineOrders_TEST].dbo.ReserveLine rl WITH (NOLOCK) INNER JOIN
                                                   [OnlineOrders_TEST].dbo.ReserveHeader rh WITH (NOLOCK) ON rh.ReserveId = rl.ReserveId
                            WHERE      rh.OrderId = oh.OrderId AND rh.WWSOrderId = oh.WWSOrderId) AS TotalPrice, oh.BasketId, CASE d .HasDelivery WHEN 0 THEN p.OperatorPupId + ', ' + p.Title ELSE NULL 
                      END AS PickupAddress, oh.ExpirationDatePostponed, oh.ExpirationDate,  d.RequestedDeliveryTimeslot
FROM         [RoutableOrderDb_TEST].[dbo].[RoutableOrders] ro WITH (NOLOCK) LEFT JOIN
                      [OnlineOrders_TEST].dbo.OrderHeader oh WITH (NOLOCK) ON oh.OrderId = ro.[OrderId] LEFT JOIN
                      [OnlineOrders_TEST].dbo.Customer c WITH (NOLOCK) ON c.CustomerId = oh.CustomerId LEFT JOIN
                          (SELECT     CustomerId, OrderId, DeliveryDate, PickupLocationId, HasDelivery, RequestedDeliveryTimeslot, CASE HasDelivery WHEN 1 THEN isnull(City + ' ', '') + isnull([Address], '') ELSE NULL END AS [Address]
                            FROM          [OnlineOrders_TEST].dbo.Delivery WITH (NOLOCK)) d ON d .CustomerId = c.CustomerId AND d .OrderId = ro.OrderId LEFT JOIN
                      [OnlineOrders_TEST].dbo.PickupLocation p WITH (NOLOCK) ON p.PickupLocationId = d .PickupLocationId LEFT JOIN
                      [TicketToolDb_TEST].dbo.Tickets t WITH (NOLOCK) ON t .WorkItemId = ro.[OrderId] LEFT JOIN
                          (SELECT     CASE WHEN tt.CurrentOutcome IS NULL THEN tt.Name ELSE CAST(tt.Name AS NVARCHAR(1000)) + ' : ' + tt.CurrentOutcome END CurrentOutcome, tt.CreateDate, tt.Ticket_TicketId
                            FROM          [TicketToolDb_TEST].dbo.TicketTasks tt WITH (NOLOCK) INNER JOIN
                                                       (SELECT     Ticket_TicketId, MAX(UpdateDate) UpdateDate
                                                         FROM          [TicketToolDb_TEST].dbo.TicketTasks WITH (NOLOCK)
                                                         GROUP BY Ticket_TicketId) tt2 ON tt.Ticket_TicketId = tt2.Ticket_TicketId AND tt.UpdateDate = tt2.UpdateDate) ts2 ON ts2.Ticket_TicketId = t .TicketId
WHERE     ro.CreateDate > '2014-07-01';


GO


