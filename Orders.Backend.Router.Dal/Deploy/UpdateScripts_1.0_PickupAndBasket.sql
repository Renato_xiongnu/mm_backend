﻿USE [RoutableOrderDb_TEST]
GO

/****** Object:  View [dbo].[OrderMonitoring]    Script Date: 12/08/2014 13:08:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[OrderMonitoring] 
AS
SELECT 
		 ro.[OrderId]		 
		,oh.[SapCode]
		,ro.[IsCallSuccessful]
		,ro.[IsProcessOrder]
		,ro.ShouldBeProcessed
		,ro.CreateDate
		,ISNULL(oh.OrderSource,ro.OrderSource) as OrderSource
		
		,c.Name + ' ' + c.Surname as CustomerName
		,c.Phone as CustomerPhone
		,c.Email as CustomerEmail
		
		,CAST(d.HasDelivery AS BIGINT) as HasDelivery
		,d.Address
		,d.DeliveryDate
		
		,oh.PaymentType
		,oh.WWSOrderId
		,oh.OrderStatus
		,oh.Comment

		,(select WWSOrderId + ' '
		  from [OnlineOrders_TEST].dbo.ReserveHeader rh WITH (NOLOCK)
		  where rh.OrderId = oh.OrderId
		  FOR XML PATH('')
		  ) as WWSOrders
		
		,oh.GiftCertificateId
		
		,t.CloseDate
		,ts2.CurrentOutcome as TaskName
		,ts2.CreateDate as TaskCreationDate	
		,(select SUM(Price * Qty)
		  from [OnlineOrders_TEST].dbo.ReserveLine rl WITH (NOLOCK)
		  inner join [OnlineOrders_TEST].dbo.ReserveHeader rh WITH (NOLOCK) on rh.ReserveId=rl.ReserveId
		  where rh.OrderId = oh.OrderId and rh.WWSOrderId = oh.WWSOrderId) as TotalPrice,
		  oh.BasketId,
		  CASE d.HasDelivery WHEN 0 THEN RTRIM(p.OperatorPupId) + ', '+ p.Title ELSE NULL END as PickupAddress

	FROM [RoutableOrderDb_TEST].[dbo].[RoutableOrders] ro WITH (NOLOCK)
	left join [OnlineOrders_TEST].dbo.OrderHeader oh WITH (NOLOCK) on oh.OrderId=ro.[OrderId]
	left join [OnlineOrders_TEST].dbo.Customer c  WITH (NOLOCK)on c.CustomerId=oh.CustomerId	
	left join (
		select
			CustomerId, OrderId, DeliveryDate, PickupLocationId, HasDelivery,
			CASE HasDelivery WHEN 1 THEN isnull(City + ' ', '') + isnull([Address], '') ELSE NULL END AS [Address]
			from [OnlineOrders_TEST].dbo.Delivery WITH (NOLOCK)
		) d on d.CustomerId = c.CustomerId and d.OrderId = ro.OrderId
	left join [OnlineOrders_TEST].dbo.PickupLocation p WITH (NOLOCK) on p.PickupLocationId=d.PickupLocationId
	left join [TicketToolDb_TEST].dbo.Tickets t WITH (NOLOCK) on t.WorkItemId=ro.[OrderId]	
	left join (select CASE
				WHEN tt.CurrentOutcome IS NULL THEN tt.Name
				ELSE CAST(tt.Name AS NVARCHAR(1000)) + ' : ' + tt.CurrentOutcome END CurrentOutcome, tt.CreateDate, tt.Ticket_TicketId 
		  from [TicketToolDb_TEST].dbo.TicketTasks tt WITH (NOLOCK) 
		  INNER JOIN (SELECT Ticket_TicketId, MAX(UpdateDate) UpdateDate FROM [TicketToolDb_TEST].dbo.TicketTasks WITH (NOLOCK) GROUP BY Ticket_TicketId) tt2 
		    ON tt.Ticket_TicketId = tt2.Ticket_TicketId AND tt.UpdateDate = tt2.UpdateDate) ts2 ON ts2.Ticket_TicketId = t.TicketId
    WHERE ro.CreateDate > '2014-07-01';


GO


