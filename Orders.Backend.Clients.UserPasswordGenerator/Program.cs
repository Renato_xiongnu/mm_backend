﻿using System;
using System.Linq;
using System.Text;
using Orders.Backend.Clients.UserPasswordGenerator.Proxy.Notification;
using Orders.Backend.Clients.UserPasswordGenerator.Proxy.TicketToolAdmin;
using Orders.Backend.Clients.UserPasswordGenerator.Proxy.TicketToolAuth;
using Orders.Backend.Clients.Web.Common;

namespace Orders.Backend.Clients.UserPasswordGenerator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var password1 = GetDefaultPassword("ManagerR201");
            ////return;
            var sbPasswords = new StringBuilder();

            //var stores = new[] {"R266"};
            var users = GetAllUsers();
            var roleUsers = users
                .SelectMany(u => u.Roles.Select(r => new {Role = GetRoleDescription(r), User = u}))
                .SelectMany(u => u.User.Parameters.Select(r => new
                {
                    u.Role,
                    u.User,
                    r.ParameterName,
                    DafaultUserName = string.Format("{0}{1}", u.Role, r.ParameterName)
                }))
                .Where(o => string.Equals(o.User.Name, o.DafaultUserName, StringComparison.InvariantCultureIgnoreCase))
                .GroupBy(o => new
                {
                    o.Role,
                    o.ParameterName
                })
                .ToDictionary(o => o.Key, o => o.First().User);
            users = roleUsers
               // .Where(ru => stores.Contains(ru.Key.ParameterName))
                .Select(kv => kv.Value).ToArray();


            //var users1 = GetAllUsers().Where(u => args.Any(arg=>string.Join("",arg,u.)));            
            foreach(var user in users)
            {                
                var password = GetDefaultPassword(user.Name);
                var message = string.Join(";", user.Name, password, user.Email);
                sbPasswords.AppendLine(message);
                //using(var authClient = new AuthenticationServiceClient())
                //{
                //    if(!authClient.LogIn(user.Name, password))
                //    {
                //        var userUpdated = authClient.UpdateUser(user.Name, user.Email, password, user.Roles,
                //       user.Parameters == null
                //           ? new string[0]
                //           : user.Parameters.Select(p => p.ParameterName).ToArray(),
                //       user.AvailableSkillSets);
                //        if (!userUpdated)
                //        {
                //            Console.WriteLine("Не смогли обновит данные пользователя");
                //            continue;
                //        }
                //    }
                  
                   
                //    //SendNewPassword(user, password, sbPasswords);
                //}
            }
            Console.WriteLine("Ready");
            var foo = sbPasswords.ToString();
            var bar = foo;
            Console.Read();
            foreach(var userPrefix in args)
            {
            }
        }

        private static bool SendNewPassword(User user, string password, StringBuilder sbPasswords)
        {
            var message = string.Join(";", user.Name, password, user.Email);
            sbPasswords.AppendLine(message);
            try
            {
                Console.WriteLine(user.Email);
                var plainText = new StringBuilder();
                plainText.AppendFormat("Добрый день,{0}!\r\n", user.Name);
                plainText.AppendFormat("Ваш пароль изменен.\r\n");
                plainText.AppendFormat("Имя пользователя: {0}\r\nПароль: {1}\r\n\r\n", user.Name, password);
                plainText.AppendFormat("Список своих задач можно посмотреть здесь: {0}\r\n", "https://orders.backend.mediasaturnrussia.ru/ru/Clients/ManagerUI");
                var htmlText = new StringBuilder();
                htmlText.AppendFormat("Добрый день,{0}!<br />", user.Name);
                htmlText.AppendFormat("Ваш пароль изменен.<br />");
                htmlText.AppendFormat("Имя пользователя: {0}<br />Пароль: <b>{1}<b/><br /><br />", user.Name, password);
                htmlText.AppendFormat("Список своих задач можно посмотреть здесь: {0}<br />", "https://orders.backend.mediasaturnrussia.ru/ru/Clients/ManagerUI");

                using(var client = new NotificationServiceClient())
                {
                    client.SendEmail(new SendEmailSettings
                    {                        
                        To = new[] {user.Email},
                        Header = string.Format("Ваш пароль в ТикетТуле изменен"),
                        Subject = string.Format("Ваш пароль в ТикетТуле изменен"),
                        PlainText = plainText.ToString(),
                        HtmlText = htmlText.ToString()
                    });
                }
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }            
        }

        private static User[] GetAllUsers()
        {
            using(var ticketToolAdminClient = new TicketToolAdminClient())
            {
                return ticketToolAdminClient.GetAllUsers();
            }
        }

        private static string GetDefaultPassword(string userName)
        {
            return PasswordHelper.GetDefaultPassword(userName);
        }

        private static string GetRoleDescription(string role)
        {
            if (string.IsNullOrEmpty(role))
            {
                return role;
            }
            switch (role)
            {
                case "StoreManager":
                    return "Manager";
                default:
                    return role;
            }
        }
    }
}