﻿using OnlineOrders.Common;

namespace Orders.Backend.Clients.UserPasswordGenerator
{
    public static class PasswordHelper
    {
        public static string GetDefaultPassword(string userName)
        {
            var key = string.Join("_", userName.ToLower(), "salt");
            var encodedKey = Encoder.Encode(key);
            var password = encodedKey.Substring(encodedKey.Length - 6, 6);
            return password;
        }
    }
}
