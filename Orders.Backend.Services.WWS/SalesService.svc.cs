﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using Common.Logging;
using Orders.Backend.Services.WWS.Helpers;
using Orders.Backend.Services.WWS.Mapping;
using Orders.Backend.Services.WWS.OrdersBackendService;
using Orders.Backend.Services.WWS.Store;
using WWS.Services;
using WWS.Services.DataAccess;
using WWS.Services.Interfaces;
using WWS.Services.Proxies.PosServicesSalesProcessing;
using WwsTunnel.Common.Helpers;
using Settings = Orders.Backend.Services.WWS.Properties.Settings;
using WWSContracts = WWS.Services.DataContracts;

namespace Orders.Backend.Services.WWS
{
    public class SalesService : ISalesService
    {
        private readonly ILog _logger;
        private readonly ISalesService serviceV1;
        private readonly ISalesService serviceV2;

        public SalesService()
        {
            _logger = LogManager.GetLogger(GetType().Name);
            serviceV1 = new SalesServiceV1();
            serviceV2 = new SalesServiceV2();
        }


        public CreateSalesOrderResult CreateSalesOrder(OrderInfo order)
        {
            var sapCodes = (ConfigurationManager.AppSettings["SalesServiceV2SapCodes"] ?? "").Split(new [] {";"}, StringSplitOptions.RemoveEmptyEntries);
            if(sapCodes.Contains(order.SapCode))
            {
                _logger.TraceFormat("Call V2 CreateSalesOrder OnlineOrderId={0} SapCode={1} PaymentType={2}", order.OnlineOrderId, order.SapCode, order.PaymentType);
                return serviceV2.CreateSalesOrder(order);
            }
            _logger.TraceFormat("Call V1 CreateSalesOrder OnlineOrderId={0} SapCode={1} PaymentType={2}", order.OnlineOrderId, order.SapCode, order.PaymentType);
            return serviceV1.CreateSalesOrder(order);
        }

        public CancelSalesOrderResult CancelSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var sapCodes = (ConfigurationManager.AppSettings["SalesServiceV2SapCodes"] ?? "").Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (sapCodes.Contains(orderNumberInfo.SapCode))
            {
                return serviceV2.CancelSalesOrder(orderNumberInfo);
            }
            return serviceV1.CancelSalesOrder(orderNumberInfo);
        }

        public GetSalesOrderResult GetSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var sapCodes = (ConfigurationManager.AppSettings["SalesServiceV2SapCodes"] ?? "").Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (sapCodes.Contains(orderNumberInfo.SapCode))
            {
                _logger.TraceFormat("Call V2 GetSalesOrder OrderNumber={0} SapCode={1}", orderNumberInfo.OrderNumber, orderNumberInfo.SapCode);
                return serviceV2.GetSalesOrder(orderNumberInfo);
            }
            return serviceV1.GetSalesOrder(orderNumberInfo);
        }

        public ChangeDeliveryDateResult ChangeDeliveryDate(OrderNumberInfo orderNumberInfo, DateTime deliveryDate)
        {
            return serviceV2.ChangeDeliveryDate(orderNumberInfo, deliveryDate);
        }
    }
}
