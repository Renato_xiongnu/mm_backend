﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using Common.Logging;
using MMS.StoreService.Client;
using Orders.Backend.Services.WWS.Helpers;
using Orders.Backend.Services.WWS.Mapping;
using Orders.Backend.Services.WWS.OrdersBackendService;
using WWS.Services;
using WWS.Services.DataAccess;
using WWS.Services.Interfaces;
using WWS.Services.Proxies.PosServicesSalesProcessing;
using WwsTunnel.Common.Helpers;
using Settings = Orders.Backend.Services.WWS.Properties.Settings;
using WWSContracts = WWS.Services.DataContracts;

namespace Orders.Backend.Services.WWS
{
    public class SalesServiceV1 : ISalesService
    {
        public SalesServiceV1()
        {
            _logger = LogManager.GetLogger(GetType().Name);
        }

        private StoreServiceClient GetStoreService()
        {
            return new StoreServiceClient();
        }

        private SalesDocumentService GetDocService(string sapCode)
        {
            var storeService = GetStoreService();
            var storeInfo = storeService.GetStore(sapCode);

            return new SalesDocumentService(storeInfo.MapTo<WWSContracts.StoreInfo>()); //TODO
        }

        private const string ErrorStart = "msserr";

        private static readonly Regex Regex =
            new Regex(ErrorStart + @":(\d+);(\d+);(\d+);([^;]+);\d+;(.*?)(\s+at(\s+(.{0,5}))|\z)");

        private readonly ILog _logger;

        private static IEnumerable<string> ParseFaultException(FaultException ex)
        {
            return Regex.Split(ex.Message).Where(t => !string.IsNullOrEmpty(t)).Take(6);
        }

        private const string MarkerMissingLines = "MISSING_SALESDOCLINES";

        private const string MarkerSalesDocNotFound = "SALESDOC_NOTFOUND";

        private const string MarkerSolidSqlError = "SOLID SQL Error";

        private static Tuple<string, string> FindJBossSubCode(string message)
        {
            var index = message.IndexOf(MarkerMissingLines, StringComparison.Ordinal);

            if(index > -1)
            {
                return new Tuple<string, string>(SubCodes.JBOSSMissingLines, MessageTexts.JBOSSMissingLines);
            }

            index = message.IndexOf(MarkerSolidSqlError, StringComparison.Ordinal);

            if(index > -1)
            {
                return new Tuple<string, string>(SubCodes.JBOSSSqlError, MessageTexts.JBOSSSqlError);
            }

            index = message.IndexOf(MarkerSalesDocNotFound, StringComparison.Ordinal);
            if(index > -1)
            {
                return new Tuple<string, string>(SubCodes.JBOSSNotFound, MessageTexts.JBOSSSalesDocNotFound);
            }

            return new Tuple<string, string>(SubCodes.JBOSSUnknow, MessageTexts.JBOSSUnknow);
        }

        private OperationCode PreapairOperationCode(Exception ex)
        {
            var operationCode = new OperationCode();
            var faultException = ex as FaultException;
            if(faultException != null)
            {
                var errors = ParseFaultException(faultException).ToArray();
                var subCode = FindJBossSubCode(errors.Last());
                var errorCodes = "JBoss ErrorCode:[" + string.Join(";", errors.Take(3)) + "]";
                operationCode.Code = ReturnCode.JBossException;
                operationCode.SubCode = subCode.Item1;
                operationCode.MessageText = subCode.Item2 + ". " + errorCodes + ". Message: " +
                                            string.Join(" ", errors.Skip(3));
            }
            else
            {
                operationCode.Code = ReturnCode.Exception;
                operationCode.SubCode = SubCodes.SystemException;
                operationCode.MessageText = MessageTexts.SystemException + ". " + ex.GetExceptionFullInfoText();
            }

            return operationCode;
        }

        public CreateSalesOrderResult CreateSalesOrder(OrderInfo order)
        {
            _logger.TraceFormat("Start CreateSalesOrder OnlineOrderId={0} SapCode={1} PaymentType={2}", order.OnlineOrderId, order.SapCode, order.PaymentType);
            var result = new CreateSalesOrderResult();

            try
            {

                var service = GetDocService(order.SapCode);

                var customer = order.Customer;

                var salesOrderInfo = new WWSContracts.OrderInfo
                {
                    PaymentType = ContractHelper.PaymentTypeToWWsType(order.PaymentType),
                    OrderDate = DateTime.Now.ToUniversalTime(),
                    PickupType = ContractHelper.ShippingToPickupType(order.ShippingMethod),
                    OnlineOrderId =
                        order.IsOnlineOrder
                            ? (int?)ContractHelper.ToWWsOnlineOrderId(order.OnlineOrderId)
                            : null,
                    Downpayment = order.Downpayment,
                    PrintableInfo = order.PrintableInfo,
                    OutletInfo = (String.IsNullOrEmpty(order.OutletInfo)) ? null : order.OutletInfo.Truncate(254),
                    ProductPickupInfo = order.ProductPickupInfo,
                    Customer = new WWSContracts.CustomerInfo
                    {
                        FirstName = customer.FirstName,
                        Address = (string.IsNullOrEmpty(customer.Address) ? "Улица1" : customer.Address).Truncate(30),
                        Surname = customer.Surname,
                        ZipCode = string.IsNullOrEmpty(customer.ZipCode) ? "1111111" : customer.ZipCode,
                        Location = string.IsNullOrWhiteSpace(customer.City) || customer.City.Equals("фейк", StringComparison.InvariantCultureIgnoreCase) ? "Не задано" : customer.City,
                        Phone = customer.Phone ?? string.Empty,
                        Mobile = customer.Mobile,
                        Email = customer.Email,
                        Salutation = "НЕТ"
                    },
                    OrderLines = new List<WWSContracts.OrderLineInfo>(),
                    SourceSystem = order.SourceSystem,
                    IsOnlineOrder = order.IsOnlineOrder,
                    DeliveryDate = order.DeliveryDate
                };

                if(order.OrderLines == null || order.OrderLines.Count == 0)
                {
                    result.OperationCode.Code = ReturnCode.ValidationException;
                    result.OperationCode.SubCode = SubCodes.ValidationError;
                    result.OperationCode.MessageText = MessageTexts.ValidationError + ". " + string.Format(ConfigurationManager.AppSettings["OrderWithoutLinesText"]);
                    return result;
                }

                var storeInfo = GetStoreInfoBySapCode(order.SapCode);
                foreach(var orderLine in order.OrderLines)
                {
                    //если это набор, то делаем эпическую херь
                    // если кто-то потом будет это разбирать, меня проклянут.
                    if(orderLine.IsSet())
                    {
                        var printInfo = "";
                        if(String.IsNullOrEmpty(order.PrintableInfo))
                            printInfo = "ВНИМАНИЕ: продажа набора №" + orderLine.ArticleNo;
                        else
                            printInfo += "; №" + orderLine.ArticleNo;
                        salesOrderInfo.PrintableInfo += printInfo;

                        try
                        {
                            var itemsInStock = GetSetItemsBySetArticleNombersFromStock(orderLine.ArticleNo, order.SapCode);
                            var itemsInWws = GetSetItemsBySetArticleNomberFromWws(storeInfo, orderLine.ArticleNo);
                            var erroeStr = String.Empty;
                            var isValid = IsValidWwsSet(itemsInStock, itemsInWws, out erroeStr);
                            if(!isValid)
                                throw new Exception(erroeStr);
                            SetPriceRecalculator.RecalculatePrices(itemsInStock, orderLine.Price);
                            var qty = orderLine.Quantity;
                            var list = new List<OrderLine>();
                            foreach(var line in itemsInStock)
                            {
                                var item = new OrderLine
                                {
                                    ArticleNo = (Int32)line.ArticleNo,
                                    Price = line.Price,
                                    Quantity = line.Quantity * qty,
                                    HasShippedFromStock = orderLine.HasShippedFromStock,
                                    StockNumber = orderLine.StockNumber,
                                };
                                list.Add(item);
                                var wwsModel = item.FromModelToWwsModel();

                                if(IsSerialRequired(item.ArticleNo, order.SapCode))
                                {
                                    var loops = item.Quantity;
                                    for(var i = 0; i < loops; i++)
                                    {
                                        wwsModel.Quantity = 1;
                                        salesOrderInfo.OrderLines.Add(wwsModel);
                                    }
                                }
                                else
                                {
                                    salesOrderInfo.OrderLines.Add(wwsModel);
                                }
                            }
                            orderLine.SubLines = list;

                        }
                        catch(Exception ex)
                        {
                            var errorMsg = String.Format("Не удалось создать резерв с набором. Ошибка: {0}", ex.ToMessageAndCompleteStacktrace());
                            _logger.Error(errorMsg);
                            return new CreateSalesOrderResult
                            {
                                OperationCode = new OperationCode
                                {
                                    Code = ReturnCode.JBossException,
                                    MessageText = errorMsg,
                                    SubCode = SubCodes.LogicError
                                }
                            };
                        }

                    }
                    else
                    {
                        var wwsModel = orderLine.FromModelToWwsModel();
                        if(IsSerialRequired(orderLine.ArticleNo, order.SapCode))
                        {
                            var loops = orderLine.Quantity;
                            for(var i = 0; i < loops; i++)
                            {
                                wwsModel.Quantity = 1;
                                salesOrderInfo.OrderLines.Add(wwsModel);
                            }
                        }
                        else
                        {
                            salesOrderInfo.OrderLines.Add(wwsModel);
                        }

                    }
                }
                salesOrderInfo.PrintableInfo = salesOrderInfo.PrintableInfo.Truncate(254);
                var salesDocumentType = GetSalesDocumentType(order);

                if(order.PaymentType == PaymentType.SocialCard)
                {
                    ProcessCashlessOrder(order, salesOrderInfo, Settings.Default.SocialCardBankOid, Settings.Default.SocialCardDefaultDeliveryDownpayment);
                }

                if(order.PaymentType == PaymentType.Yandex)
                {
                    ProcessCashlessOrder(order, salesOrderInfo, Settings.Default.YandexBankOid,
                        Settings.Default.YandexDefaultDeliveryDownpayment);
                }

                if(order.PaymentType == PaymentType.Online)
                {
                    ProcessCashlessOrder(order, salesOrderInfo, Settings.Default.RaiffeisenBankOid,
                        Settings.Default.RaiffeisenDefaultDownpayment);
                }

                var salesPersonId = order.SalesPersonId ??
                                    Convert.ToInt32(ConfigurationManager.AppSettings["SalesPersonId"]);

                salesOrderInfo.SourceSystem = string.IsNullOrEmpty(order.SourceSystem)
                    ? Settings.Default.DefaultSourceSystem
                    : order.SourceSystem;
                var salesDoc = service.CreateSalesDoc(salesOrderInfo, salesDocumentType, salesPersonId);

                if(salesDoc == null || !salesDoc.salesDocNo.HasValue || salesDoc.salesDocNo == 0)
                {
                    result.OperationCode.Code = ReturnCode.Exception;
                    result.OperationCode.SubCode = SubCodes.SystemException;
                    result.OperationCode.MessageText = MessageTexts.SystemException + ". " +
                                                       string.Format(
                                                           ConfigurationManager.AppSettings["UnxpectedErrorOccuredText"]);
                    return result;
                }

                result.CreateTime = salesDoc.createTime;
                var newSalesDocNumber = salesDoc.salesDocNo.Value;

                try
                {
                    var createdReserve = service.GetSalesDoc(newSalesDocNumber);

                    var createdLines = createdReserve.salesDocDetailTO.salesDocLineTOList;

                    var diffLines = CheckLineDifference(salesOrderInfo, createdLines);

                    result.OrderHeader = InitOrderHeader(service, createdReserve);

                    if(diffLines.Count > 0)
                    {
                        result.OperationCode.Code = ReturnCode.OkPartially;
                        result.OperationCode.SubCode = SubCodes.LogicError;
                        result.OperationCode.MessageText = MessageTexts.LogicError + ". " +
                                                           ConfigurationManager.AppSettings[
                                                               "ReserveCreatedWithDiffLinesText"] +
                                                           Environment.NewLine +
                                                           string.Join(Environment.NewLine, diffLines);
                    }
                    CreateHierarchy(order, result.OrderHeader);

                    //CreateSetHierarchy(order, result.OrderHeader);
                }
                catch
                {
                    service.CancelSalesDoc(newSalesDocNumber);
                    throw;
                }
            }
            catch(Exception ex)
            {
                result.OperationCode = PreapairOperationCode(ex);
                _logger.ErrorFormat("CreateSalesOrder SapCode={0} OnlineOrderId={1}", ex, order.SapCode,
                    order.OnlineOrderId);
            }

            return result;
        }

        private void ProcessCashlessOrder(OrderInfo order, WWSContracts.OrderInfo salesOrderInfo, string financingBankOid, decimal defaultDeliveryDownpayment)
        {
            salesOrderInfo.CreditNo = order.CreditNo ?? order.OnlineOrderId;
            salesOrderInfo.FinancingBankOID = financingBankOid;
            _logger.TraceFormat("Start CreateSalesOrder OnlineOrderId={0} CreditNo={1} FinancingBankOID={2}", order.OnlineOrderId, salesOrderInfo.CreditNo, salesOrderInfo.FinancingBankOID);
            if((order.ShippingMethod == ShippingMethod.Delivery) && (!order.Downpayment.HasValue || order.Downpayment.Value <= 0))
            {
                var downpayment = defaultDeliveryDownpayment;
                salesOrderInfo.Downpayment = downpayment;
                if(!salesOrderInfo.OrderLines.Any(ol => ol.Price > 0 && ol.Quantity > 0))
                {
                    return;
                }
                var orderLine = salesOrderInfo.OrderLines.FirstOrDefault(ol => ol.Quantity == 1);
                if(orderLine == null)
                {
                    orderLine = salesOrderInfo.OrderLines.First(ol => ol.Price > 0);
                    orderLine.Quantity--;
                    var orderLineCopy = orderLine.Copy();
                    orderLineCopy.Quantity = 1;
                    salesOrderInfo.OrderLines.Add(orderLineCopy);
                    orderLine = orderLineCopy;
                }
                orderLine.Price += downpayment;

            }
        }

        private static SalesDocumentType GetSalesDocumentType(OrderInfo order)
        {
            switch(order.PaymentType)
            {
                case PaymentType.Cash:
                    return SalesDocumentType.CASH_SALE;
                case PaymentType.CreditCard:
                    break;
                case PaymentType.SocialCard:
                    break;
                case PaymentType.Yandex:
                    break;
                case PaymentType.Online:
                    break;
            }
            return SalesDocumentType.FINANCING;
        }

        private OperationResult Validate(OrderInfo order)
        {
            var result = new OperationResult();
            if(order.OrderLines == null || order.OrderLines.Count == 0)
            {
                result.OperationCode.Code = ReturnCode.ValidationException;
                result.OperationCode.SubCode = SubCodes.ValidationError;
                result.OperationCode.MessageText = MessageTexts.ValidationError + ". " +
                                                   string.Format(
                                                       ConfigurationManager.AppSettings["OrderWithoutLinesText"]);
                return result;
            }
            return result;
        }

        private static ICollection<string> CheckLineDifference(WWSContracts.OrderInfo salesOrderInfo, SalesDocLineTO[] createdLines)
        {
            var checkedLines = new HashSet<int>();
            var diffText = new LinkedList<string>();
            var setNumbers = new HashSet<int>();
            foreach(var orderLine in salesOrderInfo.OrderLines)
            {
                var found = false;

                for(var i = 0; i < createdLines.Length; i++)
                {
                    if(checkedLines.Contains(i))
                    {
                        continue;
                    }
                    var line = createdLines[i];
                    string productId;

                    if(string.IsNullOrEmpty(line.articleNo))
                    {
                        setNumbers.Add(line.productSetNo.Value);
                        productId = line.productSetNo.Value.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        if(line.productSetNo.HasValue && setNumbers.Contains(line.productSetNo.Value))
                        {
                            checkedLines.Add(i);
                            continue;
                        }

                        productId = line.articleNo;
                    }

                    if(productId == orderLine.ArticleNo.ToString(CultureInfo.InvariantCulture) &&
                       line.quantity == orderLine.Quantity && line.retailPrice.HasValue &&
                       Math.Abs(Convert.ToDouble(orderLine.Price) - line.retailPrice.Value) < 0.000001)
                    {
                        checkedLines.Add(i);
                        found = true;
                        break;
                    }
                }

                if(!found)
                {
                    diffText.AddLast(string.Format(ConfigurationManager.AppSettings["DiffLineDescrText"],
                        orderLine.ArticleNo, orderLine.Price, orderLine.Quantity));
                }
            }

            return diffText;
        }

        public CancelSalesOrderResult CancelSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var result = new CancelSalesOrderResult();
            try
            {
                int orderId;

                if(!int.TryParse(orderNumberInfo.OrderNumber, out orderId))
                {
                    result.OperationCode.Code = ReturnCode.ValidationException;
                    result.OperationCode.SubCode = SubCodes.ValidationError;
                    result.OperationCode.MessageText = MessageTexts.ValidationError + ". " +
                                                       string.Format(
                                                           ConfigurationManager.AppSettings["OrderShouldBeNumericText"],
                                                           orderNumberInfo.OrderNumber);

                    return result;
                }

                var service = GetDocService(orderNumberInfo.SapCode);

                service.CancelSalesDoc(orderId);

                return result;
            }
            catch(Exception ex)
            {
                result.OperationCode = PreapairOperationCode(ex);
                _logger.ErrorFormat("CancelSalesOrder SapCode={0} OrderId={1}", ex, orderNumberInfo.SapCode,
                    orderNumberInfo.OrderNumber);
            }

            return result;
        }

        public GetSalesOrderResult GetSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var result = new GetSalesOrderResult();
            try
            {
                var service = GetDocService(orderNumberInfo.SapCode);

                var salesDoc = service.GetSalesDoc(Convert.ToInt32(orderNumberInfo.OrderNumber));

                if(salesDoc == null)
                {
                    result.OperationCode = new OperationCode()
                    {
                        Code = ReturnCode.Exception,
                        SubCode = SubCodes.SystemException,
                        MessageText =
                            MessageTexts.SystemException + ". " +
                            string.Format(
                                ConfigurationManager.AppSettings["UnxpectedErrorOccuredText"])
                    };
                }
                else
                {
                    result.OrderHeader = InitOrderHeader(service, salesDoc);
                }
            }
            catch(Exception ex)
            {
                result.OperationCode = PreapairOperationCode(ex);
                _logger.ErrorFormat("GetSalesOrder SapCode={0} OrderId={1}", ex, orderNumberInfo.SapCode,
                    orderNumberInfo.OrderNumber);
            }

            return result;
        }

        private static OrderHeader InitOrderHeader(SalesDocumentService service, SalesDocTO salesDoc)
        {
            var customerService = service.CustomerService;
            var deliveryCustomer = GetCustomer(salesDoc.salesDocHeadTO.deliveryCustomerOID, customerService);
            var invoiceCustomer = GetCustomer(salesDoc.salesDocHeadTO.invoiceCustomerOID, customerService);
            var deliveryAddress = FilterAddress(salesDoc.salesDocHeadTO.deliveryAddressOID,
                deliveryCustomer.addressTOList);
            var invoiceAddress = FilterAddress(salesDoc.salesDocHeadTO.invoiceAddressOID, invoiceCustomer.addressTOList);
            var header = salesDoc.FromJbosObjectToModel(deliveryCustomer);

            if(salesDoc.salesDocHeadTO.salesDocNo.HasValue)
            {
                header.DocBarcode = service.GetSalesDocBarCode(salesDoc.salesDocHeadTO.salesDocNo.Value);
                var bytes = BarcodeInter25.GetImage(header.DocBarcode, false);
                header.DocBarcodeImage = Convert.ToBase64String(bytes);
            }
            else
            {
                header.DocBarcode = string.Empty;
                header.DocBarcodeImage = string.Empty;
            }
            header.Delivery = new Delivery
            {
                Date = salesDoc.salesDocHeadTO.deliveryOrPickupDate ?? new DateTime(),
            };

            header.DeliveryCustomer = deliveryCustomer.FromJbosObjectToModel(deliveryAddress);
            header.InvoiceCustomer = invoiceCustomer.FromJbosObjectToModel(invoiceAddress);

            return header;
        }

        private static CustomerTO GetCustomer(string customerId, ICustomerService customerService)
        {
            var customer = !String.IsNullOrEmpty(customerId) ? customerService.GetCustomer(customerId) : new CustomerTO { salutationTO = new SalutationTO(), };
            return customer;
        }

        private static AddressTO FilterAddress(string addressId, IEnumerable<AddressTO> addresses)
        {
            AddressTO address = null;
            if(addresses != null)
            {
                address = addresses.FirstOrDefault(t => t.addressOID == addressId);
            }
            if(address == null)
            {
                address = new AddressTO()
                {
                    countryTO = new CountryTO(),
                    addressTypeTO = new AddressTypeTO()
                };
            }
            return address;
        }


        /// <summary>
        ///  Проверяем, нужен ли серийный номер
        /// (для кких-то товаров обязателен)
        /// </summary>
        /// <param name="articleNo"></param>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        private bool IsSerialRequired(Int32 articleNo, String sapCode)
        {
            const string query = @"
                                select t3.SLIP_SN_REQUIRED_FLAG from artikel t1
                                inner join prod_typ_prop t3 on t1.prod_typ_oid=t3.prod_typ_oid
                                where  t1.art_no=?";

            var info = GetStoreInfoBySapCode(sapCode);

            var result = new WWSHelper(info).ExecuteScalar<short>(query, articleNo);

            return Convert.ToBoolean(result);
        }

        /// <summary>
        /// ToDo: выпилить, т.к. костыль
        /// но влом заморачиваться
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        private WWSContracts.StoreInfo GetStoreInfoBySapCode(String sapCode)
        {
            var store = GetStoreService().GetStore(sapCode);
            var info = store.MapTo<WWSContracts.StoreInfo>();
            return info;
        }


        private static Boolean IsValidWwsSet(IEnumerable<SetItem> setItemsFromStock, List<SetItem> setItemsFromWws, out String errorStr)
        {
            var result = true;
            errorStr = "Не найдена информация по элементу набора с артикулами: ";
            foreach(var setItemFromStock in setItemsFromStock)
            {
                var stockItem = setItemFromStock;
                var wwsItem = setItemsFromWws.FirstOrDefault(x => x.ArticleNo == stockItem.ArticleNo && x.Price > 0 && ((x.UntilDate != null && x.UntilDate == setItemsFromWws.Max(i => i.UntilDate)) || x.UntilDate == null));
                if(wwsItem == null)
                {
                    result = false;
                    errorStr += stockItem.ArticleNo + ";";
                }
                else
                {
                    setItemFromStock.Price = wwsItem.Price;
                }
            }
            return result;
        }

        /// <summary>
        /// Получаем из сервиса стоков данные по элементам набора
        /// (артикульный состав и т.д.)
        /// </summary>
        /// <param name="setArticleNo">номера артикула набора</param>
        /// <param name="sapCode"></param>
        /// <param name="channel">по умолчаниею для ММ</param>
        /// <returns></returns>
        private List<SetItem> GetSetItemsBySetArticleNombersFromStock(Int32 setArticleNo, String sapCode, String channel = "MM")
        {
            var result = new List<SetItem>();
            GetArticleStockStatusResponse response = null;
            var service = new OrdersBackendServiceClient();
            try
            {
                var saleLocation = "shop_" + sapCode;
                var singleItemArray = new[] { setArticleNo.ToString(CultureInfo.InvariantCulture) };
                var request = new GetItemsRequest { Articles = singleItemArray, Channel = channel, SaleLocation = saleLocation };
                response = service.GetArticleStockStatus(request);
                service.Close();
            }
            catch(CommunicationException e)
            {
                service.Abort();
                throw;
            }
            catch(TimeoutException e)
            {
                service.Abort();
                throw;
            }
            catch(Exception e)
            {
                service.Abort();
                throw;
            }

            //using (var service = new OrdersBackendServiceClient())
            //{
            //    var saleLocation = "shop_" + sapCode;
            //    var singleItemArray = new[] {setArticleNo.ToString(CultureInfo.InvariantCulture)};
            //    var request = new GetItemsRequest { Articles = singleItemArray, Channel = channel, SaleLocation = saleLocation };
            //    response = service.GetArticleStockStatus(request);
            //}

            if(response == null || response.Items == null || response.Items.Any(x => x.SetItems == null || !x.SetItems.Any()))
                throw new Exception("Нет данных о элементах набора");

            var subItems = response.Items.FirstOrDefault().SetItems;

            var zeroQty = subItems.Where(x => x.QtyInSet <= 0).ToList();
            if(zeroQty.Any())
                throw new Exception(string.Format("Количество элементов в наборе не может быть меньше 1 для артикулов: {0}",
                    string.Join(",", zeroQty.Select(x => x.Article))));

            result.AddRange(subItems.Select(subItem => new SetItem { ArticleNo = subItem.Article, Quantity = subItem.QtyInSet }));

            return result;

        }

        /// <summary>
        /// Получаем из wws данные по элементам набора
        /// </summary>
        /// <param name="storeInfo"></param>
        /// <param name="setArtocleNo"></param>
        /// <returns></returns>
        private List<SetItem> GetSetItemsBySetArticleNomberFromWws(WWSContracts.StoreInfo storeInfo, Int32 setArtocleNo)
        {
            var result = new List<SetItem>();
            var sqlQuery = String.Format(@"select
                                                PROD_SET_Price_Val as ProdSetPrice
                                                ,QTY
                                                ,coalesce(a1.ART_NO  ,a2.ART_NO) as ArticleNo
                                                ,ART_BEZ as ArticleTitle
                                                ,Until_Date as UntilDate 
                                            from MD_PROD_SET p
                                            inner join  PROD_SET_ELMNT e ON e.PARENT_OID = p.OID
                                            left join ARTIKEL a1 on a1.oid = e.SUBSID_PROD_OID
                                            left join TARTIKEL a2 on a2.oid = e.SUBSID_PROD_OID
                                            where ID = {0}", setArtocleNo);
            try
            {
                var wws = new WWSHelper(storeInfo);
                var reader = wws.ExecuteReaderCommand(sqlQuery);

                while(reader.Read())
                {
                    var line = new SetItem
                    {
                        ArticleNo = (Int32)reader["ArticleNo"],
                        Price = (Decimal?)reader["ProdSetPrice"] ?? 0,
                        Quantity = (Int32?)reader["QTY"] ?? 0,
                        UntilDate = (DateTime?)reader["UntilDate"],
                    };
                    result.Add(line);
                }
            }
            catch(Exception ex)
            {
                _logger.Error("Get set info from wws exception: ", ex);
                throw;

            }

            return result;
        }




        /// <summary>
        /// Богоподобный метод, который собирает назад иерархию
        /// с учетом всех вышереализованных разбивок.
        /// </summary>
        /// <param name="order"></param>
        /// <param name="header"></param>
        private void CreateHierarchy(OrderInfo order, OrderHeader header)
        {
            var orderRootLines = order.OrderLines;

            foreach(var orderRoorLine in orderRootLines)
            {
                var loops = orderRoorLine.Quantity;
                //if is set
                if(orderRoorLine.IsSet())
                {
                    var subIds = orderRoorLine.SubLines.Select(x => new { x.ArticleNo, x.Price, x.Quantity });
                    var headerSubLines = new List<OrderLine>();
                    foreach(var subId in subIds)
                    {
                        var id = subId;
                        var byQty = header.Lines.Where(x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == loops).Take(loops);
                        if(!byQty.Any())
                            byQty = header.Lines.Where(x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == 1).Take(loops);

                        headerSubLines.AddRange(byQty);
                    }

                    foreach(var headerSubLine in headerSubLines)
                    {
                        header.Lines.Remove(headerSubLine);
                    }
                    var distSubLines = headerSubLines.GroupBy(x => x.ArticleNo).Select(x => x.First()).ToList();
                    foreach(var distSubLine in distSubLines)
                    {
                        distSubLine.TotalPrice = new ReservePrice();
                        distSubLine.Quantity = loops;
                    }

                    orderRoorLine.ProductType = ProductType.Set;
                    orderRoorLine.FromModelToWwsModel();
                    orderRoorLine.TotalPrice = new ReservePrice();
                    orderRoorLine.SubLines = new OrderLine[headerSubLines.Count];
                    orderRoorLine.SubLines = distSubLines.ToArray();
                    header.Lines.Add(orderRoorLine);

                }
                else
                {
                    var byQty = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == loops).Take(loops).ToList();
                    if(!byQty.Any())
                        byQty = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == 1).Take(loops).ToList();

                    //var headerRootLines = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price).Take(loops).ToList();
                    foreach(var headerRoorLine in byQty)
                    {
                        header.Lines.Remove(headerRoorLine);
                    }
                    var headerRootLine = byQty.FirstOrDefault();
                    if(headerRootLine != null)
                    {
                        headerRootLine.Quantity = loops;
                        headerRootLine.TotalPrice = new ReservePrice();
                        //headerRootLine.ProductType = ProductType.Article;
                        header.Lines.Add(headerRootLine);
                    }
                }
            }

        }

        public ChangeDeliveryDateResult ChangeDeliveryDate(OrderNumberInfo orderNumberInfo, DateTime deliveryDate)
        {
            throw new NotImplementedException();
        }
    }

    public class SetItem
    {
        public Int64 ArticleNo { get; set; }
        public Decimal Price { get; set; }
        public DateTime? UntilDate { get; set; }
        public Int32 Quantity { get; set; }
    }

}