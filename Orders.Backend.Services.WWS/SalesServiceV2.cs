﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using Common.Logging;
using Orders.Backend.Services.WWS.Helpers;
using Orders.Backend.Services.WWS.Mapping;
using Orders.Backend.Services.WWS.OrdersBackendService;
using WwsTunnel.Common.Helpers;
using Settings = Orders.Backend.Services.WWS.Properties.Settings;
using MMS.Integration.Gms.WebServices.Client;
using MMS.Integration.Gms.WebServices.Interfaces;
using MMS.StoreService.Client;
using MMS.StoreService.Interfaces;
using MMS.StoreService.Interfaces.DataContracts;

namespace Orders.Backend.Services.WWS
{
    public class SalesServiceV2 : ISalesService
    {
        private readonly ILog _logger;
        private readonly StoreServiceClient _storeServiceClient;

        public SalesServiceV2()
        {
            _logger = LogManager.GetLogger(GetType().Name);
            _storeServiceClient = new StoreServiceClient();
        }

        private GmsClient GetDocService(string sapCode)
        {
            return new GmsClient(sapCode);
        }

        public CreateSalesOrderResult CreateSalesOrder(OrderInfo order)
        {
            _logger.TraceFormat("[{0}] Start CreateSalesOrder OnlineOrderId={1} SapCode={2} PaymentType={3}", GetType().FullName, order.OnlineOrderId, order.SapCode, order.PaymentType);
            _logger.DebugFormat("[{0}] OrderInfo = {1}", GetType().FullName, order.ToJson());
            var resultBuilder = new CreateSalesOrderResultBuilder();
            resultBuilder.SetOrderInfo(order);
            try
            {
                if(order.OrderLines == null || order.OrderLines.Count == 0)
                {
                    resultBuilder.SetValidationExceptionOperationCode();
                    return resultBuilder.BuildResult();
                }
                var wwsService = GetDocService(order.SapCode);
                var service = wwsService.SalesProcessing;
                var storeInfo = _storeServiceClient.GetStore(order.SapCode);

                var requestBuilder = new RequestBuilder(order, n => storeInfo.IsSerialRequired(n), n => ServiceHelper.GetSetItemsFromStock((int)n, order.SapCode), n => ServiceHelper.GetSetItemsFromWws(storeInfo, n));
                var createRequest = requestBuilder.BuildCreateRequest();

                var salesPersonId = order.SalesPersonId ?? Convert.ToInt32(ConfigurationManager.AppSettings["SalesPersonId"]);

                _logger.TraceFormat("Call CreateSalesOrder, CreateSalesDocumentRequest OnlineOrderId={0} CreditNo={1} FinancingBankOID={2}", createRequest.OnlineOrderId, createRequest.CreditNo, createRequest.Bank.Oid);
                var salesDocCreationResult = service.CreateSalesDocument(createRequest, salesPersonId);
                _logger.DebugFormat("[{0}] CreateSalesDocumentResponse = {1}", GetType().FullName, salesDocCreationResult.ToJson());

                if(salesDocCreationResult == null || salesDocCreationResult.SalesDocNo == 0)
                {
                    resultBuilder.SetSystemExceptionOperationCode();
                    return resultBuilder.BuildResult();
                }

                resultBuilder.SetCreateSalesDocumentResponse(salesDocCreationResult);
                var salesDocNo = salesDocCreationResult.SalesDocNo;

                try
                {
                    var salesDocumentInfo = service.GetSalesDocument(salesDocNo);
                    resultBuilder.SetSalesDocumentInfo(salesDocumentInfo);

                    var diffLines = CheckLineDifference(createRequest, salesDocumentInfo);

                    if(diffLines.Count > 0)
                    {
                        resultBuilder.SetLogicErrorOperationCode(diffLines);
                    }
                    var result = resultBuilder.BuildResult();
                    _logger.DebugFormat("[{0}] CreateSalesOrderResult = {1}", GetType().FullName, result.ToJson());
                    return result;
                }
                catch
                {
                    service.CancelSalesDocument(salesDocNo);
                    throw;
                }
            }
            catch(Exception ex)
            {
                resultBuilder.SetOperationCodeFromException(ex);
                _logger.ErrorFormat("Error CreateSalesOrder SapCode={0} OnlineOrderId={1}", ex, order.SapCode, order.OnlineOrderId);
            }
            return resultBuilder.BuildResult();
        }

        public CancelSalesOrderResult CancelSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var resultBulider = new ResultBuilder<CancelSalesOrderResult>();
            try
            {
                int orderId;

                if(!int.TryParse(orderNumberInfo.OrderNumber, out orderId))
                {
                    resultBulider.SetValidationExceptionOperationCode(orderNumberInfo.OrderNumber);
                    return resultBulider.BuildResult();
                }

                var wwsService = GetDocService(orderNumberInfo.SapCode);
                var service = wwsService.SalesProcessing;
                service.CancelSalesDocument(orderId);

                return resultBulider.BuildResult();
            }
            catch(Exception ex)
            {
                resultBulider.SetOperationCodeFromException(ex);
                _logger.ErrorFormat("CancelSalesOrder SapCode={0} OrderId={1}", ex, orderNumberInfo.SapCode, orderNumberInfo.OrderNumber);
            }
            return resultBulider.BuildResult();
        }

        public GetSalesOrderResult GetSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            var resultBulider = new GetSalesOrderResultBuilder();
            try
            {
                int orderId;

                if(!int.TryParse(orderNumberInfo.OrderNumber, out orderId))
                {
                    resultBulider.SetValidationExceptionOperationCode(orderNumberInfo.OrderNumber);
                    return resultBulider.BuildResult();
                }

                var wwsService = GetDocService(orderNumberInfo.SapCode);
                var service = wwsService.SalesProcessing;
                var salesDoc = service.GetSalesDocument(Convert.ToInt32(orderNumberInfo.OrderNumber));

                if(salesDoc == null)
                {
                    resultBulider.SetSystemExceptionOperationCode();
                    return resultBulider.BuildResult();
                }
                resultBulider.SetSalesDocumentInfo(salesDoc);
                return resultBulider.BuildResult();
            }
            catch(Exception ex)
            {
                resultBulider.SetOperationCodeFromException(ex);
                _logger.ErrorFormat("GetSalesOrder SapCode={0} OrderId={1}", ex, orderNumberInfo.SapCode, orderNumberInfo.OrderNumber);
            }
            return resultBulider.BuildResult();
        }

        private static ICollection<string> CheckLineDifference(CreateSalesDocumentRequest request, SalesDocumentInfo salesDocumentInfo)
        {
            var checkedLines = new HashSet<int>();
            var diffText = new LinkedList<string>();
            var setNumbers = new HashSet<int>();

            var createdLines = salesDocumentInfo.SalesDocumentLines.ToArray();

            foreach(var orderLine in request.Order.OrderLines.OfType<OrderLineInfo>())
            {
                var found = false;
                for(var i = 0; i < createdLines.Length; i++)
                {
                    if(checkedLines.Contains(i))
                    {
                        continue;
                    }
                    var line = createdLines[i];

                    if(line.Product.ArticleNo == orderLine.ArticleNo && line.Quantity == orderLine.Quantity && orderLine.Price == line.RetailPrice)
                    {
                        checkedLines.Add(i);
                        found = true;
                        break;
                    }
                }

                if(!found)
                {
                    diffText.AddLast(string.Format(ConfigurationManager.AppSettings["DiffLineDescrText"], orderLine.ArticleNo, orderLine.Price, orderLine.Quantity));
                }
            }
            return diffText;
        }

        public ChangeDeliveryDateResult ChangeDeliveryDate(OrderNumberInfo orderNumberInfo, DateTime deliveryDate)
        {
            var result = new ChangeDeliveryDateResult();
            try
            {
                int orderId;
                if(!int.TryParse(orderNumberInfo.OrderNumber, out orderId))
                {
                    result.OperationCode.Code = ReturnCode.ValidationException;
                    result.OperationCode.SubCode = SubCodes.ValidationError;
                    result.OperationCode.MessageText = MessageTexts.ValidationError + ". " + string.Format(ConfigurationManager.AppSettings["OrderShouldBeNumericText"], orderNumberInfo.OrderNumber);
                    return result;
                }

                var wwsService = GetDocService(orderNumberInfo.SapCode);
                var service = wwsService.SalesProcessing;
                service.ChangeDeliveryDate(orderId, deliveryDate);
                return result;
            }
            catch(Exception ex)
            {
                result.OperationCode.Code = ReturnCode.Exception;
                result.OperationCode.SubCode = SubCodes.SystemException;
                result.OperationCode.MessageText = MessageTexts.SystemException + ". " + ex.GetExceptionFullInfoText();
                _logger.ErrorFormat("GetSalesOrder SapCode={0} OrderId={1}", ex, orderNumberInfo.SapCode, orderNumberInfo.OrderNumber);
            }
            return result;
        }
    }
}