﻿using System;
using System.Text.RegularExpressions;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;
using Orders.Backend.Services.WWS.Helpers;

namespace Orders.Backend.Services.WWS.Mapping
{
    public static class MappingExtenisonsV2
    {
        /// <summary>
        /// Clone OrderLineInfo object
        /// </summary>
        /// <param name="orderLine"></param>
        /// <returns></returns>
        public static Interfaces.OrderLineInfo Copy(this Interfaces.OrderLineInfo orderLine)
        {
            return CopyWithQuantity(orderLine, orderLine.Quantity);
        }

        public static Interfaces.OrderLineInfo CopyWithQuantity(this Interfaces.OrderLineInfo orderLine, int quantity)
        {
            var result = new Interfaces.OrderLineInfo
            {
                ArticleNo = orderLine.ArticleNo,
                ArticleTitle = orderLine.ArticleTitle,
                CommissionContractNo = orderLine.CommissionContractNo,
                Price = orderLine.Price,
                ProductPickupSign = orderLine.ProductPickupSign,
                Quantity = quantity,
                SerialNumber = orderLine.SerialNumber,
                WarrantyInsuranceArticleNo = orderLine.WarrantyInsuranceArticleNo
            };
            return result;
        }

        public static Interfaces.PickupType MapToPickupType(this ShippingMethod shipping)
        {
            switch (shipping)
            {
                case ShippingMethod.Delivery:
                    return Interfaces.PickupType.DELIVERY;
                default:
                    return Interfaces.PickupType.CARRY;
            }
        }

        public static ShippingMethod MapToShippingMethod(this Interfaces.PickupType pickupType)
        {
            switch (pickupType)
            {
                case Interfaces.PickupType.DELIVERY:
                    return ShippingMethod.Delivery;
                default:
                    return ShippingMethod.Pickup;
            }
        }

        public static int MapToWwsOnlineOrderId(this string onlineOrderId)
        {
            const string newNumberFormat = "002-12345678";
            var newNumberFormatLength = newNumberFormat.Length;
            var result = Regex.Replace(onlineOrderId, @"[^\d]", "");
            if (onlineOrderId.Length == newNumberFormatLength)
            {
                result = result.Substring(3, 8);
            }
            if (result.Length > 8)
            {
                result = result.Substring(0, 8);
            }
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Map TT CustomerInfo to WWS CustomerInfo
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public static Interfaces.CustomerInfo MapToWwsCustomerInfo(this CustomerInfo customer)
        {
            var customerInfo = new MMS.Integration.Gms.WebServices.Interfaces.CustomerInfo
            {
                //Account
                //Birthday
                //CustomerNo
                FirstName = customer.FirstName,
                Surname = customer.Surname,
                Phone = customer.Phone ?? string.Empty,
                Mobile = customer.Mobile,
                Email = customer.Email,
                Salutation = "НЕТ",
                Address = new Interfaces.AddressInfo
                {
                    Country = "RU",
                    Address = string.IsNullOrEmpty(customer.Address) ? "Не задано" : customer.Address.Truncate(30),
                    ZipCode = string.IsNullOrEmpty(customer.ZipCode) ? "1111111" : customer.ZipCode,
                    Location = string.IsNullOrWhiteSpace(customer.City) || customer.City.Equals("фейк", StringComparison.InvariantCultureIgnoreCase) ? "Не задано" : customer.City,
                },
            };
            return customerInfo;
        }

        public static Interfaces.SalesDocumentType MapToSalesDocumentType(this PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.Cash:
                    return Interfaces.SalesDocumentType.CASH_SALE;
                case PaymentType.CreditCard:
                case PaymentType.SocialCard:
                case PaymentType.Online:
                    return Interfaces.SalesDocumentType.CREDIT_NOTE;
                case PaymentType.Yandex:
                default:
                    return Interfaces.SalesDocumentType.FINANCING;
            }
        }

        public static Delivery MapToDelivery(this Interfaces.DeliveryInfo deliveryInfo)
        {
            var delivery = new Delivery
            {
                Date = deliveryInfo.DateFrom ?? new DateTime(),
                //Period = 
                Comment = string.Empty
            };
            return delivery;
        }

        public static Customer MapFromOrderCustomerInfo(this Customer customerSource, CustomerInfo customerInfo)
        {
            var customer = new Customer
            {
                Address = customerSource.Address ?? customerInfo.Address,
                AddressIndex = customerSource.AddressIndex ?? customerInfo.ZipCode,
                Birthday = customerSource.Birthday,
                City = customerSource.City ?? customerInfo.City,
                ClassificationNumber = customerSource.ClassificationNumber,
                CountryAbbreviation = customerSource.CountryAbbreviation,
                Email = customerSource.Email ?? customerInfo.Email,
                INN = customerSource.INN,
                KPP = customerSource.KPP,
                Name = customerSource.Name ?? customerInfo.FirstName,
                Phone = customerSource.Phone ?? customerInfo.Phone,
                Phone2 = customerSource.Phone2 ?? customerInfo.Mobile,
                Salutation = customerSource.Salutation,
                Surname = customerSource.Surname ?? customerInfo.Surname
            };
            return customer;
        }

        public static Customer MapToCustomer(this Interfaces.CustomerInfo customerInfo)
        {
            var customer = new Customer
            {
                Address = customerInfo.HasAddress ? customerInfo.Address.Address : null,
                AddressIndex = customerInfo.Address.ZipCode,
                Birthday = customerInfo.Birthday.HasValue ? customerInfo.Birthday.ToString() : null,
                City = customerInfo.HasAddress ? customerInfo.Address.Location : null,
                //ClassificationNumber
                CountryAbbreviation = customerInfo.HasAddress ? customerInfo.Address.Country : null,
                Email = customerInfo.Email,
                INN = customerInfo.Account != null ? customerInfo.Account.INN : null,
                KPP = customerInfo.Account != null ? customerInfo.Account.KPP : null,
                Name = customerInfo.FirstName,
                Phone = customerInfo.Phone,
                Phone2 = customerInfo.Mobile,
                Salutation = customerInfo.Salutation,
                Surname = customerInfo.Surname
            };
            return customer;
        }

        public static OrderLine MapToOrderLine(this Interfaces.SalesDocumentLineInfo lineInfo, int index)
        {
            var productInfo = (Interfaces.ProductInfo)lineInfo.Product;
            ProductType productType;
            int productId;
            if (!string.IsNullOrEmpty(lineInfo.WarrantyInsuranceNo))
            {
                productType = ProductType.WarrantyPlus;
                productId = (int)productInfo.ArticleNo;
            }
            else if (productInfo.ArticleNo == 0)
            {
                productType = ProductType.Set;
                productId = productInfo.ProductGroupNo ?? 0;
            }
            else
            {
                productType = ProductType.Article;
                productId = (int)productInfo.ArticleNo;
            }

            return new OrderLine()
            {
                ArticleNo = productId,
                ManufacturerName = productInfo.ManufacturerName,
                ProductGroupName = productInfo.ProductGroup,
                ProductGroupNo = productInfo.ProductGroupNo ?? 0,
                Description = productInfo.Title, // salesDocLine.productOrSetDescription,
                Price = lineInfo.RetailPrice ?? 0,
                PriceOrig = lineInfo.OriginalPrice ?? 0,
                FreeQuantity = lineInfo.FreeQuantity ?? 0,
                ReservedQuantity = lineInfo.ReservedQuantity ?? 0,
                Quantity = lineInfo.Quantity ?? 0,
                StockNumber = lineInfo.StockAreaNo ?? 0,
                StoreNumber = lineInfo.StockAreaOutletNo ?? 0,
                DepartmentNumber = productInfo.DepartmentNo ?? 0,
                PositionNumber = index + 1,
                LineNumber = index + 1,
                HasShippedFromStock = lineInfo.HasShippedFromStock ?? false,

                SerialNumber = lineInfo.SerialNo,
                TotalPrice = new ReservePrice
                {
                    //VAT = Convert.ToDecimal(salesDocLine.vatRate),
                    //VatPrice = Convert.ToDecimal(salesDocLine.vatAmountSum),
                    //NetPrice = Convert.ToDecimal(salesDocLine.netAmountSum),
                    GrossPrice = (lineInfo.RetailPrice ?? 0) * (lineInfo.Quantity ?? 0)
                },
                ProductType = productType,
                WarrantyInsurance = new WarrantyInsurance
                {
                    Number = lineInfo.WarrantyInsuranceNo,
                    //WwsExtensionPrint = lineInfo.warrantyExtensionPrint,
                    //WwsCertificateState = salesDocLine.warrantyInsuranceState
                }
            };
        }
    }
}