﻿using AutoMapper;
using WWS.Services.DataContracts;
using MMSContracts = MMS.StoreService.Interfaces.DataContracts;

namespace Orders.Backend.Services.WWS.Mapping
{
    internal static class MappingExtensions
    {
        static MappingExtensions()
        {
            ConfigureMapping();
        }

        private static void ConfigureMapping()
        {
            MapStoreInfo();
        }

        private static void MapStoreInfo()
        {
            Mapper.CreateMap<Store.Requisite, Requisite>();
            Mapper.CreateMap<Store.Address, Address>();
            Mapper.CreateMap<Store.Bank, Bank>();
            Mapper.CreateMap<Store.Employee, Employee>();
            Mapper.CreateMap<Store.Printer, Printer>();
            Mapper.CreateMap<Store.StoreInfo, StoreInfo>()
                .ForMember(s => s.POA, opt => opt.Ignore()) ;

            Mapper.CreateMap<MMSContracts.Requisite, Requisite>();
            Mapper.CreateMap<MMSContracts.Address, Address>();
            Mapper.CreateMap<MMSContracts.Bank, Bank>();
            Mapper.CreateMap<MMSContracts.Employee, Employee>();
            Mapper.CreateMap<MMSContracts.Printer, Printer>();
            Mapper.CreateMap<MMSContracts.StoreInfo, StoreInfo>()
                .ForMember(s => s.POA, opt => opt.Ignore());
        }

        public static void Validate()
        {
            Mapper.AssertConfigurationIsValid();
        }

        public static T MapTo<T>(this object obj)
        {
            return Mapper.Map<T>(obj);
        }
    }
}