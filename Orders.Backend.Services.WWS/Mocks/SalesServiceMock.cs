﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Web;
using Common.Logging;
using MMS.Json;
using NLog;
using Orders.Backend.Services.Contracts.OrderService;
using WWS.Services;
using LogManager = Common.Logging.LogManager;

namespace Orders.Backend.Services.WWS.Mocks
{
    public class SalesServiceMock : ISalesService
    {
        private static Dictionary<string, OrderHeader> _reserves;
        private ILog _logger = LogManager.GetLogger("SalesServiceMock");

        static SalesServiceMock()
        {
            _reserves = new Dictionary<string, OrderHeader>();
        }

        public CreateSalesOrderResult CreateSalesOrder(OrderInfo order)
        {
            _logger.InfoFormat("Start create reserve for {0}", order.OnlineOrderId);
            _logger.InfoFormat("Serialized order:", JsonConvert.SerializeObject(order));
            var result = InternalCreateSalesOrderResult(order);

            if(result.OperationCode.Code == ReturnCode.Ok)
            {
                _reserves.Add(ToUniqueKey(result.OrderHeader.OrderNumber, order.SapCode), result.OrderHeader);
            }

            _logger.InfoFormat("Created reserve for {0}: {1}", order.OnlineOrderId, result.OperationCode.Code);
            return result;
        }

        private static CreateSalesOrderResult InternalCreateSalesOrderResult(OrderInfo order)
        {
            if(order == null)
            {
                throw new ArgumentNullException("order");
            }
            if(order.Customer == null)
            {
                throw new ArgumentNullException("order", "Order.Customer");
            }
            if(order.OrderLines == null)
            {
                throw new ArgumentNullException("order", "Order.OrderLines");
            }

            var result = new CreateSalesOrderResult();
            var header = result.OrderHeader = new OrderHeader();

            header.OrderNumber = GetNewOrderNumber();
            header.CreateTime = DateTime.Now;
            header.Delivery = new Delivery
            {
                Period = "dede",
                //Для заполнения комментария используется поле Surname так как комментария к доставке нет в контракте
                Comment = order.Customer.Surname,
                Date = DateTime.Now
            };
            header.DeliveryCustomer = new Customer
            {
                Address = order.Customer.Address,
                AddressIndex = order.Customer.Address,
                City = order.Customer.City,
                ClassificationNumber = "9090",
                CountryAbbreviation = "dd",
                Email = order.Customer.Email,
                INN = "111",
                KPP = "2222",
                Name = order.Customer.FirstName,
                Phone = order.Customer.Phone,
                Phone2 = "2222222",
                Salutation = "efewfew",
                Surname = order.Customer.Surname
            };
            header.DocBarcode = "";
            header.DownpaymentPrice = 111;
            header.DownpaymentPriceFull = new ReservePrice
            {
                GrossPrice = 1111,
                NetPrice = 3434,
                VAT = 18,
                VatPrice = 34324
            };
            header.InvoiceCustomer = new Customer
            {
                Address = order.Customer.Address,
                AddressIndex = order.Customer.Address,
                City = order.Customer.City,
                ClassificationNumber = "9090",
                CountryAbbreviation = "dd",
                Email = order.Customer.Email,
                INN = "111",
                KPP = "2222",
                Name = order.Customer.FirstName,
                Phone = order.Customer.Phone,
                Phone2 = "2222222",
                Salutation = "efewfew",
                Surname = order.Customer.Surname
            };
            header.LastUpdateTime = DateTime.Now;
            header.LeftoverPrice = new ReservePrice
            {
                GrossPrice = 1111,
                NetPrice = 3434,
                VAT = 18,
                VatPrice = 34324
            };

            header.OrderStatus = SalesOrderStatus.Unknown;
            header.OriginalSum = order.OrderLines.Sum(el => el.Price * el.Quantity);

            var orderLines = header.Lines = new List<OrderLine>();
            foreach(var line in order.OrderLines.Select(orderLine => new OrderLine
            {
                ArticleNo = orderLine.ArticleNo,
                DepartmentNumber = orderLine.DepartmentNumber,
                Description = orderLine.Description,
                FreeQuantity = orderLine.FreeQuantity,
                Price = orderLine.Price,
                PriceOrig = orderLine.PriceOrig,
                ProductGroupName = orderLine.ProductGroupName,
                Quantity = orderLine.Quantity,
                ReservedQuantity = orderLine.ReservedQuantity,
                StockNumber = orderLine.StockNumber,
                PositionNumber = orderLine.PositionNumber,
                ProductGroupNo = orderLine.ProductGroupNo,
                StoreNumber = orderLine.StoreNumber,
                TotalPrice = new ReservePrice
                {
                    GrossPrice = 1111,
                    NetPrice = 33,
                    VAT = 18,
                    VatPrice = 1000
                }
            }))
            {
                orderLines.Add(line);
            }

            header.SalesPersonName = "Test Test Manager";
            header.ShippingMethod = ShippingMethod.Delivery;
            header.TotalPrice = new ReservePrice
            {
                GrossPrice = 1111,
                NetPrice = 3434,
                VAT = 18,
                VatPrice = 34324
            };

            result.CreateTime = DateTime.Now.ToUniversalTime();
            result.OperationCode = new OperationCode
            {
                Code = ReturnCode.Ok,
                SubCode = string.Empty
            };

            result.OperationCode.Code = order.OrderLines.Any(el => el.ArticleNo.ToString().StartsWith("3")) ||
                                        ConfigurationManager.AppSettings["MockCreateReserveAction"] == "ReturnJBossError"
                //Or it breaks set
                ? ReturnCode.JBossException
                : ReturnCode.Ok;
            return result;
        }

        private static string GetNewOrderNumber()
        {
            return DateTime.Now.ToString("sshhmmff");
        }

        public CancelSalesOrderResult CancelSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            if(orderNumberInfo == null)
                throw new ArgumentNullException("orderNumberInfo");
            var key = ToUniqueKey(orderNumberInfo.OrderNumber, orderNumberInfo.SapCode);
            var orderHeader = _reserves[key];
            orderHeader.OrderStatus = SalesOrderStatus.Canceled;
            return new CancelSalesOrderResult
            {
                OperationCode = new OperationCode
                {
                    Code = ReturnCode.Ok
                }
            };
        }

        public GetSalesOrderResult GetSalesOrder(OrderNumberInfo orderNumberInfo)
        {
            if(orderNumberInfo == null)
                throw new ArgumentNullException("orderNumberInfo");

            var command = "";
            if(orderNumberInfo.OrderNumber.Contains("Команда"))
            {
                var parameters = orderNumberInfo.OrderNumber.Split('_');
                orderNumberInfo.OrderNumber = parameters[0];
                command = parameters[2];
            }
            var key = ToUniqueKey(orderNumberInfo.OrderNumber, orderNumberInfo.SapCode);
            if(!_reserves.ContainsKey(key))
            {
                return new GetSalesOrderResult
                {
                    OperationCode = new OperationCode
                    {
                        Code = ReturnCode.Exception
                    }
                };
            }
            var orderHeader = _reserves[key];

            if(!string.IsNullOrEmpty(command))
            {
                switch(command)
                {
                    case "КОплате":
                        orderHeader.OrderStatus = SalesOrderStatus.ToPay;
                        break;
                    case "Предоплата":
                        orderHeader.OrderStatus = SalesOrderStatus.Prepaid;
                        break;
                    case "Отменить":
                        orderHeader.OrderStatus = SalesOrderStatus.Canceled;
                        break;
                    case "СоздатьКопию":
                        if(string.IsNullOrEmpty(orderHeader.RefOrderNumber))
                        {
                            var copy = Copy(orderHeader);
                            var key1 = ToUniqueKey(copy.OrderNumber, orderNumberInfo.SapCode);
                            _reserves[key1] = copy;
                            return new GetSalesOrderResult
                            {
                                OrderHeader = copy,
                                OperationCode = new OperationCode
                                {
                                    Code = ReturnCode.Ok
                                }
                            };
                        }
                        break;
                    case "Оплатить":
                        orderHeader.OrderStatus = SalesOrderStatus.Paid;
                        break;
                    case "Трансформировать":
                        orderHeader.OrderStatus = SalesOrderStatus.Transformed;
                        if(string.IsNullOrEmpty(orderHeader.RefOrderNumber))
                        {
                            var copy = Copy(orderHeader);
                            copy.OrderStatus = SalesOrderStatus.Unknown;
                            var key1 = ToUniqueKey(copy.OrderNumber, orderNumberInfo.SapCode);
                            _reserves[key1] = copy;
                            orderHeader.RefOrderNumber = copy.OrderNumber;
                        }
                        break;
                }
                return new GetSalesOrderResult
                {
                    OrderHeader = orderHeader,
                    OperationCode = new OperationCode
                    {
                        Code = ReturnCode.Ok
                    }

                };
            }

            switch(orderHeader.Delivery.Comment)
            {
                case "ИЗМЕНЕННЫЙ И ОПЛАЧЕННЫЙ РЕЗЕРВ":
                    {
                        orderHeader.OrderStatus = SalesOrderStatus.Transformed;
                        if(string.IsNullOrEmpty(orderHeader.RefOrderNumber))
                        {
                            var copy = Copy(orderHeader);
                            copy.Delivery.Comment = "ОПЛАЧЕННЫЙ РЕЗЕРВ";
                            copy.OrderStatus = SalesOrderStatus.Paid;
                            var key1 = ToUniqueKey(copy.OrderNumber, orderNumberInfo.SapCode);
                            _reserves[key1] = copy;
                            orderHeader.RefOrderNumber = copy.OrderNumber;
                        }
                        break;
                    }
                case "ИЗМЕНЕННЫЙ И ОТМЕНЕННЫЙ РЕЗЕРВ":
                    {
                        orderHeader.OrderStatus = SalesOrderStatus.Transformed;
                        if(string.IsNullOrEmpty(orderHeader.RefOrderNumber))
                        {
                            var copy = Copy(orderHeader);
                            copy.Delivery.Comment = "ОТМЕНЕННЫЙ РЕЗЕРВ";
                            copy.OrderStatus = SalesOrderStatus.Canceled;
                            var key1 = ToUniqueKey(copy.OrderNumber, orderNumberInfo.SapCode);
                            _reserves[key1] = copy;
                            orderHeader.RefOrderNumber = copy.OrderNumber;
                        }

                        break;
                    }
                case "ИЗМЕНЕННЫЙ РЕЗЕРВ":
                    {
                        orderHeader.OrderStatus = SalesOrderStatus.Transformed;
                        if(string.IsNullOrEmpty(orderHeader.RefOrderNumber))
                        {
                            var copy = Copy(orderHeader);
                            copy.Delivery.Comment = "НЕИЗВЕСТНЫЙ";
                            copy.OrderStatus = SalesOrderStatus.Unknown;
                            var key1 = ToUniqueKey(copy.OrderNumber, orderNumberInfo.SapCode);
                            _reserves[key1] = copy;
                            orderHeader.RefOrderNumber = copy.OrderNumber;
                        }
                        break;
                    }
                case "ОПЛАЧЕННЫЙ РЕЗЕРВ":
                    orderHeader.OrderStatus = SalesOrderStatus.Paid;
                    break;
                case "ОТМЕНЕННЫЙ РЕЗЕРВ":
                    orderHeader.OrderStatus = SalesOrderStatus.Canceled;
                    break;
                default:
                    //orderHeader.OrderStatus = SalesOrderStatus.Unknown;
                    break;
            }
            return new GetSalesOrderResult
            {
                OrderHeader = orderHeader,
                OperationCode = new OperationCode
                {
                    Code = ReturnCode.Ok
                }

            };
        }

        private OrderHeader Copy(OrderHeader orderHeader)
        {
            var copy = DeepClone(orderHeader);
            copy.OrderNumber = GetNewOrderNumber();
            return copy;
        }

        private static T DeepClone<T>(T obj)
        {
            using(var ms = new MemoryStream())
            {
                var ser = new DataContractSerializer(typeof(T));
                ser.WriteObject(ms, obj);
                ms.Position = 0;
                return (T)ser.ReadObject(ms);
            }
        }

        private string ToUniqueKey(string reserveId, string sapCode)
        {
            return string.Concat(reserveId, sapCode);
        }

        public ChangeDeliveryDateResult ChangeDeliveryDate(OrderNumberInfo orderNumberInfo, DateTime deliveryDate)
        {
            if(orderNumberInfo == null)
                throw new ArgumentNullException("orderNumberInfo");
            var key = ToUniqueKey(orderNumberInfo.OrderNumber, orderNumberInfo.SapCode);
            return new ChangeDeliveryDateResult
            {
                OperationCode = new OperationCode
                {
                    Code = ReturnCode.Ok
                }
            };

        }
    }
}