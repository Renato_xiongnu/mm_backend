﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.ServiceModel;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;
using Orders.Backend.Services.WWS.Mapping;
using WwsTunnel.Common.Helpers;
using System.Diagnostics.Contracts;

namespace Orders.Backend.Services.WWS.Helpers
{
    public class ResultBuilder<T>
        where T : OperationResult, new()
    {
        protected OrderInfo Order { get; set; }

        protected OperationCode OperationCode { get; set; }

        protected Interfaces.SalesDocumentInfo SalesDocumentInfo { get; set; }

        public ResultBuilder()
        {
            OperationCode = new OperationCode
            {
                Code = ReturnCode.Ok,
                SubCode = ""
            };
        }

        public void SetOrderInfo(OrderInfo order)
        {
            Order = order;
        }

        public void SetOperationCode(OperationCode code)
        {
            OperationCode = code;
        }

        public void SetOperationCodeFromException(Exception ex)
        {
            OperationCode = PrepareOperationCode(ex);
        }

        public void SetSalesDocumentInfo(Interfaces.SalesDocumentInfo salesDocumentInfo)
        {
            SalesDocumentInfo = salesDocumentInfo;
        }

        public virtual T BuildResult()
        {
            var result = new T
            {
                OperationCode = OperationCode
            };
            return result;
        }

        public static OrderHeader GetOrderHeader(Interfaces.SalesDocumentInfo documentInfo, Interfaces.CreateSalesDocumentResponse response = null)
        {
            Contract.Requires(documentInfo != null);

            var barCode = response != null ? response.SalesDocBarCode : ServiceHelper.GetSalesDocBarcode(documentInfo.SalesDocNo);

            var orderLines = documentInfo.SalesDocumentLines.Select(MappingExtenisonsV2.MapToOrderLine).Where(t => t != null).ToList();

            var lineHelper = SubLineHelper.CreateHelper(orderLines);
            var resultOrdeLines = lineHelper.CorrelateLines();
            var reservePrice = lineHelper.CalcReservePrice();

            var header = new OrderHeader
            {
                DocBarcode = barCode,
                DocBarcodeImage = Convert.ToBase64String(BarcodeInter25.GetImage(barCode, false)),

                DeliveryCustomer = documentInfo.DeliveryCustomer.Customer.MapToCustomer(),
                InvoiceCustomer = (documentInfo.InvoiceCustomer ?? documentInfo.DeliveryCustomer.Customer).MapToCustomer(),

                OrderNumber = documentInfo.SalesDocNo.ToString(),
                CreateTime = documentInfo.SalesDocDate,
                ShippingMethod = documentInfo.PickupType.MapToShippingMethod(),
                WwsPaymentStatus = documentInfo.PaymentStatus,
                WwsSalesDocStatus = documentInfo.SalesDocStatus,
                WwsSalesDocType = documentInfo.SalesDocType,
                OriginalSum = documentInfo.OriginalSum ?? 0,
                LastUpdateTime = documentInfo.SalesDocDate,
                SalesPersonName = documentInfo.SalesPersonName,
                TotalPrice = reservePrice,
                DownpaymentPriceFull = new ReservePrice
                {
                    GrossPrice = documentInfo.DownPaymentPrice ?? 0
                },
                DownpaymentPrice = documentInfo.DownPaymentPrice ?? 0,
                LeftoverPrice = new ReservePrice(),
                PrintableInfo = documentInfo.PrintInfo,
                //OutletInfo = _salesDocumentInfo.,
                ProductPickupInfo = documentInfo.ProductPickupInfo,
                Lines = resultOrdeLines,
                Delivery = documentInfo.DeliveryCustomer.MapToDelivery()
            };
            return header;
        }

        /// <summary>
        /// Богоподобный метод, который собирает назад иерархию
        /// с учетом всех вышереализованных разбивок.
        /// Скопирован из SalesServiceV1
        /// </summary>
        /// <param name="order"></param>
        /// <param name="header"></param>
        public static void CreateHierarchy(OrderInfo order, OrderHeader header)
        {
            Contract.Requires(order != null);
            Contract.Requires(header != null);
            if(order == null)
            {
                throw new ArgumentNullException("order", "OrderInfo can not be Null. Call SetOrderInfo method before call CreateHierarchy method.");
            }
            Contract.EndContractBlock();

            var orderRootLines = order.OrderLines;

            foreach(var orderRoorLine in orderRootLines)
            {
                var loops = orderRoorLine.Quantity;
                //if is set
                if(orderRoorLine.IsSet())
                {
                    var subIds = orderRoorLine.SubLines.Select(x => new { x.ArticleNo, x.Price, x.Quantity });
                    var headerSubLines = new List<OrderLine>();
                    foreach(var subId in subIds)
                    {
                        var id = subId;
                        var byQty = header.Lines.Where(x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == loops).Take(loops);
                        if(!byQty.Any())
                            byQty = header.Lines.Where(x => x.ArticleNo == id.ArticleNo && x.Price == id.Price && x.Quantity == 1).Take(loops);

                        headerSubLines.AddRange(byQty);
                    }

                    foreach(var headerSubLine in headerSubLines)
                    {
                        header.Lines.Remove(headerSubLine);
                    }
                    var distSubLines = headerSubLines.GroupBy(x => x.ArticleNo).Select(x => x.First()).ToList();
                    foreach(var distSubLine in distSubLines)
                    {
                        distSubLine.TotalPrice = new ReservePrice();
                        distSubLine.Quantity = loops;
                    }

                    orderRoorLine.ProductType = ProductType.Set;
                    orderRoorLine.FromModelToWwsModel();
                    orderRoorLine.TotalPrice = new ReservePrice();
                    orderRoorLine.SubLines = new OrderLine[headerSubLines.Count];
                    orderRoorLine.SubLines = distSubLines.ToArray();
                    header.Lines.Add(orderRoorLine);

                }
                else
                {
                    var byQty = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == loops).Take(loops).ToList();
                    if(!byQty.Any())
                        byQty = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price && x.Quantity == 1).Take(loops).ToList();

                    //var headerRootLines = header.Lines.Where(x => x.ArticleNo == orderRoorLine.ArticleNo && x.Price == orderRoorLine.Price).Take(loops).ToList();
                    foreach(var headerRoorLine in byQty)
                    {
                        header.Lines.Remove(headerRoorLine);
                    }
                    var headerRootLine = byQty.FirstOrDefault();
                    if(headerRootLine != null)
                    {
                        headerRootLine.Quantity = loops;
                        headerRootLine.TotalPrice = new ReservePrice();
                        //headerRootLine.ProductType = ProductType.Article;
                        header.Lines.Add(headerRootLine);
                    }
                }
            }
        }

        public static OperationCode PrepareOperationCode(Exception ex)
        {
            var operationCode = new OperationCode();
            var faultException = ex as FaultException;
            if(faultException != null)
            {
                var errors = ParseFaultException(faultException).ToArray();
                var subCode = FindJBossSubCode(errors.Last());
                var errorCodes = "JBoss ErrorCode:[" + string.Join(";", errors.Take(3)) + "]";
                operationCode.Code = ReturnCode.JBossException;
                operationCode.SubCode = subCode.Item1;
                operationCode.MessageText = subCode.Item2 + ". " + errorCodes + ". Message: " +
                                            string.Join(" ", errors.Skip(3));
            }
            else
            {
                operationCode.Code = ReturnCode.Exception;
                operationCode.SubCode = SubCodes.SystemException;
                operationCode.MessageText = MessageTexts.SystemException + ". " + ex.GetExceptionFullInfoText();
            }

            return operationCode;
        }

        private const string ErrorStart = "msserr";
        private static readonly Regex ExceptionRegex = new Regex(ErrorStart + @":(\d+);(\d+);(\d+);([^;]+);\d+;(.*?)(\s+at(\s+(.{0,5}))|\z)");
        private const string MarkerMissingLines = "MISSING_SALESDOCLINES";
        private const string MarkerSalesDocNotFound = "SALESDOC_NOTFOUND";
        private const string MarkerSolidSqlError = "SOLID SQL Error";

        private static IEnumerable<string> ParseFaultException(FaultException ex)
        {
            return ExceptionRegex.Split(ex.Message).Where(t => !string.IsNullOrEmpty(t)).Take(6);
        }

        private static Tuple<string, string> FindJBossSubCode(string message)
        {
            var index = message.IndexOf(MarkerMissingLines, StringComparison.Ordinal);

            if(index > -1)
            {
                return new Tuple<string, string>(SubCodes.JBOSSMissingLines, MessageTexts.JBOSSMissingLines);
            }

            index = message.IndexOf(MarkerSolidSqlError, StringComparison.Ordinal);

            if(index > -1)
            {
                return new Tuple<string, string>(SubCodes.JBOSSSqlError, MessageTexts.JBOSSSqlError);
            }

            index = message.IndexOf(MarkerSalesDocNotFound, StringComparison.Ordinal);
            if(index > -1)
            {
                return new Tuple<string, string>(SubCodes.JBOSSNotFound, MessageTexts.JBOSSSalesDocNotFound);
            }

            return new Tuple<string, string>(SubCodes.JBOSSUnknow, MessageTexts.JBOSSUnknow);
        }

        public void SetValidationExceptionOperationCode()
        {
            OperationCode = new OperationCode
            {
                Code = ReturnCode.ValidationException,
                SubCode = SubCodes.ValidationError,
                MessageText = MessageTexts.ValidationError + ". " + string.Format(ConfigurationManager.AppSettings["OrderWithoutLinesText"]),
            };
        }

        public void SetValidationExceptionOperationCode(string orderNumber)
        {
            OperationCode = new OperationCode
            {
                Code = ReturnCode.ValidationException,
                SubCode = SubCodes.ValidationError,
                MessageText = MessageTexts.ValidationError + ". " + string.Format(ConfigurationManager.AppSettings["OrderShouldBeNumericText"], orderNumber),
            };
        }

        public void SetSystemExceptionOperationCode()
        {
            OperationCode = new OperationCode
            {
                Code = ReturnCode.Exception,
                SubCode = SubCodes.SystemException,
                MessageText = MessageTexts.SystemException + ". " + string.Format(ConfigurationManager.AppSettings["UnxpectedErrorOccuredText"])
            };
        }

        public void SetLogicErrorOperationCode(ICollection<string> diffLines)
        {
            OperationCode = new OperationCode
            {
                Code = ReturnCode.OkPartially,
                SubCode = SubCodes.LogicError,
                MessageText = MessageTexts.LogicError + ". " + ConfigurationManager.AppSettings["ReserveCreatedWithDiffLinesText"] + Environment.NewLine + string.Join(Environment.NewLine, diffLines)
            };
        }

    }
}