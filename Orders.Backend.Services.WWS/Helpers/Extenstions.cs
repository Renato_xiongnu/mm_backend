﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using WWS.Services.Proxies.PosServicesSalesProcessing;
using WWS.Services;
using WWS.Services.DataContracts;
using WwsTunnel.Common.Helpers;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;

namespace Orders.Backend.Services.WWS.Helpers
{
    public static class Extenstions
    {

        public static Customer FromJbosObjectToModel(this CustomerTO customer, AddressTO address)
        {
            return new Customer()
            {
                AddressIndex = address.zip,
                City = address.city,
                ClassificationNumber = customer.customerNo.ToString(CultureInfo.InvariantCulture),
                CountryAbbreviation = address.countryTO.isoCountryCode,
                Email = address.email,
                Birthday = customer.birthday,
                INN = customer.taxNo,
                KPP = customer.sector,
                Name = customer.firstName,
                Surname = customer.lastName,
                Phone = address.phone,
                Phone2 = address.cellphone,
                Salutation = customer.salutationTO.value,
                Address = string.Format("{0} {1} {2}", address.street1, address.street2, address.street3)
            };
        }


        public static OrderHeader FromJbosObjectToModel(this SalesDocTO salesDoc, CustomerTO deliveryCustomer)
        {
            var header = salesDoc.salesDocHeadTO;
            var headerDetail = salesDoc.salesDocDetailTO;

            List<OrderLine> orderLines;
            if (headerDetail.salesDocLineTOList == null || headerDetail.salesDocLineTOList.Length == 0)
                orderLines = new List<OrderLine>();
            else
            {
                orderLines = headerDetail.salesDocLineTOList.Select(t =>
                {
                    ProductType productType;
                    int productId;
                    if (t.isWarrantyInsurance.HasValue && t.isWarrantyInsurance.Value)
                    {
                        productType = ProductType.WarrantyPlus;
                        productId = Convert.ToInt32(t.articleNo);
                    }
                    else if (string.IsNullOrEmpty(t.articleNo))
                    {
                        productType = ProductType.Set;
                        productId = t.productSetNo.HasValue? t.productSetNo.Value: 0;
                    }
                    else
                    {
                        productType = ProductType.Article;
                        productId = Convert.ToInt32(t.articleNo);
                    }
                    return FromJbosObjectToModel(t, productId, productType);
                }).Where(t => t != null).ToList();
                
            }

            var lineHelper = SubLineHelper.CreateHelper(orderLines);
            var resultOrdeLines = lineHelper.CorrelateLines();
            var reservePrice = lineHelper.CalcReservePrice();

            var orderHeader = new OrderHeader()
            {
                OrderNumber = header.salesDocNo.ToString(),

                CreateTime = header.createTime,

                ShippingMethod = ContractHelper.PickupTypeToShipping(header.pickupType.ToEnum<PickupType>()),

                WwsPaymentStatus = header.paymentStatus.ToEnum<Interfaces.PaymentStatus>(),

                WwsSalesDocStatus = header.salesDocStatus.ToEnum<Interfaces.SalesDocumentStatus>(),

                WwsSalesDocType = header.salesDocType.ToEnum<Interfaces.SalesDocumentType>(),

                OriginalSum = Convert.ToDecimal(headerDetail.originalSalesDocSum),

                LastUpdateTime = header.lastUpdateTime,

                SalesPersonName = header.salesPersonName,

                TotalPrice = reservePrice,

                DownpaymentPriceFull = new ReservePrice
                {
                    GrossPrice = Convert.ToDecimal(header.downpaymentPrice)
                },

                DownpaymentPrice = Convert.ToDecimal(header.downpaymentPrice),

                LeftoverPrice = new ReservePrice(),

                PrintableInfo = header.printableInfo,

                OutletInfo = header.outletInfo,

                ProductPickupInfo = header.productPickupInfo,

                Lines = resultOrdeLines
            };
            orderHeader.OrderStatus = ContractHelper.CalcSalesOrderStatus(orderHeader);

            if (orderHeader.OrderStatus == SalesOrderStatus.Transformed)
            {
                if (header.successorSalesDocInfoTO != null)
                    orderHeader.RefOrderNumber = header.successorSalesDocInfoTO.salesDocNo.ToString();
            }

            return orderHeader;
        }

        private static OrderLine FromJbosObjectToModel(this SalesDocLineTO salesDocLine, int productId, ProductType productType)
        {
            return new OrderLine()
            {
                ArticleNo = productId,

                ManufacturerName = salesDocLine.manufacturerName,

                ProductGroupName = salesDocLine.productGroupName,

                ProductGroupNo = salesDocLine.productGroupNo ?? 0,

                Description = salesDocLine.productOrSetDescription,

                Price = Convert.ToDecimal(salesDocLine.retailPrice),

                PriceOrig = Convert.ToDecimal(salesDocLine.originalRetailPrice),

                FreeQuantity = salesDocLine.freeQuantity ?? 0,

                ReservedQuantity = salesDocLine.reservedQuantity ?? 0,

                Quantity = salesDocLine.quantity ?? 0,

                StockNumber = salesDocLine.stockAreaNo ?? 0,

                StoreNumber = salesDocLine.stockAreaOutletNo ?? 0,

                DepartmentNumber = salesDocLine.departmentNo ?? 0,

                PositionNumber = salesDocLine.posNo ?? 0,

                LineNumber = salesDocLine.salesDocLineNo ?? 0,

                HasShippedFromStock = salesDocLine.productPickupSign ?? false,

                SerialNumber = salesDocLine.serialNo,

                TotalPrice = new ReservePrice
                {
                    VAT = Convert.ToDecimal(salesDocLine.vatRate),
                    VatPrice = Convert.ToDecimal(salesDocLine.vatAmountSum),
                    NetPrice = Convert.ToDecimal(salesDocLine.netAmountSum),
                    GrossPrice = Convert.ToDecimal(salesDocLine.retailPriceSum)
                },
                ProductType = productType,
                WarrantyInsurance = new WarrantyInsurance
                {
                    Number = salesDocLine.warrantyInsuranceNo,
                    WwsExtensionPrint = salesDocLine.warrantyExtensionPrint,
                    WwsCertificateState = salesDocLine.warrantyInsuranceState
                }
            };
        }


        public static OrderLineInfo FromModelToWwsModel(this OrderLine orderLine)
        {
            OrderLineInfo result;
            if (ContractHelper.IsSet(orderLine.ArticleNo))
            {
                result = new SetLineInfo();
                var line = (SetLineInfo)result;
                if (orderLine.SubLines != null)
                    line.Lines = orderLine.SubLines.Select(FromModelToWwsModel).ToList();
            }
            else
            {
                result = new OrderLineInfo();
            }

            result.ArticleNo = orderLine.ArticleNo;
            result.ArticleTitle = orderLine.ArticleNo.ToString(CultureInfo.InvariantCulture);
            result.Price = orderLine.Price;
            result.Quantity = orderLine.Quantity;
            result.SerialNumber = orderLine.SerialNumber;
            result.WarrantyInsuranceArticleNo = orderLine.WarrantyInsurance != null ? orderLine.WarrantyInsurance.RelatedArticleNo : 0;
            result.ProductPickupSign = orderLine.HasShippedFromStock;
            result.StockNumber = orderLine.StockNumber;
            return result;
        }

        public static OrderLineInfo Copy(this OrderLineInfo orderLine)
        {
            OrderLineInfo result;
            var setLineInfo = orderLine as SetLineInfo;
            if (setLineInfo!=null)
            {
                result = new SetLineInfo();
                var line = (SetLineInfo)result;
                if (setLineInfo.Lines != null)
                    line.Lines = setLineInfo.Lines.Select(Copy).ToList();
            }
            else
            {
                result = new OrderLineInfo();
            }

            result.ArticleNo = orderLine.ArticleNo;
            result.ArticleTitle = orderLine.ArticleNo.ToString(CultureInfo.InvariantCulture);
            result.Price = orderLine.Price;
            result.Quantity = orderLine.Quantity;
            result.SerialNumber = orderLine.SerialNumber;
            result.WarrantyInsuranceArticleNo = orderLine.WarrantyInsuranceArticleNo;
            result.ProductPickupSign = orderLine.ProductPickupSign;
            result.StockNumber = orderLine.StockNumber;
            return result;
        }

        /// <summary>
        ///  Check if line is set
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static Boolean IsSet(this OrderLine line)
        {
            return line.ProductType == ProductType.Set || line.ArticleNo.ToString(CultureInfo.InvariantCulture).Length == 9;
        }

        public static string ToMessageAndCompleteStacktrace(this Exception exception)
        {
            var e = exception;
            var s = new StringBuilder();
            while (e != null)
            {
                s.AppendLine("Exception type: " + e.GetType().FullName);
                s.AppendLine("Message       : " + e.Message);
                s.AppendLine("Stacktrace:");
                s.AppendLine(e.StackTrace);
                s.AppendLine();
                e = e.InnerException;
            }
            return s.ToString();
        }

        public static string Truncate(this string source, int length)
        {
            return (source != null && source.Length > length ? source.Substring(0, length) : source);
        }
    }
}