﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orders.Backend.Services.WWS.Helpers
{
    public class SetPriceRecalculator
    { 
        /// <summary>
        /// Магический метод
        /// </summary>
        /// <param name="setLines"></param>
        /// <param name="setPrice"></param>
        /// <param name="decimals"></param>
        public static void RecalculatePrices(List<SetItem> setLines, Decimal setPrice, Int32 decimals = 0)
        {
            var setRowSum = CalcSetSum(setLines);

            var ratio = setPrice / setRowSum;

            setLines.ForEach(s => s.Price = Math.Round(s.Price * ratio, decimals));
            var delta = CalcDelta(setLines, setPrice);

            if (delta == 0)
                return;

            RecalculationMultipleElements(setLines, setPrice);
            delta = CalcDelta(setLines, setPrice);
            if (delta == 0)
                return;
            SimplifiedRecalculation(setLines, setPrice);
            delta = CalcDelta(setLines, setPrice);
            if (delta == 0)
                return;
            var single = setLines.FirstOrDefault(x => x.Quantity == 1);
            if (single != null)
                single.Price += delta;
            else
            {
                var maxValue = setLines.Max(x => x.Quantity);
                single = setLines.FirstOrDefault(x => x.Quantity == maxValue);
                single.Quantity--;
                setLines.Add(new SetItem { ArticleNo = single.ArticleNo, Quantity = 1, Price = single.Price + delta });
            }

        }

        private static void SimplifiedRecalculation(List<SetItem> setLines, Decimal setPrice)
        {
            var delta = CalcDelta(setLines, setPrice);
            var absDelta = Math.Abs(delta);
            var item = setLines.FirstOrDefault(x => x.Quantity % absDelta == 0);
            if (item != null)
            {
                setLines.ForEach(x =>
                {

                    if (x.ArticleNo == item.ArticleNo)
                    {
                        if (delta > 0)
                            x.Price += absDelta;
                        else
                            x.Price -= absDelta;
                    }
                    else
                    {
                        if (delta > 0)
                            x.Price -= absDelta;
                        else
                            x.Price += absDelta;
                    }

                });
            }
        }

        /// <summary>
        /// Пересчет Кратных Элементов
        /// если дельта кратна количеству поделементов в наборе.
        /// </summary>
        /// <param name="setLines"></param>
        /// <param name="setPrice"></param>
        private static void RecalculationMultipleElements(List<SetItem> setLines, Decimal setPrice)
        {
            var delta = CalcDelta(setLines, setPrice);
            var absDelta = Math.Abs(delta);
            setLines.ForEach(x =>
            {
                delta = setPrice - setLines.Sum(s => s.Price * s.Quantity);
                if (delta == 0)
                    return;
                if (x.Quantity >= absDelta && x.Quantity % absDelta == 0 && absDelta != 1)
                {
                    delta = Math.Round(x.Quantity / delta);
                    x.Price += delta;

                }
                else if (x.Quantity < absDelta && absDelta % x.Quantity == 0 && absDelta != 1)
                {
                    delta = Math.Round(delta / x.Quantity);
                    x.Price += delta;
                }
            });
        }

        private static Decimal CalcDelta(IEnumerable<SetItem> setLines, Decimal setPrice)
        {
            return setPrice - CalcSetSum(setLines);
        }

        private static Decimal CalcSetSum(IEnumerable<SetItem> setLines)
        {
            return setLines.Sum(s => s.Price * s.Quantity);
        }
    }

}