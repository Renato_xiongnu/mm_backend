﻿using System;
using System.Linq;
using MMS.Integration.Gms.WebServices.Interfaces;
using System.Text.RegularExpressions;

namespace Orders.Backend.Services.WWS.Helpers
{
    public static class ContractHelper
    {
        public static global::WWS.Services.PaymentType PaymentTypeToWWsType(PaymentType paymentType)
        {
            switch(paymentType)
            {
                case PaymentType.Cash:
                    return global::WWS.Services.PaymentType.Cash;
                case PaymentType.CreditCard:
                case PaymentType.SocialCard:
                case PaymentType.Online:
                    return global::WWS.Services.PaymentType.CreditCard;
                case PaymentType.Yandex:
                    return global::WWS.Services.PaymentType.Yandex;
                default:
                    throw new ArgumentOutOfRangeException("paymentType");
            }
        }

        public static ShippingMethod PickupTypeToShipping(PickupType pickupType)
        {
            return pickupType == PickupType.DELIVERY ? ShippingMethod.Delivery : ShippingMethod.Pickup;
        }

        public static ShippingMethod PickupTypeToShipping(global::WWS.Services.PickupType pickupType)
        {
            return pickupType == global::WWS.Services.PickupType.DELIVERY ? ShippingMethod.Delivery : ShippingMethod.Pickup;
        }

        public static global::WWS.Services.PickupType ShippingToPickupType(ShippingMethod shipping)
        {
            return shipping == ShippingMethod.Delivery ? global::WWS.Services.PickupType.DELIVERY : global::WWS.Services.PickupType.CARRY;
        }


        internal static bool IsSet(int articleNo)
        {
            return articleNo.ToString().Length == 9;
        }




        internal static SalesOrderStatus CalcSalesOrderStatus(OrderHeader orderHeader)
        {
            if(orderHeader.WwsSalesDocType == SalesDocumentType.FINANCING)
            {
                if(orderHeader.WwsSalesDocStatus == SalesDocumentStatus.CLOSED)
                    return SalesOrderStatus.Paid;

                if(orderHeader.WwsPaymentStatus == PaymentStatus.PREPAID &&
                   orderHeader.WwsSalesDocStatus == SalesDocumentStatus.PRINTED)
                    return SalesOrderStatus.Prepaid;

                if(orderHeader.WwsSalesDocStatus == SalesDocumentStatus.APPLIED &&
                    orderHeader.WwsPaymentStatus == PaymentStatus.NONE)
                    return SalesOrderStatus.ToPay;
            }

            if(orderHeader.WwsSalesDocType == SalesDocumentType.CASH_SALE)
            {
                if(orderHeader.WwsPaymentStatus == PaymentStatus.PREPAID &&
                   orderHeader.WwsSalesDocStatus == SalesDocumentStatus.PRINTED)
                    return SalesOrderStatus.Prepaid;
            }

            if(orderHeader.WwsSalesDocType == SalesDocumentType.PURCHASE_ON_ACCOUNT)
            {
                if(orderHeader.WwsSalesDocStatus == SalesDocumentStatus.CLOSED)
                    return SalesOrderStatus.Paid;
            }

            if(orderHeader.WwsPaymentStatus == PaymentStatus.TOPAY &&
                orderHeader.WwsSalesDocStatus == SalesDocumentStatus.PRINTED)
                return SalesOrderStatus.ToPay;

            if(orderHeader.WwsSalesDocStatus == SalesDocumentStatus.CLOSED)
            {
                if(orderHeader.WwsPaymentStatus == PaymentStatus.PAID)
                    return SalesOrderStatus.Paid;

                if(orderHeader.WwsPaymentStatus == PaymentStatus.TOPAYBACK)
                    return SalesOrderStatus.PaidBack; //in prod return SalesOrderStatus.Unknown
            }

            if(orderHeader.WwsSalesDocStatus == SalesDocumentStatus.CANCELED)
            {
                if(orderHeader.WwsPaymentStatus == PaymentStatus.PAIDBACK ||
                    orderHeader.WwsPaymentStatus == PaymentStatus.TOPAYBACK ||
                    orderHeader.WwsPaymentStatus == PaymentStatus.NONE)
                    return SalesOrderStatus.Canceled;
            }

            if(orderHeader.WwsSalesDocStatus == SalesDocumentStatus.TRANSFORMED)
                return SalesOrderStatus.Transformed; //in prod return SalesOrderStatus.Unknown

            return SalesOrderStatus.Unknown;
        }




        /// <summary>
        ///Формата номера два (на данный момент) - 002-12-345 и 002-12345678
        ///Если первый - должны вернуть 212345 (все цифры использовать),
        ///иначе - все, кроме первых трех (12345678)
        /// </summary>
        public static int ToWWsOnlineOrderId(string onlineOrderId)
        {
            const string newNumberFormat = "002-12345678";
            var newNumberFormatLength = newNumberFormat.Length;
            if(onlineOrderId.Length == newNumberFormatLength)
            {
                var newId = Regex.Replace(onlineOrderId, @"[^\d]", "");

                newId = new string(newId.Skip(3).Take(8).ToArray());
                return Convert.ToInt32(newId);
            }

            var result = Regex.Replace(onlineOrderId, @"[^\d]", "");
            if(result.Length > 8)
            {
                result = result.Substring(0, 8);
            }
            return Convert.ToInt32(result);
        }



    }
}