﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Orders.Backend.Services.WWS.Mapping;
using MMS.StoreService.Interfaces;
using MMS.StoreService.Interfaces.DataContracts;
using Common.Logging;
using WWSSettings = Orders.Backend.Services.WWS.Properties.Settings;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;
using System.Configuration;

namespace Orders.Backend.Services.WWS.Helpers
{
    public class RequestBuilder
    {
        private readonly ILog _logger;

        private readonly OrderInfo _orderInfo;

        private List<string> _printableInfo;

        public Func<long, bool> IsSerialRequired { get; private set; }

        public Func<long, List<SetItem>> GetSetItemsBySetArticleNumberFromStock { get; private set; }

        public Func<long, List<SetItem>> GetSetItemsBySetArticleNumberFromWws { get; private set; }

        public RequestBuilder(OrderInfo orderInfo, Func<long, bool> isSerialRequired, Func<long, List<SetItem>> getSetItemsFromStock, Func<long, List<SetItem>> getSetItemsFromWws)
        {
            _logger = LogManager.GetLogger(GetType().Name);
            _orderInfo = orderInfo;
            _printableInfo = new List<string>();
            IsSerialRequired = isSerialRequired ?? (n => false);
            GetSetItemsBySetArticleNumberFromStock = getSetItemsFromStock ?? (n => new List<SetItem>());
            GetSetItemsBySetArticleNumberFromWws = getSetItemsFromWws ?? (n => new List<SetItem>());
        }

        public Interfaces.CreateSalesDocumentRequest BuildCreateRequest()
        {
            var request = MapToRequest(_orderInfo);

            switch(_orderInfo.PaymentType)
            {
                case PaymentType.SocialCard:
                    ProcessCashlessOrder(_orderInfo, request, WWSSettings.Default.SocialCardBankOid, WWSSettings.Default.SocialCardDefaultDeliveryDownpayment);
                    break;
                case PaymentType.Yandex:
                    ProcessCashlessOrder(_orderInfo, request, WWSSettings.Default.YandexBankOid, WWSSettings.Default.YandexDefaultDeliveryDownpayment);
                    break;
                case PaymentType.Online:
                    ProcessCashlessOrder(_orderInfo, request, WWSSettings.Default.RaiffeisenBankOid, WWSSettings.Default.RaiffeisenDefaultDownpayment);
                    break;
            }
            request.PrintableInfo += string.Join("; ", _printableInfo.ToArray());
            request.PrintableInfo = request.PrintableInfo.Truncate(254);
            request.OutletInfo = request.OutletInfo.Truncate(254);
            return request;
        }

        /// <summary>
        /// Map TT OrderInfo to WWS OrderInfo
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public Interfaces.CreateSalesDocumentRequest MapToRequest(OrderInfo order)
        {
            var request = new Interfaces.CreateSalesDocumentRequest
            {
                Bank = new Interfaces.BankInfo(),
                CreditNo = order.CreditNo ?? order.OnlineOrderId,
                DeliveryDate = order.DeliveryDate,
                DocumentType = order.PaymentType.MapToSalesDocumentType(),
                PickupType = order.ShippingMethod.MapToPickupType(),
                IsOnlineOrder = order.IsOnlineOrder,
                OnlineOrderId = order.IsOnlineOrder ? order.OnlineOrderId.MapToWwsOnlineOrderId() as int? : null,
                Downpayment = order.Downpayment,
                PrintableInfo = order.PrintableInfo ?? string.Empty,
                OutletInfo = string.IsNullOrEmpty(order.OutletInfo) ? null : order.OutletInfo,
                ProductPickupInfo = order.ProductPickupInfo,
                Customer = order.Customer.MapToWwsCustomerInfo(),
                Order = MapToWwsOrderInfo(order)
            };
            return request;
        }

        public Interfaces.OrderInfo MapToWwsOrderInfo(OrderInfo order)
        {
            var orderInfo = new Interfaces.OrderInfo
            {
                OrderDate = DateTime.Now.ToUniversalTime(),
                //OrderNumber
                OrderLines = MapToOrderLineBaseList(order.OrderLines)
            };
            return orderInfo;
        }

        public List<Interfaces.OrderLineBase> MapToOrderLineBaseList(List<OrderLine> lines)
        {
            var simpleLines = lines.Where(l => !(l.IsSet())).Select(MapOrderLineSimple);
            var setLines = lines.Where(Extenstions.IsSet).SelectMany(MapOrderLineSet);
            var flatLines = simpleLines.Union(setLines).SelectMany(FlatOrderLineWithSerial);
            return flatLines.Cast<Interfaces.OrderLineBase>().ToList();
        }

        public Interfaces.OrderLineInfo MapOrderLineSimple(OrderLine orderLine)
        {
            var result = new Interfaces.OrderLineInfo
            {
                ArticleNo = orderLine.ArticleNo,
                ArticleTitle = orderLine.ArticleNo.ToString(CultureInfo.InvariantCulture),
                Price = orderLine.Price,
                Quantity = orderLine.Quantity,
                SerialNumber = orderLine.SerialNumber,
                WarrantyInsuranceArticleNo = orderLine.WarrantyInsurance != null ? orderLine.WarrantyInsurance.RelatedArticleNo as int? : null,
                ProductPickupSign = orderLine.HasShippedFromStock
            };
            //result.StockNumber = orderLine.StockNumber;
            return result;
        }

        public IEnumerable<Interfaces.OrderLineInfo> MapOrderLineSet(OrderLine orderLine)
        {
            if(_printableInfo.Count == 0)
            {
                _printableInfo.Add("ВНИМАНИЕ: продажа набора №" + orderLine.ArticleNo);
            }
            else
            {
                _printableInfo.Add("№" + orderLine.ArticleNo);
            }
            try
            {
                var itemsInStock = GetSetItemsBySetArticleNumberFromStock(orderLine.ArticleNo);
                var itemsInWws = GetSetItemsBySetArticleNumberFromWws(orderLine.ArticleNo);
                var errorMessage = String.Empty;
                var isValid = IsValidWwsSet(itemsInStock, itemsInWws, out errorMessage);
                if(!isValid)
                {
                    throw new Exception(errorMessage);
                }
                SetPriceRecalculator.RecalculatePrices(itemsInStock, orderLine.Price);
                var lines = itemsInStock.Select(item =>
                {
                    return new OrderLine
                    {
                        ArticleNo = (int)item.ArticleNo,
                        Price = item.Price,
                        Quantity = item.Quantity * orderLine.Quantity,
                        HasShippedFromStock = orderLine.HasShippedFromStock,
                        StockNumber = orderLine.StockNumber,
                    };
                }).ToList();
                orderLine.SubLines = lines;
                return lines.Select(MapOrderLineSimple);
            }
            catch(Exception ex)
            {
                var errorMsg = String.Format("Не удалось создать резерв с набором. Ошибка: {0}", ex.ToMessageAndCompleteStacktrace());
                _logger.Error(errorMsg);
                throw new ArgumentException(errorMsg, ex);
            }
        }

        public IEnumerable<Interfaces.OrderLineInfo> FlatOrderLineWithSerial(Interfaces.OrderLineInfo orderLine)
        {
            var serialRequired = IsSerialRequired(orderLine.ArticleNo);
            if(serialRequired)
            {
                return Enumerable.Range(0, orderLine.Quantity).Select(i => orderLine.CopyWithQuantity(1));
            }
            return Enumerable.Repeat(orderLine, 1);
        }

        public static bool IsValidWwsSet(List<SetItem> setItemsFromStock, List<SetItem> setItemsFromWws, out String errorStr)
        {
            var result = true;
            if (setItemsFromStock.Count == 0 || setItemsFromWws.Count == 0)
            {
                errorStr = "Не найдена информация по элементам набора";
                return false;
            }
            errorStr = "Не найдена информация по элементу набора с артикулами: ";
            foreach(var stockItem in setItemsFromStock)
            {
                var wwsItem = setItemsFromWws.FirstOrDefault(x => x.ArticleNo == stockItem.ArticleNo && x.Price > 0 && ((x.UntilDate != null && x.UntilDate == setItemsFromWws.Max(i => i.UntilDate)) || x.UntilDate == null));
                if(wwsItem == null)
                {
                    result = false;
                    errorStr += stockItem.ArticleNo + ";";
                }
                else
                {
                    stockItem.Price = wwsItem.Price;
                }
            }
            return result;
        }

        /// <summary>
        /// Пересчитывает предоплату и вносит ее в цену одного из товаров
        /// </summary>
        /// <param name="order"></param>
        /// <param name="createRequest"></param>
        /// <param name="financingBankOid"></param>
        /// <param name="defaultDeliveryDownpayment"></param>
        private void ProcessCashlessOrder(OrderInfo order, Interfaces.CreateSalesDocumentRequest createRequest, string financingBankOid, decimal defaultDeliveryDownpayment)
        {
            createRequest.Bank.Oid = financingBankOid;

            if((order.ShippingMethod == ShippingMethod.Delivery) && (!order.Downpayment.HasValue || order.Downpayment.Value <= 0))
            {
                var downpayment = defaultDeliveryDownpayment;
                createRequest.Downpayment = downpayment;
                var orderLines = createRequest.Order.OrderLines.OfType<Interfaces.OrderLineInfo>();
                if(!orderLines.Any(ol => ol.Price > 0 && ol.Quantity > 0))
                {
                    return;
                }
                var orderLine = orderLines.FirstOrDefault(ol => ol.Quantity == 1);
                if(orderLine == null)
                {
                    orderLine = orderLines.First(ol => ol.Price > 0);
                    orderLine.Quantity--;
                    var orderLineCopy = orderLine.CopyWithQuantity(1);
                    createRequest.Order.OrderLines.Add(orderLineCopy);
                    orderLine = orderLineCopy;
                }
                orderLine.Price += downpayment;
            }
        }
    }
}