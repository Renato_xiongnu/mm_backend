﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Services.WWS.Mapping;

namespace Orders.Backend.Services.WWS.Helpers
{
    public class GetSalesOrderResultBuilder : ResultBuilder<GetSalesOrderResult>
    {
        public override GetSalesOrderResult BuildResult()
        {
            var result = base.BuildResult();
            if(OperationCode.Code == ReturnCode.Ok || OperationCode.Code == ReturnCode.OkPartially)
            {
                result.OrderHeader = GetOrderHeader(SalesDocumentInfo);
            }
            return result;
        }
    }
}