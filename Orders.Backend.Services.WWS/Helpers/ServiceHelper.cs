﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using WWS.Services;
using WWS.Services.DataAccess;
using WWS.Services.DataContracts;
using Orders.Backend.Services.WWS;
using Orders.Backend.Services.WWS.Mapping;
using Orders.Backend.Services.WWS.OrdersBackendService;

namespace Orders.Backend.Services.WWS.Helpers
{
    public static class ServiceHelper
    {
        /// <summary>
        ///  Проверяем, нужен ли серийный номер (для кких-то товаров обязателен)
        /// </summary>
        /// <param name="articleNo"></param>
        /// <returns></returns>
        public static bool IsSerialRequired(this StoreInfo info, long articleNo)
        {
            const string query = @"
                                select t3.SLIP_SN_REQUIRED_FLAG from artikel t1
                                inner join prod_typ_prop t3 on t1.prod_typ_oid=t3.prod_typ_oid
                                where  t1.art_no=?";

            var result = new WWSHelper(info).ExecuteScalar<short>(query, articleNo);
            return Convert.ToBoolean(result);
        }

        public static bool IsSerialRequired(this MMS.StoreService.Interfaces.DataContracts.StoreInfo storeInfo, long articleNo)
        {
            var info = storeInfo.MapTo<StoreInfo>();
            return IsSerialRequired(info, articleNo);
        }

        /// <summary>
        /// Получаем из wws данные по элементам набора
        /// </summary>
        /// <param name="storeInfo"></param>
        /// <param name="setArtocleNo"></param>
        /// <returns></returns>
        public static List<SetItem> GetSetItemsFromWws(this StoreInfo storeInfo, long articleNo)
        {
            var result = new List<SetItem>();
            var sqlQuery = String.Format(@"select
                                                PROD_SET_Price_Val as ProdSetPrice
                                                ,QTY
                                                ,coalesce(a1.ART_NO  ,a2.ART_NO) as ArticleNo
                                                ,ART_BEZ as ArticleTitle
                                                ,Until_Date as UntilDate 
                                            from MD_PROD_SET p
                                            inner join  PROD_SET_ELMNT e ON e.PARENT_OID = p.OID
                                            left join ARTIKEL a1 on a1.oid = e.SUBSID_PROD_OID
                                            left join TARTIKEL a2 on a2.oid = e.SUBSID_PROD_OID
                                            where ID = {0}", articleNo);
            try
            {
                var wws = new WWSHelper(storeInfo);
                var reader = wws.ExecuteReaderCommand(sqlQuery);

                while(reader.Read())
                {
                    var line = new SetItem
                    {
                        ArticleNo = (Int32)reader["ArticleNo"],
                        Price = (Decimal?)reader["ProdSetPrice"] ?? 0,
                        Quantity = (Int32?)reader["QTY"] ?? 0,
                        UntilDate = (DateTime?)reader["UntilDate"],
                    };
                    result.Add(line);
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return result;
        }

        public static List<SetItem> GetSetItemsFromWws(this MMS.StoreService.Interfaces.DataContracts.StoreInfo storeInfo, long articleNo)
        {
            var info = storeInfo.MapTo<StoreInfo>();
            return GetSetItemsFromWws(info, articleNo);
        }

        /// <summary>
        /// Получаем из сервиса стоков данные по элементам набора
        /// (артикульный состав и т.д.)
        /// </summary>
        /// <param name="setArticleNo">номера артикула набора</param>
        /// <param name="sapCode"></param>
        /// <param name="channel">по умолчаниею для ММ</param>
        /// <returns></returns>
        public static List<SetItem> GetSetItemsFromStock(Int32 setArticleNo, String sapCode, String channel = "MM")
        {
            var result = new List<SetItem>();
            GetArticleStockStatusResponse response = null;
            var service = new OrdersBackendServiceClient();
            try
            {
                var saleLocation = "shop_" + sapCode;
                var singleItemArray = new[] { setArticleNo.ToString(CultureInfo.InvariantCulture) };
                var request = new GetItemsRequest { Articles = singleItemArray, Channel = channel, SaleLocation = saleLocation };
                response = service.GetArticleStockStatus(request);
                service.Close();
            }
            catch(CommunicationException e)
            {
                service.Abort();
                throw;
            }
            catch(TimeoutException e)
            {
                service.Abort();
                throw;
            }
            catch(Exception e)
            {
                service.Abort();
                throw;
            }

            if(response == null || response.Items == null || response.Items.Any(x => x.SetItems == null || !x.SetItems.Any()))
                throw new Exception("Нет данных о элементах набора");

            var subItems = response.Items.FirstOrDefault().SetItems;

            var zeroQty = subItems.Where(x => x.QtyInSet <= 0).ToList();
            if(zeroQty.Any())
                throw new Exception(string.Format("Количество элементов в наборе не может быть меньше 1 для артикулов: {0}",
                    string.Join(",", zeroQty.Select(x => x.Article))));

            result.AddRange(subItems.Select(subItem => new SetItem { ArticleNo = subItem.Article, Quantity = subItem.QtyInSet }));

            return result;
        }

        public static string GetSalesDocBarcode(this int docNo)
        {
            string strConst = "0002999";

            byte[] str1 = Encoding.ASCII.GetBytes(strConst);
            byte[] str2 = Encoding.ASCII.GetBytes(docNo.ToString());
            long lCheck = getCheckNumber(str1, str2);

            // Barcode
            return (strConst + (char)lCheck + docNo.ToString());
        }

        private static char getCheckNumber(byte[] constCharBytes, byte[] noCharBytes)
        {
            long lCheck = 0L;

            for(int i = 0; i < constCharBytes.Length; i++)
            {
                lCheck += (constCharBytes[i] - (byte)'0') * (i + 1);
            }
            for(int i = 0; i < noCharBytes.Length; i++)
            {
                lCheck += (noCharBytes[i] - (byte)'0') * (i + 1);
            }
            lCheck = ((lCheck % 9) + 1) + '0';
            return (char)lCheck;
        }
    }
}