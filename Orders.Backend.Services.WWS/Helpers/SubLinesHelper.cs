﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orders.Backend.Services.WWS
{

    public sealed class SubLineHelper
    {
        private readonly HashSet<int> _skippedLines;
        private ICollection<OrderLine> _resultOrdeLines;

        private readonly OrderLine[] _sets;
        private readonly OrderLine[] _arts;
        private readonly OrderLine[] _warranties;       

        public static SubLineHelper CreateHelper(ICollection<OrderLine> orderLines)
        {
            return new SubLineHelper(orderLines.Where(t => t.ProductType == ProductType.Set),
                                                  orderLines.Where(t => t.ProductType == ProductType.Article),
                                                  orderLines.Where(t => t.ProductType == ProductType.WarrantyPlus));
        }

        public ReservePrice CalcReservePrice()
        {
            if (_resultOrdeLines == null)
                throw new InvalidOperationException();

            var grossPrice = _resultOrdeLines.Sum(t => t.TotalPrice.GrossPrice);
            var netPrice = _resultOrdeLines.Sum(t => t.TotalPrice.NetPrice);
            var vatPrice = _resultOrdeLines.Sum(t => t.TotalPrice.VatPrice);

            var line = _resultOrdeLines.FirstOrDefault(t => t.TotalPrice.VAT > 0);
            var vat = line != null ? line.TotalPrice.VAT : 0;

            return new ReservePrice
            {
                GrossPrice = grossPrice,
                NetPrice = netPrice,
                VatPrice = vatPrice,
                VAT = vat
            };
        }
        
        public ICollection<OrderLine> CorrelateLines()
        {
            _resultOrdeLines = new List<OrderLine>();
            foreach (var set in _sets)
            {
                _resultOrdeLines.Add(set);

                foreach (var art in _arts)
                {
                    if (_skippedLines.Contains(art.LineNumber))
                        continue;
                    if (set.PositionNumber == art.PositionNumber)
                    {
                        if (set.SubLines == null)
                            set.SubLines = new List<OrderLine>();

                        art.RefLineNumber = set.LineNumber;
                        set.SubLines.Add(art);
                        _skippedLines.Add(art.LineNumber);
                        UpdateWarranty(art, set.PositionNumber);
                    }
                }
            }

            foreach (var art in _arts)
            {
                if (_skippedLines.Contains(art.LineNumber))
                    continue;
                _resultOrdeLines.Add(art);
                UpdateWarranty(art, art.PositionNumber);
            }

            return _resultOrdeLines;
        }


        private SubLineHelper(IEnumerable<OrderLine> sets, IEnumerable<OrderLine> arts, IEnumerable<OrderLine> warranties)
        {
            _resultOrdeLines = null;
            _skippedLines = new HashSet<int>();
            _sets = sets.ToArray();
            _arts = arts.ToArray();
            _warranties = warranties.ToArray();
        }

        private void UpdateWarranty(OrderLine mainLine, int artPostionNumber)
        {
            foreach (var warranty in _warranties)
            {
                if (_skippedLines.Contains(warranty.LineNumber))
                    continue;

                if (warranty.WarrantyInsurance.Number == mainLine.WarrantyInsurance.Number)
                {
                    mainLine.WarrantyInsurance.RelatedArticleNo = warranty.ArticleNo;

                    warranty.RefLineNumber = mainLine.LineNumber;
                    warranty.WarrantyInsurance.RelatedArticleNo = mainLine.ArticleNo;
                    warranty.WarrantyInsurance.DocumentPositionNumber = artPostionNumber;
                    warranty.WarrantyInsurance.WarrantySum = mainLine.TotalPrice.GrossPrice;
                    _resultOrdeLines.Add(warranty);

                    _skippedLines.Add(warranty.LineNumber);
                }
            }
        }
    }
}