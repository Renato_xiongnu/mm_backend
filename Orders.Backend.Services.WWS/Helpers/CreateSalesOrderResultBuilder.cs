﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.ServiceModel;
using Interfaces = MMS.Integration.Gms.WebServices.Interfaces;
using Orders.Backend.Services.WWS.Mapping;
using WwsTunnel.Common.Helpers;

namespace Orders.Backend.Services.WWS.Helpers
{
    public class CreateSalesOrderResultBuilder : ResultBuilder<CreateSalesOrderResult>
    {
        protected Interfaces.CreateSalesDocumentResponse Response { get; set; }
        
        public void SetCreateSalesDocumentResponse(Interfaces.CreateSalesDocumentResponse response)
        {
            Response = response;
        }

        public override CreateSalesOrderResult BuildResult()
        {
            var result = base.BuildResult();
            if(OperationCode.Code == ReturnCode.Ok || OperationCode.Code == ReturnCode.OkPartially)
            {
                result.CreateTime = SalesDocumentInfo.SalesDocDate;
                result.OrderHeader = GetOrderHeader(SalesDocumentInfo, Response);
                result.OrderHeader.DeliveryCustomer = result.OrderHeader.DeliveryCustomer.MapFromOrderCustomerInfo(Order.Customer);
                CreateHierarchy(Order, result.OrderHeader);
            }
            return result;
        }
    }
}