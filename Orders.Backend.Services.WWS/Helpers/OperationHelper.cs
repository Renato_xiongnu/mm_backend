﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Orders.Backend.Services.WWS
{
    public static class SubCodes
    {
        //ValidationException
        public static string ValidationError { get { return "10"; } }

        //Exception
        public static string SystemException { get { return "10"; } }

        //OkPartially
        public static string LogicError { get { return "10"; } }

        //JBOSSException
        public static string JBOSSMissingLines { get { return "10"; } }
        public static string JBOSSNotFound { get { return "20"; } }
        public static string JBOSSSqlError { get { return "100"; } }
        public static string JBOSSUnknow { get { return "-1"; } }
    }



    public static class MessageTexts
    {
        public static string SystemException { get { return ConfigurationManager.AppSettings["SystemExceptionText"]; } }
        public static string LogicError { get { return ConfigurationManager.AppSettings["LogicErrorText"]; } }

        public static string ValidationError { get { return ConfigurationManager.AppSettings["ValidationErrorText"]; } }

        public static string JBOSSMissingLines { get { return ConfigurationManager.AppSettings["JBOSSMissingLinesText"]; } }
        public static string JBOSSSqlError { get { return ConfigurationManager.AppSettings["JBOSSSqlErrorText"]; } }
        public static string JBOSSSalesDocNotFound { get { return ConfigurationManager.AppSettings["JBOSSSalesDocNotFoundText"]; } }

        public static string JBOSSUnknow { get { return ConfigurationManager.AppSettings["JBOSSUnknowText"]; } }
    }
}