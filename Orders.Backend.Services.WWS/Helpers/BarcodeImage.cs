﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using iTextSharp.text.pdf;

namespace Orders.Backend.Services.WWS.Helpers
{
    public static class Settings
    {
        public static float BarcodeHeigth { get { return 40; } }

        public static float BarcodeN { get { return 2; } }

        public static float BarCodeX { get { return 2; } }
    }

    public class BarcodeInter25 : iTextSharp.text.pdf.BarcodeInter25
    {
        public static byte[] GetImage(string barCode, bool printText = true, string barCodeText = "")
        {
            using (var image = BarcodeInter25.CreateImage(barCode, printText, barCodeText))
            using (var stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public static Image CreateImage(string barcode, bool printText = true, string altText = "", bool withCheckSum = false)
        {
            var bar = new BarcodeInter25
            {
                GuardBars = true,
                //StartStopText = true,
                ChecksumText = false,
                AltText = altText,
                Code = barcode,
                GenerateChecksum = withCheckSum,
                BarHeight = Settings.BarcodeHeigth,
                N = Settings.BarcodeN,
                X = Settings.BarCodeX,
            };

            return bar.CreateDrawingImage(Color.Black, Color.White, printText);
        }

        private Bitmap CreateBmp(Color foreground, Color background, out int fullWidth, out int height)
        {
            String bCode = KeepNumbers(code);
            if (generateChecksum)
                bCode += GetChecksum(bCode);
            int len = bCode.Length;
            int nn = (int)n;
            int xx = (int)x;
            fullWidth = (len * (3 + 2 * nn) + (6 + nn)) * xx;
            byte[] bars = GetBarsInter25(bCode);
            height = (int)barHeight;
            var bmp = new System.Drawing.Bitmap(fullWidth, height);
            for (int h = 0; h < height; ++h)
            {
                bool print = true;
                int ptr = 0;
                for (int k = 0; k < bars.Length; ++k)
                {
                    int w = (bars[k] == 0 ? xx : xx * nn);
                    var color = background;
                    if (print)
                        color = foreground;
                    print = !print;
                    for (int j = 0; j < w; ++j)
                        bmp.SetPixel(ptr++, h, color);
                }
            }

            return bmp;
        }

        private Bitmap AddText(Bitmap bmp, int fullWidth, int height)
        {
            var graphics = Graphics.FromImage(bmp);

            var codeValue = string.IsNullOrEmpty(altText) ? code : altText;

            var textCode = generateChecksum ? string.Format("{0}   {1}", codeValue, GetChecksum(codeValue)) : string.Format("{0}  {1}", codeValue.Substring(0, codeValue.Length - 1), codeValue.Last());
            var fontCode = new Font("Futura Bk BT", 8);
            var measureCode = graphics.MeasureString(textCode, fontCode);

            var textRectX = (fullWidth - measureCode.Width) / 2;
            var textRectY = height - measureCode.Height;

            graphics.FillRectangle(new SolidBrush(Color.White), textRectX - 5, textRectY - 1, measureCode.Width + 10, measureCode.Height + 2);

            graphics.DrawString(textCode, fontCode, new SolidBrush(Color.Black), textRectX, textRectY + 2);

            return bmp;
        }

        /// <summary>
        /// This method is overrided because of errors in the base class
        /// </summary>
        /// <param name="foreground"></param>
        /// <param name="background"></param>
        /// <returns></returns>
        public override Image CreateDrawingImage(Color foreground, Color background)
        {
            int fullWidth;
            int height;
            var bmp = CreateBmp(foreground, background, out fullWidth, out height);

            return AddText(bmp, fullWidth, height);
        }

        /// <summary>
        /// This method is overloaded to add posibility for choosing of printing altText
        /// </summary>
        /// <param name="foreground"></param>
        /// <param name="background"></param>
        /// <returns></returns>
        public Image CreateDrawingImage(Color foreground, Color background, bool printText)
        {
            int fullWidth;
            int height;
            var bmp = CreateBmp(foreground, background, out fullWidth, out height);
            return printText ? AddText(bmp, fullWidth, height) : bmp;
        }

        public long GetCheckSum(long result)
        {
            return long.Parse(GetChecksum(result.ToString()).ToString());
        }
    }

    public class Barcode39 : iTextSharp.text.pdf.Barcode39
    {
        public static byte[] GetImage(string barCode, string barCodeText)
        {
            using (var image = Barcode39.CreateImage(barCode, barCodeText))
            using (var stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public static Image CreateImage(string barcode, string altText = "", bool withCheckSum = false)
        {
            var bar = new Barcode39
            {
                GuardBars = true,
                //StartStopText = true,
                ChecksumText = false,
                AltText = altText,
                Code = barcode,
                GenerateChecksum = withCheckSum,
                BarHeight = Settings.BarcodeHeigth,
                N = Settings.BarcodeN,
                X = Settings.BarCodeX,
            };

            return bar.CreateDrawingImage(Color.Black, Color.White);
        }
    }
}