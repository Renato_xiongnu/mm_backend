﻿using System;
using System.ServiceModel;

namespace Orders.Backend.Services.WWS
{
    [ServiceContract]
    public interface ISalesService
    {
        [OperationContract]
        CreateSalesOrderResult CreateSalesOrder(OrderInfo order);

        [OperationContract]
        CancelSalesOrderResult CancelSalesOrder(OrderNumberInfo orderNumberInfo);

        [OperationContract]
        GetSalesOrderResult GetSalesOrder(OrderNumberInfo orderNumberInfo);

        [OperationContract]
        ChangeDeliveryDateResult ChangeDeliveryDate(OrderNumberInfo orderNumberInfo, DateTime deliveryDate);
    }
}
