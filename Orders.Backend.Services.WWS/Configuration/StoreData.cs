﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Configuration;

namespace Orders.Backend.Services.WWS.Configuration
{
    [Serializable]
    public class StoreData
    {
        [XmlAttribute]
        public string SapCode { get; set; }


        [XmlElement]
        public string OnlineBankName { get; set; }

        public static ICollection<StoreData> GetStores()
        {
            return (List<StoreData>)ConfigurationManager.GetSection("Stores");
        }
    }
}