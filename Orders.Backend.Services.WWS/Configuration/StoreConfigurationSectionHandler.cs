﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Xml.Serialization;
using System.Xml;

namespace Orders.Backend.Services.WWS.Configuration
{
    public class StoreConfigurationSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlSerializer xs = new XmlSerializer(typeof(StoreData));

            List<StoreData> result = new List<StoreData>();

            foreach (XmlNode node in section.ChildNodes)
            {
                using (var xr = new XmlNodeReader(node))
                {
                    var store = (StoreData)xs.Deserialize(xr);
                    if (store != null)
                        result.Add(store);
                }

            }

            return result;
        }
    }
}