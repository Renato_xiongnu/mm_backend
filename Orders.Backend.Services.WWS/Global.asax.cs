﻿using System;
using Autofac;
using Autofac.Configuration;
using Autofac.Integration.Wcf;
using Common.Logging;
using Orders.Backend.Services.WWS.Mapping;

namespace Orders.Backend.Services.WWS
{
    public class Global : System.Web.HttpApplication
    {
        private ILog _logger;

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ConfigurationSettingsReader("autofac"));
            AutofacHostFactory.Container = builder.Build();

            _logger = LogManager.GetLogger("Orders.Backend.Services.WWS");
            _logger.Info("Application started");       
            MappingExtensions.Validate();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var unhandledException = Server.GetLastError();
            _logger.Error("Application_Error", unhandledException);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            _logger.Info("Application ended"); 
        }
    }
}