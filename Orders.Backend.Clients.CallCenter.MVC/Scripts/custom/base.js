﻿function generateLoader(container, optWidth, optHeight) {
    var height = optHeight === undefined || optHeight === null ? $(container).height() : optHeight;
    var width = optWidth === undefined || optWidth === null ? $(container).width() : optWidth;
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="' + approot + '/Content/Images/ajax-loader.gif"></div>';
}

$(function () {
    var $typeSelect = $('select[id$="PickupType"]');
    var $preservedPlace = $('[id$="SelectedPlaceCode"]');
    var $placeSelect = $('select[id$="DeliveryPlace"]');
    if ($preservedPlace.val() != $placeSelect.val()) {
        $placeSelect.prop("selectedIndex", -1)
    }
    $typeSelect.on('change', function () {
        $('.datepicker').datepicker('option', 'minDate', getMinDeliveryDate());
        $('[id$="DeliveryTypeName"]').val($(this).find(":selected").text());
        if ($(this).val() === "2") {
            $('.deliveryPlace').show();
            $('.storePlace').hide();
        }
        else if ($(this).val() === "1") {
            $('.deliveryPlace').hide();            
            $('.storePlace').show();
        }
        else {
            $('.deliveryPlace').hide();
            $('.storePlace').hide();
        }
    });
    
    $placeSelect.on('change', function () {
        $('.datepicker').datepicker('option', 'minDate', getMinDeliveryDate());
        $('[id$="DeliveryPlaceName"]').val($(this).find(":selected").text());
    });

    $placeSelect.trigger('change');
    $typeSelect.trigger('change');
    if ($('input[id$="DeliveryZone"]:checked').val() == "-1") {
        $('.unknownPrice').show();
    }
    $('[id$="DeliveryZone"]').on('click', function () {
        if ($(this).val() == "-1") {
            $('.unknownPrice').show();
        }
        else {
            $('.unknownPrice').hide();
        }
    });

    $(".expandableIcon").on('click', function () {
        var container = $(this).parent();
        if (container.hasClass("show")) {
            $(this).removeClass("ui-icon-triangle-1-n").addClass("ui-icon-triangle-1-s");
            container.removeClass("show");
        }
        else {
            $(this).removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-n");
            container.addClass("show");
        }
    });

    //$('[id$="DeliveryZone"]').trigger('click');

    $(".autocompleteCity").autocomplete({
        source: $.routeLinks.AucCity,
        select: function (event, ui) {
            $('[id$="CityId"]').val(ui.item.code);
        },
        search: function (event, ui) {
            $('[id$="CityId"]').val('');
            //$(this).attr('disabled', 'disabled');
            var $loader = $(this).parent().find('.loader');
            $loader.html(generateLoader($loader, 20, 20));
        },
        response: function (event, ui) {
           // $(this).removeAttr('disabled', 'disabled');
            var $loader = $(this).parent().find('.loader');
            $loader.html('');
            if (ui.content.length == 1 && $(this).val().toLowerCase() == ui.content[0].value.toLowerCase())
            {
                $('[id$="CityId"]').val(ui.content[0].code);
                $(this).val(ui.content[0].value);
                $(this).autocomplete("close");
            }
        }
    });

    $('.datepicker').inputmask("d.m.y").datepicker({
        minDate: getMinDeliveryDate(),
        dateFormat: 'dd.mm.yy',
        buttonImage: approot + '/Content/Images/calendar.gif',
        buttonImageOnly: true,
        showOn: 'both'
    });
    $('input[type="checkbox"][id$="InvoiceNotSame"]').on('click', function () {
        if ($(this).is(':checked')) {
            $('.invoiceCustomerBlock').show();
        }
        else {
            $('.invoiceCustomerBlock').hide();
        }
    });
    $('.submitButton').on('click', function () {
        var href = $(this).attr('data-href');
        if (href !== null && href !== undefined) {
            $(this).closest('form').attr('action', href).submit();
        }
    });
    var additionalMasks = {
        phone: /[\d\.\, \-\+\(\)]/
    };
    $.extend($.fn.keyfilter.defaults.masks, additionalMasks);
    $('.keyfilter-phone').keyfilter($.fn.keyfilter.defaults.masks.phone);
});

$(document).ready(function() {
    var pickupDropDown = $('#PickupInfo_PickupType');
    pickupDropDown.find('option:selected').prop('selected', true);
});

function getMinDeliveryDate() {
    var correctDate = new Date();
    var typeId = $('select[id$="DeliveryType"]').val();
    var placeId = $('select[id$="DeliveryPlace"]').val();
    if ((typeId === "20" && placeId !== null && placeId.indexOf("R930") == 0) || typeId === "2") {
        var hours = correctDate.getHours();
        var minutes = correctDate.getMinutes();
        if (hours > 18 || (hours === 18 && minutes > 30))
            correctDate.setDate(correctDate.getDate() + 2);
        else
            correctDate.setDate(correctDate.getDate() + 1);
    }
    else if (typeId === "2") {

    }
    return correctDate;
}