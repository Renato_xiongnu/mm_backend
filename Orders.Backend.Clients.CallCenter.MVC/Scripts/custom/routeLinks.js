﻿jQuery.routeLinks || (function ($, window, undefined) {
    $.routeLinks = {
        CheckQuantity: approot + '/Home/CheckQuantity',
        GetSummary: approot + '/Home/GetSummary',
        GetStations: approot + '/Coach/GetMetroStations',
        AucCity: approot + '/Coach/GetCities',
        GetBoNumber: approot + '/Home/GetBoNumber'
    };
})(jQuery, this);