﻿function makeDialog(container, options) {
    var baseOptions = {
        autoOpen: false,
        width: 450,
        title: "Default",
        modal: true,
        close: function () {
        }
    };
    var buttons = {
        "OK": function () {
            $(this).dialog("close");
        }
    }
    $.extend(baseOptions, options);
    $(container).dialog(baseOptions);
}
