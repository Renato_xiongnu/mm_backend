﻿/// <reference path="keypress-2.0.1.min.js"/>
/// <reference path="base.js"/>
/// <reference path="jquery-1.9.1.min.js"/>

window.Dadata = window.Dadata || {};
window.Dadata.Application = function (options) {
    var that = this, i, jqi, j, e;
    that.Options = $.extend({
        InputSelectors: [],
        CallbackUrl: '',
        TooltipTemplate: $('#dadata-template').text(),
        OutputSelectors: [],
    }, options);
    that._inputs = [];
    that._keyPressListeners = [];
    for (i = 0; i < that.Options.InputSelectors.length; i++) {
        jqi = $(that.Options.InputSelectors[i]);
        if (!jqi.length)
            continue;
        for (j = 0; j < jqi.length; j++) {
            e = jqi[j];
            that._inputs.push({
                element: e
            });
        }
    }
    for (i = 0; i < that._inputs.length; i++) {
        jqi = that._inputs[i];
        var listener = new window.keypress.Listener();
        listener.register_many([{
                keys: "tab",
                is_unordered: true,
                prevent_default: true,
                on_release: function() {
                    that.delayedCheck(jqi);
                },
            }]);
        listener.stop_listening();

        var el = $(jqi.element);
        var p = el.position();
        var tooltip = $(this.Options.TooltipTemplate);
        tooltip.position({
            top: p.top + el.height(),
            left: p.left + el.width() / 2,
        });
        tooltip.insertAfter(el);
        jqi.tt = tooltip;
        tooltip.hide();

        $(jqi.element).bind("blur", function() {
            listener.stop_listening();
            that.toggleTooltip(jqi, false);
        }).bind("focus", function() {
            listener.listen();
            that.toggleTooltip(jqi, true);
        });
        that._keyPressListeners.push(listener);
    }
};

window.Dadata.Application.prototype = {
    delayedCheck: function(input) {
        var v = $(input.element).val() || ''
            , that = this
            , h = v.toString().hashCode();
        v = v.trim();
        if(v && v.length)
        $.ajax({
            url: that.Options.CallbackUrl,
            type: 'POST',
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify({ "Address": v, "Hash": h }),
            success: function (data) {
                if (data.Hash !== h)
                    return;
                input.data = data.AddressData;
                if (data.AddressData) {
                    that.displayTooltip(input);
                    that.applyValues(input);
                }
            }
        });
    },
    displayTooltip: function (element) {
        if (!element || !element.data || !element.tt || !element.tt.length)
            return;
        var v, e, that = this;
        e = element.tt.children('.param-list').find('.city').find('p');
        v = element.data.settlement || element.data.city || element.data.region;
        if (e.length && v && v.length) {
            e.html(v);
            e.closest('li').show();
        }
        else e.hide();

        e = element.tt.children('.param-list').find('.street').find('p');
        v = element.data.street;
        if (e.length && v && v.length) {
            e.html(v);
            e.closest('li').show();
        } else e.closest('li').hide();

        v = element.data.house_type ? element.data.house_type + ' ' : '';
        e = element.tt.children('.param-list').find('.house').find('p');
        if (e.length && v && v.length) {
            e.html((v) + element.data.house);
            e.closest('li').show();
        } else e.closest('li').hide();

        e = element.tt.children('.param-list').find('.building').find('p');
        v = (element.data.block_type && element.data.block_type.length ? element.data.block_type + ' ' : '') + element.data.block;
        if (e.length && v && v.length) {
            e.html(v);
            e.closest('li').show();
        } else e.closest('li').hide();

        e = element.tt.children('.param-list').find('.flat').find('p');
        v = (element.data.flat_type && element.data.flat_type.length ? element.data.flat_type + ' ' : '') + element.data.flat;
        if (e.length && v && v.length) {
            e.html(v);
            e.closest('li').show();
        } else e.closest('li').hide();

        element.tt.children('.dadata-template-check').click(function() {
            that.applyValues(element);
        });

        this.toggleTooltip(element, true);
    },
    applyValues: function (element) {
        if (!element || !element.data || !element.tt || !element.tt.length)
            return;

        var e, f, i, c;
        for (i = 0; i < this.Options.OutputSelectors.length; i++) {
            e = this.Options.OutputSelectors[i];
            switch (e.field) {
                case 'city':
                    c = element.data.settlement || element.data.city || element.data.region || '';
                    c = c.substring(c.indexOf(' ') + 1);
                    if(c && c.length)
                        $(e.selector).find('option').each(function(x, y) {
                            if ($(y).html() === c) {
                                $(y).attr('selected', true);
                                $(y).trigger('change');
                            }
                        });
                    break;
                case 'street':
                    $(e.selector).val(element.data.street || '');
                    break;
                case 'house':
                    $(e.selector).val((element.data.house_type ? element.data.house_type + ' ' : '') + element.data.house);
                    break;
                case 'building':
                    $(e.selector).val((element.data.block_type && element.data.block_type.length ? element.data.block_type + ' ' : '') + element.data.block || '');
                    break;
                case 'flat':
                    $(e.selector).val((element.data.flat_type && element.data.flat_type.length ? element.data.flat_type + ' ' : '') + element.data.flat || '');
                    break;
            }
        }
    },
    generateKeys: function () {
        var result = [];
        var i = 'А'.charCodeAt(0),
            j = 'я'.charCodeAt(0);

        for (; i <= j; i++) {
            result.push(String.fromCharCode(i));
        }
        return result;
    },
    toggleTooltip: function (element, show) {
        var f = show ? $.prototype.show : $.prototype.hide;
        f.call(element.tt);
    }
};

$.extend(window.Dadata, {
    AjaxApi: {
        applyValues: function(target, dadata) {
            var e, i, c, d = dadata.data;
            if (!d)
                return;
            for (i = 0; i < target.length; i++) {
                e = target[i];
                switch (e.field) {
                case 'city':
                    c = d.settlement || d.city || d.region || '';
                    c = c.substring(c.indexOf(' ') + 1);
                    if (c && c.length)
                        $(e.selector).find('option').each(function(x, y) {
                            if ($(y).html() === c) {
                                $(y).attr('selected', true);
                                $(y).trigger('change');
                            }
                        });
                    break;
                case 'street':
                    $(e.selector).val(d.street || '');
                    break;
                case 'house':
                    $(e.selector).val((d.house_type ? d.house_type + ' ' : '') + (d.house || ''));
                    break;
                case 'building':
                    $(e.selector).val((d.block_type && d.block_type.length ? d.block_type + ' ' : '') + (d.block || ''));
                    break;
                case 'flat':
                    $(e.selector).val((d.flat_type && d.flat_type.length ? d.flat_type + ' ' : '') + (d.flat || ''));
                    break;
                }
            }
        }
    }
});