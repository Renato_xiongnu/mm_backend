﻿using System.Web.Mvc;
using Orders.Backend.Clients.CallCenter.MVC.Common;

namespace Orders.Backend.Clients.CallCenter.MVC.Controllers
{
    public class TransferController : Controller
    {
        //
        // GET: /Transfer/

        public ActionResult CreateTransfer(string orderId)
        {
            var transferOrder = OrderProcessing.GetTransferModel(orderId);
            return View(transferOrder);
        }
    }
}
