﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Orders.Backend.Clients.CallCenter.MVC.Common;
using Orders.Backend.Clients.CallCenter.MVC.Models.CallCenter;
using Orders.Backend.Clients.CallCenter.MVC.OrderLink;

namespace Orders.Backend.Clients.CallCenter.MVC.Controllers
{
    public class CallCenterController : Controller
    {
        //
        // GET: /CallCenter/        
        public ActionResult Index(string orderId, string canEdit, string outcome="")
        {
            var ord = OrderProcessing.GetOrder(orderId);
            var callCenterModel = new CallCenterModel
            {
                SessionId = Guid.NewGuid().ToString(),
                SaleLocationId = ord.SaleLocationId.Trim(),
                BasketId = ord.BasketId,
                //ord.BasketId
                CanEditBlocks =
                    string.IsNullOrEmpty(canEdit)
                        ? new string[0]
                        : canEdit.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries),
                OrderOptions = new OrderOption
                {
                    LineOptions = ord.AllArticles
                        .Select(a => new OrdelLine
                        {
                            Article = long.Parse(a.ArticleNum),
                            Comment = a.Comment,
                            ReviewState = a.StateName
                        })
                        .GroupBy(a => a.Article)
                        .ToDictionary(g => g.Key, g => g.First()),
                    BasketComment = ord.Comment ?? string.Empty
                },
                ValidateIfOutcome = outcome
            };

            TempData["OrderId"] = ord.OrderId;

            return View(callCenterModel);
        }

        //
        // GET: /CallCenter/        
        public ActionResult IndexNew(string orderId, string canEdit, string outcome = "")
        {
            var ord = OrderProcessing.GetOrder(orderId);
            var callCenterModel = new CallCenterModel
            {
                SessionId = Guid.NewGuid().ToString(),
                SaleLocationId = ord.SaleLocationId.Trim(),
                BasketId = ord.BasketId,
                //ord.BasketId
                CanEditBlocks =
                    string.IsNullOrEmpty(canEdit)
                        ? new string[0]
                        : canEdit.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries),
                OrderOptions = new OrderOption
                {
                    LineOptions = ord.AllArticles
                        .Select(a => new OrdelLine
                        {
                            Article = long.Parse(a.ArticleNum),
                            Comment = a.Comment,
                            ReviewState = a.StateName
                        })
                        .GroupBy(a => a.Article)
                        .ToDictionary(g => g.Key, g => g.First()),
                    BasketComment = ord.Comment ?? string.Empty
                },
                ValidateIfOutcome = outcome
            };

            TempData["OrderId"] = ord.OrderId;

            return View(callCenterModel);
        }

        [HttpPost]
        [AllowCrossOrigin]
        public JsonResult UpdateOrderOptions(UpdateBasketCommentsModel model)
        {
            try
            {
                var orderId = model.orderId ?? GetAndRestoreOrderId();

                var order = OrderProcessing.GetOrder(orderId);
                order.NewComment = model.orderOptions.newBasketComment;
                if (model.orderOptions.lineOptions != null)
                {
                    order.BasketArticles.ForEach(el =>
                    {
                        var updatedArticle =
                            model.orderOptions.lineOptions.FirstOrDefault(t => t.article == el.ArticleNum);
                        if (updatedArticle != null)
                        {
                            el.NewComment = updatedArticle.newComment;
                        }
                    });
                }

                CommentHelper.UpdateComment(order);
                if (model.orderOptions.lineOptions != null)
                {
                    CommentHelper.UpdateComments(order.BasketArticles);
                }

                var updateOrderResult = OrderProcessing.UpdateOrder(order);

                if (updateOrderResult.ReturnCode == ReturnCode.Error)
                {
                    return Json(new {Result = false, Message = updateOrderResult.ErrorMessage});
                }

                order = OrderProcessing.GetOrder(orderId); //Надо бы это в updateOrder возвращать

                return Json(new
                {
                    Result = true,
                    LineOptions = order.AllArticles
                        .Select(a => new OrdelLine
                        {
                            Article = long.Parse(a.ArticleNum),
                            Comment = a.Comment,
                            ReviewState = a.StateName
                        })
                        .GroupBy(a => a.Article)
                        .ToDictionary(g => g.Key.ToString(), g => g.First().Comment),
                    BasketComment = order.Comment ?? string.Empty

                });

            }
            catch (Exception e)
            {
                return Json(new {Result = false, Message = e.Message});
            }
        }

        [HttpGet]
        [AllowCrossOrigin]
        public JsonResult GetAllCityStores(string orderId)
        {
            try
            {
                var orderItem = OrderProcessing.GetOrder(orderId);
                return Json(new { Result = true, Stores = orderItem.PickupInfo.AllCityStores }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex.Message });
            }
        }

        [HttpGet]
        public JsonResult RefreshBasketId()
        {
            try
            {
                var orderId = GetAndRestoreOrderId();

                var order = OrderProcessing.GetOrder(orderId);

                return Json(order.BasketId);

            }
            catch (Exception e)
            {
                return Json(new { Result = false, Message = e.Message });
            }
        }

        private string GetAndRestoreOrderId()
        {
            var orderId = (string)TempData["OrderId"];
            TempData["OrderId"] = orderId;

            return orderId;
        }
    }

    public class LineOption
    {
        public string article { get; set; }
        public string newComment { get; set; }
    }

    public class OrderOptions
    {
        public List<LineOption> lineOptions { get; set; }
        public string newBasketComment { get; set; }
    }

    public class UpdateBasketCommentsModel
    {
        public string orderId { get; set; }
        public OrderOptions orderOptions { get; set; }
    }
}
