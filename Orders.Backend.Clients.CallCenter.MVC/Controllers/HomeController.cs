﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Orders.Backend.Clients.CallCenter.MVC.Common;
using Orders.Backend.Clients.CallCenter.MVC.Models;
using ReturnCode = Orders.Backend.Clients.CallCenter.MVC.OrderLink.ReturnCode;

namespace Orders.Backend.Clients.CallCenter.MVC.Controllers
{
    public class HomeController : Controller
    {
        [NonAction]
        public OrderItem PrepareOrder(OrderItem order)
        {
            ModelState.Clear();
            order.ErrorMessage = "";
            order.IsApproved = false;
            order.PickupInfo.OriginalSapCode = order.PickupInfo.SapCode;
            order.PickupInfo.OriginalName = order.PickupInfo.Name;
            order.PickupInfo.NewSapCode = order.PickupInfo.SapCode;
            OrderProcessing.RefreshProductInfo(order);
            order = OrderProcessing.ValidateOrder(order);
            if (order.IsSmart)
            {
                order = PrepareSmartOrder(order);
            }  
            return order;
        }

        [NonAction]
        private OrderItem PrepareSmartOrder(OrderItem order)
        {            
            using(var client=new Proxy.Sls.SaleLocationServiceClient())
            {
                var firstOutcome = client.GetSaleLocationOutcomes(order.OrderId).Result
                    .OrderBy(r => r.Created).FirstOrDefault();
                if(firstOutcome!=null)
                {
                    order.PickupInfo.OriginalSapCode = firstOutcome.SapCode;
                    order.PickupInfo.OriginalName = order.PickupInfo.AllCityStores.First(s => s.SapCode == firstOutcome.SapCode).Name;
                }
                if (order.ReserveInfo != null 
                    && !string.IsNullOrEmpty(order.ReserveInfo.Store.SapCode) 
                    && !string.IsNullOrEmpty(order.ReserveInfo.WWSOrderId)
                    && order.ReserveInfo.AllReserved)
                {
                    order.PickupInfo.ReserveSapCode = order.ReserveInfo == null 
                        ? null 
                        : order.ReserveInfo.Store.SapCode;
                    order.PickupInfo.ReserveName =
                        order.PickupInfo.AllCityStores.First(s => s.SapCode == order.PickupInfo.ReserveSapCode).Name;                    
                    
                }                
            }
            if(!order.ReadOnly)
            {
                order.PickupInfo.NeedTransfer = OrderProcessing.NeedTransfer(order);   
            }            
            return order;
        }

        [HttpGet]
        public ActionResult Order(string orderId, string ro, string fi, string smart, string showArticleStockStatus, string canChangeBasket, string canChangeShipmentType, string outcome="")
        {
            var orderReadonly = !String.IsNullOrEmpty(ro) && ro == "1";
            var orderFixedMethod = !String.IsNullOrEmpty(fi) && fi == "1";
            var itIsSmartOrder = !String.IsNullOrEmpty(smart) && smart == "1";
            var canChangeBasketVal = String.IsNullOrEmpty(canChangeBasket) || canChangeBasket == "1";
            var canChangeShipmentTypeVal = String.IsNullOrEmpty(canChangeShipmentType) || canChangeShipmentType == "1";            
            var ord = OrderProcessing.GetOrder(orderId);
            ord.ReadOnly = orderReadonly;
            ord.IsFixed = orderFixedMethod;
            ord.IsSmart = itIsSmartOrder;
            ord.CanChangeBasket = canChangeBasketVal;
            ord.CanChangeShipmentType = canChangeShipmentTypeVal;
            ord.ShowArticleStockStatus = !String.IsNullOrEmpty(showArticleStockStatus) && showArticleStockStatus == "1";
            ord.PickupInfo.ShowArticleStockStatus = !String.IsNullOrEmpty(showArticleStockStatus) && showArticleStockStatus == "1";
            ord.ValidateIfOutcome = outcome;
            ord = PrepareOrder(ord);
                      
                    
            if(ord.ReadOnly)
            {
                return View("Display", ord);
            }
            return View("Index", ord);
        }        

        // from php callcenter
        public ActionResult BaseOrder(string data)
        {
            var order = new OrderItem();
            try
            {
                order = JsonConvert.DeserializeObject<OrderItem>(data);
                foreach(var art in order.BasketArticles)
                {
                    art.Type = ArticleType.Product;
                }
                order = PrepareOrder(order);
                order.ExternalInterface = true;
            }
            catch(Exception ex)
            {
            }
            return View("Index", order);
        }        

        [HttpGet]
        public ActionResult Result(string orderId, string boNumber, bool externalInterface)
        {
            var result = new ResultItem
                {
                    BoOrderId = boNumber,
                    OrderId = orderId,
                    IsExternalInterface = externalInterface
                };
            if(result.IsExternalInterface && !String.IsNullOrEmpty(result.BoOrderId) &&
               !String.IsNullOrEmpty(result.OrderId))
            {
                return
                    Redirect(String.Format(ConfigurationManager.AppSettings["CallCenterFinishPage"], result.OrderId,
                                           result.BoOrderId));
            }
            return View(result);
        }

        public ActionResult GetBoNumber(string orderId)
        {
            if(String.IsNullOrEmpty(orderId))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            try
            {
                var order = OrderProcessing.GetOrder(orderId);
                return Json(order.BoOrderId, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpGet]
        public ActionResult Index()
        {
            return View("Index", new OrderItem
            {
                CampaignId = ConfigurationManager.AppSettings["CampaignName"],
                ConsumerId = ConfigurationManager.AppSettings["ConsumerName"],
            });
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult Index(OrderItem item)
        {
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = OrderProcessing.ValidateOrder(item);
            if(ModelState.IsValid && item.IsApproved && item.HasArticles &&
               item.AllArticles.All(m => !m.IsDataWrong && !m.IsWrong))
            {
                CommentHelper.UpdateComments(item.BasketArticles);
                CommentHelper.UpdateComment(item);
                var updateResult = OrderProcessing.UpdateOrder(item);
                if(updateResult.ReturnCode != ReturnCode.Ok)
                {
                    item.ErrorMessage = "Произошла ошибка обновления данных заказа: " + updateResult.ErrorMessage;
                    return View("Index", item);
                }
                return RedirectToAction("Order", new
                {
                    orderId = item.OrderId,
                    ro = item.ReadOnly?"1":"0",
                    fi = item.IsFixed ? "1" : "0",
                    smart = item.IsSmart ? "1" : "0",
                    showArticleStockStatus = item.ShowArticleStockStatus ? "1" : "0",
                    canChangeBasket = item.CanChangeBasket ? "1" : "0",
                    canChangeShipmentType = item.CanChangeShipmentType ? "1" : "0",
                });
            }            
            return View("Index", item);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Validate(OrderItem item)
        {
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = PrepareOrder(item);
            CommentHelper.UpdateComments(item.BasketArticles);
            CommentHelper.UpdateComment(item);            
            return View("Index", item);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ValidateAndSaveOrder(OrderItem item)
        {
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = OrderProcessing.ValidateOrder(item);

            CommentHelper.UpdateComments(item.BasketArticles);
            CommentHelper.UpdateComment(item);
            var updateResult = OrderProcessing.UpdateOrder(item);
            if (updateResult.ReturnCode != ReturnCode.Ok)
            {
                item.ErrorMessage = "Произошла ошибка обновления данных заказа: " + updateResult.ErrorMessage;
                return Json(false);
            }

            return
                Json(ModelState.IsValid && item.AllArticles.Any() &&
                     item.AllArticles.All(m => !m.IsDataWrong && !m.IsWrong));
        }

        #region [ArticleActions]

        [HttpPost,ValidateInput(false)]
        public ActionResult AddArticle(OrderItem item)
        {
            item.NewArticles.Add(new ArticleLineItem {IsWrong = true, Type = ArticleType.Product});
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = PrepareOrder(item);            
            return View("Index", item);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult DeleteNewArticle(OrderItem item, int index)
        {
            item.NewArticles.RemoveAt(index);
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = PrepareOrder(item);            
            return View("Index", item);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult DeleteBasketArticle(OrderItem item, int index)
        {
            item.BasketArticles.RemoveAt(index);
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = PrepareOrder(item);            
            return View("Index", item);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AddQty(OrderItem item, int index)
        {
            item.BasketArticles[index].Qty++;
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = PrepareOrder(item);            
            return View("Index", item);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SubQty(OrderItem item, int index)
        {
            item.BasketArticles[index].Qty--;
            if(item.BasketArticles[index].Qty == 0)
            {
                item.BasketArticles.RemoveAt(index);
            }
            item.PickupInfo.AllCityStores = OrderProcessing.GetAllCityStores(item);
            item = PrepareOrder(item);            
            return View("Index", item);
        }

        #endregion
    }
}