﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Orders.Backend.Clients.CallCenter.MVC.Common;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class CustomerItem
    {
        public CustomerItem()
        {
            Gender = GenderDictionary.First().Key;
        }

        public long? CustomerId { get; set; }

        [Display(Name = "Фамилия заказчика")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string CustomerSurname { get; set; }

        [Display(Name = "Имя заказчика")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string CustomerName { get; set; }

        [Display(Name = "Отчество заказчика")]
        public string CustomerLastName { get; set; }

        [Display(Name = "Дата рождения")]
        public DateTime? Birthday { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [RegularExpression(@"^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$", ErrorMessage = "Неверный email.")]
        public string Email { get; set; }

        [Display(Name = "Основной телефон")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string MainPhone { get; set; }

        [Display(Name = "Дополнительные телефоны")]
        public string OptionalPhones { get; set; }

        [Display(Name = "Факс")]
        public string Fax { get; set; }

        [Display(Name = "Получатель не заказчик")]
        public bool InvoiceNotSame { get; set; }
        
        [Display(Name = "Пол")]
        public string Gender { get; set; }

        public Dictionary<string, string> GenderDictionary
        {
            get
            {
                return new Dictionary<string, string> 
                {
                    { "M", "мужской" }, { "Ж", "женский" }
                };
            }
        }

    }
}