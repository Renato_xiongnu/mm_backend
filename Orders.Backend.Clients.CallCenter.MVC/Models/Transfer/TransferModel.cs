﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Orders.Backend.Clients.CallCenter.MVC.Models.Transfer
{
    public class CreateTransferModel
    {
        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }

        [Display(Name = "Номер резерва")]
        public string WWSOrderId { get; set; }

        [Display(Name = "Источник заказа")]
        public string OrderSourceName { get; set; }

        [Display(Name = "Тип оплаты")]
        [HiddenInput(DisplayValue = true)]
        public string PaymentType { get; set; }

        [UIHint("Controls/BasketArticleLine")]
        public List<ArticleLineItem> BasketArticles { get; set; }

        [Display(Name = "Способ получения")]
        public string PickupType { get; set; }

        [Display(Name = "Магазин")]
        public StoreModel Store { get; set; }

        [UIHint("Controls/StoreStockInfo")]
        public List<StoreModel> CityStores { get; set; }
    }

    public class WwsArticleInfoRequestModel
    {
        private List<string> _articles;

        public string SapCode { get; set; }

        public List<string> Articles
        {
            get { return _articles ?? (_articles=new List<string>()); }
            set { _articles = value; }
        }
    }

    public class WwsArticleInfoModel
    {
        private WwsArticleInfo[] _wwsArticleInfos;

        public WwsArticleInfo[] WwsArticleInfos
        {
            get { return _wwsArticleInfos??(_wwsArticleInfos = new WwsArticleInfo[0]); }
            set { _wwsArticleInfos = value; }
        }
    }
}

public class WwsArticleInfo
{
    [Display(Name = "Артикул")]
    public long Article { get; set; }

    [Display(Name = "Цена закупки")]
    public decimal NettoPrice { get; set; }
}