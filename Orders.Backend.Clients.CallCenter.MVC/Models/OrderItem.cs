﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Common.Helpers;
using Orders.Backend.Clients.CallCenter.MVC.Common;
using Orders.Backend.Clients.CallCenter.MVC.OrderLink;
using Orders.Backend.Clients.Web.Common;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class OrderItem
    {
        public OrderItem()
        {
            PickupInfo = new PickupInfoItem();
            Customer = new CustomerItem();
            BasketArticles = new List<ArticleLineItem>();
            NewArticles = new List<ArticleLineItem>();
        }
        
        [UIHint("Controls/DeliveryInfo")]
        public PickupInfoItem PickupInfo { get; set; }
        
        [UIHint("Controls/Customer")]
        public CustomerItem Customer { get; set; }

        [UIHint("Controls/BasketArticleLine")]
        public List<ArticleLineItem> BasketArticles { get; set; }

        [UIHint("Controls/ArticleLine")]
        public List<ArticleLineItem> NewArticles { get; set; }

        public IEnumerable<ArticleLineItem> AllArticles
        {
            get
            {
                return BasketArticles.Concat(NewArticles);
            }
        }

        public bool ExternalInterface { get; set; }

        public bool ReadOnly { get; set; }
        public bool IsFixed { get; set; }

        public string SaleLocationId { get; set; }

        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }

        [Display(Name = "Номер резерва")]
        public string WWSOrderId { get; set; }

        [Display(Name = "Номер заказа БО")]
        public string BoOrderId { get; set; }

        [Display(Name = "Название акции")]
        public string CampaignId { get; set; }

        [Display(Name = "Потребитель акции")]
        public string ConsumerId { get; set; }

        [Display(Name = "Тип оплаты")]
        [HiddenInput(DisplayValue = true)]
        public string PaymentType { get; set; }

        [Display(Name = "Комментарии к заказу")]
        public string Comment { get; set; }

        [Display(Name = "Новый комментарий к заказу")]
        [DataType(DataType.MultilineText)]
        public string NewComment { get; set; }

        public Reserve ReserveInfo { get; set; }

        public bool HasArticles
        {
            get
            {
                return AllArticles.Count() > 0;
            }
        }

        public bool IsApproved { get; set; }

        public ErrorObject ErrorType { get; set; }

        public string ErrorMessage { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return BasketArticles.Sum(m => m.Price * m.Qty)/*+ DeliveryP.Sum(m => m.Price)*/;
            }
        }

        public Dictionary<string, string> PickupTypes
        {
            get
            {
                return new Dictionary<string, string>
                    {
                        {"1", "Самовывоз"},
                        {"2", "Доставка"}
                    };
            }
        }

        public OrderSource OrderSource { get; set; }

        [Display(Name = "Источник заказа")]
        public string OrderSourceName
        {
            get
            {
                return
                    ManagerUIExtension.GetOrderSourceDisplayName(
                        OrderSource.ConvertTo<Web.Common.OnlineOrder.OrderSource>());
            }
        }

        public bool IsSmart { get; set; }

        public bool ShowArticleStockStatus { get; set; }

        public bool CanChangeBasket { get; set; }

        public bool CanChangeShipmentType { get; set; }
        public string BasketId { get; set; }
        private string _validatedOutcome=string.Empty;
        public string ValidateIfOutcome { get { return _validatedOutcome; } set { _validatedOutcome = value; } }
        public bool OutcomeShouldBeValidated { get { return !string.IsNullOrEmpty(ValidateIfOutcome); } }
    }
}