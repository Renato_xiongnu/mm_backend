﻿using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class ArticleStockStatusInfoItem
    {
        [Display(Name = "Артикул")]
        public long Article { get; set; }

        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Количество")]
        public int Quantity { get; set; }

        [Display(Name = "Цена закупки")]
        public decimal NettoPrice { get; set; }

        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Статус")]
        public StockStatus StockStatus { get; set; }

        [Display(Name = "Тип продукта")]
        public ArticleProductType ProductType { get; set; }

        [Display(Name = "Необходимое количество")]
        public int RequiredQuantity { get; set; }
    }
}