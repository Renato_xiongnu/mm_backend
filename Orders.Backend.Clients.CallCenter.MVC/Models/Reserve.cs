﻿using System.ComponentModel;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class Reserve
    {
        [DisplayName("Магазин в котором создан резерв")]
        public string SapCode { get; set; }

        [DisplayName("Номер резерва WWS")]
        public string WWSOrderId { get; set; }

        public StoreModel Store { get; set; }

        public bool AllReserved { get; set; }
    }
}