﻿using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public enum ArticleProductType
    {
        [Display(Name = "Продукт")] Product = 0,
        [Display(Name = "Набор")] Set = 1,
        [Display(Name = "Гарантия")] WarrantyPlus = 2,
        [Display(Name = "Сервис установки")] InstallationService = 3,
        [Display(Name = "Доставка")] DeliveryService = 4,
        [Display(Name = "Статистика")] Statistical = 5,
        [Display(Name = "Доплата за установку")] InstallServiceMarging = 6
    }
}