﻿using System.ComponentModel.DataAnnotations;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public enum StockStatus
    {
        [Display(Name = "Нет в наличии")]
        NotInStock = 0,

        [Display(Name = "В наличии")]
        InStock = 1,

        [Display(Name = "Витрина")]
        DisplayItem = 2,

        [Display(Name = "Заблокирован")]
        IsBlocked = 3,
    }
}