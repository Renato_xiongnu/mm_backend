﻿using System.ComponentModel;
using System.Web.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Orders.Backend.Clients.CallCenter.MVC.Common;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class PickupInfoItem:IValidatableObject
    {
        private List<StoreModel> _allCityStores;

        [Display(Name = "Адрес")]
        [RequiredIf("HasDelivery", true, ErrorMessage = "Поле 'Адрес' не заполнено")] //TODO RequiredIf не делает корректный Format
        public string Address { get; set; }

        [Display(Name = "Город")]
        [RequiredIf("HasDelivery", true, ErrorMessage = "Поле 'Город' не заполнено")]
        public string City { get; set; }

        [Display(Name = "Дата доставки")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = false)]        
        public DateTime? DeliveryDate { get; set; }

        [Display(Name = "Желаемая дата доставки")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = false, NullDisplayText = "Не указана")]
        [Editable(false)]
        public DateTime? RequestedDeliveryDate { get; set; }

        [Display(Name = "Желаемый интервал доставки")]
        [DisplayFormat(NullDisplayText = "Не указан")]
        [Editable(false)]
        public string RequestedDeliveryTimeSlot { get; set; }

        [Display(Name = "Способ получения")]
        public string PickupType { get; set; }

        [Display(Name = "Способ получения")]
        public string PickupTypeName
        {
            get { return PickupType == "2" ? "Доставка" : "Самовывоз"; }
        }

        public IEnumerable<SelectListItem> PickupTypes
        {
            get
            {
                var selectListItems = new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = "Самовывоз",
                                Value = "1",
                                Selected = !HasDelivery
                            },
                        new SelectListItem
                            {
                                Text = "Доставка",
                                Value = "2",
                                Selected = HasDelivery
                            }
                    };

                return selectListItems;
            }
        }

        [Display(Name="Магазин")]
        public string SapCode { get; set; }

        [Display(Name="SaleLocation")]
        public string SaleLocationId { get; set; }

        [Display(Name = "Магазин")]
        public string Name { get; set; }

        [Display(Name = "Новый магазин")]
        public string NewSapCode { get; set; }

        [Display(Name = "Заказ собран в")]
        public string ReserveSapCode { get; set; }

        [Display(Name = "Заказ собран в")]
        public string ReserveName { get; set; }

        [Display(Name = "Исходный магазин")]
        public string OriginalSapCode { get; set; }

        [Display(Name = "Исходный магазин")]
        public string OriginalName { get; set; }

        public bool HasDelivery
        {
            get
            {
                return PickupType=="2";
            }
        }

        public bool RequiredDeliveryDate { get { return HasDelivery && AnyStockInHub; } }

        public bool AnyStockInHub
        {
            get
            {
                return AllCityHubStores
                    .Where(c => c.IsActive)
                    .Where(c => !c.IsVirtual)
                    .Any(c => c.ArticleStockStatusInfoItems
                        .All(a => a.StockStatus == StockStatus.InStock &&
                                  a.Quantity >= a.RequiredQuantity));
            }
        }

        [UIHint("Controls/StoreStockInfo")]
        public List<StoreModel> AllCityHubStores
        {
            get { return AllCityStores.Where(c => c.IsHub).ToList(); }// ?? (_allCityStores = OrderProcessing.GetAllCityStores(SapCode)); }            
        }

        [UIHint("Controls/StoreStockInfo")]
        public List<StoreModel> AllCityPuPStores
        {
            get { return AllCityStores.Where(c => !c.IsHub).ToList(); }// ?? (_allCityStores = OrderProcessing.GetAllCityStores(SapCode)); }            
        }

        [UIHint("Controls/StoreStockInfo")]
        public List<StoreModel> AllCityStores
        {
            get { return _allCityStores??(_allCityStores=new List<StoreModel>()); }// ?? (_allCityStores = OrderProcessing.GetAllCityStores(SapCode)); }
            set { _allCityStores = value; }
        }

        public bool NeedTransfer { get; set; }

        

        public bool ShowArticleStockStatus { get; set; }

        public bool TransferAgreementWithCustomer { get; set; }

        public string Region { get; set; }

        public string ZIPCode { get; set; }

        public string StreetAbbr { get; set; }

        public string Street { get; set; }

        public string House { get; set; }

        public string Housing { get; set; }

        public string Building { get; set; }

        public string Apartment { get; set; }

        public string District { get; set; }

        public string Entrance { get; set; }

        public string EntranceCode { get; set; }

        public string Kladr { get; set; }

        public int? Floor { get; set; }

        public string PickupLocationId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();            
            //if(RequiredDeliveryDate&&!DeliveryDate.HasValue)
            //{
            //    result.Add(new ValidationResult("Поле 'Дата доставки' не заполнено", new[] { "DeliveryDate" }));
            //}
            return result;
        }
    }
}