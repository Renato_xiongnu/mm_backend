﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class ResultItem
    {
        public string OrderId { get; set; }
        public string BoOrderId { get; set; }
        public bool IsExternalInterface { get; set; }
    }
}