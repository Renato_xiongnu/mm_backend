﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Orders.Backend.Clients.CallCenter.MVC.Models.Transfer;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class StoreModel
    {
        private string _wwsArticleInfoPageUrl;

        public string SapCode { get; set; }

        [DisplayName("Магазин")]
        public string Name { get; set; }

        public string Phone { get; set; }

        public string City { get; set; }
        
        public List<ArticleStockStatusInfoItem> ArticleStockStatusInfoItems { get; set; }

        public bool IsSlow { get; set; }

        public bool IsVirtual { get; set; }

        public bool IsActive { get; set; }

        public string WwsArticleInfoPageUrl
        {
            get { return Format(); }            
        }

        public bool IsHub { get; set; }

        private string Format()
        {
            var wwsArticleInfoRequest = new WwsArticleInfoRequestModel
            {
                SapCode = SapCode,
                Articles = ArticleStockStatusInfoItems.Select(a=>a.Article.ToString(CultureInfo.InvariantCulture)).ToList()
            };
            var request = JsonConvert.SerializeObject(wwsArticleInfoRequest);
            request = HttpUtility.UrlEncode(request);
            return string.Format(ConfigurationManager.AppSettings["WwsArticleInfoPage"], request);
        }
    }
}