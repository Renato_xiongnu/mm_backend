﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Web.Mvc;
using Orders.Backend.Clients.CallCenter.MVC.ArticleDataLink;
using Orders.Backend.Clients.CallCenter.MVC.Common;
using Orders.Backend.Clients.CallCenter.MVC.OrderLink;

namespace Orders.Backend.Clients.CallCenter.MVC.Models
{
    public class ArticleLineItem
    {
        public ArticleLineItem()
        {
            Qty = 1;
        }

        [Display(Name = "Артикул")]
        public string ArticleNum { get; set; }

        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        public int Qty { get; set; }

        public ReviewItemState State { get; set; }

        [Display(Name = "Статус")]
        public string StateName
        {
            get
            {
                switch(State)
                {
                    case ReviewItemState.Empty:
                        return "";
                    case ReviewItemState.IncorrectPrice:
                        return "Неправильная цена";
                    case ReviewItemState.NotFound:
                        return "Не найден";
                    case ReviewItemState.Reserved:
                        return "Отложен";
                    case ReviewItemState.ReservedDisplayItem:
                        return "Отложен витринный экземпляр";
                    case ReviewItemState.ReservedPart:
                        return "Отложен частично";
                    case ReviewItemState.DisplayItem:
                        return "Витрина";
                    case ReviewItemState.Transfer:
                        return "Трансфер";
                    case ReviewItemState.TooLowPrice:
                        return "Слишком низкая цена";
                    default:
                        return "";
                }
            }
        }

        [Display(Name = "Комментарии")]                
        public string Comment { get; set; }

        [Display(Name = "Новый комментарий")]
        [DataType(DataType.MultilineText)]
        public string NewComment { get; set; }

        public long LineId { get; set; }

        public bool IsWrong { get; set; }

        public bool IsAbsent { get; set; }

        public bool IsDataWrong
        {
            get
            {
                if(String.IsNullOrEmpty(ArticleNum) 
                    || Price < 0
                    || (Qty <= 0 && ProductType == ArticleProductType.Product && ProductType == ArticleProductType.Set))
                {
                    return true;
                }
                return false;
            }
        }

        public string AvailablePlaces { get; set; }

        public ArticleType Type { get; set; }
        //[Display(Name = "Артикул доставки")]
        //public bool IsDelivery { get; set; }

        public string ErrorMessage
        {
            get
            {
                if(IsDataWrong)
                {
                    if(String.IsNullOrEmpty(ArticleNum))
                    {
                        return "Не задан номер артикула";
                    }
                    //if (Price <= 0)
                    //    return "Не задана цена";
                    if(Qty <= 0)
                    {
                        return "Не задано количество";
                    }
                }
                else if(IsWrong)
                {
                    return "Неправильный артикул";
                }
                return "";
            }
        }

        public bool ReadOnly { get; set; }

        public string Description { get; set; }

        public string ProductLink
        {
            get
            {
                return String.Format(ConfigurationManager.AppSettings["CallCenterProductPage"], ArticleNum);
            }
        }

        public ArticleProductType ProductType { get; set; }

        public void FillAdditionalInfo(SMItemInfo item)
        {
            Description = item.ShortDescription;
        }
    }
}