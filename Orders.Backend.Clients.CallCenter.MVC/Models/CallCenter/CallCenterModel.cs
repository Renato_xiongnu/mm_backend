﻿using System.Collections.Generic;

namespace Orders.Backend.Clients.CallCenter.MVC.Models.CallCenter
{
    public class CallCenterModel
    {
        public string SaleLocationId { get; set; }

        public string SessionId { get; set; }

        public string BasketId { get; set; }

        public string[] CanEditBlocks { get; set; }

        public OrderOption OrderOptions { get; set; }

        private string _validatedOutcome = string.Empty;
        public string ValidateIfOutcome { get { return _validatedOutcome; } set { _validatedOutcome = value; } }

        public bool OutcomeShouldBeValidated { get { return !string.IsNullOrEmpty(ValidateIfOutcome); } }
    }

    public class OrderOption
    {
        public Dictionary<long, OrdelLine> LineOptions { get; set; }

        public string BasketComment { get; set; }
    }

    public class OrdelLine
    {
        public long Article { get; set; }

        public string Comment { get; set; }

        public string ReviewState { get; set; }
    }
}