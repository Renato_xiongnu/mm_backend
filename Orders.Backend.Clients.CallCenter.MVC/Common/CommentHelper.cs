using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Clients.CallCenter.MVC.Models;

namespace Orders.Backend.Clients.CallCenter.MVC.Common
{
    public class CommentHelper
    {
        public static void UpdateComments(IEnumerable<ArticleLineItem> basketArticles)
        {
            var now = DateTime.Now;
            foreach (var article in basketArticles.Where(ba => !string.IsNullOrEmpty(ba.NewComment)))
            {
                var newComment = HttpUtility.HtmlEncode(article.NewComment.Trim()).Replace("\r\n", @"</br>");
                newComment = string.Format(@"<b>Call center ({0:yyyy-MM-dd HH:mm}):</b><br/>{1}", now, newComment);
                article.Comment = string.IsNullOrEmpty(article.Comment)
                    ? newComment
                    : string.Join("<br/>", article.Comment, newComment);
                article.NewComment = string.Empty;
            }
        }

        public static void UpdateComment(OrderItem orderItem)
        {
            var now = DateTime.Now;
            if (string.IsNullOrEmpty(orderItem.NewComment))
            {
                return;
            }
            var newComment = HttpUtility.HtmlEncode(orderItem.NewComment.Trim()).Replace("\r\n", @"</br>");
            newComment = string.Format(@"<b>Call center ({0:yyyy-MM-dd HH:mm}):</b><br/>{1}", now, newComment);
            orderItem.Comment = string.IsNullOrEmpty(orderItem.Comment)
                ? newComment
                : string.Join("<br/>", orderItem.Comment, newComment);
            orderItem.NewComment = string.Empty;
        }
    }
}