﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Orders.Backend.Clients.CallCenter.MVC.BasketServiceLink;
using Orders.Backend.Clients.CallCenter.MVC.Models;
using Orders.Backend.Clients.CallCenter.MVC.Models.Transfer;
using Orders.Backend.Clients.CallCenter.MVC.OrderLink;
using Orders.Backend.Clients.CallCenter.MVC.Proxy.ProductBackend2;
using Orders.Backend.Clients.CallCenter.MVC.Proxy.ProductInfoService;
using Orders.Backend.Clients.CallCenter.MVC.Proxy.Sls;
using ArticleData = Orders.Backend.Clients.CallCenter.MVC.BasketServiceLink.ArticleData;
using ArticleProductType = Orders.Backend.Clients.CallCenter.MVC.Models.ArticleProductType;
using ProductType = Orders.Backend.Clients.CallCenter.MVC.Proxy.ProductInfoService.ProductType;
using ReturnCode = Orders.Backend.Clients.CallCenter.MVC.OrderLink.ReturnCode;

namespace Orders.Backend.Clients.CallCenter.MVC.Common
{
    public static class OrderProcessing
    {
        public static OrderItem ValidateOrder(OrderItem order)
        {
            var response = CallValidate(order);
            foreach (var article in order.AllArticles)
            {
                article.IsAbsent = false;
            }
            if (response.Result == ResponseResult.OK)
            {
                // ugly

                order.IsApproved = true;
                order.ErrorMessage = "";
                ApproveArticles(order, response.Articles);
            }
            else
            {
                order.IsApproved = false;
                order.ErrorType = (ErrorObject)Enum.ToObject(typeof(ErrorObject), response.ErrorObjectType);
                switch (response.ErrorObjectType)
                {
                    case BasketServiceLink.ErrorObject.Article:
                        order.ErrorMessage = response.ErrorObjectId + ": " + response.ErrorMessage;
                        var article =
                            order.AllArticles.FirstOrDefault(m => m.ArticleNum == response.ErrorObjectId);
                        if (article != null)
                        {
                            article.IsWrong = true;
                        }
                        break;
                    case BasketServiceLink.ErrorObject.Internal:
                        order.ErrorMessage = "Артикульный состав содержит одну или несколько некорректных позиций.";
                        break;
                    case BasketServiceLink.ErrorObject.Global:
                        order.ErrorMessage = response.ErrorMessage;
                        break;
                }
            }
            return order;
        }

        public static OrderItem GetOrder(string orderId)
        {
            var client = new OrderServiceClient();
            var gerOrderDataResult = client.GerOrderDataByOrderId(orderId);
            if (gerOrderDataResult.ReturnCode == ReturnCode.Error)
            {
                throw new InvalidOperationException(orderId + ": " + gerOrderDataResult.ErrorMessage);
            }
            var getAllStoresData = client.GetAllStoresData();
            if (getAllStoresData.ReturnCode == ReturnCode.Error)
            {
                throw new InvalidOperationException(orderId + ": " + getAllStoresData.ErrorMessage);
            }

            var ord = gerOrderDataResult.ToOrderModel();
            var stores = GetAllCityStores(ord);
            ord.PickupInfo.AllCityStores = stores.ToList();
            ord.PickupInfo.Name = stores.First(s => s.SapCode == ord.PickupInfo.SapCode).Name;
            if (ord.ReserveInfo != null)
            {
                ord.ReserveInfo.Store = stores.First(s => s.SapCode == ord.ReserveInfo.SapCode);
            }

            return ord;
        }

        public static void RefreshProductInfo(OrderItem orderItem)
        {
            long id;
            var articles = orderItem.AllArticles
                .Where(a => long.TryParse(a.ArticleNum, out id))
                .Select(a => long.Parse(a.ArticleNum))
                .Distinct()
                .ToArray();
            using (var productClient = new ProductInfoServiceClient())
            {
                var productInfos = productClient.GetProductInfo(articles);
                foreach (var productInfo in productInfos)
                {
                    var info = productInfo;
                    foreach (var article in orderItem.AllArticles
                        .Where(a => long.TryParse(a.ArticleNum, out id))
                        .Where(a => long.Parse(a.ArticleNum) == info.Article))
                    {
                        article.ProductType = info.ProductType.ToArticleProductType();
                    }
                }
            }
        }

        public static CreateTransferModel GetTransferModel(string orderId)
        {
            var order = GetOrder(orderId);
            var currentStore = order.PickupInfo
                .AllCityStores
                .First(c => c.SapCode == order.PickupInfo.SapCode);
            return new CreateTransferModel
            {
                OrderId = orderId,
                OrderSourceName = order.OrderSourceName,
                WWSOrderId = order.WWSOrderId,
                PaymentType = order.PaymentType,
                PickupType = order.PickupInfo.PickupType,
                Store = currentStore,
                CityStores = order.PickupInfo
                    .AllCityStores
                    .Where(s => !s.IsVirtual)
                    .Where(s => s.IsActive)
                    .Where(
                        s =>
                            s.ArticleStockStatusInfoItems.All(
                                assii =>
                                    assii.RequiredQuantity <= assii.Quantity && assii.StockStatus == StockStatus.InStock))
                    .ToList(),
                BasketArticles = order.AllArticles
                    .Where(
                        a =>
                            currentStore.ArticleStockStatusInfoItems.Any(
                                assii => assii.Article == long.Parse(a.ArticleNum)))
                    .ToList()
            };
        }

        public static List<StoreModel> GetAllCityStores(OrderItem orderItem)
        {
            StoreModel[] stores;
            using (var client = new OrderServiceClient())
            {
                var getAllStoresData = client.GetAllStoresData();
                if (getAllStoresData.ReturnCode == ReturnCode.Error)
                {
                    throw new InvalidOperationException(orderItem.PickupInfo.SapCode + ": " +
                                                        getAllStoresData.ErrorMessage);
                }
                stores = getAllStoresData.Stores.Select(s => s.ToModel()).ToArray();
                var saleCityName = stores.First(s => s.SapCode == orderItem.PickupInfo.SapCode).City;
                stores = stores.Where(s => s.City == saleCityName).ToArray();
            }
            ReserveInfo[] reserves;
            using (var client = new OrderServiceClient())
            {
                var getReservesResult = client.GetReservesByOrderId(orderItem.OrderId);
                if (getReservesResult.ReturnCode == ReturnCode.Error)
                {
                    throw new InvalidOperationException(getReservesResult.ErrorMessage);
                }
                reserves = getReservesResult.Reserves;
            }
            var requiredQuantities = orderItem.AllArticles
                .Where(a => !string.IsNullOrEmpty(a.ArticleNum))
                .GroupBy(a => a.ArticleNum)
                .Select(g => new
                {
                    Article = long.Parse(g.Key),
                    Quantity = g.Sum(a => a.Qty)
                })
                .ToDictionary(a => a.Article, a => a.Quantity);
            var productTypes = new Dictionary<long, ArticleProductType>();
            using (var productClient = new ProductInfoServiceClient())
            {
                var productInfos = productClient.GetProductInfo(requiredQuantities.Keys.ToArray());
                foreach (var productInfo in productInfos)
                {
                    var articleProductType = productInfo.ProductType.ToArticleProductType();
                    if (articleProductType != ArticleProductType.Product
                        && articleProductType != ArticleProductType.Set)
                    {
                        requiredQuantities.Remove(productInfo.Article);
                    }
                    else
                    {
                        productTypes[productInfo.Article] = articleProductType;
                    }
                }
            }
            SaleLocationOutcome[] storeOutcomes;
            using (var client = new SaleLocationServiceClient())
            {
                storeOutcomes = client.GetSaleLocationOutcomes(orderItem.OrderId).Result
                    .OrderBy(r => r.Created).ToArray();
            }
            using (var ordersBackendServiceClient = new OrdersBackendServiceClient())
            {
                var request = new GetItemsMultipleStocksRequest
                {
                    Channel = "MM",
                    Articles = requiredQuantities.Keys.Select(a => a.ToString(CultureInfo.InvariantCulture)).ToArray(),
                    SaleLocations = stores.Select(s => String.Format("shop_{0}", s.SapCode)).ToArray()
                };
                var stockResult = ordersBackendServiceClient.GetArticleStocksStatus(request);

                foreach (var store in stores)
                {
                    var storeKey = String.Format("shop_{0}", store.SapCode);

                    var storeReserveLines = reserves
                        .Where(r => r.SapCode == store.SapCode)
                        .SelectMany(r => r.ReserveLines)
                        .ToArray();
                    //var result = ordersBackendServiceClient.GetArticleStockStatus(new GetItemsRequest
                    //{
                    //    Channel = "MM",
                    //    SaleLocation = "shop_" + store.SapCode,
                    //    Articles = requiredQuantities.Keys.Select(a => a.ToString(CultureInfo.InvariantCulture)).ToArray()
                    //});
                    store.IsSlow = storeOutcomes.Where(o => o.SapCode == store.SapCode).Any(o => o.Outcome == "Время истекло");
                    store.ArticleStockStatusInfoItems =
                        stockResult.Items
                        .Select(obi =>
                        {
                            var reserveLine = storeReserveLines
                                .Where(rl => long.Parse(rl.ArticleData.ArticleNum) == obi.Article)
                                .FirstOrDefault(rl =>
                                    rl.ReviewItemState == ReviewItemState.IncorrectPrice
                                    || rl.ReviewItemState == ReviewItemState.NotFound
                                    || rl.ReviewItemState == ReviewItemState.DisplayItem
                                    || rl.ReviewItemState == ReviewItemState.TooLowPrice);

                            var price = obi.Prices.ContainsKey(storeKey) ? obi.Prices[storeKey] : 0M;
                            var qty = obi.Qtys.ContainsKey(storeKey) ? obi.Qtys[storeKey] : 0;
                            var status = obi.Statuses.ContainsKey(storeKey) ? obi.Statuses[storeKey].ToStockStatus() : StockStatus.NotInStock;
                            return new ArticleStockStatusInfoItem
                            {
                                Article = obi.Article,
                                Title = obi.Title,
                                Price = price,
                                Quantity = qty,
                                RequiredQuantity = requiredQuantities[obi.Article],
                                StockStatus = reserveLine == null
                                    ? status
                                    : reserveLine.ReviewItemState.ToStockStatus(),
                                ProductType = productTypes[obi.Article]
                            };
                        }).ToList();
                }
            }
            return stores.ToList();
        }

        private static ArticleProductType ToArticleProductType(this ProductType articleProductType)
        {
            switch (articleProductType)
            {
                case ProductType.Product:
                    return ArticleProductType.Product;
                case ProductType.Set:
                    return ArticleProductType.Set;
                case ProductType.WarrantyPlus:
                    return ArticleProductType.WarrantyPlus;
                case ProductType.InstallationService:
                    return ArticleProductType.InstallationService;
                case ProductType.DeliveryService:
                    return ArticleProductType.DeliveryService;
                default:
                    throw new ArgumentOutOfRangeException("articleProductType");
            }
        }

        internal static ArticleProductType ToArticleProductType(this OrderLink.ArticleProductType articleProductType)
        {
            switch (articleProductType)
            {
                case OrderLink.ArticleProductType.Product:
                    return ArticleProductType.Product;
                case OrderLink.ArticleProductType.Set:
                    return ArticleProductType.Set;
                case OrderLink.ArticleProductType.WarrantyPlus:
                    return ArticleProductType.WarrantyPlus;
                case OrderLink.ArticleProductType.InstallationService:
                    return ArticleProductType.Product;
                case OrderLink.ArticleProductType.DeliveryService:
                    return ArticleProductType.DeliveryService;
                case OrderLink.ArticleProductType.Statistical:
                    return ArticleProductType.Statistical;
                case OrderLink.ArticleProductType.InstallServiceMarging:
                    return ArticleProductType.InstallServiceMarging;
                default:
                    throw new ArgumentOutOfRangeException("articleProductType");
            }
        }

        internal static OrderLink.ArticleProductType ToArticleProductType(this ArticleProductType articleProductType)
        {
            switch (articleProductType)
            {
                case ArticleProductType.Product:
                    return OrderLink.ArticleProductType.Product;
                case ArticleProductType.Set:
                    return OrderLink.ArticleProductType.Set;
                case ArticleProductType.WarrantyPlus:
                    return OrderLink.ArticleProductType.WarrantyPlus;
                case ArticleProductType.InstallationService:
                    return OrderLink.ArticleProductType.Product;
                case ArticleProductType.DeliveryService:
                    return OrderLink.ArticleProductType.DeliveryService;
                case ArticleProductType.Statistical:
                    return OrderLink.ArticleProductType.Statistical;
                case ArticleProductType.InstallServiceMarging:
                    return OrderLink.ArticleProductType.InstallServiceMarging;
                default:
                    throw new ArgumentOutOfRangeException("articleProductType");
            }
        }

        private static StockStatus ToStockStatus(this StockStatusEnum stockStatusEnum)
        {
            switch (stockStatusEnum)
            {
                case StockStatusEnum.InStock:
                    return StockStatus.InStock;
                case StockStatusEnum.DisplayItem:
                    return StockStatus.DisplayItem;
                default:
                    return StockStatus.NotInStock;
            }
        }

        private static StockStatus ToStockStatus(this ReviewItemState reviewItemState)
        {
            switch (reviewItemState)
            {
                case ReviewItemState.ReservedDisplayItem:
                case ReviewItemState.DisplayItem:
                    return StockStatus.DisplayItem;
                case ReviewItemState.NotFound:
                case ReviewItemState.IncorrectPrice:
                case ReviewItemState.Transfer:
                case ReviewItemState.TooLowPrice:
                    return StockStatus.NotInStock;
            }
            return StockStatus.InStock;
        }

        public static UpdateOrderOperationResult UpdateOrder(OrderItem order)
        {
            var client = new OrderServiceClient();
            var articles = order.BasketArticles.Concat(order.NewArticles);
            var response = client.UpdateOrder(new UpdateOrderData
            {
                SapCode = order.PickupInfo.PickupType == "2" ? order.PickupInfo.SapCode : order.PickupInfo.NewSapCode,
                Comment = order.Comment,
                OrderId = order.OrderId,
                ClientData = order.Customer.ToOrderCustomerContract(),
                DeliveryInfo = order.PickupInfo.ToDeliveryInfo(),
            }, new ReserveInfo
            {
                ReserveLines = articles.Select(m => m.ToOrderReserveContract()).ToArray()
            });
            return response;
        }

        private static ValidateResponse CallValidate(OrderItem order)
        {
            var articlesToRemove = new List<ArticleLineItem>();
            foreach (var newArticle in order.NewArticles)
            {
                var basketArticle = order.BasketArticles.FirstOrDefault(m => m.ArticleNum == newArticle.ArticleNum);
                if (basketArticle != null)
                {
                    basketArticle.Qty += newArticle.Qty;
                    articlesToRemove.Add(newArticle);
                }
            }
            foreach (var removeArticle in articlesToRemove)
            {
                order.NewArticles.Remove(removeArticle);
            }
            var articles = order.BasketArticles.Concat(order.NewArticles);

            return new ValidateResponse
            {
                Articles = articles.Select(m => new ArticleData
                {
                    Id = m.ArticleNum,
                    Price = m.Price,
                    Qty = m.Qty,
                    Name = m.Description,
                    CampaignId = order.CampaignId,
                    Type =
                            (BasketServiceLink.ArticleType)
                            Enum.Parse(typeof(BasketServiceLink.ArticleType), m.Type.ToString())
                }).ToArray()
            };
        }

        private static void ApproveArticles(OrderItem order, ArticleData[] approvedArticles)
        {
            var allArticles = order.BasketArticles.Concat(order.NewArticles);
            foreach (var item in allArticles)
            {
                var approvedArticle = approvedArticles.FirstOrDefault(m => m.Id == item.ArticleNum);
                if (approvedArticle != null)
                {
                    item.IsWrong = false;
                    if (item.Price == 0)
                    {
                        item.Price = approvedArticle.Price;
                    }
                    item.Description = approvedArticle.Name;
                }
            }
            var newBasketArticles = order.NewArticles.Where(m => !m.IsWrong && !m.IsDataWrong).ToList();
            //!String.IsNullOrEmpty(m.Description) && 
            order.BasketArticles.AddRange(newBasketArticles);
            foreach (var article in newBasketArticles)
            {
                order.NewArticles.Remove(article);
            }
        }

        public static bool NeedTransfer(OrderItem order)
        {
            bool transferAgreementWithCustomer;
            if (order.PickupInfo.HasDelivery)
            {
                transferAgreementWithCustomer = false;
            }
            else
            {
                var storeModel = order.PickupInfo
                    .AllCityStores
                    .First(c => c.SapCode == order.PickupInfo.SapCode);
                transferAgreementWithCustomer = storeModel
                    .ArticleStockStatusInfoItems
                    .Where(a => a.ProductType == ArticleProductType.Product || a.ProductType == ArticleProductType.Set)
                    .Any(a => a.StockStatus != StockStatus.InStock || a.Quantity < a.RequiredQuantity)
                                                || storeModel.IsSlow;
            }
            if (order.PickupInfo.TransferAgreementWithCustomer != transferAgreementWithCustomer)
            {
                using (var client = new OrderServiceClient())
                {
                    var updateResult = client.UpdateOrder(
                        new UpdateOrderData
                        {
                            OrderId = order.OrderId,
                            DeliveryInfo =
                                new DeliveryInfo()
                                {
                                    HasDelivery = order.PickupInfo.HasDelivery,
                                    TransferAgreementWithCustomer = transferAgreementWithCustomer
                                }
                        }, null);
                    if (updateResult.ReturnCode == ReturnCode.Error)
                    {
                        throw new ApplicationException(updateResult.ErrorMessage);
                    }
                    order.PickupInfo.TransferAgreementWithCustomer = transferAgreementWithCustomer;
                }
            }
            return transferAgreementWithCustomer;
        }
    }
}