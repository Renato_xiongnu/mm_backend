﻿using System.Linq;
using Orders.Backend.Clients.CallCenter.MVC.Models;
using Orders.Backend.Clients.CallCenter.MVC.OrderLink;

namespace Orders.Backend.Clients.CallCenter.MVC.Common
{
    public static class ExtensionsOrder
    {
        public static OrderItem ToOrderModel(this GerOrderDataResult order)
        {
            var deliveryInfo = order.DeliveryInfo;
            var item = new OrderItem
            {
                BasketArticles = order.ReserveInfo.ReserveLines.Select(m => m.ToOrderModel()).ToList(),
                SaleLocationId = "shop_"+order.OrderInfo.SaleLocationSapCode,
                PickupInfo =
                {
                    City = deliveryInfo.City,
                    DeliveryDate = deliveryInfo.DeliveryDate,
                    RequestedDeliveryTimeSlot = deliveryInfo.RequestedDeliveryTimeslot,
                    RequestedDeliveryDate = deliveryInfo.RequestedDeliveryDate,
                    Region = deliveryInfo.Region,
                    ZIPCode = deliveryInfo.ZIPCode,                    
                    StreetAbbr = deliveryInfo.StreetAbbr,
                    Street = deliveryInfo.Street,
                    House = deliveryInfo.House,
                    Housing = deliveryInfo.Housing,
                    Building = deliveryInfo.Building,
                    Apartment = deliveryInfo.Apartment,
                    District = deliveryInfo.District,
                    Entrance = deliveryInfo.Entrance,
                    EntranceCode = deliveryInfo.EntranceCode,
                    Kladr = deliveryInfo.Kladr,
                    Address = deliveryInfo.Address,
                    Floor = deliveryInfo.Floor,
                    PickupType = deliveryInfo.HasDelivery ? "2" : "1",
                    PickupLocationId=deliveryInfo.PickupLocationId,
                    SapCode = order.OrderInfo.SapCode,
                    TransferAgreementWithCustomer = deliveryInfo.TransferAgreementWithCustomer.HasValue &&
                                                    deliveryInfo.TransferAgreementWithCustomer.Value,
                },
                Customer = order.ClientData.ToOrderModel(),
                OrderId = order.OrderInfo.OrderId,
                WWSOrderId = order.OrderInfo.WWSOrderId,
                OrderSource = order.OrderInfo.OrderSource,
                Comment = order.OrderInfo.Comment,
                BasketId=order.OrderInfo.BasketId,
                ReserveInfo = ToOrderReserveModel(order.ReserveInfo),
            };
            switch(order.OrderInfo.PaymentType) //TODO
            {
                case PaymentType.Cash:
                {
                    item.PaymentType = "Наличные";
                    break;
                }
                case PaymentType.Online:
                {
                    item.PaymentType = "Онлайн-оплата";
                    break;
                }
                case PaymentType.OnlineCredit:
                {
                    item.PaymentType = "Онлайн кредит";
                    break;
                }            
                case PaymentType.SocialCard:
                {
                    item.PaymentType = "Социальная карта";
                    break;
                }
                default:
                {
                    item.PaymentType = order.OrderInfo.ToString();
                    break;
                }
            }

            return item;
        }

        private static Reserve ToOrderReserveModel(ReserveInfo reserveInfo)
        {
            return new Reserve
            {
                WWSOrderId = reserveInfo.WWSOrderId,
                SapCode = reserveInfo.SapCode,
                AllReserved =
                    reserveInfo.ReserveLines != null
                    && reserveInfo.ReserveLines.Any()
                    && reserveInfo.ReserveLines.All(r => r.ReviewItemState == ReviewItemState.Reserved)
            };
        }

        public static CustomerItem ToOrderModel(this ClientData customer)
        {
            var cust = new CustomerItem
                {
                    Birthday = customer.Birthday,
                    CustomerName = customer.Name,
                    CustomerSurname = customer.Surname,
                    CustomerLastName = customer.LastName,
                    Email = customer.Email,
                    Fax = customer.Fax,
                    MainPhone = customer.Phone,
                    OptionalPhones = customer.Phone2,
                    CustomerId = customer.ZZTCustomerId
                };
            return cust;
        }

        public static ArticleLineItem ToOrderModel(this ReserveLine line)
        {
            return new ArticleLineItem
                {
                    LineId = line.LineId,
                    ArticleNum = line.ArticleData.ArticleNum,
                    Price = line.ArticleData.Price,
                    Qty = line.ArticleData.Qty,
                    Description = line.Title,
                    Type = ArticleType.Product,
                    State = line.ReviewItemState,
                    //Comment = line.ArticleCondition
                    Comment = line.Comment,
                    ProductType = line.ArticleProductType.ToArticleProductType()
                };
        }

        public static ClientData ToOrderCustomerContract(this CustomerItem item)
        {
            return new ClientData
                {
                    Birthday = item.Birthday,
                    Name = item.CustomerName,
                    Surname = item.CustomerSurname,
                    Gender = item.Gender,
                    LastName = item.CustomerLastName,
                    Email = item.Email,
                    Fax = item.Fax,
                    Phone = item.MainPhone,
                    Phone2 = item.OptionalPhones,
                    ZZTCustomerId = item.CustomerId
                };
        }

        public static DeliveryInfo ToDeliveryInfo(this PickupInfoItem item)
        {
            return new DeliveryInfo
            {
                Address = item.Address,
                City = item.City,
                DeliveryDate = item.DeliveryDate,
                HasDelivery = item.HasDelivery,
                RequestedDeliveryTimeslot = item.RequestedDeliveryTimeSlot,
                Region = item.Region,
                ZIPCode = item.ZIPCode,
                StreetAbbr = item.StreetAbbr,
                Street = item.Street,
                House = item.House,
                Housing = item.Housing,
                Building = item.Building,
                Apartment = item.Apartment,
                District = item.District,
                Entrance = item.Entrance,
                EntranceCode = item.EntranceCode,
                Kladr = item.Kladr,
                Floor = item.Floor,
                PickupLocationId =
                    string.IsNullOrWhiteSpace(item.PickupLocationId)
                        ? string.Format("pickup_{0}", item.NewSapCode)
                        : item.PickupLocationId //Fix for SocialOrders
            };
        }

        public static ReserveLine ToOrderReserveContract(this ArticleLineItem item)
        {
            return new ReserveLine
                {
                    LineId = item.LineId,
                    Comment = item.Comment,
                    ReviewItemState =  item.State,
                    ArticleProductType = item.ProductType.ToArticleProductType(),
                    ArticleData = new ArticleData
                        {
                            ArticleNum = item.ArticleNum,
                            Price = item.Price,
                            Qty = item.Qty,                           
                        }
                };
        }

        public static StoreModel ToModel(this StoreData storeData)
        {
            return new StoreModel
            {
                SapCode = storeData.SapCode,
                Name = storeData.Name,
                Phone = storeData.Phone,
                City = storeData.City,
                IsVirtual = storeData.IsVirtual,
                IsActive = storeData.IsActive,
                IsHub = storeData.IsHub,
            };
        }
        
    }
}