﻿CREATE TABLE [dbo].[Log] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [EventDateTime]   DATETIME2 (7)  NOT NULL DEFAULT GETDATE(),
    [EventLevel]      NVARCHAR (10)  NOT NULL,
    [UserName]        NVARCHAR (50)  NULL,
    [MachineName]     NVARCHAR (50)  NULL,
    [ApplicationName] NVARCHAR (50)  NULL,
    [Message]         NVARCHAR (MAX) NOT NULL,
    [CorrelationId]   NVARCHAR (50)  NOT NULL DEFAULT '',
    [Exception] NVARCHAR(MAX) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Log_CorrelationId]
ON [dbo].[Log] 
(
    [CorrelationId] ASC
)
INCLUDE ([EventDateTime]);
