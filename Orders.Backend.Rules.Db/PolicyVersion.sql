﻿CREATE TABLE [dbo].[PolicyVersion]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[PolicyId] INT NOT NULL,
	[Created] DATETIME2 NOT NULL DEFAULT GETUTCDATE(),
	--[Status] NVARCHAR(50) NOT NULL,
	--[Description] NVARCHAR(MAX),
	CONSTRAINT [FK_PolicyVersion_Policy] FOREIGN KEY([PolicyId]) REFERENCES [dbo].[Policy] ([Id])
)
