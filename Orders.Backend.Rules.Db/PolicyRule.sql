﻿CREATE TABLE [dbo].[PolicyRule]
(	
    [PolicyVersionId] INT NOT NULL,
	[Created] DATETIME2 NOT NULL DEFAULT GETUTCDATE(),
	[Order] INT NOT NULL DEFAULT 0,
    [Condition] NVARCHAR(MAX) NOT NULL,
	[Value] NVARCHAR(50) NOT NULL,
	CONSTRAINT [PK_PolicyRule] PRIMARY KEY([PolicyVersionId], [Order]),
	CONSTRAINT [FK_PolicyRule_PolicyVersion] FOREIGN KEY([PolicyVersionId]) REFERENCES [dbo].[PolicyVersion] ([Id]) ON DELETE CASCADE
)
