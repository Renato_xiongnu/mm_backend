﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using Orders.Backend.Clients.Web.Common;
using Orders.Backend.Clients.Web.Common.OnlineOrder;

namespace Orders.Backend.Clients.Web
{
    public partial class OrderInfoControl : UserControl
    {
        private const string CommentTitle = "Call Center";
        private const string ErrorMessage = "Исправьте ошибки в заказе! Цена и наличие должны быть положительными, Артикул - число.";
        private const string ErrorMainInfoMessage = "Исправьте ошибки в заказе! Не правильный телефон.";
        private const string MessageDelimiter = "<br/>";
        private const string PriceFormat = "0.00";
        private const string DateFormat = "yyyy-MM-dd";
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        private const string NumberDelimiter = @"(-|\s)?";
        private const string FirstNumberPrefix = @"((7|8|(\+7))"+ NumberDelimiter+ ")?";
        private const string CityPrefix3 = @"(\d{3}|(\(\d{3}\)))";
        private const string CityPrefix4 = @"(\d{4}|(\(\d{4}\)))";

        private const string TelephoneRegexp =
            @"(((" + FirstNumberPrefix + CityPrefix3 + NumberDelimiter + @")?(\d{3})(" + NumberDelimiter + @"\d{4}))" + 
            "|((" + FirstNumberPrefix + CityPrefix3 + NumberDelimiter + @")?(\d{3})(" + NumberDelimiter + @"\d{2})(" + NumberDelimiter + @"\d{2}))" +
            "|((" + FirstNumberPrefix + CityPrefix3 + NumberDelimiter + @")?((\d(-|\s)?){6}\d))" + 
            "|((" + FirstNumberPrefix + CityPrefix4 + NumberDelimiter + @")?(\d{3})(" + NumberDelimiter + @"\d{3}))" + 
            "|((" + FirstNumberPrefix + CityPrefix4 + NumberDelimiter + @")?(\d{2})(" + NumberDelimiter + @"\d{2})(" + NumberDelimiter + @"\d{2}))"+
            "|((" + FirstNumberPrefix + CityPrefix4 + NumberDelimiter + @")?((\d(-|\s)?){5}\d))" + 
            ")";

        public string OrderId;

        public bool CanChangeArticleStatus;
        public bool CanChangeArticles;
        public bool CanEditArticleNumber;
        
        public bool InEditMode;
        public bool InStatusEditMode;
        public bool InArticleEditMode;
        public bool InPriceEditMode;
        
        public bool HasDelivery;

        public bool OrderPaymentShouldBeBold { get; set; }



        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            tbCustomerTelephoneValidator.ValidationExpression = string.Format(@"^({0}(\s*(;|,)\s*{0})?)$", TelephoneRegexp);
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(OrderId))
                {
                    UpdateErrorStatus(true, "Нет номера заказа!");
                    return;
                }
                UpdateErrorStatus(false, "");
                FillDropDownList(ddlItemState);
                try
                {
                    var client = Services.GetOrderService();
                    
                    var response = client.GerOrderDataByOrderId(OrderId);
                    FillOrderMainInfo(response);
                    BindRows(response, true);
                    ClearNewLineControls();
                }
                catch (Exception exp)
                {

                }
                InEditMode = InStatusEditMode || InArticleEditMode || InPriceEditMode;
                UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);
                hfOrderId.Value = OrderId;
                hfCanChangeArticleStatus.Value = "" + CanChangeArticleStatus;
                hfCanChangeArticles.Value = "" + CanChangeArticles;
                FillEditModeFields(InStatusEditMode, InArticleEditMode, InPriceEditMode);
            }
        }

        private void FillOrderMainInfo(GerOrderDataResult response)
        {
            if (response.DeliveryInfo != null && response.DeliveryInfo.HasDelivery)
            {
                HasDelivery = true;
                lDeliveryCity.Text = response.DeliveryInfo.City;
                lDeliveryAddress.Text = response.DeliveryInfo.Address;
                lDeliveryDate.Text = response.DeliveryInfo.DeliveryDate.HasValue
                                         ? (response.DeliveryInfo.DeliveryDate.Value.ToString(DateFormat))
                                         : "-";
                tbDeliveryCity.Text = response.DeliveryInfo.City;
                tbDeliveryAddress.Text = response.DeliveryInfo.Address;
            }
            else
            {
                HasDelivery = false;
                lDeliveryCity.Text = "";
                lDeliveryAddress.Text = "";
                lDeliveryDate.Text = "";
                tbDeliveryCity.Text = "";
                tbDeliveryAddress.Text = "";
            }
            hfHasDelivery.Value = HasDelivery.ToString();
            lSAPCode.Text = response.OrderInfo.SapCode;
            lOrderSource.Text = ManagerUIExtension.GetOrderSourceDisplayName(response.OrderInfo.OrderSource);
            lPaymentType.Text = ManagerUIExtension.GetPaymentTypeDisplayName(response.OrderInfo.PaymentType);
            OrderPaymentShouldBeBold = response.OrderInfo.PaymentType == PaymentType.OnlineCredit;
            if (OrderPaymentShouldBeBold)
            {
                lPaymentType.Text = string.Format("<b>{0}</b>", lPaymentType.Text);
            }
            lStatusInfoStatus.Text = ManagerUIExtension.GetInternalOrderStatusDisplayName(response.OrderInfo.StatusInfo.Status);

            var udpdateDate = TimeZoneInfo.ConvertTimeFromUtc(response.OrderInfo.UpdateDate,
                                                              TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
            lUpdateDate.Text = string.Format("{0} (МСК)", udpdateDate.ToString(DateTimeFormat));

            lWWSOrderId.Text = response.OrderInfo.WWSOrderId;
            lOrderOnlineNumber.Text = response.OrderInfo.OrderId;

            if (response.ClientData != null)
            {
                lCustomerName.Text = string.Format("{0} {1}", response.ClientData.Name,
                                                   response.ClientData.Surname);
                lCustomerEmail.Text = response.ClientData.Email;
                lCustomerTelephone.Text = response.ClientData.Phone;
                tbCustomerTelephone.Text = response.ClientData.Phone;
            }
            lbOrderCommentLog.Text = response.OrderInfo.Comment;
        }

        protected string ValidateArticle(object bStr, string ok, string bad)
        {
            return ((bool)bStr) ? ok : bad;
        }
        
        protected void ReserveLineBound(object sender, ListViewItemEventArgs e)
        {
            var lineControls = new ReserveLineControls(e.Item);

            var ad = ((ReserveLineDto)e.Item.DataItem);
            lineControls.TbArticleNum.Text = ad.ArticleNum;
            lineControls.TbPrice.Text = "" + ad.Price.ToString(PriceFormat);
            lineControls.TbQty.Text = "" + ad.Qty;
            lineControls.TbComment.Text = "" + ad.Comment;
            FillDropDownList(lineControls.DdlItemState);
            lineControls.DdlItemState.SelectedValue = "" + (int)ad.ReviewItemState;

            lineControls.LNoTitle.Visible = string.IsNullOrEmpty(ad.Title);
            lineControls.LArticleNum.Text = ad.ArticleNum;
            lineControls.LPrice.Text = "" + ad.Price.ToString(PriceFormat);
            lineControls.LQty.Text = "" + ad.Qty;
            lineControls.LComment.Text = (ad.Comment ?? "").Replace(Environment.NewLine, MessageDelimiter).Replace("\n", MessageDelimiter);
            lineControls.LCommentLog.Text = (ad.CommentLog ?? "").Replace(Environment.NewLine, MessageDelimiter).Replace("\n", MessageDelimiter);
            lineControls.LItemState.Text = ManagerUIExtension.GetReviewItemStateDisplayName(ad.ReviewItemState);
            lineControls.LStockPrice.Text = "" + ad.StockPrice.ToString(PriceFormat);
            lineControls.LArticleConditionState.Text = ManagerUIExtension.GetItemStateDisplayName(ad.ArticleConditionState);
            lineControls.LArticleCondition.Text = ad.ArticleCondition;

            UpdateOrderLineVisibility(CanChangeArticleStatus, CanChangeArticles, lineControls, ad.ReviewItemState);
        }

        private void UpdateControlsVisibility(bool canChangeArticleStatus, bool canChangeArticles)
        {
            var readOnly = !canChangeArticleStatus && !canChangeArticles;

            bAddRow.Visible = canChangeArticles && InArticleEditMode;
            pNewRow.Visible = canChangeArticles && InArticleEditMode;

            ddlItemState.Visible = canChangeArticleStatus && InStatusEditMode;
            tbNewPrice.Visible = canChangeArticles && InArticleEditMode;

            bSaveChanges.Visible = !readOnly && InEditMode;
            bCancelEdit.Visible = !readOnly && InEditMode;

            bGoToEditMode.Visible = !readOnly && !InArticleEditMode && !InStatusEditMode;
            bGoToPriceEditMode.Visible = false; //!readOnly && !InPriceEditMode;

            lDeliveryCity.Visible = readOnly || !InArticleEditMode;
            lDeliveryAddress.Visible = readOnly || !InArticleEditMode;
            tbDeliveryCity.Visible = !readOnly && InArticleEditMode;
            tbDeliveryAddress.Visible = !readOnly && InArticleEditMode;

            lCustomerTelephone.Visible = readOnly || !InArticleEditMode;
            tbCustomerTelephone.Visible = !readOnly && InArticleEditMode;
        }

        private void UpdateOrderLinesVisibility(bool canChangeArticleStatus, bool canChangeArticles)
        {
            foreach (var item in lvLines.Items)
            {
                var lineControls = new ReserveLineControls(item);
                var reviewItemState = GetReviewItemStateByNumber(lineControls.DdlItemState.SelectedValue);
                UpdateOrderLineVisibility(canChangeArticleStatus, canChangeArticles, lineControls, reviewItemState);
            }
        }

        private void UpdateOrderLineVisibility(bool canChangeArticleStatus, bool canChangeArticles,
                                               ReserveLineControls lineControls, ReviewItemState reviewItemState)
        {
            var editArticles = canChangeArticles && InArticleEditMode;
            var editArticleStatus = canChangeArticleStatus && InStatusEditMode;
            var editPriceStatus = canChangeArticles && InPriceEditMode;

            lineControls.LNoTitle.Visible = string.IsNullOrEmpty(lineControls.HfArticleTitle.Value);

            lineControls.TbArticleNum.Visible = editArticles;
            long l;
            lineControls.TbArticleNum.Enabled = !long.TryParse(lineControls.TbArticleNum.Text, out l) || CanEditArticleNumber;
            lineControls.TbPrice.Visible = editPriceStatus;
            lineControls.TbQty.Visible = editArticles;
            lineControls.TbComment.Visible = true; //editPriceStatus || editArticles || editArticleStatus;
            lineControls.BAddComment.Visible = !(editPriceStatus || editArticles || editArticleStatus);
            lineControls.DdlItemState.Visible = editArticleStatus;

            lineControls.LItemStateError.Visible = reviewItemState == ReviewItemState.Empty && canChangeArticleStatus;
            lineControls.RfItemState.Enabled = reviewItemState == ReviewItemState.Empty && canChangeArticleStatus;

            lineControls.LArticleNum.Visible = !editArticles;
            lineControls.LPrice.Visible = !editPriceStatus;
            lineControls.LQty.Visible = !editArticles;
            lineControls.LComment.Visible = false; // !(editPriceStatus || editArticles || editArticleStatus);
            lineControls.LItemState.Visible = !editArticleStatus;

            lineControls.BDeleteRow.Visible = editArticles;
        }

        protected void AddLineClick(object sender, EventArgs e)
        {
            HasDelivery = IsHaveDelivery();
            var lines = GetReserveLinesFromControls(false);
            if (lines == null)
            {
                hierarchyRoundPanel.ClientVisible = true;
                btnExpand.ClientVisible = false;
                UpdateErrorStatus(true, ErrorMessage);
                return;
            }
            InStatusEditMode = bool.Parse(hfInStatusEditMode.Value);
            InArticleEditMode = bool.Parse(hfInArticleEditMode.Value);
            InPriceEditMode = bool.Parse(hfInPriceEditMode.Value);
            InEditMode = true;
            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            CanChangeArticles = bool.Parse(hfCanChangeArticles.Value);
            UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);
            var newLine = GetNewReserveLineFromControls();
            if (newLine != null)
            {
                newLine.Number = lines.Count + 1;
                lines.Add(newLine);
                lvLines.DataSource = lines;
                lvLines.DataBind();
                UpdateSummary(lines);
            }
            else
            {
                hierarchyRoundPanel.ClientVisible = true;
                btnExpand.ClientVisible = false;
                UpdateErrorStatus(true, ErrorMessage);
                return;
            }
            ClearNewLineControls();

            hierarchyRoundPanel.ClientVisible = false;
            btnExpand.ClientVisible = true;
            UpdateErrorStatus(false, "");
        }

        private bool IsFormMainInfoValid()
        {
            var telephoneValid = tbCustomerTelephoneValidator.IsValid;
            return telephoneValid;
        }
        
        protected void SaveChanges(object sender, EventArgs e)
        {
            HasDelivery = IsHaveDelivery();
            OrderId = hfOrderId.Value;
            if (IsFormMainInfoValid())
            {
                SaveOrder();
            }
            else
            {
                UpdateErrorStatus(true, ErrorMainInfoMessage);
            }
        }

        protected void AddCommentClick(object sender, EventArgs e)
        {
            SaveOrder();
        }

        public bool SaveOrder()
        {
            HasDelivery = IsHaveDelivery();
            OrderId = hfOrderId.Value;

            var lines = GetReserveLinesFromControls(true);
            if (lines == null)
            {
                UpdateErrorStatus(true, ErrorMessage);
                return false;
            }            
            var client = Services.GetOrderService();
            var reserveLines = lines.Select(x => new ReserveLine
                                                        {
                                                            LineId = x.LineId,
                                                            ArticleData = new ArticleData
                                                                {
                                                                    ArticleNum = x.ArticleNum, Price = x.Price, Qty = x.Qty
                                                                },
                                                            Comment = GetResultCommentByArticle(x), 
                                                            ReviewItemState = x.ReviewItemState, 
                                                            Title = x.Title, 
                                                            StockPrice = x.StockPrice, 
                                                            StockQty = x.StockQty, 
                                                            StockItemState = x.ArticleConditionState,
                                                            ArticleCondition = x.ArticleCondition
                                                        }).ToArray();


            var deliveryInfo = new DeliveryInfo
                                   {
                                       Address = tbDeliveryAddress.Text,
                                       City = tbDeliveryCity.Text,
                                       HasDelivery = HasDelivery
                                   };
            var clientData = new ClientData {Phone = tbCustomerTelephone.Text, Email = lCustomerEmail.Text};
            client.UpdateOrderByEmployee(new UpdateOrderData
                                   {
                                       OrderId = OrderId, 
                                       DeliveryInfo = deliveryInfo, 
                                       ClientData = clientData,
                                       Comment = GetResultComment(lbOrderCommentLog.Text,tbNewOrderComment.Text),                                       
                                   }, new ReserveInfo { ReserveLines = reserveLines });
            tbNewOrderComment.Text = null;
            var response = client.GerOrderDataByOrderId(OrderId);
            FillEditModeFields(false, false, false);

            FillOrderMainInfo(response);
            BindRows(response, false);
            hierarchyRoundPanel.ClientVisible = false;
            btnExpand.ClientVisible = true;
            UpdateErrorStatus(false, "");

            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            CanChangeArticles = bool.Parse(hfCanChangeArticles.Value);
            UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);
            //UpdateOrderLinesVisibility(CanChangeArticleStatus, CanChangeArticles);
            return true;
        }

        private static string GetResultCommentByArticle(ReserveLineDto x)
        {
            return x.CommentLog 
                   + (String.IsNullOrEmpty(x.Comment) ? "" :
                          string.Format("{0}<b>{1} ({2}):</b>{0}{3}", MessageDelimiter, CommentTitle, DateTime.Today.ToString(DateFormat), x.Comment));
        }

        private static string GetResultComment(string commentLog, string newComment)
        {
            return commentLog
                   + (String.IsNullOrEmpty(newComment) ? "" :
                          string.Format("{0}<b>{1} ({2}):</b>{0}{3}", MessageDelimiter, CommentTitle, DateTime.Today.ToString(DateFormat), newComment));
        }

        private void BindRows(GerOrderDataResult response, bool gotoToEditModeIfHaveProblem)
        {
            long l;
            int number = 1;
            var lines = response.ReserveInfo.ReserveLines.Select(x => new ReserveLineDto
            {
                Number = number++,
                LineId = x.LineId,
                ArticleNum = x.ArticleData.ArticleNum,
                Title = string.IsNullOrEmpty(x.Title) ? "" : x.Title,
                Price = x.ArticleData.Price,
                Qty = x.ArticleData.Qty,
                ReviewItemState = x.ReviewItemState,
                Comment = "",
                CommentLog = x.Comment,
                StockPrice = x.StockPrice,
                StockQty = x.StockQty,
                ArticleConditionState = x.StockItemState,
                ArticleCondition = x.ArticleCondition,
                WWSDepartmentNumber = x.WWSDepartmentNumber,
                WWSFreeQty = x.WWSFreeQty,
                WWSPriceOrig = x.WWSPriceOrig,
                WWSProductGroupNo = x.WWSProductGroupNumber,
                WWSProductGroupName = x.WWSProductGroupName,
                WWSReserverQuantity = x.WWSReservedQty,
                WWSStockNumber = x.WWSStockNumber,
                WWSInfo = String.Format("Отдел: {0}; ТГ: {1}; Склад: {2}", x.WWSDepartmentNumber, x.WWSProductGroupName, x.WWSStockNumber),
                ValidArticleNum = long.TryParse(x.ArticleData.ArticleNum, out l),
                ValidPrice = x.ArticleData.Price > 0 && x.ArticleData.Price == x.WWSPriceOrig,
                ValidQty = x.ArticleData.Qty > 0 && x.ArticleData.Qty <= x.WWSFreeQty
            }).ToList();
            if (gotoToEditModeIfHaveProblem && lines.Any(x => !long.TryParse(x.ArticleNum, out l)))
            {
                InArticleEditMode = true;
                UpdateErrorStatus(true, ErrorMessage);
            }
            lvLines.DataSource = lines;
            lvLines.DataBind();
            UpdateSummary(lines);
        }

        private List<ReserveLineDto> GetReserveLinesFromControls(bool validate)
        {
            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            var lines = new List<ReserveLineDto>();
            int number = 1;
            foreach (var item in lvLines.Items)
            {
                var lineControls = new ReserveLineControls(item);

                long article;
                var articleNum = lineControls.TbArticleNum.Text;
                if ((validate || articleNum != lineControls.HfArticleNum.Value) && !long.TryParse(articleNum, out article))
                {
                    return null;
                }
                long lineId;
                if (!long.TryParse(lineControls.HfLineId.Value, out lineId))
                {
                    return null;
                }

                decimal pr;
                if ((!decimal.TryParse(lineControls.TbPrice.Text, out pr) || pr <= 0))
                {
                    if (validate)
                    {
                        return null;
                    }
                }
                int q;
                if ((!Int32.TryParse(lineControls.TbQty.Text, out q) || q <= 0))
                {
                    if (validate)
                    {
                        return null;
                    }
                }
                int stockQ;
                if ((!Int32.TryParse(lineControls.HfArticleStockQty.Value, out stockQ)))
                {
                    if (validate)
                    {
                        return null;
                    }
                }
                decimal stockP;
                if ((!decimal.TryParse(lineControls.HfStockPrice.Value, out stockP)))
                {
                    if (validate)
                    {
                        return null;
                    }
                }
                if (string.IsNullOrEmpty(lineControls.HfArticleNum.Value))
                {
                    return null;
                }
                var reviewItemState = GetReviewItemStateByNumber(lineControls.DdlItemState.SelectedValue);

                var itemState = ManagerUIExtension.GetItemState(lineControls.HfStockItemState.Value);
                if (validate && CanChangeArticleStatus && (lineControls.DdlItemState.SelectedValue == "" 
                    || reviewItemState == ReviewItemState.Empty))
                {
                    return null;
                }

                int wwsDepartmentNumber;
                Int32.TryParse(lineControls.HfWwsDepartmentNumber.Value, out wwsDepartmentNumber);
                int wwsFreeQty;
                Int32.TryParse(lineControls.HfWwsFreeQty.Value, out wwsFreeQty);
                decimal wwsPriceOrig;
                decimal.TryParse(lineControls.HfWwsPriceOrig.Value, out wwsPriceOrig);
                //WWSProductGroupName = x.WWSProductGroupName,
                int wwsProductGroupNo;
                Int32.TryParse(lineControls.HfWwsProductGroupNo.Value, out wwsProductGroupNo);
                int wwsReserverQuantity;
                Int32.TryParse(lineControls.HfWwsReserverQuantity.Value, out wwsReserverQuantity);
                int wwsStockNumber;
                Int32.TryParse(lineControls.HfWwsStockNumber.Value, out wwsStockNumber);
                long l;
                var line = new ReserveLineDto
                               {
                                   Number = number++,
                                   ArticleNum = articleNum,
                                   LineId = lineId,
                                   Price = pr,
                                   Qty = q,
                                   Comment = lineControls.TbComment.Text,
                                   CommentLog = lineControls.HfCommentLog.Value,
                                   Title = lineControls.HfArticleTitle.Value,
                                   StockQty = stockQ,
                                   StockPrice = stockP,
                                   ReviewItemState = reviewItemState,
                                   ArticleConditionState = itemState.HasValue ? itemState.Value : ItemState.New,
                                   ArticleCondition = lineControls.LArticleCondition.Text,
                                   WWSDepartmentNumber = wwsDepartmentNumber,
                                   WWSFreeQty = wwsFreeQty,
                                   WWSPriceOrig = wwsPriceOrig,
                                   WWSProductGroupNo = wwsProductGroupNo,
                                   WWSProductGroupName = lineControls.HfWwsProductGroupName.Value,
                                   WWSReserverQuantity = wwsReserverQuantity,
                                   WWSStockNumber = wwsStockNumber,
                                   WWSInfo = lineControls.HfWwsInfo.Value,
                                   ValidArticleNum = long.TryParse(articleNum, out l)
                               };

                line.ValidPrice = line.Price > 0 && line.Price == line.WWSPriceOrig;
                line.ValidQty = line.Qty > 0 && line.Qty <= line.WWSFreeQty;

                lines.Add(line);
            }
            return lines;
        }

        private ReserveLineDto GetNewReserveLineFromControls()
        {
            string articleStr = tbNewArticle.Text;
            string priceStr = tbNewPrice.Text;
            string qtyStr = tbNewQty.Text;
            decimal price;
            decimal.TryParse(priceStr, out price);
            int qty;
            Int32.TryParse(qtyStr, out qty);
            long article;
            if (string.IsNullOrEmpty(articleStr) 
                || !long.TryParse(articleStr, out article)
                || price <= 0 || qty <= 0
                || (CanChangeArticleStatus && (ddlItemState.SelectedValue == "" || ddlItemState.SelectedValue == "0")))
            {
                return null;
            }
            var newLine = new ReserveLineDto
                              {
                                  ArticleNum = articleStr,
                                  LineId = 0,
                                  Price = price,
                                  Qty = qty,
                                  Comment = tbNewComment.Text,
                                  ReviewItemState = GetReviewItemStateByNumber(ddlItemState.SelectedValue)
                              };
            long l;
            newLine.ValidArticleNum = long.TryParse(newLine.ArticleNum, out l);
            newLine.ValidPrice = newLine.Price > 0 && newLine.Price == newLine.WWSPriceOrig;
            newLine.ValidQty = newLine.Qty > 0 && newLine.Qty <= newLine.WWSFreeQty;
            return newLine;
        }
        
        protected void GoToEditMode(object sender, EventArgs e)
        {
            HasDelivery = IsHaveDelivery();
            FillEditModeFields(true, true, bool.Parse(hfInPriceEditMode.Value));
            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            CanChangeArticles = bool.Parse(hfCanChangeArticles.Value);
            UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);
            UpdateOrderLinesVisibility(CanChangeArticleStatus, CanChangeArticles);
        }

        protected void GoToPriceEditMode(object sender, EventArgs e)
        {
            HasDelivery = IsHaveDelivery();
            FillEditModeFields(bool.Parse(hfInStatusEditMode.Value), bool.Parse(hfInArticleEditMode.Value), true);
            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            CanChangeArticles = bool.Parse(hfCanChangeArticles.Value);
            UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);
            UpdateOrderLinesVisibility(CanChangeArticleStatus, CanChangeArticles);
        }

        protected void CancelEdit(object sender, EventArgs e)
        {
            HasDelivery = IsHaveDelivery();
            OrderId = hfOrderId.Value;
            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            CanChangeArticles = bool.Parse(hfCanChangeArticles.Value);

            var client = Services.GetOrderService();
            var response = client.GerOrderDataByOrderId(OrderId);

            FillEditModeFields(false, false, false);
            FillOrderMainInfo(response);
            BindRows(response, false);
            hierarchyRoundPanel.ClientVisible = false;
            btnExpand.ClientVisible = true;
            UpdateErrorStatus(false, "");

            UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);
            //UpdateOrderLinesVisibility(CanChangeArticleStatus, CanChangeArticles);
        }

        protected void DeleteRow(object sender, EventArgs e)
        {
            HasDelivery = IsHaveDelivery();
            var b = (ASPxButton)sender;
            var commandArgument = b.CommandArgument;
            var lines = GetReserveLinesFromControls(false);
            if (lines == null)
            {
                return;
            }
            lines = lines.Where(l => l.Number.ToString() != commandArgument).ToList();
            var number = 1;
            foreach (var line in lines)
            {
                line.Number = number++;
            }

            FillEditModeFields(bool.Parse(hfInStatusEditMode.Value), bool.Parse(hfInArticleEditMode.Value), bool.Parse(hfInPriceEditMode.Value));
            CanChangeArticleStatus = bool.Parse(hfCanChangeArticleStatus.Value);
            CanChangeArticles = bool.Parse(hfCanChangeArticles.Value);
            UpdateControlsVisibility(CanChangeArticleStatus, CanChangeArticles);

            lvLines.DataSource = lines;
            lvLines.DataBind();
            UpdateSummary(lines);
        }

        private bool IsHaveDelivery()
        {
            return hfHasDelivery.Value != null && hfHasDelivery.Value.ToLowerInvariant() == "true";
        }

        private void UpdateErrorStatus(bool show, string message)
        {
            lError.Visible = show;
            lErrorDetailse.Visible = show;
            lErrorDetailse.Text = message;
        }

        private void UpdateSummary(IEnumerable<ReserveLineDto> lines)
        {
            var sum = lines.Sum(x => x.Qty * x.Price);
            lSummary.Text = sum.ToString(PriceFormat);
        }

        private void ClearNewLineControls()
        {
            tbNewArticle.Text = "";
            tbNewPrice.Text = "0";
            tbNewQty.Text = "1";
            tbNewComment.Text = "";
            ddlItemState.SelectedValue = "" + (int)ReviewItemState.Empty;
        }

        private void FillEditModeFields(bool inStatusEditMode, bool inArticleEditMode, bool inPriceEditMode)
        {
            InStatusEditMode = inStatusEditMode;
            InArticleEditMode = inArticleEditMode;
            InPriceEditMode = inPriceEditMode;
            InEditMode = inStatusEditMode || inArticleEditMode || inPriceEditMode;
            hfInStatusEditMode.Value = "" + inStatusEditMode;
            hfInArticleEditMode.Value = "" + inArticleEditMode;
            hfInPriceEditMode.Value = "" + inPriceEditMode;
        }

        private static void FillDropDownList(DropDownList ddlItemState)
        {
            var states = new[] {
                ReviewItemState.Empty,
                ReviewItemState.IncorrectPrice,
                ReviewItemState.NotFound,
                ReviewItemState.Reserved,
                ReviewItemState.ReservedDisplayItem,
                ReviewItemState.ReservedPart
            };
            foreach (var state in states)
            {
                ddlItemState.Items.Add(new ListItem(ManagerUIExtension.GetReviewItemStateDisplayName(state), "" + (int)state));
            }
        }

        private ReviewItemState GetReviewItemStateByNumber(string numberStr)
        {
            return (ReviewItemState)Enum.ToObject(typeof(ReviewItemState), Int32.Parse(numberStr));
        }

        protected void AddOrderCommentClick(object sender, EventArgs e)
        {
            SaveOrder();
        }
    }
}