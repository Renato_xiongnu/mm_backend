﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Authentification;
using OnlineOrders.Common;
using Orders.Backend.Clients.Web.Common;
using Orders.Backend.Clients.Web.Common.Configuration;
using Orders.Backend.Clients.Web.Common.OnlineOrder;
using Orders.Backend.Clients.Web.TicketTool;

namespace Orders.Backend.Clients.Web
{
    public partial class ManagerTaskFormStand : System.Web.UI.Page
    {
        protected const string PriceFormat = "0.00";
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        private string _taskId;
        protected string ErrorMessage;
        protected string StatusMessage;
        //task properties
        protected bool TaskExist;
        protected string TicketId;
        protected string TaskName;
        protected string TaskTime;
        protected string TaskDescription;
        protected string OrderId;
        //order properties
        protected string OrderSapCode;
        protected string OrderSource;
        protected bool OrderPaymentShouldBeBold;
        protected string OrderPaymentType;
        protected string OrderStatusInfo;
        protected string OrderUpdateDate;
        protected string OrderWWSOrderId;
        protected string OrderCustomerName;
        protected string OrderCustomerEmail;
        protected string OrderCustomerTelephone;
        protected bool HaveDelivery;
        protected string DeliveryCity;
        protected string DeliveryAddress;

        protected bool CanEdit;
        protected bool HaveAnyRequiredField;

        protected List<RequiredFieldDto> RequiredFields = new List<RequiredFieldDto>();
        protected List<OurcomeDto> Outcomes = new List<OurcomeDto>();
        protected List<ReserveLineDto> ReserveLines = new List<ReserveLineDto>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            //Response.CacheControl = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();

            
            //_taskId = Request.QueryString[Settings.TaskIdParameter];
            TaskExist = true;
            
            var ticketService = Services.GetTicketToolService();
            try
            {
                if (!IsPostBack)
                {
                    //var task = AuthContext.Auth.Execute(() => ticketService.GetNextTaskById(_taskId), ticketService.InnerChannel);
                    //if (task == null)
                    var task = GenerateTask();
                    
                    TicketId = task.TicketId;
                    TaskName = task.Name;
                    var description = task.Description;
                    description = UpdateDescription(description);
                    TaskDescription = description;

                    CanEdit = true;//TODO !(task.RelatedContent == null || task.RelatedContent.IsReadOnly);
                    OrderId = task.WorkItemId ?? GetOrderId(task);
                    
                    var client = Services.GetOrderService();
                    //var response = client.GerOrderDataByOrderId(OrderId);
                    var response = GenerateOrder();
                    OrderSapCode = response.OrderInfo.SapCode;
                    OrderSource = ManagerUIExtension.GetOrderSourceDisplayName(response.OrderInfo.OrderSource);
                    OrderPaymentType = ManagerUIExtension.GetPaymentTypeDisplayName(response.OrderInfo.PaymentType);
                    OrderPaymentShouldBeBold = response.OrderInfo.PaymentType == PaymentType.OnlineCredit;
                    OrderStatusInfo = ManagerUIExtension.GetInternalOrderStatusDisplayName(response.OrderInfo.StatusInfo.Status);
                    var updateDate = TimeZoneInfo.ConvertTimeFromUtc(response.OrderInfo.UpdateDate,
                                                                      TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
                    OrderUpdateDate = updateDate.ToString(DateTimeFormat);
                    OrderWWSOrderId = response.OrderInfo.WWSOrderId;

                    if (response.ClientData != null)
                    {
                        OrderCustomerName = string.Format("{0} {1}", response.ClientData.Name, response.ClientData.Surname);
                        OrderCustomerEmail = response.ClientData.Email;
                        OrderCustomerTelephone = response.ClientData.Phone;
                    }
                    HaveDelivery = response.DeliveryInfo != null && response.DeliveryInfo.HasDelivery;
                    if (HaveDelivery)
                    {
                        DeliveryCity = response.DeliveryInfo.City;
                        DeliveryAddress = response.DeliveryInfo.Address;
                    }
                    ReserveLines = FillReserveLines(response);
                    //RequiredFields = OrderStatesExtension.GetTestRequiredFields();
                    RequiredFields = GetRequiredFieldDtos(task.RequiredFields, true);
                    int outcomeNumber = 1;
                    Outcomes = task.Outcomes.Select(x => new OurcomeDto
                                                             {
                                                                 Number = outcomeNumber++,
                                                                 Name = x.Value,
                                                                 IsSuccess = x.IsSuccess,
                                                                 SkipTaskRequeriedFields = x.SkipTaskRequeriedFields,
                                                                 Fields = GetRequiredFieldDtos(x.RequiredFields, false),
                                                                 FieldsCount = this.RequiredFields.Count + x.RequiredFields.Count()
                                                             }).ToList();
                    HaveAnyRequiredField = RequiredFields.Count > 0 || Outcomes.Any(o => o.Fields.Count > 0);
                }
                else
                {
                    FinishTask(ticketService);
                }
            }
            catch (Exception exp)
            {
                TaskExist = false;
                StatusMessage = "Ошибка!";
                ErrorMessage = exp.ToString();
            }
        }

        private static List<RequiredFieldDto> GetRequiredFieldDtos(IEnumerable<RequiredField> requiredFields, bool taskField)
        {
            return requiredFields.Select(y => new RequiredFieldDto
                                                  {
                                                      DefaultValue = y.DefaultValue != null ? y.DefaultValue.ToString() : "",
                                                      Name = y.Name,
                                                      TypeName = y.Type,
                                                      Type = ManagerUIExtension.GetRequiredFieldTypeBySystemType(y.Type),
                                                      Value = y.Value != null ? y.Value.ToString() : "",
                                                      PredefinedValues =
                                                          (y is RequiredFieldValueList)
                                                              ? (y as RequiredFieldValueList).PredefinedValuesList.Select(z => z.ToString()).ToList() :
                                                              new List<string>(),
                                                      TaskField = taskField
                                                  }).ToList();
        }

        private void FinishTask(TicketToolServiceClient ticketService)
        {
            StatusMessage = "Задача завершена!";
            TaskExist = false;
        }

        private string GetOrderId(TicketTask task)
        {
            return task.WorkItemId;
        }

        private List<ReserveLineDto> FillReserveLines(GerOrderDataResult response)
        {
            int number = 1;
            long l;
            return response.ReserveInfo.ReserveLines
                .Select(x => new ReserveLineDto
                                {
                                    Number = number++,
                                    LineId = x.LineId,
                                    ArticleNum = x.ArticleData.ArticleNum,
                                    Title = x.Title ?? "",
                                    BrandTitle =x.ArticleData.BrandTitle ?? "",
                                    Price = x.ArticleData.Price,
                                    Qty = x.ArticleData.Qty,
                                    ReviewItemState = x.ReviewItemState,
                                    Comment = "",
                                    CommentLog = x.Comment,
                                    StockPrice = x.StockPrice,
                                    StockQty = x.StockQty,
                                    ArticleConditionState = x.StockItemState,
                                    ArticleCondition = x.ArticleCondition,
                                    Promotions = x.ArticleData.Promotions,

                                    WWSDepartmentNumber = x.WWSDepartmentNumber,
                                    WWSProductGroupNo = x.WWSProductGroupNumber,
                                    WWSProductGroupName = x.WWSProductGroupName,
                                    WWSStockNumber = x.WWSStockNumber,
                                    WWSFreeQty = x.WWSFreeQty,
                                    WWSReserverQuantity = x.WWSReservedQty,
                                    WWSPriceOrig = x.WWSPriceOrig,

                                    ValidArticleNum = long.TryParse( x.ArticleData.ArticleNum, out l),
                                    ValidPrice = x.ArticleData.Price > 0 &&
                                        x.ArticleData.Price == x.WWSPriceOrig,
                                    ValidQty = x.ArticleData.Qty > 0 &&
                                        x.ArticleData.Qty <= x.WWSFreeQty
                                }).ToList();
        }

        private IEnumerable<RequiredFieldDto> GetRequiredFieldFromForm()
        {
            List<RequiredFieldDto> fields = new List<RequiredFieldDto>();

            var outcomeNumberStr = this.Request.Form["hOutcomeNumber"];
            var fieldsCountStr = this.Request.Form["hOutcomeFieldsCount" + outcomeNumberStr];
            int fieldsCount;
            Int32.TryParse(fieldsCountStr, out fieldsCount);
            for (int i = 1; i <= fieldsCount; i++)
            {
                var fieldType = this.Request.Form["hOutcomeFieldType" + outcomeNumberStr + "_" + i];
                var fieldTypeName = this.Request.Form["hOutcomeFieldTypeName" + outcomeNumberStr + "_" + i];
                var fieldName = this.Request.Form["hOutcomeFieldName" + outcomeNumberStr + "_" + i];
                var fieldValue = this.Request.Form["hOutcomeFieldValue" + outcomeNumberStr + "_" + i];
                var fieldTaskField = this.Request.Form["hOutcomeFieldTaskField" + outcomeNumberStr + "_" + i].ToLowerInvariant() == "true";
                var type = ManagerUIExtension.GetRequiredFieldType(fieldType);
                fields.Add(new RequiredFieldDto
                               {
                                   Name = fieldName,
                                   TaskField = fieldTaskField,
                                   Type = type.HasValue ? type.Value : RequiredFieldType.String,
                                   TypeName = fieldTypeName,
                                   Value = fieldValue
                               });
            }

            return fields;
        }

        private IEnumerable<ManagerArticle> GetManagerArticlesFromForm()
        {
            var rowsCountStr = this.Request.Form["hRowsCount"];
            int rowsCount;
            Int32.TryParse(rowsCountStr, out rowsCount);
            List<ManagerArticle> articles = new List<ManagerArticle>();
            for (int number = 1; number <= rowsCount; number++)
            {
                var rowTitle = this.Request.Form["rowTitle" + number];
                var rowLineIdStr = this.Request.Form["rowLineId" + number];
                var articleStr = this.Request.Form["rowArticle" + number];
                var priceStr = this.Request.Form["rowPrice" + number];
                var qtyStr = this.Request.Form["rowQty" + number];
                var commentStr = this.Request.Form["rowComment" + number];
                var rowCommentLog = this.Request.Form["rowCommentLog" + number];

                var rowState = this.Request.Form["rowState" + number];

                var rowCorrectPrice = this.Request.Form["hRowCorrectPrice" + number];
                var rowCorrectQty = this.Request.Form["hRowCorrectQty" + number];
                var rowAltArticle = this.Request.Form["hRowAltArticle" + number];
                var rowAltPrice = this.Request.Form["hRowAltPrice" + number];
                var rowAltQty = this.Request.Form["hRowAltQty" + number];
                var rowAltDisplayItem = this.Request.Form["hRowAltDisplayItem" + number];
                var rowAltDisplayItemCondition = this.Request.Form["hRowAltDisplayItemCondition" + number];

                var rowConditionState = this.Request.Form["rowConditionState" + number];
                var rowConditionStateComment = this.Request.Form["rowConditionStateComment" + number];

                var WWSDepartmentNo = this.Request.Form["WWSDepartmentNo" + number];
                var WWSProductGroup = this.Request.Form["WWSProductGroup" + number];
                var WWSProductGroupNo = this.Request.Form["WWSProductGroupNo" + number];
                var WWSStockNo = this.Request.Form["WWSStockNo" + number];
                var WWSFreeQty = this.Request.Form["WWSFreeQty" + number];
                var WWSReservedQty = this.Request.Form["WWSReservedQty" + number];
                var WWSPriceOrig = this.Request.Form["WWSPriceOrig" + number];

                var state = ManagerUIExtension.GetReviewItemState(rowState);
                var conditionState = ManagerUIExtension.GetItemState(rowConditionState);

                var line = new ManagerArticle
                               {
                                   Title = rowTitle,
                                   Number = number,
                                   Article = articleStr,
                                   Price = decimal.Parse(priceStr),
                                   Qty = Int32.Parse(qtyStr),
                                   Comment = commentStr,
                                   CommentLog = rowCommentLog,
                                   RowState = state.HasValue ? state.Value : ReviewItemState.Empty,
                                   ConditionState = conditionState.HasValue ? conditionState.Value : ItemState.New,
                                   ConditionStateComment = rowConditionStateComment
                               };
                long l;
                line.LineId = long.TryParse(rowLineIdStr, out l) ? l : 0;
                decimal d;
                line.RowCorrectPrice = decimal.TryParse(rowCorrectPrice, out d) ? d : 0;
                int i;
                line.RowCorrectQty = Int32.TryParse(rowCorrectQty, out i) ? i : 0;
                line.RowAltArticle = rowAltArticle;
                line.RowAltPrice = decimal.TryParse(rowAltPrice, out d) ? d : 0;
                line.RowAltQty = Int32.TryParse(rowAltQty, out i) ? i : 0;
                line.RowDisplayItem = rowAltDisplayItem == "true";
                line.RowAltDisplayItemCondition = rowAltDisplayItemCondition;

                line.WWSDepartmentNo = Int32.TryParse(WWSDepartmentNo, out i) ? i : 0;
                line.WWSProductGroup = WWSProductGroup;
                line.WWSProductGroupNo = Int32.TryParse(WWSProductGroupNo, out i) ? i : 0;
                line.WWSStockNo = Int32.TryParse(WWSStockNo, out i) ? i : 0;
                line.WWSFreeQty = Int32.TryParse(WWSFreeQty, out i) ? i : 0;
                line.WWSReservedQty = Int32.TryParse(WWSReservedQty, out i) ? i : 0;
                line.WWSPriceOrig = decimal.TryParse(WWSPriceOrig, out d) ? d : 0;
                
                articles.Add(line);
            }
            return articles;
        }

        private static string UpdateDescription(string description)
        {
            var startPreffix = "<!--mediamarkt.description.start-->";
            var endPreffix = "<!--mediamarkt.description.end-->";
            var s1 = description.IndexOf(startPreffix);
            var e1 = description.IndexOf(endPreffix);
            if (s1 < e1 && s1 > 0)
            {
                var text = description.Substring(s1 + startPreffix.Length, e1 - s1 - startPreffix.Length);
                return text;
            }
            return description;
        }        

        private static TicketTask GenerateTask()
        {
            var task = new TicketTask();
            task.TicketId = "92241d03-6d26-4c6d-a4d5-fccfbec1b1e8";
            task.Name = "Позвонить клиенту";
            task.Description = @"Открой <a href='[TaskId]'>заказ</a> и позвони клиенту. 
<!--mediamarkt.description.start--> Перейди по <a href=''>ссылке</a> и подтверди кредит <!--mediamarkt.description.end-->";
            //task.RelatedContent = new RelatedContent();
            task.RequiredFields = new RequiredField[0];
            //task.RelatedContent.IsReadOnly = true;
            task.WorkItemId = "006-001-285";
            //task.RelatedContent.Url = new Uri(@"http://5.9.83.99:8082/Clients.Web/OrderForm.aspx?id=006-001-285&ro=1");
            List<Outcome> outs = new List<Outcome>();
            outs.Add(new Outcome
            {
                IsSuccess = false,
                SkipTaskRequeriedFields = false,
                Value = "Не дозвонился",
                RequiredFields = new RequiredField[0]
            });
            outs.Add(new Outcome
            {
                IsSuccess = false,
                SkipTaskRequeriedFields = true,
                Value = "Перезвонить позже",
                RequiredFields = new RequiredField[]{
                            new RequiredField
                            {
                                DefaultValue = null,
                                Name = "Перезвоните мне через",
                                Type = "System.TimeSpan"
                            }
                        }
            });
            outs.Add(new Outcome
            {
                IsSuccess = false,
                SkipTaskRequeriedFields = false,
                Value = "Подтверждено",
                RequiredFields = new RequiredField[]{
                            new RequiredFieldValueList
                            {
                                DefaultValue = "Кредит выбран",
                                Name = "Подтверждено",
                                Type = "Choice",
                                PredefinedValuesList = new object[] {"Кредит выбран", "Кредит будет выбран в магазине"}
                            }
                        }
            });
            outs.Add(new Outcome
            {
                IsSuccess = false,
                SkipTaskRequeriedFields = false,
                Value = "Не подтверждено",
                RequiredFields = new RequiredField[]{
                            new RequiredFieldValueList
                            {
                                DefaultValue = "Отказ банков",
                                Name = "Не подтверждено",
                                Type = "Choice",
                                PredefinedValuesList = new object[] {"Отказ банков", "Не подошли условия кредита"}
                            }
                        }
            });
            outs.Add(new Outcome
            {
                IsSuccess = false,
                SkipTaskRequeriedFields = false,
                Value = "Отклонено покупателем",
                RequiredFields = new RequiredField[]{
                            new RequiredFieldValueList
                            {
                                DefaultValue = "Отказ банков",
                                Name = "Отклонено покупателем",
                                Type = "Choice",
                                PredefinedValuesList = new object[] {"Отказ банков", "Не подошли условия кредита", "Другое"}
                            }
                        }
            });
            task.Outcomes = outs.ToArray();
            return task;
        }

        public static GerOrderDataResult GenerateOrder()
        {
            var response = new GerOrderDataResult();
            response.OrderInfo = new OrderInfo();
            response.OrderInfo.SapCode = "R006";
            response.OrderInfo.PaymentType = PaymentType.OnlineCredit;
            response.OrderInfo.OrderSource = Orders.Backend.Clients.Web.Common.OnlineOrder.OrderSource.WebSite;
            response.OrderInfo.StatusInfo = new InternalStatusInfo();
            response.OrderInfo.StatusInfo.Status = InternalOrderStatus.Created;
            response.OrderInfo.StatusInfo.Text = "Created";
            response.OrderInfo.UpdateDate = new DateTime(2013, 8, 29, 15, 17, 56);
            response.OrderInfo.WWSOrderId = "40491007";
            response.ClientData = new ClientData();
            response.ClientData.Name = "ИВАН";
            response.ClientData.Surname = "ИВАНОВ";
            response.ClientData.Email = "mailmail@mailmail.com";
            response.ClientData.Phone = "000011110000";
            response.DeliveryInfo = new DeliveryInfo();
            response.DeliveryInfo.HasDelivery = false;
            response.ReserveInfo = new ReserveInfo();
            response.ReserveInfo.ReserveLines = new ReserveLine[] 
            {
                new ReserveLine
                {
                    LineId = 650,
                    Title = "iPhone 4s",
                    ArticleData = new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 10000,
                        Promotions = new string[0],
                        Qty = 1,
                    },
                    ReviewItemState = Orders.Backend.Clients.Web.Common.OnlineOrder.ReviewItemState.Reserved,
                    Comment = "<br/><b>Магазин (2013-08-29):</b><br/>Отложен",
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = Orders.Backend.Clients.Web.Common.OnlineOrder.ItemState.New,
                    ArticleCondition = "",
                    WWSDepartmentNumber = 28,
                    WWSProductGroupNumber = 3900,
                    WWSProductGroupName = "СМАРТФОНЫ",
                    WWSStockNumber = 6,
                    WWSFreeQty = -185,
                    WWSReservedQty = 31,
                    WWSPriceOrig = 21799
                },
                new ReserveLine
                {
                    LineId = 651,
                    Title = "Phillips SG-450",
                    ArticleData = new ArticleData
                    {
                        ArticleNum = "1234511",
                        Price = 5599,
                        Promotions = new string[0],
                        Qty = 1,
                    },
                    ReviewItemState = Orders.Backend.Clients.Web.Common.OnlineOrder.ReviewItemState.NotFound,
                    Comment = "<br/><b>Магазин (2013-08-29):</b><br/>Не найден",
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = Orders.Backend.Clients.Web.Common.OnlineOrder.ItemState.New,
                    ArticleCondition = "",
                    WWSDepartmentNumber = 29,
                    WWSProductGroupNumber = 3750,
                    WWSProductGroupName = "Микроволновки",
                    WWSStockNumber = 6,
                    WWSFreeQty = -185,
                    WWSReservedQty = 31,
                    WWSPriceOrig = 21799
                }
            };
            return response;
        }
    }
}