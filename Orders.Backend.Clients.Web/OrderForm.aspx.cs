﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Orders.Backend.Clients.Web
{
    public partial class OrderForm : Page
    {
        protected const string OrderIdParameter = "id";

        protected const string ReadOnlyParameter = "ro";

        public string OrderId;

        public bool IsReadOnly;

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            //Response.CacheControl = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();

            OrderId = "";
            if(Request.QueryString.AllKeys.Contains(OrderIdParameter))
            {
                OrderId = Request.QueryString[OrderIdParameter];
            }
            if(Request.QueryString.AllKeys.Contains(ReadOnlyParameter))
            {
                if(Request.QueryString[ReadOnlyParameter] == "1")
                {
                    IsReadOnly = true;
                }
                else if(Request.QueryString[ReadOnlyParameter] == "0")
                {
                    IsReadOnly = false;
                }
                else
                {
                    IsReadOnly = true;
                }
            }
            else
            {
                IsReadOnly = true;
            }
            /* string key = "";
            if (Request.QueryString.AllKeys.Contains(KeyParameter))
            {
                key = Encoder.Encode(Request.QueryString[KeyParameter]);
            }
            var ps = key.Split(new [] {"|"}, StringSplitOptions.RemoveEmptyEntries);
            if (ps.Count() != 2)
            {
                return;
            }
            var order = ps[0];
            var isReadOnlyStr = ps[1];
            if (!bool.TryParse(isReadOnlyStr, out IsReadOnly) 
                || order != OrderId)
            {
                return;
            }*/
            if(!IsPostBack)
            {
                orderControl.DataBind();
            }
        }
    }
}