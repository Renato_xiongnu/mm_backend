﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderInfoControl.ascx.cs" 
    Inherits="Orders.Backend.Clients.Web.OrderInfoControl" ValidateRequestMode="Enabled"%>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<div>
    <script type="text/javascript">
        function OnExpandHierarhy(s, e) {
            hierarchyRoundPanel.SetClientVisible(!hierarchyRoundPanel.GetClientVisible());
            btnExpand.SetClientVisible(false);
        }
        function OnCollapseHierarhy(s, e) {
            hierarchyRoundPanel.SetClientVisible(!hierarchyRoundPanel.GetClientVisible());
            btnExpand.SetClientVisible(true);
        }
    </script>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="mainPanel">
                <ProgressTemplate>
                    <font color="green">Идет загрузка...</font>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="mainPanel">
                <ContentTemplate>
                    <asp:HiddenField runat="server" ID="hfOrderId"/>
                    <asp:HiddenField runat="server" ID="hfCanChangeArticleStatus"/>
                    <asp:HiddenField runat="server" ID="hfCanChangeArticles"/>
                    <asp:HiddenField runat="server" ID="hfInStatusEditMode"/>
                    <asp:HiddenField runat="server" ID="hfInArticleEditMode"/>
                    <asp:HiddenField runat="server" ID="hfInPriceEditMode"/>
                    <asp:HiddenField runat="server" ID="hfHasDelivery"/>
                    <asp:Panel ID="Panel1" runat="server">
                        <asp:Label runat="server" ID="lError" Visible="False" Text="Необходимо дозаполнить!" ForeColor="Red" />
                        <asp:Label runat="server" ID="lErrorDetailse" Visible="False" ForeColor="Red" />
                        <table  style="color: #333333; background-color: #F7F6F3;"  width="100%">
                            <tr >
                                <td colspan="2" style="border-bottom: 1px solid darkblue; text-align: center; font-size: larger">
                                    <h1>
                                        Заказ <asp:Literal runat="server" ID="lOrderOnlineNumber"></asp:Literal>
                                    </h1>
                                </td>
                            </tr>                                                       
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Номер магазина</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lSAPCode"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Откуда поступил заказ</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lOrderSource"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Тип оплаты</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lPaymentType"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Статус</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lStatusInfoStatus"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Время обновления заказа</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lUpdateDate"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Номер заказа в WWS</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lWWSOrderId"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>                                                        
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Имя покупателя</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lCustomerName"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Почта</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lCustomerEmail"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Телефон (один или два)</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lCustomerTelephone"/>
                                    <asp:TextBox runat="server" ID="tbCustomerTelephone" 
                                        TextMode="Phone" 
                                        Width="200"
                                        ToolTip="примеры: +79677654321 или +79677654321; 9654321 или 9654321"/>
                                    &nbsp;
                                    <asp:RegularExpressionValidator ID="tbCustomerTelephoneValidator" runat="server"
                                                ControlToValidate="tbCustomerTelephone"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!"
                                                ValidationExpression="^(((((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{3})(-|\s)?)?(\d{3})((-|\s)?\d{4}))|(((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{3})(-|\s)?)?(\d{3})((-|\s)?\d{2})((-|\s)?\d{2}))|(((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{4})(-|\s)?)?(\d{3})((-|\s)?\d{3}))|(((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{4})(-|\s)?)?(\d{2})((-|\s)?\d{2})((-|\s)?\d{2})))(\s*(;|,)\s*((((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{3})(-|\s)?)?(\d{3})((-|\s)?\d{4}))|(((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{3})(-|\s)?)?(\d{3})((-|\s)?\d{2})((-|\s)?\d{2}))|(((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{4})(-|\s)?)?(\d{3})((-|\s)?\d{3}))|(((8|(\+7)|(8(-|\s))|(\+7(-|\s)))?(\d{4})(-|\s)?)?(\d{2})((-|\s)?\d{2})((-|\s)?\d{2}))))?)$"  ValidationGroup="mainGroup"/>
                                </td>
                            </tr>
                            <% if (HasDelivery)
                               {
                                %>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Город доставки</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lDeliveryCity"/>
                                    <asp:TextBox runat="server" ID="tbDeliveryCity"/>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Адрес доставки</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lDeliveryAddress"/>
                                    <asp:TextBox runat="server" ID="tbDeliveryAddress" TextMode="MultiLine" Rows="3" Width="400"/>
                                    &nbsp;
                                </td>
                            </tr>                            
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Дата доставки</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lDeliveryDate"/>
                                    &nbsp;
                                </td>
                            </tr>                           
                            <% }

                            %>
                             <tr>
                                <td style="border-bottom: 1px solid darkblue;">Комментарий к заказу</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lbOrderCommentLog"/>
                                    <br/>
                                    <asp:Literal runat="server" ID="lbNewOrderComment"/>
                                    <asp:TextBox runat="server" ID="tbNewOrderComment" TextMode="MultiLine" Rows="2" Width="400"/>
                                    <dx:ASPxButton ID="bAddComment" runat="server" Text="Добавить комментарий"
                                                   OnClick="AddOrderCommentClick">
                                        <ClientSideEvents
                                            Click="function(s, e) {LoadingPanel.Show();}" 
                                            Init="function(s, e) {LoadingPanel.Hide();}"/>
                                    </dx:ASPxButton>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style="background-color: lightblue;">&nbsp;</td>
                                <td>Поле без ошибок</td>
                            </tr>
                            <tr>
                                <td style="background-color: pink;">&nbsp;</td>
                                <td>Поле с проблемами</td>
                            </tr>
                        </table>
                        <table  width="100%">
                            <thead style="background-color: #5D7B9D; color: #FFFFFF;">
                                <tr style="border-bottom: 1px solid darkblue;">
                                    <th colspan="4"
                                        style="text-align: center">Товары</th>
                                </tr>
                                <tr style="border-bottom: 1px solid darkblue;">
                                    <th align="left">№</th>
                                    <th style="text-align: left;">Артикул</th>
                                    <th style="text-align: right;">Цена</th>
                                    <th style="text-align: right;">Кол-во</th>
                                </tr>
                            </thead>
                            <asp:ListView runat="server" OnItemDataBound="ReserveLineBound" ID="lvLines">
                                <LayoutTemplate>
                                    <tbody>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                    </tbody>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfLineId" Value='<%# Eval("LineId") %>' />
                                    <asp:HiddenField runat="server" ID="hfArticleNum" Value='<%# Eval("ArticleNum") %>' />
                                    <asp:HiddenField runat="server" ID="hfArticleTitle" Value='<%# Eval("Title") %>' />
                                    <asp:HiddenField runat="server" ID="hfArticleStockQty" Value='<%# Eval("StockQty") %>' />
                                    <asp:HiddenField runat="server" ID="hfStockPrice" Value='<%# Eval("StockPrice") %>' />
                                    <asp:HiddenField runat="server" ID="hfStockItemState" Value='<%# Eval("ArticleConditionState") %>' />
                                    <asp:HiddenField runat="server" ID="hfCommentLog" Value='<%# Eval("CommentLog") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSInfo" Value='<%# Eval("WWSInfo") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSDepartmentNumber" Value='<%# Eval("WWSDepartmentNumber") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSFreeQty" Value='<%# Eval("WWSFreeQty") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSPriceOrig" Value='<%# Eval("WWSPriceOrig") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSProductGroupNo" Value='<%# Eval("WWSProductGroupNo") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSProductGroupName" Value='<%# Eval("WWSProductGroupName") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSReserverQuantity" Value='<%# Eval("WWSReserverQuantity") %>' />
                                    <asp:HiddenField runat="server" ID="hfWWSStockNumber" Value='<%# Eval("WWSStockNumber") %>' />
                                    <tr id="Tr1" style="color: #333333; background-color: #F7F6F3;">
                                        <td >
                                            <h1>
                                                <%# Eval("Number")%>
                                            </h1>
                                        </td>
                                        <td colspan="3">
                                            <asp:Label runat="server" ID="lNoTitle" Text="[нет названия]" Visible="False"/><%# Eval("Title")%><br/>
                                            <%# Eval("WWSInfo")%>
                                        </td>
                                    </tr>
                                    <tr id="rows" style="color: #333333; background-color: #F7F6F3;">
                                        <td >
                                            
                                        </td>
                                        <td style='vertical-align: central; background-color: <%# ValidateArticle(Eval("ValidArticleNum"), "lightblue", "pink") %>'>
                                            <asp:Label runat="server" ID="lArticleNum"/>
                                            <asp:TextBox runat="server" ID="tbArticleNum"  Width="90" style="text-align:right"/>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator41" runat="server"
                                                ControlToValidate="tbArticleNum"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!"
                                                ValidationExpression="^\d*\.?\d*$"  ValidationGroup="mainGroup"/>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server"
                                                ControlToValidate="tbArticleNum"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!"  ValidationGroup="mainGroup"/>
                                        </td>
                                        <td style="vertical-align: central; text-align: right; background-color: <%# ValidateArticle(Eval("ValidPrice"), "lightblue", "pink") %>">
                                            <asp:RegularExpressionValidator ID="v2" runat="server"
                                                ControlToValidate="tbPrice"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!"
                                                ValidationExpression="^\d*\.?\d*$"  ValidationGroup="mainGroup"/>
                                            <asp:RequiredFieldValidator ID="v4" runat="server"
                                                ControlToValidate="tbPrice"
                                                InitialValue="0"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!"  ValidationGroup="mainGroup"/>
                                            <asp:Label runat="server" ID="lPrice"/>
                                            <asp:TextBox runat="server" ID="tbPrice" Width="90" style="text-align:right"/>
                                        </td>
                                        <td style="vertical-align: central; text-align: right; background-color: <%# ValidateArticle(Eval("ValidQty"), "lightblue", "pink") %>">
                                            <asp:RegularExpressionValidator ID="v1" runat="server"
                                                ControlToValidate="tbQty"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!"
                                                ValidationExpression="^\d+$"  ValidationGroup="mainGroup"/>
                                            <asp:RequiredFieldValidator ID="v3" runat="server"
                                                ControlToValidate="tbQty"
                                                InitialValue="0"
                                                SetFocusOnError="True"
                                                ErrorMessage="Please Enter Only Numbers" ForeColor="Red"
                                                Text="!"  ValidationGroup="mainGroup"/>
                                            <asp:Label runat="server" ID="lQty"/>
                                            <asp:TextBox runat="server" ID="tbQty" Width="40" style="text-align:right"/>
                                        </td>
                                    </tr>
                                    <tr style="color: #333333; background-color: #F7F6F3; display: none">
                                        <td>
                                            
                                        </td>
                                        <td style="background-color: lightgreen">
                                            <asp:Label runat="server" ID="lStockItemState"/>
                                        </td>
                                        <td style="text-align: right; background-color: lightgreen">
                                            <asp:Label runat="server" ID="lStockPrice"/>
                                        </td>
                                        <td style="text-align: right; background-color: lightgreen">
                                            <%# Eval("StockQty")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td colspan="3">
                                            <b>Состояние товара:</b>
                                            <asp:Label runat="server" ID="lArticleConditionState"/>
                                            <br/>
                                            <asp:Literal runat="server" ID="lArticleCondition"/>
                                        </td>
                                    </tr>
                                    <tr style="color: #333333; background-color: #F7F6F3;">
                                        <td >
                                            
                                        </td>
                                        <td colspan="3">
                                            <b>Ответ менеджера:</b>
                                            <asp:Label runat="server" ID="lItemState"/>
                                            <asp:DropDownList ID="ddlItemState" runat="server"
                                                style="-webkit-border-radius:20px;-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                Height="30"/>
                                            <asp:Label runat="server" ID="lItemStateError" Text="Заполнить!"  ForeColor="Red"/>
                                            <asp:RequiredFieldValidator ID="rfItemState" runat="server"
                                                ControlToValidate="ddlItemState"
                                                InitialValue="0"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="!" 
                                                ValidationGroup="mainGroup"/>
                                            
                                        </td>
                                    </tr>
                                    <tr style="color: #333333; background-color: #F7F6F3;">
                                        <td >
                                            
                                        </td>
                                        <td colspan="3">
                                            <b>Комментарий</b><br/>
                                            <asp:Literal runat="server" ID="lCommentLog"/>
                                            <br/>
                                            <asp:Literal runat="server" ID="lComment"/>
                                            <asp:TextBox runat="server" ID="tbComment" TextMode="MultiLine" Rows="3" Width="400" />
                                            <dx:ASPxButton ID="bAddComment" runat="server" Text="Добавить комментарий"
                                                OnClick="AddCommentClick">
                                                <ClientSideEvents
                                                    Click="function(s, e) {LoadingPanel.Show();}" 
                                                    Init="function(s, e) {LoadingPanel.Hide();}"/>
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td colspan="3">
                                            <dx:ASPxButton ID="bDeleteRow" runat="server" 
                                                OnClick="DeleteRow" Text="Удалить товар" 
                                                CommandArgument='<%#Eval("Number")%>' 
                                                style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                Width="120"
                                                Height="60">
                                                <ClientSideEvents
                                                    Click="function(s, e) {LoadingPanel.Show();}" 
                                                    Init="function(s, e) {LoadingPanel.Hide();}"/>
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                    <tr style="color: #333333; background-color: darkblue;">
                                        <td colspan="4">
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                            <tr style="text-align: left; color: #333333; background-color: #F7F6F3;">
                                <td colspan="2">
                                      <h3>
                                          Итого:
                                      </h3>     
                                </td>
                                <td style="text-align: right;">
                                    <h3>
                                        <asp:Label runat="server" ID="lSummary"/>
                                    </h3>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr >
                                <td>
                                    
                                </td>
                                <td colspan="3">
                                    <asp:Panel runat="server" ID="pNewRow">
                                        <table>
                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                <td>
                                        
                                                    <dx:ASPxButton ID="btnExpand" 
                                                        ClientInstanceName="btnExpand"
                                                        runat="server" AllowFocus="False"
                                                        AutoPostBack="False"
                                                        Image-Url="Images/add.png"
                                                        Text="Добавить товар"
                                                        style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                        Height="60"
                                                        Width="300">
                                                        <Paddings Padding="1px" />
                                                        <FocusRectPaddings Padding="0" />
                                                        <ClientSideEvents Click="OnExpandHierarhy" />
                                                    </dx:ASPxButton>
                                                     <dx:ASPxRoundPanel ID="hierarchyRoundPanel" ClientInstanceName="hierarchyRoundPanel" runat="server" 
                                                         Width="400px" HeaderStyle-Height="0"
                                                            BackColor="#AAAAAA" ShowHeader="false" ClientVisible="false">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <PanelCollection>
                                                            <dx:PanelContent ID="PanelContent3" runat="server">
                                                                <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100%" ClientInstanceName="HierarhyContentPanel">
                                                                    <PanelCollection>
                                                                        <dx:PanelContent ID="PanelContent4" runat="server">
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <h4>
                                                                                        Заполните поля добавляемого товара
                                                                                    </h4>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                                                <td >
                                                                                    Артикул:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="tbNewArticle" Width="80" />
                                                                                    <asp:RegularExpressionValidator ID="v0" runat="server"
                                                                                        ControlToValidate="tbNewArticle"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!"
                                                                                        ValidationExpression="^\d+$"  
                                                                                        ValidationGroup="addGroup"/>
                                                                                    <asp:RequiredFieldValidator ID="v01" runat="server"
                                                                                        ControlToValidate="tbNewArticle"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!"  
                                                                                        ValidationGroup="addGroup"/>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                                                <td>
                                                                                    Цена:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="tbNewPrice" Width="90" style="text-align:right"/>
                                                                                    <asp:RegularExpressionValidator ID="v2" runat="server"
                                                                                        ControlToValidate="tbNewPrice"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!"
                                                                                        ValidationExpression="^\d*\.?\d*$"  
                                                                                        ValidationGroup="addGroup"/>
                                                                                    <asp:RequiredFieldValidator ID="v21" runat="server"
                                                                                        ControlToValidate="tbNewPrice"
                                                                                        InitialValue="0"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!" 
                                                                                        ValidationGroup="addGroup"/>
                                                                                </td>    
                                                                            </tr>
                                                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                                                <td>
                                                                                    Количество:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="tbNewQty" Width="40" style="text-align:right"/>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                                        ControlToValidate="tbNewQty"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!"
                                                                                        ValidationExpression="^\d+$"  
                                                                                        ValidationGroup="addGroup"/>
                                                                                    <asp:RequiredFieldValidator ID="v11" runat="server"
                                                                                        ControlToValidate="tbNewQty"
                                                                                        InitialValue="0"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!" 
                                                                                        ValidationGroup="addGroup"/>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                                                <td colspan="2">
                                                                                    Статус:
                                                                                    <br/>
                                                                                    <asp:DropDownList runat="server" ID="ddlItemState"
                                                                                        style="-webkit-border-radius:20px;-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                                                                Height="30"/>
                                                                                    <asp:RequiredFieldValidator ID="rfv222" runat="server"
                                                                                        ControlToValidate="ddlItemState"
                                                                                        InitialValue="0"
                                                                                        SetFocusOnError="True"
                                                                                        ForeColor="Red"
                                                                                        Text="!" 
                                                                                        ValidationGroup="addGroup"/>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                                                <td>
                                                                                    Комментарий:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="tbNewComment" TextMode="MultiLine" Rows="3" Width="200"/>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="color: #284775; background-color: #AAAAAA; vertical-align: top">
                                                                                <td >
                                                                                    <dx:ASPxButton ID="bAddRow" runat="server" 
                                                                                        Width="160px"
                                                                                        Text="Добавить товар" OnClick="AddLineClick"  ValidationGroup="addGroup" 
                                                                                        style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                                                        Height="60">
                                                                                        <ClientSideEvents
                                                                                            Click="function(s, e) {LoadingPanel.Show();}" 
                                                                                            Init="function(s, e) {LoadingPanel.Hide();}"
                                                                                            />
                                                                                        </dx:ASPxButton>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="btnCollapse" 
                                                                                        ClientInstanceName="btnCollapse"
                                                                                        runat="server" AllowFocus="False"
                                                                                        AutoPostBack="False" Width="160px" 
                                                                                        Text="Отмена добавления товара"
                                                                                        style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                                                        Height="60">
                                                                                        <Paddings Padding="1px" />
                                                                                        <FocusRectPaddings Padding="0" />
                                                                                        <ClientSideEvents Click="OnCollapseHierarhy" 
                                                                                            />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </dx:PanelContent>
                                                                    </PanelCollection>
                                                                </dx:ASPxPanel>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                        <Border BorderColor="#8B8B8B" BorderStyle="Solid" BorderWidth="0px" />
                                                    </dx:ASPxRoundPanel>
    
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr style="color: #333333; background-color: darkblue;">
                                <td colspan="4">
                                </td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr style="color: #284775; background-color: lightblue;">
                                <td>
                            <asp:Panel runat="server" ID="pButtons">
                                <table  >
                                <tr style="color: #284775; background-color: lightblue;">
                                    <td style="text-align:left" >
                                        <dx:ASPxButton ID="bSaveChanges" runat="server" 
                                            OnClick="SaveChanges" Text="Сохранить изменения" 
                                            ValidationGroup="mainGroup"
                                            CausesValidation="True"
                                            style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                            Height="60"
                                            Width="150">
                                            <ClientSideEvents 
                                                Click="function(s, e) {LoadingPanel.Show();}" 
                                                Init="function(s, e) {LoadingPanel.Hide();}"/>
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="bCancelEdit" runat="server" 
                                            OnClick="CancelEdit" Text="Не сохранять" 
                                            ValidationGroup="mainGroup"
                                            style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                            Height="60"
                                            Width="150">
                                            <ClientSideEvents Click="function(s, e) {
                                                                                        LoadingPanel.Show();
                                                                                        }" Init="function(s, e) {LoadingPanel.Hide();}"/>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                </table>
                                <table >
                                <tr style="color: #284775; background-color: lightblue;">
                                    <td>
                                        <dx:ASPxButton ID="bGoToEditMode" runat="server" 
                                            OnClick="GoToEditMode" Text="Редактировать артикульный состав" 
                                            ValidationGroup="mainGroup"
                                            style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                            Height="60"
                                            Width="150">
                                            <ClientSideEvents Click="function(s, e) {
                                                                                        LoadingPanel.Show();
                                                                                        }" 
                                                Init="function(s, e) {LoadingPanel.Hide();}"/>
                                        </dx:ASPxButton>
                                        
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="bGoToPriceEditMode" runat="server" 
                                            OnClick="GoToPriceEditMode" Text="Редактировать цену" 
                                            ValidationGroup="mainGroup"
                                            style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                            Height="60"
                                            Width="150">
                                            <ClientSideEvents Click="function(s, e) {
                                                                                        LoadingPanel.Show();
                                                                                        }" 
                                                Init="function(s, e) {LoadingPanel.Hide();}"/>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                </table>
                            </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
