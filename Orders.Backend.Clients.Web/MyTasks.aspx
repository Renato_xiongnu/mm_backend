﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTasks.aspx.cs" Inherits="Orders.Backend.Clients.Web.MyTasks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Мои задачи</title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet"/>
    <meta name="viewport"  content="width=450, initial-scale=0.7, maximum-scale=0.7, user-scalable=0" />
    <style type="text/css">
        .modal {
            max-height: 90%;
            overflow-y: auto;
        }
        .btn-custom {
          background-color: hsl(214, 37%, 28%) !important;
          background-repeat: repeat-x;
          filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#7a99c1", endColorstr="#2c4361");
          background-image: -khtml-gradient(linear, left top, left bottom, from(#7a99c1), to(#2c4361));
          background-image: -moz-linear-gradient(top, #7a99c1, #2c4361);
          background-image: -ms-linear-gradient(top, #7a99c1, #2c4361);
          background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #7a99c1), color-stop(100%, #2c4361));
          background-image: -webkit-linear-gradient(top, #7a99c1, #2c4361);
          background-image: -o-linear-gradient(top, #7a99c1, #2c4361);
          background-image: linear-gradient(#7a99c1, #2c4361);
          border-color: #2c4361 #2c4361 hsl(214, 37%, 19.5%);
          color: #fff !important;
          text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.56);
          -webkit-font-smoothing: antialiased;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
