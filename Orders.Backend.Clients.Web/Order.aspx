﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="Orders.Backend.Clients.Web.Order" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="mainPanel">
                <ProgressTemplate>
                    <font color="green">Идет загрузка...</font>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="mainPanel">
                <ContentTemplate>
                    <asp:Panel runat="server">
                        <asp:Label runat="server" ID="lError" Visible="False" Text="Необходимо дозаполнить!" ForeColor="Red" />
                        <table>
                            <tr>
                                <td colspan="2">Заказ</td>
                            </tr>
                            <tr>
                                <td>SAP code:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lSAPCode"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>Order Id:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lOrderId"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>Order Source:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lOrderSource"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>Payment Type:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lPaymentType"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>StatusInfo.Status:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lStatusInfoStatus"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>StatusInfo.Text:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lStatusInfoText"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>UpdateDate:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lUpdateDate"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>WWSOrderId:</td>
                                <td>
                                    <asp:Literal runat="server" ID="lWWSOrderId"></asp:Literal></td>
                            </tr>
                        </table>
                        <table>
                            <thead style="background-color: #5D7B9D; color: #FFFFFF;">
                                <tr>
                                    <th width="30" align="left"></th>
                                    <th>Article
                                    </th>
                                    <th>Price
                                    </th>
                                    <th>Qty
                                    </th>
                                    <th>State
                                    </th>
                                </tr>
                            </thead>
                            <asp:ListView runat="server" OnItemDataBound="FeatureBound" ID="lvLines">
                                <LayoutTemplate>

                                    <tbody>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                    </tbody>

                                </LayoutTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfArticleNum" Value='<%# Eval("ArticleData.ArticleNum") %>' />
                                    <tr id="rows" style="color: #333333; background-color: #F7F6F3;">
                                        <td>
                                            <asp:CheckBox runat="server" ID="selectCheckBox" name="checkboxlist" />
                                        </td>
                                        <td>
                                            <%# Eval("ArticleData.ArticleNum")%>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="tbPrice" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="tbQty" ValidateRequestMode="Enabled" CausesValidation="True" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlItemState" runat="server"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="v1" runat="server"
                                                ControlToValidate="tbQty"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Не правильный Qty!"
                                                ValidationExpression="^\d+$" ValidationGroup="mainGroup"/>
                                            <br/>
                                            <asp:RegularExpressionValidator ID="v2" runat="server"
                                                ControlToValidate="tbPrice"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Не правильный Price!"
                                                ValidationExpression="^\d*\.?\d*$"  ValidationGroup="mainGroup"/>
                                            <br/>
                                            <asp:RequiredFieldValidator ID="v3" runat="server"
                                                ControlToValidate="tbQty"
                                                SetFocusOnError="True"
                                                ErrorMessage="Please Enter Only Numbers" ForeColor="Red"
                                                Text="Qty не может быть пустым!"  ValidationGroup="mainGroup"/>
                                            <br/>
                                            <asp:RequiredFieldValidator ID="v4" runat="server"
                                                ControlToValidate="tbPrice"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Price не может быть пустым!"  ValidationGroup="mainGroup"/>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfArticleNum" Value='<%# Eval("ArticleData.ArticleNum") %>' />
                                    <tr id="rows" style="color: #333333; background-color: #F7F6F3;">
                                        <td>
                                            <asp:CheckBox runat="server" ID="selectCheckBox" name="checkboxlist" />
                                        </td>
                                        <td>
                                            <%# Eval("ArticleData.ArticleNum")%>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="tbPrice" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="tbQty" ValidateRequestMode="Enabled" CausesValidation="True" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlItemState" runat="server"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="v1" runat="server"
                                                ControlToValidate="tbQty"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Не правильный Qty!"
                                                ValidationExpression="^\d+$"  
                                                ValidationGroup="mainGroup" />
                                            <br/>
                                            <asp:RegularExpressionValidator ID="v2" runat="server"
                                                ControlToValidate="tbPrice"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Не правильный Price!"
                                                ValidationExpression="^\d*\.?\d*$"  
                                                ValidationGroup="mainGroup"/>
                                            <br/>
                                            <asp:RequiredFieldValidator ID="v3" runat="server"
                                                ControlToValidate="tbQty"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Qty не может быть пустым!"  
                                                ValidationGroup="mainGroup"/>
                                            <br/>
                                            <asp:RequiredFieldValidator ID="v4" runat="server"
                                                ControlToValidate="tbPrice"
                                                SetFocusOnError="True"
                                                ForeColor="Red"
                                                Text="Price не может быть пустым!"  
                                                ValidationGroup="mainGroup"/>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            </asp:ListView>

                            <tr>
                                <td></td>
                                <td>
                                    <asp:TextBox runat="server" ID="tbNewArticle"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="tbNewPrice"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="tbNewQty"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlItemState"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Button ID="Button1" runat="server" Text="Добавить строку" OnClick="AddLineClick"  ValidationGroup="addGroup" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:RegularExpressionValidator ID="v0" runat="server"
                                        ControlToValidate="tbNewArticle"
                                        SetFocusOnError="True"
                                        ForeColor="Red"
                                        Text="Не правильный Article!"
                                        ValidationExpression="^\d+$"  
                                        ValidationGroup="addGroup"/>
                                    <br/>
                                    <asp:RequiredFieldValidator ID="v01" runat="server"
                                        ControlToValidate="tbNewArticle"
                                        SetFocusOnError="True"
                                        ForeColor="Red"
                                        Text="Article не может быть пустым!"  
                                        ValidationGroup="addGroup"/>
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="v2" runat="server"
                                        ControlToValidate="tbNewPrice"
                                        SetFocusOnError="True"
                                        ForeColor="Red"
                                        Text="Не правильный Price!"
                                        ValidationExpression="^\d*\.?\d*$"  
                                        ValidationGroup="addGroup"/>
                                    <br/>
                                    <asp:RequiredFieldValidator ID="v21" runat="server"
                                        ControlToValidate="tbNewPrice"
                                        SetFocusOnError="True"
                                        ForeColor="Red"
                                        Text="Price не может быть пустым!" 
                                        ValidationGroup="addGroup"/>
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="v1" runat="server"
                                        ControlToValidate="tbNewQty"
                                        SetFocusOnError="True"
                                        ForeColor="Red"
                                        Text="Не правильный Qty!"
                                        ValidationExpression="^\d+$"  
                                        ValidationGroup="addGroup"/>
                                    <br/>
                                    <asp:RequiredFieldValidator ID="v11" runat="server"
                                        ControlToValidate="tbNewQty"
                                        SetFocusOnError="True"
                                        ForeColor="Red"
                                        Text="Qty не может быть пустым!" 
                                        ValidationGroup="addGroup"/>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        <asp:Button runat="server" OnClick="DeleteSelectedLines" Text="Удалить выбранные строки" ValidationGroup="mainGroup" />
                        <asp:Button runat="server" OnClick="SaveChanges" Text="Сохранить изменения" CausesValidation="True" ValidationGroup="mainGroup"/>

                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
