﻿using Orders.Backend.Clients.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Orders.Backend.Clients.Web
{
    public partial class AuthTestPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.PreRender += (o, args) =>
            //                      {
            //                          var authService = Services.GetTicketToolAuthService();
            //                          var user = AuthContext.Auth.GetCurrentUser(() => authService.GetCurrentUser(), authService.InnerChannel);
            //                          lblStatus.Text = user.IsAuthtorized
            //                                               ? string.Format("YES name:{0} roles:{1}", user.LoginName, string.Join("|", user.Roles))
            //                                               : "NO";
            //                      };
        }

        protected void lo_Click(object sender, EventArgs e)
        {
            AuthContext.Auth.Logout();
            Refresh_Click(sender, e);
        }

        protected void li_Click(object sender, EventArgs e)
        {
            var authService = Services.GetTicketToolAuthService();
            //AuthContext.Auth.Login(() => authService.LogIn("ManagerR007", "MMDefManager1"), authService.InnerChannel);
            AuthContext.Auth.Login(() => authService.LogIn("admin", "1"), authService.InnerChannel);
            Refresh_Click(sender, e);
        }

        protected void Call_Click(object sender, EventArgs e)
        {
            var dataService = Services.GetTicketToolService();
            try
            {
                //.GetNextTaskById("42")
                //TODO var task = AuthContext.Auth.Execute(() => dataService.GetAllProccess(), dataService.InnerChannel);
                //TODO Label1.Text = task == null ? "null" : task[0].ProcessName;
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
            }
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            var authService = Services.GetTicketToolAuthService();
            var user = AuthContext.Auth.GetCurrentUser(authService.GetCurrentUser, authService.InnerChannel);
            lblStatus.Text = user.IsAuthtorized
                                 ? string.Format("YES name:{0} roles:{1}", user.LoginName, string.Join("|", user.Roles))
                                 : "NO";
        }
    }
}