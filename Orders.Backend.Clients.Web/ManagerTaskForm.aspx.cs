﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using Common.Authentification;
using OnlineOrders.Common;
using Orders.Backend.Clients.Web.Common;
using Orders.Backend.Clients.Web.Common.Configuration;
using Orders.Backend.Clients.Web.Common.OnlineOrder;
using Orders.Backend.Clients.Web.TicketTool;

namespace Orders.Backend.Clients.Web
{
    public partial class ManagerTaskForm : Page
    {
        protected const string PriceFormat = "0.00";

        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        private string _taskId;

        protected string ErrorMessage;

        protected string StatusMessage;

        //task properties
        protected bool TaskExist;

        protected string TicketId;

        protected string TaskName;

        protected string TaskTime;

        protected string TaskDescription;

        protected string OrderId;

        //order properties
        protected string OrderSapCode;

        protected string OrderSource;

        protected bool OrderPaymentShouldBeBold;

        protected string OrderPaymentType;

        protected string OrderStatusInfo;

        protected string OrderUpdateDate;

        protected string OrderWWSOrderId;

        protected string OrderCustomerName;

        protected string OrderCustomerEmail;

        protected string OrderCustomerTelephone;

        protected bool HaveDelivery;

        protected string DeliveryCity;

        protected string DeliveryAddress;

        protected DateTime? RequestedDeliveryDate { get; set; }

        protected string RequestedDeliveryTimeSlot { get; set; }

        protected bool CanEdit;

        protected bool HaveAnyRequiredField;

        protected List<RequiredFieldDto> RequiredFields = new List<RequiredFieldDto>();

        protected List<OurcomeDto> Outcomes = new List<OurcomeDto>();

        protected List<ReserveLineDto> ReserveLines = new List<ReserveLineDto>();

        protected bool ItIsSmartStartOrder { get; set; }

        protected bool ItIsCollectTransfer { get; set; }

        protected bool ItIsTransferOrder { get; set; }

        protected DateTime? DeliveryDate { get; set; }

        protected string СouponСode { get; set; }

        protected List<string> Benefit { get; set; }

       

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            //Response.CacheControl = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();
            _taskId = string.Empty;
            TaskExist = true;
            var smartStartParameterValueStr = Request.QueryString[Settings.SmartStartParameter];
            var transferParameterValueStr = Request.QueryString[Settings.TransferParameter];
            var collectTransferParameterValueStr = Request.QueryString[Settings.CollectTransferParameter];
            ItIsSmartStartOrder = !string.IsNullOrEmpty(smartStartParameterValueStr) && smartStartParameterValueStr=="1";
            ItIsTransferOrder = !string.IsNullOrEmpty(transferParameterValueStr) && transferParameterValueStr == "1";
            ItIsCollectTransfer = !string.IsNullOrEmpty(collectTransferParameterValueStr) && collectTransferParameterValueStr == "1";
            var userName = Request.QueryString[Settings.ManagerUserName];            
            if(Request.QueryString.AllKeys.Contains(Settings.TaskIdParameter))
            {
                _taskId = Request.QueryString[Settings.TaskIdParameter];
                var key1 = Request.QueryString[Settings.KeyParameter];                
                var manager = Request.QueryString[Settings.ManagerParameter];

                var isWrongUrl = !LoginManager(userName, manager, _taskId, key1);

                if(isWrongUrl)
                {                                        
                    TaskExist = false;
                    StatusMessage = "Проблема! Неправильный адрес страницы!";
                    return;
                }
            }
            if(string.IsNullOrEmpty(_taskId))
            {
                TaskExist = false;
                StatusMessage = "Такой задачи нет!";
                return;
            }            
            try
            {
                var ticketService = Services.GetTicketToolService();
                if(!IsPostBack)
                {

                    var result = AuthContext.Auth.Execute(() => ticketService.AssignTaskForMe(_taskId), ticketService.InnerChannel);
                    if(result.HasError)
                    {
                        TaskExist = false;
                        StatusMessage = result.MessageText;
                        return;
                    }
                    var task = result.Result;
                    if(task == null)
                    {
                        TaskExist = false;
                        StatusMessage = "Задачи нет, либо она завершена, либо у вас нет на нее прав!";
                        return;
                    }
                    TicketId = task.TicketId;
                    TaskName = task.Name;
                    var description = task.Description;
                    description = UpdateDescription(description);
                    TaskDescription = description;
                    CanEdit = GetCanEdit(task); //!(task.RelatedContent == null || task.RelatedContent.IsReadOnly);
                    OrderId = task.WorkItemId ?? GetOrderId(task);
                    if(string.IsNullOrEmpty(OrderId))
                    {
                        TaskExist = false;
                        StatusMessage = "Нет заказа у задачи!" + task.TaskId;
                        return;
                    }

                    var client = Services.GetOrderService();
                    var response = client.GerOrderDataByOrderId(OrderId);
                    if(response.DeliveryInfo !=null)
                        DeliveryDate = response.DeliveryInfo.DeliveryDate;
                   
                    OrderSapCode = response.OrderInfo.SapCode;
                    OrderSource = ManagerUIExtension.GetOrderSourceDisplayName(response.OrderInfo.OrderSource);
                    OrderPaymentType = ManagerUIExtension.GetPaymentTypeDisplayName(response.OrderInfo.PaymentType);
                    OrderPaymentShouldBeBold = response.OrderInfo.PaymentType == PaymentType.OnlineCredit;
                    OrderStatusInfo =
                        ManagerUIExtension.GetInternalOrderStatusDisplayName(response.OrderInfo.StatusInfo.Status);
                    var updateDate = TimeZoneInfo.ConvertTimeFromUtc(response.OrderInfo.UpdateDate,
                        TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
                    OrderUpdateDate = updateDate.ToString(DateTimeFormat);
                    OrderWWSOrderId = response.OrderInfo.WWSOrderId;
                    lbOrderCommentLog.Text = response.OrderInfo.Comment;
                    if(response.ClientData != null)
                    {
                        OrderCustomerName = string.Format("{0} {1}", response.ClientData.Name,
                            response.ClientData.Surname);
                        OrderCustomerEmail = response.ClientData.Email;
                        OrderCustomerTelephone = response.ClientData.Phone;
                    }
                    HaveDelivery = response.DeliveryInfo != null && response.DeliveryInfo.HasDelivery;
                    if(HaveDelivery)
                    {
                        DeliveryCity = response.DeliveryInfo.City;
                        DeliveryAddress = response.DeliveryInfo.Address;
                        RequestedDeliveryDate = response.DeliveryInfo.RequestedDeliveryDate;
                        RequestedDeliveryTimeSlot = response.DeliveryInfo.RequestedDeliveryTimeslot;
                    }
                    ReserveLines = FillReserveLines(response);
                    //RequiredFields = OrderStatesExtension.GetTestRequiredFields();
                    RequiredFields = GetRequiredFieldDtos(task.RequiredFields, true);
                    var outcomeNumber = 1;
                    Outcomes = task.Outcomes.Select(x => new OurcomeDto
                    {
                        Number = outcomeNumber++,
                        Name = x.Value,
                        IsSuccess = x.IsSuccess,
                        SkipTaskRequeriedFields = x.SkipTaskRequeriedFields,
                        Fields = GetRequiredFieldDtos(x.RequiredFields, false),
                        FieldsCount = RequiredFields.Count + x.RequiredFields.Count()
                    }).ToList();
                    HaveAnyRequiredField = RequiredFields.Count > 0 || Outcomes.Any(o => o.Fields.Count > 0);
                    if (!String.IsNullOrEmpty(response.OrderInfo.CouponCode))
                    { 
                        СouponСode = response.OrderInfo.CouponCode;
                        var muxClient = new MuxClient();
                        Benefit = muxClient.GetBenefitForCoupon(response.OrderInfo.BasketId);
                    }
                }
                else
                {
                    OrderSapCode = Request.Form["hOrderSapCode"];                    
                    FinishTask(ticketService);
                }
            }
            catch(Exception exp)
            {
                TaskExist = false;
                StatusMessage = "Ошибка!";
                ErrorMessage = exp.ToString();
            }
        }

        private bool GetCanEdit(TicketTask task)
        {
            //TODO Временная мера, пока не будет реализован сервис возвращающий можно ли редактировать текущую задачу
            if(string.IsNullOrEmpty(task.WorkitemPageUrl))
            {
                return true;
            }
            var uri = new Uri(task.WorkitemPageUrl);
            var queryString = HttpUtility.ParseQueryString(uri.Query);
            if(!queryString.HasKeys())
            {
                return true;
            }
            var values = queryString.GetValues("ro");
            return values == null || values.Length <= 0 ||
                   values.All(v => !string.Equals(v, "1", StringComparison.InvariantCultureIgnoreCase));
        }

        private static List<RequiredFieldDto> GetRequiredFieldDtos(IEnumerable<RequiredField> requiredFields,
            bool taskField)
        {
            return requiredFields.Select(y => new RequiredFieldDto
            {
                DefaultValue = y.DefaultValue != null ? y.DefaultValue.ToString() : "",
                Name = y.Name,
                TypeName = y.Type,
                Type = ManagerUIExtension.GetRequiredFieldTypeBySystemType(y.Type),
                Value = y.Value != null ? y.Value.ToString() : "",
                PredefinedValues =
                    (y is RequiredFieldValueList)
                        ? (y as RequiredFieldValueList).PredefinedValuesList.Select(z => z.ToString()).ToList()
                        : new List<string>(),
                TaskField = taskField
            }).ToList();
        }

        private void FinishTask(TicketToolServiceClient ticketService)
        {           
            OrderId = Request.Form["hOrderId"];
            var outcome = Request.Form["hOutcomeName"];
            var outcomeIsSuccessStr = Request.Form["hOutcomeIsSuccess"];
            if(outcome == "CancelOutcome")
            {
                var result = AuthContext.Auth.Execute(() => ticketService.PostponeTask(_taskId, null),
                    ticketService.InnerChannel);
                if(result.HasError)
                {
                    ErrorMessage = result.MessageText;
                }
                StatusMessage = "Задача отложена!";
            }
            else
            {
                bool.TryParse(Request.Form["hCanEdit"], out CanEdit);
                bool outcomeIsSuccess;
                bool.TryParse(outcomeIsSuccessStr, out outcomeIsSuccess);
                var task = AuthContext.Auth.Execute(() => ticketService.GetTask(_taskId), ticketService.InnerChannel);
                if (task == null)
                {
                    StatusMessage = "Проблема! не удается найти задачу.";
                    ErrorMessage = "Попробуйте заново открыть ссылку";
                    TaskExist = false;
                    return;
                }
                if(CanEdit)
                {
                    var articles = GetManagerArticlesFromForm();
                    var sapCode = task.Parameter;
                    var reserveLines = ManagerUIExtension.GetReserveLines(articles, sapCode, ItIsSmartStartOrder||ItIsCollectTransfer);
                    var client = Services.GetOrderService();
                    var lines = reserveLines.ToArray();
                    var operationResult = client.UpdateOrderByEmployee(new UpdateOrderData
                    {
                        OrderId = OrderId,
                        Comment = ManagerUIExtension.GetResultComment(lbOrderCommentLog.Text, tbNewOrderComment.Text, sapCode)
                    }, new ReserveInfo
                    {
                        ReserveLines = lines
                    });
                    if (operationResult.ReturnCode == ReturnCode.Error)
                    {
                        ErrorMessage = "Проблемы с сохранением заказа! " + operationResult.ErrorMessage;
                        TaskExist = false;
                        StatusMessage = "Проблемы с сохранением заказа!";
                        return;
                    }                    
                }    
            
                var reqFields = GetRequiredFieldFromForm();
                
                
                var taskOutcome = task.Outcomes.FirstOrDefault(x => x.Value == outcome);
                if(taskOutcome == null)
                {
                    StatusMessage = "Проблема! Нет такого исхода: " + outcome;
                    ErrorMessage = "";
                    TaskExist = false;
                    return;
                }
                if(taskOutcome.RequiredFields.Count() + task.RequiredFields.Count() != reqFields.Count())
                {
                    StatusMessage = "Проблема с обязательными полями для заполнения!";
                    ErrorMessage = "";
                    TaskExist = false;
                    return;
                }
                var fields = reqFields.Select(f => new RequiredField
                {
                    Name = f.TaskField
                        ? f.Name
                        : string.Format("{0};#{1}", outcome, f.Name),
                    Type = f.TypeName,
                    Value =
                        ManagerUIExtension.GetRequiredFieldValue(f.Value, f.TypeName)
                }).ToArray();                
                foreach(var f in reqFields)
                {
                    if(string.IsNullOrEmpty(f.Value) ||
                       !ManagerUIExtension.ValidateRequiredFieldValue(f.Value, f.TypeName))
                    {
                        StatusMessage = "Проблема со значение обязательного поля!";
                        ErrorMessage = string.Format("Название: '{0}'; 'Тип: {1}'; Значение: '{2}'", f.Name, f.TypeName,
                            f.Value);
                        TaskExist = false;
                        return;
                    }
                }
                var result = AuthContext.Auth.Execute(() => ticketService.CompleteTask(_taskId, outcome, fields),
                    ticketService.InnerChannel);
                if (result.HasError)
                {
                    StatusMessage = "Проблемы с завершением задачи!";
                    ErrorMessage = result.MessageText;
                    TaskExist = false;
                    return;
                }
                StatusMessage = "Задача завершена!";
            }
            TaskExist = false;
        }

        private string GetOrderId(TicketTask task)
        {
            return task.WorkItemId;
        }

        private List<ReserveLineDto> FillReserveLines(GerOrderDataResult response)
        {
            var number = 1;
            long l;
            return response.ReserveInfo.ReserveLines
                .Select(x =>
                {
                    var isService = x.ArticleProductType == ArticleProductType.DeliveryService
                                    || x.ArticleProductType == ArticleProductType.InstallationService
                                    || x.ArticleProductType == ArticleProductType.InstallServiceMarging
                                    || x.ArticleProductType == ArticleProductType.Statistical;
                    return new ReserveLineDto
                    {
                        Number = number++,
                        LineId = x.LineId,
                        ArticleNum = x.ArticleData.ArticleNum,
                        Title = x.Title ?? "",
                        BrandTitle = x.ArticleData.BrandTitle ?? "",
                        Price = x.ArticleData.Price,
                        Qty = x.ArticleData.Qty,
                        ReviewItemState = isService ? ReviewItemState.Reserved : x.ReviewItemState,
                        Comment = "",
                        CommentLog = x.Comment,
                        StockPrice = x.StockPrice,
                        StockQty = x.StockQty,
                        ArticleConditionState = x.StockItemState,
                        ArticleCondition = x.ArticleCondition,
                        Promotions = x.ArticleData.Promotions,
                        WWSDepartmentNumber = x.WWSDepartmentNumber,
                        WWSProductGroupNo = x.WWSProductGroupNumber,
                        WWSProductGroupName = x.WWSProductGroupName,
                        WWSStockNumber = x.WWSStockNumber,
                        WWSFreeQty = x.WWSFreeQty,
                        WWSReserverQuantity = x.WWSReservedQty,
                        WWSPriceOrig = x.WWSPriceOrig,
                        ValidArticleNum = long.TryParse(x.ArticleData.ArticleNum, out l),
                        ValidPrice = x.ArticleData.Price > 0 &&
                                     x.ArticleData.Price == x.WWSPriceOrig,
                        ValidQty = x.ArticleData.Qty > 0 &&
                                   x.ArticleData.Qty <= x.WWSFreeQty,
                        TransferSapCode = x.TransferSapCode,
                        TransferNumber = x.TransferNumber,
                        IsService = isService
                    };
                }).ToList();
        }

        private IEnumerable<RequiredFieldDto> GetRequiredFieldFromForm()
        {
            var fields = new List<RequiredFieldDto>();

            var outcomeNumberStr = Request.Form["hOutcomeNumber"];
            var fieldsCountStr = Request.Form["hOutcomeFieldsCount" + outcomeNumberStr];
            int fieldsCount;
            Int32.TryParse(fieldsCountStr, out fieldsCount);
            for(var i = 1; i <= fieldsCount; i++)
            {
                var fieldType = Request.Form["hOutcomeFieldType" + outcomeNumberStr + "_" + i];
                var fieldTypeName = Request.Form["hOutcomeFieldTypeName" + outcomeNumberStr + "_" + i];
                var fieldName = Request.Form["hOutcomeFieldName" + outcomeNumberStr + "_" + i];
                var fieldValue = Request.Form["hOutcomeFieldValue" + outcomeNumberStr + "_" + i];
                var fieldTaskField =
                    Request.Form["hOutcomeFieldTaskField" + outcomeNumberStr + "_" + i].ToLowerInvariant() == "true";
                var type = ManagerUIExtension.GetRequiredFieldType(fieldType);
                fields.Add(new RequiredFieldDto
                {
                    Name = fieldName,
                    TaskField = fieldTaskField,
                    Type = type.HasValue ? type.Value : RequiredFieldType.String,
                    TypeName = fieldTypeName,
                    Value = fieldValue
                });
            }

            return fields;
        }

        private IEnumerable<ManagerArticle> GetManagerArticlesFromForm()
        {
            var rowsCountStr = Request.Form["hRowsCount"];
            int rowsCount;
            Int32.TryParse(rowsCountStr, out rowsCount);
            var articles = new List<ManagerArticle>();
            for(var number = 1; number <= rowsCount; number++)
            {
                var rowTitle = Request.Form["rowTitle" + number];
                var rowBrandTitle = Request.Form["rowBrandTitle" + number];
                var rowLineIdStr = Request.Form["rowLineId" + number];
                var articleStr = Request.Form["rowArticle" + number];
                var priceStr = Request.Form["rowPrice" + number];
                var qtyStr = Request.Form["rowQty" + number];
                var commentStr = Request.Form["rowComment" + number];
                var rowCommentLog = Request.Form["rowCommentLog" + number];

                var rowState = Request.Form["rowState" + number];

                var rowCorrectPrice = Request.Form["hRowCorrectPrice" + number];
                var rowCorrectQty = Request.Form["hRowCorrectQty" + number];
                var rowAltArticle = Request.Form["hRowAltArticle" + number];
                var rowAltPrice = Request.Form["hRowAltPrice" + number];
                var rowAltQty = Request.Form["hRowAltQty" + number];
                var rowAltDisplayItem = Request.Form["hRowAltDisplayItem" + number];
                var rowAltDisplayItemCondition = Request.Form["hRowAltDisplayItemCondition" + number];
                var rowIsService = Request.Form["rowIsService" + number];

                var rowConditionState = Request.Form["rowConditionState" + number];
                var rowConditionStateComment = Request.Form["rowConditionStateComment" + number];

                var rowTransformSapCode = Request.Form["hRowTransSapCode" + number];
                var rowTransformNumber = Request.Form["hRowTransNumber" + number];

                var WWSDepartmentNo = Request.Form["WWSDepartmentNo" + number];
                var WWSProductGroup = Request.Form["WWSProductGroup" + number];
                var WWSProductGroupNo = Request.Form["WWSProductGroupNo" + number];
                var WWSStockNo = Request.Form["WWSStockNo" + number];
                var WWSFreeQty = Request.Form["WWSFreeQty" + number];
                var WWSReservedQty = Request.Form["WWSReservedQty" + number];
                var WWSPriceOrig = Request.Form["WWSPriceOrig" + number];

                var state = ManagerUIExtension.GetReviewItemState(rowState);
                var conditionState = ManagerUIExtension.GetItemState(rowConditionState);

                var line = new ManagerArticle
                {
                    Title = rowTitle,
                    Number = number,
                    Article = articleStr,
                    Price = decimal.Parse(priceStr),
                    Qty = Int32.Parse(qtyStr),
                    Comment = commentStr,
                    CommentLog = rowCommentLog,
                    RowState = state.HasValue ? state.Value : ReviewItemState.Empty,
                    ConditionState = conditionState.HasValue ? conditionState.Value : ItemState.New,
                    ConditionStateComment = rowConditionStateComment
                };
                long l;
                line.LineId = long.TryParse(rowLineIdStr, out l) ? l : 0;
                decimal d;
                line.RowCorrectPrice = decimal.TryParse(rowCorrectPrice, out d) ? d : 0;
                int i;
                line.RowCorrectQty = Int32.TryParse(rowCorrectQty, out i) ? i : 0;
                line.RowAltArticle = rowAltArticle;
                line.RowAltPrice = decimal.TryParse(rowAltPrice, out d) ? d : 0;
                line.RowAltQty = Int32.TryParse(rowAltQty, out i) ? i : 0;
                line.RowDisplayItem = rowAltDisplayItem == "true";
                line.RowAltDisplayItemCondition = rowAltDisplayItemCondition;

                line.WWSDepartmentNo = Int32.TryParse(WWSDepartmentNo, out i) ? i : 0;
                line.WWSProductGroup = WWSProductGroup;
                line.WWSProductGroupNo = Int32.TryParse(WWSProductGroupNo, out i) ? i : 0;
                line.WWSStockNo = Int32.TryParse(WWSStockNo, out i) ? i : 0;
                line.WWSFreeQty = Int32.TryParse(WWSFreeQty, out i) ? i : 0;
                line.WWSReservedQty = Int32.TryParse(WWSReservedQty, out i) ? i : 0;
                line.WWSPriceOrig = decimal.TryParse(WWSPriceOrig, out d) ? d : 0;

                line.TransferSapCode = rowTransformSapCode;
                line.TransferNumber = int.TryParse(rowTransformNumber, out i) ? i : 0;
                line.IsService = rowIsService == "true";
                if (line.IsService)
                {
                    line.RowState = ReviewItemState.Reserved;
                }

                articles.Add(line);
            }
            return articles;
        }

        private static string UpdateDescription(string description)
        {
            const string startPreffix = "<!--mediamarkt.description.start-->";
            const string endPreffix = "<!--mediamarkt.description.end-->";
            var s1 = description.IndexOf(startPreffix, StringComparison.Ordinal);
            var e1 = description.IndexOf(endPreffix, StringComparison.Ordinal);
            if(s1 < e1 && s1 > 0)
            {
                var text = description.Substring(s1 + startPreffix.Length, e1 - s1 - startPreffix.Length);
                return text;
            }
            return description;
        }

        private bool LoginManager(string userName, string userToken, string taskId, string taskToken)
        {
            try
            {
                var authService = Services.GetTicketToolAuthService();
                return AuthContext.Auth.Login(
                        () => authService.LoginByToken(userName, userToken, taskId, taskToken), authService.InnerChannel);     
            }
            catch (FaultException ex)
            {
                //TODO Log
            }
            catch(Exception)
            {
                //TODO Log
            }
            return false;
        }
    }
}