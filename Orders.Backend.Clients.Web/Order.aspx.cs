﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Orders.Backend.Clients.Web.Common.Configuration;

namespace Orders.Backend.Clients.Web
{
    public partial class Order : Page
    {
        private string _orderId;        
        /*
        protected void Page_Load(object sender, EventArgs e)
        {
            _orderId = "";
            if (Request.QueryString.AllKeys.Contains(Settings.OrderIdParameter))
            {
                _orderId = Request.QueryString[Settings.OrderIdParameter];
            }
            if(!IsPostBack)
            {
                lError.Visible = false;
                FillDropDownList(ddlItemState);
                try
                {
                    var client = Services.GetOrderService();

                    var response = client.GerOrderDataByOrderId(_orderId);


                    lSAPCode.Text = response.OrderInfo.SapCode;
                    lOrderId.Text = response.OrderInfo.OrderId;
                    lOrderSource.Text = response.OrderInfo.OrderSource.ToString();
                    lPaymentType.Text = response.OrderInfo.PaymentType.ToString();
                    lStatusInfoStatus.Text = response.OrderInfo.StatusInfo.Status.ToString();
                    lStatusInfoText.Text = response.OrderInfo.StatusInfo.Text;

                    lUpdateDate.Text = response.OrderInfo.UpdateDate.ToString();
                    lWWSOrderId.Text = response.OrderInfo.WWSOrderId;

                    

                    lvLines.DataSource = response.ReserveInfo.ReserveLines;
                    lvLines.DataBind();

                }
                catch (Exception exp)
                {

                }
            }
        }

        protected void FeatureBound(object sender, ListViewItemEventArgs e)
        {
            var tbPrice = e.Item.FindControl("tbPrice") as TextBox;
            var tbQty = e.Item.FindControl("tbQty") as TextBox;
            var ddlItemState = e.Item.FindControl("ddlItemState") as DropDownList;
            var ad = ((ReserveLine)e.Item.DataItem);
            tbPrice.Text = "" + ad.ArticleData.Price;
            tbQty.Text = "" + ad.ArticleData.Qty;
            FillDropDownList(ddlItemState);
            ddlItemState.SelectedValue = "" + (int) ad.ItemState;
        }

        private static void FillDropDownList(DropDownList ddlItemState)
        {
            ddlItemState.Items.Add(new ListItem(ItemState.InStock.ToString(), "" + (int) ItemState.InStock));
            ddlItemState.Items.Add(new ListItem(ItemState.NotForSale.ToString(), "" + (int) ItemState.NotForSale));
            ddlItemState.Items.Add(new ListItem(ItemState.WasInUse.ToString(), "" + (int) ItemState.WasInUse));
            ddlItemState.Items.Add(new ListItem(ItemState.OutOfStock.ToString(), "" + (int) ItemState.OutOfStock));
        }

        protected void AddLineClick(object sender, EventArgs e)
        {
            var lines = GetReserveLinesFromControls(false);
            if(lines == null)
            {
                lError.Visible = true;
                return;
            }
            var client = Services.GetOrderService();

            var newLine = GetNewReserveLineFromControls();
            if(newLine != null)
            {
                lines.Add(newLine);
                client.UpdateReserveDataForOrder(_orderId, lines.ToArray());

                lvLines.DataSource = lines;
                lvLines.DataBind();
            } else
            {
                lError.Visible = true;
                return;
            }
            tbNewArticle.Text = "";
            tbNewPrice.Text = "";
            tbNewQty.Text = "";
            ddlItemState.SelectedValue = "" + (int) ItemState.InStock;

            lError.Visible = false;
        }

        private ReserveLine GetNewReserveLineFromControls()
        {
            string articleStr = tbNewArticle.Text;
            string priceStr = tbNewPrice.Text;
            string qtyStr = tbNewQty.Text;
            decimal price;
            decimal.TryParse(priceStr, out price);
            int qty;
            Int32.TryParse(qtyStr, out qty);
            if(string.IsNullOrEmpty(articleStr) || price <= 0 || qty <= 0)
            {
                return null;
            }
            var newLine = new ReserveLine {ArticleData = new ArticleData {ArticleNum = articleStr}};
            newLine.ArticleData.Price = price;
            newLine.ArticleData.Qty = qty;
            newLine.ItemState = (ItemState) Enum.ToObject(typeof (ItemState), Int32.Parse(ddlItemState.SelectedValue));
            return newLine;
        }

        private List<ReserveLine> GetReserveLinesFromControls(bool onlyNotSelected)
        {
            var lines = new List<ReserveLine>();
            foreach (var item in lvLines.Items)
            {
                var selectCheckBox = (CheckBox) item.FindControl("selectCheckBox");
                if(onlyNotSelected && selectCheckBox.Checked)
                {
                    continue;
                }
                var tbPrice = (TextBox) item.FindControl("tbPrice");
                var tbQty = (TextBox) item.FindControl("tbQty");
                var ddl = (DropDownList) item.FindControl("ddlItemState");
                var hfArticleNum = (HiddenField) item.FindControl("hfArticleNum");
                decimal pr;
                if(!decimal.TryParse(tbPrice.Text, out pr))
                {
                    return null;
                }
                int q;
                if(!Int32.TryParse(tbQty.Text, out q))
                {
                    return null;
                }
                if(string.IsNullOrEmpty(hfArticleNum.Value))
                {
                    return null;
                }

                var line = new ReserveLine {ArticleData = new ArticleData {ArticleNum = hfArticleNum.Value}};

                line.ArticleData.Price = pr;
                line.ArticleData.Qty = q;
                line.ItemState = (ItemState) Enum.ToObject(typeof (ItemState), Int32.Parse(ddl.SelectedValue));
                lines.Add(line);
            }
            return lines;
        }

        protected void SaveChanges(object sender, EventArgs e)
        {
            var lines = GetReserveLinesFromControls(false);
            if (lines == null)
            {
                lError.Visible = true;
                return;
            }
            var client = Services.GetOrderService();
            client.UpdateReserveDataForOrder(_orderId, lines.ToArray());

            lvLines.DataSource = lines;
            lvLines.DataBind();
            lError.Visible = false;
        }

        protected void DeleteSelectedLines(object sender, EventArgs e)
        {
            var lines = GetReserveLinesFromControls(true);
            if (lines == null)
            {
                lError.Visible = true;
                return;
            }
            var client = Services.GetOrderService();
            client.UpdateReserveDataForOrder(_orderId, lines.ToArray());

            lvLines.DataSource = lines;
            lvLines.DataBind();
            lError.Visible = false;
        }*/
    }
}