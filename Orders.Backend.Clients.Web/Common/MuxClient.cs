﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Multiplexor.Client;

namespace Orders.Backend.Clients.Web.Common
{
    public class MuxClient
    {
        public const String FreeDeliveryCoupon = "Бесплатная доставка";

        public List<String> GetBenefitForCoupon(String basketId)
        {
            var result = new List<String>();
            try
            {
                var multiplexorClient = new MultiplexorClient(ManagerFormSettings.Default.MultiplexorUrl, ManagerFormSettings.Default.MultiplexorApiKey, ManagerFormSettings.Default.MultiplexorTimeout);
                multiplexorClient.Basket.GetBasket(basketId);
                multiplexorClient.Execute();
                var executionCommandResults = multiplexorClient.ExecutionCommandResults;
                var requestResult = executionCommandResults[0].Result;
                if (requestResult.Errors == null)
                {
                    var item = requestResult.Result as BasketApi.BasketResponseTO;
                    if (item != null && (item.FreeDelivery.HasValue || item.TotalDiscount.HasValue))
                    {
                        if(item.FreeDelivery.HasValue && item.FreeDelivery.Value)
                            result.Add(FreeDeliveryCoupon);
                        if (item.PromoMessages != null && item.PromoMessages.BasketBenefits.Any())
                        {
                            //var strResult = item.PromoMessages.BasketBenefits.Aggregate(String.Empty, (current, message) => current + (" " + message));
                            result.AddRange(item.PromoMessages.BasketBenefits);
                        }
                    }
                }
            }
            catch (Exception)
            {
            } 

            return result;
        }
    }
}