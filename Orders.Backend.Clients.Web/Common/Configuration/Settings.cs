﻿using System;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.Web;

namespace Orders.Backend.Clients.Web.Common.Configuration
{
    public static class Settings
    {              
        public static string TaskIdParameter
        {
            get { return ConfigurationManager.AppSettings["TaskIdParameter"]; }
        }

        public static string KeyParameter
        {
            get { return ConfigurationManager.AppSettings["KeyParameter"]; }
        }

        public static string SapCodeParameter
        {
            get { return ConfigurationManager.AppSettings["SapCodeParameter"]; }
        }

        public static string ManagerParameter
        {
            get { return ConfigurationManager.AppSettings["ManagerParameter"]; }
        }

        public static string ManagerUserName
        {
            get { return ConfigurationManager.AppSettings["ManagerUserName"]; }
        }

        public static string SmartStartParameter
        {
            get { return ConfigurationManager.AppSettings["SmartStartParameter"]; }
        }

        public static string TransferParameter
        {
            get { return ConfigurationManager.AppSettings["TransferParameter"]; }
        }

        public static string CollectTransferParameter
        {
            get { return "CollectTransfer"; }
        }

        public static string OrderMarkerParam
        {
            get { return ConfigurationManager.AppSettings["OrderMarkerParam"]; }
        }

        public static string OrderIdParameter
        {
            get { return ConfigurationManager.AppSettings["OrderIdParameter"]; }
        }
    }
}