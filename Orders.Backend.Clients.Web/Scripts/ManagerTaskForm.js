﻿function updateInput(number, smart, value, collectTransfer, transferSapCode,transferNumber) {
    smart = smart === 'True';
    debugger;
    collectTransfer = collectTransfer === 'True';
    if (collectTransfer) {
        return;
    }
    window.currentRow = number;    
    //document.getElementById("rowArticle" + number).value = value;
    if (value == window.NotFoundState && !smart && !collectTransfer) {
        document.getElementById("modalNotFoundCurrentArticle").innerHTML =
            "Предложите альтернативу для артикула " + document.getElementById("rowArticle" + currentRow).value;
        $('#modalNotFound').modal({
            keyboard: false,
            backdrop: "static"
        });
    }
    else if (value == window.IncorrectPriceState) {
        var correctPrice = document.getElementById("modalIncorrectPriceCorrectPrice").value;
        document.getElementById("modalIncorrectPriceCurrentArticle").innerHTML =
            "Введите правильную цену для артикула " + document.getElementById("rowArticle" + currentRow).value;
        document.getElementById("modalIncorrectPriceCurrentPrice").innerHTML =
            "цена сейчас " + document.getElementById("rowPrice" + currentRow).value + ".00";
        $('#modalIncorrectPrice').modal({
            keyboard: false,
            backdrop: "static",
        });
    }
    else if (value == window.ReservedPartState) {

        var currentQty = parseInt(document.getElementById("rowQty" + currentRow).value);
        if (currentQty <= 1) {
            document.getElementById("rowState" + currentRow).value = window.EmptyState;
            bootbox.alert('Нельзя резервировать частично товар с таким кол-вом!', function () { });
            return;
        }

        document.getElementById("modalReservedPartCurrentArticle").innerHTML =
            "Введите правильное кол-во и предложите альтернативу для артикула " + document.getElementById("rowArticle" + currentRow).value;
        document.getElementById("modalReservedPartCurrentQty").innerHTML =
            "кол-во сейчас " + document.getElementById("rowQty" + currentRow).value;
        
        $('#modalReservedPart').modal({
            keyboard: false,
            backdrop: "static",
        });
    }
    else if (value == window.ReservedDisplayItemState) {
        document.getElementById("modalReservedDisplayItemCurrentArticle").innerHTML =
            "Введите правильное кол-во, предложите альтернативу, опишите состояние витринного экземпляра для артикула " + document.getElementById("rowArticle" + currentRow).value;
        document.getElementById("modalReservedDisplayItemCurrentQty").innerHTML =
            "кол-во сейчас " + document.getElementById("rowQty" + currentRow).value;
        document.getElementById("modalReservedDisplayItemCurrentPrice").innerHTML =
            "цена сейчас " + document.getElementById("rowPrice" + currentRow).value + ".00";
        
        $('#modalReservedDisplayItem').modal({
            keyboard: false,
            backdrop: "static",
        });
    }
    else if (value == window.TransferState && !collectTransfer) {
        document.getElementById("modalTransferItemCurrentArticle").innerHTML =
            "Введите SapCode и номер трансфера для артикула " + document.getElementById("rowArticle" + currentRow).value;        

        $('#modalTransferItem').modal({
            keyboard: false,
            backdrop: "static",
        });
    }
    else if (value == window.ReservedState && !collectTransfer) {
        ChangeRowValues('', '', '', '', '', '', false, '', '', '', '');
        toggle(window.ReservedState, currentRow, false);
    }
    else if (value == window.DisplayItemState && collectTransfer) {
        toggle(window.DisplayItemState, currentRow, false, true);
    }
    else if (value == window.TransferState && collectTransfer) {
        toggle(window.TransferState, currentRow, false, true);
    }
    else if (value == window.NotFoundState && collectTransfer) {
        toggle(window.NotFoundState, currentRow, false, true);
    } else if (value == window.DisplayItemState) {
        ChangeRowValues('', '', '', '', '', '', false, '', '', '', '');
        toggle(window.DisplayItemState, currentRow, false);
    } else if (value == window.EmptyState && collectTransfer) {
        
    } else if (value == window.TooLowPriceState) {
        ChangeRowValues('', '', '', '', '', '', false, '', '', '', '');
        toggle(window.TooLowPriceState, currentRow, false);
    } else if (value == window.EmptyState) {
        ChangeRowValues('', '', '', '', '', '', false, '', '', '', '');
        toggle(window.EmptyState, currentRow, false);
    } else {
        ChangeRowValues('', '', '', '', '', '', false, '', '', '', '');
        toggle(window.EmptyState, currentRow, false);
    }
}

function DisableAllButtons(disable) {
    $('.btn').each(function () {
        if (disable) {
            $(this).attr('disabled', 'disabled');
            $(this).addClass("disabled");
        } else {
            $(this).removeAttr('disabled');
            $(this).removeClass("disabled");
        }
    });
}

function checkform() {
    if (document.getElementById("hOutcomeName").value == 'CancelOutcome') {
        DisableAllButtons(true);
        return true;
    }
    var skip = document.getElementById("hOutcomeSkipTaskRequeriedFields").value;
    var skipTaskRequeriedFields = skip == "true" || skip == "True";
    skipTaskRequeriedFields = false;
    if (//  && document.getElementById("hOutcomeIsSuccess").value == "true"
        (document.getElementById("hCanEdit").value == "true" || document.getElementById("hCanEdit").value == "True")) {
        for (var i = 0; i < window.rowsCount; i++) {
            var n = i + 1;
            var article = document.getElementById("rowArticle" + n).value;
            var state = document.getElementById("rowState" + n).value;
            if (state == null || state == '' || state == window.EmptyState) {
                bootbox.alert('Не заполнен ответ менеджера для артикула ' + article, function () { });
                //alert('Не заполнен ответ менеджера для артикула ' + article);
                return false;
            }
            var conditionState = document.getElementById("rowConditionState" + n).value;
            var conditionStateComment = document.getElementById("rowConditionStateComment" + n).value;
            if (conditionState != null && conditionState != '' && conditionState != window.ArticleConditionNewState
                && (conditionStateComment == null || conditionStateComment == '')) {
                bootbox.alert('Заполните состояние для артикула ' + article, function () { });
                return false;
            }
        }
    }
    var outcomeNumber = document.getElementById("hOutcomeNumber").value;
    var fieldsCount = document.getElementById("hOutcomeFieldsCount" + outcomeNumber).value;
    for (var j = 1; j < parseInt(fieldsCount) + 1; j++) {
        var fieldName = document.getElementById("hOutcomeFieldName" + outcomeNumber + "_" + j).value;
        var fieldType = document.getElementById("hOutcomeFieldType" + outcomeNumber + "_" + j).value;
        var value = '';
        if (fieldType == "TimeSpan") {
            var value1 = document.getElementById("rf_d_" + outcomeNumber + "_" + j).value;
            var value2 = document.getElementById("rf_h_" + outcomeNumber + "_" + j).value;
            var value3 = document.getElementById("rf_m_" + outcomeNumber + "_" + j).value;
            value = value1 + "." + value2 + ":" + value3 + ":00";
            if (value == '0.0:0:00') {
                bootbox.alert('Заполните обязательное поле "' + fieldName + '"', function () { });
                return false;
            }
        } else if (fieldType == "Boolean") {
            value = document.getElementById("rf_" + outcomeNumber + "_" + j).checked;
        } else if (fieldType == "Int32") {
            value = document.getElementById("rf_" + outcomeNumber + "_" + j).value;
            if (!skipTaskRequeriedFields) {
                if (value == null || value == '') {
                    bootbox.alert('Заполните обязательное поле "' + fieldName + '"', function () { });
                    return false;
                } else if (!IsStringNumber(value)) {
                    bootbox.alert('Поле "' + fieldName + '" должно быть целым числом!', function () { });
                    return false;
                }
            }
        } else if (fieldType == "Decimal") {
            value = document.getElementById("rf_" + outcomeNumber + "_" + j).value;
            if (!skipTaskRequeriedFields) {
                if (value == null || value == '') {
                    bootbox.alert('Заполните обязательное поле "' + fieldName + '"', function () { });
                    return false;
                } else if (!IsStringDecimal(value)) {
                    bootbox.alert('Поле "' + fieldName + '" должно быть числом!', function () { });
                    return false;
                }
            }
        } else if (fieldType == "Double") {
            value = document.getElementById("rf_" + outcomeNumber + "_" + j).value;
            if (!skipTaskRequeriedFields) {
                if (value == null || value == '') {
                    bootbox.alert('Заполните обязательное поле "' + fieldName + '"', function () { });
                    return false;
                } else if (!IsStringDecimal(value)) {
                    bootbox.alert('Поле "' + fieldName + '" должно быть числом!', function () { });
                    return false;
                }
            }
        } else if (fieldType == "DateTime") {
            value = document.getElementById("rf_" + outcomeNumber + "_" + j).value;
            if (!skipTaskRequeriedFields) {
                if (value == null || value == '' || !isDate(value)) {
                    bootbox.alert('Введите дату "' + fieldName + '" в формате "2012-06-24"', function () { });
                    return false;
                }
            }
        }  else {
            value = document.getElementById("rf_" + outcomeNumber + "_" + j).value;
            if (!skipTaskRequeriedFields) {
                if (value == null || value == '') {
                    bootbox.alert('Заполните обязательное поле "' + fieldName + '"', function () { });
                    return false;
                }
            }
        }
        document.getElementById("hOutcomeFieldValue" + outcomeNumber + "_" + j).value = value;
    }
    DisableAllButtons(true);
    return true;
}

function saveOutcome(value, isSuccess, skipTaskRequeriedFields, outcomeNumber) {
    document.getElementById("hOutcomeName").value = value;
    document.getElementById("hOutcomeNumber").value = outcomeNumber;
    document.getElementById("hOutcomeIsSuccess").value = isSuccess;
    document.getElementById("hOutcomeSkipTaskRequeriedFields").value = skipTaskRequeriedFields;
    document.getElementById("hRowsCount").value = window.rowsCount;
}

function ChangeRowValues(correctPrice, correctQty, altArticle, altPrice, altQty, comment, isDisplayItem, displayItemConditionComment,
    conditionStateComment, transferSapCode, transferNumber) {
    document.getElementById("rowCorrectPrice" + currentRow).innerHTML = correctPrice;
    document.getElementById("hRowCorrectPrice" + currentRow).value = correctPrice;

    document.getElementById("rowCorrectQty" + currentRow).innerHTML = correctQty;
    document.getElementById("hRowCorrectQty" + currentRow).value = correctQty;

    document.getElementById("rowAltArticle" + currentRow).innerHTML = altArticle;
    document.getElementById("hRowAltArticle" + currentRow).value = altArticle;

    document.getElementById("rowAltPrice" + currentRow).innerHTML = altPrice;
    document.getElementById("hRowAltPrice" + currentRow).value = altPrice;

    document.getElementById("rowAltQty" + currentRow).innerHTML = altQty;
    document.getElementById("hRowAltQty" + currentRow).value = altQty;
    
    document.getElementById("rowAltDisplayItemCondition" + currentRow).innerHTML = displayItemConditionComment;
    document.getElementById("hRowAltDisplayItemCondition" + currentRow).value = displayItemConditionComment;

    document.getElementById("rowTransSapCode" + currentRow).innerHTML = transferSapCode;
    document.getElementById("hRowTransSapCode" + currentRow).value = transferSapCode;

    document.getElementById("rowTransNumber" + currentRow).innerHTML = transferNumber;
    document.getElementById("hRowTransNumber" + currentRow).value = transferNumber;

    var di;
    if (altArticle == null || altArticle == "") {
        di = "";
    } else if (isDisplayItem) {
        di = "Витринный экземпляр";
    } else {
        di = "Склад";
    }    
    document.getElementById("rowAltDisplayItem" + currentRow).innerHTML = di;
    document.getElementById("hRowAltDisplayItem" + currentRow).value = isDisplayItem;
    
    document.getElementById("rowComment" + currentRow).innerHTML = comment;

    document.getElementById("rowConditionStateComment" + currentRow).value = conditionStateComment;
}


function closeModalNotFound(closeOnly) {
    if (closeOnly) {
        document.getElementById("rowState" + currentRow).value = window.EmptyState;
    }
    var article = document.getElementById("modalNotFoundAltArticle").value;
    var price = document.getElementById("modalNotFoundAltPrice").value;
    var qty = document.getElementById("modalNotFoundAltQty").value;
    var comment = document.getElementById("modalNotFoundComment").value;
    var isDisplayItem = document.getElementById("modalNotFoundAltDisplayItem").checked;
    var displayItemCondition = document.getElementById("modalNotFoundAltDisplayItemCondition").value;
    if (!closeOnly && isDisplayItem &&
        (displayItemCondition == null || displayItemCondition == '')) {
        bootbox.alert("Заполни состояние витринного экземпляра!", function () { });
        return;
    }
    if (!closeOnly && (article == null || article == '' || parseInt(article) <= 0
        || price == null || price == '' || parseInt(price) <= 0
        || qty == null || qty == '' || parseInt(qty) <= 0)
        ) {
        
        if ((article == null || article == '')
            && (price == null || price == '')
            && (qty == null || qty == '')) {
            $('#modalNotFound').modal('hide');
            clearNotFoundForm();
        } else {
            bootbox.alert("Заполни поля!", function () {});
         //   alert('Заполни поля!');
        }
    } else {
        $('#modalNotFound').modal('hide');
        clearNotFoundForm();
    }
}

function clearNotFoundForm() {
    document.getElementById("modalNotFoundAltArticle").value = '';
    document.getElementById("modalNotFoundAltPrice").value = '';
    document.getElementById("modalNotFoundAltQty").value = '';
    document.getElementById("modalNotFoundComment").value = '';
    document.getElementById("modalNotFoundAltDisplayItem").checked = false;
    document.getElementById("modalNotFoundAltDisplayItemCondition").value = '';
    toggleRow('modalNotFoundAltDisplayItemConditionTr', false);
}

function closeModalIncorrectPrice(closeOnly) {
    if (closeOnly) {
        document.getElementById("rowState" + currentRow).value = window.EmptyState;
    }    
    var correctPrice = document.getElementById("modalIncorrectPriceCorrectPrice").value;
    var comment = document.getElementById("modalIncorrectPriceComment").value;
    if (!closeOnly && (correctPrice == null || correctPrice == '' || !IsStringNumber(correctPrice))) {
        bootbox.alert("Заполни поля!", function () { });
        //alert('Заполни поля!');
    } else {
        $('#modalIncorrectPrice').modal('hide');
        clearIncorrectPriceForm();
    }
}

function clearIncorrectPriceForm() {
    document.getElementById("modalIncorrectPriceCorrectPrice").value = '';
    document.getElementById("modalIncorrectPriceComment").value = '';
}

function closeModalReservedPart(closeOnly) {
    if (closeOnly) {
        document.getElementById("rowState" + currentRow).value = window.EmptyState;
    }
    var currentQty = parseInt(document.getElementById("rowQty" + currentRow).value);
    var correctQty = document.getElementById("modalReservedPartCorrectQty").value;
    var article = document.getElementById("modalReservedPartAltArticle").value;
    var price = document.getElementById("modalReservedPartAltPrice").value;
    var qty = document.getElementById("modalReservedPartAltQty").value;
    var comment = document.getElementById("modalReservedPartComment").value;
    var isDisplayItem = document.getElementById("modalReservedPartAltDisplayItem").checked;
    var displayItemCondition = document.getElementById("modalReservedPartAltDisplayItemCondition").value;
    if (!closeOnly && isDisplayItem &&
        (displayItemCondition == null || displayItemCondition == '')) {
        bootbox.alert("Заполни состояние витринного экземпляра!", function () { });
        return;
    }
    if (!closeOnly && (correctQty == null || correctQty == '' || !IsStringNumber(correctQty)
        || article == null || article == '' || !IsStringNumber(article)
        || price == null || price == '' || !IsStringNumber(price)
        || qty == null || qty == '' || !IsStringNumber(qty))) {
        if ((article == null || article == '')
            && (price == null || price == '')
            && (qty == null || qty == '')
            && IsStringNumber(correctQty)) {
            if (parseInt(correctQty) < currentQty) {
                $('#modalReservedPart').modal('hide');
                clearReservedPartForm();
            } else {
                bootbox.alert("Новое кол-во должно быть меньше текущего!", function () { });
            }
        } else {
            bootbox.alert("Заполни поля!", function () { });
        }
    } else {
        if (closeOnly || parseInt(correctQty) < currentQty) {
            $('#modalReservedPart').modal('hide');
            clearReservedPartForm();
        } else {
            bootbox.alert("Новое кол-во должно быть меньше текущего!", function () { });
        }
    }
}

function clearReservedPartForm() {
    document.getElementById("modalReservedPartCorrectQty").value = '';
    document.getElementById("modalReservedPartAltArticle").value = '';
    document.getElementById("modalReservedPartAltPrice").value = '';
    document.getElementById("modalReservedPartAltQty").value = '';
    document.getElementById("modalReservedPartComment").value = '';
    document.getElementById("modalReservedPartAltDisplayItem").checked = false;
    document.getElementById("modalReservedPartAltDisplayItemCondition").value = '';
    toggleRow('modalReservedPartAltDisplayItemConditionTr', false);
}

function closeModalReservedDisplayItem(closeOnly) {
    if (closeOnly) {
        document.getElementById("rowState" + currentRow).value = window.EmptyState;
    }
    var conditionComment = document.getElementById("modalReservedDisplayItemRowConditionStateComment").value;
    var correctPrice = document.getElementById("modalReservedDisplayItemCorrectPrice").value;
    
    var currentQty = parseInt(document.getElementById("rowQty" + currentRow).value);
    
    var correctQty = document.getElementById("modalReservedDisplayItemCorrectQty").value;
    var article = document.getElementById("modalReservedDisplayItemAltArticle").value;
    var price = document.getElementById("modalReservedDisplayItemAltPrice").value;
    var qty = document.getElementById("modalReservedDisplayItemAltQty").value;
    var comment = document.getElementById("modalReservedDisplayItemComment").value;
    var isDisplayItem = document.getElementById("modalReservedDisplayItemAltDisplayItem").checked;
    var displayItemCondition = document.getElementById("modalReservedDisplayItemAltDisplayItemCondition").value;
    if (!closeOnly && isDisplayItem &&
        (displayItemCondition == null || displayItemCondition == '')) {
        bootbox.alert("Заполни состояние витринной альтернативы!", function () { });
        return;
    }
    if (!closeOnly && (conditionComment == null || conditionComment == ''
        || correctPrice == null || correctPrice == '' || !IsStringNumber(correctPrice)
        || correctQty == null || correctQty == '' || !IsStringNumber(correctQty)
        || (article == null || article == '' || !IsStringNumber(article)
        || price == null || price == '' || !IsStringNumber(price)
        || qty == null || qty == '' || !IsStringNumber(qty))
    )) {
        if ((article == null || article == '')
            && (price == null || price == '')
            && (qty == null || qty == '')
            && IsStringNumber(correctQty)
            && IsStringNumber(correctPrice)
            && conditionComment != null && conditionComment != '') {
            if (parseInt(correctQty) <= currentQty) {
                $('#modalReservedDisplayItem').modal('hide');
                clearReservedDisplayItemForm();
            } else {
                bootbox.alert("Новое кол-во должно быть меньше либо равно текущему!", function () { });
            }
        } else {
            bootbox.alert("Заполни поля!", function () { });
        }
    } else {
        if (closeOnly || parseInt(correctQty) <= currentQty) {
            $('#modalReservedDisplayItem').modal('hide');
            clearReservedDisplayItemForm();
        } else {
            bootbox.alert("Новое кол-во должно быть меньше либо равно текущему!", function () { });
        }
    }
}

function clearReservedDisplayItemForm() {
    document.getElementById("modalReservedDisplayItemRowConditionStateComment").value = '';
    document.getElementById("modalReservedDisplayItemCorrectPrice").value = '';
    
    document.getElementById("modalReservedDisplayItemCorrectQty").value = '';
    document.getElementById("modalReservedDisplayItemAltArticle").value = '';
    document.getElementById("modalReservedDisplayItemAltPrice").value = '';
    document.getElementById("modalReservedDisplayItemAltQty").value = '';
    document.getElementById("modalReservedDisplayItemComment").value = '';
    document.getElementById("modalReservedDisplayItemAltDisplayItem").checked = false;
    document.getElementById("modalReservedDisplayItemAltDisplayItemCondition").value = '';
    toggleRow('modalReservedDisplayItemAltDisplayItemTr', false);
}

function closeModalTransferItem(closeOnly) {
    if (closeOnly) {
        clearModalTransferItem();
        document.getElementById("rowState" + currentRow).value = window.EmptyState;
        $('#modalTransferItem').modal('hide');
        return;
    }
    var transferSapCode = document.getElementById("modalTransferItemRowTransferSapCode").value;
    var transferNumber = document.getElementById("modalTransferItemRowTransferNumber").value;
    if (!closeOnly) {
        if (transferSapCode == '') {
            bootbox.alert("Заполни SapCode магазина!", function () { });
            return false;
        }
        if (transferNumber == '') {
            bootbox.alert("Заполни номер трансфера!", function () { });
            return false;
        }
        var rxSapCodePattern = /^R\d{3}$/;
        if (!rxSapCodePattern.test(transferSapCode)) {
            bootbox.alert('Поле "' + 'Магазин из которого запрошен трансфер' + '" - неправильный формат ( Пример R221 )!', function () { });
            return false;
        }
        if (!IsStringNumber(transferNumber)) {
            bootbox.alert('Поле "' + 'Номер трансфера' + '" должно быть целым числом!', function () { });
            return false;
        }
    }
    $('#modalTransferItem').modal('hide');
}

function clearModalTransferItem() {
    document.getElementById("modalTransferItemRowTransferSapCode").value = '';
    document.getElementById("modalTransferItemRowTransferNumber").value = '';
}

function IsStringNumber(numberStr) {
    var i = parseInt(numberStr,10);
    return i > 0 && i.toString() == numberStr;
}

function IsStringDecimal(numberStr) {
    var i = parseFloat(numberStr);
    return i > 0 && i.toString() == numberStr;
}

function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;
    
    var date = moment(currVal);
    return date.isValid();
    //Declare Regex  
    var rxDatePattern = /^(\d{4})(-)(\d{1,2})(-)(\d{1,2})$/;
    if (!date.isValid()) {
        return false;
    }
    debugger;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    //dtMonth = dtArray[1];
    //dtDay = dtArray[3];
    //dtYear = dtArray[5];
    
    //Checks for yyyy-mm-dd format.
    dtYear = dtArray[1];
    dtMonth = dtArray[3];
    dtDay = dtArray[5];
    

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}

function toggle(type, number, haveAltArticle, showTransRow) {
    var showCorrectPrice = false;
    var showCorrectQty = false;
    var showAltRow = false;
    var showComment = true;
    var showConditionState = false;
    showTransRow = showTransRow ? showTransRow : false;

    if (type == window.NotFoundState) {
        showAltRow = true;
    }
    else if (type == window.IncorrectPriceState) {
        showCorrectPrice = true;
    }
    else if (type == window.ReservedPartState) {
        showCorrectQty = true;
        showAltRow = true;
    }
    else if (type == window.ReservedDisplayItemState) {
        showCorrectPrice = true;
        showCorrectQty = true;
        showAltRow = true;
        showConditionState = true;
    }
    else if (type == window.EmptyState) {
        showComment = false;
    }
    else if (type == window.ReservedState) {
        showComment = false;
    }
    else if (type == window.TransferState) {
        showTransRow = true;
        showComment = false;
    }
    else {
        showComment = false;
    }

    toggleRow("rowCorrectTr" + number, showCorrectPrice || showCorrectQty);
    toggleRow("rowCorrectPriceTr" + number, showCorrectPrice);
    toggleRow("rowCorrectQtyTr" + number, showCorrectQty);
    toggleRow("rowAltTr1" + number, showAltRow && haveAltArticle);
    toggleRow("rowAltTr2" + number, showAltRow && haveAltArticle);
    toggleRow("rowCommentTr" + number, showComment);
    toggleRow("rowConditionStateTr" + number, showConditionState);
    toggleRow("rowTransTr1" + number, showTransRow);
}

function toggleRow(rowId, show) {
    if (show) {
        document.getElementById(rowId).style.display = '';
    } else {
        document.getElementById(rowId).style.display = 'none';
    }
}


function initRowsEvents() {
    $('#modalNotFound').on('hide', function () {
        var article = document.getElementById("modalNotFoundAltArticle").value;
        var price = document.getElementById("modalNotFoundAltPrice").value;
        var qty = document.getElementById("modalNotFoundAltQty").value;
        var comment = document.getElementById("modalNotFoundComment").value;
        var isDisplayItem = document.getElementById("modalNotFoundAltDisplayItem").checked;
        
        var displayItemCondition = document.getElementById("modalNotFoundAltDisplayItemCondition").value;

        ChangeRowValues("", "", article, price, qty, comment, isDisplayItem, displayItemCondition, '');

        var rowState = document.getElementById("rowState" + currentRow).value;
        toggle(rowState, currentRow, IsStringNumber(article));
    });

    $('#modalIncorrectPrice').on('hide', function () {
        var correctPrice = document.getElementById("modalIncorrectPriceCorrectPrice").value;
        var comment = document.getElementById("modalIncorrectPriceComment").value;
        ChangeRowValues(correctPrice, "", "", "", "", comment, false, '', '');

        var rowState = document.getElementById("rowState" + currentRow).value;
        toggle(rowState, currentRow, false);
    });

    $('#modalReservedPart').on('hide', function () {
        var correctQty = document.getElementById("modalReservedPartCorrectQty").value;
        var article = document.getElementById("modalReservedPartAltArticle").value;
        var price = document.getElementById("modalReservedPartAltPrice").value;
        var qty = document.getElementById("modalReservedPartAltQty").value;
        var comment = document.getElementById("modalReservedPartComment").value;
        var isDisplayItem = document.getElementById("modalReservedPartAltDisplayItem").checked;
        var altRowDisplayItemCondition = document.getElementById("modalReservedPartAltDisplayItemCondition").value;

        ChangeRowValues("", correctQty, article, price, qty, comment, isDisplayItem, altRowDisplayItemCondition, '');

        var rowState = document.getElementById("rowState" + currentRow).value;
        toggle(rowState, currentRow, IsStringNumber(article));
    });

    $('#modalReservedDisplayItem').on('hide', function () {
        var correctPrice = document.getElementById("modalReservedDisplayItemCorrectPrice").value;
        var correctQty = document.getElementById("modalReservedDisplayItemCorrectQty").value;
        var article = document.getElementById("modalReservedDisplayItemAltArticle").value;
        var price = document.getElementById("modalReservedDisplayItemAltPrice").value;
        var qty = document.getElementById("modalReservedDisplayItemAltQty").value;
        var comment = document.getElementById("modalReservedDisplayItemComment").value;
        var isDisplayItem = document.getElementById("modalReservedDisplayItemAltDisplayItem").checked;
        var altRowDisplayItemCondition = document.getElementById("modalReservedDisplayItemAltDisplayItemCondition").value;

        var conditionComment = document.getElementById("modalReservedDisplayItemRowConditionStateComment").value;

        ChangeRowValues(correctPrice, correctQty, article, price, qty, comment, isDisplayItem, altRowDisplayItemCondition, conditionComment);

        var rowState = document.getElementById("rowState" + currentRow).value;
        toggle(rowState, currentRow, IsStringNumber(article));
    });

    $('#modalTransferItem').on('hide', function () {
        var transferSapCode = document.getElementById("modalTransferItemRowTransferSapCode").value;
        var transferNumber = document.getElementById("modalTransferItemRowTransferNumber").value;
        
        ChangeRowValues("", '', '', '', '', '', '', '', '', transferSapCode, transferNumber);

        var rowState = document.getElementById("rowState" + currentRow).value;        
        toggle(rowState, currentRow, false);
    });
}