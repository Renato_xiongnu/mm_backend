﻿using System;
using System.Linq;
using System.Collections.Generic;
using Orders.Backend.Clients.Web.Common.OnlineOrder;

namespace Orders.Backend.Clients.Web
{
    public static class Services
    {
        public static OrderServiceClient GetOrderService()
        {
            return new OrderServiceClient();
        }

        public static TicketTool.TicketToolServiceClient GetTicketToolService()
        {
            return new TicketTool.TicketToolServiceClient();
        }

        public static TicketToolAuth.AuthenticationServiceClient GetTicketToolAuthService()
        {
            return new TicketToolAuth.AuthenticationServiceClient();
        }       
       
    }
}