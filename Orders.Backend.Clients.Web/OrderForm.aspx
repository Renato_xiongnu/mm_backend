﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderForm.aspx.cs" Inherits="Orders.Backend.Clients.Web.OrderForm" 
    ValidateRequest="false"%>
<!DOCTYPE html>
<%@ Register  src="~/OrderInfoControl.ascx" tagName="OrderInfoControl" tagPrefix="my" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Заказ</title>
    <meta http-equiv="cache-control" content="no-cache, no-store, max-age=0, must-revalidate"/>
    <meta http-equiv="expires" content="0"/>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"/>
    <div id="mainDiv">
        <br/>
        <br/>
        <my:OrderInfoControl runat="server" OrderId="<%#OrderId%>" ID="orderControl" 
            CanChangeArticleStatus="False" CanChangeArticles="<%# !IsReadOnly %>"
            InEditMode="False"/>
        <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
            Text="Загрузка! Пожалуйста, подождите!"
            ContainerElementID="mainDiv" Modal="True">
        </dx:ASPxLoadingPanel>
    </div>
    </form>
</body>
</html>
