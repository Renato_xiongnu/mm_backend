﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuthTestPage.aspx.cs" Inherits="Orders.Backend.Clients.Web.AuthTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
            <asp:Button ID="Button3" runat="server" Text="log in" OnClick="li_Click" />
            <asp:Button ID="Button2" runat="server" Text="log out" OnClick="lo_Click" />
            <br/>
            <asp:Button ID="Button1" runat="server" Text="Call" OnClick="Call_Click" />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <asp:Button ID="Button4" runat="server" Text="refresh" OnClick="Refresh_Click" />
        </div>
    </form>
</body>
</html>
