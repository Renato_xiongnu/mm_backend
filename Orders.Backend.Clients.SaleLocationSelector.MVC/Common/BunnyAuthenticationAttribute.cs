﻿using System;
using System.Web;
using System.Web.Mvc;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Properties;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Common
{
    public class BunnyAuthenticationAttribute : AuthorizeAttribute
    {
        public static String CurrentUser = String.Empty;
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            base.AuthorizeCore(httpContext);
            var token = MultiplexorSecurityService.GetAuthCookie(); //"39a610b5a8832ea3add4c48c4f2d9e66";//

            if (String.IsNullOrEmpty(token))
                return false;
            var service = new MultiplexorSecurityService();
            
            var user = service.GetUser(token, MuxSettings.Default.MultiplexorChannelId);
            if (user == null)
                return false;
            httpContext.Items["CurrentUser"] = user;
            CurrentUser = String.Format("{0} {1}", user.Name, user.Surname);
            if (!service.HasPermission(token, MuxSettings.Default.MultiplexorChannelId))
            {
                httpContext.Response.RedirectToRoute(new 
                { 
                    controller = "Errors", 
                    action = "Http403", 
                 
                });
            } 
            return true;
        }

      

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        { 
            base.HandleUnauthorizedRequest(filterContext);
            if (!filterContext.HttpContext.Response.IsRequestBeingRedirected)
            {
                var redirectUrl = String.Format(MuxSettings.Default.LoginUrl,HttpUtility.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
                filterContext.Result = new RedirectResult(redirectUrl, true); 
            }
        }
    }
}