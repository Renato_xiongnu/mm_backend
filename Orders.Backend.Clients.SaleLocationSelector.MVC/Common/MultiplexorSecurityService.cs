﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MMS.Multiplexor.Client;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Models;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Properties;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Common
{
    public class MultiplexorSecurityService
    {
        private const String AuthenticateSuccess = "Success";
        public const String AuthenticateCookieName = "BUNNY_SESSION";
        public const String PermissionName = "virtual-shops-tt";
        private const String WritePermission = "write";


        public Boolean Authenticate(String token, String email, String password, String channel)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                return false;
            }

            var multiplexorClient = MultiplexorClientFactory();

            multiplexorClient.UserChannel.Authenticate(token,channel,email,password);
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults;
            var requestResult = executionCommandResults[0].Result;
            var errorMessage = GetMultiplexorResponsError(requestResult); 
            var result = requestResult.Result as OperationStatus; 
            return result != null && result.Status.Equals(AuthenticateSuccess, StringComparison.InvariantCultureIgnoreCase);
        }

        public Boolean HasPermission(String token, String channel)
        {
            if (string.IsNullOrEmpty(token))
            {
                return false;
            }
            var multiplexorClient = MultiplexorClientFactory();

            multiplexorClient.UserChannel.Permissions(token, channel);
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults;
            var requestResult = executionCommandResults[0].Result;
            var errorMessage = GetMultiplexorResponsError(requestResult);
            var result = requestResult.Result as UserChannelApi.PermissinResponse;
            return result != null && result.Resources.Any(x => x.Name.Equals(PermissionName, StringComparison.InvariantCultureIgnoreCase) && WritePermission.Equals(x.Permission, StringComparison.InvariantCultureIgnoreCase));
        }

        public List<String> GetUserRoles(String token, String channel)
        {
            var multiplexorClient = MultiplexorClientFactory();

            multiplexorClient.UserChannel.GetRoles(token, channel);
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults;
            var requestResult = executionCommandResults[0].Result;
            var errorMessage = GetMultiplexorResponsError(requestResult);
            var roles = requestResult.Result as List<String>;
            return roles;
        }

        public UserChannelApi.UserTO GetUser(String token, String channel)
        {
            var multiplexorClient = MultiplexorClientFactory();

            multiplexorClient.UserChannel.GetUser(token, channel);
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults;
            var requestResult = executionCommandResults[0].Result;
            var errorMessage = GetMultiplexorResponsError(requestResult);
            var user = requestResult.Result as UserChannelApi.UserTO;
            return user;
        }


        public User GetUserWithRoles(String token, String channel)
        {
            User user = null;
            var multiplexorClient = MultiplexorClientFactory();
            multiplexorClient.UserChannel.GetUser(token, channel);
            multiplexorClient.UserChannel.GetRoles(token, channel);
            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults;
            var userResult = executionCommandResults[0].Result;
            var errorMessage = GetMultiplexorResponsError(userResult);
            var item = userResult.Result as UserChannelApi.UserTO;
            if (item == null)
                return null;
          
            user= new User
            {
                Email = item.Email, 
                Id = item.UserId,
                Name = item.Name,
                SurName = item.Surname,
                UserRoles = new List<String>(),
            };
            
            var rolesResult = executionCommandResults[0].Result;
            errorMessage += GetMultiplexorResponsError(rolesResult);
            var roles = rolesResult.Result as List<String>;
            if (roles != null)
                user.UserRoles = roles;
            return user;
        }



        private static MultiplexorClient MultiplexorClientFactory()
        {
            return new MultiplexorClient(MuxSettings.Default.MultiplexorUrl, MuxSettings.Default.MultiplexorApiKey, MuxSettings.Default.MultiplexorTimeout);
        }

        public static String GetAuthCookie()
        {
            var sesionCookie = HttpContext.Current.Request.Cookies[AuthenticateCookieName];

            return sesionCookie != null ? sesionCookie.Value : String.Empty;
        }

        private static String GetMultiplexorResponsError(RequestResult requestResult)
        {
            var errorMessage = String.Empty;
            var errors = new Dictionary<String, String>();
            if (requestResult.Errors != null)
            {
                foreach (var error in requestResult.Errors)
                {
                    errors[error.Key] = error.Value;
                }
            }

            if (errors.Count > 0)
            {
                errorMessage = string.Join("\r\n", errors.Select(e => e.Value));
            }
            return errorMessage;
        }
    }
}