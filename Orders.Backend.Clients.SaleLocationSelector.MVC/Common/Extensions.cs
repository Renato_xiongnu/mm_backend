﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Proxy.OrderService;
using Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel;
using Orders.Backend.StockLocationSelector.Dal.Entities;


namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Common
{
    public static class Extensions
    {

        public static SaleLocationViewModel ToSaleLocationViewModel(this SaleLocation saleLocation)
        {
            var saleLocationVm = new SaleLocationViewModel
            {
                SapCode = saleLocation.SapCode,
                Name = saleLocation.Name,
                StockLocationSelectorStrategy = !saleLocation.StockLocationSelectorStrategy.HasValue? StockLocationSelectorStrategyType.Rating.GetDescriptionFromEnumValue():((StockLocationSelectorStrategyType)saleLocation.StockLocationSelectorStrategy.Value).GetDescriptionFromEnumValue(),
                StrategyType = !saleLocation.StockLocationSelectorStrategy.HasValue ? StockLocationSelectorStrategyType.Rating : ((StockLocationSelectorStrategyType)saleLocation.StockLocationSelectorStrategy.Value),
            };
            return saleLocationVm;
        }

        public static String GetDescriptionFromEnumValue(this Enum value)
        {
            var attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }

        public static SaleLocation ToSaleLocation(this StoreData storeData)
        {
            var saleLocation = new SaleLocation
            {
                SapCode = storeData.SapCode,
                Name = storeData.Name,
                IsActive = storeData.IsActive,
                IsHub = storeData.IsHub
            };
            return saleLocation;
        }

        public static StockLocation ToStockLocation(this StoreData storeData)
        {
            var stockLocation = new StockLocation
            {
                SapCode = storeData.SapCode,
                Name = storeData.Name,
                IsActive = storeData.IsActive,
                IsHub = storeData.IsHub
            };
            return stockLocation;
        }

        public static IEnumerable<SelectListItem> ToSelectList(this Enum enumValue)
        {
            return from Enum e in Enum.GetValues(enumValue.GetType())
                   select new SelectListItem
                   {
                       Selected = e.Equals(enumValue),
                       Text = e.GetDescriptionFromEnumValue(),
                       Value = e.ToString()
                   };
        }
    }
}