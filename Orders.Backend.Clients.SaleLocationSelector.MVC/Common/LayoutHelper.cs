﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Caching;
using Newtonsoft.Json;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Properties;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Common
{
    public static class LayoutHelper
    {
        public const int AbsoluteCachePeriodMinutes = 10;
        public const string LayoutCacheKey = "LayoutContainer";

        public static LayoutContainer GetContainer()
        {
            var container = HttpContext.Current.Cache[LayoutCacheKey] as LayoutContainer;
            if (container == null)
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(MuxSettings.Default.InterfaceData);
                    request.Method = "GET";
                    request.CookieContainer = new CookieContainer();
                    request.CookieContainer.Add(
                        new Cookie(
                            MultiplexorSecurityService.AuthenticateCookieName,
                            MultiplexorSecurityService.GetAuthCookie(),
                            "/",
                            new Uri(MuxSettings.Default.InterfaceData).Host));

                    var response = request.GetResponse();


                    using (var responseStream = response.GetResponseStream())
                    {
                        container = JsonConvert.DeserializeObject<LayoutContainer>(new StreamReader(responseStream).ReadToEnd());
                    }

                    HttpContext.Current.Cache.Add(
                        LayoutCacheKey,
                        container,
                        null,
                        DateTime.Now.AddMinutes(AbsoluteCachePeriodMinutes),
                        TimeSpan.Zero,
                        CacheItemPriority.Normal,
                        null);
                }
                catch (Exception ex)
                {
                    container = new LayoutContainer
                    {
                        Header = string.Format("There is a problem retrieving header: {0}", ex.Message),
                        Footer = string.Format("There is a problem retrieving footer: {0}", ex.Message)
                    };
                }
            }

            return container;
        }

       
    }

    public class LayoutContainer
    {
        public string Header { get; set; }

        public string Footer { get; set; }
    }
}