﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Proxy.OrderService;
using Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel;
using Orders.Backend.StockLocationSelector.Dal;
using Orders.Backend.StockLocationSelector.Dal.Entities;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Common
{
    public class StockLocationService : DbServiceBase
    {  
        public SaleLocation GetSaleLocationById(String id)
        {
            return WithDb(db => db.SaleLocations.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(id, StringComparison.InvariantCultureIgnoreCase)));
        }

        public StockLocation GetStockLocationById(String id)
        {
            return WithDb(db => db.StockLocations.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(id, StringComparison.InvariantCultureIgnoreCase)));
        }

        public void CreateOrUpdateStockLocation(StockLocation stockLocation)
        {
            WithDb(db =>
            {
                CreateOrUpdateStockLocation(stockLocation, db);
                db.SaveChanges();

            });
        }

        public void CreateOrUpdateSaleLocation(SaleLocation saleLocation)
        {
            WithDb(db =>
            {
                CreateOrUpdateSaleLocation(saleLocation, db);
                db.SaveChanges();

            });
        }

        protected void CreateOrUpdateSaleLocation(SaleLocation saleLocation, SeleSelectorDbContext db)
        {
            var exist =
                   db.SaleLocations.FirstOrDefault(x =>
                                   x.SapCode != null &&
                                   x.SapCode.Equals(saleLocation.SapCode, StringComparison.InvariantCultureIgnoreCase));
            if (exist == null)
            {
                saleLocation.StockLocationSelectorStrategy = saleLocation.StockLocationSelectorStrategy ?? (Int16)StockLocationSelectorStrategyType.Rating;
                db.SaleLocations.Add(saleLocation);
            }
            else
            {
                exist.Name = saleLocation.Name;
                exist.IsActive = saleLocation.IsActive;
                exist.IsHub = saleLocation.IsHub;
                exist.StockLocationSelectorStrategy = exist.StockLocationSelectorStrategy ?? (Int16) StockLocationSelectorStrategyType.Rating;
            }
        }

        protected void CreateOrUpdateStockLocation(StockLocation stockLocation, SeleSelectorDbContext db)
        {
            var exist = db.StockLocations.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(stockLocation.SapCode, StringComparison.InvariantCultureIgnoreCase));
            if (exist == null)
                db.StockLocations.Add(stockLocation);
            else
            {
                exist.Name = stockLocation.Name;
                exist.IsActive = stockLocation.IsActive;
                exist.IsHub = stockLocation.IsHub;
            }
        }

        public void CreateOrUpdateStockLocations(IEnumerable<StockLocation> stockLocations)
        {
            WithDb(db =>
            {
                foreach (var stockLocation in stockLocations)
                {
                    CreateOrUpdateStockLocation(stockLocation, db);
                }

                db.SaveChanges();

            });
        }

        public void CreateOrUpdateSaleLocations(IEnumerable<SaleLocation> saleLocations)
        {
            WithDb(db =>
            {
                foreach (var saleLocation in saleLocations)
                {
                    CreateOrUpdateSaleLocation(saleLocation, db);
                }

                db.SaveChanges();

            });
        }


        public List<SaleLocation> GetAllSaleLocations()
        {
            return WithDb(db => db.SaleLocations.ToList());
        }


        public List<StockLocation> GetAllStockLocations()
        {
            return WithDb(db => db.StockLocations.ToList());
        }


        public List<AvailableStock> GetAvailableStocksForSaleLocation(String sapCode)
        {
            return WithDb(db => db.AvailableStocks
                                .Include(x => x.SaleLocation)
                                .Include(x => x.StockLocation)
                                .Where(x => x.SaleLocation != null && x.SaleLocation.SapCode.Equals(sapCode, StringComparison.InvariantCultureIgnoreCase)).ToList());
        }


        public void AddStockLocationToSaleLocation(string saleLocationSapCode, string stockLocationSapCode, int power, int rate, Int64? shippingMethods)
        {
            WithDb(db =>
            {
                var saleLocation = db.SaleLocations.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(saleLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
                var stockLocation = db.StockLocations.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(stockLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
                var exist = db.AvailableStocks
                        .Include(x => x.SaleLocation)
                        .Include(x => x.StockLocation)
                        .FirstOrDefault(x => x.SaleLocation != null && x.StockLocation != null
                                        && x.SaleLocation.SapCode.Equals(saleLocation.SapCode, StringComparison.InvariantCultureIgnoreCase)
                                        && x.StockLocation.SapCode.Equals(stockLocation.SapCode, StringComparison.InvariantCultureIgnoreCase));
                if (saleLocation != null && stockLocationSapCode != null && exist == null)
                {
                    var avaliableStock = new AvailableStock
                    {
                        SaleLocation = saleLocation,
                        StockLocation = stockLocation,
                        Rate = rate,
                        Capacity = power,
                        AvailableShippingMethods = shippingMethods,
                    };
                    db.AvailableStocks.Add(avaliableStock);

                    db.SaveChanges();
                }




            });
        }

        public void UpdateRating(List<EditRetingViewModel> model)
        {
            WithDb(db =>
            {
                foreach (var item in model)
                {
                    var exist = db.AvailableStocks
                        .Include(x => x.StockLocation)
                        .Include(x => x.SaleLocation)
                        .FirstOrDefault(x => x.Id == item.Id);
                    if (exist != null)
                    {
                        if (exist.Rate != item.NewRate)
                        {
                            exist.Rate = item.NewRate;
                        }
                        if (exist.Capacity != item.NewCapacity)
                        {
                            exist.Capacity = item.NewCapacity;
                        }
                        if (exist.AvailableShippingMethods != item.DeliveryMethod())
                        {
                            exist.AvailableShippingMethods = item.DeliveryMethod();
                        }
                    }
                }
                db.SaveChanges();
            });
        }

        public void DeleteAvailableStock(int id)
        {
            WithDb(db =>
            {
                var stock = db.AvailableStocks.FirstOrDefault(s => s.Id == id);
                if (stock != null)
                {
                    db.AvailableStocks.Remove(stock);
                }
                db.SaveChanges();
            });
        }

        public void ImportDataFromStore(List<StoreData> allShopsFromStore)
        {
            if(allShopsFromStore == null || !allShopsFromStore.Any())
                return;
            var virtualShops = allShopsFromStore.Where(x => x.IsVirtual);
            var notVierualShops = allShopsFromStore.Where(x => !x.IsVirtual);
            CreateSaleLocations(virtualShops);
            CreateStockLocations(notVierualShops);
        }

        private void CreateSaleLocations(IEnumerable<StoreData> virtualShops)
        {
            try
            {
                var saleLocations = virtualShops.Select(x => x.ToSaleLocation());
                CreateOrUpdateSaleLocations(saleLocations);
            }
            catch (Exception ex)
            { 
                throw;
            }

        }

        private void CreateStockLocations(IEnumerable<StoreData> notVirtualShops)
        {
            try
            {
                var stockLocations = notVirtualShops.Select(x => x.ToStockLocation());
                CreateOrUpdateStockLocations(stockLocations);
            }
            catch (Exception ex)
            { 
                throw;
            } 
        }

        public void UpdateStockLocationSelectorStrategy(String saleLocationSapCode, StockLocationSelectorStrategyType strategyType)
        {
            WithDb(db =>
            {
                UpdateStockLocationSelectorStrategy(saleLocationSapCode, strategyType, db);
                db.SaveChanges();
            });
        }

        protected void UpdateStockLocationSelectorStrategy(String saleLocationSapCode, StockLocationSelectorStrategyType strategyType, SeleSelectorDbContext db)
        {
            var exist = db.SaleLocations.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(saleLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
            if (exist != null)
            {
                exist.StockLocationSelectorStrategy = (Int16) strategyType;
            }
          
        }
    }
}