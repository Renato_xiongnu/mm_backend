﻿using System;
using System.Collections.Generic;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Models
{
    public class User
    {
        public String Id { get; set; }

        public String Name { get; set; }

        public String SurName { get; set; }

        public String Email { get; set; }

        public IList<string> UserRoles { get; set; }

        //public UserAccessInfo UserAccessInfo { get; set; }
    }
}