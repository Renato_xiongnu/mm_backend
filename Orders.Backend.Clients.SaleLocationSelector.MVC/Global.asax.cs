﻿using System; 
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Common;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Controllers;  
using Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var binderDateTime = new DateTimeModelBinder(new[] { "dd.MM.yyyy", "yyyy-MM-dd" });
            ModelBinders.Binders.Add(typeof(DateTime), binderDateTime);
            ModelBinders.Binders.Add(typeof(DateTime?), binderDateTime);


           // ModelBinders.Binders.DefaultBinder = new FlagEnumModelBinder();
        }
        protected void Application_Error()
        {
            var exception = Server.GetLastError();
            var httpException = exception as HttpException;
            Response.Clear();
            Server.ClearError();
            var routeData = new RouteData();
            routeData.Values["controller"] = "Errors";
            routeData.Values["action"] = "General";
            routeData.Values["exception"] = exception != null && exception.InnerException != null ? exception.InnerException : exception;
            Response.StatusCode = 500;
            if (httpException != null)
            {
                Response.StatusCode = httpException.GetHttpCode();
                switch (Response.StatusCode)
                {
                    case 403:
                        routeData.Values["action"] = "Http403";
                        break;
                    case 404:
                        routeData.Values["action"] = "Http404";
                        break;
                }
            }

            IController errorsController = new ErrorsController();
            var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
            errorsController.Execute(rc);
        }
    }
}
