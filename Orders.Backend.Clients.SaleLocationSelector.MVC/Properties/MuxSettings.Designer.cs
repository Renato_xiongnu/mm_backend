﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.0
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class MuxSettings : global::System.Configuration.ApplicationSettingsBase {
        
        private static MuxSettings defaultInstance = ((MuxSettings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new MuxSettings())));
        
        public static MuxSettings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://mux-test.mediasaturnrussia.ru/")]
        public string MultiplexorUrl {
            get {
                return ((string)(this["MultiplexorUrl"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api_key_12345")]
        public string MultiplexorApiKey {
            get {
                return ((string)(this["MultiplexorApiKey"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("8000")]
        public int MultiplexorTimeout {
            get {
                return ((int)(this["MultiplexorTimeout"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://stage-login.bunny.mediasaturnrussia.ru/login?back_url={0}")]
        public string LoginUrl {
            get {
                return ((string)(this["LoginUrl"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("prelive-admin-ui")]
        public string MultiplexorChannelId {
            get {
                return ((string)(this["MultiplexorChannelId"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://stage-login.bunny.mediasaturnrussia.ru/api/interface-data")]
        public string InterfaceData {
            get {
                return ((string)(this["InterfaceData"]));
            }
        }
    }
}
