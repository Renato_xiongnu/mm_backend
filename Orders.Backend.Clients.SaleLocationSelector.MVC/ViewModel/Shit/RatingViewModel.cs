﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using Orders.Backend.Clients.SaleLocationSelector.MVC.Common;
//using Orders.Backend.Clients.SaleLocationSelector.MVC.Proxy.OrderService;
//using Orders.Backend.SaleLocationSelector.Dal.Entities;

//namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
//{
//    public class RatingViewModel
//    {
//        [Display(Name = "Название магазина")]
//        public String ShopName { get; set; }

//        [Required(ErrorMessage = "Поле обязательно для заполнения")]
//        [Display(Name = "Код магазина")]
//        public String SapCode { get; set; }

//        [Required(ErrorMessage = "Поле обязательно для заполнения")]
//        [Display(Name = "Город")]
//        public String CityName { get; set; }

//        [Required(ErrorMessage = "Поле обязательно для заполнения")]
//        [Display(Name = "Рейтинг")]
//        public Int32 Rate { get; set; }

//        //[Required(ErrorMessage = "Поле обязательно для заполнения")]
//        [Display(Name = "Новый рейтинг")]
//        public Int32 NewRate { get; set; }


//        //[Display(Name = "Возможные способы получения")]
//        //public String AvailableShippingMethods { get; set; }


//        public static List<RatingViewModel> CreateRatingViewModel(List<SaleLocation> saleLocations, List<StoreData> allShops, String virtualShopSapCode)
//        {
//            var model = new List<RatingViewModel>();
//            var virtualSop = allShops.FirstOrDefault(x => x.SapCode.Equals(virtualShopSapCode, StringComparison.InvariantCultureIgnoreCase));
          
//            if (virtualSop != null && virtualSop.City !=null)
//            {
//                var currentSaleLications = saleLocations.Where(x => x.CityName.Equals(virtualSop.City, StringComparison.InvariantCultureIgnoreCase)).ToList();
//                foreach (var saleLocation in currentSaleLications)
//                {
//                    var shopInfo = allShops.FirstOrDefault(x => x.SapCode.Equals(saleLocation.SapCode, StringComparison.InvariantCultureIgnoreCase));
//                    if (shopInfo != null && shopInfo.IsVirtual)
//                        continue;
//                    var item = saleLocation.ToRatingViewModel();
//                    if (shopInfo != null)
//                        item.ShopName = shopInfo.Name;
//                    else
//                        item.ShopName = "Не заведен в другой системе!";
//                    model.Add(item);
//                }
//            }
//            return model;
//        } 
//    }   
//}