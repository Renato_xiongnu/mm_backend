﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using Orders.Backend.Clients.SaleLocationSelector.MVC.Proxy.OrderService;
//using Orders.Backend.SaleLocationSelector.Dal.Entities;

//namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
//{
//    public class VirtualShopViewModel
//    {
//        [Required(ErrorMessage = "Поле обязательно для заполнения")]
//        [Display(Name = "Код магазина")]
//        public String SapCode { get; set; }

//        [Required(ErrorMessage = "Поле обязательно для заполнения")]
//        [Display(Name = "Город")]
//        public String CityName { get; set; }

//        [Display(Name = "Название магазина")]
//        public String ShopName { get; set; }

//        public static List<VirtualShopViewModel> CreatVirtualShopViewModel(List<City> cities, List<StoreData> allShops)
//        {
//            var model = new List<VirtualShopViewModel>();
//            var virtualShops = allShops.Where(x => x.IsVirtual && cities.Any(c=>c.Name!=null && c.Name.Equals(x.City, StringComparison.InvariantCultureIgnoreCase))).ToList();
         
//            if (virtualShops != null && virtualShops.Any())
//            {
//                model.AddRange(virtualShops.Select(virtualShop => new VirtualShopViewModel
//                {
//                    CityName = virtualShop.City, 
//                    SapCode = virtualShop.SapCode, 
//                    ShopName = virtualShop.Name
//                }));
//            }
//            return model;
//        }
//    }


//}