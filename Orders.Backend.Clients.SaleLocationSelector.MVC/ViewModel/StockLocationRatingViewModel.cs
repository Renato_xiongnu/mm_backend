﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Common;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Proxy.OrderService;
using Orders.Backend.StockLocationSelector.Dal.Entities;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{   

    public class StockLocationRatingViewModel
    {
        public AvailableStock StockInfo { get; set; }

        [Display(Name = "Новый рейтинг")]
        public Int32 NewRate { get; set; }

        [Display(Name = "Новая пропускная способность")]
        public Double NewCapacity { get; set; }

        [Display(Name = "Доставка")]
        public bool Delivery { get; set; }

        [Display(Name = "Самовывоз из магазина")]
        public bool PickupShop { get; set; }
        
        [Display(Name = "Самовывоз из постаматов")]
        public bool PickupExternal { get; set; }
     
        public static List<StockLocationRatingViewModel> CreateStockLocationRatingViewModel(List<AvailableStock> stockLocations, List<StoreData> allShops)
        { 
            var model = new List<StockLocationRatingViewModel>();
            var service = new StockLocationService();
            foreach (var stockLocation in stockLocations)
            {
                var item = new StockLocationRatingViewModel();
              
                var exist = allShops.FirstOrDefault(x => x.SapCode.Equals(stockLocation.StockLocation.SapCode, StringComparison.InvariantCultureIgnoreCase));
                if (exist == null)
                {
                    stockLocation.StockLocation.Name = "Магазин не добавлен в OnlineOrders";
                }
                else
                {
                    if (!stockLocation.StockLocation.Name.Equals(exist.Name) ||
                        stockLocation.StockLocation.IsActive != exist.IsActive ||
                        stockLocation.StockLocation.IsHub != exist.IsHub)
                    {
                        var updatedStockLocation = exist.ToStockLocation();
                        service.CreateOrUpdateStockLocation(updatedStockLocation);
                        stockLocation.StockLocation = updatedStockLocation;
                    }
                }
                item.StockInfo = stockLocation;
                item.NewRate = stockLocation.Rate;
                item.NewCapacity = stockLocation.Capacity ?? default(Double);

                if (stockLocation.AvailableShippingMethods.HasValue)
                {
                    var shippingMethods =  (StockShippingMethod)stockLocation.AvailableShippingMethods.Value;
                    item.Delivery = shippingMethods.HasFlag(StockShippingMethod.Delivery);
                    item.PickupShop = shippingMethods.HasFlag(StockShippingMethod.PickupShop);
                    item.PickupExternal = shippingMethods.HasFlag(StockShippingMethod.PickupExternal);
                }
                model.Add(item);
            }
          
            return model;
        }  
    }
}