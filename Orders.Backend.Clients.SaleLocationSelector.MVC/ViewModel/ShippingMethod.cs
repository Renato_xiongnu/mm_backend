﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{ 
    public class AvaliableShippingMethod
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public object Tags { get; set; }
        public Boolean IsSelected { get; set; }
    }

    public class AvaliableShippingMethodViewModel
    {
        public IList<AvaliableShippingMethod> AvailableMethods { get; set; }
        public IList<AvaliableShippingMethod> SelectedMethods { get; set; }
        public PostedMethods PostedMethods { get; set; }
    }

    public class PostedMethods
    {
        public List<Int32> MethodIDs { get; set; }
    }
}