﻿using Orders.Backend.StockLocationSelector.Dal.Entities;
using System;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{

    public class EditRetingViewModel
    {
        public Int32 Id { get; set; }

        public Int32 Rate { get; set; }

        public Int32 NewRate { get; set; }

        public Double Capacity { get; set; }

        public Double NewCapacity { get; set; }

        public String SapCode { get; set; }

        public bool Delivery { get; set; }

        public bool PickupShop { get; set; }
        
        public bool PickupExternal { get; set; }

        public long? DeliveryMethod()
        {
            StockShippingMethod method = 0;
            if (Delivery)
                method |= StockShippingMethod.Delivery;
            if (PickupShop)
                method |= StockShippingMethod.PickupShop;
            if (PickupExternal)
                method |= StockShippingMethod.PickupExternal;
            return method == 0 ? null as long? : (long)method;
        }
    }
}