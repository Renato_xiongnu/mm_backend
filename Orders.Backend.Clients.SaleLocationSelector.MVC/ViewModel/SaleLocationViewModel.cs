﻿using System;
using System.ComponentModel.DataAnnotations;
using Orders.Backend.StockLocationSelector.Dal.Entities;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{
    public class SaleLocationViewModel
    {
        [Display(Name = "Код магазина")]
        public String SapCode { get; set; }

        [Display(Name = "Название магазина")]
        public String Name { get; set; }

        [Display(Name = "Страьегия выбора магазина")]
        public String StockLocationSelectorStrategy { get; set; }

        public StockLocationSelectorStrategyType StrategyType { get; set; }
    }
}