﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Common;
using Orders.Backend.StockLocationSelector.Dal.Entities;
using SaleLocation = Orders.Backend.StockLocationSelector.Dal.Entities.SaleLocation;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{
    public class NewStockLocationRatingViewModel
    {
        public SaleLocation SaleLocation { get; set; }

        [Display(Name = "Доступные stock locations")]
        public IEnumerable<SelectListItem> AvaliableStockLocations { get; set; } 

        [Display(Name = "Способы получения")]
        public AvaliableShippingMethodViewModel ShippingMethods { get; set; }

        [Display(Name = "Рейтинг")]
        public Int32 Rate { get; set; }

        [Display(Name = "Пропускная способность")]
        public Int32 Capacity { get; set; }

        [Required]
        public String StockLocationSapCode { get; set; }

        public StockLocationSelectorStrategyType StrategyType { get; set; }

        public String SaleLocationSapCode { get; set; }



        public static NewStockLocationRatingViewModel CreateNewStockLocationRatingViewModel(String sapCode, StockLocationSelectorStrategyType strategyType)
        {
            var model = new NewStockLocationRatingViewModel {StrategyType = strategyType, SaleLocationSapCode = sapCode};
            var service = new StockLocationService();
            model.SaleLocation = service.GetSaleLocationById(sapCode);
            var existStocks = service.GetAvailableStocksForSaleLocation(sapCode);
            var allStocks = service.GetAllStockLocations();
            foreach (var existStock in existStocks)
            {
                var itemForDel = allStocks.FirstOrDefault(x => x.SapCode != null && x.SapCode.Equals(existStock.StockLocation.SapCode, StringComparison.InvariantCultureIgnoreCase));
                allStocks.Remove(itemForDel);
            }
            model.AvaliableStockLocations = allStocks.Select(x => new SelectListItem { Text = x.SapCode+" - "+x.Name, Value = x.SapCode}); ;

           
            model.ShippingMethods = new AvaliableShippingMethodViewModel
            {
                AvailableMethods = new List<AvaliableShippingMethod>(),
                SelectedMethods = new List<AvaliableShippingMethod>(),
                PostedMethods = new PostedMethods { MethodIDs = new List<Int32>() },
            };

            foreach (var enumValue in (StockShippingMethod[])Enum.GetValues(typeof(StockShippingMethod)))
            {
                var item = new AvaliableShippingMethod
                {
                    Id = (Int32)enumValue,
                    Name = enumValue.GetDescriptionFromEnumValue(),
                    IsSelected = false
                }; 
                model.ShippingMethods.AvailableMethods.Add(item);

            }

            return model;

        }
    }
}