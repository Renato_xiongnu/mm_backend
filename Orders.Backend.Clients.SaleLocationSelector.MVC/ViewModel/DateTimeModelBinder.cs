﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        private readonly string[] _customFormat;
        private readonly CultureInfo _cultureInfo = CultureInfo.InvariantCulture;

        public DateTimeModelBinder(string[] customFormat)
        {
            _customFormat = customFormat;
        }
        public DateTimeModelBinder(string[] customFormat, CultureInfo cultureInfo)
        {
            _customFormat = customFormat;
            _cultureInfo = cultureInfo;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value != null && !String.IsNullOrEmpty(value.AttemptedValue))
            {
                DateTime date;
                if (DateTime.TryParseExact(value.AttemptedValue, _customFormat, _cultureInfo, DateTimeStyles.None, out date))
                {
                    return date;
                }

                bindingContext.ModelState.AddModelError(
                    bindingContext.ModelName,
                    string.Format("{0} Невалидное значение даты", value.AttemptedValue)
                    );
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}