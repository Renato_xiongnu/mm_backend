﻿using System;
using System.Collections.Generic; 
using Orders.Backend.StockLocationSelector.Dal.Entities;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel
{
    public class StockLocationSelectorChangeType
    {
        public List<StockLocationRatingViewModel> StocksLocationsRatingViewModel { get; set; }

        public StockLocationSelectorStrategyType SelelctedStrategyType { get; set; }

        public String SaleLocationSapCode { get; set; } 
      
    }
}