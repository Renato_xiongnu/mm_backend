﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Common.Logging;
using MMS.Multiplexor.Client;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Common;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Proxy.OrderService;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Controllers
{
    public class BaseController : Controller
    {
        protected readonly StockLocationService StockLocationService;

        protected readonly ILog Logger;

       
        public BaseController()
        {
            StockLocationService = new StockLocationService();
            Logger = LogManager.GetLogger("StockLocationApplication");
        }

       

        protected List<StoreData> GetAllShopsFromStore()
        {
            var allShops = new List<StoreData>();
            try
            {
                using (var service = new OrderServiceClient())
                {
                    var response = service.GetAllStoresData();
                    if (response.ReturnCode == ReturnCode.Ok)
                    {
                        allShops = response.Stores.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return allShops;
        }

    }
}