﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Orders.Backend.Clients.SaleLocationSelector.MVC.Common;
using Orders.Backend.Clients.SaleLocationSelector.MVC.ViewModel;
using Orders.Backend.StockLocationSelector.Dal.Entities;

namespace Orders.Backend.Clients.SaleLocationSelector.MVC.Controllers
{
    [BunnyAuthentication]
    public class ShopController : BaseController
    {

        public ActionResult Index()
        {
            try
            {
                var allShops = GetAllShopsFromStore().ToList();
                StockLocationService.ImportDataFromStore(allShops);
                var model = StockLocationService.GetAllSaleLocations().Select(x => x.ToSaleLocationViewModel());
                return View(model);

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }


        }


        public ActionResult StockLocations(String sapCode, StockLocationSelectorStrategyType strategyType)
        {

            try
            {
                var model = new StockLocationSelectorChangeType { StocksLocationsRatingViewModel = new List<StockLocationRatingViewModel>() };
                if (String.IsNullOrEmpty(sapCode))
                    return RedirectToAction("Index");
                var allShops = GetAllShopsFromStore();
                var avaliableStocks = StockLocationService.GetAvailableStocksForSaleLocation(sapCode);
                model.StocksLocationsRatingViewModel = StockLocationRatingViewModel.CreateStockLocationRatingViewModel(avaliableStocks, allShops);
                model.SelelctedStrategyType = strategyType;
                model.SaleLocationSapCode = sapCode;
                return View(model);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public ActionResult EditSaleLacationSelectorType(String sapCode, StockLocationSelectorStrategyType strategyType)
        {
            try
            {
                if (String.IsNullOrEmpty(sapCode))
                    return RedirectToAction("Index");
                StockLocationService.UpdateStockLocationSelectorStrategy(sapCode, strategyType);
                ViewBag.VirtualShopSapCode = sapCode;
                Logger.Info(String.Format("Пользователь {0} обновил стратегию для {2}:{1}", BunnyAuthenticationAttribute.CurrentUser, strategyType.GetDescriptionFromEnumValue(), sapCode));
                return RedirectToAction("StockLocations", new { sapCode = sapCode, strategyType = strategyType });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public ActionResult EditRating(List<EditRetingViewModel> model, String sapCode, StockLocationSelectorStrategyType strategyType)
        {
            try
            {
                if (String.IsNullOrEmpty(sapCode))
                {
                    return RedirectToAction("Index");
                }
                //var updateModel = new List<EditRetingViewModel>();
                //var updateStr = String.Empty;

                //model.ForEach(x =>
                //{
                //    if (x.Rate != x.NewRate || x.Capacity != x.NewCapacity)
                //    {
                //        updateModel.Add(x);
                //        updateStr += String.Format("SapCode - {0} - OldRate - {1} - NewRate - {2} OldPow - {3} NewPow - {4};", sapCode, x.Rate, x.NewRate, x.Capacity, x.NewCapacity);
                //    }
                //});
                StockLocationService.UpdateRating(model);
                ViewBag.VirtualShopSapCode = sapCode;
                Logger.Info(String.Format("Пользователь {0} обновил рейтинги:{1}", BunnyAuthenticationAttribute.CurrentUser, ""));
                return RedirectToAction("StockLocations", new { sapCode = sapCode, strategyType = strategyType });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public ActionResult Delete(int stockId, String sapCode, StockLocationSelectorStrategyType strategyType)
        {
            try
            {
                if (String.IsNullOrEmpty(sapCode))
                {
                    return RedirectToAction("Index");
                }
                StockLocationService.DeleteAvailableStock(stockId);
                ViewBag.VirtualShopSapCode = sapCode;
                Logger.Info(String.Format("Пользователь {0} удалил точку продаж с Id = {1}", BunnyAuthenticationAttribute.CurrentUser, stockId));
                return RedirectToAction("StockLocations", new { sapCode = sapCode, strategyType = strategyType });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }


        public ActionResult NewStockLocationForSaleLocation(String sapCode, StockLocationSelectorStrategyType strategyType)
        {
            try
            {
                if (String.IsNullOrEmpty(sapCode))
                    return RedirectToAction("Index");
                var model = NewStockLocationRatingViewModel.CreateNewStockLocationRatingViewModel(sapCode, strategyType);
                ViewBag.VirtualShopSapCode = sapCode;
                return View(model);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public ActionResult Craete(NewStockLocationRatingViewModel model)
        {
            try
            {
                if (String.IsNullOrEmpty(model.SaleLocation.SapCode))
                    return RedirectToAction("Index");
                Int64? shippingMethod = null;
                if (model.ShippingMethods != null && model.ShippingMethods.PostedMethods != null && model.ShippingMethods.PostedMethods.MethodIDs != null)
                    shippingMethod = model.ShippingMethods.PostedMethods.MethodIDs.Sum();
                var updateStr = String.Format("Магазин-{0}, рейтинг-{1}, способы получения-{2}, пропусканая способность- {3}", model.StockLocationSapCode, model.Rate, GetAvailableShippingMethodsDescriptions(shippingMethod), model.Capacity);
                StockLocationService.AddStockLocationToSaleLocation(model.SaleLocation.SapCode, model.StockLocationSapCode, model.Capacity, model.Rate, shippingMethod);

                Logger.Info(String.Format("Пользователь {0} добавил {2} к виртуальному магазину {1}", BunnyAuthenticationAttribute.CurrentUser, model.SaleLocation.SapCode, updateStr));
                return RedirectToAction("StockLocations", new { sapCode = model.SaleLocation.SapCode, strategyType = model.StrategyType });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        private static String GetAvailableShippingMethodsDescriptions(Int64? summ)
        {
            if (!summ.HasValue || summ.Value <= 0)
                return "Не выбраны способы получения";
            return ((StockShippingMethod[])Enum.GetValues(typeof(StockShippingMethod))).Where(enumValue => summ.HasValue && ((StockShippingMethod)summ.Value).HasFlag(enumValue)).Aggregate(String.Empty, (current, enumValue) => current + (enumValue.GetDescriptionFromEnumValue() + ";"));
        }
    }
}