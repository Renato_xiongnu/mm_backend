﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Basket.Contracts.Bunny;

namespace Orders.Backend.Basket.Common
{
    public static class Extensions
    {
        public static bool IsMediaMarkt(this SaleLocation saleLocation)
        {
            return saleLocation.SaleLocationId.ToLower().Contains("shop_r");
        }

        public static bool IsZzt(this SaleLocation saleLocation)
        {
            return saleLocation.SaleLocationId.ToLower().Contains("city");
        }

        public static string GetSapCode(this SaleLocation saleLocation) //Этот метод - идиотский, для идиотского использования
        {
            if (saleLocation.IsMediaMarkt()) //Треш
            {
                return saleLocation.SaleLocationId.Replace("shop_", "").ToUpper();
            }
            if (saleLocation.IsZzt())
            {
                return "R930";
            }

            throw new ArgumentException(string.Format("Incorrect SaleLocationId: {0}", saleLocation.SaleLocationId));
        }
    }
}