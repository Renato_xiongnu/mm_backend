﻿using AutoMapper;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Services.Contracts.OrderService;
using CreateOrderRequest = Orders.Backend.Basket.Contracts.Bunny.CreateOrderRequest;

namespace Orders.Backend.Basket.Mapping
{
    public class MapperConfiguration
    {
        public static void ConfigureMappings()
        {
            Mapper.CreateMap<CreateOrderRequest, ValidateOrderRequest>();
            Mapper.CreateMap<CreateOrderRequest, CreateOrderData>();
            Mapper.CreateMap<CreateQuickOrderRequest, ValidateQuickOrderRequest>();
            Mapper.CreateMap<OrderSource, Proxy.ServiceRouter.OrderSource>();
            Mapper.CreateMap<CreateQuickOrderRequest, CreateOrderData>()
                  .ForMember(el => el.OrderSource,
                             opt =>
                             opt.MapFrom(
                                 t => t.OrderSource.HasValue ? t.OrderSource.Value : OrderSource.WebSiteQuickOrder));
            Mapper.CreateMap<CreateOrderOperationResult, CreateOrderResult>();
            Mapper.CreateMap<CreateOrderOperationResult, CreateQuickOrderResult>();
            Mapper.CreateMap<ValidateOrderResult, ValidateQuickOrderResult>();
            Mapper.CreateMap<Proxy.ServiceRouter.CreateOrderOperationResult, CreateOrderResult>();
            Mapper.CreateMap<Proxy.ServiceRouter.CreateOrderOperationResult, CreateQuickOrderResult>();

            Mapper.CreateMap<ClientData, Proxy.ServiceRouter.ClientData>()
                  .ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<CreateOrderData, Proxy.ServiceRouter.CreateOrderData>()
                  .ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<DeliveryInfo, Proxy.ServiceRouter.DeliveryInfo>()
                  .ForMember(el => el.ExtensionData, opt => opt.Ignore());
            Mapper.CreateMap<ArticleData, Proxy.ServiceRouter.ArticleData>()
                  .ForMember(el => el.ExtensionData, opt => opt.Ignore());
        }
    }
}