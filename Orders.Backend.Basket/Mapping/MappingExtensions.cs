﻿using AutoMapper;

namespace Orders.Backend.Basket.Mapping
{
    public static class MappingExtensions
    {
        public static T ConvertTo<T>(this object model)
        {
            return Mapper.Map<T>(model);
        }
    }
}