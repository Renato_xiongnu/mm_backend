﻿using System.Runtime.Serialization;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.Contracts.Bunny
{
    [DataContract]
    public class ValidateQuickOrderResult:OperationResult
    {
        [DataMember]
        public decimal TotalPrice { get; set; }
    }
}