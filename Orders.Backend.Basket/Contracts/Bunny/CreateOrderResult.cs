﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.Contracts.Bunny
{
    [DataContract]
    public class CreateOrderResult:OperationResult
    {
        [DataMember]
        public string OrderId { get; set; }
    }

    [DataContract]
    public class CreateQuickOrderResult : OperationResult
    {
        [DataMember]
        public string OrderId { get; set; }
    }
}