﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.Contracts.Bunny
{
    [DataContract]
    public class CreateOrderRequest
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public SaleLocation SaleLocation { get; set; }

        [DataMember]
        public ClientData Customer { get; set; }

        [DataMember]
        public ICollection<ArticleData> ArticleDatas { get; set; }

        [DataMember]
        public DeliveryInfo Delivery { get; set; }
    }

    [DataContract]
    public class CreateQuickOrderRequest
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public OrderSource? OrderSource { get; set; }

        [DataMember]
        public SaleLocation SaleLocation { get; set; }

        [DataMember]
        public ClientData Customer { get; set; }

        [DataMember]
        public ICollection<ArticleData> ArticleDatas { get; set; }
    }
}