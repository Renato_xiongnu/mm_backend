﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.Contracts.Bunny
{
    [DataContract]
    public class ValidateQuickOrderRequest : CreateOrderRequest
    {
        [DataMember]
        public OrderSource? OrderSource { get; set; }
    }
}