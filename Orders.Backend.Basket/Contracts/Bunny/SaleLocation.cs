﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Orders.Backend.Basket.Contracts.Bunny
{
    [DataContract]
    public class SaleLocation
    {
        [DataMember]
        public string SaleLocationId { get; set; }
    }
}