﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Orders.Backend.Basket.Mapping;

namespace Orders.Backend.Basket
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            MapperConfiguration.ConfigureMappings();
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}