﻿using System.Collections.Generic;
using System.Linq;
using Orders.Backend.Basket.Common;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Basket.Mapping;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.Validators
{
    public class QuickOrderValidator : IValidator<ValidateQuickOrderRequest, ValidateQuickOrderResult>
    {
        public ValidateQuickOrderResult Validate(ValidateQuickOrderRequest request)
        {
            if(request.SaleLocation == null)
            {
                return new ValidateQuickOrderResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessages = new List<string> {"SaleLocation cannot be null"}
                    };
            }

            if(request.SaleLocation.IsMediaMarkt())
            {
                return ValidateMediaRequest(request).ConvertTo<ValidateQuickOrderResult>();
            }

            if(request.SaleLocation.IsZzt())
            {
                return ValidateZztRequest(request).ConvertTo<ValidateQuickOrderResult>();
            }

            return new ValidateQuickOrderResult
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessages =
                        new List<string>
                            {
                                string.Format("Invalid SaleLocation: {0}", request.SaleLocation.SaleLocationId)
                            }
                };
        }

        private ValidateOrderResult ValidateMediaRequest(ValidateQuickOrderRequest request)
        {
            var errorMessages = new List<string>();
            if(string.IsNullOrWhiteSpace(request.Customer.Surname))
            {
                errorMessages.Add("order.quick.validate.user_full_name_required");
            }
            if(!CommonValidation.IsValidEmail(request.Customer.Email))
            {
                errorMessages.Add("order.quick.validate.user_email_invalid");
            }
            if(string.IsNullOrWhiteSpace(request.Customer.Email))
            {
                errorMessages.Add("order.quick.validate.user_email_required");
            }
            if(string.IsNullOrWhiteSpace(request.Customer.Phone))
            {
                errorMessages.Add("order.quick.validate.user_phone_required");
            }
            if(!CommonValidation.IsValidPhone(request.Customer.Phone))
            {
                errorMessages.Add("order.quick.validate.user_phone_invalid");
            }

            if(errorMessages.Count == 0)
            {
                return new ValidateOrderResult
                    {
                        ReturnCode = ReturnCode.Ok,
                        TotalPrice = request.ArticleDatas.Sum(el => el.Price) //TODO Не работает для setов
                    };
            }

            return new ValidateOrderResult
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessages = errorMessages
                };
        }

        private ValidateOrderResult ValidateZztRequest(ValidateQuickOrderRequest request)
        {
            var errorMessages = new List<string>();
            if(string.IsNullOrWhiteSpace(request.Customer.Phone))
            {
                errorMessages.Add("order.quick.validate.user_phone_required");
            }
            if(!CommonValidation.IsValidPhone(request.Customer.Phone))
            {
                errorMessages.Add("order.quick.validate.user_phone_invalid");
            }

            if(errorMessages.Count == 0)
            {
                return new ValidateOrderResult
                    {
                        ReturnCode = ReturnCode.Ok,
                        TotalPrice = request.ArticleDatas.Sum(el => el.Price)
                    };
            }

            return new ValidateOrderResult
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessages = errorMessages
                };
        }
    }
}