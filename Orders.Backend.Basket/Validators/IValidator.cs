﻿namespace Orders.Backend.Basket.Validators
{
    public interface IValidator<in T, out T1>
    {
        T1 Validate(T request);
    }
}