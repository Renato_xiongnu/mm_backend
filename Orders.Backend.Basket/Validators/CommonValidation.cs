﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Orders.Backend.Basket.Validators
{
    public class CommonValidation
    {
        private static Regex _phoneRegex = new Regex(@"^[\s\d]*$", RegexOptions.Compiled);

        public static bool IsValidEmail(string email)
        {
            try
            {
// ReSharper disable ObjectCreationAsStatement
                new MailAddress(email); //TODO Может, лучше на регулярку
// ReSharper restore ObjectCreationAsStatement

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        public static bool IsValidPhone(string phone)
        {
            return phone != null && _phoneRegex.IsMatch(phone);
        }
    }
}