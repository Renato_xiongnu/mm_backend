﻿using System.Linq;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Services.Contracts.OrderService;
using ValidateOrderResult = Orders.Backend.Basket.Contracts.Bunny.ValidateOrderResult;

namespace Orders.Backend.Basket.Validators
{
    public class OrderValidator:IValidator<ValidateOrderRequest,ValidateOrderResult>
    {
        public ValidateOrderResult Validate(ValidateOrderRequest request)
        {
            return new ValidateOrderResult
                {
                    ReturnCode = ReturnCode.Ok,
                    TotalPrice = request.ArticleDatas.Sum(el => el.Price)
                }; //throw new NotImplementedException();
        }
    }
}