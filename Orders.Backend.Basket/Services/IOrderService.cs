﻿using System.ServiceModel;
using Orders.Backend.Basket.Contracts.Bunny;

namespace Orders.Backend.Basket.Services
{
    [ServiceContract]
    public interface IOrderService
    {
        [OperationContract]
        CreateOrderResult CreateOrder(CreateOrderRequest request);

        [OperationContract]
        CreateQuickOrderResult CreateQuickOrder(CreateQuickOrderRequest request);

        [OperationContract]
        ValidateQuickOrderResult ValidateQuickOrder(ValidateQuickOrderRequest request);

        [OperationContract]
        ValidateOrderResult ValidateOrder(ValidateOrderRequest request);
    }
}
