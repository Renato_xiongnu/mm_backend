﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineOrders.Common.Helpers;
using Orders.Backend.Basket.Common;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Basket.IdGenerator;
using Orders.Backend.Basket.Mapping;
using Orders.Backend.Basket.Proxy.ServiceRouter;
using Orders.Backend.Basket.Validators;
using log4net;
using CreateOrderData = Orders.Backend.Services.Contracts.OrderService.CreateOrderData;
using ReturnCode = Orders.Backend.Services.Contracts.OrderService.ReturnCode;

namespace Orders.Backend.Basket.Services
{
    public class OrderService : IOrderService
    {
        private readonly ILog _logger = LogManager.GetLogger("Basket.OrderService");

        public CreateOrderResult CreateOrder(CreateOrderRequest request)
        {
            try
            {
                _logger.DebugFormat("Start creating order: {0}", request.ToJson());

                var client = new Proxy.ServiceRouter.OrderServiceClient();
                var validationResult = ValidateOrder(request.ConvertTo<ValidateOrderRequest>());
                if(validationResult.ReturnCode != ReturnCode.Ok)
                {
                    return new CreateOrderResult
                        {
                            ReturnCode = ReturnCode.Error,
                            ErrorMessages = validationResult.ErrorMessages
                        };
                }

                var orderData = request.ConvertTo<CreateOrderData>();

                var createOrderData = orderData.ConvertTo<Proxy.ServiceRouter.CreateOrderData>();
                createOrderData.StoreInfo = new Proxy.ServiceRouter.StoreInfo
                    {
                        SapCode = request.SaleLocation.GetSapCode()
                    };
                var createOrderResult = client.CreateOrder(
                    request.Customer.ConvertTo<Proxy.ServiceRouter.ClientData>(),
                    orderData.ConvertTo<Proxy.ServiceRouter.CreateOrderData>(),
                    request.Delivery.ConvertTo<Proxy.ServiceRouter.DeliveryInfo>(),
                    request.ArticleDatas.Select(el => el.ConvertTo<Proxy.ServiceRouter.ArticleData>()).ToArray());

                return createOrderResult.ConvertTo<CreateOrderResult>();
            }
            catch(Exception e)
            {
                _logger.DebugFormat("Error during creating order {0}. Error: {1}", request.ToJson(), e);
                _logger.Error("Error during creating order: ", e);

                return new CreateOrderResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessages = new List<string> { string.Format("Error during creating order: {0}", e.Message) }
                    };
            }
        }

        public CreateQuickOrderResult CreateQuickOrder(CreateQuickOrderRequest request)
        {
            try
            {
                _logger.DebugFormat("Start creating order: {0}", request.ToJson());

                var client = new OrderServiceClient();
                var validationResult = ValidateQuickOrder(request.ConvertTo<ValidateQuickOrderRequest>());
                if(validationResult.ReturnCode != ReturnCode.Ok)
                {
                    return new CreateQuickOrderResult
                        {
                            ReturnCode = ReturnCode.Error,
                            ErrorMessages = validationResult.ErrorMessages
                        };
                }

                var orderData = request.ConvertTo<CreateOrderData>();
                if(string.IsNullOrEmpty(orderData.OrderId))
                {
                    var factory = IdGeneratorFactory.CreateGenerator(request.SaleLocation);
                    orderData.OrderId = factory.GenerateId(request.SaleLocation);
                }

                var clientData = request.Customer.ConvertTo<ClientData>();
                if(clientData.Name == null)
                {
                    clientData.Name = string.Empty; //TODO
                }
                if(!string.IsNullOrEmpty(clientData.Phone))
                {
                    clientData.Phone = clientData.Phone.Replace(" ", string.Empty);
                }
                if(!string.IsNullOrEmpty(clientData.Phone2))
                {
                    clientData.Phone2 = clientData.Phone2.Replace(" ", string.Empty);
                }

                var createOrderData = orderData.ConvertTo<Proxy.ServiceRouter.CreateOrderData>();
                createOrderData.StoreInfo = new StoreInfo {SapCode = request.SaleLocation.GetSapCode()};
                //TODO Сделано для заказа в один клик по старому процессу
                var createOrderResult = client.CreateOrder(
                    clientData,
                    createOrderData,
                    new DeliveryInfo {HasDelivery = false},
                    request.ArticleDatas.Select(el => el.ConvertTo<ArticleData>()).ToArray());

                return createOrderResult.ConvertTo<CreateQuickOrderResult>();
            }
            catch(Exception e)
            {
                _logger.DebugFormat("Error during creating quick order {0}. Error: {1}", request.ToJson(), e);
                _logger.Error("Error during creating quick order: ", e);

                return new CreateQuickOrderResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessages =
                            new List<string> {string.Format("Error during creating quick order: {0}", e.Message)}
                    };
            }
        }

        public ValidateQuickOrderResult ValidateQuickOrder(ValidateQuickOrderRequest request)
        {
            try
            {
                var validator = new QuickOrderValidator();
                return validator.Validate(request);
            }
            catch(Exception e)
            {
                _logger.DebugFormat("Error during validating order {0}. Error: {1}", request.ToJson(), e);
                _logger.Error("Error validating creation order: ", e);

                return new ValidateQuickOrderResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessages =
                            new List<string> {string.Format("Exception during validating order: {0}", e.Message)}
                    };
            }
        }

        public ValidateOrderResult ValidateOrder(ValidateOrderRequest request)
        {
            try
            {
                var validator = new OrderValidator();
                return validator.Validate(request);
            }
            catch(Exception e)
            {
                _logger.DebugFormat("Error during validating order {0}. Error: {1}", request.ToJson(), e);
                _logger.Error("Error validating creation order: ", e);

                return new ValidateOrderResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessages =
                            new List<string> {string.Format("Exception during validating order: {0}", e.Message)}
                    };
            }
        }
    }
}