﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Basket.Common;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.IdGenerator
{
    public class FakeIdGenerator:IIdGenerator
    {
        public string GenerateId(SaleLocation saleLocation)
        {
            var rand = new Random();

            return string.Format("{0:0000}-{1:0000}", rand.Next(0, 9999), rand.Next(0, 9999));
        }
    }
}