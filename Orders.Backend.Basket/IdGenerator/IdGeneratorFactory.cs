﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Basket.Common;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.IdGenerator
{
    public class IdGeneratorFactory
    {
        public static IIdGenerator CurrentGenerator; //For tests

        public static IIdGenerator CreateGenerator(SaleLocation saleLocation)
        {
            if (CurrentGenerator != null)
            {
                return CurrentGenerator;
            }

            if (saleLocation.IsMediaMarkt())
            {
                return new MMIdGenerator(); //TODO Поправить на prod
            }

            if (saleLocation.IsZzt())
            {
                throw new NotImplementedException("IdGenerator for ZZT doesn't implemented");
            }

            throw new ArgumentException("SaleLocationId {0} cannot be parsed", saleLocation.SaleLocationId);
        }
    }
}