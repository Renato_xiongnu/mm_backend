﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.IdGenerator
{
    public interface IIdGenerator
    {
        string GenerateId(SaleLocation saleLocation);
    }
}
