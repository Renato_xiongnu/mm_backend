﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Orders.Backend.Basket.Common;
using Orders.Backend.Basket.Contracts.Bunny;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Basket.IdGenerator
{
    public class MMIdGenerator : IIdGenerator
    {
        public string GenerateId(SaleLocation saleLocation)
        {
            int maxCount;
            int.TryParse(ConfigurationManager.AppSettings["IdGenerationTryCount"], out maxCount);
            if (maxCount == 0)
            {
                maxCount = 3;
            }

            for (var i = 0; i < maxCount; i++)
            {
                try
                {
                    var uri =
                        new Uri(String.Format(ConfigurationManager.AppSettings["IdGenerationUrl"], saleLocation.GetSapCode()));
                    var username = ConfigurationManager.AppSettings["IdGenerationLogin"];
                    var password = ConfigurationManager.AppSettings["IdGenerationPassword"];
                    var authInfo = username + ":" + password;
                    authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                    var request = (HttpWebRequest) WebRequest.Create(uri);
                    request.Headers["Authorization"] = "Basic " + authInfo;
                    using (var response = (HttpWebResponse) request.GetResponse())
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var js = new JavaScriptSerializer();
                        var result = reader.ReadToEnd();
                        var order = js.Deserialize<dynamic>(result);

                        var orderId = order["orderNum"];
                        if (string.IsNullOrEmpty(orderId))
                        {
                            throw new InvalidDataException();
                        }
                        return order["orderNum"];
                    }
                }
// ReSharper disable EmptyGeneralCatchClause
                catch (Exception)
// ReSharper restore EmptyGeneralCatchClause
                {

                }
            }
            var rand = new Random();

            return string.Format("{0}-{1:0000}-{2:0000}", saleLocation.GetSapCode(), rand.Next(0, 9999), rand.Next(0, 9999));
            //TODO Исправить этот неебически феерический костыль
        }
    }
}