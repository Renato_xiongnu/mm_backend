﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using InstallService.Dal;
using InstallService.Dal.OrderService;
using InstallService.Model;
using InstallService.Web.ProductBackendService;
using InstallService.Web.ViewModel;
using Microsoft.Ajax.Utilities;
using OrderLine = InstallService.Web.ViewModel.OrderLine;

namespace InstallService.Web.Controllers
{
    [Authorize]
    public class InstallationsController : ApiController
    {
        private readonly Service _service = new Service(new InstallRepository());

        [HttpGet]
        [ActionName("Installations")]
        public async Task<IEnumerable<InstallationItem>> Installations(SearchQuery searchQuery)
        {
            var result = new List<InstallationItem>();
            var auth = new AuthenticationService.AuthenticationServiceClient();
            var currentUser = auth.GetCurrentUser();
            var isAdmin = currentUser.Roles.Contains(ConfigurationManager.AppSettings["AdminRole"]);
            searchQuery = new SearchQuery {Parameters = currentUser.Parameters};
            var allItems = (await _service.All(searchQuery)).OrderByDescending(el => el.CreatedDate);

            var productBackendClient = new OrdersBackendServiceClient();
            foreach (var item in allItems)
            {
                if (item.Items.Any(el => string.IsNullOrEmpty(el.Name)))
                {
                    var articleStockStatus =
                        await
                            productBackendClient.GetArticleStockStatusAsync(new GetItemsRequest
                                //TODO Перенести в Details, иначе слишком много запросов
                            {
                                Channel = "MM",
                                SaleLocation = "shop_R002",
                                Articles =
                                    item.Items.Where(
                                        el =>
                                            string.IsNullOrEmpty(el.Name) &&
                                            el.ArticleType == ArticleProductType.InstallationService.ToString()).
                                        Select(el => el.ArticleId).ToArray()
                            });
                    foreach (var ordersBackendItem in articleStockStatus.Items)
                    {
                        var targetItems =
                            item.Items.Where(el => el.ArticleId == ordersBackendItem.Article.ToString()).ToList();
                        foreach (var targetItem in targetItems)
                        {
                            if (targetItem != null)
                                targetItem.Name = ordersBackendItem.Title;
                        }
                    }
                }

                var newItems = item.OrderToInstallationItems(isAdmin);
                result.AddRange(newItems);
            }

            return result;
        }

        [HttpGet]
        [ActionName("InstallationDetails")]
        public async Task<IEnumerable<OrderLine>> InstallationDetails(string orderId, string installationArticle)
        {
            var result = new List<OrderLine>();
            var auth = new AuthenticationService.AuthenticationServiceClient();
            var currentUser = auth.GetCurrentUser();
            var searchQuery = new SearchQuery {OrderId = orderId, Parameters = currentUser.Parameters};
            var item = await _service.SearchInstallationAsync(searchQuery);
            var installation = item.Items.First(el => el.ArticleId == installationArticle);
            item.Items =
                item.Items.Where(
                    el =>
                        el.ArticleId == installation.ReferToArticle ||
                        el.ArticleType == ArticleProductType.InstallServiceMarging.ToString()).DistinctBy(t=>t.ArticleId).ToList();

            var productBackendClient = new OrdersBackendServiceClient();
            var articleStockStatus =
                await
                    productBackendClient.GetArticleStockStatusAsync(new GetItemsRequest
                    {
                        Channel = "MM",
                        SaleLocation = "shop_R002",
                        Articles = item.Items.Where(el => string.IsNullOrEmpty(el.Name)).
                            Select(el => el.ArticleId).ToArray()
                    });
            foreach (var ordersBackendItem in articleStockStatus.Items)
            {
                var targetItem = item.Items.FirstOrDefault(el => el.ArticleId == ordersBackendItem.Article.ToString());
                if (targetItem != null)
                    targetItem.Name = ordersBackendItem.Title;
            }
            result.AddRange(item.Items.Select(el => new OrderLine
            {
                Article = el.ArticleId,
                Name = el.Name,
                Price = el.Price
            }));

            return result;
        }

        [HttpPost]
        [ActionName("UpdateInstallation")]
        public async Task<IHttpActionResult> UpdateInstallation([FromBody] UpdateInstallation installationItem)
        {
            if (!ModelState.IsValid)
            {
                throw new Exception(string.Join(";",
                    ModelState.SelectMany(el => el.Value.Errors.Select(x => x.ErrorMessage))));
            }

            var auth = new AuthenticationService.AuthenticationServiceClient();
            var userName = await auth.GetCurrentUserAsync();

            await _service.UpdateInstallationAsync(installationItem.Id, installationItem.NewApprovedDate,
                installationItem.NewFactDate, installationItem.NewInstallationAddress, installationItem.NewStatus,
                installationItem.CancelReason, userName.LoginName);

            return Ok();
        }

        [HttpGet]
        [ActionName("AllStatuses")]
        public IEnumerable<string> AllStatuses()
        {
            return
                Enum.GetValues(typeof (InstallationStatus))
                    .OfType<InstallationStatus>()
                    .Except(new[] {InstallationStatus.None})
                    .Select(Service.GetStatusLocalizableName);
        }

        [HttpGet]
        [ActionName("AllDeliveryTypes")]
        public IEnumerable<string> AllDeliveryTypes()
        {
            return new[] {"Доставка", "Самовывоз"};
        }

        [HttpGet]
        [ActionName("AllCancelReasons")]
        public IEnumerable<string> AllCancelReasons()
        {
            return new[] {"брак", "допработы", "отказ при выезде", "отказ при звонке", "отмена заказа"};
        }
    }
}