﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using InstallService.Dal;
using InstallService.Model;
using InstallService.Web.ViewModel;

namespace InstallService.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly Service _service = new Service(new InstallRepository());

        public ActionResult Index()
        {
            return View();
        }
    }
}