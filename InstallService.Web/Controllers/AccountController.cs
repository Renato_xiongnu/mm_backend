﻿using System.Web.Mvc;
using System.Web.Security;
using InstallService.Web.AuthenticationService;

namespace InstallService.Web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string login, string password)
        {
            using (var authService = new AuthenticationServiceClient())
            {
                var authResult = authService.LogIn(login, password, true);

                switch (authResult)
                {
                    case true:
                        return RedirectToAction("Index", "Home");
                    case false:
                        return View("Index");
                } 
                return View("Index");
            }
           
        }

        public ActionResult Logout()
        {
            using (var authService = new AuthenticationServiceClient())
            {
                authService.LogOut();
                return RedirectToAction("Index");
            }
        }
    }
}