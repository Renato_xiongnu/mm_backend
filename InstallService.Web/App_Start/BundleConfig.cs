﻿using System.Web.Optimization;

namespace InstallService.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                           .Include("~/Scripts/jquery-{version}.min.js")
                           );

            bundles.Add(new StyleBundle("~/Content/kendo/css")
                .Include("~/Content/kendo/kendo.common.css")
                .Include("~/Content/kendo/kendo.bootstrap.css")
                .Include("~/Content/kendo.css")
                );

            bundles.Add(new ScriptBundle("~/bundles/kendoui")
                .Include("~/Scripts/kendo/kendo.core.js")
                .Include("~/Scripts/kendo/kendo.data.odata.js")
                .Include("~/Scripts/kendo/kendo.data.xml.js")
                .Include("~/Scripts/kendo/kendo.data.js")
                .Include("~/Scripts/kendo/kendo.validator.js")
                .Include("~/Scripts/kendo/kendo.binder.js")
                .Include("~/Scripts/kendo/kendo.editable.js")
                .Include("~/Scripts/kendo/kendo.fx.js")
                .Include("~/Scripts/kendo/kendo.popup.js")
                .Include("~/Scripts/kendo/kendo.upload.js")
                .Include("~/Scripts/kendo/kendo.list.js")
                .Include("~/Scripts/kendo/kendo.calendar.js")
                .Include("~/Scripts/kendo/kendo.combobox.js")
                .Include("~/Scripts/kendo/kendo.datepicker.js")
                .Include("~/Scripts/kendo/kendo.numerictextbox.js")
                .Include("~/Scripts/kendo/kendo.dropdownlist.js")
                .Include("~/Scripts/kendo/kendo.filtermenu.js")
                .Include("~/Scripts/kendo/kendo.menu.js")
                .Include("~/Scripts/kendo/kendo.columnmenu.js")
                .Include("~/Scripts/kendo/kendo.multiselect.js")
                .Include("~/Scripts/kendo/kendo.selectable.js")
                .Include("~/Scripts/kendo/kendo.groupable.js")
                .Include("~/Scripts/kendo/kendo.pager.js")
                .Include("~/Scripts/kendo/kendo.sortable.js")
                .Include("~/Scripts/kendo/kendo.reorderable.js")
                .Include("~/Scripts/kendo/kendo.userevents.js")
                .Include("~/Scripts/kendo/kendo.draganddrop.js")
                .Include("~/Scripts/kendo/kendo.resizable.js")
                .Include("~/Scripts/kendo/kendo.window.js")
                .Include("~/Scripts/kendo/kendo.tabstrip.js")
                .Include("~/Scripts/kendo/kendo.tooltip.js")
                .Include("~/Scripts/kendo/kendo.grid.js")
                .Include("~/Scripts/kendo/kendo.button.js")
                .Include("~/Scripts/kendo/kendo.progressbar.js")
                .Include("~/Scripts/kendo/kendo.autocomplete.js")
                .Include("~/Scripts/kendo/cultures/kendo.culture.ru-RU.js")
                .Include("~/Scripts/kendo.extended.js")
                );

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap-datepicker.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
