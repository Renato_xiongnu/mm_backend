﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InstallService.Model;

namespace InstallService.Web.ViewModel
{
    public class InstallationItem
    {
        public String OrderId { get; set; }

        public String WwsOrderId { get; set; }

        public String SapCode { get; set; }

        public String UserName { get; set; }

        public String Phone { get; set; }

        public String DeliveryType { get; set; }

        public String DeliveryAddress { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string RequestedDeliveryPeriod { get; set; }

        public String Status { get; set; }
        
        public String InstallStatusDescription { get; set; }

        public String InstallStatusReason { get; set; }

        public String PaymentType { get; set; }

        public DateTime CreatedDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ApprovedDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? FactDate { get; set; }

        public String InstallationAddress { get; set; }

        public Int32 Id { get; set; } 

        public String ArticleId { get; set; }

        public String Name { get; set; }

        public Decimal? Price { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Select a correct license")]
        public InstallationStatus? InstallStatus { get; set; }

        [DataType(DataType.Date)]
        public DateTime? CompleteDate { get; set; }

        public String RefToItem { get; set; }

        public IEnumerable<Status> AvailableStatuses { get; set; }

        public IEnumerable<OrderLine> RelatedItems { get; set; }

        public string ServiceMargingArticleId { get; set; }

        public string ServiceMargingName { get; set; }

        public decimal? ServiceMargingPrice { get; set; }
    }

    public class OrderLine
    {
        public String Article { get; set; }

        public String Name { get; set; }

        public Decimal? Price { get; set; }
    }

    public class Status
    {
        public string InstallationStatusText { get; set; }

        public string InstallationStatusValue { get; set; }
    }
}