﻿using System.Collections.Generic;
using System.Linq;
using InstallService.Dal.OrderService;
using InstallService.Model;
using InstallService.Web.AuthenticationService;

namespace InstallService.Web.ViewModel
{
    public static class Extensions
    {
        public static List<InstallationItem> OrderToInstallationItems(this Order order, bool isAdmin)
        {
            var result = new List<InstallationItem>();

            foreach (var orderItem in order.Items)
            {
                if (orderItem.ArticleType != ArticleProductType.InstallationService.ToString())
                {
                    continue;
                }

                var installServiceMarging = order.Items.Where(el => el.ArticleType == ArticleProductType.InstallServiceMarging.ToString()).FirstOrDefault();

                Model.OrderLine item = orderItem;
                var inst = new InstallationItem
                {
                    OrderId = order.OrderId,
                    SapCode = order.SapCode,
                    WwsOrderId = order.WwsOrderId,
                    Status = order.Status,
                    PaymentType = order.PaymentType,
                    CreatedDate = order.CreatedDate,
                    DeliveryDate = order.DeliveryDate,
                    DeliveryType = order.DeliveryType,
                    DeliveryAddress = order.DeliveryAddress,
                    UserName = order.Name.Trim(),
                    Phone = order.Phone,
                    InstallationAddress = order.InstallationAddress,
                    Id = orderItem.Id,
                    ApprovedDate = orderItem.ApprovedDate,
                    FactDate = orderItem.FactDate,
                    InstallStatus = orderItem.Status,
                    InstallStatusDescription = Service.GetStatusLocalizableName(orderItem.Status),
                    InstallStatusReason = orderItem.StatusReason,
                    CompleteDate = orderItem.CompleteDate,
                    ArticleId = orderItem.ArticleId,
                    Name = orderItem.Name,
                    Price = orderItem.Price,

                    ServiceMargingArticleId = installServiceMarging != null ? installServiceMarging.ArticleId : null,
                    ServiceMargingName = installServiceMarging != null ? installServiceMarging.Name : null,
                    ServiceMargingPrice = installServiceMarging != null ? installServiceMarging.Price : null,

                    RefToItem = orderItem.ReferToArticle,
                    AvailableStatuses =
                        orderItem.AvailableStatuses(isAdmin)
                            .Select(
                                el =>
                                    new Status
                                    {
                                        InstallationStatusText = Service.GetStatusLocalizableName(el),
                                        InstallationStatusValue = el.ToString(),
                                    }),
                    RelatedItems =
                        order.Items.Where(
                            el =>
                                el.ArticleId == item.ReferToArticle ||
                                el.ArticleType == ArticleProductType.InstallServiceMarging.ToString())
                            .Select(el => new OrderLine
                            {
                                Article = el.ArticleId,
                                Name = el.Name,
                                Price = el.Price
                            }),
                    RequestedDeliveryPeriod = order.RequestedDeliveruPeriod
                };

                result.Add(inst);
            }
            return result;
        }

        public static IEnumerable<InstallationStatus> AvailableStatuses(this Model.OrderLine line, bool isAdmin)
        {
            if (isAdmin)
            {
                return new[] { InstallationStatus.New, InstallationStatus.Processing, InstallationStatus.ProcessingWithOpenDate, InstallationStatus.Canceled, InstallationStatus.Closed };
            }
            if (line.Status == InstallationStatus.New)
            {
                return new[] { InstallationStatus.Processing, InstallationStatus.ProcessingWithOpenDate, InstallationStatus.Canceled, };
            }

            if (line.Status == InstallationStatus.ProcessingWithOpenDate)
            {
                return new[] { InstallationStatus.Canceled, InstallationStatus.Processing };
            }

            if (line.Status == InstallationStatus.Processing)
            {
                return new[] { InstallationStatus.Canceled, InstallationStatus.Closed };
            }
            return Enumerable.Empty<InstallationStatus>();
        }

    }
}