﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InstallService.Model;

namespace InstallService.Web.ViewModel
{
    public class UpdateInstallation:IValidatableObject
    {
        public int Id { get; set; }
        public DateTime? NewApprovedDate { get; set; }
        public DateTime? NewFactDate { get; set; }
        public string NewInstallationAddress { get; set; }
        public InstallationStatus? NewStatus { get; set; }
        public string CancelReason { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (NewStatus == InstallationStatus.Canceled && string.IsNullOrEmpty(CancelReason))
            {
                results.Add(new ValidationResult("CancelReason is required to cancel installation"));
            }
            if (NewStatus != InstallationStatus.Canceled && !string.IsNullOrEmpty(CancelReason))
            {
                results.Add(new ValidationResult("Can't add cancel reason to not canceled"));
            }

            return results;
        }
    }
}