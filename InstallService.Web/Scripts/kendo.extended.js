﻿var customDateFilter =
{
    extra: true,
    operators: {},
    ui: function (element) {
        var parent = element.parent();
        while (parent.children().length > 1)
            $(parent.children()[0]).remove();

        parent.prepend(
            "От:<br/><span class=\"k-widget k-datepicker k-header\">" +
            "<span class=\"k-picker-wrap k-state-default\">" +
            "<input data-bind=\"value:filters[0].value\" class=\"k-input\" type=\"text\" data-role=\"datepicker\"" +
            " style=\"width: 100%\" role=\"textbox\" aria-haspopup=\"true\" aria-expanded=\"false\" aria-disabled=\"false\" " +
            " aria-readonly=\"false\" aria-label=\"Choose a date\">" +
            "<span unselectable=\"on\" class=\"k-select\" role=\"button\">" +
            "<span unselectable=\"on\" class=\"k-icon k-i-calendar\">select</span></span></span></span>" +

            "<br/>До:<br/>" +
            "<span class=\"k-widget k-datepicker k-header\"><span class=\"k-picker-wrap k-state-default\">" +
            "<input data-bind=\"value: filters[1].value\" class=\"k-input\" type=\"text\" data-role=\"datepicker\"" +
            " style=\"width: 100%\" role=\"textbox\" aria-haspopup=\"true\" aria-expanded=\"false\" " +
            " aria-disabled=\"false\" aria-readonly=\"false\" aria-label=\"Choose a date\">" +
            "<span unselectable=\"on\" class=\"k-select\" role=\"button\">" +
            "<span unselectable=\"on\" class=\"k-icon k-i-calendar\">select</span></span></span></span>"
        );
    }
};

kendo.ui.OnInitDayFilter = function (e) {
    var firstDatePicker = e.container
        .find('input[data-bind="value:filters[0].value"]')
        .data("kendoDatePicker");
    var firstDropDown = e.container
        .find('select[data-bind="value: filters[0].operator"]')
        .data("kendoDropDownList");
    $('.k-dropdown', e.container).hide();
    var firstDateInput = e.container
        .find('input[data-bind="value:filters[0].value"]');
    var secondDatePicker = e.container
        .find('input[data-bind="value: filters[1].value"]')
        .data("kendoDatePicker");
    var secondDropDown = e.container
        .find('select[data-bind="value: filters[1].operator"]')
        .data("kendoDropDownList");
    var menuVisible = false;
    var filterVisibled = function (parameters) {
        var firstDate = firstDatePicker.value();
        var secondDate = secondDatePicker.value();
        if (!firstDate && !secondDate) {
            return;
        }
        var firstOperation = firstDropDown.value();
        if (firstDate && !secondDate && firstOperation === 'lte') {
            firstDatePicker.value(null);
            firstDatePicker.trigger('change');
            secondDatePicker.value(firstDate);
            secondDatePicker.trigger('change');
        }
    };
    var visibilityCheck = function (p) {
        if (!menuVisible && e.container.is(':visible')) {
            menuVisible = true;
            filterVisibled();
            return;
        }
        if (menuVisible && !e.container.is(':visible')) {
            menuVisible = false;
            return;
        }
    };
    var dateChanged = function () {
        var firstDate = firstDatePicker.value();
        var secondDate = secondDatePicker.value();
        if (secondDate) {
            var hours = secondDate.getHours();
            if (hours != 23) {
                //secondDate.setUTCDate(secondDate.getUTCDate() + 1);
                secondDate.setHours(23, 59, 59, 999);
                secondDatePicker.value(secondDate);
                secondDatePicker.trigger('change');
            }
        }
        if (firstDate && secondDate) {
            firstDropDown.value('gte');
            firstDropDown.trigger('change');
            secondDropDown.value('lte');
            secondDropDown.trigger('change');
            return;
        }
        if (firstDate && !secondDate) {
            firstDropDown.value('gte');
            firstDropDown.trigger('change');
            secondDropDown.value('eq');
            secondDropDown.trigger('change');
            return;
        }
        if (!firstDate && secondDate) {
            firstDropDown.value('eq');
            firstDropDown.trigger('change');
            secondDropDown.value('lte');
            secondDropDown.trigger('change');
            return;
        }
    };
    firstDatePicker.bind('change', dateChanged);
    secondDatePicker.bind('change', dateChanged);
    window.setInterval($.proxy(visibilityCheck, this), 100);
};