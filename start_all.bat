iisreset \start

net start MM.Orders.Backend.Scheduler
net start MM.TicketTool.Scheduler
net start MMS.Cloud.Wf.Connector.TTMMFORD
net start MMS.Orders.Backend.Push.FromCloud
net start MMS.Orders.Backend.Push.PickupLocation
net start TTMoitoringService
net start AppFabricEventCollectionService
net start AppFabricWorkflowManagementService

