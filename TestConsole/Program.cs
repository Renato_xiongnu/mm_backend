﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestConsole.InstallationServic;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ImportOrderForInstall();
            Console.ReadLine();
        }

        private static void ImportOrderForInstall()
        {
            var ordersIds = new[] {"006-470-512", "006-490-509"};

            using (var client = new InstallationServiceClient())
            {
                foreach (var ordersId in ordersIds)
                {
                     client.CreateInstallation(ordersId);
                } 
            }
          
                 
       }
    }
}
