﻿using System;
using System.Collections.Generic;
using Orders.Backend.Services.Router.BackendOrdersNew;
using ArticleData = Orders.Backend.Services.Router.BackendOrdersNew.ArticleData;
using ClientData = Orders.Backend.Services.Router.BackendOrdersNew.ClientData;
using CreateOrderData = Orders.Backend.Services.Router.BackendOrdersNew.CreateOrderData;
using CreateOrderOperationResult = Orders.Backend.Services.Router.BackendOrdersNew.CreateOrderOperationResult;
using DeliveryInfo = Orders.Backend.Services.Router.BackendOrdersNew.DeliveryInfo;
using OperationResult = Orders.Backend.Services.Router.BackendOrdersNew.OperationResult;
using StoreInfo = Orders.Backend.Services.Router.BackendOrdersNew.StoreInfo;
using UpdateOrderData = Orders.Backend.Services.Router.BackendOrdersNew.UpdateOrderData;
using ValidateOrderOperationResult = Orders.Backend.Services.Router.BackendOrdersNew.ValidateOrderOperationResult;

namespace Orders.Backend.Services.Router
{
    public interface IRouteService
    {
        CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<ArticleData> articlesData);

        CreateOrderOperationResult CreateOrder2(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<InitArticleData> articlesData);

        UpdateOrderOperationResult UpdateWWSOrderNumber(string orderId, string wwsOrderId);


        /// <summary>
        /// Updates the comment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="comment">The comment.</param>
        /// <returns></returns>
        UpdateOrderOperationResult UpdateComment(string orderId, string comment);

        OperationResult CancelOrder(string orderId, bool rejectByCustomer, string reasonText, string cancelBy);

        OperationResult ConfirmOrder(string orderId, string confirmBy);

        OperationResult ApproveOrder(string orderId, string approvedBy);

        OperationResult FixOrder(string orderId, string fixedBy);

        OperationResult UpdateExpirationDate(string orderId, string updatedBy, DateTime newExpirationDate);
    }
}