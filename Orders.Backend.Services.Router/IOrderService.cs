﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Orders.Backend.Services.Router.BackendOrdersNew;
using ArticleData = Orders.Backend.Services.Router.BackendOrdersNew.ArticleData;
using ClientData = Orders.Backend.Services.Router.BackendOrdersNew.ClientData;
using CreateOrderData = Orders.Backend.Services.Router.BackendOrdersNew.CreateOrderData;
using CreateOrderOperationResult = Orders.Backend.Services.Router.BackendOrdersNew.CreateOrderOperationResult;
using DeliveryInfo = Orders.Backend.Services.Router.BackendOrdersNew.DeliveryInfo;
using OperationResult = Orders.Backend.Services.Router.BackendOrdersNew.OperationResult;
using StoreInfo = Orders.Backend.Services.Router.BackendOrdersNew.StoreInfo;
using UpdateOrderData = Orders.Backend.Services.Router.BackendOrdersNew.UpdateOrderData;
using ValidateOrderOperationResult = Orders.Backend.Services.Router.BackendOrdersNew.ValidateOrderOperationResult;
using System.ServiceModel.Web;

namespace Orders.Backend.Services.Router
{
    [ServiceContract]
    public interface IOrderService
    {
        [OperationContract]
        ValidateOrderOperationResult ValidateOrder(StoreInfo storeInfo, DeliveryInfo delivery,
            ICollection<ArticleData> articlesData);

        [OperationContract]
        [WebInvoke(Method = "POST",RequestFormat = WebMessageFormat.Json,ResponseFormat = WebMessageFormat.Json)]
        CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<ArticleData> articlesData);

        [OperationContract]
        CreateOrderOperationResult CreateOrder2(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<InitArticleData> articlesData);

        [OperationContract]
        OperationResult CancelOrder(string orderId, string reasonText, string cancelBy);

        [OperationContract]
        UpdateOrderOperationResult UpdateWWSOrderNumber(string orderNo, string wwsOrderNo);

        [OperationContract]
        OperationResult ConfirmOrder(string orderId, string confirmBy);
        /// <summary>
        /// Updates the order comment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="comment">The comment.</param>
        /// <returns></returns>
        [OperationContract]
        UpdateOrderOperationResult UpdateOrderComment(string orderId, string comment);

        [OperationContract]
        OperationResult ApproveOrder(string orderId, string approvedBy);

        [OperationContract]
        OperationResult FixOrder(string orderId, string approvedBy);

        [OperationContract]
        OperationResult UpdateExpirationDate(string orderId, string updatedBy, DateTime newExpirationDate);
    }
}