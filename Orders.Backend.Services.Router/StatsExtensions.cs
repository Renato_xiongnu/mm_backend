﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders.Backend.Services.Router.BackendOrdersNew;

namespace Orders.Backend.Services.Router
{
    public static class StatsExtensions
    {
        public static string ToStatsOrderSource(this OrderSource orderSource)
        {
            switch (orderSource)
            {
                case OrderSource.CallCenter:
                    return "callcenter";
                case OrderSource.IPhone:
                    return "iphone";
                case OrderSource.MobileWebSite:
                    return "mobile_website";
                case OrderSource.WebSite:
                    return "website";
                case OrderSource.Windows8:
                    return "windows8";
                case OrderSource.Store:
                    return "store";
                case OrderSource.Avito:
                    return "avito";
                case OrderSource.Metro:
                    return "metro";
                case OrderSource.MetroCallCenter:
                    return "metro_callcenter";
                case OrderSource.WebSiteQuickOrder:
                    return "website_quick_order";
                case OrderSource.MobileWebSiteQuickOrder:
                    return "mobile_website_quick_order";
                case OrderSource.YandexMarket:
                    return "yandex_market";
                case OrderSource.IPhoneQuickOrder:
                    return "iphone_quick_order";
                default:
                    return orderSource.ToString().ToLower();
            }
        }

        public static string ToStatsOrderType(this CreateOrderData createOrderData)
        {
            switch (createOrderData.OrderSource)
            {
                case OrderSource.WebSiteQuickOrder:
                case OrderSource.MobileWebSiteQuickOrder:
                case OrderSource.IPhoneQuickOrder:
                    return "quick";
                default:
                    return "standart";
            }
        }

        public static string ToStatsDeliveryType(this DeliveryInfo deliveryData)
        {
            if (deliveryData.HasDelivery)
                return "delivery";

            return "pickup";
        }

        public static string ToStatsPaymentType(this CreateOrderData createOrderData)
        {
            switch (createOrderData.PaymentType)
            {
                case PaymentType.Cash:
                    return "cash";
                case PaymentType.Online:
                    return "online";
                case PaymentType.OnlineCredit:
                    return "online_credit";
                case PaymentType.SocialCard:
                    return "social_card";
                default:
                    return createOrderData.PaymentType.ToString().ToLower();
            }
        }
    }
}