﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NLog;
using Orders.Backend.Services.Router.BackendOrdersNew;
using ReturnCode = Orders.Backend.Services.Router.BackendOrdersNew.ReturnCode;

namespace Orders.Backend.Services.Router
{
    public class OrderService : IOrderService
    {
        private static readonly Logger Logger;

        static OrderService()
        {            
            Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);
        }

        private readonly IRouteService _routeService;
        public OrderService()
        {
            _routeService = new RouteService();
        }
        public ValidateOrderOperationResult ValidateOrder(StoreInfo storeInfo, DeliveryInfo delivery, ICollection<ArticleData> articlesData)
        {
            return null; //_routeService.ValidateOrder(storeInfo, delivery, articlesData);
        }

        public CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData, DeliveryInfo deliveryInfo, ICollection<ArticleData> articlesData)
        {
            Logger.Info(string.Format("CreateOrder starts orderId {0}", orderData.OrderId));
            var result = _routeService.CreateOrder(clientData, orderData, deliveryInfo, articlesData);
            if (result.ReturnCode == ReturnCode.Error)
            {
                Logger.Error("CreateOrder Error. Message: {0} | Id: {1} | Status: {2}", result.ErrorMessage, result.OrderId, result.GeneralOrderStatus);
            }
            else
            {
                Logger.Info("CreateOrder complete. Id: {0} | Status: {1}", result.OrderId, result.GeneralOrderStatus);
            }
            
            return result;
        }

        public CreateOrderOperationResult CreateOrder2(ClientData clientData, CreateOrderData orderData, DeliveryInfo deliveryInfo, ICollection<InitArticleData> articlesData)
        {
            Logger.Info(string.Format("CreateOrder starts orderId {0}", orderData.OrderId));
            var result = _routeService.CreateOrder2(clientData, orderData, deliveryInfo, articlesData);
            Logger.Info(string.Format("CreateOrder stops. Error: {0} | Id: {1} | Status: {2}",
                                        result.ErrorMessage, result.OrderId, result.GeneralOrderStatus));
            return result;
        }

        //public UpdateOrderStatusOperationResult UpdateOrderStatus(BackendOrdersNew.GeneralOrderStatus changeStatusTo, UpdateOrderData orderData)
        //{
        //    Logger.Info(string.Format("UpdateOrderStatus starts to status {0}", changeStatusTo.ToString()));
        //    var result = _routeService.UpdateOrderStatus(changeStatusTo, orderData);
        //    Logger.Info(string.Format("UpdateOrderStatus stops to status {0}", changeStatusTo.ToString()));
        //    return result;
        //}

        public UpdateOrderOperationResult UpdateWWSOrderNumber(string orderNo, string wwsOrderNo)
        {
            Logger.Info(string.Format("UpdateWWSOrderNumber | OrderNo: {0}; WWSOrderNo: {1}", orderNo, wwsOrderNo));
            return _routeService.UpdateWWSOrderNumber(orderNo, wwsOrderNo);
        }

        public OperationResult CancelOrder(string orderId, string reasonText, string cancelBy)
        {
            Logger.Info(string.Format("CancelOrder | OrderNo: {0}; reasonText: {1}; cancelBy: {2}", orderId, reasonText, cancelBy));
            return _routeService.CancelOrder(orderId, false, reasonText, cancelBy);
        }

        public OperationResult ConfirmOrder(string orderId, string confirmBy)
        {
            Logger.Info(string.Format("ConfirmOrder | OrderNo: {0}; confirmBy: {1}", orderId, confirmBy));
            return _routeService.ConfirmOrder(orderId, confirmBy);
        }
        /// <summary>
        /// Updates the order comment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="comment">The comment.</param>
        /// <returns></returns>
        public UpdateOrderOperationResult UpdateOrderComment(string orderId, string comment)
        {
            Logger.Info(string.Format("UpdateOrderComment | OrderId: {0}; Comment: {1}", orderId, comment));
            return _routeService.UpdateComment(orderId, comment); 
        }

        public OperationResult ApproveOrder(string orderId, string approvedBy)
        {
            Logger.Info(string.Format("ApproveOrder | OrderNo: {0}; approvedBy: {1}", orderId, approvedBy));
            return _routeService.ApproveOrder(orderId, approvedBy);
        }

        public OperationResult FixOrder(string orderId, string approvedBy)
        {
            Logger.Info(string.Format("FixOrder | OrderNo: {0}; fixedBy: {1}", orderId, approvedBy));
            return _routeService.FixOrder(orderId, approvedBy);
        }

        public OperationResult UpdateExpirationDate(string orderId, string updatedBy, DateTime newExpirationDate)
        {
            Logger.Info(string.Format("UpdateExpirationDate | OrderNo: {0}; updatedBy: {1}; newExpirationDate: {2}", orderId,
                updatedBy, newExpirationDate));
            return _routeService.UpdateExpirationDate(orderId, updatedBy, newExpirationDate);
        }
    }
}
