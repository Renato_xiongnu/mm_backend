﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Common.Helpers;
using NLog;
using System.Threading;
using MMS.Monitoring.Statistics;
using OnlineOrders.Common.Helpers;
//using Orders.Backend.Services.Router.AvitoOrder;
using Orders.Backend.Router.Infastructure;
using Orders.Backend.Services.Router.BackendOrdersNew;
using Orders.Backend.Services.Router.MetroOrders;
using Orders.Backend.Services.Router.StoreOrder;
using Orders.Backend.Services.Router.Proxy.Wws;
using ArticleData = Orders.Backend.Services.Router.BackendOrdersNew.ArticleData;
using ClientData = Orders.Backend.Services.Router.BackendOrdersNew.ClientData;
using CreateOrderData = Orders.Backend.Services.Router.BackendOrdersNew.CreateOrderData;
using CreateOrderOperationResult = Orders.Backend.Services.Router.BackendOrdersNew.CreateOrderOperationResult;
using DeliveryInfo = Orders.Backend.Services.Router.BackendOrdersNew.DeliveryInfo;
using OperationResult = Orders.Backend.Services.Router.BackendOrdersNew.OperationResult;
using OrderServiceClient = Orders.Backend.Services.Router.BackendOrdersNew.OrderServiceClient;
using OrderSource = Orders.Backend.Services.Router.BackendOrdersNew.OrderSource;
using PaymentType = Orders.Backend.Services.Router.BackendOrdersNew.PaymentType;
using ReturnCode = Orders.Backend.Services.Router.BackendOrdersNew.ReturnCode;
using UpdateOrderData = Orders.Backend.Services.Router.BackendOrdersNew.UpdateOrderData;

namespace Orders.Backend.Services.Router
{
    using ContractTupel = Tuple<ClientData, CreateOrderData, DeliveryInfo, ICollection<ArticleData>>;

    public class RouteService : IRouteService
    {
        private readonly LogDataProvider _dataProvider = new LogDataProvider();
        public RouteService()
        {
            Logger = LogManager.GetLogger(this.GetType().Name);
        }

        public readonly Logger Logger;

        private const string CallByMistakeString = "Site or external system  shouldn't call";

        private static OrderServiceClient GetBackendOrdersClientNew()
        {
            return new OrderServiceClient();
        }

        private static MetroProcessingServiceClient GetMetroClient()
        {
            return new MetroProcessingServiceClient();
        }

        private static BackendOrdersNew.OrderServiceClient GetBackendOrdersClient()
        {
            return new BackendOrdersNew.OrderServiceClient();
        }

        private static InternalOrderProcessingServiceClient GetStoreProcessClient()
        {
            return new InternalOrderProcessingServiceClient();
        }


        private IReadOnlyCollection<string> GetSapCodecollection()
        {
            var sapCodes = ConfigurationManager.AppSettings["SapCodeCollection"];
            return string.IsNullOrEmpty(sapCodes) ? new string[0] : sapCodes.Split(';');
        }

        private bool IsExistingOrder(string orderId)
        {
            bool isProccess;
            return _dataProvider.CheckIfOrderCalledRepeatedly(orderId, out isProccess);
        }

        public CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, ICollection<ArticleData> articlesData)
        {
            if (IsExistingOrder(orderData.OrderId))
            {
                return new CreateOrderOperationResult
                {
                    OrderId = orderData.OrderId,
                    GeneralOrderStatus = GeneralOrderStatus.Created,
                    ReturnCode = ReturnCode.Ok
                };
            }

            CreateOrderOperationResult result;

            var shouldBeProcessed = false;

            var sapCode = orderData.StoreInfo != null
                ? !string.IsNullOrEmpty(orderData.StoreInfo.SapCode)
                    ? orderData.StoreInfo.SapCode
                    : orderData.StoreInfo.McsId
                : string.Empty;
            try
            {
                var enableRouting = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableRouting"]);
                if (enableRouting)
                {
                    shouldBeProcessed =
                        !((orderData.IsPreorder || orderData.IsTest) ||
                          (orderData.PaymentType != PaymentType.Cash &&
                           orderData.PaymentType != PaymentType.OnlineCredit &&
                           orderData.PaymentType != PaymentType.SocialCard &&
                           orderData.PaymentType != PaymentType.Online));
                    if (shouldBeProcessed)
                    {
                        var sapCodes = GetSapCodecollection();
                        if (sapCodes.Count > 0 && !sapCodes.Contains(sapCode))
                        {
                            shouldBeProcessed = false;
                        }
                    }
                }
                result = SaveOrder(clientData, orderData, deliveryInfo, articlesData);
                if (result.ReturnCode == ReturnCode.Ok)
                {
                    var orderTuple = new ContractTupel(clientData, orderData, deliveryInfo, articlesData);

                    var seralizedOrder = DataContractJsonHelper<ContractTupel>.SerializeData(orderTuple);
                    _dataProvider.AddOrder(
                        orderData.OrderId,
                        sapCode,
                        orderData.OrderSource.ToString(),
                        shouldBeProcessed,
                        false,
                        string.Format("Order created at backend with status {0}", result.GeneralOrderStatus),
                        seralizedOrder, string.Empty);
                }
                else
                {
                    LogError(orderData, sapCode, shouldBeProcessed, result.ErrorMessage);
                }

            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();

                LogError(orderData, sapCode, shouldBeProcessed, fullText);

                result = new CreateOrderOperationResult()
                {
                    OrderId = orderData.OrderId,
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }

            SendStatistics(orderData, deliveryInfo, clientData, result);

            return result;
        }

        private void SendStatistics(CreateOrderData orderData, DeliveryInfo deliveryInfo, ClientData clientData, CreateOrderOperationResult result)
        {
            try
            {
                var metrics = new List<StatsMetric>
                {
                    StatsMetric.Counter("customer_orders", 1, null,
                        string.Format("source:{0},type:{1},delivery_type:{2},payment_type:{3},result:{4}",
                            GetRealOrderSource(clientData,orderData).ToStatsOrderSource(),
                            orderData.ToStatsOrderType(), deliveryInfo.ToStatsDeliveryType(),
                            orderData.ToStatsPaymentType(),
                            result.ReturnCode == ReturnCode.Ok
                                ? "success"
                                : "error"))
                };
                StatsEngine.Send(metrics);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, "Failed to send Stats metric: {0}");
            }
        }

        private CreateOrderOperationResult SaveOrder(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo, IEnumerable<ArticleData> articlesData)
        {
            orderData.IsOnlineOrder = orderData.OrderSource != OrderSource.Store
                                      && !string.IsNullOrEmpty(orderData.OrderId);
            CreateOrderOperationResult result;
            var backendOrdersClient = GetBackendOrdersClientNew();
            var newContractOrderData = new CreateOrderData
            {
                IsOnlineOrder = orderData.IsOnlineOrder,
                OrderId = orderData.OrderId,
                CreatedBy = orderData.CreatedBy,
                OrderSource = orderData.OrderSource.ConvertTo<OrderSource>(),
                IsPreorder = orderData.IsPreorder,
                IsTest = orderData.IsTest,
                PaymentType = orderData.PaymentType.ConvertTo<PaymentType>(),
                Comment = orderData.Comment,
                BasketComment = orderData.BasketComment,
                ExternalNumber = orderData.ExternalNumber,
                ExternalSystem = orderData.ExternalSystem.ConvertTo<ExternalSystem>(),
                ExpirationDate = orderData.ExpirationDate,
                StoreInfo = orderData.StoreInfo == null
                    ? null
                    : new BackendOrdersNew.StoreInfo
                    {
                        McsId = orderData.StoreInfo.McsId,
                        SapCode = orderData.StoreInfo.SapCode
                    },
                BasketId = orderData.BasketId,                    
                InitialSapCode = orderData.StoreInfo.SapCode,
                Prepay = orderData.Prepay,
                UtmSource = orderData.UtmSource,
                CouponCode = orderData.CouponCode,
                InstallServiceAddress = orderData.InstallServiceAddress
            };
            var newClientData = new ClientData
            {
                Email = clientData.Email,
                Name = clientData.Name,
                Phone = clientData.Phone,
                Surname = clientData.Surname,
            };
            if (clientData.SocialCard != null)
            {
                newClientData.SocialCard = new CardInfo
                {
                    Number = clientData.SocialCard.Number
                };
            }
            var newDeliveryInfo = new DeliveryInfo
            {
                Address = deliveryInfo.Address,
                City = deliveryInfo.City,
                Latitude = deliveryInfo.Latitude,
                Longitude = deliveryInfo.Longitude,
                HasDelivery = deliveryInfo.HasDelivery,
                PickupLocationId = deliveryInfo.PickupLocationId,
                RequestedDeliveryDate = deliveryInfo.RequestedDeliveryDate,
                RequestedDeliveryTimeslot = deliveryInfo.RequestedDeliveryTimeslot,
                DeliveryDate = deliveryInfo.DeliveryDate,
                RequestedDeliveryServiceOption = deliveryInfo.RequestedDeliveryServiceOption,
                Floor = deliveryInfo.Floor
            };
           
            var articleDatas = articlesData.ToArray();

            PrepareOrder(ref newClientData, ref  newContractOrderData, ref  newDeliveryInfo, ref  articleDatas);

            var newResult = backendOrdersClient.CreateOrder(newClientData, newContractOrderData, newDeliveryInfo,
                articleDatas);

            result = new CreateOrderOperationResult
            {
                ErrorMessage = newResult.ErrorMessage,
                OrderId = newResult.OrderId,
                ReturnCode = newResult.ReturnCode.ConvertTo<ReturnCode>()
            };

            return result;
        }

        //TODO
        private void PrepareOrder(ref ClientData newClientData, ref  CreateOrderData newContractOrderData, ref  DeliveryInfo newDeliveryInfo, ref  ArticleData[] toArray)
        {
            var originalOrderSource = newContractOrderData.OrderSource;
            const string pickupPointTemplate = @"pickup_(?<SapCode>R\d{3})";
            if (newDeliveryInfo != null && !string.IsNullOrEmpty(newDeliveryInfo.PickupLocationId))
            {
                newContractOrderData.StoreInfo.SapCode =
                    Regex.IsMatch(newDeliveryInfo.PickupLocationId, pickupPointTemplate)
                        ? Regex.Match(newDeliveryInfo.PickupLocationId, pickupPointTemplate).Groups["SapCode"].Value
                        : newContractOrderData.StoreInfo.SapCode;
            }

            const string callCenterTemplate = @"^\s?@(?<AgentId>[^@]+)@(?<Surname>.*)";
            if (newClientData != null
                && newContractOrderData.OrderSource != OrderSource.CallCenter
                && !string.IsNullOrEmpty(newClientData.Name)
                && Regex.IsMatch(newClientData.Name, callCenterTemplate))
            {
                newContractOrderData.OrderSource = OrderSource.CallCenter;
                newContractOrderData.CreatedBy = Regex.Replace(newClientData.Name, callCenterTemplate, "${AgentId}").Trim();
                newClientData.Name = Regex.Replace(newClientData.Name, callCenterTemplate, "${Surname}").Trim();
                Logger.Info("{0}: OrderSource Changed {1}=>{2}", newContractOrderData.OrderId, originalOrderSource, newContractOrderData.OrderSource);
            }
            if (newClientData != null
                && newContractOrderData.OrderSource != OrderSource.CallCenter
                && !string.IsNullOrEmpty(newClientData.Surname)
                && Regex.IsMatch(newClientData.Surname, callCenterTemplate))
            {
                newContractOrderData.OrderSource = OrderSource.CallCenter;
                newContractOrderData.CreatedBy = Regex.Replace(newClientData.Surname, callCenterTemplate, "${AgentId}").Trim();
                newClientData.Surname = Regex.Replace(newClientData.Surname, callCenterTemplate, "${Surname}").Trim();
                Logger.Info("{0}: OrderSource Changed {1}=>{2}", newContractOrderData.OrderId, originalOrderSource, newContractOrderData.OrderSource);
            }
            if (newContractOrderData.OrderSource == OrderSource.IPhoneQuickOrder)
            {
                newContractOrderData.OrderSource = OrderSource.WebSiteQuickOrder;
                Logger.Info("{0}: OrderSource Changed {1}=>{2}", newContractOrderData.OrderId, originalOrderSource, newContractOrderData.OrderSource);
            }

            if (newDeliveryInfo != null && string.IsNullOrEmpty(newDeliveryInfo.PickupLocationId) &&
                !newDeliveryInfo.HasDelivery &&
                (newContractOrderData.OrderSource != OrderSource.WebSiteQuickOrder ||
                 newContractOrderData.OrderSource != OrderSource.MobileWebSiteQuickOrder))
            {
                newDeliveryInfo.PickupLocationId = string.Format("pickup_{0}", newContractOrderData.StoreInfo.SapCode);
                Logger.Info("Pickup location changed for order {0}: {1}", newContractOrderData.OrderId,
                    newDeliveryInfo.PickupLocationId);
            }
        }

        private OrderSource GetRealOrderSource(ClientData newClientData, CreateOrderData newContractOrderData)
        {
            const string callCenterTemplate = @"^\s?@(?<AgentId>[^@]+)@(?<Surname>.*)";

            if (newClientData != null
                && newContractOrderData.OrderSource != OrderSource.CallCenter
                && !string.IsNullOrEmpty(newClientData.Name)
                && Regex.IsMatch(newClientData.Name, callCenterTemplate))
            {
                return OrderSource.CallCenter;
            }
            if (newClientData != null
                && newContractOrderData.OrderSource != OrderSource.CallCenter
                && !string.IsNullOrEmpty(newClientData.Surname)
                && Regex.IsMatch(newClientData.Surname, callCenterTemplate))
            {
                return OrderSource.CallCenter;
            }

            return newContractOrderData.OrderSource;
        }

        public CreateOrderOperationResult CreateOrder2(ClientData clientData, CreateOrderData orderData,
            DeliveryInfo deliveryInfo,
            ICollection<InitArticleData> articlesData)
        {
            return CreateOrder(clientData, orderData, deliveryInfo, (ICollection<ArticleData>)articlesData);
        }

        public UpdateOrderOperationResult UpdateComment(string orderId, string comment)
        {
            try
            {
                using (var client = new OrderServiceClient())
                {
                    var result = client.UpdateOrder(new UpdateOrderData
                    {
                        OrderId = orderId,
                        Comment = comment
                    }, null);

                    LogInfo(orderId, string.Format("Change comment to \"{0}\"", comment));

                    return result;
                }
            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();
                LogError(orderId, string.Empty, fullText);
                return new UpdateOrderOperationResult()
                {
                    ReturnCode = BackendOrdersNew.ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }
        }

        private bool CancelProcessOrder(string orderId, bool rejectByCustomer, string reasonText, string cancelBy)
        {
            var processVersion = _dataProvider.GetOrderProcessVersion(orderId);
            var address = SoapServiceHelper.GetServiceAddress(processVersion);
            var rejectedBy = rejectByCustomer
                ? ConfigurationManager.AppSettings["ProcessCancleByCustomerFlag"]
                : cancelBy;
            
            Logger.Info(string.Format("CancelProcessOrder | OrderNo: {0}; reasonText: {1}; rejectedBy: {2}", orderId, reasonText, rejectedBy));
            return SoapServiceHelper.Cancel(new SoapServiceHelper.CancelRequest
            {
                WorkItemId = orderId,
                reasonText = reasonText,
                cancelBy = rejectedBy
            }, address);
        }
        
        public UpdateOrderOperationResult UpdateWWSOrderNumber(string orderId, string wwsOrderId)
        {
            bool isProcessOrder;
            if (!_dataProvider.CheckIfOrderExists(orderId, out isProcessOrder))
            {
                return new UpdateOrderOperationResult
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = string.Format("Order {0} doesn't exists", orderId)
                };
            }

            if (isProcessOrder)
            {
                using (var client = new OrderServiceClient())
                {
                    var result = client.UpdateOrder(new UpdateOrderData
                    {
                        OrderId = orderId,
                        WWSOrderId = wwsOrderId
                    }, null);

                    LogInfo(orderId, string.Format("Change wws number to {0}", wwsOrderId));

                    return result;
                }
            }

            LogError(orderId, string.Empty, CallByMistakeString);

            return new UpdateOrderOperationResult
            {
                ReturnCode = ReturnCode.Error,
                ErrorMessage = CallByMistakeString
            };
        }


        private void SefeExecutor(Action action)
        {
            try
            {
                action();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                var fullError = sb.ToString();
                Logger.Error(fullError, ex);
            }
            catch (Exception ex)
            {
                var fullError = ex.GetExceptionFullInfoText();
                Logger.Error(ex, fullError);
            }
        }

        private void LogInfo(string orderId, string message)
        {
            SefeExecutor(
                () => _dataProvider.LogInfo(orderId, message));
        }

        private void LogError(string orderId, string orderSource, string message)
        {
            SefeExecutor(() => _dataProvider.LogError(orderId, orderSource, message));
        }

        private void LogError(CreateOrderData orderData, string sapCode, bool isProcessOrder, string fullText)
        {
            SefeExecutor(() => _dataProvider.LogError(orderData.OrderId, sapCode, isProcessOrder, orderData.OrderSource.ToString(), fullText));
        }


        public OperationResult CancelOrder(string orderId, bool rejectByCustomer, string reasonText, string cancelBy)
        {
            try
            {
                bool isProcessOrder;
                if (!_dataProvider.CheckIfOrderExists(orderId, out isProcessOrder))
                {
                    return new OperationResult()
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessage = string.Format("Order {0} doesn't exists", orderId)
                    };
                }

                if (isProcessOrder)
                {
                    if (!CancelProcessOrder(orderId, rejectByCustomer, reasonText, cancelBy))
                    {
                        var order = _dataProvider.GetOrder(orderId);
                        var client = GetBackendOrdersClientNew();
                        var orderData = client.GerOrderDataByOrderId(orderId);

                        if (order.CreateDate < new DateTime(2014, 6, 5)
                           && orderData.OrderInfo.StatusInfo != null
                           && orderData.OrderInfo.StatusInfo.Status != BackendOrdersNew.InternalOrderStatus.Rejected
                           && orderData.OrderInfo.StatusInfo.Status !=
                           BackendOrdersNew.InternalOrderStatus.RejectedByCustomer
                           && orderData.OrderInfo.StatusInfo.Status != BackendOrdersNew.InternalOrderStatus.Closed
                           && orderData.OrderInfo.StatusInfo.Status != BackendOrdersNew.InternalOrderStatus.Paid
                            )
                        {
                            if (orderData.ReserveInfo != null && !string.IsNullOrEmpty(orderData.ReserveInfo.WWSOrderId))
                            {
                                ThreadPool.QueueUserWorkItem((o) =>
                                {
                                    var wwsClient = new SalesServiceClient();
                                    wwsClient.CancelSalesOrder(new OrderNumberInfo
                                    {
                                        OrderNumber = orderData.ReserveInfo.WWSOrderId,
                                        SapCode = orderData.ReserveInfo.SapCode
                                    });
                                });
                            }
                            var result = client.UpdateOrder(new BackendOrdersNew.UpdateOrderData()
                            {
                                OrderId = orderId,
                                ChangeStatusTo = GeneralOrderStatus.Rejected,
                                ReasonText = reasonText,
                                UpdatedBy = cancelBy
                            }, null);
                            return new OperationResult
                            {
                                ReturnCode =
                                    result.ReturnCode == BackendOrdersNew.ReturnCode.Ok
                                        ? ReturnCode.Ok
                                        : ReturnCode.Error,
                                ErrorMessage = result.ErrorMessage
                            };
                        }
                        //---------------------------
                        return new OperationResult
                        {
                            ReturnCode = ReturnCode.Error,
                            ErrorMessage = "Не удалось отменить заказ",
                        };
                    }
                }
                else
                {
                    var client = GetBackendOrdersClientNew();
                    var result = client.UpdateOrder(new BackendOrdersNew.UpdateOrderData()
                    {
                        OrderId = orderId,
                        ChangeStatusTo = GeneralOrderStatus.Rejected,
                        ReasonText = reasonText,
                        UpdatedBy = cancelBy
                    }, null);
                    return new OperationResult
                    {
                        ReturnCode =
                            result.ReturnCode == BackendOrdersNew.ReturnCode.Ok
                                ? ReturnCode.Ok
                                : ReturnCode.Error,
                        ErrorMessage = result.ErrorMessage
                    };

                }
                return new OperationResult()
                {
                    ReturnCode = ReturnCode.Ok
                };
            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();
                LogError(orderId, "", fullText);
                return new OperationResult()
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }
        }

        public OperationResult ConfirmOrder(string orderId, string confirmBy)
        {
            try
            {
                bool isProcessOrder;
                if (!_dataProvider.CheckIfOrderExists(orderId, out isProcessOrder))
                {
                    return new OperationResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessage = string.Format("Order {0} doesn't exists", orderId)
                    };
                }
                var updateOrderData = new UpdateOrderData
                {
                    OrderId = orderId,
                    UpdatedBy = confirmBy
                };
                return new OperationResult
                {
                    ReturnCode = ReturnCode.Ok
                };
            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();
                LogError(orderId, "", fullText);
                return new OperationResult()
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }
        }

        public OperationResult ApproveOrder(string orderId, string approvedBy)
        {
            try
            {
                bool isProcessOrder;
                if (!_dataProvider.CheckIfOrderExists(orderId, out isProcessOrder))
                {
                    return new OperationResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessage = string.Format("Order {0} doesn't exists", orderId)
                    };
                }

                var processVersion = _dataProvider.GetOrderProcessVersion(orderId);
                var address = SoapServiceHelper.GetServiceAddress(processVersion);

                Logger.Info(string.Format("ApproveOrder | OrderNo: {0}; approvedBy: {1}", orderId,
                    approvedBy));
                var result = SoapServiceHelper.ApproveOrder(new SoapServiceHelper.ApproveOrder_External
                {
                    WorkItemId = orderId,
                    ApprovedBy = approvedBy

                }, address);

                return new OperationResult
                {
                    ReturnCode = result ? ReturnCode.Ok : ReturnCode.Error
                };
            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();
                LogError(orderId, "", fullText);
                return new OperationResult()
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }
        }

        public OperationResult FixOrder(string orderId, string fixedBy)
        {
            try
            {
                bool isProcessOrder;
                if (!_dataProvider.CheckIfOrderExists(orderId, out isProcessOrder))
                {
                    return new OperationResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessage = string.Format("Order {0} doesn't exists", orderId)
                    };
                }

                var processVersion = _dataProvider.GetOrderProcessVersion(orderId);
                var address = SoapServiceHelper.GetServiceAddress(processVersion);

                Logger.Info(string.Format("FixOrder | OrderNo: {0}; fixedBy: {1}", orderId,
                    fixedBy));
                var result = SoapServiceHelper.FixOrder(new SoapServiceHelper.FixOrder_External
                {
                    WorkItemId = orderId,
                    FixedBy = fixedBy
                }, address);

                return new OperationResult
                {
                    ReturnCode = result ? ReturnCode.Ok : ReturnCode.Error
                };
            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();
                LogError(orderId, "", fullText);
                return new OperationResult()
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }
        }

        public OperationResult UpdateExpirationDate(string orderId, string updatedBy, DateTime newExpirationDate)
        {
            try
            {
                bool isProcessOrder;
                if (!_dataProvider.CheckIfOrderExists(orderId, out isProcessOrder))
                {
                    return new OperationResult
                    {
                        ReturnCode = ReturnCode.Error,
                        ErrorMessage = string.Format("Order {0} doesn't exists", orderId)
                    };
                }

                using (var client = new OrderServiceClient())
                {
                    var updateOrderResult = client.UpdateOrder(new BackendOrdersNew.UpdateOrderData
                    {
                        OrderId = orderId,
                        ExpirationDate = newExpirationDate
                    }, null);

                    LogInfo(orderId, string.Format("Change expiration date to \"{0}\"", newExpirationDate));

                    if (updateOrderResult.ReturnCode == ReturnCode.Error)
                    {
                        return new OperationResult
                        {
                            ReturnCode = ReturnCode.Error,
                            ErrorMessage = updateOrderResult.ErrorMessage
                        };
                    }
                }

                var processVersion = _dataProvider.GetOrderProcessVersion(orderId);
                var address = SoapServiceHelper.GetServiceAddress(processVersion);

                Logger.Info(string.Format("UpdateOrderExpiration | OrderNo: {0}; updatedBy: {1}", orderId,
                    updatedBy));
                var result = SoapServiceHelper.UpdateExpirationDateOrder(new SoapServiceHelper.UpdateOrderExpiration_External
                {
                    WorkItemId = orderId,
                    UpdatedBy = updatedBy
                }, address);

                return new OperationResult
                {
                    ReturnCode = result ? ReturnCode.Ok : ReturnCode.Error
                };
            }
            catch (Exception ex)
            {
                var fullText = ex.GetExceptionFullInfoText();
                LogError(orderId, "", fullText);
                return new OperationResult()
                {
                    ReturnCode = ReturnCode.Error,
                    ErrorMessage = fullText
                };
            }
        }
    }
}