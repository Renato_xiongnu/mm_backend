﻿using System;
using NLog;

namespace Orders.Backend.Services.Router
{
    public class Global : System.Web.HttpApplication
    {

        private Logger _logger;

        protected void Application_Start(object sender, EventArgs e)
        {
            _logger = LogManager.GetLogger("RouterHttpApplication");
            _logger.Info("Router Application_Start");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            _logger.Error(ex, "Router Application_Error");
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
