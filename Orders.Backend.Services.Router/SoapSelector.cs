﻿using System;
using Orders.Backend.Services.Router.BackendOrdersNew;

namespace Orders.Backend.Services.Router
{
    public class SoapSelector
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderSource"></param>
        /// <param name="paymentType"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <returns></returns>
        public string GetProcessService(OrderSource orderSource, PaymentType paymentType)
        {
            switch (orderSource)
            {
                case OrderSource.Store:
                    {
                        return "IInternalOrderProcessingService";
                    }
                case OrderSource.Avito:
                    {
                        return "IAvitoOrdersService";
                    }
                case OrderSource.MetroCallCenter:
                case OrderSource.Metro:
                    {
                        return "IMetroProcessingService";
                    }
                default:
                    switch (paymentType)
                    {
                        case PaymentType.OnlineCredit:
                        case PaymentType.Cash:
                            {
                                return "IOrderProcessingService";
                            }
                        case PaymentType.Online:
                            {
                                return "IOnlinePaymentProcessingService";
                            }
                        default:
                            {
                                throw new ArgumentOutOfRangeException("paymentType");
                            }
                    }
            }
        }
    }
}