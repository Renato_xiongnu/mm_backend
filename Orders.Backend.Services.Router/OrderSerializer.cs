﻿using System;
using System.Collections.Generic;
using System.Globalization;
using OnlineOrders.Common.Helpers;
using Orders.Backend.Services.Router.BackendOrdersNew;

namespace Orders.Backend.Services.Router
{
    public class OrderSerializer
    {
        public static Dictionary<string, string> SerializeOrder(CreateOrderData orderData, DeliveryInfo deliveryInfo, ClientData clientData, ICollection<ArticleData> articlesData)
        {
            var args = new Dictionary<string, string>
                    {
                        {"clientData.Name", clientData.Name},
                        {"clientData.Phone", clientData.Phone},
                        {"clientData.Phone2", clientData.Phone2},
                        {"clientData.Surname", clientData.Surname},
                        {"clientData.LastName", clientData.LastName},
                        {"clientData.Email", clientData.Email},
                        {"clientData.Fax", clientData.Fax},
                        {"clientData.Gender", clientData.Gender},
                        {"clientData.BirthDay", Convert.ToString(clientData.Birthday)},
                        {"clientData.ZztCustomerId", Convert.ToString(clientData.ZZTCustomerId)},
                        {"orderData.OrderId", orderData.OrderId},
                        {"orderData.OrderSource", orderData.OrderSource.ToString()},
                        {"orderData.PaymentType", orderData.PaymentType.ToString()},
                        {"orderData.FinalPaymentType", orderData.PaymentType.ToString()},
                        {"orderData.Created", DateTime.Now.ToUniversalTime().ToString(CultureInfo.InvariantCulture)},
                        {"orderData.CreatedBy", orderData.CreatedBy},
                        {"orderData.IsTest", orderData.IsTest.ToString()},
                        {"orderData.IsPreorder", orderData.IsPreorder.ToString()},
                        {"orderData.StoreInfo.McsId", orderData.StoreInfo.McsId},
                        {"orderData.StoreInfo.SapCode", orderData.StoreInfo.SapCode},
                        {"orderData.Comment", orderData.Comment},
                        {"orderData.BasketComment", orderData.BasketComment},
                        {"orderData.ExternalNumber", orderData.ExternalNumber},
                        {"orderData.ExternalSystem", orderData.ExternalSystem.ToString()},
                        {
                            "orderData.ExpirationDate",
                            orderData.ExpirationDate.HasValue
                                ? orderData.ExpirationDate.ToString()
                                : DateTime.MinValue.ToString(CultureInfo.InvariantCulture)
                        },

                        {
                            "orderData.IsOnlineOrder",
                            (orderData.OrderSource != OrderSource.Store
                             && !string.IsNullOrEmpty(orderData.OrderId)).ToString()
                        },
                        {"deliveryInfo.HasDelivery", deliveryInfo.HasDelivery.ToString()},
                        {"deliveryInfo.City", deliveryInfo.City},
                        {"deliveryInfo.Address", deliveryInfo.Address},
                        {"deliveryInfo.ExactAddress", deliveryInfo.ExactAddress},
                        {
                            "deliveryInfo.DeliveryDate",
                            deliveryInfo.DeliveryDate.HasValue
                                ? deliveryInfo.DeliveryDate.ToString()
                                : DateTime.MinValue.ToString(CultureInfo.InvariantCulture)
                        },
                        {"deliveryInfo.Comment", deliveryInfo.Comment},
                        {"deliveryInfo.Period", deliveryInfo.Period},
                        {"deliveryInfo.Latitude", deliveryInfo.Latitude.ToString()},
                        {"deliveryInfo.Longitude", deliveryInfo.Longitude.ToString()},
                    };

            if (orderData.SalesDocumentData != null)
            {
                args.Add("orderData.OutletInfo", orderData.SalesDocumentData.OutletInfo);
                args.Add("orderData.ProductPickupInfo", orderData.SalesDocumentData.ProductPickupInfo);
                args.Add("orderData.PrintableInfo", orderData.SalesDocumentData.PrintableInfo);
                args.Add("orderData.SalesPersonId", Convert.ToString(orderData.SalesDocumentData.SalesPersonId));
            }

            if (orderData.ZZTData != null)
            {
                args.Add("zztData.CampaignId", orderData.ZZTData.CampaignId);
                args.Add("zztData.ConsumerId", orderData.ZZTData.ConsumerId);
                args.Add("zztData.ZztOrderId", orderData.ZZTData.ZztOrderId);
                args.Add("zztData.RecipientName", orderData.ZZTData.RecipientName);

                if (orderData.ZZTData.DeliveryInfo != null)
                {
                    args.Add("zztDeliveryInfo.ClimbPrice",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.ClimbPrice));
                    args.Add("zztDeliveryInfo.DeliveryDate",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.DeliveryDate));
                    args.Add("zztDeliveryInfo.DeliveryPlaceId",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.DeliveryPlaceId));
                    args.Add("zztDeliveryInfo.DeliveryPlaceSapCode",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.DeliveryPlaceSapCode));
                    args.Add("zztDeliveryInfo.DeliveryPrice",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.DeliveryPrice));
                    args.Add("zztDeliveryInfo.DeliveryTime",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.DeliveryTime));
                    args.Add("zztDeliveryInfo.DeliveryType",
                        Convert.ToString(orderData.ZZTData.DeliveryInfo.DeliveryType));
                }

                if (orderData.ZZTData.DeliveryAddress != null)
                {
                    args.Add("zztDeliveryAddress.Additional",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Additional));
                    args.Add("zztDeliveryAddress.Building",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Building));
                    args.Add("zztDeliveryAddress.City",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.City));
                    args.Add("zztDeliveryAddress.CityId",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.CityId));
                    args.Add("zztDeliveryAddress.RegionId",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.RegionId));
                    args.Add("zztDeliveryAddress.Country",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Country));
                    args.Add("zztDeliveryAddress.District",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.District));
                    args.Add("zztDeliveryAddress.Entrance",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Entrance));
                    args.Add("zztDeliveryAddress.EntranceCode",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.EntranceCode));
                    args.Add("zztDeliveryAddress.Flat",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Flat));
                    args.Add("zztDeliveryAddress.Floor",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Floor));
                    args.Add("zztDeliveryAddress.HasElevator",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.HasElevator));
                    args.Add("zztDeliveryAddress.House",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.House));
                    args.Add("zztDeliveryAddress.Housing",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Housing));
                    args.Add("zztDeliveryAddress.MetroStation",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.MetroStation));
                    args.Add("zztDeliveryAddress.PostalCode",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.PostalCode));
                    args.Add("zztDeliveryAddress.Region",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Region));
                    args.Add("zztDeliveryAddress.Street",
                        Convert.ToString(orderData.ZZTData.DeliveryAddress.Street));
                }
            }

            var i = 0;
            foreach (var data in articlesData)
            {
                var id = string.Format("line_{0}", i);
                args.Add(string.Format("{0}.{1}", id, "ArticleNum"), data.ArticleNum);
                args.Add(string.Format("{0}.{1}", id, "Price"),
                    data.Price.ToString(CultureInfo.InvariantCulture));
                args.Add(string.Format("{0}.{1}", id, "Qty"),
                    data.Qty.ToString(CultureInfo.InvariantCulture));
                args.Add(string.Format("{0}.{1}", id, "ItemState"), data.ItemState.ToString());
                var promotions = data.Promotions;
                var prs = promotions.ToJson();
                args.Add(string.Format("{0}.{1}", id, "Promotions"), prs);
                args.Add(string.Format("{0}.{1}", id, "Title"), data.Title);

                var initArticleData = data as InitArticleData;
                if (initArticleData == null)
                {
                    i++;
                    continue;
                }

                args.Add(string.Format("{0}.{1}", id, "HasShippedFromStock"),
                    initArticleData.HasShippedFromStock.ToString());
                args.Add(string.Format("{0}.{1}", id, "SerialNumber"),
                    initArticleData.SerialNumber);
                args.Add(string.Format("{0}.{1}", id, "WarrantyInsuranceArticle"),
                    initArticleData.WarrantyInsuranceArticle);
                args.Add(string.Format("{0}.{1}", id, "StockNumber"),
                    Convert.ToString(initArticleData.StockNumber));

                i++;
                if (initArticleData.SubArticleData != null)
                {
                    foreach (var subArticleData in initArticleData.SubArticleData)
                    {
                        var subId = string.Format("line_{0}", i);
                        args.Add(string.Format("{0}.{1}", subId, "ArticleNum"), subArticleData.ArticleNum);
                        args.Add(string.Format("{0}.{1}", subId, "Price"),
                            subArticleData.Price.ToString(CultureInfo.InvariantCulture));
                        args.Add(string.Format("{0}.{1}", subId, "Qty"),
                            subArticleData.Qty.ToString(CultureInfo.InvariantCulture));
                        args.Add(string.Format("{0}.{1}", subId, "ItemState"),
                            subArticleData.ItemState.ToString());
                        promotions = subArticleData.Promotions;
                        prs = promotions.ToJson();
                        args.Add(string.Format("{0}.{1}", subId, "Promotions"), prs);
                        args.Add(string.Format("{0}.{1}", subId, "HasShippedFromStock"),
                            subArticleData.HasShippedFromStock.ToString());
                        args.Add(string.Format("{0}.{1}", subId, "SerialNumber"),
                            subArticleData.SerialNumber);
                        args.Add(string.Format("{0}.{1}", subId, "ParentId"), id);
                        args.Add(string.Format("{0}.{1}", subId, "WarrantyInsuranceArticle"),
                            subArticleData.WarrantyInsuranceArticle);
                        args.Add(string.Format("{0}.{1}", subId, "StockNumber"),
                            Convert.ToString(subArticleData.StockNumber));

                        i++;
                    }
                }
            }

            return args;
        }
    }
}