﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Orders.Backend.Services.Order.Notification;

namespace Orders.Backend.Admin.NotificationFiller
{
    class Program
    {
        static void Main(string[] args)
        {
           /* using (LightProvider provider = new LightProvider(true))
            {
                var orderIds = provider.GetBadConfirmOrderIds();
                int i = 1;
                foreach (var orderId in orderIds)
                {
                    Console.WriteLine(i + ") Order Id = '" + orderId + "'");
                    
                    var header = provider.GetOrderDataByOrderId(orderId, RelatedEntity.OrderLines | RelatedEntity.GiftCert | RelatedEntity.Customer | RelatedEntity.Delivery);
                    OrderStateConfirmed.SendNotification(header);
                    Console.WriteLine(" customer = " + header.Customer.Name);
                    i++;
                }
            }
            using (LightProvider provider = new LightProvider(true))
            {
                var orderIds = provider.GetBadCertifOrderIds();
                int i = 1;
                foreach (var orderId in orderIds)
                {
                    Console.WriteLine(i + ") Order Id = '" + orderId + "'");

                    var header = provider.GetOrderDataByOrderId(orderId, RelatedEntity.OrderLines | RelatedEntity.GiftCert | RelatedEntity.Customer | RelatedEntity.Delivery);
                    OrderStateGiftCertCredited.SendNotification(header);
                    Console.WriteLine(" customer = " + header.Customer.Name);
                    i++;
                }
            }*/
            
            Program pr = new Program();
            
            /*var type = MetaDataSetType.ConfirmForManager2;
            NotificationManager.RegisterEvent(type);
            NotificationManager.RegisterEventPatterns(type);
            string notification1Id = "";
            var parameters = pr.GetConfirmForManager2Parameters();
            notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);*/
            /*
            var type = MetaDataSetType.ConfirmForManager3;
            NotificationManager.RegisterEventByObject(type);
            NotificationManager.RegisterEventPatterns(type);
            string notification1Id = "02736180-581c-45c2-b0f6-61c12f72c06f";
            var parameters = pr.GetConfirmForManager3Parameters();
            notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"a3cdee97-4458-44c6-ab07-b4cc251105ec"test
            //"02736180-581c-45c2-b0f6-61c12f72c06f"prod
            */

            
           /* var type = MetaDataSetType.ConfirmForManager4;
            NotificationManager.RegisterEventByObject(type);
            NotificationManager.RegisterEventPatterns(type);
            string notification1Id = "";
            var parameters = pr.GetConfirmForManager4Parameters(true);
            notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"d4559e6b-5e0f-4930-8073-f4560fa1ac72"test
            //"82fa144a-f2f4-472e-ae32-630ab37bdfb3"prod
            */
            /*
            var type = MetaDataSetType.InstantloanWarning;
            NotificationManager.RegisterEventByObject(type);
            NotificationManager.RegisterEventPatterns(type);
            string notification1Id = "";
            var parameters = pr.GetInstantloanParameters("Warning", "Instantloan: Предупреждение", "sergey.prokofyev@gmail.com");
            notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"f50c2deb-ea20-463a-bb84-bb8095a507d1"test
            //"5dcfa052-6679-4af6-966f-17a441be2484"prod
            */
            /*
            var type = MetaDataSetType.InstantloanError;
            NotificationManager.RegisterEventByObject(type);
            NotificationManager.RegisterEventPatterns(type);
            string notification1Id = "";
            var parameters = pr.GetInstantloanParameters("Error", "Instantloan: Ошибка", "sergey.prokofyev@gmail.com");
            notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"01f3c3cb-dfe4-4f02-85d6-729f08b56af6"test
            //"e91f0260-e6d8-4b8b-a9a9-4adf648948f7"prod
             */
              
            /*
            var type = MetaDataSetType.InstantloanException;
            NotificationManager.RegisterEventByObject(type);
            NotificationManager.RegisterEventPatterns(type);
            string notification1Id = "";
            var parameters = pr.GetInstantloanParameters("Exception", "Instantloan: Исключение", "sergey.prokofyev@gmail.com");
            notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"15384ec0-fe51-4555-a159-0dcfd182bde3"test
            //"c6eaa125-e314-42b4-a523-53b1630714ae"prod
            */
            //Cancel_StoreTemplate
            //var type = MetaDataSetType.CancelWithReturnToStoreForManager;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "5488066b-fe4a-44c6-b74f-5fe503296ca1";
            //var parameters = pr.GetCancelWithReturnToStoreForManagerParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //		"58edda19-5e33-4aeb-86f9-876dfa197f5b"	//test
            //      "5488066b-fe4a-44c6-b74f-5fe503296ca1"  //prod

            //CancelWorkflow_StoreTemplate
            //var type = MetaDataSetType.CancelWithoutReturnToStoreForManager;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "62ca7ff5-dffb-46c0-b4b3-f928188b116c";
            //var parameters = pr.GetCancelWithoutReturnToStoreForManagerParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"62ca7ff5-dffb-46c0-b4b3-f928188b116c" prod

            //Change_StoreTemplate
            //var type = MetaDataSetType.ChangeStoreForManager;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "69b81cdd-aa8f-4555-ba7b-ddb0f5e83b6d";
            //var parameters = pr.GetChangeStoreForManagerParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"69b81cdd-aa8f-4555-ba7b-ddb0f5e83b6d" prod

            //ChangeWwsOrder_StoreTemplate
            //var type = MetaDataSetType.ChangeSalesDocumentForManager;

            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "fbf675bd-ce5a-4ead-933d-3d57390889fa";
            //var parameters = pr.GetChangeWwsOrderIdForManagerParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            
            //var notification = new NotificationManager();
            //var ds = NotificationManager.GetMetaDataSetsObject(type);
            //var result = notification.SendJsonString(MetaDataSetType.ChangeSalesDocumentForManager, 
            //    NotificationManager.SerializeObject(ds), "alwag85@gmail.com",null);


            return;            
            
            

            

            //fbf675bd-ce5a-4ead-933d-3d57390889fa

            //notification1Id = "dd666791-1e7e-4395-a005-e5345dfd5fa4" - CustomerConfirmDelivery prod
            
            //Delivery_StoreTemplate
            //var type = MetaDataSetType.DeliveryApproveForManager;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "6b491d84-cdb8-4fd5-8292-b9a707828436";
            //var parameters = pr.GetDeliveryApproveForManagerParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //      "6b491d84-cdb8-4fd5-8292-b9a707828436" prod

            ////New_StoreTemplate
            //var type = MetaDataSetType.NewOrderCollectForManager;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "b5e306ab-31a2-4102-b385-60554bb39e4a";
            //var parameters = pr.GetNewOrderCollectForManagerParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"b5e306ab-31a2-4102-b385-60554bb39e4a" prod
            /*
            */

            //CustomerConfirmDelivery
            //var type = MetaDataSetType.CustomerConfirmDelivery;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "";
            //var parameters = pr.GetCustomerConfirmDeliveryParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"dd666791-1e7e-4395-a005-e5345dfd5fa4" prod

            //CustomerConfirmWithoutKupon
            //var type = MetaDataSetType.CustomerConfirmWithoutKupon;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "14843879-e402-4b79-94c6-9601448f6119";
            //var parameters = pr.GetCustomerConfirmWithoutKuponParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"14843879-e402-4b79-94c6-9601448f6119" prod

            //CustomerConfirm
            //var type = MetaDataSetType.CustomerConfirm;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "";
            //var parameters = pr.GetCustomerConfirmParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);

            //CustomerCertificate
            //var type = MetaDataSetType.CustomerCertificate;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "";
            //var parameters = pr.GetCustomerCertificateParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"96f6f912-0f45-4d2c-bd4b-5e96ad4239b2" prod

            //CustomerStoreCancelReserve
            //var type = MetaDataSetType.CustomerStoreCancelReserve;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "1be800e0-c2ae-4ae4-aae6-8c6f357fe4b0";
            //var parameters = pr.GetCustomerStoreCancelReserveParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"1be800e0-c2ae-4ae4-aae6-8c6f357fe4b0" prod

            //CustomerUserCancelReserve
            //var type = MetaDataSetType.CustomerCustomerCancelReserve;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "0af708e9-1ad8-41c9-b61e-bb51a61003a7";
            //var parameters = pr.GetCustomerUserCancelReserveParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"0af708e9-1ad8-41c9-b61e-bb51a61003a7" prod

            //ErrorCancelReserve
            //var type = MetaDataSetType.CustomerErrorCancelReserve;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "e3024c27-7757-43ac-b522-240c40e113ba";
            //var parameters = pr.GetCustomerErrorCancelReserveParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"e3024c27-7757-43ac-b522-240c40e113ba" prod

            //CreditBrokerApproval
            //var type = MetaDataSetType.CreditBrokerApproval;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "";
            //var parameters = pr.GetCreditBrokerApprovalParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"72d27927-83b4-460f-8789-7a9629262cfa" prod

            //GsBtGCard
            //var type = MetaDataSetType.GsBtGiftCard;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "c2b2246a-801f-47b5-ace6-f4005b4afc23";
            //var parameters = pr.GetGsBtGiftCardParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"c2b2246a-801f-47b5-ace6-f4005b4afc23" prod

            //CustomerFeedback
            //var type = MetaDataSetType.CustomerFeedback;
            //NotificationManager.RegisterEventByObject(type);
            //NotificationManager.RegisterEventPatterns(type);
            //var notification1Id = "4a7cbf54-90d3-4e2a-ac00-aae1f2da0f81";
            //var parameters = pr.GetCustomerFeedbackParameters();
            //notification1Id = pr.CreateOrUpdateNotification(notification1Id, type, parameters);
            //"4a7cbf54-90d3-4e2a-ac00-aae1f2da0f81" prod
            //"6219db39-1742-4686-baed-393d3dc4b792" test
            return;

            bool update = false;
            string confirmId = string.Empty;
            string confirmWithoutKuponId = string.Empty;
            string confirmDeliveryId = string.Empty;
            string confirmForManagerId = string.Empty;
            string confirmForManager2Id = string.Empty;
            string confirmForManager3Id = string.Empty;
            string confirmForManager4Id = string.Empty;
            string certificateId = string.Empty;
            string systemId = string.Empty;
            string storeCancelReserveId = string.Empty;
            string userCancelReserveId = string.Empty;
            string errorCancelReserveId = string.Empty;
            foreach (var a in args)
            {
                if (a.Equals("-update", StringComparison.CurrentCultureIgnoreCase))
                {
                    update = true;
                }
                if (a.StartsWith("-confirm:"))
                {
                    confirmId = a.Replace("-confirm:", string.Empty);
                }
                if (a.StartsWith("-confirmWithoutKupon:"))
                {
                    confirmWithoutKuponId = a.Replace("-confirmWithoutKupon:", string.Empty);
                }
                if (a.StartsWith("-confirmDelivery:"))
                {
                    confirmDeliveryId = a.Replace("-confirmDelivery:", string.Empty);
                }
                if (a.StartsWith("-confirmForManager:"))
                {
                    confirmForManagerId = a.Replace("-confirmForManager:", string.Empty);
                }
                if (a.StartsWith("-confirmForManager2:"))
                {
                    confirmForManager2Id = a.Replace("-confirmForManager2:", string.Empty);
                }
                if (a.StartsWith("-confirmForManager3:"))
                {
                    confirmForManager3Id = a.Replace("-confirmForManager3:", string.Empty);
                }
                if (a.StartsWith("-confirmForManager4:"))
                {
                    confirmForManager4Id = a.Replace("-confirmForManager4:", string.Empty);
                }
                if (a.StartsWith("-certificate:"))
                {
                    certificateId = a.Replace("-certificate:", string.Empty);
                }
                if (a.StartsWith("-system:"))
                {
                    systemId = a.Replace("-system:", string.Empty);
                }
                if (a.StartsWith("-storeCancelReserve:"))
                {
                    storeCancelReserveId = a.Replace("-storeCancelReserve:", string.Empty);
                }
                if (a.StartsWith("-userCancelReserve:"))
                {
                    userCancelReserveId = a.Replace("-userCancelReserve:", string.Empty);
                }
                if (a.StartsWith("-errorCancelReserve:"))
                {
                    errorCancelReserveId = a.Replace("-errorCancelReserve:", string.Empty);
                }
            }
            if (update)
            {
                if (string.IsNullOrEmpty(confirmId) || string.IsNullOrEmpty(confirmWithoutKuponId) || string.IsNullOrEmpty(confirmDeliveryId)
                    || string.IsNullOrEmpty(confirmForManagerId) || string.IsNullOrEmpty(confirmForManager2Id) || string.IsNullOrEmpty(confirmForManager3Id) || string.IsNullOrEmpty(confirmForManager4Id) 
                    || string.IsNullOrEmpty(certificateId) || string.IsNullOrEmpty(systemId)
                    || string.IsNullOrEmpty(storeCancelReserveId) || string.IsNullOrEmpty(userCancelReserveId) || string.IsNullOrEmpty(errorCancelReserveId))
                {
                    Console.WriteLine("Bad notification Ids!");
                }
            }



            NotificationManager.RegisterRuleProvider();
            NotificationManager.RegisterEventProvider();

            NotificationManager.RegisterEvent(MetaDataSetType.Confirm);
            NotificationManager.RegisterEvent(MetaDataSetType.ConfirmWithoutKupon);
            NotificationManager.RegisterEvent(MetaDataSetType.ConfirmDelivery);
            NotificationManager.RegisterEvent(MetaDataSetType.ConfirmForManager);
            NotificationManager.RegisterEvent(MetaDataSetType.ConfirmForManager2);
            NotificationManager.RegisterEventByObject(MetaDataSetType.ConfirmForManager3);
            NotificationManager.RegisterEventByObject(MetaDataSetType.ConfirmForManager4);
            NotificationManager.RegisterEvent(MetaDataSetType.Certificate);
            NotificationManager.RegisterEvent(MetaDataSetType.HQ_ResponsibleForCertificate);
            NotificationManager.RegisterEvent(MetaDataSetType.StoreCancelReserve);
            NotificationManager.RegisterEvent(MetaDataSetType.CustomerCancelReserve);
            NotificationManager.RegisterEvent(MetaDataSetType.ErrorCancelReserve);

            NotificationManager.RegisterEventPatterns(MetaDataSetType.Confirm);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ConfirmWithoutKupon);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ConfirmDelivery);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ConfirmForManager);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ConfirmForManager2);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ConfirmForManager3);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ConfirmForManager4);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.Certificate);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.HQ_ResponsibleForCertificate);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.StoreCancelReserve);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.CustomerCancelReserve);
            NotificationManager.RegisterEventPatterns(MetaDataSetType.ErrorCancelReserve);


            if (update)
            {
                pr.Task06UpdateNotificationAndRules(confirmId, confirmWithoutKuponId, confirmDeliveryId,
                    confirmForManagerId, confirmForManager2Id, confirmForManager3Id, confirmForManager4Id,
                    certificateId, 
                    systemId,
                    storeCancelReserveId, userCancelReserveId, errorCancelReserveId);
            }
            else
            {
                pr.Task05CreateNotificationAndRules(confirmId, confirmWithoutKuponId, confirmDeliveryId,
                    confirmForManagerId, confirmForManager2Id, confirmForManager3Id, confirmForManager4Id,
                    certificateId, 
                    systemId,
                    storeCancelReserveId, userCancelReserveId, errorCancelReserveId);
            }

        }

        public void Task05CreateNotificationAndRules(string confirmNotificationId, string confirmWithoutKuponNotificationId,
            string confirmDeliveryNotificationId, string confirmForManagerId, string confirmForManager2Id, string confirmForManager3Id, string confirmForManager4Id,
            string certificateNotificationId, 
            string systemNotificationId,
            string storeCancelReserveNotificationId, string userCancelReserveNotificationId, string errorCancelReserveNotificationId)
        {
            string notificationId = confirmNotificationId;
            NotificationParameters parameters = GetConfirmParameters();
            var type = MetaDataSetType.Confirm;
            notificationId = this.CreateOrUpdateNotification(notificationId, type, parameters);

            string notification1Id = confirmWithoutKuponNotificationId;
            parameters = this.GetConfirmWithoutKuponParameters();
            type = MetaDataSetType.ConfirmWithoutKupon;
            notification1Id = this.CreateOrUpdateNotification(notification1Id, type, parameters);

            string notification7Id = confirmDeliveryNotificationId;
            parameters = this.GetConfirmDeliveryParameters();
            type = MetaDataSetType.ConfirmDelivery;
            notification7Id = this.CreateOrUpdateNotification(notification7Id, type, parameters);

            var notification8Id = confirmForManagerId;
            parameters = this.GetConfirmForManagerParameters();
            notification8Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager, notification8Id, parameters);

            var notification9Id = confirmForManager2Id;
            parameters = this.GetConfirmForManager2Parameters();
            notification9Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager2, notification9Id, parameters);

            var notification10Id = confirmForManager3Id;
            parameters = this.GetConfirmForManager3Parameters();
            notification10Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager3, notification10Id, parameters);

            var notification11Id = confirmForManager4Id;
            parameters = this.GetConfirmForManager4Parameters(true);
            notification11Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager4, notification11Id, parameters);

            string notification2Id = certificateNotificationId;
            parameters = this.GetCertificateParameters();
            type = MetaDataSetType.Certificate;
            notification2Id = this.CreateOrUpdateNotification(notification2Id, type, parameters);

            var notification3Id = systemNotificationId;
            parameters = this.GetSystemParameters();
            type = MetaDataSetType.HQ_ResponsibleForCertificate;
            notification3Id = this.CreateOrUpdateNotification(notification3Id, type, parameters);

            var notification4Id = storeCancelReserveNotificationId;
            parameters = this.GetStoreCancelReserveParameters();
            type = MetaDataSetType.StoreCancelReserve;
            notification4Id = this.CreateOrUpdateNotification(notification4Id, type, parameters);

            var notification5Id = storeCancelReserveNotificationId;
            parameters = this.GetUserCancelReserveParameters();
            type = MetaDataSetType.CustomerCancelReserve;
            notification5Id = this.CreateOrUpdateNotification(notification5Id, type, parameters);

            var notification6Id = storeCancelReserveNotificationId;
            parameters = this.GetErrorCancelReserveParameters();
            type = MetaDataSetType.ErrorCancelReserve;
            notification6Id = this.CreateOrUpdateNotification(notification6Id, type, parameters);

            NotificationManager.CreateRules(notificationId, notification1Id, notification7Id, notification8Id,
                notification2Id, notification3Id,
                notification4Id, notification5Id, notification6Id);
        }

        private string CreateOrUpdateNotification(string notificationId, MetaDataSetType type, NotificationParameters parameters)
        {
            if (string.IsNullOrEmpty(notificationId))
            {
                notificationId = NotificationManager.CreateNotification(type, parameters);
            }
            else
            {
                notificationId = NotificationManager.UpdateNotification(type, notificationId, parameters);
            }
            return notificationId;
        }

        //private const string _path = @"C:\work\svn\mediamarkt\OnlineOrders\trunk\OnlineOrders\TestService\bin\Debug\";
        //private const string _path = @"C:\Projects\OnlineOrders\TestService\";
        private const string _path = @"";
        private NotificationParameters GetConfirmParameters()
        {
            var rdlFileName = "OnlineOrder_ConfirmEvent_PdfAttachment3.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmHtmlText.xml"));
            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ConfirmEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "kupon{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCustomerConfirmParameters()
        {
            var rdlFileName = "OnlineOrder_ConfirmEvent_PdfAttachment3.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/OnlineOrder/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmHtmlText.xml"));
            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerConfirmEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "kupon{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetConfirmWithoutKuponParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmWithoutKuponHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmWithoutKuponPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmWithoutKuponSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ConfirmWithoutKuponEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }
        
        private NotificationParameters GetCustomerConfirmWithoutKuponParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmWithoutKuponHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmWithoutKuponPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmWithoutKuponSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerConfirmWithoutKuponEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }


        private NotificationParameters GetConfirmDeliveryParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmDeliveryHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmDeliveryPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmDeliverySMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ConfirmDeliveryEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCustomerFeedbackParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CustomerFeedback/CustomerFeedbackHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CustomerFeedback/CustomerFeedbackPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CustomerFeedback/CustomerFeedbackSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerFeedbackEvent__','Id',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Отзыв о покупке в Media Markt",
                ToSMS = "",
                TextSMS = "",
                SimpleDataSetNames = false,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCustomerConfirmDeliveryParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmDeliveryHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmDeliveryPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ConfirmDeliverySMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerConfirmDeliveryEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetConfirmForManagerParameters()
        {
            var rdlFileName = "OnlineOrder_ConfirmForManagerEvent_PdfAttachment.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManagerHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManagerPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManagerSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ConfirmForManagerEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "act{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Заказ: РЕЗЕРВ ([[Field('OnlineOrder_ConfirmForManagerEvent_ClientInformationDataSet','City',1)]] [[Field('OnlineOrder_ConfirmForManagerEvent_ClientInformationDataSet','Surname',1)]] [[Field('OnlineOrder_ConfirmForManagerEvent_ClientInformationDataSet','FirstName',1)]])",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetConfirmForManager2Parameters()
        {
            var rdlFileName = "OnlineOrder_ConfirmForManager2Event_PdfAttachment.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager2HtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager2PlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager2SMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ConfirmForManager2Event__','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "act{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Заказ: РЕЗЕРВ ([[Field('OnlineOrder_ConfirmForManager2Event__','ClientInfo.City',1)]] [[Field('OnlineOrder_ConfirmForManager2Event__','ClientInfo.Surname',1)]] [[Field('OnlineOrder_ConfirmForManager2Event__','ClientInfo.FirstName',1)]])",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetConfirmForManager3Parameters()
        {
            var rdlFileName = "OnlineOrder_ConfirmForManager3Event_PdfAttachment.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager3HtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager3PlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager3SMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ConfirmForManager3Event__','Id',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "act{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Заказ: РЕЗЕРВ ([[Field('OnlineOrder_ConfirmForManager3Event__','Customer.Name',1)]] [[Field('OnlineOrder_ConfirmForManager3Event__','Customer.Surname',1)]] [[Field('OnlineOrder_ConfirmForManager3Event__','Customer.Phone',1)]])",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetConfirmForManager4Parameters(bool emfFormat)
        {
            var rdlFileName = "OnlineOrder_ConfirmForManager5Event_PdfAttachment.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/ConfirmForManager/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var subReportAllBytes = File.ReadAllBytes(string.Format("{1}Data/ConfirmForManager/{0}", "OnlineOrder_ConfirmForManager5Event_SetItems_PdfAttachment.rdl", _path));
            var subReportString = Convert.ToBase64String(subReportAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager/ConfirmForManager5HtmlText.xml"));
            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager/ConfirmForManager5PlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ConfirmForManager/ConfirmForManager5SMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var emfProperties = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("OutputFormat", "EMF"),
                    new KeyValuePair<string, string>("DpiX", "400"),
                    new KeyValuePair<string, string>("DpiY", "400")
                };
            var emf = Newtonsoft.Json.JsonConvert.SerializeObject(emfProperties);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('defaultdataset','Id',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = emfFormat ? "act{0}.emf" : "act{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = emfFormat ? "IMAGE" : "",
                PdfAttachmentProperties = emfFormat ? emf : "",
                PdfAttachmentSubReportName = "OnlineOrder_ConfirmForManager5Event_SetItems_PdfAttachment",
                PdfAttachmentSubReportValue = subReportString,
                Subject = "Заказ: РЕЗЕРВ ([[Field('defaultdataset','Customer.Name',1)]] [[Field('defaultdataset','Customer.Surname',1)]] [[Field('defaultdataset','Customer.Phone',1)]])",
                ToSMS = "",
                TextSMS = smsText,
                SimpleDataSetNames = true,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetInstantloanParameters(string typeName, string subject, string toEmail)
        {
            var htmlText = File.ReadAllText(string.Format("{0}Data/Instantloan/{1}HtmlText.xml", _path, typeName));
            var plainText = File.ReadAllText(string.Format("{0}Data/Instantloan/{1}PlainText.txt", _path, typeName));
            
            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);


            
            var parameters = new NotificationParameters
            {
                Header = "[[Field('defaultdataset','Id',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                PdfAttachmentSubReportName = "",
                PdfAttachmentSubReportValue = "",
                Subject = subject,
                ToSMS = "",
                TextSMS = "",
                SimpleDataSetNames = true,
                ToEmail = toEmail
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCancelWithReturnToStoreForManagerParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/Cancel_StoreTemplate.html"));

            var parameters = new NotificationParameters
                {
                    Header = "[[Field('OnlineOrder_CancelWithReturnToStoreForManagerEvent__','Id',1)]]",
                    HtmlText = htmlText,
                    Subject = "Верните отложенный товар в зал продаж",
                    HtmlTextResources = new List<KeyValuePair<string, string>>(),
                    PdfAttachment = "",
                    PdfAttachmentFormat = "",
                    PdfAttachmentName = "",
                    PdfAttachmentProperties = "",
                    PlainText = "",
                    TextSMS = "",
                    ToSMS = "",
                    ToEmail = ""
                };
            return parameters;
        }

        private NotificationParameters GetCancelWithoutReturnToStoreForManagerParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CancelWorkflow_StoreTemplate.html"));

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CancelWithoutReturnToStoreForManagerEvent__','Id',1)]]",
                HtmlText = htmlText,
                Subject = "Верните отложенный товар в зал продаж",
                HtmlTextResources = new List<KeyValuePair<string, string>>(),
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentName = "",
                PdfAttachmentProperties = "",
                PlainText = "",
                TextSMS = "",
                ToSMS = "",
                ToEmail = ""
            };
            return parameters;
        }

        private NotificationParameters GetChangeStoreForManagerParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/Change_StoreTemplate.html"));

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ChangeStoreForManagerEvent__','Id',1)]]",
                HtmlText = htmlText,
                Subject = "Замените товар в резерве",
                HtmlTextResources = new List<KeyValuePair<string, string>>(),
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentName = "",
                PdfAttachmentProperties = "",
                PlainText = "",
                TextSMS = "",
                ToSMS = "",
                ToEmail = ""
            };
            return parameters;
        }

        private NotificationParameters GetChangeWwsOrderIdForManagerParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/Change_SalesDocumentTemplate.html"));

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ChangeSalesDocumentForManager__','Id',1)]]",
                HtmlText = htmlText,
                Subject = "Замените документ продажи",
                HtmlTextResources = new List<KeyValuePair<string, string>>(),
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentName = "",
                PdfAttachmentProperties = "",
                PlainText = "",
                TextSMS = "",
                ToSMS = "",
                ToEmail = ""
            };
            return parameters;
        }

        private NotificationParameters GetNewOrderCollectForManagerParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/New_StoreTemplate.html"));

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_NewOrderCollectForManagerEvent__','Id',1)]]",
                HtmlText = htmlText,
                Subject = "Соберите интернет-заказ",
                HtmlTextResources = new List<KeyValuePair<string, string>>(),
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentName = "",
                PdfAttachmentProperties = "",
                PlainText = "",
                TextSMS = "",
                ToSMS = "",
                ToEmail = ""
            };
            return parameters;
        }

        private NotificationParameters GetDeliveryApproveForManagerParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/Delivery_StoreTemplate.html"));

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_DeliveryApproveForManagerEvent__','Id',1)]]",
                HtmlText = htmlText,
                Subject = "Согласуйте дату доставки",
                HtmlTextResources = new List<KeyValuePair<string, string>>(),
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentName = "",
                PdfAttachmentProperties = "",
                PlainText = "",
                TextSMS = "",
                ToSMS = "",
                ToEmail = ""
            };
            return parameters;
        }

        private NotificationParameters GetCertificateParameters()
        {
            var rdlFileName = "OnlineOrder_CertificateEvent_PdfAttachment.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CertificateHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CertificatePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/CertificateSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CertificateEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "gift certificate{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Подарочный сертификат Media Markt",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCustomerCertificateParameters()
        {
            var rdlFileName = "OnlineOrder_CertificateEvent_PdfAttachment.rdl";
            var rdlAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", rdlFileName, _path));
            var rdlString = Convert.ToBase64String(rdlAllBytes);

            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/CertificateHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/CertificatePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/CertificateSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerCertificateEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "gift certificate{0}.pdf",
                PdfAttachment = rdlString,
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Подарочный сертификат Media Markt",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }


        private NotificationParameters GetStoreCancelReserveParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/StoreCancelReserveHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/StoreCancelReservePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/StoreCancelReserveSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_StoreCancelReserve_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: отказ от резерва",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }


        private NotificationParameters GetCustomerStoreCancelReserveParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/StoreCancelReserveHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/StoreCancelReservePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/StoreCancelReserveSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerStoreCancelReserve_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: отказ от резерва",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetUserCancelReserveParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/UserCancelReserveHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/UserCancelReservePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/UserCancelReserveSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_UserCancelReserve_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: отмена заказа",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCustomerUserCancelReserveParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/UserCancelReserveHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/UserCancelReservePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/UserCancelReserveSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_UserCancelReserve_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: отмена заказа",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }


        private NotificationParameters GetErrorCancelReserveParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ErrorCancelReserveHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ErrorCancelReservePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/ErrorCancelReserveSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_ErrorCancelReserve_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: отмена заказа",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCustomerErrorCancelReserveParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ErrorCancelReserveHtmlText.xml"));

            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ErrorCancelReservePlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/OnlineOrder/ErrorCancelReserveSMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_CustomerErrorCancelReserve_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "Ваш заказ в Media Markt: отмена заказа",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetCreditBrokerApprovalParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/Instantloan/CreditBrokerApproval_Template.html"));
            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/Instantloan/CreditBrokerApproval_Template.txt"));

            var parameters = new NotificationParameters
            {
                Header = "[[Field('defaultdataset','Id',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                SimpleDataSetNames = true,
                Subject = "Позвонить клиенту",
                ToSMS = "",
                TextSMS = "",
                ToEmail = ""
            };
            return parameters;
        }

        private NotificationParameters GetGsBtGiftCardParameters()
        {
            var htmlText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/GiftCards/ConfirmDeliveryHtmlText.xml"));
            var plainText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/GiftCards/ConfirmDeliveryPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", _path, "Data/GiftCards/ConfirmDeliverySMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", _path));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
            {
                Header = "[[Field('defaultdataset','Id',1)]]",
                HtmlText = htmlText,
                PlainText = plainText,
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                SimpleDataSetNames = true,
                Subject = "Ваш заказ в Media Markt: РЕЗЕРВ",
                ToSMS = "",
                TextSMS = smsText,
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }

        private NotificationParameters GetSystemParameters()
        {
            var parameters = new NotificationParameters
            {
                Header = "[[Field('OnlineOrder_SystemEvent_TextMessageDataSet','SystemInformation',1)]]",
                HtmlText = "[[Field('OnlineOrder_SystemEvent_TextMessageDataSet','MessageText',1)]]",
                PlainText = "[[Field('OnlineOrder_SystemEvent_TextMessageDataSet','MessageText',1)]]",
                PdfAttachmentName = "",
                PdfAttachment = "",
                PdfAttachmentFormat = "",
                PdfAttachmentProperties = "",
                Subject = "[[Field('OnlineOrder_SystemEvent_TextMessageDataSet','Text',1)]]",
                ToSMS = "",
                TextSMS = "",
                ToEmail = ""
            };
            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            return parameters;
        }


        public void Task06UpdateNotificationAndRules(string confirmNotificationId, string confirmWithoutKuponNotificationId,
            string confirmDeliveryNotificationId, string confirmForManagerId, string confirmForManager2Id, string confirmForManager3Id, string confirmForManager4Id,
            string certificateNotificationId, 
            string systemNotificationId,
            string storeCancelReserveNotificationId, string userCancelReserveNotificationId, string errorCancelReserveNotificationId)
        {
            var notificationId = confirmNotificationId;
            NotificationParameters parameters = GetConfirmParameters();
            notificationId = NotificationManager.UpdateNotification(MetaDataSetType.Confirm, notificationId, parameters);

            var notification1Id = confirmWithoutKuponNotificationId;
            parameters = this.GetConfirmWithoutKuponParameters();
            notification1Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmWithoutKupon, notification1Id, parameters);

            var notification7Id = confirmDeliveryNotificationId;
            parameters = this.GetConfirmDeliveryParameters();
            notification7Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmDelivery, notification7Id, parameters);

            var notification8Id = confirmForManagerId;
            parameters = this.GetConfirmForManagerParameters();
            notification8Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager, notification8Id, parameters);

            var notification9Id = confirmForManager2Id;
            parameters = this.GetConfirmForManager2Parameters();
            notification9Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager2, notification9Id, parameters);

            var notification10Id = confirmForManager3Id;
            parameters = this.GetConfirmForManager3Parameters();
            notification10Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager3, notification10Id, parameters);

            var notification11Id = confirmForManager4Id;
            parameters = this.GetConfirmForManager4Parameters(true);
            notification11Id = NotificationManager.UpdateNotification(MetaDataSetType.ConfirmForManager4, notification11Id, parameters);

            var notificationId2 = certificateNotificationId;
            parameters = GetCertificateParameters();
            notificationId2 = NotificationManager.UpdateNotification(MetaDataSetType.Certificate, notificationId2, parameters);

            var notificationId3 = systemNotificationId;
            parameters = this.GetSystemParameters();
            notificationId3 = NotificationManager.UpdateNotification(MetaDataSetType.HQ_ResponsibleForCertificate, notificationId3,
                                                                     parameters);

            var notificationId4 = storeCancelReserveNotificationId;
            parameters = this.GetStoreCancelReserveParameters();
            notificationId4 = NotificationManager.UpdateNotification(MetaDataSetType.StoreCancelReserve, notificationId4, parameters);

            var notificationId5 = userCancelReserveNotificationId;
            parameters = this.GetUserCancelReserveParameters();
            notificationId5 = NotificationManager.UpdateNotification(MetaDataSetType.CustomerCancelReserve, notificationId5, parameters);

            var notificationId6 = errorCancelReserveNotificationId;
            parameters = this.GetErrorCancelReserveParameters();
            notificationId6 = NotificationManager.UpdateNotification(MetaDataSetType.ErrorCancelReserve, notificationId6, parameters);

            NotificationManager.CreateRules(
                notificationId, notification1Id, notification7Id, notification8Id,
                notificationId2, notificationId3,
                notificationId4, notificationId5, notificationId6);
        }


        public void Task07SendSystemMessage()
        {
            var notification = new NotificationManager();

            notification.Send(MetaDataSetType.HQ_ResponsibleForCertificate, new SysSendDataSetParams()
            {
                Text = "Список сертификатов",
                TextList = new[] { "1111", "22222", "3333", "433434334" }
            }, "prokofevs@media-saturn.com;vasilevig@media-saturn.com");
        }
    }
}

