﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Orders.Backend.Services.Order.Notification;

namespace Orders.Backend.Admin.NotificationFiller.Services
{
    public class ParametersService
    {
        public NotificationParameters GetParameters(GetParametersRequest request)
        {
            var htmlText =
                File.ReadAllText(string.Format("{0}{1}", request.StorePath,
                                               string.Format("Data/{0}", request.StoreTemplate)));

            var plainText = File.ReadAllText(string.Format("{0}{1}", request.StorePath, "Data/ConfirmDeliveryPlainText.txt"));
            var smsText = File.ReadAllText(string.Format("{0}{1}", request.StorePath, "Data/ConfirmDeliverySMSText.txt"));

            var mmemailbaneкAllBytes = File.ReadAllBytes(string.Format("{1}Data/{0}", "mmemailbaner.png", request.StorePath));
            var mmemailbanerString = Convert.ToBase64String(mmemailbaneкAllBytes);

            var parameters = new NotificationParameters
                {
                    Header = string.Format("[[Field('OnlineOrder_{0}__','Id',1)]]", request.EventName),
                    HtmlText = htmlText,
                    Subject = request.Subject,
                    HtmlTextResources = new List<KeyValuePair<string, string>>(),
                    PdfAttachment = "",
                    PdfAttachmentFormat = "",
                    PdfAttachmentName = "",
                    PdfAttachmentProperties = "",
                    PlainText = plainText,
                    TextSMS = smsText,
                    ToSMS = "",
                    ToEmail = ""
                };
            

            parameters.HtmlTextResources = new List<KeyValuePair<string, string>>();
            parameters.HtmlTextResources.Add(new KeyValuePair<string, string>("mmemailbaner.png", mmemailbanerString));
            return parameters;
        }
    }

    public class GetParametersRequest
    {
        public string StorePath { get; set; }

        public string StoreTemplate { get; set; }

        public string StorePlainTextTemplate { get; set; }

        public string StoreSmsTemplate { get; set; }

        public string EventName { get; set; }

        public string Subject { get; set; }
    }
}
