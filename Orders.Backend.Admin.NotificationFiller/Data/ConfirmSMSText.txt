﻿Ваш заказ зарезервирован. Номер резерва: [[Field('OnlineOrder_ConfirmEvent_TextMessageDataSet','ReserveNo',1)]].
Для получения заказа обратитесь на стойку выдачи интернет заказов. 
Вам на почту выслан купон на активацию подарочного сертификата на [[Field('OnlineOrder_ConfirmEvent_TextMessageDataSet','Amount',1)]] рублей.
Распечатайте и предъявите купон при оплате товара. После оплаты товара мы пришлем Вам подарочный сертификат в течение 24 часов. 
Адрес магазина: [[Field('OnlineOrder_ConfirmEvent_TextMessageDataSet','StoreInfo',1)]].
Media Markt.