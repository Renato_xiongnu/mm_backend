﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Helpers;
using NLog;
using OnlineOrders.Common.Helpers;
using Order.Backend.Dal.Common;
using Orders.Backend.Dal;
using Orders.Backend.Services.Contracts.CustomerService;
using Orders.Backend.Services.Contracts.OrderService;
using Orders.Backend.Services.Order.Common;
using Orders.Backend.Services.Order.Global;
using ReserveLine = Orders.Backend.Services.Contracts.OrderService.ReserveLine;
using CommonDal = Order.Backend.Dal.Common;
using OrderInfo = Orders.Backend.Services.Contracts.OrderService.OrderInfo;
using ReturnCode = Orders.Backend.Services.Contracts.OrderService.ReturnCode;

namespace Orders.Backend.Services.Order.Facades
{
    public static class OrderHeaderFacade
    {
        private static readonly ILogger Logger = LogManager.GetLogger("OrderHeaderFacade");

        #region [Untouched]

        public static void GiftCertificateNumberReservedNotifyCustomer(string orderId, string updatedBy, string reasonText)
        {
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId,
                                                        Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                                        Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                {
                    throw new InvalidOperationException(orderId + " doesn't exist");
                }
                GiftCertificateNumberReservedNotifyCustomerInContext(header, updatedBy, reasonText);
                provider.Save();
            }
        }

        public static void GiftCertificateNumberReservedNotifyCustomerInContext(OrderHeader header, string updatedBy, string reasonText)
        {
            if (header.OrderStatus != (int)InternalOrderStatus.Confirmed)
            {
                throw new InvalidOperationException(header.OrderId + " move to Confirmed state allow only for orders in 'Confirmed' state");
            }
            ChangeStatus(header, InternalOrderStatus.GiftCertificateNumberReservedNotifyCustomer, changedType: ChangedType.System, whoChanged: updatedBy, reasonText: reasonText);
        }

        public static void Confirmed(string orderId, string wwsOrderId, string updatedBy, string reasonText, DateTime? newExpirationDate = null)
        {
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId,
                                                        Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                                        Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                {
                    throw new InvalidOperationException(orderId + " doesn't exists");
                }
                ConfirmedInContext(header, wwsOrderId, updatedBy, reasonText, newExpirationDate);
                provider.Save();
            }
        }

        public static void ConfirmedInContext(OrderHeader header, string wwsOrderId, string updatedBy, string reasonText, DateTime? newExpirationDate = null)
        {
            if (header.OrderStatus != (int)InternalOrderStatus.Created && header.OrderStatus != (int)InternalOrderStatus.Confirmed)
            {
                throw new InvalidOperationException(header.OrderId + " Move to Confirmed state allow only for orders in 'CREATED' state");
            }
            if (wwsOrderId != null)
            {
                UpdateReserve(wwsOrderId, header);
                header.WWSOrderId = wwsOrderId;
            }

            if (header.OrderStatus == (int)InternalOrderStatus.Created && newExpirationDate == null)
            //Set Expiration date only at first transition
            {
                Logger.Info("Start changing order expiration date for order {0}", header.OrderId);
                SetExpirationDate(header, Cache.GetStore(header.SapCode));
                Logger.Info("Finish changing order expiration date for order {0}, expires {1}", header.OrderId,
                    header.ExpirationDate);
            }

            if (newExpirationDate != null)
            {
                Logger.Info("Start changing order expiration date for order {0}", header.OrderId);
                header.ExpirationDate = Dal.LightProvider.GetValidDbDateTime(newExpirationDate);
                reasonText = string.Format("Новая дата истекания резерва {0}. {1}", header.ExpirationDate,
                    reasonText);
                Logger.Info("Finish changing order expiration date for order {0}, expires {1}", header.OrderId,
                    header.ExpirationDate);
            }

            ChangeStatus(header, InternalOrderStatus.Confirmed, changedType: ChangedType.Customer, whoChanged: updatedBy, reasonText: reasonText);
        }

        public static void Reject(string orderId, ChangedType rejectedBy, string updatedBy, string reasonText)
        {
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId,
                                                        Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                                        Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                    throw new InvalidOperationException(orderId + " doesn't exists");

                RejectInContext(header, rejectedBy, updatedBy, reasonText);
                provider.Save();
            }
        }

        public static void RejectInContext(OrderHeader header, ChangedType rejectedBy, string updatedBy, string reasonText)
        {
            var orderStatus = (InternalOrderStatus)header.OrderStatus;
            var orderStatusStr = orderStatus.GetDescription();
            if (header.OrderStatus >= (int)InternalOrderStatus.Paid)
            {
                throw new InvalidOperationException(string.Format("You can't rejected order in status {0}", orderStatusStr));
            }

            InternalOrderStatus toStatus;
            switch (rejectedBy)
            {
                case ChangedType.Customer:
                    toStatus = InternalOrderStatus.RejectedByCustomer;
                    break;
                case ChangedType.Manager:
                    toStatus = InternalOrderStatus.Rejected;
                    break;
                case ChangedType.System:
                    toStatus = InternalOrderStatus.Rejected;
                    break;
                default:
                    throw new InvalidOperationException(string.Format("You can't rejected order in status {0}", orderStatusStr));
            }

            ChangeStatus(header, toStatus, changedType: ChangedType.Customer, whoChanged: updatedBy, reasonText: reasonText);
        }

        public static InternalOrderStatus Paid(string orderId)
        {
            InternalOrderStatus lastStatus;
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId,
                                                        Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                                        Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                    throw new InvalidOperationException(orderId + " doesn't exists");
                lastStatus = PaidInContext(header);
                provider.Save();
            }

            return lastStatus;
        }

        public static InternalOrderStatus PaidInContext(OrderHeader header)
        {
            InternalOrderStatus lastStatus = header.OrderStatus.ToEnum<InternalOrderStatus>();
            ChangeStatus(header, InternalOrderStatus.Paid);
            return lastStatus;
        }

        public static InternalOrderStatus Shipped(string orderId)
        {
            InternalOrderStatus lastStatus;
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId, Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                    throw new InvalidOperationException(orderId + " doesn't exists");
                lastStatus = ShippedInContext(header);
                provider.Save();
            }

            return lastStatus;
        }

        public static InternalOrderStatus ShippedInContext(OrderHeader header)
        {
            InternalOrderStatus lastStatus = header.OrderStatus.ToEnum<InternalOrderStatus>();
            var orderStatus = (InternalOrderStatus)header.OrderStatus;
            var orderStatusStr = orderStatus.GetDescription();
            if (header.OrderStatus > (int)InternalOrderStatus.Shipped)
            {
                throw new InvalidOperationException(string.Format("You can't shipped order in status {0}", orderStatusStr));
            }
            ChangeStatus(header, InternalOrderStatus.Shipped);
            return lastStatus;
        }

        public static InternalOrderStatus Created(string orderId)
        {
            InternalOrderStatus lastStatus;
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId, Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                    throw new InvalidOperationException(orderId + " doesn't exists");
                lastStatus = CreatedInContext(header);
                provider.Save();
            }
            return lastStatus;
        }

        public static InternalOrderStatus CreatedInContext(OrderHeader header)
        {
            InternalOrderStatus lastStatus;
            var orderStatus = (InternalOrderStatus)header.OrderStatus;
            var orderStatusStr = orderStatus.GetDescription();
            lastStatus = header.OrderStatus.ToEnum<InternalOrderStatus>();
            switch (lastStatus)
            {
                case InternalOrderStatus.Paid:
                case InternalOrderStatus.Closed:
                case InternalOrderStatus.Rejected:
                case InternalOrderStatus.RejectedByCustomer:
                    throw new InvalidOperationException(string.Format("You can't created order in status {0}", orderStatusStr));
            }
            lastStatus = header.OrderStatus.ToEnum<InternalOrderStatus>();
            ChangeStatus(header, InternalOrderStatus.Created);
            return lastStatus;
        }

        public static InternalOrderStatus Close(string orderId)
        {
            InternalOrderStatus lastStatus;
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId,
                                                        Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                                        Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (header == null)
                    throw new InvalidOperationException(orderId + " doesn't exists");
                lastStatus = CloseInContext(header);
                provider.Save();
            }
            return lastStatus;
        }

        public static InternalOrderStatus CloseInContext(OrderHeader header)
        {
            InternalOrderStatus lastStatus = header.OrderStatus.ToEnum<InternalOrderStatus>();
            ChangeStatus(header, InternalOrderStatus.Closed);
            return lastStatus;
        }

        #endregion

        public static StoreData GetStoreData(string sapCode)
        {
            if (String.IsNullOrEmpty(sapCode))
                throw new InvalidOperationException("sapCode cannot be null");
            using (var provider = new Dal.LightProvider())
            {
                var store = provider.GetStore(sapCode);
                if (store == null)
                    throw new InvalidOperationException(sapCode + " store doesn't exist");
                return store.ToContract();
            }
        }

        public static ICollection<StoreData> GetAllStoresData()
        {
            using (var provider = new Dal.LightProvider())
            {
                var stores = provider.Stores.ToArray();

                return stores.Select(t => t.ToContract()).ToList();
            }
        }

        public static string CreateOrderHeader(ClientData clientData, CreateOrderData orderData,
                                                      DeliveryInfo deliveryInfo,
                                                      ICollection<ArticleData> articlesData)
        {
            if (clientData == null)
                throw new InvalidOperationException(string.Format("the order must have client data"));
            if (deliveryInfo == null)
                throw new InvalidOperationException(string.Format("the order must have delivery info"));
            if (deliveryInfo.HasDelivery && string.IsNullOrEmpty(deliveryInfo.City))
                throw new InvalidOperationException("for Delivery info the City must be filled in");
            if (deliveryInfo.HasDelivery && string.IsNullOrEmpty(deliveryInfo.Address))
                throw new InvalidOperationException("for Delivery info the Address must be filled in");
            if (orderData.StoreInfo == null)
                throw new InvalidOperationException(string.Format("the order must have store info"));
            if (string.IsNullOrEmpty(orderData.StoreInfo.SapCode) || string.IsNullOrWhiteSpace(orderData.StoreInfo.SapCode))
                throw new InvalidOperationException(string.Format("the sapCode must be filled in"));
            if (articlesData == null)
                throw new InvalidOperationException(string.Format("the order must have lines"));
            if (articlesData.Any(m => string.IsNullOrEmpty(m.ArticleNum) || string.IsNullOrWhiteSpace(m.ArticleNum)))
                throw new InvalidOperationException(string.Format("the all order lines must have a article number"));
            Cache.GetStore(orderData.StoreInfo.SapCode); // Check if sapCode exists            
            using (var provider = new Dal.LightProvider())
            {
                var header = orderData.ToOrderHeader();
                var customer = clientData.ToCustomer();//.CheckIfExistingCustomer(provider);
                header.Customer = customer;
                header.Deliveries.Add(deliveryInfo.ToDelivery(customer));

                var reserve = new Dal.ReserveHeader
                    {
                        SapCode = orderData.StoreInfo.SapCode,
                        /*WWSOrderId = header.WWSOrderId,*/
                        ReserveStatus = ReserveStatus.Prepared.ToShort(),
                        IsOnlineOrder = orderData.IsOnlineOrder
                    };

                if (orderData.SalesDocumentData != null)
                {
                    reserve.OutletInfo = orderData.SalesDocumentData.OutletInfo;
                    reserve.PrintableInfo = orderData.SalesDocumentData.PrintableInfo;
                    reserve.ProductPickupInfo = orderData.SalesDocumentData.ProductPickupInfo;
                    reserve.SalesPersonId = orderData.SalesDocumentData.SalesPersonId;
                }

                articlesData = articlesData.SelectMany(el => el.SplitSet()).ToList();
                foreach (var articleData in articlesData)
                {
                    var orderLine = articleData.ToOrderLine();
                    var reserveLine = articleData.ToReserveLine();

                    header.OrderLines.Add(orderLine);
                    foreach (var nesterOrderLine in orderLine.NesterOrderLines)
                    {
                        header.OrderLines.Add(nesterOrderLine);
                    }
                    reserve.ReserveLine.Add(reserveLine);
                }
                header.ReserveHeaders.Add(reserve);

                //TODO: here
                UpdateReserveTitle(header.SapCode, reserve.ReserveLine);
                ChangeStatus(header, InternalOrderStatus.Created, changedType: ChangedType.Customer, whoChanged: orderData.CreatedBy, reasonText: string.Empty);

                provider.OrderHeader.AddObject(header);
                provider.Save();

                return header.OrderId;
            }
        }

        public static InternalOrderStatus UpdateOrderInfo(UpdateOrderData orderData, bool historyRecordSkip)
        {
            if (orderData == null)
                throw new InvalidOperationException("the order must have update data");

            using (var provider = new Dal.LightProvider())
            {
                var oldHeader = provider.GetOrderDataByOrderId(orderData.OrderId,
                                                            Dal.RelatedEntity.OrderLines |
                                                            Dal.RelatedEntity.GiftCert |
                                                            Dal.RelatedEntity.Customer |
                                                            Dal.RelatedEntity.Delivery);

                if (oldHeader == null)
                    throw new InvalidOperationException(orderData.OrderId + " doesn't exist");

                var currentOrderStatus = UpdateOrderInfoInContext(oldHeader, orderData, historyRecordSkip);

                provider.Save();

                return currentOrderStatus;
            }
        }

        public static InternalOrderStatus UpdateOrderInfoInContext(OrderHeader oldHeader, UpdateOrderData orderData, bool historyRecordSkip)
        {
            var newWwsOrder = orderData.WWSOrderId;

            Dal.ReserveHeader reserve;
            if (newWwsOrder != null)
            {
                reserve = UpdateReserve(newWwsOrder, oldHeader);
                oldHeader.WWSOrderId = newWwsOrder;
            }
            else
            {
                reserve = oldHeader.ReserveHeaders.Single(t => t.WWSOrderId == oldHeader.WWSOrderId);
            }

            if (orderData.IsApproved.HasValue)
            {
                reserve.IsApproved = orderData.IsApproved.Value;
            }

            oldHeader.UpdateHeaderFields(orderData);

            if (orderData.DeliveryInfo != null)
            {
                var delivery = oldHeader.Deliveries.SingleOrDefault(t => t.CustomerId == oldHeader.CustomerId);
                var dbHasDelivery = delivery != null;
                if (dbHasDelivery)
                {
                    delivery.UpdateDeliveryFields(orderData.DeliveryInfo);
                }
                else
                {
                    oldHeader.Deliveries.Add(orderData.DeliveryInfo.ToDelivery(oldHeader.Customer));
                }

                //TODO: new delivery?
            }
            if (orderData.ClientData != null && oldHeader.Customer != null)
            {
                if (oldHeader.Customer.IsDefault
                    && (!string.IsNullOrEmpty(orderData.ClientData.Name) &&
                    !string.IsNullOrEmpty(orderData.ClientData.Surname)))
                {
                    oldHeader.Customer = orderData.ClientData.ToCustomer();
                }
                else
                {
                    oldHeader.Customer.UpdateCustomerFields(orderData.ClientData);
                }
            }
            var currentOrderStatus = oldHeader.OrderStatus.ToEnum<InternalOrderStatus>();
            if (!historyRecordSkip)
            {
                ChangeStatus(oldHeader, currentOrderStatus, changedType: ChangedType.System, whoChanged: orderData.UpdatedBy, reasonText: orderData.ReasonText);
            }
            return currentOrderStatus;
        }

        public static GerOrderDataResult GetOrderInfoByOrderId(string orderId)
        {
            using (var provider = new Dal.LightProvider())
            {
                var orderHeader = provider.GetOrderDataByOrderId(orderId,
                                               Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                               Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                return orderHeader.ToOrderDataResult();
            }
        }

        public static GerOrderDataResult GetOrderInfoByWWSOrderId(string sapCode, string wwsOrderId)
        {
            using (var provider = new Dal.LightProvider())
            {
                var orderHeader = provider.GetOrderDataByWWSOrderId(sapCode, wwsOrderId,
                                               Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                               Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                return orderHeader.ToOrderDataResult();
            }
        }

        public static GerOrderDataResult GetOrderInfoByCertificate(string certificateId)
        {
            using (var provider = new Dal.LightProvider())
            {
                var orderHeader = provider.GetOrderDataByCertificateId(certificateId,
                                               Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                               Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                return orderHeader.ToOrderDataResult();
            }
        }

        public static ICollection<GerOrderDataResult> GetOrderInfoByOrderIds(ICollection<string> orderIds)
        {
            using (var provider = new Dal.LightProvider())
            {
                var orders = provider.GetOrderDataByOrderId(orderIds,
                                               Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                               Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery).ToList();
                return orders.Select(m => m.ToOrderDataResult()).ToList();
            }
        }

        public static ICollection<GerOrderDataResult> GetOrderInfoByType(short type, ICollection<string> sapCodes)
        {
            using (var provider = new Dal.LightProvider())
            {
                var orders = provider.GetOrderDataByType(type,
                                               Dal.RelatedEntity.OrderLines | Dal.RelatedEntity.GiftCert |
                                               Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                if (sapCodes != null)
                {
                    orders = orders.Where(m => sapCodes.Contains(m.SapCode));
                }
                var data = orders.ToList();
                return data.Select(m => m.ToOrderDataResult()).ToList();
            }
        }

        public static void UpdateReserveInfo(string orderId, ReserveInfo reserveInfo, bool byEmployee)
        {            
            using (var provider = new Dal.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId,
                                                               Dal.RelatedEntity.OrderLines |
                                                               Dal.RelatedEntity.GiftCert |
                                                               Dal.RelatedEntity.Customer |
                                                               Dal.RelatedEntity.Delivery);

                if (header == null)
                    throw new InvalidOperationException(orderId + " doesn't exist");

                UpdateReserveInfoInContext(provider, header, reserveInfo, byEmployee);

                provider.Save();
            }
        }

        public static void UpdateReserveInfoInContext(LightProvider context, OrderHeader header, ReserveInfo reserveInfo, bool byEmployee)
        {
            var newReserveLines = !reserveInfo.ReserveLines.IsNullOrEmpty() ? reserveInfo.ReserveLines : new List<ReserveLine>();

            var wwsOrderId = header.WWSOrderId;

            var reserveHeader = header.ReserveHeaders.Single(t => t.WWSOrderId == wwsOrderId);

            //TODO: if we have more time to test i would be able to use optimistic concurrence model            

            reserveHeader.ReserveStatus = ReserveStatus.IsEditing.ToShort();
            context.Save();

            if (!byEmployee)
            {
                UpdateReserveHeader(reserveHeader, reserveInfo);

                var customerInfo = reserveInfo.CustomerInfo;
                if (customerInfo != null
                    && customerInfo.Email != null
                    && customerInfo.Name != null
                    && customerInfo.Surname != null)
                {
                    Dal.ReserveCustInfo reserveCustomer;
                    if (reserveHeader.ReserveCustInfoId.HasValue)
                    {
                        reserveCustomer = context.GetReserveCust(reserveHeader.ReserveCustInfoId.Value);

                        if (reserveCustomer.Email != customerInfo.Email ||
                            reserveCustomer.Name != customerInfo.Name ||
                            reserveCustomer.Surname != customerInfo.Surname)
                            reserveCustomer = null;
                    }
                    else
                    {
                        reserveCustomer = context.GetReserveCust(customerInfo.Email, customerInfo.Name,
                                                                  customerInfo.Surname);
                    }

                    if (reserveCustomer == null)
                    {
                        reserveCustomer = new Dal.ReserveCustInfo();
                        reserveHeader.ReserveCustInfo = reserveCustomer;
                        reserveCustomer.CustomerId = header.CustomerId;
                    }

                    UpdateReserveCustomer(reserveCustomer, customerInfo);
                }
            }

            var linesForDelete = new List<Dal.ReserveLine>();
            var nestedLinesToDelete = new List<Dal.ReserveLine>();
            foreach (var reserveLine in reserveHeader.ReserveLine)
            {
                var newReserveLine = newReserveLines.FindChildRecursive(reserveLine.LineId);

                if (newReserveLine == null)
                {
                    var isSubLine = reserveLine.ParentReserveLine != null;
                    if (!isSubLine)
                    {
                        nestedLinesToDelete.AddRange(reserveLine.NestedReserveLine);
                        linesForDelete.Add(reserveLine);
                    }
                    continue;
                }

                CreateOrCopyLineFromDal(newReserveLine, reserveLine, byEmployee);
            }

            foreach (var reserveLine in nestedLinesToDelete) //Два удаления из-за self reference
            {
                context.ReserveLine.DeleteObject(reserveLine);
            }
            context.Save();

            foreach (var reserveLine in linesForDelete)
            {
                context.ReserveLine.DeleteObject(reserveLine);
            }
            context.Save();

            foreach (var newReserveLine in newReserveLines)
            {
                var reserveLine = reserveHeader.ReserveLine.FirstOrDefault(t => t.LineId == newReserveLine.LineId);
                if (newReserveLine.LineId == 0 ||
                    reserveLine == null)
                {
                    var newLine = new Dal.ReserveLine();
                    reserveHeader.ReserveLine.Add(CreateOrCopyLineFromDal(newReserveLine, newLine, byEmployee));
                    if (newReserveLine.SubLines != null)
                    {
                        foreach (var subReserveLine in newReserveLine.SubLines)
                        {
                            var newSubLine = CreateOrCopyLineFromDal(subReserveLine);
                            newSubLine.ParentReserveLine = newLine;
                            reserveHeader.ReserveLine.Add(newSubLine);
                        }
                    }
                }
                if (newReserveLine.SubLines != null && newReserveLine.LineId != 0 && reserveLine != null)
                {
                    foreach (var newSubReserverLine in newReserveLine.SubLines)
                    {
                        if (newSubReserverLine.LineId == 0 ||
                            reserveHeader.ReserveLine.FirstOrDefault(t => t.LineId == newSubReserverLine.LineId) ==
                            null)
                        {
                            var newSubLine = CreateOrCopyLineFromDal(newSubReserverLine, null, byEmployee);
                            newSubLine.ParentReserveLine = reserveLine;
                            reserveHeader.ReserveLine.Add(newSubLine);
                        }
                    }
                }
            }

            reserveHeader.ReserveStatus = ReserveStatus.Prepared.ToShort();

            UpdateReserveTitle(header.SapCode, reserveHeader.ReserveLine);
        }

        #region [Conversion]

        private static void UpdateReserveHeader(Dal.ReserveHeader reserveHeader, ReserveInfo newReserveInfo)
        {
            if (!string.IsNullOrEmpty(newReserveInfo.SapCode) && !string.IsNullOrWhiteSpace(newReserveInfo.SapCode))
            {
                reserveHeader.SapCode = newReserveInfo.SapCode;
            }
            reserveHeader.StoreManagerName = newReserveInfo.StoreManagerName;
            reserveHeader.CreationDate = newReserveInfo.CreationDate;


            reserveHeader.TotalVat = newReserveInfo.TotalPrice != null ? newReserveInfo.TotalPrice.Vat : 0;
            reserveHeader.TotalNetPrice = newReserveInfo.TotalPrice != null ? newReserveInfo.TotalPrice.NetPrice : 0;
            reserveHeader.TotalGrossPrice = newReserveInfo.TotalPrice != null ? newReserveInfo.TotalPrice.GrossPrice : 0;
            reserveHeader.TotalVatPrice = newReserveInfo.TotalPrice != null ? newReserveInfo.TotalPrice.VatPrice : 0;

            reserveHeader.PrepaymentVat = newReserveInfo.PrepaymentPrice != null ? newReserveInfo.PrepaymentPrice.Vat : 0;
            reserveHeader.PrepaymentNetPrice = newReserveInfo.PrepaymentPrice != null ? newReserveInfo.PrepaymentPrice.NetPrice : 0;
            reserveHeader.PrepaymentGrossPrice = newReserveInfo.PrepaymentPrice != null ? newReserveInfo.PrepaymentPrice.GrossPrice : 0;
            reserveHeader.PrepaymentVatPrice = newReserveInfo.PrepaymentPrice != null ? newReserveInfo.PrepaymentPrice.VatPrice : 0;

            reserveHeader.LeftoverVat = newReserveInfo.LeftoverPrice != null ? newReserveInfo.LeftoverPrice.Vat : 0;
            reserveHeader.LeftoverNetPrice = newReserveInfo.LeftoverPrice != null ? newReserveInfo.LeftoverPrice.NetPrice : 0;
            reserveHeader.LeftoverGrossPrice = newReserveInfo.LeftoverPrice != null ? newReserveInfo.LeftoverPrice.GrossPrice : 0;
            reserveHeader.LeftoverVatPrice = newReserveInfo.LeftoverPrice != null ? newReserveInfo.LeftoverPrice.VatPrice : 0;

            reserveHeader.DeliveryDate = newReserveInfo.DeliveryPeriod != null ? Dal.LightProvider.GetValidDbDateTime(newReserveInfo.DeliveryPeriod.DeliveryDate) : null;
            reserveHeader.DeliveryPeriod = newReserveInfo.DeliveryPeriod != null ? newReserveInfo.DeliveryPeriod.Period : null;
            reserveHeader.DeliveryComment = newReserveInfo.DeliveryPeriod != null ? newReserveInfo.DeliveryPeriod.Comment : string.Empty;

            reserveHeader.OutletInfo = newReserveInfo.SalesDocumentData != null
                                           ? newReserveInfo.SalesDocumentData.OutletInfo
                                           : string.Empty;
            reserveHeader.ProductPickupInfo = newReserveInfo.SalesDocumentData != null
                               ? newReserveInfo.SalesDocumentData.ProductPickupInfo
                               : string.Empty;
            reserveHeader.PrintableInfo = newReserveInfo.SalesDocumentData != null
                               ? newReserveInfo.SalesDocumentData.PrintableInfo
                               : string.Empty;

            reserveHeader.SalesPersonId = newReserveInfo.SalesDocumentData != null
                                              ? newReserveInfo.SalesDocumentData.SalesPersonId
                                              : null;

        }

        private static void UpdateReserveCustomer(Dal.ReserveCustInfo reserveCustomer, Contracts.OrderService.ReserveCustomerInfo newCustomerInfo)
        {
            reserveCustomer.ClassificationNumber = newCustomerInfo.ClassificationNumber;
            reserveCustomer.City = newCustomerInfo.City;
            reserveCustomer.Name = newCustomerInfo.Name;
            reserveCustomer.Surname = newCustomerInfo.Surname;
            reserveCustomer.Email = newCustomerInfo.Email;
            reserveCustomer.INN = newCustomerInfo.INN;
            reserveCustomer.KPP = newCustomerInfo.KPP;
            reserveCustomer.Phone = newCustomerInfo.Phone;
            reserveCustomer.Phone2 = newCustomerInfo.Phone2;
            reserveCustomer.Salutation = newCustomerInfo.Salutation;
            reserveCustomer.AddressIndex = newCustomerInfo.AddressIndex;
            reserveCustomer.Address = newCustomerInfo.Address;
            reserveCustomer.CountryAbbreviation = newCustomerInfo.CountryAbbreviation;
        }

        private static Dal.ReserveLine CreateOrCopyLineFromDal(Contracts.OrderService.ReserveLine newReserveLine, Dal.ReserveLine copyTo = null, bool byEmployee = false)
        {
            if (copyTo == null)
                copyTo = new Dal.ReserveLine();
            //Immutable - цена/количество поменяться может, тип артикула - нет
            copyTo.ArticleNum = newReserveLine.ArticleData.ArticleNum;
            copyTo.Price = newReserveLine.ArticleData.Price;
            copyTo.Qty = newReserveLine.ArticleData.Qty;
            copyTo.IsVirtual = newReserveLine.ArticleData.IsVirtual;
            copyTo.OldPrice = copyTo.OldPrice.HasValue ? copyTo.OldPrice : newReserveLine.ArticleData.OldPrice;
            copyTo.Benefit = copyTo.Benefit.HasValue ? copyTo.Benefit : newReserveLine.ArticleData.Benefit; //Immutable
            copyTo.CouponCompainId = !string.IsNullOrEmpty(copyTo.CouponCompainId) ? copyTo.CouponCompainId : newReserveLine.ArticleData.CouponCompainId; //Immutable
            copyTo.CouponCode = !string.IsNullOrEmpty(copyTo.CouponCode) ? copyTo.CouponCode : newReserveLine.ArticleData.CouponCode; //Immutable
            copyTo.ItemState = newReserveLine.ReviewItemState.ToShort();
            copyTo.StockItemState = newReserveLine.StockItemState.ToShort();
            copyTo.Comment = newReserveLine.Comment;
            copyTo.ArticleCondition = newReserveLine.ArticleCondition;
            copyTo.ProductType = newReserveLine.ProductType.ToShort();
            copyTo.ArticleProductType = newReserveLine.ArticleProductType != Contracts.OrderService.ArticleProductType.Product //immutable
                                        ? newReserveLine.ArticleProductType.ToShort()
                                        : copyTo.ArticleProductType;
            copyTo.HasShippedFromStock = newReserveLine.HasShippedFromStock;
            copyTo.SerialNumber = newReserveLine.SerialNumber;
            copyTo.WWSStockNo = newReserveLine.WWSStockNumber;
            copyTo.TransferSapCode = newReserveLine.TransferSapCode ?? string.Empty;
            copyTo.TransferNumber = newReserveLine.TransferNumber;
            copyTo.RefersToItem = copyTo.RefersToItem.GetValueOrDefault() != 0
                ? copyTo.RefersToItem
                : newReserveLine.ArticleData.RefersToItem; //Immutable

            copyTo.BrandTitle = !string.IsNullOrEmpty(copyTo.BrandTitle) ? copyTo.BrandTitle : newReserveLine.ArticleData.BrandTitle;

            if (!byEmployee)
            {
                copyTo.Title = newReserveLine.Title;                
                /*copyTo.LineType = newReserveLine.ToEnum<LineType>().ToShort();*/
                // не проставляется
                copyTo.WWSProductGroup = newReserveLine.WWSProductGroupName;
                copyTo.WWSProductGroupNo = newReserveLine.WWSProductGroupNumber;
                copyTo.WWSFreeQty = newReserveLine.WWSFreeQty;
                copyTo.WWSReservedQty = newReserveLine.WWSReservedQty;
                copyTo.WWSPriceOrig = newReserveLine.WWSPriceOrig;
                copyTo.WWSStockNo = newReserveLine.WWSStockNumber;
                copyTo.WWSDepartmentNo = newReserveLine.WWSDepartmentNumber;

                copyTo.WWSPositionNumber = newReserveLine.WWSPositionNumber;
                copyTo.Promotions = newReserveLine.ArticleData.Promotions.ToJson();
                copyTo.WWSPositionNumber = newReserveLine.WWSPositionNumber;
                copyTo.WWSStoreNumber = newReserveLine.WWSStoreNumber;
            }

            if (newReserveLine.WarrantyInsurance != null)
            {
                copyTo.WI_Number = newReserveLine.WarrantyInsurance.Number;
                copyTo.WI_DocumentPositionNumber = newReserveLine.WarrantyInsurance.DocumentPositionNumber;
                copyTo.WI_RelatedArticleNo = newReserveLine.WarrantyInsurance.RelatedArticleNo;
                copyTo.WI_WarrantySum = newReserveLine.WarrantyInsurance.WarrantySum;
                copyTo.WI_WwsCertificateState = newReserveLine.WarrantyInsurance.WwsCertificateState;
                copyTo.WI_WwsExtensionPrint = newReserveLine.WarrantyInsurance.WwsExtensionPrint;
            }

            return copyTo;
        }

        #endregion

        private static GerOrderDataResult ToOrderDataResult(this Dal.OrderHeader orderHeader)
        {
            var result = new GerOrderDataResult
            {
                OrderInfo = new OrderInfo(),
                ReturnCode = ReturnCode.Ok
            };

            if (orderHeader != null)
            {
                result.OrderInfo = orderHeader.ToOrderInfo();
                result.ClientData = orderHeader.ToClientData();
                result.DeliveryInfo = orderHeader.ToDeliveryInfo();
                result.PickupLocation = orderHeader.ToPickupInfo();
                result.ArticleLines = new List<ArticleData>();
                //if(orderHeader.ZZTOrderHeader != null)
                //    result.ZZTData = orderHeader.ZZTOrderHeader.ToContract();
                if (orderHeader.OrderLines != null)
                    foreach (var line in orderHeader.OrderLines)
                    {
                        result.ArticleLines.Add(line.ToArticleData());
                    }
                result.ReserveInfo = orderHeader.ToReserveInfo();
                UpdateReserveTitle(result.OrderInfo.SapCode, result.ReserveInfo.ReserveLines);
            }
            else
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(string.Format("The order isn't found"));
            }

            return result;
        }

        private static void UpdateReserveTitle(string sapCode, IEnumerable<ReserveLine> reserveLines)
        {
            try
            {
                var emptyLines = reserveLines.Where(t => string.IsNullOrEmpty(t.Title)).ToList();
                var articles = emptyLines.Select(l =>
                {
                    long art;
                    var b = long.TryParse(l.ArticleData.ArticleNum, out art);
                    return b ? art : 0;
                }).Where(l => l > 0).ToArray();
                if (articles.Length > 0)
                {
                    var infoDtos = ArticleInfoFacade.GetArticleInfo(articles, sapCode).ToList();
                    foreach (var reserveLine in emptyLines)
                    {
                        var infoDto = infoDtos.FirstOrDefault(i => i.Article.ToString() == reserveLine.ArticleData.ArticleNum);
                        if (infoDto != null)
                            reserveLine.Title = infoDto.Title;
                    }
                }
            }
            catch
            {

            }
        }

        private static void UpdateReserveTitle(string sapCode, IEnumerable<Dal.ReserveLine> reserveLines)
        {
            try
            {
                var emptyLines = reserveLines.Where(t => string.IsNullOrEmpty(t.Title)).ToList();
                var articles = emptyLines.Select(l =>
                {
                    long art;
                    var b = long.TryParse(l.ArticleNum, out art);
                    return b ? art : 0;
                }).Where(l => l > 0).ToArray();
                if (articles.Length > 0)
                {
                    var infoDtos = ArticleInfoFacade.GetArticleInfo(articles, sapCode).ToList();
                    foreach (var reserveLine in emptyLines)
                    {
                        var infoDto = infoDtos.FirstOrDefault(i => i.Article.ToString() == reserveLine.ArticleNum);
                        if (infoDto != null)
                            reserveLine.Title = infoDto.Title;
                    }
                }
            }
            catch
            {

            }
        }

        private static void SetExpirationDate(Dal.OrderHeader orderHeader, Dal.Store store)
        {
            var delivery = orderHeader.Deliveries.FirstOrDefault();
            if (delivery != null && delivery.HasDelivery)
            {
                orderHeader.ExpirationDate = delivery.DeliveryDate;
                return;
            }
            if (delivery == null ||
               (delivery.PickupLocation != null && Dal.Properties.Settings.Default.StorePickupPointTypes.Contains(delivery.PickupLocation.PickupPointType)))
            {
                var now = DateTime.UtcNow;
                if (store.ReserveLifeTime.HasValue)
                    orderHeader.ExpirationDate =
                        now.Add(LightProvider.GetTimeSpan(store.ReserveLifeTime.Value));
            }
        }

        private static void ChangeStatus(OrderHeader orderHeader, InternalOrderStatus toStatus, ChangedType changedType = ChangedType.System, string whoChanged = null, string reasonText = null)
        {
            var fromStatus = orderHeader.OrderStatus.ToEnum<InternalOrderStatus>();
            if (fromStatus == InternalOrderStatus.Rejected && fromStatus == toStatus) return; //Omit Rejected->Rejected
            var updateDate = DateTime.Now;

            var statusHistory = new Dal.OrderStatusHistory
            {
                FromStatus = fromStatus.ToShort(),
                ToStatus = toStatus.ToShort(),
                ChangedType = changedType.ToShort(),
                WhoChanged = whoChanged,
                ReasonText = reasonText,
                UpdateDate = updateDate
            };

            orderHeader.OrderStatusHistories.Add(statusHistory);
            orderHeader.UpdateDate = updateDate;
            orderHeader.OrderStatus = toStatus.ToShort();
        }

        private static Dal.ReserveHeader UpdateReserve(string newWwsOrderId, Dal.OrderHeader oldHeader)
        {
            var sapCode = oldHeader.SapCode;
            var oldReserveNumber = oldHeader.WWSOrderId;
            Dal.ReserveHeader reserveHeader;
            if (string.IsNullOrEmpty(oldReserveNumber) || oldReserveNumber == newWwsOrderId)
                reserveHeader = oldHeader.ReserveHeaders.Single(t => t.WWSOrderId == oldReserveNumber);
            else
            {
                var oldReserve = oldHeader.ReserveHeaders.Single(t => t.WWSOrderId == oldReserveNumber);
                oldReserve.ReserveStatus = ReserveStatus.Canceled.ToShort();
                reserveHeader = new Dal.ReserveHeader
                {
                    SapCode = oldHeader.SapCode,
                    WWSOrderId = oldHeader.WWSOrderId,
                    ReserveStatus = ReserveStatus.Prepared.ToShort(),

                };

                foreach (var oldLine in oldReserve.ReserveLine)
                {
                    reserveHeader.ReserveLine.Add(new Dal.ReserveLine
                    {
                        ArticleNum = oldLine.ArticleNum,
                        LineType = oldLine.LineType,
                        Price = oldLine.Price,
                        ItemState = oldLine.ItemState,
                        StockItemState = oldLine.StockItemState,
                        Qty = oldLine.Qty,
                        Comment = oldLine.Comment,
                        Title = oldLine.Title,
                        BrandTitle = oldLine.BrandTitle,
                        IsOffered = oldLine.IsOffered,
                        WWSDepartmentNo = oldLine.WWSDepartmentNo,
                        WWSFreeQty = oldLine.WWSFreeQty,
                        WWSPriceOrig = oldLine.WWSPriceOrig,
                        WWSProductGroup = oldLine.WWSProductGroup,
                        WWSProductGroupNo = oldLine.WWSProductGroupNo,
                        WWSReservedQty = oldLine.WWSReservedQty,
                        WWSStockNo = oldLine.WWSStockNo,
                        ArticleCondition = oldLine.ArticleCondition,
                        WWSPositionNumber = oldLine.WWSPositionNumber,
                        Promotions = oldLine.Promotions,
                        WWSStoreNumber = oldLine.WWSStoreNumber,
                        WWSVAT = oldLine.WWSVAT,
                        TransferNumber = oldLine.TransferNumber,
                        TransferSapCode = oldLine.TransferSapCode,
                        IsVirtual = oldLine.IsVirtual,
                        OldPrice = oldLine.OldPrice,
                        Benefit = oldLine.Benefit,
                        CouponCode = oldLine.CouponCode,
                        CouponCompainId = oldLine.CouponCompainId,
                        RefersToItem = oldLine.RefersToItem,
                    });
                }

                oldHeader.ReserveHeaders.Add(reserveHeader);
            }

            reserveHeader.WWSOrderId = newWwsOrderId;
            return reserveHeader;
        }

        public static GerOrderStatusHistoryResult GetOrderStatusHistoryByOrderId(string orderId)
        {
            var result = new GerOrderStatusHistoryResult();
            try
            {
                using (var provider = new Dal.LightProvider())
                {
                    result.OrderStatusHistory = provider.GetOrderStatusHistoryByOrderId(orderId)
                        .Select(osh => new OrderStatusHistoryItem
                        {
                            WhoChanged = osh.WhoChanged,
                            UpdateDate = osh.UpdateDate,
                            ReasonText = osh.ReasonText,
                            ChangedType = new ChangedTypeInfo
                            {
                                ChangedType = osh.ChangedTypeEnum,
                                Text = osh.ChangedTypeEnum.GetDescription(),
                            },
                            FromStatus = new InternalStatusInfo
                            {
                                Status = osh.FromStatusEnum,
                                Text = osh.FromStatusEnum.GetDescription()
                            },
                            ToStatus = new InternalStatusInfo
                            {
                                Status = osh.ToStatusEnum,
                                Text = osh.ToStatusEnum.GetDescription()
                            }
                        })
                        .ToArray();
                }
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        public static GerReservesResult GetReservesByOrderId(string orderId)
        {
            var result = new GerReservesResult();
            try
            {
                using (var provider = new Dal.LightProvider())
                {
                    var header = provider.GetOrderDataByOrderId(orderId,
                                                               Dal.RelatedEntity.OrderLines |
                                                               Dal.RelatedEntity.GiftCert |
                                                               Dal.RelatedEntity.Customer |
                                                               Dal.RelatedEntity.Delivery);
                    if (header == null)
                        throw new InvalidOperationException(orderId + " doesn't exist");
                    result.Reserves = header.ReserveHeaders
                        .ToArray()
                        .Select(rh => rh.ToReserveInfo())
                        .ToArray();
                }
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        public static GetOrderDataResult GetOrderDataByBasketId(string basket3Id)
        {
            using (var provider = new LightProvider())
            {
                var orderHeader = provider.GetOrderDataByBasketId(basket3Id,
                                               RelatedEntity.OrderLines | Dal.RelatedEntity.Customer | Dal.RelatedEntity.Delivery);
                var result = orderHeader.ToOrderDataResult();
                return new GetOrderDataResult
                {
                    ReturnCode = result.ReturnCode,
                    ClientData = result.ClientData,
                    DeliveryInfo = result.DeliveryInfo,
                    ReserveInfo = result.ReserveInfo,
                    ArticleLines = result.ArticleLines,
                    ErrorMessage = result.ErrorMessage,
                    ErrorMessages = result.ErrorMessages
                };
            }
        }

        public static GetCustomerResult GetCustomerByPhone(string phone)
        {
            using (var provider = new LightProvider())
            {
                phone = phone.Trim();
                var customerWithOrder =
                    provider.OrderHeader.Where(
                        t => t.Customer.Phone == phone && t.OrderStatus == (short)InternalOrderStatus.Closed)
                        .OrderByDescending(t => t.Created).FirstOrDefault();

                if (customerWithOrder != null)
                {
                    var delivery = customerWithOrder.Deliveries.FirstOrDefault();
                    return new GetCustomerResult
                    {
                        Name = customerWithOrder.Customer.Name,
                        Surname = customerWithOrder.Customer.Surname,
                        Phone = customerWithOrder.Customer.Phone,
                        Email = customerWithOrder.Customer.Email,
                        DeliveryAddress = delivery != null && delivery.HasDelivery ? delivery.Address : string.Empty,
                        PickupLocationId =
                            delivery != null && !delivery.HasDelivery ? delivery.PickupLocationId : string.Empty
                    };
                }

                return null;
            }
        }

        public static IEnumerable<StockLocationOrderCount> GetOrderCountInStockBySaleLocationAndStockLoations(String saleLocationSapCode, IEnumerable<String> stockLocationSapCodes, Int32 daysRange)
        {
            var result = new List<StockLocationOrderCount>();
            using (var provider = new LightProvider(true))
            {
                var items = provider.GetOrderCountInStockBySaleLocationAndStockLoations(saleLocationSapCode, stockLocationSapCodes, daysRange);
                result.AddRange(items.Select(item => new StockLocationOrderCount { StockLocationSapCode = item.StockLocationSapCode, OrdersCount = item.OrdersCount }));
            }
            return result;
        }
    }
}
