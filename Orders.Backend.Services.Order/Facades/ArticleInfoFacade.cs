﻿using System.Collections.Generic;
using System.Linq;
using Orders.Backend.Services.Order.ProductBackend;

namespace Orders.Backend.Services.Order.Facades
{
    public static class ArticleInfoFacade
    {
        public static IEnumerable<ArticleInfoDto> GetArticleInfo(long[] articles, string sapCode)
        {           
            var client = new OrdersBackendServiceClient();
            var response=client.GetArticleStockStatus(new GetItemsRequest
            {
                Articles = articles.Select(el => el.ToString()).ToArray(),
                Channel = "MM",
                SaleLocation = string.Concat("shop_", sapCode)
            });
            return response.Items
                .Select(x => new ArticleInfoDto
                {
                    Article = x.Article,
                    Title = x.Title
                }).ToList();
        }
    }

    public class ArticleInfoDto
    {
        public long Article { get; set; }
        public string Title { get; set; }                
    }

}
