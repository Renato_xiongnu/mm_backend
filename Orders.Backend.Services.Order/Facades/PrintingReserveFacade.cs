﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Helpers;
using Order.Backend.Dal.Common;
using Orders.Backend.Services.Contracts.OrderService;
using Entity = Orders.Backend.Dal;

namespace Orders.Backend.Services.Order.Facades
{
    public class PrintingReserveFacade
    {
        public static void UpdateReserveContentStatus(ICollection<ReserveContentInfo> contents)
        {        
            
            using (var provider = new Entity.LightProvider())
            {
                var orderIds = contents.Select(t => t.OrderId).ToArray();
                
                var data = provider.ReserveContent.Where(t => orderIds.Contains(t.OrderId)).ToArray();
                var currentDate = DateTime.Now;
                foreach (var content in contents)
                {
                    var reserveContent = data.FirstOrDefault(t => t.WWSOrderId == content.WWSOrderId);
                   
                    if (reserveContent != null)
                    {
                        switch (content.PrintingStatus)
                        {
                            case PrintingStatus.Error:
                                reserveContent.Status = ReserveContentStatus.Error.ToShort();
                                reserveContent.MessageText += string.Format("{0}. {2}: {1}", reserveContent.MessageText, content.Message, currentDate);
                                break;                                                            
                            case PrintingStatus.Printed:
                                reserveContent.Status = ReserveContentStatus.Printed.ToShort();                                
                                break;
                        }
                                               
                    }                    
                }

                provider.Save();
                

            }        
            
        }      

        public static void SaveReserveContent(string orderId, string content)
        {
            using (var provider = new Entity.LightProvider())
            {
                var header = provider.GetOrderDataByOrderId(orderId);

                SaveReserveContentInContext(provider, header, content);
                provider.Save();
            }
        }

        public static void SaveReserveContentInContext(Entity.LightProvider context, Entity.OrderHeader header, string content)
        {
            var reserveContent = context.GetReserveContent(header.OrderId, header.WWSOrderId);

            if (reserveContent == null)
            {
                reserveContent = new Entity.ReserveContent()
                {
                    OrderId = header.OrderId,
                    WWSOrderId = header.WWSOrderId,
                    Status = ReserveContentStatus.New.ToShort(),
                    DocContent = content
                };
                context.ReserveContent.AddObject(reserveContent);
            }
            else
            {
                reserveContent.DocContent = content;
                reserveContent.Status = ReserveContentStatus.Updated.ToShort();
            }
        }

        public static ICollection<Order.ReserveContent> GetReservesContentForPrinting(string sapCode, int maxRecordQty)
        {
            using (var provider = new Entity.LightProvider())
            {
                //TODO: what we have to do with Error status?
                var statusNew = ReserveContentStatus.New.ToShort();
                var statusUpdated = ReserveContentStatus.Updated.ToShort();
                var result = (from header in provider.OrderHeader
                              join reserveContent in provider.ReserveContent on new { header.OrderId, header.WWSOrderId }
                                  equals
                                  new { reserveContent.OrderId, reserveContent.WWSOrderId }
                              where header.SapCode == sapCode && (reserveContent.Status == statusNew || reserveContent.Status == statusUpdated)
                              select new Order.ReserveContent()
                              {
                                  OrderId = reserveContent.OrderId,
                                  WWSOrderId = reserveContent.WWSOrderId,
                                  Content = reserveContent.DocContent
                              }).Take(maxRecordQty);

                return result.ToList();
            }
        }
    }
}
