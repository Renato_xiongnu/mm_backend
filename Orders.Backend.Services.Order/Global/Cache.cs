﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;

using Entity = Orders.Backend.Dal;

namespace Orders.Backend.Services.Order.Global
{
    internal class CacheDict<T>
    {
        public IDictionary<string, T> Data;
        public object SyncRoot;
        public DateTime time;
        public CacheDict()
        {
            SyncRoot = new object();
            Data = new Dictionary<string,  T>();
        }
    }

    public static class Cache
    {
        private static CacheDict<Entity.Store> _storeData;
        private static CacheDict<Entity.SysMailerSetting> _mailerSetting;

        static Cache()
        {
            _storeData = new CacheDict<Entity.Store>();
            _mailerSetting = new CacheDict<Entity.SysMailerSetting>();
        }

        public static Entity.Store GetStore(string sapCode)
        {
            return GetData(sapCode, _storeData, cache => 
            {
                Entity.Store newStore = null;
                using (var provider = new Entity.LightProvider())
                {
                    var stores = provider.Stores.Include("City1").ToList();
                    if (stores == null)
                        throw new System.Data.ObjectNotFoundException("Stores not found");

                    foreach (var store in stores)
                    {
                        cache.Data.Add(store.SapCode, store);
                    }

                    if (!cache.Data.ContainsKey(sapCode))
                        throw new System.Data.ObjectNotFoundException(string.Format("Store with SapCode {0} not found", sapCode));

                    newStore = cache.Data[sapCode];
                }

                return newStore;
            });
        }


        public static Entity.SysMailerSetting GetSysMailerSetting(string sendingType)
        {
            return GetData(sendingType, _mailerSetting, cache => 
            {
                Entity.SysMailerSetting mailerSetting;

                using (var provider = new Entity.LightProvider())
                {
                    var settings = provider.SysMailerSettings.ToList();
                    if (settings == null)
                        throw new System.Data.ObjectNotFoundException("Settings not found");

                    foreach (var item in settings)
                    {
                        cache.Data.Add(item.SendingType, item);
                    }

                    if (!cache.Data.ContainsKey(sendingType))
                        throw new System.Data.ObjectNotFoundException(string.Format("Store with SapCode {0} not found", sendingType));

                    mailerSetting = cache.Data[sendingType];
                }

                return mailerSetting;
            });
        }



        private static T GetData<T>(string key, CacheDict<T> cache, Func<CacheDict<T>, T> queryFunc) where T : EntityObject
        {
            lock (cache.SyncRoot)
            {
                var currentTime = DateTime.Now;

                if (cache.time > DateTime.MinValue && currentTime - cache.time < Settings.CacheTime && cache.Data.ContainsKey(key))
                    return cache.Data[key];

                cache.Data.Clear();
                cache.time = currentTime;
                return queryFunc(cache);
            }
        }

    }
}
