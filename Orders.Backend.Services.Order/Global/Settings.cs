﻿using System;
using System.Configuration;

namespace Orders.Backend.Services.Order.Global
{
    public static class Settings
    {
        public static TimeSpan CacheTime { get { return new TimeSpan(0, 0, Convert.ToInt32(ConfigurationManager.AppSettings["CacheTime"]), 0); } }
    }
}
