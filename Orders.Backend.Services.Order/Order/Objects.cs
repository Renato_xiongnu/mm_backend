﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orders.Backend.Services.Order.Order
{
    public class GiftCertificateForCrediting
    {
        public string ChainType { get; set; }

        public string SapCode { get; set; }

        public string CertificateId { get; set; }

        public Decimal Amount { get; set; }
    }

    public class OrdersStatistics
    {
        [PropertyFieldDescription(Name = "Всего заказов")]
        public int TotalQty { get; set; }

        [PropertyFieldDescription(Name = "Кол-во оплаченных")]
        public int PaidQty { get; set; }

        [PropertyFieldDescription(Name = "Кол-во отмененных")]
        public int CanceledQty { get; set; }

        [PropertyFieldDescription(Name = "Кол-во завершенных")]
        public int FinishedQty { get; set; }
    }

    public class ReserveContent
    {
        public string OrderId { get; set; }

        public string WWSOrderId { get; set; }

        public string Content { get; set; }
    }
}
