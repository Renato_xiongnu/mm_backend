﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Transactions;
using Common.Helpers;
using iTextSharp.text.log;
using NLog;
using MMS.Multiplexor.Client;
using Order.Backend.Dal.Common;
using Orders.Backend.Dal.Entities;
using Orders.Backend.Services.Order.Common;
using Orders.Backend.Services.Contracts.AdminService;
using Orders.Backend.Services.Contracts.GiftCertificate;
using Orders.Backend.Services.Contracts.OrderService;
using Orders.Backend.Services.Order.Facades;
using Orders.Backend.Services.Order.InstallationService;
using Entity = Orders.Backend.Dal;
using StockLocationOrderCount = Orders.Backend.Services.Contracts.OrderService.StockLocationOrderCount;

namespace Orders.Backend.Services.Order
{
    public class OrderManager
    {
        private static readonly Logger Logger;

        static OrderManager()
        {
            Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);
        }

        #region [Untouched]

        /// <summary>
        /// OrderService
        /// </summary>
        /// <param name="contentData"></param>
        /// <returns></returns>
        public OperationResult UpdateReserveContentForPrinting(UpdateReservesContentData contentData)
        {
            var result = new OperationResult
            {
                ReturnCode = ReturnCode.Ok
            };
            try
            {
                PrintingReserveFacade.UpdateReserveContentStatus(contentData.Contents);
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(ex.GetExceptionFullInfoText());
            }
            return result;
        }

        /// <summary>
        /// OrderService
        /// </summary>
        /// <param name="sapCode"></param>
        /// <param name="maxQty"></param>
        /// <returns></returns>
        public GetReservesContentResult GetReservesContentForPrinting(string sapCode, int maxQty)
        {
            var result = new GetReservesContentResult()
            {
                ReturnCode = ReturnCode.Ok
            };

            try
            {
                var contents = PrintingReserveFacade.GetReservesContentForPrinting(sapCode, maxQty);
                result.Contents = contents.Select(t => t.ToContract()).ToList();
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(ex.GetExceptionFullInfoText());
            }

            return result;

        }

        #endregion

        public GetStoreDataResult GetStoreData(string sapCode)
        {

            var result = new GetStoreDataResult { ReturnCode = ReturnCode.Ok };
            try
            {
                var storeData = OrderHeaderFacade.GetStoreData(sapCode);
                result.StoreData = storeData;
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(ex.Message);
            }

            return result;            
        }

        public GetAllStoresDataResult GetAllStoresData()
        {
            var result = new GetAllStoresDataResult { ReturnCode = ReturnCode.Ok };
            try
            {
                var storeData = OrderHeaderFacade.GetAllStoresData();
                result.Stores = storeData;
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(ex.Message);
            }

            return result;
        }

        public CreateOrderOperationResult CreateOrder(ClientData clientData, CreateOrderData orderData,
                                                      DeliveryInfo deliveryInfo,
                                                      ICollection<ArticleData> articlesData)
        {
            var operationResult = new CreateOrderOperationResult { ReturnCode = ReturnCode.Ok };
            try
            {
                var id = OrderHeaderFacade.CreateOrderHeader(clientData, orderData, deliveryInfo, articlesData);
                operationResult.OrderId = id;
                operationResult.GeneralOrderStatus = GeneralOrderStatus.Created;
            }
            catch (InvalidOperationException ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("Data error {0}", ex.Message);
            }
            catch (Exception ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("System error {0}", ex.GetExceptionFullInfoText());
            }
            return operationResult;
        }

        public UpdateOrderOperationResult UpdateOrderByEmployee(UpdateOrderData orderData)
        {
            var operationResult = new UpdateOrderOperationResult
            {
                ReturnCode = ReturnCode.Ok
            };
            try
            {
                var internalStatus = OrderHeaderFacade.UpdateOrderInfo(orderData, false);
                var returnStatus = GetOrderStatus(internalStatus);

                operationResult.FromStatus = returnStatus;
                operationResult.ToStatus = returnStatus;
            }
            catch (InvalidOperationException ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("Data error {0}", ex.Message);
            }
            catch (Exception ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("System error {0}", ex.GetExceptionFullInfoText());
            }


            return operationResult;

        }

        public UpdateOrderOperationResult UpdateOrder(UpdateOrderData orderData)
        {
            var operationResult = new UpdateOrderOperationResult
            {
                ReturnCode = ReturnCode.Ok
            };

            try
            {
                if (orderData.ChangeStatusTo.HasValue)
                {
                    var changeStatusTo = orderData.ChangeStatusTo;
                    switch (changeStatusTo)
                    {
                        case GeneralOrderStatus.Created:
                            var statusBeforeCreated = OrderHeaderFacade.Created(orderData.OrderId);
                            operationResult.FromStatus = GetOrderStatus(statusBeforeCreated);
                            operationResult.ToStatus = GeneralOrderStatus.Created;
                            break;
                        case GeneralOrderStatus.Confirmed:
                            OrderHeaderFacade.Confirmed(orderData.OrderId, orderData.WWSOrderId,
                                orderData.UpdatedBy,
                                orderData.ReasonText, orderData.ExpirationDate);
                            operationResult.FromStatus = GeneralOrderStatus.Created;
                            operationResult.ToStatus = GeneralOrderStatus.Confirmed;
                            AddInstallationToQueue(orderData.OrderId, InstallationCreationOperationType.Create);
                            break;
                        case GeneralOrderStatus.ConfirmedWithGiftCertificate:
                            OrderHeaderFacade.Confirmed(orderData.OrderId, orderData.WWSOrderId,
                                orderData.UpdatedBy,
                                orderData.ReasonText);
                            operationResult.FromStatus = GeneralOrderStatus.Created;
                            operationResult.ToStatus = GeneralOrderStatus.ConfirmedWithGiftCertificate;
                            break;
                        case GeneralOrderStatus.Rejected:
                            OrderHeaderFacade.Reject(orderData.OrderId, ChangedType.Manager,
                                orderData.UpdatedBy, orderData.ReasonText);
                            operationResult.FromStatus = GeneralOrderStatus.Created;
                            operationResult.ToStatus = GeneralOrderStatus.Rejected;
                            AddCouponToQueue(orderData.OrderId, CouponsUseQueueOperationType.UnfreezCoupon);
                            AddInstallationToQueue(orderData.OrderId,InstallationCreationOperationType.Cancel);
                            break;
                        case GeneralOrderStatus.RejectedByCustomer:
                            OrderHeaderFacade.Reject(orderData.OrderId, ChangedType.Customer,
                                orderData.UpdatedBy, orderData.ReasonText);
                            operationResult.FromStatus = GeneralOrderStatus.Created;
                            operationResult.ToStatus = GeneralOrderStatus.RejectedByCustomer;
                            AddCouponToQueue(orderData.OrderId, CouponsUseQueueOperationType.UnfreezCoupon);
                            AddInstallationToQueue(orderData.OrderId, InstallationCreationOperationType.Cancel);
                            break;
                        case GeneralOrderStatus.GiftCertificateNumberReservedNotifyCustomer:
                            OrderHeaderFacade.GiftCertificateNumberReservedNotifyCustomer(orderData.OrderId,
                                orderData.UpdatedBy,
                                orderData.ReasonText);
                            operationResult.FromStatus = GeneralOrderStatus.Confirmed;
                            operationResult.ToStatus =
                                GeneralOrderStatus.GiftCertificateNumberReservedNotifyCustomer;
                            break;
                        case GeneralOrderStatus.Paid:
                            var statusBeforePaid = OrderHeaderFacade.Paid(orderData.OrderId);
                            operationResult.FromStatus = GetOrderStatus(statusBeforePaid);
                            operationResult.ToStatus = GeneralOrderStatus.Closed;
                            AddCouponToQueue(orderData.OrderId, CouponsUseQueueOperationType.UseCoupon);
                            //UseCoupon(orderData.OrderId);
                            break;
                        case GeneralOrderStatus.Closed:
                            var statusBeforeClosed = OrderHeaderFacade.Close(orderData.OrderId);
                            operationResult.FromStatus = GetOrderStatus(statusBeforeClosed);
                            operationResult.ToStatus = GeneralOrderStatus.Closed;
                            break;
                        case GeneralOrderStatus.Shipped:
                            var statusBeforeShipped = OrderHeaderFacade.Shipped(orderData.OrderId);
                            operationResult.FromStatus = GetOrderStatus(statusBeforeShipped);
                            operationResult.ToStatus = GeneralOrderStatus.Shipped;
                            break;
                        case GeneralOrderStatus.OnlinePaymentReceived:
                            //TODO: add code for OnlinePaymentReceived                            
                            operationResult.FromStatus = GeneralOrderStatus.Confirmed;
                            operationResult.ToStatus = GeneralOrderStatus.OnlinePaymentReceived;
                            break;
                        case GeneralOrderStatus.OnlinePaymentDeclined:
                            //TODO: add code for OnlinePaymentDeclined                            
                            operationResult.FromStatus = GeneralOrderStatus.Confirmed;
                            operationResult.ToStatus = GeneralOrderStatus.OnlinePaymentDeclined;
                            break;
                        default:
                            operationResult.FromStatus = changeStatusTo.Value;
                            operationResult.ToStatus = changeStatusTo.Value;
                            break;
                    }
                    OrderHeaderFacade.UpdateOrderInfo(orderData, true);
                }
                else
                {
                    var internalStatus = OrderHeaderFacade.UpdateOrderInfo(orderData, false);
                    var returnStatus = GetOrderStatus(internalStatus);

                    operationResult.FromStatus = returnStatus;
                    operationResult.ToStatus = returnStatus;
                }
            }
            catch (InvalidOperationException ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("Data error {0}", ex.GetExceptionFullInfoText(true));
            }
            catch (Exception ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("System error {0}", ex.GetExceptionFullInfoText(true));
            }


            return operationResult;
        }

        public UpdateOrderOperationResult UpdateOrderWithReserve(UpdateOrderData orderData, ReserveInfo newReserveInfo = null)
        {
            var operationResult = new UpdateOrderOperationResult
            {
                ReturnCode = ReturnCode.Ok
            };

            try
            {
                // Use Single Context
                using (var context = new Dal.LightProvider())
                {
                    // Get Current Header With Joins
                    var header = context.GetOrderDataByOrderId(orderData.OrderId, 
                                                            Dal.RelatedEntity.OrderLines |
                                                            Dal.RelatedEntity.GiftCert |
                                                            Dal.RelatedEntity.Customer |
                                                            Dal.RelatedEntity.Delivery);
                    if (header == null)
                    {
                        throw new InvalidOperationException(orderData.OrderId + " doesn't exist");
                    }

                    Logger.Info("{0} : UpdateOrderInContext starts", orderData.OrderId);
                    UpdateOrderInContext(context, header, orderData, operationResult);
                    context.Save();
                    Logger.Info("{0} : UpdateOrderInContext stops", orderData.OrderId);                    

                    if (newReserveInfo != null)
                    {
                        Logger.Info("{0} : UpdateReserveInfoInContext starts", orderData.OrderId);
                        OrderHeaderFacade.UpdateReserveInfoInContext(context, header, newReserveInfo, false);
                        context.Save();
                        Logger.Info("{0} : UpdateReserveInfoInContext stops", orderData.OrderId);
                    }

                    if (!string.IsNullOrEmpty(orderData.ReserveContent))
                    {
                        Logger.Info("{0} : SaveReserveContentInContext starts", orderData.OrderId);
                        PrintingReserveFacade.SaveReserveContentInContext(context, header, orderData.ReserveContent);
                        context.Save();
                        Logger.Info("{0} : SaveReserveContentInContext stops", orderData.OrderId);
                    }
                    //context.Save();
                }
            }
            catch (InvalidOperationException ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("Data error {0}", ex.GetExceptionFullInfoText(true));
            }
            catch (Exception ex)
            {
                operationResult.ReturnCode = ReturnCode.Error;
                operationResult.AddError("System error {0}", ex.GetExceptionFullInfoText(true));
            }
            return operationResult;
        }

        public static void UpdateOrderInContext(Entity.LightProvider context, Entity.OrderHeader header, UpdateOrderData orderData, UpdateOrderOperationResult operationResult)
        {
            // Process Changes
            // Should call only *InContext methods
            if (orderData.ChangeStatusTo.HasValue)
            {
                var changeStatusTo = orderData.ChangeStatusTo.Value;
                switch (changeStatusTo)
                {
                    case GeneralOrderStatus.Created:
                        var statusBeforeCreated = OrderHeaderFacade.CreatedInContext(header);
                        operationResult.FromStatus = GetOrderStatus(statusBeforeCreated);
                        operationResult.ToStatus = GeneralOrderStatus.Created;
                        break;
                    case GeneralOrderStatus.Confirmed:
                        OrderHeaderFacade.ConfirmedInContext(header, orderData.WWSOrderId, orderData.UpdatedBy, orderData.ReasonText, orderData.ExpirationDate);
                        operationResult.FromStatus = GeneralOrderStatus.Created;
                        operationResult.ToStatus = GeneralOrderStatus.Confirmed;
                        AddInstallationToQueueInContext(context, header.OrderId, InstallationCreationOperationType.Create);
                        break;
                    case GeneralOrderStatus.ConfirmedWithGiftCertificate:
                        OrderHeaderFacade.ConfirmedInContext(header, orderData.WWSOrderId, orderData.UpdatedBy, orderData.ReasonText);
                        operationResult.FromStatus = GeneralOrderStatus.Created;
                        operationResult.ToStatus = GeneralOrderStatus.ConfirmedWithGiftCertificate;
                        // TODO: Почему не вызывается AddInstallationToQueue ?
                        break;
                    case GeneralOrderStatus.Rejected:
                        OrderHeaderFacade.RejectInContext(header, ChangedType.Manager, orderData.UpdatedBy, orderData.ReasonText);
                        operationResult.FromStatus = GeneralOrderStatus.Created;
                        operationResult.ToStatus = GeneralOrderStatus.Rejected;
                        AddCouponToQueueInContext(context, orderData.OrderId, CouponsUseQueueOperationType.UnfreezCoupon, header.CouponCode);
                        AddInstallationToQueueInContext(context, orderData.OrderId, InstallationCreationOperationType.Cancel);
                        break;
                    case GeneralOrderStatus.RejectedByCustomer:
                        OrderHeaderFacade.RejectInContext(header, ChangedType.Customer, orderData.UpdatedBy, orderData.ReasonText);
                        operationResult.FromStatus = GeneralOrderStatus.Created;
                        operationResult.ToStatus = GeneralOrderStatus.RejectedByCustomer;
                        AddCouponToQueueInContext(context, orderData.OrderId, CouponsUseQueueOperationType.UnfreezCoupon, header.CouponCode);
                        AddInstallationToQueueInContext(context, orderData.OrderId, InstallationCreationOperationType.Cancel);
                        break;
                    case GeneralOrderStatus.GiftCertificateNumberReservedNotifyCustomer:
                        OrderHeaderFacade.GiftCertificateNumberReservedNotifyCustomerInContext(header, orderData.UpdatedBy, orderData.ReasonText);
                        operationResult.FromStatus = GeneralOrderStatus.Confirmed;
                        operationResult.ToStatus = GeneralOrderStatus.GiftCertificateNumberReservedNotifyCustomer;
                        break;
                    case GeneralOrderStatus.Paid:
                        var statusBeforePaid = OrderHeaderFacade.PaidInContext(header);
                        operationResult.FromStatus = GetOrderStatus(statusBeforePaid);
                        operationResult.ToStatus = GeneralOrderStatus.Closed;
                        AddCouponToQueueInContext(context, orderData.OrderId, CouponsUseQueueOperationType.UseCoupon, header.CouponCode);
                        break;
                    case GeneralOrderStatus.Closed:
                        var statusBeforeClosed = OrderHeaderFacade.CloseInContext(header);
                        operationResult.FromStatus = GetOrderStatus(statusBeforeClosed);
                        operationResult.ToStatus = GeneralOrderStatus.Closed;
                        break;
                    case GeneralOrderStatus.Shipped:
                        var statusBeforeShipped = OrderHeaderFacade.ShippedInContext(header);
                        operationResult.FromStatus = GetOrderStatus(statusBeforeShipped);
                        operationResult.ToStatus = GeneralOrderStatus.Shipped;
                        break;
                    case GeneralOrderStatus.OnlinePaymentReceived:
                        //TODO: add code for OnlinePaymentReceived                            
                        operationResult.FromStatus = GeneralOrderStatus.Confirmed;
                        operationResult.ToStatus = GeneralOrderStatus.OnlinePaymentReceived;
                        break;
                    case GeneralOrderStatus.OnlinePaymentDeclined:
                        //TODO: add code for OnlinePaymentDeclined                            
                        operationResult.FromStatus = GeneralOrderStatus.Confirmed;
                        operationResult.ToStatus = GeneralOrderStatus.OnlinePaymentDeclined;
                        break;
                    default:
                        operationResult.FromStatus = changeStatusTo;
                        operationResult.ToStatus = changeStatusTo;
                        break;
                }
                OrderHeaderFacade.UpdateOrderInfoInContext(header, orderData, true);
            }
            else
            {
                var internalStatus = OrderHeaderFacade.UpdateOrderInfoInContext(header, orderData, false);
                var returnStatus = GetOrderStatus(internalStatus);

                operationResult.FromStatus = returnStatus;
                operationResult.ToStatus = returnStatus;
            }
        }

      
        private void AddCouponToQueue(string orderId, CouponsUseQueueOperationType operationType)
        {
            try
            {
                var couponCode = GetCouponCodeFromOrder(orderId);

                if (!string.IsNullOrEmpty(couponCode))
                {
                    using (var context = new Entity.LightProvider())
                    {
                        AddCouponToQueueInContext(context, orderId, operationType, couponCode);
                        context.Save();
                    }
                    Logger.Info(string.Format("Add coupon {0} to queue for order {1} for operation {2}", couponCode, orderId, operationType));
                }
                else
                {
                    Logger.Info("Can't get couponCode for order " + orderId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private static void AddCouponToQueueInContext(Entity.LightProvider context, string orderId, CouponsUseQueueOperationType operationType, string couponCode)
        {
            if (!string.IsNullOrEmpty(couponCode))
            {
                var curentDate = DateTime.UtcNow;
                var item = new Entity.CouponsUseQueue
                {
                    CouponCode = couponCode,
                    OrderId = orderId,
                    CreateDate = curentDate,
                    UpdateDate = curentDate,
                    TryCount = 0,
                    OperationType = (int)operationType,
                    Processed = false
                };
                context.CouponsUseQueue.AddObject(item);
            }
        }

        private void AddInstallationToQueue(string orderId, InstallationCreationOperationType creationType)
        {
            try
            {
                using (var context = new Entity.LightProvider())
                {
                    AddInstallationToQueueInContext(context, orderId, creationType);
                    context.Save();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public static void AddInstallationToQueueInContext(Entity.LightProvider context, string orderId, InstallationCreationOperationType creationType)
        {
            var curentDate = DateTime.UtcNow;
            var item = new Entity.InstallationCreationQueue
            {
                OrderId = orderId,
                CreateDate = curentDate,
                UpdateDate = curentDate,
                TryCount = 0,
                OperationType = (short)creationType,
                Processed = false
            };
            context.InstallationCreationQueue.AddObject(item); //Можно делать еще запрос, чтобы н сохрантяь все подряд. До оптимизации Update дешевле сохранять в БД
        }

        private string GetCouponCodeFromOrder(string orderId)
        {
            var result = string.Empty;
            var order = OrderHeaderFacade.GetOrderInfoByOrderId(orderId);
            if (order != null && order.OrderInfo != null && !string.IsNullOrEmpty(order.OrderInfo.CouponCode))
                result = order.OrderInfo.CouponCode;
            return result;
        }

        public GerOrderDataResult GetOrderInfoByOrderId(string orderId)
        {
            return OrderHeaderFacade.GetOrderInfoByOrderId(orderId);
        }

        public ICollection<GerOrderDataResult> GetOrderInfoByOrderIds(ICollection<string> orderIds)
        {
            return OrderHeaderFacade.GetOrderInfoByOrderIds(orderIds);
        }

        public ICollection<GerOrderDataResult> GetOrderInfoByType(short type, ICollection<string> sapCodes = null)
        {
            return OrderHeaderFacade.GetOrderInfoByType(type, sapCodes);
        }

        public GerOrderDataResult GetOrderInfoByWWSOrderId(string sapCode, string wwsOrderId)
        {
            return OrderHeaderFacade.GetOrderInfoByWWSOrderId(sapCode, wwsOrderId);
        }

        public GerOrderDataResult GetOrderInfoByCertificate(string certificateId)
        {
            return OrderHeaderFacade.GetOrderInfoByCertificate(certificateId);
        }

        public OperationResult UpdateReserveInfo(string orderId, ReserveInfo reserveInfo, string reserveContent)
        {
            var result = new OperationResult() { ReturnCode = ReturnCode.Ok };
            try
            {
                OrderHeaderFacade.UpdateReserveInfo(orderId, reserveInfo, false);
                if (!string.IsNullOrEmpty(reserveContent))
                    PrintingReserveFacade.SaveReserveContent(orderId, reserveContent);
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(ex.GetExceptionFullInfoText(true));
            }

            return result;
        }

        public OperationResult UpdateReserveLinesByEmployee(string orderId, ReserveInfo reserveInfo)
        {
            var result = new OperationResult() { ReturnCode = ReturnCode.Ok };
            try
            {
                OrderHeaderFacade.UpdateReserveInfo(orderId, reserveInfo, true);
            }
            catch (Exception ex)
            {
                result.ReturnCode = ReturnCode.Error;
                result.AddError(ex.Message);
            }

            return result;
        }


        private static GeneralOrderStatus GetOrderStatus(InternalOrderStatus internalStatus)
        {
            switch(internalStatus)
            {
                case InternalOrderStatus.Blank:                    
                case InternalOrderStatus.Created:
                    return GeneralOrderStatus.Created;
                case InternalOrderStatus.Confirmed:
                    return GeneralOrderStatus.Confirmed;                
                case InternalOrderStatus.Paid:
                    return GeneralOrderStatus.Paid;                
                case InternalOrderStatus.Shipped:
                    return GeneralOrderStatus.Shipped;                                
                case InternalOrderStatus.Closed:
                    return GeneralOrderStatus.Closed;
                case InternalOrderStatus.Rejected:
                    return GeneralOrderStatus.Rejected;
                case InternalOrderStatus.RejectedByCustomer:
                    return GeneralOrderStatus.RejectedByCustomer;
                case InternalOrderStatus.GiftCertificateCredited:
                case InternalOrderStatus.GiftCertificateCreditedNotifyCustomer:
                case InternalOrderStatus.GiftCertificateCreditedError:
                case InternalOrderStatus.GiftCertificateNumberReservedNotifyCustomer:
                case InternalOrderStatus.LifeCycleIgnored:                    
                    throw new ArgumentOutOfRangeException("internalStatus");
                default:
                    throw new ArgumentOutOfRangeException("internalStatus");
            }            
        }

        private ICollection<EntityStatistics> GetStatistics<T>(T stat)
        {
            var type = stat.GetType();
            return type.GetProperties().Select(property => new EntityStatistics
            {
                PrimitiveType = property.DeclaringType.ToString(),
                Value = property.GetValue(stat, null),
                Name = property.GetDescription()
            }).ToList();
        }



        #region [Later]
       
        #endregion

        public GerOrderStatusHistoryResult GetOrderStatusHistoryByOrderId(string orderId)
        {
            return OrderHeaderFacade.GetOrderStatusHistoryByOrderId(orderId);  
        }

        public GerReservesResult GetReservesByOrderId(string orderId)
        {
            return OrderHeaderFacade.GetReservesByOrderId(orderId);  
        }

        public GetOrderDataResult GetOrderDataByBasketId(string basket3Id)
        {
            return OrderHeaderFacade.GetOrderDataByBasketId(basket3Id);
        }

        public IEnumerable<StockLocationOrderCount> GetOrderCountInStockBySaleLocationAndStockLoations(String saleLocationSapCode, IEnumerable<String> stockLocationSapCodes, Int32 daysRange)
        {
            return OrderHeaderFacade.GetOrderCountInStockBySaleLocationAndStockLoations(saleLocationSapCode, stockLocationSapCodes, daysRange);
        }
    }
   
}
