﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Helpers;
using log4net;
using MMS.Dadata;
using MMS.Dadata.Contracts;
using Newtonsoft.Json;
using OnlineOrders.Common.Helpers;
using Orders.Backend.Services.Order.Order;
using Entity = Orders.Backend.Dal;
using EntityObjects = Order.Backend.Dal.Common;
using OrderContract = Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Services.Order.Common
{
    public static class Extensions
    {
        private static readonly ILog Logger = LogManager.GetLogger("OrderExtensions");

        public static Entity.OrderHeader ToOrderHeader(this OrderContract.CreateOrderData orderData)
        {
            //here
            return new Entity.OrderHeader
            {
                OrderId = orderData.OrderId,
                // WWSOrderId - было в bll
                SapCode = orderData.StoreInfo.SapCode,
                PaymentType = orderData.PaymentType.ConvertTo<EntityObjects.OrderPaymentType>().ToShort(),
                FinalPaymentType = orderData.PaymentType.ConvertTo<EntityObjects.OrderPaymentType>().ToShort(),
                OrderSource = orderData.OrderSource.ConvertTo<EntityObjects.OrderSourceType>().ToString(),
                Comment = orderData.Comment,
                BasketComment = orderData.BasketComment,
                ExternalNumber = orderData.ExternalNumber,
                ExternalSystem = orderData.ExternalSystem.ConvertTo<EntityObjects.ExternalSystem>().ToShort(),
                ExpirationDate = Entity.LightProvider.GetValidDbDateTime(orderData.ExpirationDate),
                IsPreorder = orderData.IsPreorder,
                UtmSource = orderData.UtmSource,
                Prepay = orderData.Prepay.HasValue ? orderData.Prepay.Value : 0,
                Created = orderData.Created.HasValue ? orderData.Created.Value : DateTime.UtcNow,
                BasketId = orderData.BasketId,
                SaleLocationSapCode = orderData.InitialSapCode,
                CouponCode = orderData.CouponCode,
                InstallServiceAddress = orderData.InstallServiceAddress, 
            };
        }

        public static Entity.Customer ToCustomer(this OrderContract.ClientData clientData, bool allowEmptyEmail = false)
        {
            var customer = new Entity.Customer
            {
                UserId = clientData.UserId,
                Email = allowEmptyEmail && String.IsNullOrEmpty(clientData.Email) ? "" : clientData.Email,
                Name = clientData.Name,
                Surname = clientData.Surname,
                Phone = clientData.Phone,
                Phone2 = clientData.Phone2,                
            };
            if (clientData.SocialCard!=null)
            {
                customer.SocialCardNumber = clientData.SocialCard.Number;                
            }
            return customer;
        }

        public static Entity.Delivery ToDelivery(this OrderContract.DeliveryInfo deliveryInfo, Entity.Customer customer)
        {
            var delivery = new Entity.Delivery
            {
                Customer = customer,
                City = deliveryInfo.City ?? string.Empty,
                Address = deliveryInfo.Address ?? string.Empty,
                DeliveryDate = Entity.LightProvider.GetValidDbDateTime(deliveryInfo.DeliveryDate),
                DeliveryPeriod = deliveryInfo.Period,
                DeliveryComment = deliveryInfo.Comment,
                ExactAddress = deliveryInfo.ExactAddress,
                Latitude = deliveryInfo.Latitude,
                Longitude = deliveryInfo.Longitude,
                HasDelivery = deliveryInfo.HasDelivery,
                PickupLocationId = deliveryInfo.PickupLocationId,
                TransferAgreementWithCustomer = deliveryInfo.TransferAgreementWithCustomer.HasValue && deliveryInfo.TransferAgreementWithCustomer.Value,
                Kladr = deliveryInfo.Kladr,
                ZIPCode = deliveryInfo.ZIPCode,
                Region = deliveryInfo.Region,
                House = deliveryInfo.House,
                StreetAbbr = deliveryInfo.StreetAbbr,
                Street = deliveryInfo.Street,
                Housing = deliveryInfo.Housing,
                Building = deliveryInfo.Building,
                Apartment = deliveryInfo.Apartment,
                Entrance = deliveryInfo.Entrance,
                EntranceCode = deliveryInfo.EntranceCode,
                Floor = deliveryInfo.Floor,
                SubwayStationName = deliveryInfo.SubwayStationName,
                District = deliveryInfo.District,
                HasServiceLift = deliveryInfo.HasServiceLift,
                RequestedDeliveryTimeslot = deliveryInfo.RequestedDeliveryTimeslot,
                RequestedDeliveryDate = deliveryInfo.RequestedDeliveryDate,
                RequestedDeliveryServiceOption = deliveryInfo.RequestedDeliveryServiceOption
            };
            FillFullAddress(delivery);          
            return delivery;
        }

        private static void FillFullAddress(Entity.Delivery delivery)
        {
            var streetAbbrCode = new Dictionary<string, string>
            {
                {"аллея", "ал"},
                {"бульвар", "б-р"},
                {"дорога", "дор"},
                {"микрорайон", "мкр"},
                {"набережная", "наб"},
                {"переулок", "пер"},
                {"площадь", "пл"},
                {"проспект", "пр-кт"},
                {"проезд", "проезд"},
                {"станция", "станция"},
                {"тракт", "тракт"},
                {"улица", "ул"},
                {"шоссе", "ш"}
            };
            if (delivery.HasDelivery)
            {
                var client = new FactorClient();
                var customerInfo = client.VerifyCustomer(new CustomerRequest
                {
                    Address = delivery.Address
                });
                if (customerInfo.Succesfull)
                {
                    var addressData = customerInfo.AddressData;
                    if (addressData.RegionType == "ГОРОД")
                    {
                        delivery.Region = string.Format("г {0}", addressData.Region);
                        delivery.City = addressData.Region;
                    }
                    else
                    {
                        delivery.Region = addressData.Region;
                        delivery.City = addressData.City;
                    }
                    delivery.Kladr = addressData.Kladr;
                    delivery.ZIPCode = addressData.PostalCode;
                    delivery.StreetAbbr = streetAbbrCode.ContainsKey(addressData.StreetType.ToLower())
                        ? streetAbbrCode[addressData.StreetType.ToLower()]
                        : addressData.StreetType;
                    delivery.Street = addressData.Street;
                    delivery.House = addressData.House;
                    delivery.Building = addressData.Block;
                    delivery.Apartment = addressData.Flat;
                    return;
                }
            }           
            delivery.Region = "";                
            delivery.Kladr = "";
            delivery.ZIPCode = "";
            delivery.StreetAbbr = "";
            delivery.Street = "";
            delivery.House = "";
            delivery.Building = "";
            delivery.Apartment = "";
        }        

        public static Entity.OrderLine ToOrderLine(this OrderContract.ArticleData data)
        {
            var initArticleData = data as OrderContract.InitArticleData;
            if(initArticleData != null)
            {
                return initArticleData.ToOrderLine();
            }

            return new Entity.OrderLine
            {
                ArticleNum = data.ArticleNum,
                Qty = data.Qty,
                Price = data.Price,
                Promotions = data.Promotions != null ? data.Promotions.ToJson() : null,
                Title = data.Title,
                BrandTitle = data.BrandTitle,
                IsVirtual = data.IsVirtual,
                OldPrice = data.OldPrice,
                Benefit = data.Benefit,
                CouponCode = data.CouponCode,
                CouponCompainId = data.CouponCompainId,
                RefersToItem = data.RefersToItem,
                ArticleProductType = (short)data.ArticleProductType,
                
            };
        }

        public static Entity.OrderLine ToOrderLine(this OrderContract.InitArticleData data)
        {
            var orderLine = new Entity.OrderLine()
            {
                ArticleNum = data.ArticleNum,
                Qty = data.Qty,
                Price = data.Price,
                Promotions = data.Promotions != null ? data.Promotions.ToJson() : null,
                Title = data.Title,
                BrandTitle = data.BrandTitle,
                IsVirtual = data.IsVirtual,
                OldPrice = data.OldPrice,
                Benefit = data.Benefit,
                CouponCode = data.CouponCode,
                CouponCompainId = data.CouponCompainId,
               
            };

            if(data.SubArticleData != null)
            {
                foreach(var initArticleData in data.SubArticleData)
                {
                    orderLine.NesterOrderLines.Add(initArticleData.ToOrderLine());
                }
            }

            return orderLine;
        }

        public static ICollection<OrderContract.ArticleData> SplitSet(this OrderContract.ArticleData data)
        {
            if(data == null)
            {
                return null;
            }

            var initArticleData = data as OrderContract.InitArticleData;
            if(initArticleData == null)
            {
                return new Collection<OrderContract.ArticleData> {data};
            }

            if(initArticleData.ArticleNum.Length == Constants.SetArticleLength)
            {
                var result = new Collection<OrderContract.ArticleData>();
                var qty = initArticleData.Qty;
                initArticleData.Qty = 1;
                for(var i = 0; i < qty; i++)
                {
                    result.Add(initArticleData.DeepClone());
                }

                return result;
            }

            return new Collection<OrderContract.ArticleData> {data};
        }

        public static Entity.ReserveLine ToReserveLine(this OrderContract.ArticleData data)
        {
            var initArticleData = data as OrderContract.InitArticleData;
            if(initArticleData != null)
            {
                return initArticleData.ToReserveLine();
            }

            return new Entity.ReserveLine
            {
                ArticleNum = data.ArticleNum,
                Qty = data.Qty,
                Price = data.Price,
                StockItemState = data.ItemState.ConvertTo<EntityObjects.StockItemState>().ToShort(),
                Promotions = data.Promotions != null ? data.Promotions.ToJson() : null,
                Title = data.Title,
                BrandTitle = data.BrandTitle,
                TransferSapCode = string.Empty,
                TransferNumber = 0,
                IsVirtual = data.IsVirtual,
                OldPrice = data.OldPrice,
                Benefit = data.Benefit,
                CouponCode = data.CouponCode,
                CouponCompainId = data.CouponCompainId,
                ArticleProductType = (short)data.ArticleProductType,
                RefersToItem = data.RefersToItem
            };
        }

        public static Entity.ReserveLine ToReserveLine(this OrderContract.InitArticleData data)
        {
            var reserveLine = new Entity.ReserveLine();
            var promotionsString = data.Promotions != null ? data.Promotions.ToJson() : null;
            reserveLine.ArticleNum = data.ArticleNum;
            reserveLine.Qty = data.Qty;
            reserveLine.Price = data.Price;
            reserveLine.Title = data.Title;
            reserveLine.BrandTitle = data.BrandTitle;
            reserveLine.StockItemState = data.ItemState.ConvertTo<EntityObjects.StockItemState>().ToShort();
            reserveLine.WWSStockNo = data.StockNumber;
            reserveLine.Promotions = promotionsString;
            reserveLine.HasShippedFromStock = data.HasShippedFromStock;
            reserveLine.SerialNumber = data.SerialNumber;
            reserveLine.TransferSapCode = string.Empty;
            reserveLine.TransferNumber = 0;
            reserveLine.IsVirtual = data.IsVirtual;
            int articleNo;
            if(Int32.TryParse(data.WarrantyInsuranceArticle, out articleNo))
            {
                reserveLine.WI_RelatedArticleNo = articleNo;
            }
            if(data.SubArticleData != null)
            {
                foreach(var initArticleData in data.SubArticleData)
                {
                    reserveLine.NestedReserveLine.Add(initArticleData.ToReserveLine());
                }
            }
            reserveLine.OldPrice = data.OldPrice;
            reserveLine.Benefit = data.Benefit;
            reserveLine.CouponCode = data.CouponCode;
            reserveLine.CouponCompainId = data.CouponCompainId;
            reserveLine.RefersToItem = data.RefersToItem;
            return reserveLine;
        }

        public static OrderContract.ReserveContent ToContract(this ReserveContent content)
        {
            return new OrderContract.ReserveContent()
            {
                OrderId = content.OrderId,
                WWSOrderId = content.WWSOrderId,
                Content = content.Content
            };
        }

        #region [OrderHeader]

        public static OrderContract.OrderInfo ToOrderInfo(this Entity.OrderHeader header)
        {
            var status = header.OrderStatus.ToEnum<EntityObjects.InternalOrderStatus>();
            return new OrderContract.OrderInfo
            {
                OrderId = header.OrderId,
                BasketId = header.BasketId,
                //TODO: check
                OrderSource = header.OrderSource.ToEnum<OrderContract.OrderSource>(),
                PaymentType = header.PaymentType.ToEnum<OrderContract.PaymentType>(),
                FinalPaymentType = header.FinalPaymentType.ToEnum<OrderContract.PaymentType>(),
                IsPreorder = header.IsPreorder,
                SapCode = header.SapCode,
                WWSOrderId = header.WWSOrderId,
                UpdateDate = header.UpdateDate.ToUniversalTime(),
                Comment = header.Comment,
                BasketComment = header.BasketComment,
                ExternalNumber = header.ExternalNumber,
                ExternalSystem = header.ExternalSystem.ToEnum<OrderContract.ExternalSystem>(),
                ExpirationDate =
                    header.ExpirationDate.HasValue ? header.ExpirationDate.Value.UtcDateTime : (DateTime?) null,
                StatusInfo = new OrderContract.InternalStatusInfo
                {
                    Status = status,
                    Text = status.GetDescription()
                },
                Prepay = header.Prepay,
                Created = header.Created,
                certificateInfo = new OrderContract.CertificateInfo(),
                SaleLocationSapCode = header.SaleLocationSapCode.Trim(), //Change type to nvarchar
                CouponCode = header.CouponCode,
                ExpirationDatePostponed = header.ExpirationDatePostponed,
                InstallServiceAddress = header.InstallServiceAddress,
            };

        }

        public static OrderContract.ClientData ToClientData(this Entity.OrderHeader header)
        {
            var customer = header.Customer;
            return customer != null
                ? new OrderContract.ClientData
                {
                    UserId = customer.UserId,
                    Name = customer.Name,
                    Surname = customer.Surname,
                    Phone = customer.Phone,
                    Phone2 = customer.Phone2,
                    Email = customer.Email,
                    SocialCard = new OrderContract.CardInfo
                    {
                        Number = customer.SocialCardNumber
                    }
                }
                : new OrderContract.ClientData();
        }

        public static OrderContract.DeliveryInfo ToDeliveryInfo(this Entity.OrderHeader header)
        {
            var delivery = header.Deliveries.SingleOrDefault(t => t.CustomerId == header.CustomerId);
            return delivery != null
                ? new OrderContract.DeliveryInfo
                {
                    HasDelivery = delivery.HasDelivery,
                    Address = delivery.Address,
                    City = delivery.City,
                    ExactAddress = delivery.ExactAddress,
                    DeliveryDate = delivery.DeliveryDate,
                    Comment = delivery.DeliveryComment,
                    Period = delivery.DeliveryPeriod,
                    Latitude = delivery.Latitude,
                    Longitude = delivery.Longitude,
                    PickupLocationId = delivery.PickupLocationId,
                    TransferAgreementWithCustomer = delivery.TransferAgreementWithCustomer,
                    Kladr = delivery.Kladr,
                    ZIPCode = delivery.ZIPCode,
                    Region = delivery.Region,
                    House = delivery.House,
                    StreetAbbr = delivery.StreetAbbr,
                    Street = delivery.Street,                    
                    Housing = delivery.Housing,
                    Building = delivery.Building,
                    Apartment = delivery.Apartment,
                    Entrance = delivery.Entrance,
                    EntranceCode = delivery.EntranceCode,
                    Floor = delivery.Floor,
                    SubwayStationName = delivery.SubwayStationName,
                    District = delivery.District,
                    HasServiceLift = delivery.HasServiceLift,
                    RequestedDeliveryTimeslot = delivery.RequestedDeliveryTimeslot,
                    RequestedDeliveryDate = delivery.RequestedDeliveryDate,
                    RequestedDeliveryServiceOption = delivery.RequestedDeliveryServiceOption
                }
                : new OrderContract.DeliveryInfo {HasDelivery = false};
        }

        public static OrderContract.ReserveInfo ToReserveInfo(this Entity.OrderHeader header)
        {
            var reserve = header.ReserveHeaders.Single(t => t.WWSOrderId == header.WWSOrderId);
            return ToReserveInfo(reserve);
        }

        public static OrderContract.ReserveInfo ToReserveInfo(this Entity.ReserveHeader reserve)
        {
            Entity.ReserveContent reserveContent = null;
            Entity.ReserveCustInfo reserveCustomer = null;
            using(var provider = new Entity.LightProvider())
            {
                reserveContent = provider.GetReserveContent(reserve.OrderId, reserve.WWSOrderId);
                if(reserve.ReserveCustInfoId.HasValue)
                {
                    reserveCustomer = provider.GetReserveCust(reserve.ReserveCustInfoId.Value);
                }
            }
            var printingStatus = EntityObjects.PrintingStatus.None;
            if(reserveContent == null)
            {
                return new OrderContract.ReserveInfo
                {
                    WWSOrderId = reserve.WWSOrderId,
                    TotalPrice = ToTotalPrice(reserve),
                    //TODO: why?
                    PrepaymentPrice = ToTotalPrice(reserve),
                    LeftoverPrice = ToLeftoverPrice(reserve),
                    DeliveryPeriod = ToDeliveryPeriod(reserve),
                    CreationDate = reserve.CreationDate,
                    Status = reserve.ReserveStatus.ToEnum<EntityObjects.ReserveStatus>(),
                    SapCode = reserve.SapCode,
                    StoreManagerName = reserve.StoreManagerName,
                    PrintingStatus = printingStatus,
                    ReserveLines =
                        reserve.ReserveLine.Where(el => el.ParentReserveLine == null).Select(ToReserveLine).ToList(),
                    CustomerInfo = reserveCustomer == null
                        ? new OrderContract.ReserveCustomerInfo()
                        : new OrderContract.ReserveCustomerInfo
                        {
                            ClassificationNumber = reserveCustomer.ClassificationNumber,
                            City = reserveCustomer.City,
                            Name = reserveCustomer.Name,
                            Surname = reserveCustomer.Surname,
                            Email = reserveCustomer.Email,
                            INN = reserveCustomer.INN,
                            KPP = reserveCustomer.KPP,
                            Phone = reserveCustomer.Phone,
                            Phone2 = reserveCustomer.Phone2,
                            Salutation = reserveCustomer.Salutation,
                            AddressIndex = reserveCustomer.AddressIndex,
                            Address = reserveCustomer.Address,
                            CountryAbbreviation = reserveCustomer.CountryAbbreviation
                        }
                };
            }
            switch(reserveContent.Status.ToEnum<EntityObjects.ReserveContentStatus>())
            {
                case EntityObjects.ReserveContentStatus.New:
                case EntityObjects.ReserveContentStatus.Updated:
                    printingStatus = EntityObjects.PrintingStatus.None;
                    break;
                case EntityObjects.ReserveContentStatus.Error:
                    printingStatus = EntityObjects.PrintingStatus.Error;
                    break;
                case EntityObjects.ReserveContentStatus.Printed:
                    printingStatus = EntityObjects.PrintingStatus.Printed;
                    break;
            }
            return new OrderContract.ReserveInfo
            {
                WWSOrderId = reserve.WWSOrderId,
                TotalPrice = ToTotalPrice(reserve),
                //TODO: why?
                PrepaymentPrice = ToTotalPrice(reserve),
                LeftoverPrice = ToLeftoverPrice(reserve),
                DeliveryPeriod = ToDeliveryPeriod(reserve),
                CreationDate = reserve.CreationDate,
                Status = reserve.ReserveStatus.ToEnum<EntityObjects.ReserveStatus>(),
                SapCode = reserve.SapCode,
                StoreManagerName = reserve.StoreManagerName,
                PrintingStatus = printingStatus,
                ReserveLines =
                    reserve.ReserveLine.Where(el => el.ParentReserveLine == null).Select(ToReserveLine).ToList(),
                CustomerInfo = reserveCustomer == null
                    ? new OrderContract.ReserveCustomerInfo()
                    : new OrderContract.ReserveCustomerInfo
                    {
                        ClassificationNumber = reserveCustomer.ClassificationNumber,
                        City = reserveCustomer.City,
                        Name = reserveCustomer.Name,
                        Surname = reserveCustomer.Surname,
                        Email = reserveCustomer.Email,
                        INN = reserveCustomer.INN,
                        KPP = reserveCustomer.KPP,
                        Phone = reserveCustomer.Phone,
                        Phone2 = reserveCustomer.Phone2,
                        Salutation = reserveCustomer.Salutation,
                        AddressIndex = reserveCustomer.AddressIndex,
                        Address = reserveCustomer.Address,
                        CountryAbbreviation = reserveCustomer.CountryAbbreviation
                    }
            };
        }

        public static OrderContract.PickupLocation ToPickupInfo(this Entity.OrderHeader orderHeader)
        {
            var delivery = orderHeader.Deliveries.SingleOrDefault(t => t.CustomerId == orderHeader.CustomerId);
            if (delivery == null) return null;

            var pickupLocation = delivery.PickupLocation;
            if (pickupLocation == null) return null;
            return new OrderContract.PickupLocation
                {
                    Address = pickupLocation.Address,
                    PickupLocationId = pickupLocation.PickupLocationId,
                    PickupPointType = pickupLocation.PickupPointType.ToPickupPointType(),
                    Title = pickupLocation.Title,
                    OperatorPuPId = pickupLocation.OperatorPuPId,
                    OperatorName = pickupLocation.OperatorName,
                    City = pickupLocation.City,
                    ZipCode = pickupLocation.ZipCode
                };
        }

        private static OrderContract.PickupPointType ToPickupPointType(this string pickupPointType)
        {
            return Dal.Properties.Settings.Default.StorePickupPointTypes.Contains(pickupPointType.ToLower())
                ? OrderContract.PickupPointType.Shop
                : OrderContract.PickupPointType.Postomant;
        }

        #endregion

        #region [ReserveHeader]

        public static OrderContract.ReservePrice ToTotalPrice(this Entity.ReserveHeader reserve)
        {
            return new OrderContract.ReservePrice
            {
                Vat = reserve.TotalVat ?? 0,
                GrossPrice = reserve.TotalGrossPrice ?? 0,
                NetPrice = reserve.TotalNetPrice ?? 0,
                VatPrice = reserve.TotalVatPrice ?? 0
            };
        }

        public static OrderContract.ReservePrice ToLeftoverPrice(this Entity.ReserveHeader reserve)
        {
            return new OrderContract.ReservePrice
            {
                Vat = reserve.LeftoverVat ?? 0,
                GrossPrice = reserve.LeftoverGrossPrice ?? 0,
                NetPrice = reserve.LeftoverNetPrice ?? 0,
                VatPrice = reserve.LeftoverVatPrice ?? 0
            };
        }

        public static OrderContract.DeliveryPeriod ToDeliveryPeriod(this Entity.ReserveHeader reserve)
        {
            return new OrderContract.DeliveryPeriod
            {
                DeliveryDate = reserve.DeliveryDate,
                Comment = reserve.DeliveryComment,
                //TODO: why?
                Period = reserve.DeliveryComment
            };
        }

        #endregion

        public static OrderContract.StoreData ToContract(this Entity.Store store)
        {
            return new OrderContract.StoreData
            {
                Address = store.Address,
                City = store.City,
                Phone = store.Phone,
                ReserveLifeTime =
                    store.ReserveLifeTime.HasValue
                        ? Entity.LightProvider.GetTimeSpan(store.ReserveLifeTime.Value)
                        : new TimeSpan(),
                SapCode = store.SapCode,
                StartWorkingTime = store.StartWorkingTime.HasValue ? store.StartWorkingTime.Value : new TimeSpan(),
                EndWorkingTime = store.EndWorkingTime.HasValue ? store.EndWorkingTime.Value : new TimeSpan(),
                TimeZome = store.City1 != null ? store.City1.TimeZone : "",
                Name = store.Name,
                PaidDeliveryArticleId = store.PaidDeliveryArticleId,
                FreeDeliveryArticleId = store.FreeDeliveryArticleId,
                IsVirtual = store.IsVirtual,
                IsActive = store.IsActive,
                IsHub = store.IsHub,
            };
        }

        public static OrderContract.ArticleData ToArticleData(this Entity.OrderLine line)
        {
            var promotions = string.IsNullOrEmpty(line.Promotions)
                ? new List<string>()
                : JsonConvert.DeserializeObject<ICollection<string>>(line.Promotions);

            return new OrderContract.ArticleData
            {
                ArticleNum = line.ArticleNum,
                Price = line.Price,
                Qty = line.Qty,
                Promotions = promotions,
                Title = line.Title,
                BrandTitle = line.BrandTitle,
                IsVirtual = line.IsVirtual,
                OldPrice = line.OldPrice,
                Benefit = line.Benefit,
                CouponCode = line.CouponCode,
                CouponCompainId = line.CouponCompainId,
                ArticleProductType = line.ArticleProductType.HasValue ? line.ArticleProductType.Value.ToEnum<OrderContract.ArticleProductType>() : OrderContract.ArticleProductType.Product,
                RefersToItem = line.RefersToItem,
            };
        }

        public static OrderContract.ReserveLine ToReserveLine(this Entity.ReserveLine reserveLine)
        {
            var promotions = string.IsNullOrEmpty(reserveLine.Promotions)
                ? new List<string>()
                : JsonConvert.DeserializeObject<ICollection<string>>(
                    reserveLine.Promotions);
            var result = new OrderContract.ReserveLine
            {
                LineId = reserveLine.LineId,
                ReviewItemState = reserveLine.ItemState.ToEnum<OrderContract.ReviewItemState>(),
                StockItemState =
                    reserveLine.StockItemState.HasValue
                        ? reserveLine.StockItemState.Value.ToEnum<OrderContract.ItemState>()
                        : OrderContract.ItemState.New,
                Comment = reserveLine.Comment,
                WWSProductGroupName = reserveLine.WWSProductGroup,
                WWSFreeQty = reserveLine.WWSFreeQty ?? 0,
                WWSReservedQty = reserveLine.WWSReservedQty ?? 0,
                WWSPriceOrig = reserveLine.WWSPriceOrig ?? 0,
                WWSStockNumber = reserveLine.WWSStockNo ?? 0,
                WWSDepartmentNumber = reserveLine.WWSDepartmentNo ?? 0,
                WWSProductGroupNumber = reserveLine.WWSProductGroupNo ?? 0,
                ArticleCondition = reserveLine.ArticleCondition,
                Title = reserveLine.Title,
                WWSPositionNumber = reserveLine.WWSPositionNumber ?? 0,
                WWSStoreNumber = reserveLine.WWSStoreNumber ?? 0,
                WWSVAT = reserveLine.WWSVAT ?? 0,
                ProductType = reserveLine.ProductType.HasValue
                    ? reserveLine.ProductType.Value.ToEnum<OrderContract.ProductType>()
                    : OrderContract.ProductType.Article,
                
                HasShippedFromStock = reserveLine.HasShippedFromStock ?? false,
                SerialNumber = reserveLine.SerialNumber,
                TransferSapCode = reserveLine.TransferSapCode,
                TransferNumber = reserveLine.TransferNumber,
                IsVirtual = reserveLine.IsVirtual,
                
                WarrantyInsurance = new OrderContract.WarrantyInsurance()
                {
                    Number = reserveLine.WI_Number,
                    DocumentPositionNumber = reserveLine.WI_DocumentPositionNumber ?? 0,
                    RelatedArticleNo = reserveLine.WI_RelatedArticleNo ?? 0,
                    WarrantySum = reserveLine.WI_WarrantySum ?? 0,
                    WwsCertificateState = reserveLine.WI_WwsCertificateState,
                    WwsExtensionPrint = reserveLine.WI_WwsExtensionPrint
                },
                ArticleData = new OrderContract.ArticleData()
                {
                    ArticleNum = reserveLine.ArticleNum,
                    Price = reserveLine.Price,
                    Qty = reserveLine.Qty,
                    Title = reserveLine.Title,
                    BrandTitle = reserveLine.BrandTitle,
                    Promotions = promotions,
                    OldPrice = reserveLine.OldPrice,
                    Benefit = reserveLine.Benefit,
                    CouponCode = reserveLine.CouponCode,
                    CouponCompainId = reserveLine.CouponCompainId,
                    RefersToItem = reserveLine.RefersToItem,
                    ArticleProductType = reserveLine.ArticleProductType.ToEnum<OrderContract.ArticleProductType>()
                },
                Benefit = reserveLine.Benefit,
                CouponCode = reserveLine.CouponCode,
                CouponCompainId = reserveLine.CouponCompainId,
                OldPrice = reserveLine.OldPrice,
            };

            if(!reserveLine.NestedReserveLine.IsNullOrEmpty())
            {
                result.SubLines = new Collection<OrderContract.ReserveLine>();
                foreach(var nestedLine in reserveLine.NestedReserveLine)
                {
                    result.SubLines.Add(nestedLine.ToReserveLine());
                }
            }
            result.ArticleProductType = reserveLine.ArticleProductType.ToEnum<OrderContract.ArticleProductType>();
            
            return result;
        }

        public static OrderContract.ReserveLine FindChildRecursive(
            this ICollection<OrderContract.ReserveLine> reserveLines,
            Func<OrderContract.ReserveLine, bool> predicate)
        {
            var resultReserverLine = reserveLines.FirstOrDefault(predicate);
            if(resultReserverLine != null)
            {
                return resultReserverLine;
            }

            foreach(var reserveLine in reserveLines)
            {
                if(reserveLine.SubLines != null)
                {
                    resultReserverLine = reserveLine.SubLines.FindChildRecursive(predicate);
                    if(resultReserverLine != null)
                    {
                        return resultReserverLine;
                    }
                }
            }

            return null;
        }

        public static OrderContract.ReserveLine FindChildRecursive(
            this ICollection<OrderContract.ReserveLine> reserveLines, long lineId)
        {
            return reserveLines.FindChildRecursive(el => el.LineId == lineId);
        }

        #region [Update]

        public static Entity.Delivery UpdateDeliveryFields(this Entity.Delivery delivery,
            OrderContract.DeliveryInfo newInfo)
        {
            delivery.HasDelivery = newInfo.HasDelivery;

            if (!string.IsNullOrEmpty(newInfo.City))
            {
                delivery.City = newInfo.City;
            }

            if (!string.IsNullOrEmpty(newInfo.Address))
            {
                delivery.Address = newInfo.Address;
            }

            if (newInfo.DeliveryDate.HasValue)
            {
                delivery.DeliveryDate = Entity.LightProvider.GetValidDbDateTime(newInfo.DeliveryDate);
            }

            if (!string.IsNullOrEmpty(newInfo.Comment))
            {
                delivery.DeliveryComment = newInfo.Comment;
            }

            if (!string.IsNullOrEmpty(newInfo.Period))
            {
                delivery.DeliveryPeriod = newInfo.Period;
            }

            if (!string.IsNullOrEmpty(newInfo.ExactAddress))
            {
                delivery.ExactAddress = newInfo.ExactAddress;
            }
            if (!string.IsNullOrEmpty(newInfo.PickupLocationId))
            {
                delivery.PickupLocationId = newInfo.PickupLocationId;
            }
            if (newInfo.TransferAgreementWithCustomer.HasValue)
            {
                delivery.TransferAgreementWithCustomer = newInfo.TransferAgreementWithCustomer.Value;
            }
            if (!string.IsNullOrEmpty(newInfo.ZIPCode))
            {
                delivery.ZIPCode = newInfo.ZIPCode;
            }
            if (!string.IsNullOrEmpty(newInfo.Region))
            {
                delivery.Region = newInfo.Region;
            }
            if (!string.IsNullOrEmpty(newInfo.StreetAbbr))
            {
                delivery.StreetAbbr = newInfo.StreetAbbr;
            }
            if (!string.IsNullOrEmpty(newInfo.Street))
            {
                delivery.Street = newInfo.Street;
            }
            if (!string.IsNullOrEmpty(newInfo.House))
            {
                delivery.House = newInfo.House;
            }
            if (!string.IsNullOrEmpty(newInfo.Housing))
            {
                delivery.Housing = newInfo.Housing;
            }
            if (!string.IsNullOrEmpty(newInfo.Building))
            {
                delivery.Building = newInfo.Building;
            }
            if (!string.IsNullOrEmpty(newInfo.Apartment))
            {
                delivery.Apartment = newInfo.Apartment;
            }
            if (!string.IsNullOrEmpty(newInfo.Entrance))
            {
                delivery.Entrance = newInfo.Entrance;
            }
            if (!string.IsNullOrEmpty(newInfo.EntranceCode))
            {
                delivery.EntranceCode = newInfo.EntranceCode;
            }
            if (newInfo.Floor.HasValue)
            {
                delivery.Floor = newInfo.Floor.Value;
            }
            if (!string.IsNullOrEmpty(newInfo.SubwayStationName))
            {
                delivery.SubwayStationName = newInfo.SubwayStationName;
            }
            if (!string.IsNullOrEmpty(newInfo.District))
            {
                delivery.District = newInfo.District;
            }
            if (newInfo.HasServiceLift.HasValue)
            {
                delivery.HasServiceLift = newInfo.HasServiceLift.Value;
            }
            if (!string.IsNullOrEmpty(newInfo.RequestedDeliveryTimeslot))
            {
                delivery.RequestedDeliveryTimeslot = newInfo.RequestedDeliveryTimeslot;
            }
            if (!string.IsNullOrEmpty(newInfo.RequestedDeliveryTimeslot))
            {
                delivery.Kladr = newInfo.Kladr;
            }
            if (!string.IsNullOrEmpty(newInfo.RequestedDeliveryServiceOption))
            {
                delivery.RequestedDeliveryServiceOption = newInfo.RequestedDeliveryServiceOption;
            }
            FillFullAddress(delivery);
            return delivery;
        }

        public static void UpdateHeaderFields(this Entity.OrderHeader orderHeader, OrderContract.UpdateOrderData newData)
        {
            if(newData.FinalPaymentType.HasValue)
            {
                orderHeader.FinalPaymentType =
                    newData.FinalPaymentType.Value.ConvertTo<EntityObjects.OrderPaymentType>().ToShort();
            }

            if(newData.IsPreorder.HasValue)
            {
                orderHeader.IsPreorder = newData.IsPreorder.Value;
            }
            if(!string.IsNullOrEmpty(newData.SapCode) && !string.IsNullOrWhiteSpace(newData.SapCode))
            {
                orderHeader.SapCode = newData.SapCode;
            }

            if(!string.IsNullOrEmpty(newData.Comment))
            {
                orderHeader.Comment = newData.Comment;
            }

            if (newData.Prepay.HasValue)
            {
                orderHeader.Prepay = newData.Prepay.Value;
            }

            if (newData.BasketId!=null)
            {
                orderHeader.BasketId = newData.BasketId;
            }
            
            if (!string.IsNullOrEmpty(newData.CouponCode))
                orderHeader.CouponCode = newData.CouponCode;

            if (newData.ExpirationDate.HasValue &&
                (orderHeader.OrderStatus != (int) EntityObjects.InternalOrderStatus.Confirmed ||
                 (orderHeader.ExpirationDatePostponed && orderHeader.ExpirationDate.HasValue &&
                  (orderHeader.ExpirationDate.Value.Date != newData.ExpirationDate.Value.Date))))
                Logger.InfoFormat(
                    "For order {0}: Expiration date allowed only for orders in Confirmed state and only one time",
                    orderHeader.OrderId);
            if (newData.ExpirationDate.HasValue &&
                orderHeader.OrderStatus == (int) EntityObjects.InternalOrderStatus.Confirmed &&
                !orderHeader.ExpirationDatePostponed)
            {
                orderHeader.ExpirationDate = newData.ExpirationDate;
                orderHeader.ExpirationDatePostponed = true;
            }
            if(!string.IsNullOrEmpty(newData.InstallServiceAddress))
                orderHeader.InstallServiceAddress = newData.InstallServiceAddress; 

            return;
        }

        public static Entity.Customer UpdateCustomerFields(this Entity.Customer customer,
            OrderContract.ClientData newData)
        {
            if (newData.UserId!=null)
            {
                customer.UserId = newData.UserId;
            }
            if(!string.IsNullOrEmpty(newData.Phone))
            {
                customer.Phone = newData.Phone;
            }
            if(!string.IsNullOrEmpty(newData.Phone2))
            {
                customer.Phone2 = newData.Phone2;
            }
            if(!string.IsNullOrEmpty(newData.Name))
            {
                customer.Name = newData.Name;
            }
            if(!string.IsNullOrEmpty(newData.Surname))
            {
                customer.Surname = newData.Surname;
            }
            if(!string.IsNullOrEmpty(newData.Email))
            {
                customer.Email = newData.Email;
            }
            var socialCard = newData.SocialCard;
            if (socialCard!=null)
            {
                if (!string.IsNullOrEmpty(socialCard.Number))
                {
                    customer.SocialCardNumber = socialCard.Number;
                }
            }            
            return customer;
        }

        #endregion
    }
}