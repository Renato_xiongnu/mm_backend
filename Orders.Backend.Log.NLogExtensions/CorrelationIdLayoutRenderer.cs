﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NLog;
using NLog.LayoutRenderers;
using NLog.Config;

namespace Orders.Backend.Log.NLogExtensions
{
    [LayoutRenderer("correlationid")]
    public class CorrelationIdLayoutRenderer : LayoutRenderer
    {
        const string orderIdMatch = @"\d{3}\-\d{3}\-\d{3}";
        const string orderIdMatch2 = @"\d{3}\-\d{8}";

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var match = Regex.Match(logEvent.FormattedMessage, orderIdMatch, RegexOptions.Compiled);
            if (match.Success)
            {
                builder.Append(match.Value);
            }
            match = Regex.Match(logEvent.FormattedMessage, orderIdMatch2, RegexOptions.Compiled);
            if (match.Success)
            {
                builder.Append(match.Value);
            }
        }
    }
}
