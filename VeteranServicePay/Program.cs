﻿using System;
using System.IO;
using VeteranServicePay.Proxy.Veterans;

namespace VeteranServicePay
{
    class Program
    {
        static void Main(string[] args)
        {
            var orderIds = File.ReadAllLines("Orders.txt");

            var client = new VeteransServiceClient();
            foreach (var orderId in orderIds)
            {
                try
                {
                    Console.WriteLine("Start paid {0}", orderId);
                    var result=client.ChangeState(new ChangeStateRequest
                    {
                        OrderId = orderId,
                        OrderState = OrderState.Paid
                    });

                    Console.WriteLine("Finish paid {0}, result {1} {2}", orderId, result.Status, result.ErrorMessage);
                }
                catch (Exception e)
                {
                    Console.WriteLine("OrderId: {0}, exception {1}", orderId, e);
                }
            }
       }
    }
}
