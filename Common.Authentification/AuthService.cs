﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OnlineOrders.Common;

namespace Common.Authentification
{
    public class AuthService
    {
        public static string GenerateTaskAuthUrl(string taskUrlFormat, string taskId, string userName)
        {
            var key = Encoder.Encode(taskId.ToString(CultureInfo.InvariantCulture));
            var encodeManager = Encoder.Encode(userName);

            var url = string.Format(taskUrlFormat, taskId, key, encodeManager, userName);

            return url;
        }
    }
}
