﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Authentification
{
    public class AuthentificationResult
    {
        public AutentificationResultCode Result { get; set; }
    }

    public enum AutentificationResultCode
    {
        Ok,
        Error
    }
    
}
