﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Backend.Clients.Web.Common.OnlineOrder;

namespace Orders.Backend.Clients.Web.Common
{
    public class ManagerArticle
    {
        public int Number;
        public long LineId;
        public string Article;
        public string Title;
        public decimal Price;
        public int Qty;
        public string Comment;
        public string CommentLog;
        public ReviewItemState RowState;

        public decimal RowCorrectPrice;
        public int RowCorrectQty;
        public string RowAltArticle;
        public decimal RowAltPrice;
        public int RowAltQty;
        public string RowAltDisplayItemCondition;
        public bool RowDisplayItem;

        public ItemState ConditionState;
        public string ConditionStateComment;

        public int WWSDepartmentNo;
        public string WWSProductGroup;
        public int WWSProductGroupNo;
        public int WWSStockNo;
        public int WWSFreeQty;
        public int WWSReservedQty;
        public decimal WWSPriceOrig;

        public string TransferSapCode { get; set; }
        public int TransferNumber { get; set; }

        public bool IsService { get; set; }
    }

    public class ReserveLineDto
    {
        public int Number { get; set; }

        public long LineId { get; set; }
        public string ArticleNum { get; set; }
        public string Title { get; set; }
		public string BrandTitle { get; set; }
		public decimal Price { get; set; }
        public int Qty { get; set; }
        public string Comment { get; set; }
        public string CommentLog { get; set; }

        public ReviewItemState ReviewItemState { get; set; }

        public decimal StockPrice { get; set; }
        public int StockQty { get; set; }

        public int WWSDepartmentNumber { get; set; }
        public int WWSFreeQty { get; set; }
        public decimal WWSPriceOrig { get; set; }
        public int WWSProductGroupNo { get; set; }
        public string WWSProductGroupName { get; set; }
        public int WWSReserverQuantity { get; set; }
        public int WWSStockNumber { get; set; }
        public string WWSInfo { get; set; }

        public ItemState ArticleConditionState { get; set; }
        public string ArticleCondition;

        public bool ValidArticleNum { get; set; }
        public bool ValidPrice { get; set; }
        public bool ValidQty { get; set; }

        public string TransferSapCode { get; set; }
        public int TransferNumber { get; set; }

        public ICollection<string> Promotions { get; set; }

        public bool IsService { get; set; }
    }

    public class OurcomeDto
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public bool IsSuccess { get; set; }
        public bool SkipTaskRequeriedFields { get; set; }
        public int FieldsCount { get; set; }

        public List<RequiredFieldDto> Fields { get; set; }
    }

    public class RequiredFieldDto
    {
        public string DefaultValue { get; set; }
        public string Name { get; set; }
        public string TypeName { get; set; }
        public RequiredFieldType Type { get; set; }
        public string Value { get; set; }
        public List<string> PredefinedValues { get; set; }
        public bool TaskField { get; set; }
    }

    public enum RequiredFieldType
    {
        DateTime,
        TimeSpan,
        Boolean,
        String,
        Int32,
        Decimal,
        Double,
        Choice
    }
}
