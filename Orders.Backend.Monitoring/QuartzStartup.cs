﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Quartz;
using Quartz.Impl;

namespace Monitoring.Service
{
    public class QuartzStartup
    {
        private static ISchedulerFactory _schedulerFactory=new StdSchedulerFactory();
        private static IScheduler _scheduler;
        private static ILog _logger = LogManager.GetLogger(typeof (QuartzStartup).Name);

        public static void Start()
        {
            try
            {
                _logger.Info("Service starting...");
                _scheduler = _schedulerFactory.GetScheduler();
                _scheduler.Start();
                _logger.Info("Service started succesfully");
            }
            catch (Exception e)
            {
                _logger.ErrorFormat("Exception during Quartz startup: {0}", e);
            }
        }
        
        public static void Stop()
        {
            try
            {
                if (_scheduler != null)
                {
                    _scheduler.Shutdown(false);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception during Quartz stoping: {0}", e);
            }
        }
    }
}
