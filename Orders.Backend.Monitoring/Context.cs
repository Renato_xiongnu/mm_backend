﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Monitoring.Service
{
    public static class Context
    {
        static Context()
        {
            Container = new UnityContainer().LoadConfiguration();
            log4net.Config.XmlConfigurator.Configure();
        }
        
        public static IUnityContainer Container { get; private set; }
    }
}
