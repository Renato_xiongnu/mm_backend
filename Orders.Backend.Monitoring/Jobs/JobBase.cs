﻿using Monitoring.Common;
using Quartz;
using Microsoft.Practices.Unity;

namespace Monitoring.Service.Jobs
{
    public class JobBase:IStatefulJob
    {
        private IMonitoringJob _monitoringService;

        public JobBase(string serviceName)
        {
            _monitoringService = Context.Container.Resolve<IMonitoringJob>(serviceName);
        }

        public void Execute(JobExecutionContext context)
        {
            _monitoringService.Execute();
        }
    }
}
