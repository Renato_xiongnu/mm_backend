﻿using System;
using System.ServiceProcess;
using Monitoring.Service.Jobs;
using Quartz;
using Quartz.Impl;

namespace Monitoring.Service
{
    public partial class TTMoitoringService
    {
        static void Main(string[] args)
        {
            var ttMonitoringService = new TTMoitoringService();

            if (Environment.UserInteractive)
            {
                QuartzStartup.Start();
            }
            else
            {
                Run(ttMonitoringService);
            }
        }
        
        protected override void OnStart(string[] args)
        {
            QuartzStartup.Start();
        }

        protected override void OnStop()
        {
            QuartzStartup.Stop();
        }
    }
}
