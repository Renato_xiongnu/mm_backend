USE TicketToolDb_LOAD


DECLARE @UsersCreated int
SET @UsersCreated=0
WHILE @UsersCreated<70
BEGIN

BEGIN TRAN

INSERT INTO Users
(Name,Email,Password,LastPingDateTime,IsLocked,CreateDate,LastLoginDate)
VALUES ('AgentCc'+CAST(@UsersCreated as nvarchar(10)),'agent@test.ru','NMAIcGbfX9RMZ4r0vtaab444LDs=',GETDATE(),0,GETDATE(),GETDATE())

DECLARE @UserId int

SET @UserId=Scope_Identity()
PRINT @UserId

INSERT INTO UserAvailableSkillSets (UserId,SkillSetId)
SELECT @UserId,SkillSetId FROM SkillSets

INSERT INTO RoleUsers (User_UserId,Role_RoleId)
SELECT @UserId,RoleId FROM Roles

INSERT INTO ClusterUsers(User_UserId,Cluster_Id)
SELECT @UserId,Id FROM Clusters

SET @UsersCreated=@UsersCreated+1

COMMIT
END