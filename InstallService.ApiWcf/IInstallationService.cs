﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using InstallService.ApiWcf.Contracts;
using InstallService.Model;

namespace InstallService.ApiWcf
{
    [ServiceContract]
    public interface IInstallationService
    {
        [OperationContract]
        Task CreateInstallation(String orderId);

        [OperationContract]
        Task CancelInstallation(String orderId);

        [OperationContract]
        Task<Installation[]> GetInstallation(String orderId);
    }  
}
