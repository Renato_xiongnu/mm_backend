﻿using System;

namespace InstallService.ApiWcf.Contracts
{
    public class Installation
    {
        public DateTime? ApprovedDate { get; set; }

        public DateTime? FactDate { get; set; }

        public string Status { get; set; }

        public string ArticleId { get; set; }

        public string ReferToArticle { get; set; }

        public string StatusReason { get; set; }

        public string ArticleType { get; set; }
    }
}