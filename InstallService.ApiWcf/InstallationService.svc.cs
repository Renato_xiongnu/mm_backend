﻿using System;
using System.Linq;
using System.Threading.Tasks;
using InstallService.ApiWcf.Contracts;
using InstallService.Dal;
using InstallService.Model;

namespace InstallService.ApiWcf
{
    public class InstallationService : IInstallationService
    {
        private readonly Service _service;
        
        public InstallationService()
        {
            _service = new Service(new InstallRepository());
        }

        public async Task CreateInstallation(String orderId)
        {
            await _service.CreateInstallationAsync(orderId);
        }

        public async Task CancelInstallation(String orderId)
        {
            await _service.CancelInstallationAsync(orderId);
        }

        public async Task<Installation[]> GetInstallation(string orderId)
        {
            var installations = await _service.SearchInstallationAsync(new SearchQuery {OrderId = orderId});
            if (installations == null) return null;
            return installations.Items.Where(t => t.ArticleType == "InstallationService").Select(el =>
                new Installation
                {
                    ApprovedDate = el.ApprovedDate,
                    ArticleId = el.ArticleId,
                    ArticleType = el.ArticleType,
                    FactDate = el.FactDate,
                    ReferToArticle = el.ReferToArticle,
                    Status = Service.GetStatusLocalizableName(el.Status),
                    StatusReason = el.StatusReason
                }).ToArray();
        }
    }
}
