﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Orders.Backend.Router.Dal.Model;

namespace Orders.Backend.Clients.LightProvider.Controllers
{
    public class OrdersController : ApiController
    {
        public JsonResult<OrderMonitoring> GetByOrderId(string orderId)
        {
            using (var ctx = new RoutableOrderDbEntities())
            {
                return Json(ctx.OrderMonitorings.FirstOrDefault(m => m.OrderId == orderId));
            }
        }
    }
}
