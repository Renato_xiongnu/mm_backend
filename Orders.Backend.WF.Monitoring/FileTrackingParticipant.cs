﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.WF.Monitoring
{
    public sealed class FileTrackingParticipant : TrackingParticipant
    {
        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            var activityStateRecord = record as ActivityStateRecord;
            if (activityStateRecord != null)
            {
            }
        }
    }
}
