using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Router.Infastructure
{
    public class PaymentTypeProcessSelector : ProcessSelectorBase
    {
        internal PaymentTypeProcessSelector(ProcessSelectorBase next) : base(next)
        {
        }

        protected override string SelectProcessName(ProcessSelectRequest processSelectRequest)
        {
            var paymentType = processSelectRequest.PaymentType;
            switch(paymentType)
            {
                case PaymentType.OnlineCredit:
                {
                    return "IOrderProcessingService";
                }
                case PaymentType.Online:
                {
                    return string.Empty;
                }
                case PaymentType.SocialCard:
                {
                    return "ISocialCardOrderProcessingService";
                }
                default:
                {
                    return string.Empty;
                }
            }
        }
    }
}