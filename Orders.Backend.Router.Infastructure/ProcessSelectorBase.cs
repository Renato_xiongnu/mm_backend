using System.Collections.Generic;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Router.Infastructure
{
    public abstract class ProcessSelectorBase
    {
        private ProcessSelectorBase _next = null;

        protected ProcessSelectorBase() : this(null)
        {
        }

        protected ProcessSelectorBase(ProcessSelectorBase next)
        {
            _next = next;
        }

        public static ProcessSelectorBase Create(IList<string> smartStartSapCodes = null, IList<string> smartStart2SapCodes = null, IList<string> discontSapCodes=null)
        {
            return
                new OrderSourceProcessSelector(
                    new PaymentTypeProcessSelector(
                        new SmartCashProcessSelector(smartStartSapCodes,
                            new DiscontProcessSelector(discontSapCodes,
                                new Smart2CashProcessSelector(smartStart2SapCodes, null)))));
        }

        public string GetProcessName(ProcessSelectRequest processSelectRequest)
        {
            var processVersion = SelectProcessName(processSelectRequest);
            if (string.IsNullOrEmpty(processVersion))
            {
                if (_next == null)
                {
                    throw new ProcessNotFoundException();
                }
                return _next.GetProcessName(processSelectRequest);
            }

            return processVersion;
        }

        protected abstract string SelectProcessName(ProcessSelectRequest processSelectRequest);
    }

    public class ProcessSelectRequest
    {
        public OrderSource OrderSource { get; set; }

        public PaymentType PaymentType { get; set; }

        public string SapCode { get; set; }

        public ICollection<ArticleData> ArticleDatas { get; set; }

        public bool HasDelivery { get; set; }

        public ProcessSelectRequest(OrderSource orderSource, PaymentType paymentType, string sapCode, ICollection<ArticleData> articleDatas, bool hasDelivery)
        {
            OrderSource = orderSource;
            PaymentType = paymentType;
            SapCode = sapCode;
            ArticleDatas = articleDatas;
            HasDelivery = hasDelivery;
        }
    }
}