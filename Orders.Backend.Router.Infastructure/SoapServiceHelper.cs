﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Text;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace Orders.Backend.Router.Infastructure
{
    public static class SoapServiceHelper
    {
        [DataContract(Namespace = "http://tempuri.org/", Name = "StartProcess")]
        public class StartRequest
        {
            [DataMember(Order = 1)]
            public string WorkItemId { get; set; }

            [DataMember(Order = 2)]
            public string ParameterName { get; set; }
        }

        [DataContract(Namespace = "http://tempuri.org/", Name = "Cancel")]
        public class CancelRequest
        {
            [DataMember(Order = 1)]
            public string WorkItemId { get; set; }

            [DataMember(Order = 2)]
            // ReSharper disable once InconsistentNaming
            public string reasonText { get; set; }

            [DataMember(Order = 3)]
            // ReSharper disable once InconsistentNaming
            public string cancelBy { get; set; }
        }

        [DataContract(Namespace = "http://tempuri.org/", Name = "ApproveOrder_External")]
         // ReSharper disable once InconsistentNaming
        public class ApproveOrder_External
        {
            [DataMember(Order = 1)]
            public string ApprovedBy { get; set; }

            [DataMember(Order = 2)]
            public string WorkItemId { get; set; }
        }

        [DataContract(Namespace = "http://tempuri.org/", Name = "FixOrder_External")]
        // ReSharper disable once InconsistentNaming
        public class FixOrder_External
        {
            [DataMember(Order = 1)]
            public string FixedBy { get; set; }

            [DataMember(Order = 2)]
            public string WorkItemId { get; set; }
        }

        [DataContract(Namespace = "http://tempuri.org/", Name = "UpdateOrderExpiration_External")]
        // ReSharper disable once InconsistentNaming
        public class UpdateOrderExpiration_External
        {
            [DataMember(Order = 1)]
            public string UpdatedBy { get; set; }

            [DataMember(Order = 2)]
            public string WorkItemId { get; set; }
        }

        [DataContract(Namespace = "http://tempuri.org/", Name = "StartProcessResponse")]
        public class StartProcessResponse
        {
            [DataMember(Order = 1)]
            public string TicketId { get; set; }

        }

        public static string GetServiceAddress(string processName)
        {
            var clientSection = (ClientSection)WebConfigurationManager.GetSection("system.serviceModel/client");
            var address = string.Empty;
            foreach (ChannelEndpointElement endpoint in clientSection.Endpoints)
            {
                if (endpoint.Name == processName)
                {
                    address = endpoint.Address.AbsoluteUri;
                }
            }
            return address;
        }

        public static StartProcessResponse StartProcess(StartRequest request, string address)
        {
            var s1 = RequestToString(request);
            return SendStart<StartProcessResponse>("StartProcess", s1, address);
        }

        public static bool Cancel(CancelRequest request, string address)
        {
            var s1 = RequestToString(request);
            return SendCancel("Cancel", s1, address);
        }
        
        public static bool ApproveOrder(ApproveOrder_External request, string address)
        {
            var s1 = RequestToString(request);
            return SendApprove(s1, address);
        }

        public static bool FixOrder(FixOrder_External request, string address)
        {
            var s1 = RequestToString(request);
            return SendFix(s1, address);
        }

        public static bool UpdateExpirationDateOrder(UpdateOrderExpiration_External request, string address)
        {
            var s1 = RequestToString(request);
            return SendUpdateExpiration(s1, address);
        }

        private static string RequestToString(object request)
        {
            var s = new DataContractSerializer(request.GetType());
            var stream1 = new MemoryStream();
            s.WriteObject(stream1, request);
            stream1.Position = 0;
            var s1 = Encoding.UTF8.GetString(stream1.ToArray());
            return s1;
        }

        private static T SendStart<T>(string operationName, string messageFormat, string strSvcpath)
        {
            var result = default(T);
            IRequestChannel channel;
            Message replymsg;
            string ns;
            var factory = Send(strSvcpath + "?wsdl", operationName, messageFormat, strSvcpath, out channel, out replymsg,
                out ns);

            var body = replymsg.ToString();
            var document = XDocument.Load(XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(body))));
            var faultcode = (from fc in document.Descendants("faultcode")
                             select fc.Value).FirstOrDefault();
            var faultstring = (from fc in document.Descendants("faultstring")
                               select fc.Value).FirstOrDefault();
            if (faultcode == null && faultstring == null)
            {
                result = replymsg.GetBody<T>();
            }
            replymsg.Close();
            channel.Close();
            factory.Close();
            return result;
        }

        private static bool SendCancel(string operationName, string messageFormat, string strSvcpath)
        {
            var result = false;
            IRequestChannel channel;
            Message replymsg;
            string ns;
            var factory = Send(strSvcpath + "?wsdl", operationName, messageFormat, strSvcpath, out channel, out replymsg,
                out ns);

            var body = replymsg.ToString();
            var document = XDocument.Load(XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(body))));
            var faultcode = (from fc in document.Descendants("faultcode")
                             select fc.Value).FirstOrDefault();
            var faultstring = (from fc in document.Descendants("faultstring")
                               select fc.Value).FirstOrDefault();
            if (faultcode == null && faultstring == null)
            {
                XNamespace ns2 = ns;
                var x = document.Descendants(ns2 + "CancelResponse").FirstOrDefault();
                if (x != null)
                {
                    result = x.Value.ToLowerInvariant().Trim() == "true";
                }
            }
            replymsg.Close();
            channel.Close();
            factory.Close();
            return result;
        }

        private static bool SendApprove(string messageFormat, string strSvcpath)
        {
            IRequestChannel channel;
            Message replymsg;
            string ns;
            var factory = Send(strSvcpath + "?wsdl", "ApproveOrder_External", messageFormat, strSvcpath, out channel,
                out replymsg,
                out ns);

            var body = replymsg.ToString();
            var document = XDocument.Load(XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(body))));
            var faultcode = (from fc in document.Descendants("faultcode")
                select fc.Value).FirstOrDefault();
            var faultstring = (from fc in document.Descendants("faultstring")
                select fc.Value).FirstOrDefault();
            var result = string.IsNullOrEmpty(faultcode) && string.IsNullOrEmpty(faultstring);
            if (!string.IsNullOrEmpty(faultcode)) throw new Exception(faultcode);
            if (!string.IsNullOrEmpty(faultstring)) throw new Exception(faultstring);
            replymsg.Close();
            channel.Close();
            factory.Close();
            return result;
        }

        private static bool SendFix(string messageFormat, string strSvcpath)
        {
            IRequestChannel channel;
            Message replymsg;
            string ns;
            var factory = Send(strSvcpath + "?wsdl", "FixOrder_External", messageFormat, strSvcpath, out channel,
                out replymsg,
                out ns);

            var body = replymsg.ToString();
            var document = XDocument.Load(XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(body))));
            var faultcode = (from fc in document.Descendants("faultcode")
                             select fc.Value).FirstOrDefault();
            var faultstring = (from fc in document.Descendants("faultstring")
                               select fc.Value).FirstOrDefault();
            var result = string.IsNullOrEmpty(faultcode) && string.IsNullOrEmpty(faultstring);
            if (!string.IsNullOrEmpty(faultcode)) throw new Exception(faultcode);
            if (!string.IsNullOrEmpty(faultstring)) throw new Exception(faultstring);
            replymsg.Close();
            channel.Close();
            factory.Close();
            return result;
        }

        private static bool SendUpdateExpiration(string messageFormat, string strSvcpath)
        {
            IRequestChannel channel;
            Message replymsg;
            string ns;
            var factory = Send(strSvcpath + "?wsdl", "UpdateOrderExpiration_External", messageFormat, strSvcpath, out channel,
                out replymsg,
                out ns);

            var body = replymsg.ToString();
            var document = XDocument.Load(XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(body))));
            var faultcode = (from fc in document.Descendants("faultcode")
                             select fc.Value).FirstOrDefault();
            var faultstring = (from fc in document.Descendants("faultstring")
                               select fc.Value).FirstOrDefault();
            var result = string.IsNullOrEmpty(faultcode) && string.IsNullOrEmpty(faultstring);
            if (!string.IsNullOrEmpty(faultcode)) throw new Exception(faultcode);
            if (!string.IsNullOrEmpty(faultstring)) throw new Exception(faultstring);
            replymsg.Close();
            channel.Close();
            factory.Close();
            return result;
        }

        private static IChannelFactory<IRequestChannel> Send(string strUrl, string operationName, string messageFormat,
            string strSvcpath, out IRequestChannel channel, out Message replymsg, out string ns)
        {
            var strSysUrl = new Uri(strUrl);

            var servicebinding = new BasicHttpBinding { MaxReceivedMessageSize = 262144 };
            var mdClient = new MetadataExchangeClient(servicebinding)
            {
                MaximumResolvedReferences = 50,
                ResolveMetadataReferences = true
            };

            var serviceMetaDataSet = mdClient.GetMetadata(strSysUrl, MetadataExchangeClientMode.HttpGet);
            var strBdr = new StringBuilder();
            var serviceXmlWriter = XmlWriter.Create(strBdr);
            serviceMetaDataSet.WriteTo(serviceXmlWriter);

            var str = strBdr.ToString();

            var wsdl = new XmlDocument();

            wsdl.LoadXml(str);

            serviceXmlWriter.Close();

            var importer = new WsdlImporter(serviceMetaDataSet);

            var contracts = importer.ImportAllContracts();
            //var allEndpoints = importer.ImportAllEndpoints();
            var contract = contracts[0];
            ns = contract.Namespace;

            var serviceName = ns + contract.Name;
            var operation = contract.Operations.FirstOrDefault(x => x.Name == operationName);
            operationName = operation.Name;

            var binding = new BasicHttpBinding();
            var requestMessage = string.Format(messageFormat); //use ns for replace
            var reader = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(requestMessage)));
            var factory = binding.BuildChannelFactory<IRequestChannel>();
            factory.Open();
            channel = factory.CreateChannel(new EndpointAddress(strSvcpath));
            channel.Open();
            var reqmsg = Message.CreateMessage(binding.MessageVersion, serviceName + "/" + operationName, reader);
            replymsg = channel.Request(reqmsg);
            return factory;
        }

        public static string GetProcessCurrentVersion(string processName)
        {
            var currentVersion = ConfigurationManager.AppSettings["CurrentVersion" + processName];
            return string.IsNullOrEmpty(currentVersion) ? string.Empty : currentVersion;
        }

        public static IReadOnlyCollection<string> GetSmartStartSapCodecollection()
        {
            var sapCodes = ConfigurationManager.AppSettings["SmartStartSapCodecollection"];
            return string.IsNullOrEmpty(sapCodes) ? new string[0] : sapCodes.Split(';');
        }

        public static IReadOnlyCollection<string> GetSmartStart2SapCodecollection()
        {
            var sapCodes = ConfigurationManager.AppSettings["SmartStart2SapCodecollection"];
            return string.IsNullOrEmpty(sapCodes) ? new string[0] : sapCodes.Split(';');
        }

        public static IReadOnlyCollection<string> GetDiscontSapCodeCollection()
        {
            var sapCodes = ConfigurationManager.AppSettings["DiscontSapCodeCollection"];
            return string.IsNullOrEmpty(sapCodes) ? new string[0] : sapCodes.Split(';');
        }
    }
}
