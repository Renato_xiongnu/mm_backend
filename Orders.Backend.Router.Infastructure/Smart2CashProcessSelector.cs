﻿using System.Collections.Generic;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Router.Infastructure
{
    public class Smart2CashProcessSelector : ProcessSelectorBase
    {
        private readonly IList<string> _sapCodes;        
        internal Smart2CashProcessSelector(IList<string> sapCodes, ProcessSelectorBase next)
            : base(next)
        {
            _sapCodes = sapCodes;
        }

        protected override string SelectProcessName(ProcessSelectRequest processSelectRequest)
        {
            if ((processSelectRequest.PaymentType == PaymentType.Cash || processSelectRequest.PaymentType==PaymentType.Online)
                && _sapCodes != null
                && _sapCodes.Count > 0
                && _sapCodes.Contains(processSelectRequest.SapCode))
            {
                return "ISmartStart2CashOrderProcessingService";
            }

            return "IOrderProcessingService";
        }       
    }
}