﻿using System.Collections.Generic;
using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Router.Infastructure
{
    class DiscontProcessSelector : ProcessSelectorBase
    {
        private readonly IList<string> _sapCodes;

        internal DiscontProcessSelector(IList<string> sapCodes, ProcessSelectorBase next)
            : base(next)
        {
            _sapCodes = sapCodes;
        }

        protected override string SelectProcessName(ProcessSelectRequest processSelectRequest)
        {
            if (processSelectRequest.PaymentType == PaymentType.Cash
                && _sapCodes != null
                && _sapCodes.Count > 0
                && _sapCodes.Contains(processSelectRequest.SapCode))
            {
                return "IDiscontProcessingService";
            }

            return string.Empty;
        }
    }
}
