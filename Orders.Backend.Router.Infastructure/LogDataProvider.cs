﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orders.Backend.Router.Dal.Model;

namespace Orders.Backend.Router.Infastructure
{
    public class LogDataProvider
    {
        private const string MistakeCallFormat = "Call by mistake {0}";

        private RoutableOrder GetRoutableOrder(RoutableOrderDbEntities context, string orderId)
        {
            return context.RoutableOrders.Include("Histories").SingleOrDefault(t => t.OrderId == orderId);
        }


        private RoutableOrder NewOrder(RoutableOrderDbEntities context, string orderId, string sapCode, string orderSource, DateTime updateDate)
        {
            var order = new RoutableOrder
            {
                OrderId = orderId,
                SapCode = sapCode,
                OrderSource = orderSource,
                UpdateDate = updateDate,
                CreateDate = updateDate,
                Histories = new List<History>(),
            };

            context.RoutableOrders.Add(order);

            return order;
        }

        public bool CheckIfOrderExists(string orderId, out bool isProcessOrder)
        {
            var isOrderExist = false;
            isProcessOrder = false;

            using (var context = new RoutableOrderDbEntities())
            {
                var order = GetRoutableOrder(context, orderId);

                if (order != null && order.IsCallSuccessful)
                {
                    isOrderExist = true;
                    isProcessOrder = order.IsProcessOrder;
                }
            }

            return isOrderExist;
        }


        public bool CheckIfOrderCalledRepeatedly(string orderId, out bool isProcessOrder)
        {
            var isOrderExist = false;
            isProcessOrder = false;

            using (var context = new RoutableOrderDbEntities())
            {
                var currentDate = DateTime.Now.ToUniversalTime();

                var order = GetRoutableOrder(context, orderId);

                if (order != null && order.IsCallSuccessful)
                {
                    order.UpdateDate = currentDate;
                    var missQty = order.Histories.Count(t => t.RecType == (int)RecTypeEnum.MistakeCall);
                    order.Histories.Add(new History()
                    {
                        Text = string.Format(MistakeCallFormat, missQty + 1),
                        UpdateDate = currentDate
                    });

                    isOrderExist = true;

                    isProcessOrder = order.IsProcessOrder;
                }

                context.SaveChanges();
            }

            return isOrderExist;
        }


        public string GetOrderProcessVersion(string orderId)
        {
            using (var context = new RoutableOrderDbEntities())
            {
                var orderProcessVersion = context.RoutableOrders.Where(t => t.OrderId == orderId).Select(x => x.ProcessVersion).SingleOrDefault();
                return string.IsNullOrEmpty(orderProcessVersion) ? string.Empty : orderProcessVersion;
            }
        }

        public void AddOrder(
            string orderId,
            string sapCode,
            string orderSource,
            bool shouldBeProcessed,
            bool isProcessOrder,
            string text,
            string seralizedOrder,
            string currentProcessVersion)
        {
            using(var context = new RoutableOrderDbEntities())
            {
                var currentDate = DateTime.Now.ToUniversalTime();

                var order = GetRoutableOrder(context, orderId);
                if(order == null)
                {
                    order = NewOrder(context, orderId, sapCode, orderSource, currentDate);
                    order.ShouldBeProcessed = shouldBeProcessed;
                }
                order.IsProcessOrder = isProcessOrder;
                order.SerializedOrder = seralizedOrder;
                order.ProcessVersion = currentProcessVersion;
                order.IsCallSuccessful = true;
                order.Histories.Add(new History()
                {
                    RecType = (int) RecTypeEnum.Ok,
                    UpdateDate = currentDate,
                    Text = text
                });

                context.SaveChanges();
            }
        }

        public RoutableOrder[] GetShouldBeProcessedOrders()
        {
            using(var context = new RoutableOrderDbEntities())
            {

                return context.RoutableOrders
                    .Include("Histories")
                    .Where(ro => ro.ShouldBeProcessed)
                    .Where(ro=>!ro.IsProcessOrder)
                    .Where(ro=>ro.IsCallSuccessful)
                    .OrderBy(ro => ro.UpdateDate)
                    .ToArray();
            }
        }

        public void MarkIsProcessed(string orderId)
        {
            using (var context = new RoutableOrderDbEntities())
            {
                var currentDate = DateTime.Now.ToUniversalTime();
                var order = GetRoutableOrder(context, orderId);
                if (order == null)
                {
                    throw new ApplicationException(string.Format("Order {0} not found", orderId));
                }
                order.IsProcessOrder = true;
                context.SaveChanges();
            }
        }

        public void LogError(string orderId, string orderSource, string errorText)
        {
            LogError(orderId, "", true, orderSource, errorText);
        }

        public void LogInfo(string orderId, string text)
        {
            using (var context = new RoutableOrderDbEntities())
            {
                var currentDate = DateTime.Now.ToUniversalTime();
                var order = GetRoutableOrder(context, orderId);
                if (order != null)
                {
                    order.Histories.Add(new History()
                    {
                        RecType = (int)RecTypeEnum.Ok,
                        UpdateDate = currentDate,
                        Text = text
                    });
                    context.SaveChanges();
                }
            }
        }

        public void LogError(string orderId, string sapCode, bool shouldBeProcessed,
            string orderSource, string errorText)
        {
            using (var context = new RoutableOrderDbEntities())
            {
                var currentDate = DateTime.Now.ToUniversalTime();
                var order = GetRoutableOrder(context, orderId);

                if (order == null)
                {
                    order = NewOrder(context, orderId, sapCode, orderSource, currentDate);
                    order.ShouldBeProcessed = shouldBeProcessed;
                    order.IsCallSuccessful = false;
                }

                order.Histories.Add(new History()
                {
                    RecType = (int)RecTypeEnum.Exception,
                    UpdateDate = currentDate,
                    Text = errorText
                });

                context.SaveChanges();
            }
        }

        public void SetProcessVersion(string orderId, string currentProcessVersion)
        {
            using (var context = new RoutableOrderDbEntities())
            {
                var order = GetRoutableOrder(context, orderId);
                if (order == null)
                {
                    throw new ArgumentException("orderId");
                }
                order.ProcessVersion = currentProcessVersion;

                context.SaveChanges();
            }
        }

        public RoutableOrder GetOrder(string orderId)
        {
            using (var context = new RoutableOrderDbEntities())
            {
                return context.RoutableOrders.Single(o => o.OrderId == orderId);
            }
        }
    }
}
