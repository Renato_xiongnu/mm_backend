﻿using System.Collections.Generic;
using System.Linq;
using Orders.Backend.Router.Infastructure.ProductBackendClient;
using Orders.Backend.Services.Contracts.OrderService;


namespace Orders.Backend.Router.Infastructure
{
    public class SmartCashProcessSelector:ProcessSelectorBase
    {
        private readonly IList<string> _sapCodes;

        private const string MediaMarktChannel = "MM";

        internal SmartCashProcessSelector(IList<string> sapCodes, ProcessSelectorBase next) : base(next)
        {
            _sapCodes = sapCodes;
        }

        protected override string SelectProcessName(ProcessSelectRequest processSelectRequest)
        {
            if (processSelectRequest.PaymentType == PaymentType.Cash
                && _sapCodes != null
                && _sapCodes.Count > 0
                && _sapCodes.Contains(processSelectRequest.SapCode))
            {
                return "ISmartStartCashOrderProcessingService";
            }

            return string.Empty;
        }
    }
}
