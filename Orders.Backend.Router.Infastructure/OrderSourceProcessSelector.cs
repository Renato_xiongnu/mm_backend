﻿using Orders.Backend.Services.Contracts.OrderService;

namespace Orders.Backend.Router.Infastructure
{
    public class OrderSourceProcessSelector:ProcessSelectorBase
    {
        internal OrderSourceProcessSelector(ProcessSelectorBase next):base(next)
        {
        }

        protected override string SelectProcessName(ProcessSelectRequest processSelectRequest)
        {
            switch (processSelectRequest.OrderSource)
            {
                //case OrderSource.Metro:
                //case OrderSource.MetroCallCenter:
                //{
                //    return "IMetroProcessingService";
                //}
                default:
                {
                    return string.Empty;
                }
            }
        }
    }
}
