﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Orders.Backend.StockLocationSelector.Dal.Entities;


namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class OrderInfo
    {
        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String OrderId { get; set; }

        [DataMember]
        public ICollection<OrderLine> OrderLines { get; set; }

        [DataMember]
        public Boolean HasDelivery { get; set; }

        [DataMember]
        public StockShippingMethod? ShippingMethod { get; set; }

        //[DataMember]
        //public DateTime? DeliveryDate { get; set; }

        //[DataMember]
        //public String DeliveryAddress { get; set; }
   
    }
}