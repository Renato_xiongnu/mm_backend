﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class GetSaleLocationOutcomesResult : OperationResult<IList<SaleLocationOutcome>>
    {
    }
}