using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class GetSaleLocationResult : OperationResult
    {
        [DataMember]
        public String SapCode { get; set; }
    }
}