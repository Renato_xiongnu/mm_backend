using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class OrderLine
    {
        [DataMember]
        public Int64 Article { get; set; }

        [DataMember]
        public Int32 Quantity { get; set; }
    }
}