﻿using System;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class SaleLocationOutcome
    {
        [DataMember]
        public String OrderId { get; set; }

        [DataMember]
        public String SapCode { get; set; }

        [DataMember]
        public String Outcome { get; set; }

        [DataMember]
        public String Performer { get; set; }
        
        [DataMember]
        public DateTime Created { get; set; }
    }
}