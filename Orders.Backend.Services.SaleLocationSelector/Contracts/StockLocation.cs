﻿using System.Runtime.Serialization;
using Orders.Backend.StockLocationSelector.Dal.Entities;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class AvailableStockLocation
    {
        [DataMember]
        public string StockLocationSapCode { get; set; }

        [DataMember]
        public int Rate { get; set; }

        [DataMember]
        public string AvailableShippingMethods { get; set; }
    }
}