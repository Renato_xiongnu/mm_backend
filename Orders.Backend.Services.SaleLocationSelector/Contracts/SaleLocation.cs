﻿using System;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    public class SaleLocation
    {
        public String SapCode { get; set; }

        public Int32 Rate { get; set; }

        public Double Сapacity { get; set; }

        public Double CurrentDeliveryResource { get; set; }

        public Double UsedDeliveryResource { get; set; }
    }
}