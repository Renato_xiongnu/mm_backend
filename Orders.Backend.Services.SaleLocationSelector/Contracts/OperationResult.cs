﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Orders.Backend.Services.SaleLocationSelector.Contracts
{
    [DataContract]
    public class OperationResult
    {
        private IList<String> _errorMessage;

        [DataMember]
        public ReturnCode ReturnCode { get; set; }

        [DataMember]
        public IList<String> ErrorMessages
        {
            get { return _errorMessage ?? (_errorMessage = new List<String>()); }
            set { _errorMessage = value; }
        }

        [DataMember]
        public String ErrorMessage
        {
            get { return string.Join(Environment.NewLine, ErrorMessages); }
            set { }
        }

        public void AddError(String message, params object[] args)
        {
            if(message == null)
            {
                throw new ArgumentNullException("message");
            }
            ReturnCode = ReturnCode.Error;
            ErrorMessages.Add(String.Format(message, args));
        }
    }

    [DataContract]
    public class OperationResult<TResult> : OperationResult
    {
        [DataMember]
        public TResult Result { get; set; }
    }
}