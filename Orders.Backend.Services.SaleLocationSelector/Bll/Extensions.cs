﻿using Orders.Backend.Services.SaleLocationSelector.Contracts;
using Orders.Backend.StockLocationSelector.Dal.Entities;
using SaleLocationOutcome = Orders.Backend.Services.SaleLocationSelector.Contracts.SaleLocationOutcome;

namespace Orders.Backend.Services.SaleLocationSelector.Bll
{
    public static class Extensions
    {
        public static SaleLocationOutcome ToSaleLocationOutcome(this StockLocationOutcome outcome)
        {
            var item = new SaleLocationOutcome
            {
                Created = outcome.Created,
                OrderId = outcome.OrderId,
                Outcome = outcome.Outcome,
                Performer = outcome.Performer,
                SapCode = outcome.StockLocation.SapCode
            };
            return item;
        }

        public static StockLocationOutcome ToStockLocationOutcome(this SaleLocationOutcome outcome)
        {
            var item = new StockLocationOutcome
            {
                Created = outcome.Created,
                OrderId = outcome.OrderId,
                Outcome = outcome.Outcome,
                Performer = outcome.Performer,
            };
            return item;
        }

        public static StockShippingMethod GetShippingMethod(this OrderInfo orderInfo)
        {
            return orderInfo.ShippingMethod.HasValue && orderInfo.ShippingMethod.Value != 0
                ? orderInfo.ShippingMethod.Value
                : (orderInfo.HasDelivery ? StockShippingMethod.Delivery : StockShippingMethod.PickupShop);
        }
    }
}