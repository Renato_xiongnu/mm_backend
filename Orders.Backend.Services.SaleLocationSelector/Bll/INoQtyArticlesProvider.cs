﻿using System;

namespace Orders.Backend.Services.SaleLocationSelector.Bll
{
    public interface IInfiniteArticlesProvider
    {
        Int64[] Articles { get; }
    }
}