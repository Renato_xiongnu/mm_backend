﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Common.Logging;
using System.Data.Entity;
using MMS.Multiplexor.Client;
using Orders.Backend.Services.SaleLocationSelector.Contracts;
using Orders.Backend.Services.SaleLocationSelector.Properties;
using Orders.Backend.Services.SaleLocationSelector.Proxy.OrdersOnline;
using Orders.Backend.Services.SaleLocationSelector.Proxy.ProductBackend;
using Orders.Backend.Services.SaleLocationSelector.Proxy.ProductInfoService;
using Orders.Backend.StockLocationSelector.Dal;
using Orders.Backend.StockLocationSelector.Dal.Entities;
using OperationResult = Orders.Backend.Services.SaleLocationSelector.Contracts.OperationResult;
using OrderInfo = Orders.Backend.Services.SaleLocationSelector.Contracts.OrderInfo;
using ProductType = Orders.Backend.Services.SaleLocationSelector.Proxy.ProductInfoService.ProductType;
using SaleLocation = Orders.Backend.Services.SaleLocationSelector.Contracts.SaleLocation;
using SaleLocationOutcome = Orders.Backend.Services.SaleLocationSelector.Contracts.SaleLocationOutcome;
namespace Orders.Backend.Services.SaleLocationSelector.Bll
{
    public class StockLocationSelectorService : DbServiceBase
    {
        private readonly Double _defaultDistance;
        private readonly Double _measure;
        private readonly Double _maxtDistance;
        private readonly String _geoDataSaleLocationException;

        private readonly ILog _logger;
        private readonly IOrdersBackendService _searchService;
        private readonly IProductInfoService _productInfoService;
        private readonly IInfiniteArticlesProvider _infiniteArticles;
        private readonly IOrderService _onlineOrderService;
        private readonly String _mediaMarktChannel;
        private readonly String _muxUrl;
        private readonly String _apiKey;
        private readonly Int32 _timeOut;

        public StockLocationSelectorService(ILog logger, IOrdersBackendService searchService,
            IProductInfoService productInfoService, IInfiniteArticlesProvider infiniteArticles,
            IOrderService onlineOrderService, String channel = "MM")
        {
            _logger = logger;
            _mediaMarktChannel = channel;
            _productInfoService = productInfoService;
            _searchService = searchService;
            _infiniteArticles = infiniteArticles;
            _onlineOrderService = onlineOrderService;
            _apiKey = Settings.Default.apiKey; //Todo Case
            _muxUrl = Settings.Default.muxUrl;
            _timeOut = Settings.Default.muxTimeout;
            _defaultDistance = Settings.Default.defaultDistance;
            _measure = Settings.Default.measure;
            _maxtDistance = Settings.Default.maxtDistance;
            _geoDataSaleLocationException = Settings.Default.geoDataSaleLocationException;
        }

        public GetSaleLocationResult GetSaleLocation(OrderInfo orderInfo)
        {
            _logger.InfoFormat("GetSaleLocation City={0} OrderId={1} Shipping method={2}", orderInfo.City,
                orderInfo.OrderId, orderInfo.ShippingMethod);
            var getSaleLocationResult = new GetSaleLocationResult();

            try
            {
                var saleLocationSapCode = GetSaleLocationSapCodeByOrderId(orderInfo.OrderId);
                if (String.IsNullOrEmpty(saleLocationSapCode))
                    throw new Exception("Не удалось установить SapCode для SaleLocation");
                var shipmingMethod = orderInfo.GetShippingMethod();
                var strategy = GetStockLocationSelectorStrategy(saleLocationSapCode);

                var orderLines = FilterOrderLines(orderInfo);

                _logger.Info(String.Format("start order -{0}", orderInfo.OrderId));

                switch (strategy)
                {
                    case StockLocationSelectorStrategyType.Rating:
                        getSaleLocationResult.SapCode = GetStockLocationByRating(orderInfo.OrderId, saleLocationSapCode,
                            orderLines, shipmingMethod);
                        break;
                    case StockLocationSelectorStrategyType.Сapacity:
                        getSaleLocationResult.SapCode = GetStocksByGeoLocation(orderInfo.OrderId, orderLines,
                            saleLocationSapCode, shipmingMethod);
                        break;
                }

            }
            catch (Exception ex)
            {
                _logger.Error("GetSaleLocation", ex);
                getSaleLocationResult.AddError(ex.Message);
            }
            return getSaleLocationResult;
        }


        #region geodata

        /// <summary>
        /// Получение физических магазинов по геоданным
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="orderLines"></param>
        /// <param name="saleLocationSapCode"></param>
        /// <param name="shipmingMethod"></param>
        /// <returns></returns>
        private String GetStocksByGeoLocation(String orderId, List<OrderLine> orderLines, String saleLocationSapCode,
            StockShippingMethod shipmingMethod)
        {
            _logger.Info(String.Format("Заказ - {0};SaleLocation - {1}", orderId, saleLocationSapCode));

            //получаем данные по доставке
            var deliveryDate = DateTime.Now.Date;
            var deliveryAddress = String.Empty;
            var deliveryData = _onlineOrderService.GerOrderDataByOrderId(orderId);
            if (deliveryData != null && deliveryData.DeliveryInfo != null)
            {
                deliveryDate = deliveryData.DeliveryInfo.DeliveryDate.HasValue
                    ? deliveryData.DeliveryInfo.DeliveryDate.Value.Date
                    : deliveryDate.AddDays(3);
                deliveryAddress = deliveryData.DeliveryInfo.Address;
            }

            var availableSapCodes = GetAvaliableStocksWithGeoData(orderId, saleLocationSapCode, shipmingMethod,
                deliveryDate);

            if (availableSapCodes == null || !availableSapCodes.Any())
            {
                _logger.Info(String.Format("Заказ - {0};SaleLocation - {1}; Нет магазинов для исполнения", orderId,
                    saleLocationSapCode));
                return string.Empty;
            }

            _logger.Info(String.Format("Заказ - {0};SaleLocation - {1}; AvailibleSapCodes - {2}", orderId,
                saleLocationSapCode, string.Join(";", availableSapCodes.Select(x => x.SapCode))));

            var stockLocationSapCode = String.Empty;
            var existInStock = GetExistInStockScope(availableSapCodes, orderLines);

            if (existInStock == null || !existInStock.Any())
            {
                _logger.Info(String.Format("Заказ - {0};SaleLocation - {1}; Нет магазинов с товаром", orderId,
                    saleLocationSapCode));
                return string.Empty;
            }

            _logger.Info(String.Format("Заказ - {0};SaleLocation - {1}; ExistInStock - {2}", orderId,
                saleLocationSapCode, string.Join(";", existInStock.Keys)));

            var notAvailableStocksSapCodes =
                availableSapCodes.Select(x => x.SapCode).Where(a => !existInStock.ContainsKey(a)).ToList();

            _logger.Info(String.Format("Заказ - {0};SaleLocation - {1}; NotExistInStock - {2}", orderId,
                saleLocationSapCode, string.Join(";", notAvailableStocksSapCodes)));

            // Получаем данные для доставки с использованием гео сервиса
            var geoLocations = GetGeoLocations(existInStock.Select(x => x.Value).ToList(), deliveryAddress).ToList();

            var sb = new StringBuilder();
            sb.AppendLine(String.Format("Заказ - {0};SaleLocation - {1}; Геоданные:", orderId, saleLocationSapCode));
            foreach (var geoLocation in geoLocations)
            {
                sb.AppendLine(String.Format("from:{0}; distance:{1}; error:{2}", geoLocation.From,
                    geoLocation.DistanceMeters, geoLocation.Error));
            }
            _logger.Info(sb.ToString());

            var resultCollection = new List<SaleLocation>();
            //выбираем только в которых есть в наличии
            //и заполняем данные по доставке
            availableSapCodes.ForEach(x =>
            {
                if (existInStock.ContainsKey(x.SapCode))
                {
                    var shopInfo = String.Format("shop_{0}", x.SapCode);
                    var item =
                        geoLocations.FirstOrDefault(
                            g => g.From != null && g.From.Equals(shopInfo, StringComparison.InvariantCultureIgnoreCase));

                    if (item != null && item.DistanceMeters != null && item.DistanceMeters.Value < _maxtDistance)
                    {
                        x.CurrentDeliveryResource = item.DistanceMeters.Value;
                    }
                    else
                    {
                        x.CurrentDeliveryResource = _defaultDistance;
                    }
                    x.UsedDeliveryResource += x.CurrentDeliveryResource;
                    x.UsedDeliveryResource = x.UsedDeliveryResource/(x.Сapacity*_measure);
                        //TODO 1. Нужно явное поле 2.DivideByZero
                    resultCollection.Add(x);
                }

            });

            sb.Clear();
            sb.AppendLine(String.Format("Заказ - {0};SaleLocation - {1}; Дата доставки:{2} Геоданные:", orderId,
                saleLocationSapCode, deliveryDate));
            foreach (var item in resultCollection.OrderBy(x => x.UsedDeliveryResource))
            {
                sb.AppendLine(String.Format("sap code - {0};%загрузки - {1};кол-во метров до точки - {2}", item.SapCode,
                    item.UsedDeliveryResource, item.CurrentDeliveryResource));
            }
            _logger.Info(sb.ToString());
            //получаем подходящий
            var sapCode = resultCollection.OrderBy(x => x.UsedDeliveryResource).FirstOrDefault();
            if (sapCode != null)
            {
                stockLocationSapCode = sapCode.SapCode;
                //сохраняем в БД
                SaveUsedDeliveryResources(saleLocationSapCode, sapCode.SapCode, deliveryDate,
                    sapCode.CurrentDeliveryResource);
            }
            _logger.Info(String.Format("deliveryDate -{0}; m - {1}; stock - {2}", deliveryDate,
                sapCode != null ? sapCode.CurrentDeliveryResource : 0, stockLocationSapCode));

            return stockLocationSapCode;
        }


        /// <summary>
        /// Получение доступных сапкодов  согласно Outcomes
        /// и данным по доставке. 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="saleLocationSapCode"></param>
        /// <param name="shipmingMethod"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        private List<SaleLocation> GetAvaliableStocksWithGeoData(String orderId, String saleLocationSapCode,
            StockShippingMethod shipmingMethod, DateTime deliveryDate)
        {
            var availibleSapCodes = new List<SaleLocation>();
            var usedResources = new List<UsedDeliveryResource>();
            WithDb(db =>
            {
                var outcomesSapCodes =
                    GetStockLocationOutcomesByOrderId(orderId, db).Select(x => x.StockLocation.SapCode).ToList();

                availibleSapCodes = GetAvailableStocks(saleLocationSapCode, db).ToList()
                    .Where(x => x.AvailableShippingMethods.HasValue
                                && ((StockShippingMethod) x.AvailableShippingMethods.Value).HasFlag(shipmingMethod))
                    .Select(
                        sl =>
                            new SaleLocation
                            {
                                Rate = sl.Rate,
                                SapCode = sl.StockLocation.SapCode,
                                Сapacity = sl.Capacity ?? 0
                            })
                    .Where(
                        x =>
                            !outcomesSapCodes.Any(o => o.Equals(x.SapCode, StringComparison.InvariantCultureIgnoreCase)))
                    .ToList();
                usedResources =
                    GetUsedDeliveryResources(saleLocationSapCode, availibleSapCodes.Select(x => x.SapCode), deliveryDate,
                        db).ToList();

            });
            availibleSapCodes.ForEach(x =>
            {
                var used =
                    usedResources.Where(
                        u =>
                            u.StockLocation.SapCode.Equals(x.SapCode, StringComparison.InvariantCultureIgnoreCase) &&
                            u.UsedResource.HasValue).Sum(s => s.UsedResource);
                x.UsedDeliveryResource = used ?? 0;
            });
            return availibleSapCodes;
        }


        private IEnumerable<UsedDeliveryResource> GetUsedDeliveryResources(String saleLocationSapCode,
            IEnumerable<String> stockSapCodes, DateTime deliveryDate, SeleSelectorDbContext db)
        {
            return db.UsedDeliveryResources
                .Include(s => s.SaleLocation)
                .Include(s => s.StockLocation)
                .Where(u => u.DeliveryDate != null
                            &&
                            (u.DeliveryDate.Year == deliveryDate.Year && u.DeliveryDate.Month == deliveryDate.Month &&
                             u.DeliveryDate.Day == deliveryDate.Day)
                            && u.SaleLocation != null
                            && u.SaleLocation.SapCode != null
                            &&
                            u.SaleLocation.SapCode.Equals(saleLocationSapCode,
                                StringComparison.InvariantCultureIgnoreCase)
                            && u.StockLocation != null
                            && u.StockLocation.SapCode != null
                            &&
                            stockSapCodes.Any(
                                x => x.Equals(u.StockLocation.SapCode, StringComparison.InvariantCultureIgnoreCase)))
                .ToList();

        }

        private IEnumerable<SaleLocationApi.RouteResponseItemTO> GetGeoLocations(List<String> sapCodes,
            String deliveryAddress)
        {
            var multiplexorClient = new MultiplexorClient(_muxUrl, _apiKey, _timeOut);
            multiplexorClient.SaleLocation.SaleLocationRoute(sapCodes, deliveryAddress);

            multiplexorClient.Execute();
            var executionCommandResults = multiplexorClient.ExecutionCommandResults;

            var items = new List<SaleLocationApi.RouteResponseItemTO>();
            var result = executionCommandResults[0].Result;
            if (result.Errors != null)
            {
                if (result.Errors.ContainsKey(_geoDataSaleLocationException))
                    _logger.Info("Сервис геолокации отвалился сипец!");
                foreach (var error in result.Errors)
                {
                    _logger.Info(String.Format("Сервис геолокации вернул ошибки:{0}-{1}", error.Key, error.Value));
                }
                if (!result.Errors.ContainsKey(_geoDataSaleLocationException))
                {
                    throw new Exception(String.Format("Сервис геолокации вернул ошибки:{0}-{1}",
                        result.Errors.First().Key, result.Errors.First().Value));
                }
            }
            var value = result.Result as SaleLocationApi.RouteResponseItemTO[];
            if (value != null)
                items = value.ToList();
            return items;
        }


        #endregion geodata

        #region rating

        /// <summary>
        /// Распределение по рейтингу
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="saleLocationSapCode"></param>
        /// <param name="orderLines"></param>
        /// <param name="shippingMethod"></param>
        /// <returns></returns>
        private String GetStockLocationByRating(String orderId, String saleLocationSapCode, List<OrderLine> orderLines,
            StockShippingMethod shippingMethod)
        {
            var availibleSapCodes = GetAvaliableStockLocations(orderId, saleLocationSapCode, shippingMethod);
            _logger.InfoFormat("Выбираем магазин по рейтингу");
            var stockLocationSapCode = String.Empty;
            foreach (var sapCode in availibleSapCodes.OrderByDescending(x => x.Rate))
            {
                var shopName = String.Format("shop_{0}", sapCode.SapCode);

                var searchRequest = new GetItemsRequest
                {
                    Channel = _mediaMarktChannel,
                    SaleLocation = shopName,
                    Articles = orderLines.Select(ol => ol.Article.ToString(CultureInfo.InvariantCulture)).ToArray()
                };
                var result = _searchService.GetArticleStockStatus(searchRequest);
                var enough = orderLines.All(ol => result != null && result.Items.Any(obi => obi.Article == ol.Article
                                                                                            && obi.Qty >= ol.Quantity
                                                                                            &&
                                                                                            obi.Status ==
                                                                                            StockStatusEnum.InStock));
                if (!enough)
                    continue;

                stockLocationSapCode = sapCode.SapCode;
                _logger.InfoFormat("Выбирали магазин - " + stockLocationSapCode);
                break;
            }
            return stockLocationSapCode;
        }

        private IEnumerable<SaleLocation> GetAvaliableStockLocations(String orderId, String saleLocationSapCode,
            StockShippingMethod shipmingMethod)
        {
            var availibleSapCodes = new List<SaleLocation>();
            WithDb(db =>
            {
                var outcomesSapCodes =
                    GetStockLocationOutcomesByOrderId(orderId, db).Select(x => x.StockLocation.SapCode).ToList();

                availibleSapCodes = GetAvailableStocks(saleLocationSapCode, db).ToList()
                    .Where(x => x.AvailableShippingMethods.HasValue
                                && ((StockShippingMethod) x.AvailableShippingMethods.Value).HasFlag(shipmingMethod))
                    .Select(
                        sl =>
                            new SaleLocation
                            {
                                Rate = sl.Rate,
                                SapCode = sl.StockLocation.SapCode,
                                Сapacity = sl.Capacity ?? 0
                            })
                    .Where(
                        x =>
                            !outcomesSapCodes.Any(o => o.Equals(x.SapCode, StringComparison.InvariantCultureIgnoreCase)))
                    .ToList();
            });
            return availibleSapCodes;
        }

        #endregion rating

        /// <summary>
        /// Получаем наличие в магазинах
        /// </summary>
        /// <param name="availibleSapCodes"></param>
        /// <param name="orderLines"></param>
        /// <returns></returns>
        private Dictionary<String, String> GetExistInStockScope(IEnumerable<SaleLocation> availibleSapCodes,
            List<OrderLine> orderLines)
        {
            var existInStock = new Dictionary<String, String>();
            var saleLocations = availibleSapCodes as SaleLocation[] ?? availibleSapCodes.ToArray();
            var request = new GetItemsMultipleStocksRequest
            {
                Channel = _mediaMarktChannel,
                Articles = orderLines.Select(ol => ol.Article.ToString(CultureInfo.InvariantCulture)).ToArray(),
                SaleLocations = saleLocations.Select(sapCode => String.Format("shop_{0}", sapCode.SapCode)).ToArray()
            };
            var stockResult = _searchService.GetArticleStocksStatus(request);


            foreach (var sapCode in saleLocations)
            {
                var shopName = String.Format("shop_{0}", sapCode.SapCode);

                var enough =
                    orderLines.All(ol => stockResult != null && stockResult.Items.Any(obi => obi.Article == ol.Article
                                                                                             &&
                                                                                             obi.Qtys[shopName] >=
                                                                                             ol.Quantity
                                                                                             &&
                                                                                             obi.Statuses[shopName] ==
                                                                                             StockStatusEnum.InStock));
                if (enough)
                    existInStock.Add(sapCode.SapCode, shopName);
            }

            return existInStock;
        }

        private List<OrderLine> FilterOrderLines(OrderInfo orderInfo)
        {
            if (orderInfo.OrderLines == null || !orderInfo.OrderLines.Any())
                throw new ArgumentException("Order lines не содержат элементов!");
            var orderLines =
                orderInfo.OrderLines.Where(ol => ol.Quantity > 0 && !_infiniteArticles.Articles.Contains(ol.Article))
                    .ToList();

            var productInfos = _productInfoService.GetProductInfo(orderLines.Select(p => p.Article).ToArray())
                .Where(p => p.ProductType != ProductType.DeliveryService)
                .Where(p => p.ProductType != ProductType.InstallationService)
                .Where(p => p.ProductType != ProductType.WarrantyPlus)
                .ToList();

            return orderLines.Where(ol => productInfos.Any(p => p.Article == ol.Article)).ToList();
        }

        public GetSaleLocationOutcomesResult GetStockOutcomes(String orderId)
        {
            var result = new GetSaleLocationOutcomesResult();
            try
            {
                WithDb(db =>
                {
                    var oucomes =
                        GetStockLocationOutcomesByOrderId(orderId, db)
                            .ToList()
                            .Select(x => x.ToSaleLocationOutcome())
                            .ToList();
                    result.Result = oucomes;
                });
            }
            catch (Exception ex)
            {
                _logger.Error("GetSaleLocationOutcomes", ex);
                result.AddError(ex.Message);
            }

            return result;
        }

        public OperationResult SaveStockLocationOutcome(SaleLocationOutcome outcome)
        {
            var result = new OperationResult();

            try
            {
                var saleLocationSapCode = GetSaleLocationSapCodeByOrderId(outcome.OrderId);
                if (String.IsNullOrEmpty(saleLocationSapCode))
                    throw new Exception("Не удалось установить SapCode для SaleLocation");
                WithDb(db =>
                {
                    var item = outcome.ToStockLocationOutcome();
                    var stockLocation =
                        db.StockLocations.FirstOrDefault(
                            x =>
                                x.SapCode != null &&
                                x.SapCode.Equals(outcome.SapCode, StringComparison.InvariantCultureIgnoreCase));
                    if (stockLocation == null)
                        throw new Exception(String.Format("StockLocation с SapCod {0} в системе не зарегистрирован",
                            outcome.SapCode));
                    item.StockLocation = stockLocation;
                    var saleLocation =
                        db.SaleLocations.FirstOrDefault(
                            x =>
                                x.SapCode != null &&
                                x.SapCode.Equals(saleLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
                    if (saleLocation == null)
                        throw new Exception(String.Format("SaleLocation с SapCod {0} в системе не зарегистрирован",
                            outcome.SapCode));
                    item.SaleLocation = saleLocation;
                    db.StockLocationOutcomes.Add(item);
                    db.SaveChanges();

                });

            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("SaveStockLocationOutcome {0}: {1}", outcome.OrderId, ex);
                result.AddError(ex.Message);
            }
            return result;
        }

        private String GetSaleLocationSapCodeByOrderId(String orderId)
        {
            var result = String.Empty;

            var orderData = _onlineOrderService.GerOrderDataByOrderId(orderId);
            if (orderData != null && orderData.OrderInfo != null &&
                !String.IsNullOrEmpty(orderData.OrderInfo.SaleLocationSapCode))
                return orderData.OrderInfo.SaleLocationSapCode;

            return result;
        }

        private StockLocationSelectorStrategyType GetStockLocationSelectorStrategy(String saleLocationSapCode)
        {
            var strategy = StockLocationSelectorStrategyType.Rating;
            WithDb(db =>
            {
                var saleLocation =
                    db.SaleLocations.FirstOrDefault(
                        x =>
                            x.SapCode != null &&
                            x.SapCode.Equals(saleLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
                if (saleLocation != null && saleLocation.StockLocationSelectorStrategy.HasValue)
                    strategy = (StockLocationSelectorStrategyType) saleLocation.StockLocationSelectorStrategy.Value;

            });
            return strategy;
        }

        private static IEnumerable<StockLocationOutcome> GetStockLocationOutcomesByOrderId(String orderId,
            SeleSelectorDbContext db)
        {
            return db.StockLocationOutcomes
                .Include(x => x.SaleLocation)
                .Include(x => x.StockLocation)
                .Where(x => x.OrderId != null && x.OrderId.Equals(orderId, StringComparison.InvariantCultureIgnoreCase));
        }

        private static IEnumerable<AvailableStock> GetAvailableStocks(String saleLocationSapCode,
            SeleSelectorDbContext db)
        {
            return db.AvailableStocks
                .Include(x => x.StockLocation)
                .Include(x => x.SaleLocation)
                .Where(x => x.SaleLocation != null
                            && x.SaleLocation.SapCode != null
                            &&
                            x.SaleLocation.SapCode.Equals(saleLocationSapCode,
                                StringComparison.InvariantCultureIgnoreCase)
                            && x.AvailableShippingMethods.HasValue);
        }



        public void SaveUsedDeliveryResources(String saleLocationSapCode, String stockLocationSapCode,
            DateTime deliveryDate, Double deliveryResources)
        {
            WithDb(db =>
            {
                var saleLocation =
                    db.SaleLocations.FirstOrDefault(
                        x =>
                            x.SapCode != null &&
                            x.SapCode.Equals(saleLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
                var stockLocation =
                    db.StockLocations.FirstOrDefault(
                        x =>
                            x.SapCode != null &&
                            x.SapCode.Equals(stockLocationSapCode, StringComparison.InvariantCultureIgnoreCase));
                if (saleLocation != null && stockLocation != null)
                {
                    var used = new UsedDeliveryResource
                    {
                        DeliveryDate = deliveryDate,
                        SaleLocation = saleLocation,
                        StockLocation = stockLocation,
                        UsedResource = deliveryResources
                    };
                    db.UsedDeliveryResources.Add(used);
                    db.SaveChanges();
                }
            });
        }

        internal OperationResult<IList<AvailableStockLocation>> GetStockLocations(string saleLocationSapCode)
        {

            var stocks = new OperationResult<IList<AvailableStockLocation>>();
            try
            {
                stocks.Result = new List<AvailableStockLocation>();
                WithDb(db =>
                {
                    var stockLocations =
                        db.AvailableStocks.Where(
                            x =>
                                x.SaleLocation.SapCode != null &&
                                x.SaleLocation.SapCode.Equals(saleLocationSapCode,
                                    StringComparison.InvariantCultureIgnoreCase)).ToList();

                    foreach (var stockLocation in stockLocations)
                    {
                        stocks.Result.Add(new AvailableStockLocation
                        {
                            StockLocationSapCode = stockLocation.StockLocation.SapCode,
                            Rate = stockLocation.Rate,
                            AvailableShippingMethods =
                                stockLocation.AvailableShippingMethods.HasValue
                                    ? ((StockShippingMethod)stockLocation.AvailableShippingMethods.Value).ToString()
                                    : string.Empty
                        });
                    }
                });

                return stocks;

            }
            catch (Exception e)
            {
                _logger.ErrorFormat("GetStockLocations: {0}", e);
                stocks.AddError(e.Message);
                return stocks;
            }
        }
    }
}