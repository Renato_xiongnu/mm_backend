﻿using System.Collections.Generic;
using Common.Logging;
using Orders.Backend.Services.SaleLocationSelector.Contracts;
using Orders.Backend.Services.SaleLocationSelector.Proxy.ProductBackend;
using Orders.Backend.Services.SaleLocationSelector.Proxy.ProductInfoService;

namespace Orders.Backend.Services.SaleLocationSelector.Bll
{
    public class SaleLocationSelector : ISaleLocationService
    {
        private readonly StockLocationSelectorService _service;
        private const string MediaMarktChannel = "MM";

        public SaleLocationSelector(ILog logger, IOrdersBackendService searchService, IProductInfoService productInfoService, IInfiniteArticlesProvider infiniteArticles, Proxy.OrdersOnline.IOrderService onlineOrderService)
        {
            _service = new StockLocationSelectorService(logger, searchService, productInfoService, infiniteArticles, onlineOrderService, MediaMarktChannel);
        }

        public GetSaleLocationResult GetSaleLocation(OrderInfo request)
        {
            return _service.GetSaleLocation(request);
        }

        public GetSaleLocationOutcomesResult GetSaleLocationOutcomes(string orderId)
        {
            return _service.GetStockOutcomes(orderId);
        }

        public OperationResult SaveSaleLocationOutcome(SaleLocationOutcome request)
        {
            return _service.SaveStockLocationOutcome(request);
        }

        public OperationResult<IList<AvailableStockLocation>> GetStockLocations(string saleLocationSapCode)
        {
            return _service.GetStockLocations(saleLocationSapCode);
        }
    }
}