﻿using System;
using System.Web;
using Autofac;
using Autofac.Configuration;
using Autofac.Integration.Wcf;
using Common.Logging;

namespace Orders.Backend.Services.SaleLocationSelector
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var logger = LogManager.GetLogger("logger");
            var builder = new ContainerBuilder();
            var module = new ConfigurationSettingsReader("autofac");
            builder.RegisterModule(module);
            builder.RegisterInstance(logger).As<ILog>();
            AutofacHostFactory.Container = builder.Build();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}