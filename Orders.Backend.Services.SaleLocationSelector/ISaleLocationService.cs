﻿using System.Collections.Generic;
using System.ServiceModel;
using Orders.Backend.Services.SaleLocationSelector.Contracts;

namespace Orders.Backend.Services.SaleLocationSelector
{
    [ServiceContract]
    public interface ISaleLocationService
    {
        [OperationContract]
        GetSaleLocationResult GetSaleLocation(OrderInfo request);

        [OperationContract]
        GetSaleLocationOutcomesResult GetSaleLocationOutcomes(string orderId);

        [OperationContract]
        OperationResult SaveSaleLocationOutcome(SaleLocationOutcome request);

        [OperationContract]
        OperationResult<IList<AvailableStockLocation>> GetStockLocations(string saleLocationSapCode);
    }
}
