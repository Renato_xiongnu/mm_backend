﻿using Orders.Backend.Services.SaleLocationSelector.Bll;

namespace Orders.Backend.Services.SaleLocationSelector.Mocks
{
    public class InfiniteArticlesProviderMock : IInfiniteArticlesProvider
    {
        private static long[] _articles =
        {
            1142669,
            1079891
        };

        public long[] Articles
        {
            get { return _articles; }
            private set { _articles = value; }
        }
    }
}