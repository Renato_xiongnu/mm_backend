﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.ServiceProcess;

namespace PushServices
{
    [RunInstaller(true)]
    public partial class PushServicesInstaller : System.Configuration.Install.Installer
    {
        public PushServicesInstaller()
        {
            InitializeComponent();
        }

        private void OnServiceProcessInstallerCommitted(object sender, InstallEventArgs e)
        {
            //try
            //{
            //    new ServiceController(giftCertPushServiceInstaller.ServiceName).Start();
            //    new ServiceController(giftCertCreditRobotServiceInstaller.ServiceName).Start();
            //}
            //catch (Exception exception)
            //{
            //    var eventLog = new EventLog();
            //    eventLog.WriteEntry(exception.ToString());
            //}
        }

        private void OnServiceProcessInstallerBeforeUninstall(object sender, InstallEventArgs e)
        {
            try
            {
                var serviceController = new ServiceController(giftCertPushServiceInstaller.ServiceName);
                if (serviceController.Status != ServiceControllerStatus.Stopped &&
                    serviceController.Status != ServiceControllerStatus.StopPending)
                {
                    serviceController.Stop();
                }

                serviceController = new ServiceController(giftCertCreditRobotServiceInstaller.ServiceName);
                if (serviceController.Status != ServiceControllerStatus.Stopped &&
                    serviceController.Status != ServiceControllerStatus.StopPending)
                {
                    serviceController.Stop();
                }
            }
            catch (Exception exception)
            {
                var eventLog = new EventLog();
                eventLog.WriteEntry(exception.ToString());
            }
        }
    }
}
