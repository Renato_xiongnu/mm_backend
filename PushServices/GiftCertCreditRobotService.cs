﻿using System.ServiceProcess;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

using PushServices.Interfaces.Pooling;

namespace PushServices
{
    partial class GiftCertCreditRobotService : ServiceBase
    {
        private readonly IUnityContainer _container;
        private readonly IPoolingService _pollingService;

        public GiftCertCreditRobotService()
        {
            InitializeComponent();

            CanPauseAndContinue = true;
            CanStop = true;

            _container = new UnityContainer().LoadConfiguration();

            _pollingService = _container.Resolve<IPoolingService>("GiftCertCreditRobotService");

            log4net.Config.XmlConfigurator.Configure();

            //(_pollingService as PushServices.Implementation.GiftCertCreditRobot.GiftCertCreditRobot).Test();
        }

        protected override void OnStart(string[] args)
        {
            _pollingService.StartPolling();
        }

        protected override void OnStop()
        {
            _pollingService.StopPolling();
        }

        protected override void OnPause()
        {
            _pollingService.StopPolling();
            base.OnPause();
        }

        protected override void OnContinue()
        {
            _pollingService.ContinuePolling();
            base.OnContinue();
        }
    }
}
