﻿using System.ServiceProcess;

namespace PushServices
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

//#if DEBUG
            //Testing
            //var s = new GiftCertPushService();
            //var s = new GiftCertCreditRobotService();
            //System.Threading.Thread.Sleep(1000000000);
            //return;
//#else

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new GiftCertPushService(),
                new GiftCertCreditRobotService()
            };
            ServiceBase.Run(ServicesToRun);
//#endif
        }
    }
}
