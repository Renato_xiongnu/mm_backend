﻿namespace PushServices
{
    partial class PushServicesInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.giftCertPushServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.giftCertCreditRobotServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // giftCertPushServiceInstaller
            // 
            this.giftCertPushServiceInstaller.Description = "This service gets GiftCert data by numbers" +
                ".";
            this.giftCertPushServiceInstaller.DisplayName = "Orders.Backend - GiftCertPushService";
            this.giftCertPushServiceInstaller.ServiceName = "Orders.Backend.GiftCertPushService";
            this.giftCertPushServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // giftCertCreditRobotServiceInstaller
            // 
            this.giftCertCreditRobotServiceInstaller.Description = "This service increases GiftCert amount" +
                ".";
            this.giftCertCreditRobotServiceInstaller.DisplayName = "Orders.Backend - GiftCertCreditRobotService";
            this.giftCertCreditRobotServiceInstaller.ServiceName = "Orders.Backend.GiftCertCreditRobotService";
            this.giftCertCreditRobotServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // serviceProcessInstaller
            // 
            this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstaller.Password = null;
            this.serviceProcessInstaller.Username = null;
            this.serviceProcessInstaller.Committed += new System.Configuration.Install.InstallEventHandler(this.OnServiceProcessInstallerCommitted);
            this.serviceProcessInstaller.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.OnServiceProcessInstallerBeforeUninstall);
            // 
            // PushServicesInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.giftCertPushServiceInstaller,
            this.giftCertCreditRobotServiceInstaller,
            this.serviceProcessInstaller});
        }

        #endregion

        private System.ServiceProcess.ServiceInstaller giftCertPushServiceInstaller;
        private System.ServiceProcess.ServiceInstaller giftCertCreditRobotServiceInstaller;
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
    }
}