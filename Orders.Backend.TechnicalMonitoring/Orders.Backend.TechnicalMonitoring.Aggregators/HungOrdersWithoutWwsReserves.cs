﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class HungOrdersWithoutWwsReserves : BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.HungOrdersWithoutWwsReserves; }
        }

        protected override string StatsMetricName
        {
            get { return "hung_orders_without_reserves"; }
        }
    }
}
