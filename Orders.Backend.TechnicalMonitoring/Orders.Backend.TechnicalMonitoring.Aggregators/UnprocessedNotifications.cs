﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class UnprocessedNotifications : BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.UnprocessedNotificationsQuery; }
        }

        protected override string StatsMetricName
        {
            get { return "unprocessed_notifications"; }
        }
    }
}
