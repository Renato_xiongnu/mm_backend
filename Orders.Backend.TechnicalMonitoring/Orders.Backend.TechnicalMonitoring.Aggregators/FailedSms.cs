﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    class FailedSms:BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.FailedSms; }
        }

        protected override string StatsMetricName
        {
            get { return "failed_sms"; }
        }
    }
}
