﻿using NLog;
using System;
using System.Threading.Tasks;
using MMS.Monitoring.Statistics;
using LogManager = NLog.LogManager;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main()
        {
            try
            {
                Logger.Info("Start gathering metrics");

                var unprocessedOrders = new UnprocessedOrders().ExecuteScalar<Int32>();
                var unprocessedBaskets = new UnprocessedBaskets().ExecuteScalar<Int32>();
                var unprocessedCommands = new TimeFromLastCommandSent().ExecuteScalar<Int32>();
                var unprocessedNotifications = new UnprocessedNotifications().ExecuteScalar<Int32>();
                var unprocessedTasks = new UnprocessedTasks().ExecuteScalar<Int32>();
                var failedTasksCompletion = new FailedTasksCompletions().ExecuteScalar<Int32>();
                var failedNotifications = new FailedNotifications().ExecuteScalar<Int32>();
                var failedBaskets = new FailedBaskets().ExecuteScalar<Int32>();
                var hungOrdersWithoutReserves = new HungOrdersWithoutWwsReserves().ExecuteScalar<Int32>();
                var failedSms = new FailedSms().ExecuteScalar<Int32>();
                var unprocessedSms = new UnprocessedSms().ExecuteScalar<Int32>();

                var metrics =
                    Task.WhenAll(unprocessedOrders, unprocessedBaskets, unprocessedCommands, unprocessedNotifications,
                        unprocessedTasks, failedTasksCompletion, failedNotifications,
                        failedBaskets, hungOrdersWithoutReserves, failedSms, unprocessedSms).Result;
                StatsEngine.Send(metrics);

                Logger.Info("Finish sending");
            }
            catch (AggregateException ex)
            {
                Logger.Error("Failed to send Stats metric: {0}", ex.InnerException);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to send Stats metric: {0}", ex);
            }
          
        }
    }
}
