﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class FailedTasksCompletions : BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.UnprocessedTasksQuery; }
        }

        protected override string StatsMetricName
        {
            get { return "unprocessed_tasks"; }
        }
    }
}
