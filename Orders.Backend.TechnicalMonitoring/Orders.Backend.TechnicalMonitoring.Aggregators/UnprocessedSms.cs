﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    class UnprocessedSms:BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.UnprocessedSms; }
        }

        protected override string StatsMetricName
        {
            get { return "unprocessed_sms"; }
        }
    }
}
