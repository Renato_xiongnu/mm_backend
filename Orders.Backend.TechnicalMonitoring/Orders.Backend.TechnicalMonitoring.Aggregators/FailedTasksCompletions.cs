﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class UnprocessedTasks : BaseMonitoringParametrsGetter
    {
        //private const String SqlQuery = AppSettings//"SELECT COUNT(*) FROM TaskCompletionQueues WITH (NOLOCK) WHERE CompletionStatus = 110 and CreateDate >= DATEADD(day, -1, GETDATE())"; 

        protected override string SqlCommand
        {
            get { return AppSettings.Default.FailedTasksCompletionsQuery; }
        }

        protected override string StatsMetricName
        {
            get { return "failed_tasks_completion"; } 
        }
    }
}
