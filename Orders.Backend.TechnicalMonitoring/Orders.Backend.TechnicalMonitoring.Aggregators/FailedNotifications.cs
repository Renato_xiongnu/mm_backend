﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class FailedNotifications : BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.FailedNotifications; }
        }

        protected override string StatsMetricName
        {
            get { return "failed_notifications"; }
        }
    }
}
