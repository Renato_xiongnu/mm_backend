﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Transactions;
using MMS.Monitoring.Statistics;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public abstract class BaseMonitoringParametrsGetter
    {
        protected abstract String SqlCommand { get; }

        protected abstract String StatsMetricName { get; }
        
        protected String ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings[GetType().Name].ConnectionString; }
        }

        public async Task<StatsMetric> ExecuteScalar<T>()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();//
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted},TransactionScopeAsyncFlowOption.Enabled))
                {
                    var command = new SqlCommand(SqlCommand, connection);
                    var result = await command.ExecuteScalarAsync();
                    transaction.Complete();

                    var item = default(T);
                    if (result is T)
                        item = (T) result;

                    return new StatsMetric(StatsMetricName, item, StatsDefinitions.SupportedMetricType.Gauge);
                    //StatsMetric.Gauge("", item);   
                }
            }
        }
    }

}

