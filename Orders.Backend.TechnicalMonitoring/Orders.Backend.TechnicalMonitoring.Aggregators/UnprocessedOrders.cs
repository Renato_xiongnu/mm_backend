﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class UnprocessedOrders : BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.UnprocessedOrdersQuery; }
        }

        protected override string StatsMetricName
        {
            get { return "unprocessed_orders"; }
        }
    }
}
