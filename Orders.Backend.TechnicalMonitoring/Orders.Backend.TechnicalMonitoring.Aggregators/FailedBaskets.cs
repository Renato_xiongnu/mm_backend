﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    class FailedBaskets:BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.FailedBaskets; }
        }

        protected override string StatsMetricName
        {
            get { return "failed_baskets"; }
        }
    }
}
