﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class TimeFromLastCommandSent: BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.TimeFromLastCommandSentQuery; }
        }

        protected override string StatsMetricName
        {
            get { return "time_from_command_sent"; }
        }
    }
}
