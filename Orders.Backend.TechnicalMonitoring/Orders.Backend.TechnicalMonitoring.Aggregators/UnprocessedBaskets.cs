﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Orders.Backend.TechnicalMonitoring.Aggregators
{
    public class UnprocessedBaskets : BaseMonitoringParametrsGetter
    {
        protected override string SqlCommand
        {
            get { return AppSettings.Default.UnprocessedBasketsQuery; }
        }

        protected override string StatsMetricName
        {
            get { return "unprocessed_baskets"; }
        }
    }
}
