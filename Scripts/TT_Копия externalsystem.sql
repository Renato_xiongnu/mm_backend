/****** Script for SelectTopNRows command from SSMS  ******/
  
  UPDATE [dbo].[TaskTypes]
   SET [SlaControlDate] = tfrom.SlaControlDate
      ,[SlaName] = tfrom.SlaName
      ,[MaxSkillSetRunTime] = tfrom.MaxSkillSetRunTime
      ,[MaxUserRuntime] = tfrom.MaxUserRuntime   
      ,[SlaSkillSet_SkillSetId] = tfrom.[SlaSkillSet_SkillSetId]
      ,[SkillSet_SkillSetId] = tfrom.[SkillSet_SkillSetId]
      ,[CanPostponeCallingWorkflow] = tfrom.[CanPostponeCallingWorkflow]
      ,[NotificationGroup_Id] = tfrom.[NotificationGroup_Id]
      ,[WorkitemPageUrl] = tfrom.[WorkitemPageUrl]
      ,[TaskPageUrl] = tfrom.[TaskPageUrl]
 FROM [dbo].[TaskTypes]
 INNER JOIN [dbo].[TaskTypes] as tfrom ON tfrom.[Name]=[dbo].[TaskTypes].[Name]-- and tfrom.[ExternalSystem_ExternalSystemId]=[dbo].[TaskTypes].[ExternalSystem_ExternalSystemId]
 WHERE 
		tfrom.[ExternalSystem_ExternalSystemId]='ISmartStartCashOrderProcessingServiceV2_0'
		and
		[dbo].[TaskTypes].[ExternalSystem_ExternalSystemId]='IMetroProcessingService'
		
		
SELECT TOP 1000 [TaskTypeId]
      ,[Name]
      ,[SlaControlDate]
      ,[SlaName]
      ,[MaxSkillSetRunTime]
      ,[MaxUserRuntime]
      ,[ExternalSystem_ExternalSystemId]
      ,[SlaSkillSet_SkillSetId]
      ,[SkillSet_SkillSetId]
      ,[CanPostponeCallingWorkflow]
      ,[NotificationGroup_Id]
      ,[WorkitemPageUrl]
      ,[TaskPageUrl]
  FROM [dbo].[TaskTypes]
  --where [ExternalSystem_ExternalSystemId] = 'ISmartStartCashOrderProcessingServiceV2_0'
GO

